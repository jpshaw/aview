# frozen_string_literal: true

require 'rails_helper'

describe Configuration::NetworkSerializer do
  describe '.execute' do
    let(:config_data)        { json_fixture('serializers/network_configurations/individual.json') }
    let(:parent_config_data) { json_fixture('serializers/network_configurations/inherited.json') }
    let(:serialized_data)    { json_fixture('serializers/network_configurations/serialized.json') }
    let(:config)             { double('NetworkConfiguration') }
    let(:result)             { described_class.new(config).execute }

    before do
      allow(config).to receive(:data).and_return(config_data)
      allow(config).to receive(:parent_inherited_data).and_return(parent_config_data)
    end

    it 'serializes the data' do
      expect(result.with_indifferent_access).to eq serialized_data
    end
  end
end
