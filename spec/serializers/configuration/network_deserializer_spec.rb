# frozen_string_literal: true

require 'rails_helper'

describe Configuration::NetworkDeserializer do
  describe '.execute' do
    let(:serialized_data)   { json_fixture('serializers/network_configurations/serialized.json') }
    let(:deserialized_data) { json_fixture('serializers/network_configurations/deserialized.json') }
    let(:config)            { double('NetworkConfiguration') }
    let(:result)            { described_class.new(serialized_data).execute }

    it 'deserializes the data' do
      expect(result.with_indifferent_access).to eq deserialized_data
    end
  end
end
