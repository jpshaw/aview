# frozen_string_literal: true

require 'rails_helper'

describe 'power_dns_backend/lookup.json.jbuilder' do
  it 'returns a single record' do
    assign(:records, [
             PowerDnsRecordManager::Record.new(
               type: 'A',
               name: 'localhost.devices.accns.net',
               content: '127.0.0.1',
               ttl: 120
             )
           ])

    render
    json = ActiveSupport::JSON.decode(rendered)
    expect(json['result'][0]['qtype']).to   eq 'A'
    expect(json['result'][0]['qname']).to   eq 'localhost.devices.accns.net'
    expect(json['result'][0]['content']).to eq '127.0.0.1'
    expect(json['result'][0]['ttl']).to     eq 120
  end

  it 'returns two records' do
    records = [
      PowerDnsRecordManager::Record.new(
        type: 'A',
        name: 'localhost.devices.accns.net',
        content: '127.0.0.1',
        ttl: 120
      ),
      PowerDnsRecordManager::Record.new(
        type: 'SOA',
        name: 'devices.accns.net',
        content: 'localhost test@accelerated.com 0',
        ttl: 86_400
      )
    ]

    assign(:records, records)

    render
    json = ActiveSupport::JSON.decode(rendered)
    expect(json['result'].length).to        eq 2
    expect(json['result'][0]['qtype']).to   eq 'A'
    expect(json['result'][1]['qtype']).to   eq 'SOA'
  end

  it 'returns no records' do
    assign(:records, [])

    render
    json = ActiveSupport::JSON.decode(rendered)
    expect(json['result']).to be false
  end
end
