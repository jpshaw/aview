# frozen_string_literal: true

require 'rails_helper'

describe DeviceBySignalStrengthRangeFinder do
  describe '#execute' do
    context 'when no range specified' do
      subject { described_class.new.execute }

      context 'with no matching devices' do
        it 'returns an empty array' do
          expect(subject).to eq []
        end
      end

      context 'with a device modem at 0%' do
        before do
          device = create(:test_device_with_modem_association, :deployed)
          create(:device_modem, device: device, signal: DeviceModem.percent_to_signal(0).to_i)
        end

        it 'returns data as an array with each element representing a column' do
          expect(subject).to eq [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        end
      end

      context 'with a device modem at 100%' do
        before do
          @device = create(:test_device_with_modem_association, :deployed)
          create(:device_modem, device: @device, signal: DeviceModem.percent_to_signal(100).to_i)
        end

        it 'returns data as an array with each element representing a column' do
          expect(subject).to eq [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
        end

        context 'and a second, inactive modem at 75%' do
          before do
            create(:device_modem, device: @device, active: false, signal: DeviceModem.percent_to_signal(75).to_i)
          end

          it 'returns data as an array with each active modem element representing a column' do
            expect(subject).to eq [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
          end
        end
      end
    end

    context 'with devices on the boundaries' do
      before do
        @devices = []
        buckets = [[0, 9], [10, 19], [20, 29], [30, 39], [40, 49], [50, 59], [60, 69], [70, 79], [80, 89], [90, 99], 100]

        buckets.each do |bucket|
          if bucket.is_a? Array
            # Floor
            @devices << create(:test_device_with_modem_association, :deployed)
            create(:device_modem, device: @devices.last, signal: DeviceModem.percent_to_signal(bucket.first).ceil)

            # Ceil
            @devices << create(:test_device_with_modem_association, :deployed)
            create(:device_modem, device: @devices.last, signal: DeviceModem.percent_to_signal(bucket.last).floor)
          else
            @devices << create(:test_device_with_modem_association, :deployed)
            create(:device_modem, device: @devices.last, signal: DeviceModem.percent_to_signal(bucket).to_i)
          end
        end
      end

      describe 'with range specified' do
        subject { described_class }

        context 'when given 0' do
          let(:result) { subject.new(range: 0).execute }

          it 'returns only the Devices with signal in the 0-9 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[0]
            expect(result).to include @devices[1]
          end
        end

        context 'when given 1' do
          let(:result) { subject.new(range: 1).execute }

          it 'returns only the Devices with signal in the 10-19 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[2]
            expect(result).to include @devices[3]
          end
        end

        context 'when given 2' do
          let(:result) { subject.new(range: 2).execute }

          it 'returns only the Devices with signal in the 20-29 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[4]
            expect(result).to include @devices[5]
          end
        end

        context 'when given 3' do
          let(:result) { subject.new(range: 3).execute }

          it 'returns only the Devices with signal in the 30-39 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[6]
            expect(result).to include @devices[7]
          end
        end

        context 'when given 4' do
          let(:result) { subject.new(range: 4).execute }

          it 'returns only the Devices with signal in the 40-49 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[8]
            expect(result).to include @devices[9]
          end
        end

        context 'when given 5' do
          let(:result) { subject.new(range: 5).execute }

          it 'returns only the Devices with signal in the 50-59 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[10]
            expect(result).to include @devices[11]
          end
        end

        context 'when given 6' do
          let(:result) { subject.new(range: 6).execute }

          it 'returns only the Devices with signal in the 60-69 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[12]
            expect(result).to include @devices[13]
          end
        end

        context 'when given 7' do
          let(:result) { subject.new(range: 7).execute }

          it 'returns only the Devices with signal in the 70-79 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[14]
            expect(result).to include @devices[15]
          end
        end

        context 'when given 8' do
          let(:result) { subject.new(range: 8).execute }

          it 'returns only the Devices with signal in the 80-89 percent range' do
            expect(result.size).to eq 2
            expect(result).to include @devices[16]
            expect(result).to include @devices[17]
          end
        end

        context 'when given 9' do
          let(:result) { subject.new(range: 9).execute }

          it 'returns only the Devices with signal in the 90-100 percent range' do
            expect(result.size).to eq 3
            expect(result).to include @devices[18]
            expect(result).to include @devices[19]
            expect(result).to include @devices[20]
          end
        end
      end

      describe 'with no range specified' do
        subject { described_class.new.execute }

        it 'returns an array of counts' do
          expect(subject).to eq [2, 2, 2, 2, 2, 2, 2, 2, 2, 3]
        end
      end
    end
  end
end
