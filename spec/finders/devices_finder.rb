# frozen_string_literal: true

require 'rails_helper'

describe DevicesFinder do
  let(:user)    { create(:user_with_org_permission) }
  let(:params)  { {} }
  let(:subject) { described_class.new(user, params) }

  describe '#execute' do
    context 'when the user has an organization filter' do
      let(:device) { create(:test_device) }

      before do
        user.update(filter_organization_id: device.organization_id)
        user.organization.children << device.organization
      end

      it 'returns filtered devices for the user' do
        expect(subject.execute.to_a).to eq [device]
      end
    end

    context 'when the user does not have an organization filter' do
      let(:device_1) { create(:test_device) }
      let(:device_2) { create(:test_device) }

      before do
        expect(device_1.organization).not_to eq device_2.organization
        user.organization.children << device_1.organization
        user.organization.children << device_2.organization
      end

      it 'returns managed devices for the user' do
        expect(subject.execute.to_a.sort).to eq [device_1, device_2].sort
      end
    end

    context 'for organization_id' do
      before { params.merge!(organization_id: organization_id) }

      context 'when the user is authorized for it' do
        let(:device)          { create(:test_device) }
        let(:organization_id) { device.organization_id }

        before { user.organization.children << device.organization }

        it 'returns devices in the organization' do
          expect(subject.execute.to_a).to eq [device]
        end
      end

      context 'when the user is not authorized for it' do
        let(:organization_id) { -1 }

        it 'returns an empty devices relation' do
          expect(subject.execute.to_a).to eq []
        end
      end
    end

    context 'for deployment' do
      let(:deployed_device)   { create(:test_device, :deployed) }
      let(:undeployed_device) { create(:test_device, :undeployed) }

      before do
        user.organization.children << deployed_device.organization
        user.organization.children << undeployed_device.organization
      end

      context 'when given deployed' do
        before { params.merge!(deployment: Devices::Deployment::DEPLOYED) }

        it 'returns deployed devices' do
          expect(subject.execute.to_a).to eq [deployed_device]
        end
      end

      context 'when given undeployed' do
        before { params.merge!(deployment: Devices::Deployment::UNDEPLOYED) }

        it 'returns undeployed devices' do
          expect(subject.execute.to_a).to eq [undeployed_device]
        end
      end
    end

    context 'for status' do
      let(:up_device)          { create(:test_device, :up) }
      let(:down_device)        { create(:test_device, :down) }
      let(:unreachable_device) { create(:test_device, :unreachable) }

      before do
        user.organization.children << up_device.organization
        user.organization.children << down_device.organization
        user.organization.children << unreachable_device.organization
      end

      context 'when given up' do
        before { params.merge!(status: DeviceState::States::UP) }

        it 'returns up devices' do
          expect(subject.execute.to_a).to eq [up_device]
        end
      end

      context 'when given down' do
        before { params.merge!(status: DeviceState::States::DOWN) }

        it 'returns down devices' do
          expect(subject.execute.to_a).to eq [down_device]
        end
      end

      context 'when given unreachable' do
        before { params.merge!(status: DeviceState::States::UNREACHABLE) }

        it 'returns unreachable devices' do
          expect(subject.execute.to_a).to eq [unreachable_device]
        end
      end
    end

    context 'for categories' do
      let(:gateway_category) { create(:gateway_category) }
      let(:gateway_device)   { create(:gateway_device, category: gateway_category) }

      before do
        user.organization.children << gateway_device.organization
      end

      context 'when given device categories' do
        before { params.merge!(categories: [gateway_category.id]) }

        it 'returns devices in the categories' do
          expect(subject.execute.to_a).to eq [gateway_device]
        end
      end
    end

    context 'for models' do
      let(:gateway_model)  { create(:gateway_8100_model) }
      let(:gateway_device) { create(:gateway_device, series: gateway_model) }

      before do
        user.organization.children << gateway_device.organization
      end

      context 'when given device models' do
        before { params.merge!(models: [gateway_model.id]) }

        it 'returns devices in the models' do
          expect(subject.execute.to_a).to eq [gateway_device]
        end
      end
    end
  end
end
