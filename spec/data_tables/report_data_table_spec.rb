# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReportDataTable, type: :model do
  context '#as_csv' do
    subject { ReportDataTable.new(@options).as_csv }

    before do
      @options = {}
      @options[:column_headers] = ['Device ID', 'Site'].to_json
      @options[:column_values] = [%w[0011 Acc], ['1122', 'AT&T']].to_json
    end

    it 'serializes the data correctly' do
      expect(subject).to eq [
        { 'Device ID' => '0011', 'Site' => 'Acc' },
        { 'Device ID' => '1122', 'Site' => 'AT&T' }
      ]
    end
  end
end
