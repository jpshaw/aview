# frozen_string_literal: true

require 'rails_helper'

describe DnsDataTable do
  subject { described_class.new(view_context, dns_type) }

  let(:dns_type) { 'type' }
  let(:view_context) do
    double('view context',
           params: {},
           link_to: '',
           admin_dns_path: '',
           edit_admin_dns_path: '')
  end

  before do
    allow(subject).to receive(:view).and_return(view_context)
  end

  describe 'SOA records' do
    let(:dns_type) { 'soa' }

    context 'when an soa record does not have a domain' do
      let(:records) { create_list(:dns_soa, 1) }

      before do
        allow(subject).to receive(:get_raw_records).and_return(records)
      end

      it 'does not throw an exception' do
        expect { subject.as_json }.not_to raise_error
      end
    end
  end
end
