# frozen_string_literal: true

require 'rails_helper'

describe MapsController do
  before do
    create_user
    sign_in @user
  end

  let(:results) { JSON.parse(response.body) }

  describe 'GET index' do
    context 'when given an html request' do
      it 'has a 200 status code' do
        get :index
        expect(response.status).to eq(200)
      end

      it 'renders the index page' do
        get :index
        expect(response).to render_template :index
      end
    end

    context 'when given a js request' do
      it 'has a 200 status code' do
        xhr :get, :index
        expect(response.status).to eq(200)
      end

      it 'renders the index page' do
        xhr :get, :index
        expect(response).to render_template :refresh
      end
    end
  end

  describe 'GET pins' do
    before do
      user = User.first
      @device_1 = create(:test_device, :with_random_location, :model_8100, organization: user.organization)
      @device_2 = create(:test_device, :with_random_location, :model_5300_dc, organization: user.organization)
    end

    context 'when given no parameters' do
      it 'returns all devices as pins in json format' do
        xhr :get, :pins
        parsed_response = JSON.parse(response.body)
        expect(parsed_response).to include @device_1.to_map_pin.with_indifferent_access
        expect(parsed_response).to include @device_2.to_map_pin.with_indifferent_access
      end
    end

    context 'when given filter parameters' do
      let(:params) { { filter: { models: [@device_2.series.id] } } }

      it 'returns devices as pins that match the filtered criteria' do
        xhr :get, :pins, params
        parsed_response = JSON.parse(response.body)
        expect(parsed_response).not_to include @device_1.to_map_pin.with_indifferent_access
        expect(parsed_response).to include @device_2.to_map_pin.with_indifferent_access
      end
    end
  end
end
