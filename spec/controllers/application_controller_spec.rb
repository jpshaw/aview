# frozen_string_literal: true

require 'rails_helper'

describe ApplicationController do
  let(:user) { create(:user) }

  before { sign_in user }

  context 'rescue from exceptions' do
    context 'if consider_all_requests_local is false' do
      before do
        @before_consider_all_requests_local = Rails.application.config.consider_all_requests_local
        Rails.application.config.consider_all_requests_local = false
      end

      after do
        Rails.application.config.consider_all_requests_local = @before_consider_all_requests_local
      end

      context 'from ActionController::InvalidCrossOriginRequest' do
        controller do
          def index
            raise ActionController::InvalidCrossOriginRequest, 'Anything'
          end
        end

        it 'responds with 400' do
          get :index
          expect(response).to have_http_status(:bad_request)
        end
      end
    end
  end

  describe '#uri_without_params' do
    let(:uri) { 'http://test.com' }

    it 'returns the root path if the uri given is nil' do
      expect(controller.send(:uri_without_params, nil)).to eq root_path
    end

    context 'when no query exist' do
      it 'returns the given uri' do
        expect(controller.send(:uri_without_params, uri)).to eq uri
      end
    end

    context 'when a query exists' do
      let(:uri_with_query) { 'http://test.com?a=1&b=2&c=3&d=4' }

      context 'when no params are given' do
        it 'returns the uri with no query' do
          expect(
            controller.send(:uri_without_params, uri_with_query)
          ).to eq 'http://test.com?'
        end
      end

      context 'when params are given' do
        it 'returns the uri with the given params removed from the query' do
          expect(
            controller.send(:uri_without_params, uri_with_query, :a, :b)
          ).to eq 'http://test.com?c=3&d=4'
        end
      end
    end
  end
end
