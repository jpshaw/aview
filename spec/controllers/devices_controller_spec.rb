# frozen_string_literal: true

require 'rails_helper'

describe DevicesController do
  before do
    allow_any_instance_of(User).to receive(:devices).and_return(Device.where(nil))
    create_user
    create_deployed_device
    sign_in @user
  end

  describe 'GET #show' do
    before do
      get :show, id: @device.mac
    end

    it 'retrieves the correct device for events' do
      expect(assigns(:presenter).device.mac).to eq(@device.mac)
    end
  end

  describe 'GET #events' do
    it 'retrieves the correct device for events' do
      get :events, id: @device.mac
      expect(assigns(:presenter).device.mac).to eq(@device.mac)
    end

    context 'with set timestamp ranges' do
      before do
        # Set created_at as accessible so I can set the created_at timestamp
        # easily when events are created for this test
        class DeviceEvent
          attr_accessible :created_at
        end
        @device.events.create(created_at: 1.hour.ago.utc)
        @device.events.create(created_at: 25.hours.ago.utc)
        @device.events.create(created_at: 5.days.ago.utc)
        @device.events.create(created_at: 8.days.ago.utc)
        @device.events.create(created_at: 15.days.ago.utc)
        @device.events.create(created_at: 32.days.ago.utc)
      end
    end
  end
end
