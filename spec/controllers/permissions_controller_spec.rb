# frozen_string_literal: true

require 'rails_helper'

describe PermissionsController do
  before do
    @current_user = create(:user)
    @root_org     = @current_user.organization

    @organization_ability = create(:organization_ability)
    @user_ability         = create(:user_ability)
    @device_ability       = create(:device_ability)

    @role = @root_org.admin_role
    @role.abilities << @organization_ability
    @role.abilities << @user_ability
    @role.abilities << @device_ability

    @user_permission = Permission.create(manager: @current_user, manageable: @root_org, role: @role)

    @manager_org = create(:random_regular_organization)
    @root_org.add_child(@manager_org)

    @manager_user = create(:user, organization: @manager_org)
    @organization_permission = Permission.create(manager: @manager_user, manageable: @manager_org, role: @manager_org.admin_role)

    @manageable = create(:random_regular_organization)
    @root_org.add_child(@manageable)

    @params = {}

    sign_in @current_user
  end

  def set_manager_to_organization
    @manager                  = @manager_org
    @params[:organization_id] = @manager.slug
  end

  def set_manager_to_user
    @manager          = @manager_user
    @params[:user_id] = @manager.id
  end

  describe 'GET #index' do
    context 'when the manager is an organization' do
      before { set_manager_to_organization }

      it_behaves_like 'permission :index'
    end

    context 'when the manager is a user' do
      before { set_manager_to_user }

      it_behaves_like 'permission :index'
    end
  end

  describe 'GET #new' do
    context 'when the manager is an organization' do
      before { set_manager_to_organization }

      it_behaves_like 'permission :new'
    end

    context 'when the manager is a user' do
      before { set_manager_to_user }

      it_behaves_like 'permission :new'
    end
  end

  describe 'GET #edit' do
    context 'when the manager is an organization' do
      before { set_manager_to_organization }

      it_behaves_like 'permission :edit'
    end

    context 'when the manager is a user' do
      before { set_manager_to_user }

      it_behaves_like 'permission :edit'
    end
  end

  describe 'DELETE #destroy' do
    context 'when the manager is an organization' do
      before do
        set_manager_to_organization
        @permission = Permission.create(
          manager:    @manager,
          manageable: @manageable,
          role:       @manageable.admin_role
        )
      end

      it_behaves_like 'permission :destroy'
    end

    context 'when the manager is a user' do
      before do
        set_manager_to_user

        # Create permission for user's organiation
        Permission.create(
          manager:    @manager.organization,
          manageable: @manageable,
          role:       @manageable.admin_role
        )

        @permission = Permission.create(
          manager:    @manager,
          manageable: @manageable,
          role:       @manager.organization.admin_role
        )
      end

      it_behaves_like 'permission :destroy'
    end
  end
end
