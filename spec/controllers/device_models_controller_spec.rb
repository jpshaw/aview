# frozen_string_literal: true

require 'rails_helper'

describe DeviceModelsController do
  describe 'GET index' do
    context 'as html' do
      it 'redirects' do
        get :index, format: :html
        expect(response).to have_http_status(:found)
      end
    end

    context 'as json' do
      context 'unauthenticated' do
        it 'returns 401' do
          get :index, format: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context 'as customer' do
        let(:user) { create(:non_root_user) }

        before do
          sign_in user
        end

        it 'returns 401' do
          get :index, format: :json
          expect(response).to have_http_status(:unauthorized)
        end
      end

      context 'as admin' do
        let(:user) { create(:root_user) }

        before do
          sign_in user
        end

        it 'returns 200' do
          get :index, format: :json
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end
end
