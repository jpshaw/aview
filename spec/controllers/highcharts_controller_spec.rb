# frozen_string_literal: true

require 'rails_helper'

describe HighchartsController do
  context 'An authenticated user' do
    before do
      @user = create(:user)
      sign_in @user
    end

    context 'performs a GET connectivity_status_chart' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :connectivity_status_chart }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        it "doesn't raise an error" do
          expect { get :connectivity_status_chart, format: 'json' }.not_to raise_error
        end
      end
    end

    context 'performs a GET deployed_status_chart' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :deployed_status_chart }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        it "doesn't raise an error" do
          expect { get :deployed_status_chart, format: 'json' }.not_to raise_error
        end
      end
    end

    context 'performs a GET network_type_chart' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :network_type_chart }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        it "doesn't raise an error" do
          expect { get :network_type_chart, format: 'json' }.not_to raise_error
        end
      end
    end

    context 'performs a GET signal_strength_chart' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :signal_strength_chart }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        it "doesn't raise an error" do
          expect { get :signal_strength_chart, format: 'json' }.not_to raise_error
        end
      end
    end

    context 'performs a GET cellular_utilization' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :cellular_utilization }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        before do
          @device_1 = build(:test_device)
          @device_2 = build(:test_device)
        end

        context 'with managed device_id' do
          it "doesn't raise an error" do
            allow(@user).to receive_message_chain(:devices, :find_by) { @device_1 }
            expect { get :cellular_utilization, format: 'json', id: @device_1.mac }.not_to raise_error
          end
        end

        context 'with unmanaged device_id' do
          it 'returns an error' do
            allow(@user).to receive_message_chain(:devices, :find_by) { nil }
            get :cellular_utilization, format: 'json', id: @device_2.mac
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end

    context 'performs a GET wan_utilization' do
      context 'as HTML' do
        it 'raises an UnknownFormat error' do
          expect { get :wan_utilization }.to raise_error ActionController::UnknownFormat
        end
      end

      context 'as JSON' do
        before do
          @device_1 = build(:test_device)
          @device_2 = build(:test_device)
        end

        context 'with managed device_id' do
          it "doesn't raise an error" do
            allow(@user).to receive_message_chain(:devices, :find_by) { @device_1 }
            expect { get :wan_utilization, format: 'json', mac: @device_1.mac }.not_to raise_error
          end
        end

        context 'with unmanaged device_id' do
          it 'returns an error' do
            allow(@user).to receive_message_chain(:devices, :find_by) { @device_1 }
            get :wan_utilization, format: 'json', mac: @device_2.mac
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end
  end
end
