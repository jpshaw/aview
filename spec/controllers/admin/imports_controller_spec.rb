# frozen_string_literal: true

require 'rails_helper'

describe Admin::ImportsController do
  before do
    @user = create(:user_with_org_permission)
    sign_in @user
    Settings.admin = Config::Options.new(enabled: true)
    Settings.imports = Config::Options.new(enabled: true)
    allow_any_instance_of(AdminControllerPolicy).to receive(:view?).and_return(true)
    request.env['HTTP_REFERER'] = 'previous_page'
  end

  after do
    Settings.reload!
  end

  describe 'GET index' do
    it_behaves_like 'an action that authorizes import access', :get, :index

    it 'has a 200 status code' do
      get :index
      expect(response.status).to eq(200)
    end

    it 'returns a data table as json with all of the imports in it' do
      create_list(:qa_import, 5)
      xhr :get, :index
      data_table = JSON.parse(response.body)
      expect(data_table['data'].size).to eq 5
      expect(data_table['recordsTotal']).to eq 5
    end
  end

  describe 'GET new' do
    it_behaves_like 'an action that authorizes import access', :get, :new

    it 'assigns a new Import to @import' do
      xhr :get, :new
      import = assigns(:import)
      expect(import).not_to be nil
      expect(import).to be_a(Import)
      expect(import.new_record?).to be true
    end

    it 'renders the new template' do
      xhr :get, :new
      expect(response).to render_template :new
    end

    it 'has a 200 status code' do
      xhr :get, :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do
    it_behaves_like 'an action that authorizes import access', :post, :create

    context 'with valid attributes' do
      let(:import_params) { attributes_for(:import) }

      context 'when the import source is from the settings' do
        before do
          settings = {
            data_files: [
              {
                name: 'simple import file',
                type: 'smx',
                action: 'append',
                logfile: '/tmp/smx.log',
                location: Rails.root.join('spec', 'support', 'fixtures', 'simple_import_file.tab').to_s
              }
            ]
          }

          Settings.data_files = []
          Settings.merge!(settings)

          Settings.admin = Config::Options.new(enabled: true)
          Settings.imports = Config::Options.new(enabled: true)
          @params = import_params.merge(the_source_name: 'settings')
        end

        it 'saves the new import to the database' do
          expect do
            post :create, import: @params
          end.to change(Import, :count).by(1)
          expect(Import.last.the_source_name.settings?).to be true
        end

        it 'redirects to the import index page' do
          post :create, import: @params
          expect(response).to redirect_to admin_path(anchor: 'imports')
        end
      end

      context 'when an import file is uploaded' do
        before do
          @file = fixture_file_upload('/files/10_random_entries.tab', 'text/plain')
          @params = import_params.merge(the_source_name: 'upload', import_file: @file)
        end

        it 'saves the new import to the database' do
          expect do
            post :create, import: @params
          end.to change(Import, :count).by(1)
          expect(Import.last.the_source_name.upload?).to be true
        end

        it 'redirects to the import index page' do
          post :create, import: @params
          expect(response).to redirect_to admin_path(anchor: 'imports')
        end
      end
    end

    context 'with invalid attributes' do
      let(:invalid_params) { { the_source_name: 'upload', import_file: nil } }

      it 'does not save the new import to the database' do
        expect do
          post :create, import: invalid_params
        end.not_to change(Import, :count)
      end

      it 'redirects to the import index page' do
        post :create, import: invalid_params
        expect(response).to redirect_to admin_path(anchor: 'imports')
      end
    end
  end

  describe 'GET show' do
    it_behaves_like 'an action that authorizes import access', :get, :show
    it_behaves_like 'an action that redirects if the import is not found', :get, :show

    it 'assigns the requested import to @import' do
      import = create(:qa_import)
      xhr :get, :show, id: import
      expect(assigns(:import)).to eq import
    end

    it 'has a 200 status code' do
      xhr :get, :show, id: create(:finished_qa_import)
      expect(response.status).to eq(200)
    end
  end

  describe 'DELETE destroy' do
    before do
      @import = create(:finished_qa_import)
    end

    it_behaves_like 'an action that authorizes import access', :delete, :destroy
    it_behaves_like 'an action that redirects if the import is not found', :delete, :destroy

    it 'deletes the import' do
      expect do
        xhr :delete, :destroy, id: @import
      end.to change(Import, :count).by(-1)
    end

    it 'renders the desroy template' do
      xhr :delete, :destroy, id: @import
      expect(response).to render_template :destroy
    end
  end

  describe 'DELETE destroy_all' do
    it_behaves_like 'an action that authorizes import access', :delete, :destroy_all

    it 'deletes all finished imports' do
      create(:qa_import)
      create_list(:finished_qa_import, 5)
      expect(Import.count).to eq 6
      expect do
        xhr :delete, :destroy_all
      end.to change(Import, :count).by(-5)
      expect(Import.count).to eq 1
    end

    it 'renders the destroy_all template' do
      xhr :delete, :destroy_all
      expect(response).to render_template :destroy_all
    end
  end
end
