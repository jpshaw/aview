# frozen_string_literal: true

require 'rails_helper'

describe Admin::DnsController do
  before do
    # Create user with admin access
    @user = create(:user_with_org_permission)
    sign_in @user

    # Enable admin panel
    Settings.admin = Config::Options.new(enabled: true)
    allow_any_instance_of(AdminControllerPolicy).to receive(:view?).and_return(true)

    # DNS objects
    @domains     = create_list(:dns_domain, 1)
    @soas        = create_list(:dns_soa, 1, domain: @domains.first)
    @nameservers = create_list(:dns_nameserver, 1, domain: @domains.first)
  end

  after do
    Settings.reload!
  end

  let(:dns_entry_factory) { :dns_domain                             }
  let(:entry_type)        { 'domain'                                }
  let(:entry_class)       { PowerDnsRecordManager::Domain           }
  let(:entry_params)      { { entry_type: entry_type, format: :js } }

  describe 'GET index' do
    it 'has a 200 status code' do
      xhr :get, :index, type: :domain
      expect(response.status).to eq(200)
    end

    it 'returns a data table json object for domains' do
      xhr :get, :index, type: :domain
      data_table = JSON.parse(response.body)
      expect(data_table['data'].first).to include @domains.first.name
      expect(data_table['recordsTotal']).to eq 1
    end

    it 'returns a data table json object for soas' do
      xhr :get, :index, type: :soa
      data_table = JSON.parse(response.body)
      expect(data_table['data'].first).to include @soas.first.name
      expect(data_table['recordsTotal']).to eq 1
    end

    it 'returns a data table json object for nameservers' do
      xhr :get, :index, type: :nameserver
      data_table = JSON.parse(response.body)
      expect(data_table['data'].first).to include @nameservers.first.name
      expect(data_table['recordsTotal']).to eq 1
    end
  end

  describe 'GET new' do
    it 'assigns a new DNS Entry to @entry' do
      xhr :get, :new, entry_params
      expect(assigns(:entry)).to be_a(entry_class)
    end

    it 'renders the new template' do
      xhr :get, :new, entry_params
      expect(response).to render_template 'admin/dns/new'
    end

    it 'has a 200 status code' do
      xhr :get, :new, entry_params
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do
    context 'with valid attributes' do
      before do
        entry_params[:dns_entry] = attributes_for(dns_entry_factory)
      end

      it 'creates a new dns entry' do
        expect do
          post :create, entry_params
        end.to change(entry_class, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      before do
        entry_params[:dns_entry] = { name: '' }
      end

      it 'does not create a new dns entry' do
        expect do
          post :create, entry_params
        end.not_to change(entry_class, :count)
      end
    end
  end

  describe 'GET edit' do
    let(:entry) { create(dns_entry_factory) }

    before do
      entry_params[:id] = entry.id
    end

    it 'assigns the requested dns entry to @entry' do
      xhr :get, :edit, entry_params
      expect(assigns(:entry)).to eq entry
    end

    it 'renders the edit template' do
      xhr :get, :edit, entry_params
      expect(response).to render_template 'admin/dns/edit'
    end
  end

  describe 'PATCH update' do
    let(:entry) { create(dns_entry_factory) }

    before do
      entry_params[:id] = entry.id
    end

    context 'with valid attributes' do
      let(:name) { 'dns.test.accns.net' }

      before do
        entry_params[:dns_entry] = attributes_for(dns_entry_factory, name: name)
      end

      it 'located the requested @entry' do
        patch :update, entry_params
        expect(assigns(:entry)).to eq entry
      end

      it 'changes the entry attributes' do
        expect do
          patch :update, entry_params
        end.to change { entry.reload.name }.to(name)
      end
    end

    context 'with invalid attributes' do
      before do
        entry_params[:dns_entry] = attributes_for(dns_entry_factory, name: '')
      end

      it 'located the requested @entry' do
        patch :update, entry_params
        expect(assigns(:entry)).to eq entry
      end

      it 'does not change the entry attributes' do
        expect do
          patch :update, entry_params
        end.not_to change { entry.reload.name }
      end
    end
  end

  describe 'DELETE destroy' do
    let(:entry) { create(dns_entry_factory) }

    before do
      entry_params[:id] = entry.id
    end

    it 'deletes the dns entry' do
      expect do
        delete :destroy, entry_params
      end.to change(entry_class, :count).by(-1)
    end
  end
end
