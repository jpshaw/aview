# frozen_string_literal: true

require 'rails_helper'

describe DataPlansController do
  before do
    create_user
    sign_in @user

    @organization = @user.organization
    @params = { organization_id: @organization.slug }
  end

  describe 'GET #data_plans' do
    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
      end

      context 'when the format is json' do
        it 'renders the data plan data table' do
          expect(DataPlansDataTable).to receive(:new)
          get :index, @params.merge(format: :json)
        end
      end

      context 'when the format is js' do
        it 'renders the index for select 2 reload template' do
          xhr :get, :index, @params
          expect(response).to render_template(:index_for_select)
        end
      end
    end
  end

  describe 'GET #new' do
    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
      end

      it 'assigns a new data plan to @data_plan' do
        xhr :get, :new, @params
        expect(assigns(:data_plan)).to be_new_record
        expect(assigns(:data_plan)).to be_a DataPlan
      end

      it 'renders the #new template' do
        xhr :get, :new, @params
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'POST #create' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :post, :create

    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
        @params[:data_plan] = { name: 'Test Data Plan', cycle_type: :monthly, cycle_start_day: 1, data_limit: 10, individual_data_limit: 1 }
      end

      it 'creates a new data plan' do
        expect do
          xhr :post, :create, @params
        end.to change(DataPlan, :count).by(1)
      end

      it 'renders the #create template' do
        xhr :post, :create, @params
        expect(response).to render_template(:create)
      end

      it 'converts the user entered data limits from megabytes to bytes' do
        xhr :post, :create, @params
        data_plan = DataPlan.find_by(name: @params[:data_plan][:name])
        expect(data_plan.data_limit).to eq(@params[:data_plan][:data_limit] * 1_000_000)
        expect(data_plan.individual_data_limit).to eq(@params[:data_plan][:individual_data_limit] * 1_000_000)
      end
    end
  end

  describe 'GET #edit' do
    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
        @data_plan = build(:data_plan, data_limit: 10_000_000, individual_data_limit: 1_000_000)
        expect(DataPlan).to receive(:find).and_return(@data_plan)
        @params[:id] = 1
      end

      it 'renders the #edit template' do
        xhr :get, :edit, @params
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'PATCH #update' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :patch, :update

    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
        @data_plan = build(:data_plan)
        expect(DataPlan).to receive(:find).and_return(@data_plan)
        @params[:data_plan] = { name: 'New Test Data Plan', cycle_type: 'monthly', cycle_start_day: '1',
                                data_limit: 10, individual_data_limit: 1 }.with_indifferent_access
        @params[:id] = 1
      end

      it 'updates the given data plan' do
        expect(@data_plan).to receive(:update_attributes)
        xhr :patch, :update, @params
      end

      it 'renders the #update template' do
        xhr :patch, :update, @params
        expect(response).to render_template(:update)
      end

      it 'converts the user entered data limits from megabytes to bytes' do
        xhr :patch, :update, @params
        expect(@data_plan.data_limit).to eq(@params[:data_plan][:data_limit] * 1_000_000)
        expect(@data_plan.individual_data_limit).to eq(@params[:data_plan][:individual_data_limit] * 1_000_000)
      end
    end
  end

  describe 'DELETE #destroy' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :delete, :destroy

    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
        @data_plan = create(:data_plan)
        @params[:id] = @data_plan.id
      end

      it 'deletes the data plan' do
        expect do
          xhr :delete, :destroy, @params
        end.to change(DataPlan, :count).by(-1)
      end

      it 'renders the #destroy template' do
        xhr :delete, :destroy, @params
        expect(response).to render_template :destroy
      end
    end
  end

  describe 'POST #move_devices_to' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :post, :move_devices_to

    context 'when the user is authorized to modify the organization' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
      end

      it 'calls the move devices to data plan interactor' do
        expect(MoveDevicesToDataPlan).to receive(:call)
        xhr :post, :move_devices_to, @params
      end

      it 'renders the #move_devices_to template' do
        xhr :post, :move_devices_to, @params
        expect(response).to render_template :move_devices_to
      end
    end
  end

  describe 'GET #show' do
    context 'when the request is html' do
      before do
        expect(controller).to receive(:authorize_organization!).and_return(true)
        data_plan = build(:data_plan, id: 1)
        expect(DataPlan).to receive(:find).and_return(data_plan)
      end

      it 'renders the device index template' do
        get :show, id: 1, organization_id: @organization.id
        expect(response).to render_template 'index'
      end
    end
  end
end
