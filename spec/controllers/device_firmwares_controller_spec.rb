# frozen_string_literal: true

require 'rails_helper'

describe DeviceFirmwaresController do
  describe 'GET index' do
    context 'when nested device_model' do
      let(:device_model) { create(:device_model) }

      context 'as html' do
        it 'redirects' do
          get :index, format: :html, device_model_id: device_model.id
          expect(response).to have_http_status(:found)
        end
      end

      context 'as js' do
        context 'unauthenticated' do
          it 'returns 401' do
            xhr :get, :index, format: :js, device_model_id: device_model.id
            expect(response).to have_http_status(:unauthorized)
          end
        end

        context 'as customer' do
          let(:user) { create(:non_root_user) }

          before do
            sign_in user
          end

          it 'returns 401' do
            xhr :get, :index, format: :js, device_model_id: device_model.id
            expect(response).to have_http_status(:unauthorized)
          end
        end

        context 'as admin' do
          let(:user) { create(:root_user) }

          before do
            sign_in user
          end

          it 'returns 200' do
            xhr :get, :index, format: :js, device_model_id: device_model.id
            expect(response).to have_http_status(:ok)
          end
        end
      end
    end
  end
end
