# frozen_string_literal: true

require 'rails_helper'

describe PowerDnsBackendController do
  # render_views not needed, did view spec instead.

  before do
    if PowerDnsRecordManager::Domain.count.zero?
      PowerDnsRecordManager::Domain.create(name: Settings.powerdns.domain.name, type: 'NATIVE')
    end

    PowerDnsRecordManager::Record.where(name: "localhost.#{Settings.powerdns.domain.name}",
                                        type: 'A',
                                        domain_id: 1,
                                        ttl: 120).first_or_create.update_attribute(:content, '127.0.0.1')

    PowerDnsRecordManager::Record.where(name: Settings.powerdns.domain.name.to_s,
                                        type: 'SOA',
                                        domain_id: 1,
                                        ttl: Settings.powerdns.domain.ttl).first_or_create.update_attribute(:content, Settings.powerdns.domain.hostmaster.to_s)

    Settings.powerdns.domain.nameservers.each do |nameserver|
      PowerDnsRecordManager::Record.where(name: nameserver.name,
                                          type: 'NS',
                                          ttl: nameserver.ttl || 86_400).first_or_create
    end
  end

  describe "POST 'lookup'" do
    it 'returns success' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qtype: 'SOA', qname: Settings.powerdns.domain.name.to_s)
      expect(response).to be_success
      expect(assigns(:records)).not_to be nil
    end

    it 'fails on missing parameters' do
      post 'lookup'
      expect(response.status).to eq 400
    end

    it 'fails on missing parameters' do
      post 'lookup', parameters: ActiveSupport::JSON.encode({})
      expect(response.status).to eq 400
    end

    it 'fails on missing qname parameter' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qtype: 'SOA')
      expect(response.status).to eq 400
    end

    it 'fails on missing qtype parameter' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qname: Settings.powerdns.domain.name.to_s)
      expect(response.status).to eq 400
    end

    it 'returns a valid SOA record' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qtype: 'SOA', qname: Settings.powerdns.domain.name.to_s)
      expect(assigns(:records).length).to eq 1
      expect(assigns(:records)[0][:type]).to eq 'SOA'
      expect(assigns(:records)[0][:name]).to eq Settings.powerdns.domain.name.to_s
      expect(assigns(:records)[0][:content]).to eq Settings.powerdns.domain.hostmaster.to_s
      expect(assigns(:records)[0][:ttl]).to eq 30
    end

    it 'returns an A record' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qtype: 'ANY', qname: "localhost.#{Settings.powerdns.domain.name}")
      expect(assigns(:records).length).to be > 0
      expect(assigns(:records)[0][:type]).to eq 'A'
      expect(assigns(:records)[0][:name]).to eq "localhost.#{Settings.powerdns.domain.name}"
      expect(assigns(:records)[0][:content]).to eq '127.0.0.1'
      expect(assigns(:records)[0][:ttl]).to eq 120
    end

    it 'returns nothing' do
      post 'lookup', parameters: ActiveSupport::JSON.encode(qtype: 'ANY', qname: "localhorse.#{Settings.powerdns.domain.name}")
      expect(assigns(:records).length).to eq 0
    end
  end
end
