# frozen_string_literal: true

require 'rails_helper'

describe LegacyConfigurationsController do
  before do
    @organization = create :organization
    @config       = create(
      :netbridge_configuration,
      { organization: @organization }.merge(random_attributes)
    )

    @config_attrs = attributes_for(
      :netbridge_configuration,
      { organization_id: @organization.id }.merge(random_attributes)
    )

    user = create(:user, organization: @organization)
    role = @organization.admin_role

    Permission.create(manager: user, manageable: @organization, role: role)

    role.abilities << Ability.create(name: 'Modify Configurations')

    allow_any_instance_of(described_class).to receive(:cellular_firmware_versions_for).and_return([' '])

    sign_in user
  end

  describe 'GET #new' do
    it 'creates a new confiugration object' do
      get :new
      expect(assigns(:configuration)).to be_a(NetbridgeConfiguration)
      expect(assigns(:configuration)).to be_new_record
    end
  end

  describe 'GET #index' do
    it 'renders the index page' do
      get :index
      expect(response).to render_template(:index)
      expect(response.status).to eq(200)
    end
  end

  describe 'GET #show' do
    it 'assigns the requested configuration to @configuration' do
      get :show, id: @config
      expect(assigns(:configuration)).to eq(@config)
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      it 'creates a new configuration' do
        expect do
          post :create, configuration: @config_attrs
        end.to change(NetbridgeConfiguration, :count).by(1)
      end

      it 'redirects to the created configuration' do
        post :create, configuration: @config_attrs
        saved_config = NetbridgeConfiguration.last
        expect(response).to redirect_to(legacy_configuration_url(saved_config))
      end
    end

    context 'with invalid parameters' do
      before do
        @config_attrs = attributes_for(:invalid_netbridge_configuration,
                                        organization_id: @organization.id)
      end

      it 'does not create a new configuration' do
        expect do
          post :create, configuration: @config_attrs
        end.to change(NetbridgeConfiguration, :count).by(0)
      end

      it 'renders new' do
        post :create, configuration: @config_attrs
        expect(response).to render_template :new
      end
    end
  end

  describe 'PATCH #update' do
    context 'with valid parameters' do
      before do
        @attributes = @config.attributes
      end

      it 'found the requested config' do
        new_value = Random.rand(31_337)
        patch :update, id: @config, configuration: { link_timeout: new_value }
        expect(assigns(:configuration)).to eq(@config)
      end

      it 'changes attributes of @config' do
        new_value = Random.rand(31_337)
        patch :update, id: @config, configuration: @attributes.merge(link_timeout: new_value)
        @config.reload
        expect(@config.link_timeout).to eq(new_value)
      end

      it 'redirects to the updated configuration' do
        new_value = Random.rand(31_337)
        patch :update, id: @config, configuration: @attributes.merge(link_timeout: new_value)
        expect(response).to redirect_to(legacy_configuration_url(@config))
      end
    end

    context 'with invalid parameters' do
      before do
        @config_attrs = attributes_for(:invalid_netbridge_configuration,
                                        organization_id: @organization.id)
      end

      it 'renders edit' do
        patch :update, id: @config, configuration: @config_attrs
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'without dependants' do
      it 'destroys the requested record' do
        expect do
          delete :destroy, id: @config
        end.to change(NetbridgeConfiguration, :count).by(-1)
        expect(response).to redirect_to(legacy_configurations_path)
      end
    end

    context 'with dependants' do
      before do
        @dependant_config = create(:netbridge_configuration,
                                              organization_id: @organization.id,
                                              parent_id: @config.id)
      end

      it 'does not destoy parent config' do
        expect do
          delete :destroy, id: @config
        end.to change(NetbridgeConfiguration, :count).by(0)
        expect(response).to redirect_to(legacy_configuration_path(@config))
      end
    end
  end

  describe 'GET #device_config' do
    it 'attempts to authenticate the device' do
      expect(controller).to receive(:authenticate_device!)
      get :device_config, id: 'test'
    end

    it 'returns forbidden when the device id is blank' do
      get :device_config, id: ''
      expect(response).to have_http_status(:forbidden)
    end

    it 'returns forbidden when the device id is invalid' do
      get :device_config, id: '00c0ca/mac/123456'
      expect(response).to have_http_status(:forbidden)
    end

    context 'for Netbridge SX devices' do
      let(:device)  { create(:netbridge_device, :configurable, :sx) }
      let(:mac)     { device.mac                                    }
      let(:params)  { { id: mac }                                   }
      let(:config)  { device.configuration.to_keyval                }

      before do
        request.headers['HTTP_X_FORWARDED_PROTO'] = ''
        request.headers['HTTP_X_SSL_CERT_SDN']    = ''
      end

      it 'renders the configuration' do
        get :device_config, params
        expect(response.body).to eq config
      end

      context 'when the configuration does not exist' do
        let(:mac) { generate(:netbridge_sx_mac) }

        it 'returns not_found' do
          get :device_config, params
          expect(response).to have_http_status(:not_found)
        end
      end
    end

    context 'for Netbridge FX devices' do
      let(:device)  { create(:netbridge_device, :configurable, :fx) }
      let(:mac)     { device.mac                                    }
      let(:params)  { { id: mac }                                   }
      let(:config)  { device.configuration.to_keyval                }

      context 'when valid headers are given' do
        before do
          request.headers['HTTP_X_FORWARDED_PROTO'] = 'https'
          request.headers['HTTP_X_SSL_CERT_SDN']    = "CN=#{mac}"
        end

        it 'renders the configuration' do
          get :device_config, params
          expect(response).to have_http_status(:ok)
          expect(response.body).to eq config
        end

        context 'when the configuration does not exist' do
          let(:mac) { generate(:netbridge_fx_mac) }

          it 'returns not_found' do
            get :device_config, params
            expect(response).to have_http_status(:not_found)
          end
        end
      end

      context 'when invalid headers are given' do
        before do
          request.headers['HTTP_X_FORWARDED_PROTO'] = ''
          request.headers['HTTP_X_SSL_CERT_SDN']    = ''
        end

        it 'returns forbidden' do
          get :device_config, params
          expect(response).to have_http_status(:forbidden)
        end
      end
    end
  end

  describe 'GET #inherited_values' do
    context 'with no parent_id' do
      it 'renders nothing' do
        get :inherited_values
        expect(response.body.blank?).to be true
      end
    end

    context 'given a config' do
      it 'assigns inherited values' do
        get :inherited_values, parent_id: @config.id
        expect(assigns(:inherited_values)).to eq(@config.config_values_with_inherited_for_form)
      end

      it 'renders json' do
        get :inherited_values, format: :json, parent_id: @config.id
        expect(JSON.parse(response.body)).to be
      end
    end
  end
end
