# frozen_string_literal: true

require 'rails_helper'

describe ReportsController do
  before do
    create_user
    sign_in @user
  end

  describe 'GET #reports' do
    it 'renders the index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe 'SHOW #reports' do
    context 'when the report does not belong to the user' do
      before do
        @report = create(:device_report)
      end

      it 'redirects to the index page' do
        get :show, id: @report.id
        expect(response).to redirect_to reports_path
      end
    end

    context 'when the report belongs to the user' do
      before do
        @report = create(:device_report, user: @user)
      end

      context 'when requesting html' do
        it 'renders the show page' do
          get :show, id: @report.id
          expect(response).to render_template(:show)
        end
      end
    end
  end

  describe 'POST #export' do
    context 'when the report does not belong to the user' do
      before do
        @report = create(:device_report)
      end

      it 'redirects to report index' do
        post :export, id: @report.id, format: :csv
        expect(assigns(:report)).to be_nil
        expect(flash[:error]).to eq I18n.t('reports.export.error')
        expect(response).to redirect_to(reports_path)
      end

      it 'redirects to report index' do
        post :export, id: @report.id, format: :xlsx
        expect(assigns(:report)).to be_nil
        expect(flash[:error]).to eq I18n.t('reports.export.error')
        expect(response).to redirect_to(reports_path)
      end
    end

    context 'when the report belongs to the user' do
      before do
        @report = create(:device_report, user: @user)
      end

      it 'responds to csv format' do
        allow_any_instance_of(ReportDataTable).to receive(:to_csv).and_return('')
        post :export, id: @report.id, format: :csv
        expect(assigns(:report)).to eq @report
        expect(response).to have_http_status(:ok)
      end

      it 'responds to xlsx format' do
        allow_any_instance_of(ReportDataTable).to receive(:to_xlsx).and_return('')
        post :export, id: @report.id, format: :xlsx
        expect(assigns(:report)).to eq @report
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
