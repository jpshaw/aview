# frozen_string_literal: true

require 'rails_helper'

describe DataTablesController do
  before do
    create_user
    sign_in @user
  end

  describe 'POST #events' do
    it 'responds to csv format' do
      post :events, format: :csv
      expect(response).to have_http_status(:ok)
    end

    it 'responds to json format' do
      post :events, format: :json
      expect(response).to have_http_status(:ok)
    end

    it 'responds to xlsx format' do
      post :events, format: :xlsx
      expect(response).to have_http_status(:ok)
    end
  end
end
