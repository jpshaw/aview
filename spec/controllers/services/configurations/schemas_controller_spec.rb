# frozen_string_literal: true

require 'rails_helper'

describe Services::Configurations::SchemasController do
  let(:user) { create(:user) }

  before do
    sign_in user
  end

  describe 'GET #ui' do
    before do
      finder = double('finder')
      allow(ConfigurationSchemaFinder).to receive(:new) { finder }
      allow(finder).to receive(:execute) { schema }
    end

    context 'when the requested schema exists' do
      let(:schema) { { schema: 'test' } }

      it 'returns the schema' do
        get :ui, model: 'model'
        expect(response.body).to eq schema.to_json
      end
    end

    context 'when the requested schema does not exist' do
      let(:schema) { nil }

      it 'returns not found' do
        get :ui, model: 'model'
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
