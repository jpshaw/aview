# frozen_string_literal: true

require 'rails_helper'

describe Api::DeviceConfigurations::V2Controller do
  before do
    Timecop.freeze(Time.now.utc)
    allow_any_instance_of(Api::ApiController).to receive(:authorize_device!).and_return(true)
    allow_any_instance_of(described_class).to receive(:validate_sdn!).and_return(true)
  end

  after do
    Timecop.return
  end

  describe 'GET #show' do
    context 'when the device exists in the application' do
      let(:organization) { create(:organization) }

      context 'when the configuration exists for the device' do
        let(:configuration) { create(:device_configuration_with_data, organization: organization) }
        let(:device)        { create(:remote_manager_device, organization: organization, configuration: configuration) }

        it 'updates the timestamp to indicate the device performed a GET request' do
          expect(device.configuration_checked_at).to be_nil
          get :show, id: device.mac
          device.reload
          expect(device.configuration_checked_at).to be_present
        end

        context 'when the configuration has been updated since the last configuration get request' do
          it 'returns a 200 OK status code' do
            request.env['HTTP_IF_MODIFIED_SINCE'] = (Time.now.utc - 1.month).rfc2822
            get :show, id: device.mac
            expect(response.status).to eq(200)
            expect(response.headers['Last-Modified']).to eq(device.configuration.updated_at.rfc2822)
          end
        end

        context 'when the configuration has not been updated since the last configuration get request' do
          it 'returns a 304 NOT_MODIFIED status code' do
            request.env['HTTP_IF_MODIFIED_SINCE'] = (Time.now.utc + 1.month).rfc2822
            get :show, id: device.mac
            expect(response.status).to eq(304)
          end
        end

        context 'when a different profile is applied to the device' do
          before do
            # As part of the UpdateDevice class called by the devices controller,
            # we update the configuration_associated_at attribute of the device
            # any time a new configuration is associated to a device (i.e. the
            # device.configuration_id changes).
            device.configuration_associated_at = Time.now.utc + 1.day
            # stagger this timestamp so it's not confused with configuration.updated_at
            device.save
          end

          context 'when a new config has been associated since the last configuration get request' do
            it 'returns a 200 OK status code' do
              request.env['HTTP_IF_MODIFIED_SINCE'] = (Time.now.utc - 1.month).rfc2822
              get :show, id: device.mac
              expect(response.status).to eq(200)
              expect(response.headers['Last-Modified']).to eq(device.configuration_associated_at.rfc2822)
            end
          end

          context 'when a new config has not been associated since the last configuration get request' do
            it 'returns a 304 NOT_MODIFIED status code' do
              request.env['HTTP_IF_MODIFIED_SINCE'] = (Time.now.utc + 1.month).rfc2822
              get :show, id: device.mac
              expect(response.status).to eq(304)
            end
          end
        end
      end

      context 'when the configuration does not exist for the device' do
        let(:device) { create(:remote_manager_device, organization: organization) }

        it 'updates the timestamp to indicate the device performed a GET request' do
          get :show, id: device.mac
          device.reload
          expect(device.configuration_checked_at).to be_present
        end

        it 'returns a 404 NOT_FOUND status code' do
          get :show, id: device.mac
          expect(response.status).to eq(404)
        end
      end
    end

    context 'when the device does not exist in the application' do
      before do
        @bogus_mac = '123456789101'
      end

      it 'returns a 404 NOT_FOUND status code' do
        get :show, id: @bogus_mac
        expect(response.status).to eq(404)
      end
    end
  end
end
