# frozen_string_literal: true

require 'rails_helper'

describe Api::V4::DevicesController do
  render_views

  let(:json)         { JSON.parse(response.body) }
  let(:organization) { create(:organization) }
  let(:user)         { create(:api_user, organization: organization) }

  before do
    Settings.user_api = Config::Options.new(enabled?: true)
    create(:permission,
           manager:    user,
           manageable: organization,
           role:       organization.admin_role)
  end

  after do
    Settings.reload!
  end

  describe 'GET #index' do
    context 'when given a valid user token' do
      let(:token) { user.authentication_token }
      let!(:user_devices) { create_list(:test_device, 3, organization: organization, last_heartbeat_at: '2016-12-12 12:12:12') }

      it 'has a 200 OK status code' do
        get :index, auth_token: token, format: :json
        expect(response.status).to eq(200)
      end

      it 'only returns a few fields of each device by default' do
        get :index, auth_token: token, format: :json
        json['results']['devices'].each do |device|
          expect(device).to include('mac')
          expect(device).to include('deployed')
          expect(device).not_to include('category')
        end
      end

      context 'when given additional include fields' do
        it 'returns the additional fields' do
          get :index, auth_token: token, include: 'site,category', format: :json
          json['results']['devices'].each do |device|
            expect(device).to include('mac')
            expect(device).to include('deployed')
            expect(device).to include('site')
            expect(device).to include('category')
            expect(device).not_to include('model')
          end
        end
      end

      context 'when given no organization' do
        let!(:random_device) { create(:test_device) }

        it 'renders all devices the user has access to' do
          get :index, auth_token: token, format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to eq user_devices.map(&:mac)
        end
      end

      context 'when given an organization name' do
        let(:sub_organization) { create(:sub_organization) }
        let!(:sub_device) { create(:test_device, organization: sub_organization) }
        let(:organization_2) { create(:organization) }
        let!(:random_device) { create(:test_device, organization: organization_2) }

        before do
          create(:permission,
                 manager:    organization,
                 manageable: organization_2,
                 role:       organization.admin_role)
          create(:permission,
                 manager:    user,
                 manageable: organization_2,
                 role:       organization.admin_role)
        end

        context 'when no hierarchy is given' do
          it 'returns devices within the organization only' do
            get :index, auth_token: token, organization: organization.name, format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).to eq user_devices.map(&:mac)
          end
        end

        context 'when hierarchy is set to true' do
          it 'returns devices within the organization and sub organizations' do
            get :index, auth_token: token, organization: organization.name, include_hierarchy: 'true', format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            user_devices << sub_device
            expect(result_macs).to eq user_devices.map(&:mac)
          end
        end

        context 'when hierarchy set to false' do
          it 'return devices within the organization only' do
            get :index, auth_token: token, organization: organization.name, include_hierarchy: 'false', format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).to eq user_devices.map(&:mac)
          end
        end

        context 'when the organization does not exist' do
          it 'returns an empty array' do
            get :index, auth_token: token, organization: 'test', format: :json
            expect(json['results']['devices']).to eq([])
          end
        end

        context 'when the organization exists but the user does not manage it' do
          let(:organization_3) { create(:organization) }
          let!(:device_3) { create(:test_device, organization: organization_3) }

          it 'returns an empty array' do
            get :index, auth_token: token, organization: organization_3.name, format: :json
            expect(json['results']['devices']).to eq([])
          end
        end
      end

      context 'when given a limit smaller than the possible return set' do
        it 'returns the first limit number of devices' do
          get :index, auth_token: token, page_limit: '2', format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to include(user_devices.first.mac)
          expect(result_macs).to include(user_devices.second.mac)
          expect(result_macs).not_to include(user_devices.last.mac)
        end

        it 'returns the next page number' do
          get :index, auth_token: token, page_limit: '2', format: :json
          expect(json['results']['metadata']['next_page']).to eq(2)
        end

        it 'returns the total number of pages' do
          get :index, auth_token: token, page_limit: '2', format: :json
          expect(json['results']['metadata']['pages']).to eq(2)
        end

        context 'when given the second page number' do
          it 'returns the next set of devices' do
            get :index, auth_token: token, page_limit: '2', page: '2', format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).not_to include(user_devices.first.mac)
            expect(result_macs).not_to include(user_devices.second.mac)
            expect(result_macs).to include(user_devices.last.mac)
          end
        end
      end

      context 'when given a start date only' do
        let!(:other_device) { create(:test_device, organization: organization, last_heartbeat_at: '2016-12-10 22:10:06') }

        it 'returns devices that have received a heartbeat from the given time to now' do
          get :index, auth_token: token, start_date: '2016-12-11', format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to eq user_devices.map(&:mac)
        end
      end

      context 'when given an end date only' do
        let!(:other_device) { create(:test_device, organization: organization, last_heartbeat_at: 1.second.ago) }

        it 'returns devices that have received a heartbeat from the beginning of today until the given date' do
          get :index, auth_token: token, end_date: DateTime.now.to_s, format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to eq [other_device.mac]
        end
      end

      context 'when given a start date and end date' do
        context 'when the start date precedes the end date' do
          let!(:other_device) { create(:test_device, organization: organization, last_heartbeat_at: '2016-12-10 22:10:06') }
          let!(:other_device_2) { create(:test_device, organization: organization, last_heartbeat_at: '2016-12-8 22:10:06') }

          it 'returns devices that have received a heartbeat between the given dates' do
            get :index, auth_token: token, start_date: '2016-12-09', end_date: '2016-12-10', format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).to eq [other_device.mac]
          end
        end

        context 'when the end date precedes the start date' do
          it 'has a 400 Bad Request status code' do
            get :index, auth_token: token, start_date: '2016-12-10', end_date: '2016-12-09', format: :json
            expect(response.status).to eq(400)
          end
        end
      end

      context 'when given an interval' do
        let!(:other_device) { create(:test_device, organization: organization, last_heartbeat_at: '2016-11-11 12:12:12') }

        before do
          Timecop.freeze(Time.zone.parse('2016-12-12T12:13:12Z'))
        end

        after do
          Timecop.return
        end

        it 'returns devices that have received a heartbeat from the interval to now' do
          get :index, auth_token: token, interval: '65', format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to eq user_devices.map(&:mac)
        end
      end

      context 'when given an interval and a start and end date' do
        let!(:other_device) { create(:test_device, organization: organization, last_heartbeat_at: '2016-11-11 12:12:12') }
        let!(:other_device_2) { create(:test_device, organization: organization, last_heartbeat_at: '2016-10-10 12:11:00') }

        before do
          Timecop.freeze(Time.zone.parse('2016-12-12T12:13:12Z'))
        end

        after do
          Timecop.return
        end

        it 'ignores the start and end date and returns devices that have received a heartbeat from the interval to now' do
          get :index, auth_token: token, interval: '65', start_date: '2012-10-01',
                      end_date: '2016-12-12', format: :json
          result_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(result_macs).to eq user_devices.map(&:mac)
        end
      end

      context 'when a device has not received a heartbeat' do
        let!(:other_device) { create(:test_device, organization: organization) }

        context 'when a start date is given' do
          it 'does not return the device without a heartbeat' do
            get :index, auth_token: token, start_date: '2016-12-11', format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).not_to include other_device.mac
          end
        end

        context 'when no start date, end date, or interval is given' do
          it 'returns the device without a heartbeat' do
            get :index, auth_token: token, format: :json
            result_macs = json['results']['devices'].map { |device| device['mac'] }
            expect(result_macs).to include other_device.mac
          end
        end
      end
    end

    context 'when given an invalid user token' do
      let(:token) { 'testtoken' }

      it 'has a 401 Unauthorized status code' do
        get :index, auth_token: token, format: :json
        expect(response.status).to eq(401)
      end
    end
  end

  describe 'GET #usage' do
    context 'when given a valid user token' do
      let(:token) { user.authentication_token }

      it 'has a 200 OK status code' do
        get :usage, auth_token: token, format: :json
        expect(response.status).to eq(200)
      end

      context 'when given an invalid device id' do
        it 'has a 404 status code' do
          get :usage, device_id: 'ABC123', auth_token: token, format: :json
          expect(response.status).to eq(404)
        end
      end

      context 'when given a valid device id' do
        let(:device) { create(:test_device, organization: organization) }

        before do
          Timecop.freeze(Time.local(2016, 12, 12, 12, 12, 12))
        end

        after do
          Timecop.return
        end

        context 'when given no interface or date range' do
          let(:expected_options) do
            { mac: device.mac,
              start_date: DateTime.now.in_time_zone(user.timezone.name).beginning_of_day.utc.iso8601,
              end_date:  DateTime.now.in_time_zone(user.timezone.name).utc.iso8601 }
          end
          let(:mock_db_results) do
            [{ 'timestamp' => '2016-12-05T00:00:00.000Z',
               'result' => { 'received' => 50, 'transmitted' => 70 } }]
          end

          before do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options)
              .and_return(mock_db_results)
          end

          it 'returns the usage of all interfaces for today' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token, format: :json
            expect(json['results']).to eq(['mac' => device.mac,
                                           'usage' => [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'download' => 50, 'upload' => 70 }]])
          end
        end

        context 'when given a valid interface' do
          let(:interface) { 'PPP0' }
          let(:expected_options) do
            { mac: device.mac,
              start_date: DateTime.now.in_time_zone(user.timezone.name).beginning_of_day.utc.iso8601,
              end_date:  DateTime.now.in_time_zone(user.timezone.name).utc.iso8601,
              interface: interface }
          end
          let(:mock_db_results) do
            [{ 'timestamp' => '2016-12-05T00:00:00.000Z',
               'result' => { 'received' => 50, 'transmitted' => 70 } }]
          end

          before do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options)
              .and_return(mock_db_results)
          end

          it 'returns the usage for the given interface for today' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token,
                        interface: interface, format: :json
            expect(json['results']).to eq(['mac' => device.mac,
                                           'usage' => [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'download' => 50, 'upload' => 70 }]])
          end
        end

        context 'when given a valid date range' do
          let(:start_date) { '2016-12-05' }
          let(:end_date) { '2016-12-06' }
          let(:expected_options) do
            { mac: device.mac,
              start_date: subject.send(:formatted_date, start_date),
              end_date:  subject.send(:formatted_date, end_date) }
          end
          let(:mock_db_results) do
            [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'result' => { 'received' => 15, 'transmitted' => 20 } },
             { 'timestamp' => '2016-12-06T00:00:00.000Z', 'result' => { 'received' => 50, 'transmitted' => 70 } }]
          end

          before do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options)
              .and_return(mock_db_results)
          end

          it 'returns the usage per day for all interfaces in the given date range' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token,
                        start_date: start_date, end_date: end_date, format: :json
            expect(json['results']).to eq(['mac' => device.mac,
                                           'usage' => [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'download' => 15, 'upload' => 20 },
                                                       { 'timestamp' => '2016-12-06T00:00:00.000Z', 'download' => 50, 'upload' => 70 }]])
          end
        end

        context 'when given a valid interface and date range' do
          let(:interface) { 'PPP0' }
          let(:start_date) { '2016-12-05' }
          let(:end_date) { '2016-12-06' }
          let(:expected_options) do
            { mac: device.mac,
              start_date: subject.send(:formatted_date, start_date),
              end_date:  subject.send(:formatted_date, end_date),
              interface: interface }
          end
          let(:mock_db_results) do
            [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'result' => { 'received' => 15, 'transmitted' => 20 } },
             { 'timestamp' => '2016-12-06T00:00:00.000Z', 'result' => { 'received' => 50, 'transmitted' => 70 } }]
          end

          before do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options)
              .and_return(mock_db_results)
          end

          it 'returns the usage per day for the given interface in the given date range' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token,
                        start_date: start_date, end_date: end_date, interface: interface, format: :json
            expect(json['results']).to eq(['mac' => device.mac,
                                           'usage' => [{ 'timestamp' => '2016-12-05T00:00:00.000Z', 'download' => 15, 'upload' => 20 },
                                                       { 'timestamp' => '2016-12-06T00:00:00.000Z', 'download' => 50, 'upload' => 70 }]])
          end
        end

        context 'when given an invalid date format' do
          let(:start_date) { '12-05-2016' }
          let(:end_date) { '12-06-2016' }

          it 'has a 400 Bad Request status code' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token,
                        start_date: start_date, end_date: end_date, format: :json
            expect(response.status).to eq(400)
          end
        end

        context 'when given an end date that precedes the start date format' do
          let(:start_date) { '12-07-2016' }
          let(:end_date) { '12-06-2016' }

          it 'has a 400 Bad Request status code' do
            get :usage, device_id: device.mac, auth_token: user.authentication_token,
                        start_date: start_date, end_date: end_date, format: :json
            expect(response.status).to eq(400)
          end
        end
      end

      context 'when given no device id' do
        let!(:user_devices) { create_list(:test_device, 3, organization: organization) }

        context 'when given a page limit that covers all devices on a single page' do
          let(:start_date) { DateTime.now.in_time_zone(user.timezone.name).beginning_of_day.utc.iso8601 }
          let(:end_date) { DateTime.now.in_time_zone(user.timezone.name).utc.iso8601 }
          let(:expected_options_1) do
            { mac: user_devices.first.mac,
              start_date: start_date,
              end_date: end_date }
          end
          let(:expected_options_2) do
            { mac: user_devices.second.mac,
              start_date: start_date,
              end_date: end_date }
          end
          let(:expected_options_3) do
            { mac: user_devices.third.mac,
              start_date: start_date,
              end_date: end_date }
          end

          before do
            Timecop.freeze(Time.local(2016, 12, 12, 12, 12, 12))
          end

          after do
            Timecop.return
          end

          it 'returns usage for all devices the user has access to' do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options_1).once
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options_2).once
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
              .with(expected_options_3).once
            get :usage, auth_token: user.authentication_token, page_limit: '4', format: :json
          end
        end

        context 'when given a page limit that causes pagination' do
          before do
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals).at_least(:once)
          end

          it 'returns the number of pages and next page number' do
            get :usage, auth_token: user.authentication_token, page_limit: '2', format: :json
            expect(json['metadata']).to eq('pages' => 2, 'next_page' => 2)
          end

          it 'returns the page limit number of devices' do
            get :usage, auth_token: user.authentication_token, page_limit: '2', format: :json
            expect(json['results'].size).to eq(2)
          end

          context 'when given a page number' do
            it 'returns the devices usage for that page' do
              get :usage, auth_token: user.authentication_token, page_limit: '2', page: '2', format: :json
              expect(json['results'].size).to eq(1)
              expect(json['results'].first['mac']).to eq(user_devices.third.mac)
            end
          end
        end

        context 'when given an organization name' do
          let(:sub_organization) { create(:sub_organization) }
          let!(:sub_device) { create(:test_device, organization: sub_organization) }
          let(:organization_2) { create(:organization) }
          let!(:random_device) { create(:test_device, organization: organization_2) }

          before do
            create(:permission,
                   manager:    organization,
                   manageable: organization_2,
                   role:       organization.admin_role)
            create(:permission,
                   manager:    user,
                   manageable: organization_2,
                   role:       organization.admin_role)
            expect(Measurement::CellularUtilization).to receive(:daily_utilization_totals)
          end

          context 'when no hierarchy is given' do
            it 'returns usage for devices within the organization only' do
              get :usage, auth_token: token, organization: sub_organization.name, format: :json
              expect(json['results'].size).to eq(1)
              expect(json['results'].first['mac']).to eq(sub_device.mac)
            end
          end
        end
      end
    end

    context 'when given an invalid user token' do
      let(:token) { 'invalid' }

      it 'has a 401 Unauthorized status code' do
        get :usage, auth_token: token, format: :json
        expect(response.status).to eq(401)
      end
    end
  end
end
