# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::Auth::AttBcController do
  describe 'POST #create' do
    let(:user)         { create(:user_with_org_permission) }
    let(:mock_context) { double('context')                 }

    before do
      allow(mock_context).to receive(:user) { user }
      allow(mock_context).to receive(:success?).and_return(true)
      allow(AuthenticateBusinessCenterUser).to receive(:call).and_return(mock_context)
      allow_any_instance_of(described_class).to receive(:sign_in).and_return(true)
    end

    context 'with valid attributes' do
      let(:params) do
        {
          format: :json,
          token: 'dummy'
        }
      end

      before do
        allow(mock_context).to receive(:device).and_return(nil)
      end

      it 'redirects to the root path' do
        post :create, params
        expect(response.status).to redirect_to root_path
      end
    end

    context 'with invalid attributes' do
      let(:params) do
        {
          format: :json,
          something: 'dummy'
        }
      end

      it 'gets a 400 invalid parameters status code' do
        post :create, params
        expect(response.status).to eq(400)
      end
    end

    context 'with a valid MAC address' do
      context 'for a device that exists in the database' do
        let(:device) { create(:test_device, organization_id: user.organization.id) }
        let(:params) do
          {
            format: :json,
            token:  'dummy',
            mac:    device.mac
          }
        end

        before do
          allow(mock_context).to receive(:device) { device }
        end

        it 'redirects to the device#show page' do
          post :create, params
          expect(response).to redirect_to device_path(device)
        end
      end
    end
  end
end
