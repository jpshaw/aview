# frozen_string_literal: true

require 'rails_helper'

describe Api::Certificates::V2Controller do
  describe 'POST #create' do
    it_behaves_like 'a SDN presence validator', :post, :create

    context 'with SDN' do
      let(:valid_csr) { File.read("#{Rails.root}/spec/support/fixtures/certificate_request.crt.req") }

      before do
        expect(controller).to receive(:authorize_device!).and_return(true)
      end

      context 'with no csr' do
        before do
          post :create
        end

        it 'returns a BAD REQUEST (400) status' do
          expect(response.status).to be(400)
        end
      end

      context 'with an invalid csr' do
        before do
          post :create, csr: 'invalid_csr'
        end

        it 'returns a BAD REQUEST (400) status' do
          expect(response.status).to be(400)
        end
      end

      context 'with an invalid common name' do
        let(:invalid_mac) { 'invalid_mac' }

        before do
          allow_any_instance_of(CertificateAuthority).to receive(:common_name).and_return(invalid_mac)
          post :create, csr: valid_csr
        end

        it 'returns a BAD REQUEST (400) status' do
          expect(response.status).to be(400)
        end
      end

      context "with an inexistent device for the CSR's common name" do
        before do
          post :create, csr: valid_csr
        end

        it 'returns a NOT FOUND (404) status' do
          expect(response.status).to be(404)
        end
      end

      context 'with a valid csr and an existing device' do
        let(:signed_csr)  { 'a_signed_csr'                       }
        let(:device)      { create(:netbridge_device) }

        context 'when the device has no certificates' do
          before do
            expect(device.certificates).to be_empty
            allow_any_instance_of(CertificateAuthority).to receive(:common_name).and_return(device.mac)
            allow_any_instance_of(CertificateAuthority).to receive(:sign_request).and_return(signed_csr)
          end

          it 'signs the certificate' do
            post(:create, csr: valid_csr)
          end

          it 'creates an approved and signed certificate' do
            expect { post(:create, csr: valid_csr) }.to change { device.certificates.count }.by(1)
            device.reload
            certificate = device.certificates.last
            expect(certificate.cert).to eq(signed_csr)
            expect(certificate).to      be_approved
            expect(certificate).to      be_signed
          end

          it 'returns an OK (200) status' do
            post(:create, csr: valid_csr)
            expect(response.status).to be(200)
          end

          it 'returns the signed certificate' do
            post(:create, csr: valid_csr)
            expect(response.body).to eq(signed_csr)
          end
        end

        context 'when the device has other certificates' do
          let(:a_certificate) { 'a_certificate' }

          before do
            c = device.certificates.create!(cert: a_certificate)
            expect(c).not_to  be_approved
            expect(c).not_to  be_signed

            allow_any_instance_of(CertificateAuthority).to receive(:common_name).and_return(device.mac)
          end

          context 'when the certificate does not exist in the DB for the device' do
            before do
              expect(device.certificates.find_by(md5: Certificate.md5(valid_csr))).to be_falsy
              allow_any_instance_of(CertificateAuthority).to receive(:sign_request).and_return(signed_csr)
            end

            it 'signs the certificate' do
              expect_any_instance_of(CertificateAuthority).to receive(:sign_request)
              post(:create, csr: valid_csr)
            end

            it 'creates an approved/signed certificate' do
              post(:create, csr: valid_csr)
              device.reload
              certificate = device.certificates.last
              expect(certificate.cert).to eq(signed_csr)
              expect(certificate).to  be_approved
              expect(certificate).to  be_signed
            end

            it 'deletes all other revoked certificates' do
              post(:create, csr: valid_csr)
              device.reload
              expect(device.certificates.find_by(md5: Certificate.md5(a_certificate))).to be_falsy
            end

            it 'returns an OK (200) status' do
              post(:create, csr: valid_csr)
              expect(response.status).to eq(200)
            end
          end

          context 'when the certificate exists in the DB for the device' do
            let(:certificate) { device.certificates.create!(cert: valid_csr) }

            context 'when the certificate is approved' do
              before do
                certificate.update(approved: true)
                expect(certificate).to be_approved
              end

              it 'deletes all other certificates for the device' do
                # Add certificates in all other statuses to make sure all of them are deleted.
                c = device.certificates.create!(cert: 'a_revoked_cert')
                expect(c).not_to  be_approved
                expect(c).not_to  be_signed

                c = device.certificates.create!(cert: 'an_approved_cert', approved: true)
                expect(c).to      be_approved
                expect(c).not_to  be_signed

                c = device.certificates.create!(cert: 'a_signed_cert', approved: true, signed: true)
                expect(c).to  be_approved
                expect(c).to  be_signed

                allow_any_instance_of(Certificate).to receive(:signed?).and_return(true)
                post(:create, csr: valid_csr)

                device.reload
                expect(device.certificates.count).to eq(1)
                expect(device.certificates).to include(certificate)
              end

              it 'returns an OK (200) status' do
                allow_any_instance_of(Certificate).to receive(:signed?).and_return(true)
                post(:create, csr: valid_csr)
                expect(response.status).to eq(200)
              end

              it 'returns the signed certificate' do
                allow_any_instance_of(CertificateAuthority).to receive(:sign_request).and_return(signed_csr)
                post(:create, csr: valid_csr)
                expect(response.body).to eq(signed_csr)
              end

              context 'when the certificate has not been signed yet' do
                before do
                  expect(certificate).not_to be_signed
                  allow_any_instance_of(CertificateAuthority).to receive(:sign_request).and_return(signed_csr)
                end

                it 'updates the certificate record with the signed certificate' do
                  cert = certificate.cert
                  post(:create, csr: valid_csr)
                  certificate.reload
                  expect(certificate).to          be_signed
                  expect(certificate.cert).not_to eq(cert)
                end
              end
            end

            context 'when the certificate is not approved' do
              before do
                expect(certificate).not_to be_approved
              end

              it 'does not sign the certificate' do
                expect_any_instance_of(CertificateAuthority).not_to receive(:sign_request)
                post(:create, csr: valid_csr)
              end

              it 'returns an ACCEPTED (202) status' do
                post(:create, csr: valid_csr)
                expect(response.status).to eq(202)
              end
            end
          end
        end
      end
    end
  end
end
