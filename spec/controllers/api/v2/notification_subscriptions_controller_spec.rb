# frozen_string_literal: true

require 'rails_helper'

describe Api::V2::NotificationSubscriptionsController do
  let(:organization) { create(:organization) }
  let(:user)         { create(:user, organization: organization) }
  let(:profile)      { create(:notification_profile, user: user) }

  let(:valid_attributes) do
    attributes_for(:organization_subscription,
                   user: user,
                   publisher_id: organization.id,
                   profile_id: profile.id)
  end
  let(:invalid_attributes) { attributes_for(:invalid_subscription) }

  before do
    Permission.create(manager: user, manageable: organization, role: organization.admin_role)
    sign_in user
  end

  describe 'GET #index' do
    let(:blank_datatable) { { 'draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => [] } }

    it 'renders a json datatable' do
      xhr :get, :index, account_id: user
      expect(JSON.parse(response.body)).to eq blank_datatable
    end
  end

  describe 'GET #search' do
    context 'when given an organization publisher type' do
      let(:publisher_type) { NotificationSubscription::Types::ORGANIZATION }
      let(:search_results) do
        {
          'results' => [{ 'id' => organization.id, 'name' => organization.name }]
        }
      end

      it 'returns organizations matching the query' do
        query = organization.name.split(' ').first
        xhr :get, :search, account_id: user, publisher_type: publisher_type, query: query
        expect(JSON.parse(response.body)).to eq search_results
      end
    end

    context 'when given a device publisher type' do
      let(:device)         { create(:test_device, organization: organization) }
      let(:publisher_type) { NotificationSubscription::Types::DEVICE }
      let(:search_results) do
        {
          'results' => [{ 'id' => device.id, 'mac' => device.mac, 'name' => device.mac }]
        }
      end

      it 'returns devices matching the query' do
        query = device.mac[0, 3]
        xhr :get, :search, account_id: user, publisher_type: publisher_type, query: query
        expect(JSON.parse(response.body)).to eq search_results
      end
    end
  end

  describe 'GET #new' do
    it 'assigns a new subscription to @subscription' do
      xhr :get, :new, account_id: user
      expect(assigns(:subscription)).to be_a NotificationSubscription
      expect(assigns(:subscription)).to be_new_record
    end

    it 'renders the #new template' do
      xhr :get, :new, account_id: user
      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:params) { valid_attributes.merge(account_id: user) }

      it 'creates a new subscription' do
        expect do
          xhr :post, :create, params
        end.to change(NotificationSubscription, :count).by(1)
      end

      it 'renders the #create template' do
        xhr :post, :create, params
        expect(response).to render_template(:create)
      end
    end

    context 'with invalid attributes' do
      let(:params) { invalid_attributes.merge(account_id: user) }

      it 'does not create a new subscription' do
        expect do
          xhr :post, :create, params
        end.not_to change(NotificationSubscription, :count)
      end

      it 'renders the #create template' do
        xhr :post, :create, params
        expect(response).to render_template(:create)
      end
    end
  end

  describe 'GET #edit' do
    let(:subscription) do
      create(:organization_subscription,
             user: user,
             publisher_id: organization.id,
             profile_id: profile.id)
    end

    it 'assigns the requested subscription to @subscription' do
      xhr :get, :edit, account_id: user, id: subscription.id
      expect(assigns(:subscription)).to eq subscription
    end

    it 'renders the #edit template' do
      xhr :get, :edit, account_id: user, id: subscription.id
      expect(response).to render_template(:edit)
    end
  end

  describe 'PATCH #update' do
    let(:subscription) do
      create(:organization_subscription,
             user: user,
             publisher_id: organization.id,
             profile_id: profile.id)
    end

    context 'with valid attributes' do
      let(:new_profile) { create(:notification_profile, user: user) }
      let(:params)      do
        valid_attributes.merge(id: subscription.id,
                               account_id: user,
                               profile_id: new_profile.id)
      end

      it 'changes the subscription attributes' do
        expect do
          xhr :patch, :update, params
        end.to change { subscription.reload.profile }.to(new_profile)
      end

      it 'renders the #update template' do
        xhr :patch, :update, params
        expect(response).to render_template(:update)
      end
    end

    context 'with invalid attributes' do
      let(:params) do
        invalid_attributes.merge(id: subscription.id,
                                 account_id: user)
      end

      it 'does not change the subscription attributes' do
        expect do
          xhr :patch, :update, params
        end.not_to change { subscription.reload.profile }
      end

      it 'renders the #update template' do
        xhr :patch, :update, params
        expect(response).to render_template(:update)
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:subscription) do
      create(:organization_subscription,
             user: user,
             publisher_id: organization.id,
             profile_id: profile.id)
    end
    let(:params) { { account_id: user.id, id: subscription.id } }

    it 'deletes the subscription' do
      expect do
        xhr :delete, :destroy, params
      end.to change(NotificationSubscription, :count).by(-1)
    end

    it 'renders the #delete template' do
      xhr :delete, :destroy, params
      expect(response).to render_template(:destroy)
    end
  end
end
