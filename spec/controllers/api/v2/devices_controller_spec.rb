# frozen_string_literal: true

require 'rails_helper'

describe Api::V2::DevicesController do
  render_views

  let(:json)         { JSON.parse(response.body)                     }
  let(:organization) { create(:organization)                         }
  let(:user)         { create(:api_user, organization: organization) }

  before do
    Settings.user_api = Config::Options.new(enabled?: true)
    Permission.create!(
      manager:    user,
      manageable: organization,
      role:       organization.admin_role
    )
  end

  after do
    Settings.reload!
  end

  describe 'GET #index' do
    let!(:devices) { create_list(:test_device, 3, organization: organization) }

    context 'when given a valid user token' do
      let(:token) { user.authentication_token }

      it 'has a 200 OK status code' do
        get :index, auth_token: token, format: :json
        expect(response.status).to eq(200)
      end

      it 'renders the devices' do
        get :index, auth_token: token, format: :json
        device_macs = json['results']['devices'].map { |device| device['mac'] }
        expect(device_macs).to eq devices.map(&:mac)
      end

      context 'when given a per_page number less than the total number of devices' do
        it 'renders the given number of devices on the first page' do
          get :index, auth_token: token, per_page: 2, format: :json
          device_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(device_macs).to include devices.first.mac
          expect(device_macs).to include devices.second.mac
          expect(device_macs).not_to include devices.third.mac
        end

        it 'renders the last devices on the last page' do
          get :index, auth_token: token, per_page: 2, page: 2, format: :json
          device_macs = json['results']['devices'].map { |device| device['mac'] }
          expect(device_macs).not_to include devices.first.mac
          expect(device_macs).not_to include devices.second.mac
          expect(device_macs).to include devices.third.mac
        end
      end
    end

    context 'when given an invalid user token' do
      let(:token) { 'testtoken' }

      it 'has a 401 Unauthorized status code' do
        get :index, auth_token: token, format: :json
        expect(response.status).to eq(401)
      end
    end
  end

  # api_v2_device_path(device)
  describe 'GET #show' do
    let(:device) { create(:test_device, organization: organization) }

    context 'when given a valid user token' do
      let(:token) { user.authentication_token }

      it 'has a 200 OK status code' do
        get :show, id: device.mac, auth_token: token, format: :json
        expect(response.status).to eq(200)
      end

      it 'assigns @device' do
        get :show, id: device.mac, auth_token: token, format: :json
        expect(assigns(:device)).to eq(device)
      end

      it 'renders the device' do
        get :show, id: device.mac, auth_token: token, format: :json
        expect(json['device']['mac']).to eq device.mac
      end
    end

    context 'when given an invalid user token' do
      let(:token) { 'testtoken' }

      it 'has a 401 Unauthorized status code' do
        get :show, id: device.mac, auth_token: token, format: :json
        expect(response.status).to eq(401)
      end
    end
  end
end
