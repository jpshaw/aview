# frozen_string_literal: true

require 'rails_helper'

describe Api::V2::AccountsController do
  let(:organization) { create(:organization) }
  let(:user)         do
    create(:user, organization: organization,
                  name: 'Test User',
                  email: 'test.user@accelerated.com',
                  password: 'password',
                  password_confirmation: 'password')
  end

  before do
    sign_in user
  end

  describe 'PATCH #update' do
    context 'with valid attributes' do
      it 'locates the requested @user' do
        xhr :patch, :update, id: user, user: attributes_for(:user)
        expect(assigns(:user)).to eq user
      end

      it 'changes the user attributes' do
        xhr :patch, :update, id: user, user: attributes_for(:user, name: 'Hello World')
        user.reload
        expect(user.name).to eq 'Hello World'
      end

      it 'renders the #update template' do
        xhr :patch, :update, id: user, user: attributes_for(:user)
        expect(response).to render_template(:update)
      end
    end

    context 'with invalid attributes' do
      it 'locates the requested @user' do
        xhr :patch, :update, id: user, user: attributes_for(:invalid_user)
        expect(assigns(:user)).to eq user
      end

      it 'renders the #update template' do
        xhr :patch, :update, id: user, user: attributes_for(:invalid_user)
        expect(response).to render_template(:update)
      end
    end
  end

  describe 'PATCH #update_password' do
    context 'with valid attributes' do
      let(:valid_attributes) do
        {
          current_password:      'password',
          password:              'new_password',
          password_confirmation: 'new_password'
        }
      end

      it 'locates the requested @user' do
        xhr :patch, :update_password, id: user, user: valid_attributes
        expect(assigns(:user)).to eq user
      end

      it 'updates the user`s password' do
        expect do
          xhr :patch, :update_password, id: user, user: valid_attributes
        end.to change { user.reload.encrypted_password }
      end
    end

    context 'with invalid attributes' do
      let(:invalid_attributes) do
        {
          current_password:      'invalid_password',
          password:              'new_password',
          password_confirmation: 'new_password'
        }
      end

      it 'locates the requested @user' do
        xhr :patch, :update_password, id: user, user: invalid_attributes
        expect(assigns(:user)).to eq user
      end

      it 'does not update the user`s password' do
        expect do
          xhr :patch, :update_password, id: user, user: invalid_attributes
        end.not_to change { user.reload.encrypted_password }
      end
    end
  end

  describe 'PATCH #generate_api_token' do
    it 'locates the requested @user' do
      xhr :patch, :generate_api_token, id: user
      expect(assigns(:user)).to eq user
    end

    it 'generates a new token' do
      expect do
        xhr :patch, :generate_api_token, id: user
      end.to change { user.reload.authentication_token }
    end
  end
end
