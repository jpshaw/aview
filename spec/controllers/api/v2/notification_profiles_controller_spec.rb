# frozen_string_literal: true

require 'rails_helper'

describe Api::V2::NotificationProfilesController do
  let(:organization) { create(:organization)                     }
  let(:user)         { create(:user, organization: organization) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    let(:blank_datatable) { { 'draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => [] } }

    before do
      # Remove default profile
      user.notification_profiles.destroy_all
    end

    it 'renders a json datatable' do
      xhr :get, :index, account_id: user
      expect(JSON.parse(response.body)).to eq blank_datatable
    end
  end

  describe 'GET #new' do
    it 'assigns a new profile to @profile' do
      xhr :get, :new, account_id: user
      expect(assigns(:profile)).to be_a NotificationProfile
      expect(assigns(:profile)).to be_new_record
    end

    it 'renders the #new template' do
      xhr :get, :new, account_id: user
      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:params) do
        {
          account_id: user,
          notification_profile: attributes_for(:notification_profile)
        }
      end

      it 'creates a new profile' do
        expect do
          xhr :post, :create, params
        end.to change(NotificationProfile, :count).by(1)
      end

      it 'renders the #create template' do
        xhr :post, :create, params
        expect(response).to render_template(:create)
      end

      context 'when given subscribed events' do
        let(:subscribed_events) { [attributes_for(:subscribed_event)]          }
        let(:frequency)         { NotificationProfile::Frequencies::EVERY_HOUR }

        before do
          params[:notification_profile][:subscribed_events_attributes] = subscribed_events
        end

        it 'creates the subscribed event associations' do
          expect do
            xhr :post, :create, params
          end.to change(SubscribedEvent, :count).by(1)
        end

        # TODO: Remove this when notifications depend solely on the profile
        # for frequencies
        it 'assigns the frequency to the profile`s subscribed events' do
          params[:notification_profile][:frequency] = frequency
          xhr :post, :create, params
          profile = NotificationProfile.last
          expect(profile.subscribed_events.map(&:frequency).uniq).to eq [frequency]
        end
      end
    end

    context 'with invalid attributes' do
      let(:params) do
        {
          account_id: user,
          notification_profile: attributes_for(:invalid_notification_profile)
        }
      end

      it 'does not create a new subscription' do
        expect do
          xhr :post, :create, params
        end.not_to change(NotificationSubscription, :count)
      end

      it 'renders the #create template' do
        xhr :post, :create, params
        expect(response).to render_template(:create)
      end
    end
  end

  describe 'GET #edit' do
    let(:profile) { create(:notification_profile, user: user) }
    let(:params)  { { account_id: user, id: profile.id } }

    it 'assigns the requested profile to @profile' do
      xhr :get, :edit, params
      expect(assigns(:profile)).to eq profile
    end

    it 'assigns a hash to @subscribed_uuids' do
      xhr :get, :edit, params
      expect(assigns(:subscribed_uuids)).to be_a Hash
    end

    it 'renders the #edit template' do
      xhr :get, :edit, params
      expect(response).to render_template(:edit)
    end
  end

  describe 'PATCH #update' do
    let(:profile) { create(:notification_profile, user: user) }

    context 'with valid attributes' do
      let(:new_name)         { 'New Name'                                            }
      let(:valid_attributes) { attributes_for(:notification_profile, name: new_name) }
      let(:params)           do
        {
          account_id:           user,
          id:                   profile.id,
          notification_profile: valid_attributes
        }
      end

      it 'changes the profile attributes' do
        expect do
          xhr :patch, :update, params
        end.to change { profile.reload.name }.to(new_name)
      end

      it 'renders the #update template' do
        xhr :patch, :update, params
        expect(response).to render_template(:update)
      end

      context 'when updating subscribed events' do
        let(:subscribed_events) { create_list(:subscribed_event, 3, profile_id: profile.id) }
        let(:subscribed_events_attributes) { subscribed_events.map(&:attributes) }

        before do
          params[:notification_profile][:subscribed_events_attributes] = subscribed_events_attributes
        end

        # TODO: Remove this when notifications depend solely on the profile
        # for frequencies
        context 'when updating the frequency' do
          let(:frequency) { NotificationProfile::Frequencies::EVERY_HOUR }

          before do
            params[:notification_profile][:frequency] = frequency
          end

          it 'assigns the frequency to the profile`s subscribed events' do
            expect do
              xhr :patch, :update, params
            end.to change { subscribed_events.map(&:reload).map(&:frequency).uniq }.to([frequency])
          end
        end
      end
    end

    context 'with invalid attributes' do
      let(:invalid_attributes) { attributes_for(:invalid_notification_profile) }
      let(:params)             do
        {
          account_id:           user,
          id:                   profile.id,
          notification_profile: invalid_attributes
        }
      end

      it 'does not change the subscription attributes' do
        expect do
          xhr :patch, :update, params
        end.not_to change { profile.reload.name }
      end

      it 'renders the #update template' do
        xhr :patch, :update, params
        expect(response).to render_template(:update)
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:profile) { create(:notification_profile, user: user) }
    let(:params)   { { account_id: user.id, id: profile.id } }

    it 'deletes the profile' do
      expect do
        xhr :delete, :destroy, params
      end.to change(NotificationProfile, :count).by(-1)
    end

    it 'renders the #delete template' do
      xhr :delete, :destroy, params
      expect(response).to render_template(:destroy)
    end
  end
end
