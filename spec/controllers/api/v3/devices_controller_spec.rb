# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V3::DevicesController, type: :controller do
  describe 'PATCH update' do
    let(:user)   { create(:user_with_org_permission) }
    let(:device) { create(:test_device, organization_id: user.organization.id) }

    before { sign_in user }

    context 'as JSON' do
      let(:params) { { id: device.mac, device: { mgmt_host: '1234' }, format: :json } }

      context 'when the interactor succeeds' do
        let(:context) { double(:context, success?: true) }

        before do
          allow(UpdateDevice).to receive(:call).once.and_return(context)
        end

        it 'returns a 200 status code' do
          patch :update, params
          expect(response).to have_http_status(:ok)
        end
      end

      context 'when then interactor fails' do
        let(:context) { double(:context, success?: false, message: 'error') }

        before do
          allow(UpdateDevice).to receive(:call).once.and_return(context)
        end

        it 'returns a 400 status code' do
          patch :update, params
          expect(response).to have_http_status(:bad_request)
        end

        it 'uses the result message' do
          expect(context).to receive(:message)
          patch :update, params
        end
      end
    end
  end
end
