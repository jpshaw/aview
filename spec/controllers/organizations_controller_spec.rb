# frozen_string_literal: true

require 'rails_helper'

describe OrganizationsController do
  before do
    @user = create(:user_with_org_permission)
    @organization = @user.organization
    sign_in @user
  end

  describe 'GET #index' do
    let(:parent_organization)  { create(:random_regular_organization) }
    let(:sibling_organization) { create(:random_regular_organization) }

    before do
      @role = @organization.admin_role
      parent_organization.children << @organization
      parent_organization.children << sibling_organization
      @user.manager_permissions.create do |p|
        p.manageable = @organization
        p.role       = @role
      end
    end

    it 'shows managed organizations' do
      get :index
      expect(assigns(:customers)).not_to include(sibling_organization)
      expect(assigns(:customers)).not_to include(parent_organization)
      expect(assigns(:customers)).to include(@organization)
    end
    context 'with permissions on a sub-organization' do
      let(:child_organization) { create(:random_regular_organization) }

      before do
        @organization.children << child_organization
        @user.manager_permissions.create do |p|
          p.manageable = child_organization
          p.role       = @role
        end
      end

      it 'does not repeat the sub-organization' do
        get :index
        expect(assigns(:customers)).not_to include(child_organization)
      end
    end
  end

  describe 'GET #show' do
    it_behaves_like 'an action that gets an organization from params', :get, :show
  end

  describe 'GET #new' do
    let(:ability) { 'Modify Organizations' }

    context 'when the user is not authorized to create organizations' do
      it 'redirects to the root path' do
        get :new
        expect(response).to redirect_to root_path
        expect(flash).not_to be nil
        expect(flash[:error]).to match /You are not authorized to perform this action/i
      end
    end

    context 'when the user is authorized to create organizations' do
      before do
        allow(controller).to receive(:authorize).with(Organization, :new?).and_return(true)
      end

      it 'has a 200 status code' do
        get :new
        expect(response).not_to redirect_to root_path
        expect(response.status).to eq(200)
      end

      it 'assigns a new Organization to @organization' do
        get :new
        organization = assigns(:organization)
        expect(organization).to be_an Organization
        expect(organization).to respond_to :new_record?
      end

      it 'assigns a list of modifyable organizations' do
        get :new
        expect(assigns(:modifiable_organizations)).not_to be nil
      end

      it 'renders the :new template' do
        get :new
        expect(response).to render_template :new
      end
    end
  end

  describe 'POST #create' do
    let(:parent) { create(:sub_organization) }

    let(:params) do
      {
        organization: {
          name: 'Test Organization'
        },
        parent_id: parent.id
      }
    end

    before do
      Permission.create(manager: controller.current_user, manageable: parent, role: Role.new)
    end

    context 'with valid attributes' do
      before do
        create(:admin_role_with_sequenced_name)
      end

      context 'when the user is authorized to modify the parent organization' do
        before do
          allow(controller.current_user).to receive(:has_ability_on?)
            .with(parent, to: 'Modify Organizations')
            .and_return(true)
        end

        it 'creates a new organization' do
          expect do
            post :create, params
          end.to change(Organization, :count).by(1)
        end

        it 'creates a child organization for the parent organization' do
          expect(parent.children).to be_empty
          post :create, params
          expect(parent.children(true)).to be_present
        end

        it 'redirects to the new organization' do
          post :create, params
          expect(response).to redirect_to Organization.last
        end
      end

      context 'when the user is not authorized to modify the parent organization' do
        before do
          allow(controller.current_user).to receive(:has_ability_on?)
            .with(parent, to: 'Modify Organizations')
            .and_return(false)
        end

        it 'does not create a new organization' do
          expect do
            post :create, params
          end.to change(Organization, :count).by(0)
        end

        it 'redirects to the root path' do
          post :create, params
          expect(response).to redirect_to root_path
        end
      end
    end

    context 'with invalid attributes' do
      before do
        allow(controller.current_user).to receive(:has_ability_on?)
          .with(parent, to: 'Modify Organizations')
          .and_return(true)
      end

      it 'renders the new page with errors' do
        post :create, organization: { name: nil }, parent_id: parent.id
        expect(response).to render_template :new
        expect(assigns(:organization).errors).not_to be_empty
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @organization = create(:sub_organization)
    end

    it_behaves_like 'an action that gets an organization from params', :delete, :destroy

    context 'when the user can access the organization' do
      before do
        Permission.create(
          manager: controller.current_user,
          manageable: @organization,
          role: create(:role)
        )
      end

      def can_modify_organization(org)
        allow(controller.current_user).to receive(:has_ability_on?)
          .with(org, to: 'Modify Organizations')
          .and_return(true)
      end

      context 'when the user has the ability to modify the organization' do
        before do
          can_modify_organization(@organization)
        end

        context 'when the user tries to destroy the root organization' do
          it 'does not destroy the organization' do
            organization = Organization.roots.first
            can_modify_organization(organization)
            expect do
              delete :destroy, id: organization.slug
            end.not_to change(Organization, :count)
          end
        end

        context 'when the user tries to destroy a non-root organization' do
          it 'destroys the organization' do
            expect do
              delete :destroy, id: @organization.slug
            end.to change(Organization, :count).by(-1)
          end

          it 'redirects to organizations#index' do
            delete :destroy, id: @organization.slug
            expect(response).to redirect_to organizations_path
          end
        end
      end

      context 'when the user does not have the ability to destroy the organization' do
        before do
          allow(controller.current_user).to receive(:has_ability_on?)
            .with(@organization, to: 'Modify Organizations')
            .and_return(false)
        end

        it 'redirects to the root path' do
          delete :destroy, id: @organization.slug
          expect(response).to redirect_to root_path
        end
      end
    end
  end

  describe 'GET #edit' do
    it_behaves_like 'an action that gets an organization from params', :get, :edit
  end

  describe 'PATCH #update' do
    before do
      allow(controller).to receive(:authorize).and_return(true)
      allow(controller).to receive(:show_breadcrumbs)
      allow(controller).to receive(:edit_breadcrumbs)
      allow(controller).to receive(:organization_params)
    end

    it_behaves_like 'an action that gets an organization from params', :patch, :update

    context 'When update succeeds' do
      before do
        allow(UpdateOrganization).to receive(:call).and_return(OpenStruct.new(organization: @organization, success?: true))
        patch :update, id: @organization.slug
      end

      it 'redirects to the edit page' do
        expect(response).to redirect_to edit_organization_path(@organization)
      end
    end

    context 'When update fails' do
      before do
        allow(UpdateOrganization).to receive(:call).and_return(OpenStruct.new(organization: @organization, success?: false))
        patch :update, id: @organization.slug
      end

      it 'renders the edit page' do
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'GET #actions_meanu' do
    context 'when the user has the ability to view the organization' do
      context 'when the request is ajax' do
        subject { xhr :get, :actions_menu, id: @organization.slug }

        it 'renders the actions menu partial' do
          expect(subject).to render_template(partial: '_organization_action_dropdown')
        end
      end

      context 'when the request is html' do
        subject { get :actions_menu, id: @organization.slug }

        it 'redirects to the organization show page' do
          expect(subject).to redirect_to(organization_path(@organization))
        end
      end
    end
  end
end
