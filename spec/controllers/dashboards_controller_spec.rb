# frozen_string_literal: true

require 'rails_helper'

describe DashboardsController do
  context 'An authenticated user' do
    before do
      user = create(:user)
      sign_in user
    end

    context 'performs a GET show' do
      context 'as HTML' do
        it 'renders the show template' do
          get :show
          expect(response).to render_template 'show'
        end
      end

      context 'as JSON' do
        it 'raises an UnknownFormat error' do
          expect { get :show, format: 'json' }.to raise_error ActionController::UnknownFormat
        end
      end
    end
  end

  context 'An unauthenticated user' do
    context 'performs a GET show' do
      context 'as HTML' do
        it 'redirects to the login page' do
          get :show
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
