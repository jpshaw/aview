# frozen_string_literal: true

require 'rails_helper'

describe EventsController do
  before do
    create_user
    sign_in @user
  end

  describe 'SHOW #events' do
    context 'when the event does not belong to the user' do
      before { @event = create(:event) }

      it 'raises a `record not found` exception' do
        expect do
          get :show, id: @event.id
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context 'when the event belongs to the user' do
      before do
        @event = create(:event)
        @user.organization.children << @event.device.organization
      end

      it 'renders the show page' do
        get :show, id: @event.id
        expect(response).to render_template(:show)
      end
    end
  end
end
