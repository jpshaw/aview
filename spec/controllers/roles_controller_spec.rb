# frozen_string_literal: true

require 'rails_helper'

describe RolesController do
  before do
    @user          = create(:user)
    @organization  = @user.organization
    @role          = @organization.admin_role
    @role.abilities << create(:organization_ability)

    Permission.create(manager: @user, manageable: @organization, role: @role)

    @params = { organization_id: @organization.slug }

    sign_in @user
  end

  describe 'GET #index' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :get, :new

    describe 'when the request is json' do
      let(:role_count) { Role.count      }
      let(:role_name)  { Role.first.name }

      it 'renders a json datatable with the role in it' do
        get :index, @params.merge(format: :json)
        result = JSON.parse(response.body)
        expect(result['recordsTotal']).to eq role_count
        expect(result['data'].first).to include role_name
      end
    end

    describe 'when the request is js' do
      it 'renders the role select partial' do
        xhr :get, :index, @params
        expect(response).to render_template(partial: '_index_select')
      end
    end
  end

  describe 'GET #new' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :get, :new

    it 'assigns a new Role to @role' do
      xhr :get, :new, @params
      expect(assigns(:role)).to be_new_record
      expect(assigns(:role)).to be_a Role
    end

    it 'assigns abilities' do
      xhr :get, :new, @params
      expect(assigns(:abilities).sort).to eq @role.abilities.sort
    end

    it 'renders the #new template' do
      xhr :get, :new, @params
      expect(response).to render_template(:new)
    end
  end

  describe 'POST #create' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :post, :create

    context 'with valid attributes' do
      before do
        @params[:role] = { name: 'Test Role' }
      end

      it 'creates a new role' do
        expect do
          xhr :post, :create, @params
        end.to change(Role, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      before do
        @params[:role] = { name: nil }
      end

      it 'does not save the new role' do
        expect do
          xhr :post, :create, @params
        end.not_to change(Role, :count)
      end

      it 're-renders the #create template' do
        xhr :post, :create, @params
        expect(response).to render_template :create
      end

      it 'assigns abilities' do
        xhr :post, :create, @params
        expect(assigns(:abilities).sort).to eq @role.abilities.sort
      end
    end
  end

  describe 'GET #edit' do
    it_behaves_like 'an action restricted by `Modify Organizations`', :get, :edit

    it 'assigns the requested role to @role' do
      xhr :get, :edit, @params.merge(id: @role)
      expect(assigns(:role)).to eq @role
    end

    it 'renders the :edit view' do
      xhr :get, :edit, @params.merge(id: @role)
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH #update' do
    before do
      @params[:id] = @role
    end

    it_behaves_like 'an action restricted by `Modify Organizations`', :patch, :update

    context 'with valid attributes' do
      before do
        @params[:role] = { name: 'New Role Name' }
      end

      it 'located the requested @role' do
        xhr :patch, :update, @params
        expect(assigns(:role)).to eq @role
      end

      it 'changes the @role`s attributes' do
        xhr :patch, :update, @params
        @role.reload
        expect(@role.name).to eq 'New Role Name'
      end
    end

    context 'with invalid attributes' do
      before do
        @params[:role] = { name: nil }
      end

      it 'locates the requested @role' do
        xhr :patch, :update, @params
        expect(assigns(:role)).to eq @role
      end

      it 'does not change the @role`s attributes' do
        xhr :patch, :update, @params
        @role.reload
        expect(@role.name).not_to be nil
      end

      it 'renders the :update view' do
        xhr :patch, :update, @params
        expect(response).to render_template :update
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @params[:id] = @role
    end

    it_behaves_like 'an action restricted by `Modify Organizations`', :delete, :destroy

    it 'deletes the role' do
      expect do
        xhr :delete, :destroy, @params
      end.to change(Role, :count).by(-1)
    end

    it 'renders the #destroy template' do
      xhr :delete, :destroy, @params
      expect(response).to render_template :destroy
    end
  end

  describe 'GET #abilities' do
    before do
      @params[:role_id] = @role
    end

    it 'gets the abilities for the given role' do
      xhr :get, :abilities, @params
      expect(assigns(:role).abilities.sort).to eq @role.abilities.sort
    end

    it 'renders the #abilities template' do
      xhr :get, :abilities, @params
      expect(response).to render_template '_abilities'
    end
  end
end
