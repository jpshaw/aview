# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device search', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
    gateway_category = create(:gateway_category)
    gateway_series = create(:gateway_8300_model, category: gateway_category)
    @gateway = create(:gateway_device, category: gateway_category, series: gateway_series, organization: @organization)

    dial_to_ip_category = create(:dial_to_ip_category)
    dial_to_ip_default = create(:dial_to_ip_5300_DC_model, category: dial_to_ip_category)
    @dial_to_ip = create(:dial_to_ip_device, category: dial_to_ip_category, series: dial_to_ip_default, organization: @organization)
  end

  it 'Searching by mac shows the device with the given mac' do
    visit devices_path
    fill_in 'simple-search-input', with: @gateway.mac
    find('#simple-search-input').native.send_key(:enter)
    wait_for_page_to_load
    expect(current_path).to eq searches_path
    expect(page).to have_text(@gateway.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
  end

  it 'Searching by first few characters of mac shows the devices that contain the given value' do
    visit devices_path
    fill_in 'simple-search-input', with: '00'
    find('#simple-search-input').native.send_key(:enter)
    wait_for_page_to_load
    expect(current_path).to eq searches_path
    expect(page).to have_text(@gateway.mac)
    expect(page).to have_text(@dial_to_ip.mac)
  end
end
