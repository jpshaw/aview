# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Dashboard page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)

    gateway_category = create(:gateway_category)
    gateway_series = create(:gateway_8300_model, category: gateway_category)
    @gateway = create(:gateway_device, category: gateway_category, series: gateway_series, organization: @organization, is_deployed: true)
    @gateway.recovery_failed!

    dial_to_ip_category = create(:dial_to_ip_category)
    dial_to_ip_default = create(:dial_to_ip_5300_DC_model, category: dial_to_ip_category)
    @dial_to_ip = create(:dial_to_ip_device, category: dial_to_ip_category, series: dial_to_ip_default, organization: @organization, is_deployed: true)
  end

  it 'The datatable lists all down devices' do
    visit root_path
    expect(page).to have_text(@gateway.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
  end
end
