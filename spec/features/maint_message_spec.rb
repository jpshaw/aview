# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Maintenance Message', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @test_message = 'This is a test message.'
    AdminSettings['threshold_percentage'] = 2
  end

  it 'Entered message with correct time shows' do
    visit admin_path(@gateway, anchor: :message)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script("$('#admin_settings_maint_message').summernote('code', '#{@test_message}')")
    page.evaluate_script("$('#maint_message_expiration').data('date', '#{Date.tomorrow.strftime('%Y-%m-%d %H:%M:%S')}')")
    find('#maint_message_form .btn-primary', text: 'Save Changes').click
    wait_for_ajax
    page.evaluate_script('$(".profile-element a").click()')
    wait_for_page_to_load
    find('.col-md-12', text: @test_message)
  end

  it 'Entered message with already expired time does not show' do
    visit admin_path(@gateway, anchor: :message)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script("$('#admin_settings_maint_message').summernote('code', '#{@test_message}')")
    page.evaluate_script("$('#maint_message_expiration').data('date', '#{Date.today.strftime('%Y-%m-%d %H:%M:%S')}')")
    find('#maint_message_form .btn-primary', text: 'Save Changes').click
    wait_for_ajax
    page.evaluate_script('$(".profile-element a").click()')
    wait_for_page_to_load
    expect(page).not_to have_selector('.col-md-12', text: @test_message)
  end

  it 'Blank message with tomorrows time does not show' do
    visit admin_path(@gateway, anchor: :message)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script("$('#admin_settings_maint_message').summernote('code', '')")
    page.evaluate_script("$('#maint_message_expiration').data('date', '#{Date.tomorrow.strftime('%Y-%m-%d %H:%M:%S')}')")
    find('#maint_message_form .btn-primary', text: 'Save Changes').click
    wait_for_ajax
    page.evaluate_script('$(".profile-element a").click()')
    wait_for_page_to_load
    expect(page).not_to have_selector('.col-md-12', text: @test_message)
  end
end
