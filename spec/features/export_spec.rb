# frozen_string_literal: true

require 'rails_helper'
include ExportHelper
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Export Feature', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
  end

  context 'Dashboard' do
    let(:number_of_columns) { 13 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      (0...@records_length).each do |i|
        @devices << create(:test_device, :with_sequenced_category, :with_sequenced_model, organization: @organization, is_deployed: true, last_heartbeat_at: Time.current.advance(minutes: -i))
        @devices[i].recovery_failed!
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(DevicesDataTable)
      visit dashboard_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Dashboard Chart Links' do
    let(:number_of_columns) { 13 }

    before do
      @device_on_4g = create(:test_device, :with_sequenced_category, :with_sequenced_model, :deployed, organization: @organization)
      create(:device_modem, cnti: 'LTE', device: @device_on_4g)

      @device_on_unknown = create(:test_device, :with_sequenced_category, :with_sequenced_model, :deployed, organization: @organization)
      create(:device_modem, cnti: 'unknown', device: @device_on_unknown)
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(DevicesDataTable)
      visit dashboard_path
      page.execute_script("$('#network-type').highcharts().series[0].points[0].firePointEvent('click')")

      find('.devices.index')
      expect(current_path).to eq devices_path

      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq 1
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@device_on_4g.mac)
    end
  end

  context 'Device Index' do
    let(:number_of_columns) { 13 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      (0...@records_length).each do |i|
        @devices << create(:test_device, :with_sequenced_category, :with_sequenced_model, organization: @organization, is_deployed: true, last_heartbeat_at: Time.current.advance(minutes: -i))
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(DevicesDataTable)
      visit devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - Cellular Subindex' do
    let(:number_of_columns) { 41 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      create(:legacy_cellular_category)
      cellular_model = create(:cellular_6300_CX_model)

      (0...@records_length).each do |i|
        @devices << create(:cellular_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: cellular_model.category, model_id: cellular_model.id)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::CellularDataTable)
      visit cellular_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - Gateway Subindex' do
    let(:number_of_columns) { 15 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      series_model = create(:gateway_8100_model)

      (0...@records_length).each do |i|
        @devices << create(:gateway_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: series_model.category, model_id: series_model.id)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::GatewayDataTable)
      visit vpn_gateway_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - Wifi Subindex' do
    let(:number_of_columns) { 13 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      series_model = create(:netreach_4200_NX_model)

      (0...@records_length).each do |i|
        @devices << create(:netreach_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: series_model.category, model_id: series_model.id)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::NetreachDataTable)
      visit wifi_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - Dial-to-IP Subindex' do
    let(:number_of_columns) { 13 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      series_model = create(:dial_to_ip_5300_DC_model)

      (0...@records_length).each do |i|
        @devices << create(:dial_to_ip_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: series_model.category, model_id: series_model.id)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::DialToIpDataTable)
      visit dial_to_ip_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - Remote Manager' do
    let(:number_of_columns) { 15 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      series_model = create(:remote_manager_5400_RM_model)

      (0...@records_length).each do |i|
        @devices << create(:remote_manager_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: series_model.category, series: series_model)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::RemoteManagerDataTable)
      visit remote_manager_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Device - UCPE' do
    let(:number_of_columns) { 13 }

    before do
      @devices = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      series_model = create(:ucpe_9400_UA_model)

      (0...@records_length).each do |i|
        @devices << create(:ucpe_device, :deployed, organization: @organization, last_heartbeat_at: Time.current.advance(minutes: -i), category: series_model.category, model_id: series_model.id)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(Devices::UcpeDataTable)
      visit ucpe_devices_path

      find('#device-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).not_to include(@devices.last.mac)

      # All Pages
      click_animated_element(find('#device-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@devices.first.mac)
      expect(@results.last).to include(@devices.last.mac)
    end
  end

  context 'Notifications Index' do
    let(:number_of_columns) { 9 }

    before do
      @notifications = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      device = create(:test_device, :deployed, :with_sequenced_category, :with_sequenced_model, organization: @organization)

      (0...@records_length).each do |i|
        event = create(:device_event, device: device, information: "INFO: #{i}")
        @notifications << create(:notification, event: event, user: @user, delivered_at: Time.current.advance(minutes: -i))
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(NotificationsDataTable)
      visit notifications_path

      find('#events-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#events-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('INFO: 0')
      expect(@results.last).not_to include("INFO: #{@min_page_length}")

      # All Pages
      click_animated_element(find('#events-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('INFO: 0')
      expect(@results.last).to include("INFO: #{@min_page_length}")
      visit root_path # needed for mcp - otherwise the data table continues to reload during tear down
    end
  end

  context 'Individual Device Events' do
    let(:number_of_columns) { 10 }

    before do
      @events = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length + 1

      @device = create(
        :device,
        :deployed,
        category: create(:test_category),
        series: create(:test_model_with_sequenced_name),
        mac: 'AABBCC123456',
        organization: @organization
      )

      (0...@records_length).each do |i|
        @events << create(:device_event, created_at: Time.current.advance(minutes: -i), device: @device, information: "INFO: #{i}")
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(DeviceEventsDataTable)
      visit events_device_path(@device.mac)

      find('#device-events-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#device-events-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('INFO: 0')
      expect(@results.last).not_to include("INFO: #{@min_page_length}")

      # All Pages
      click_animated_element(find('#device-events-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('INFO: 0')
      expect(@results.last).to include("INFO: #{@min_page_length}")
      visit root_path # needed for mcp - otherwise the data table continues to reload during tear down
    end
  end

  context 'Report' do
    let(:number_of_columns) { 13 }

    before do
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = 38
      @mac_1 = '00C0CA49D9D4'
      @mac_38 = '00270400005E'

      results_file_path = 'spec/fixtures/files/sample_availability_report'
      params = {
        'report_type' => 'availability',
        'organization_id' => '2',
        'include_hierarchy' => '0',
        'categories' => [''],
        'models' => [''],
        'deployment' => '0',
        'sort' => 'devices.last_heartbeat_at',
        'direction' => 'desc',
        'range' => 'last_day',
        'stats_headers' => {
          'up' => 'Up (%)',
          'down' => 'Down (%)',
          'downtime_count' => 'Downtime Events',
          'unreachable_count' => 'Unreachable Events'
        }
      }.with_indifferent_access

      @report = create(:device_report, finished_at: Time.current, params: params, results_file: results_file_path, user: @user)
      allow_any_instance_of(DeviceReport).to receive(:results_file_name).and_return(@report.results_file)
    end

    it 'Implements CSV with params' do
      allow_any_instance_of(ReportsController).to receive(:send_data) do |controller|
        options = {}
        options[:report] = @report
        options[:column_headers] = controller.view_context.params['column-headers']
        options[:column_values] = controller.view_context.params['column-values']
        @results = CSV.parse(ReportDataTable.new(options).to_csv)
        controller.render nothing: true, status: :ok
      end
      intercept_send_data_results(ReportDataTable)

      visit reports_path

      # Report Index
      click_link('csv-export')
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@mac_1)
      expect(@results.last).to include(@mac_38)

      find("#report_#{@report.id} td:first-of-type a").click
      wait_for_ajax

      find('#report-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#report-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@mac_1)
      expect(@results.last).not_to include(@mac_38)

      # All Pages
      click_animated_element(find('#report-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@mac_1)
      expect(@results.last).to include(@mac_38)
    end
  end

  context 'Sites Index' do
    let(:number_of_columns) { 5 }

    before do
      @sites = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length

      (0...@records_length).each do |_i|
        @sites << create(:site, organization: @organization)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(SitesDataTable)
      visit sites_path

      wait_for_animation
      find('#site-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#site-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('Default')
      expect(@results[2]).to include(@sites.first.name)
      expect(@results.last).not_to include(@sites.last.name)

      # All Pages
      click_animated_element(find('#site-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length + 1
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include('Default')
      expect(@results[2]).to include(@sites.first.name)
      expect(@results.last).to include(@sites.last.name)
    end
  end

  context 'Users Index' do
    let(:number_of_columns) { 6 }

    before do
      @users = []
      @min_page_length = DataTables::Base::LENGTH_MENU[0][0]
      @records_length = @min_page_length
      @users << @user

      (0...@records_length).each do |_i|
        @users << create(:user, organization: @organization, name: Faker::Name.name)
      end
    end

    it 'Implements CSV with params' do
      intercept_send_data_results(UsersDataTable)
      visit users_path

      wait_for_animation
      find('#users-table_wrapper select').find("option[value='#{@min_page_length}']").select_option
      wait_for_ajax

      # Current Page
      click_animated_element(find('#users-table_wrapper button.advanced-export'))
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @min_page_length
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@users.first.name)
      expect(@results.last).not_to include(@users.last.name)

      # All Pages
      click_animated_element(find('#users-table_wrapper button.advanced-export'))
      find('#all-pages-btn').click
      find('#advanced-export-btn').click
      expect(@results.size - 1).to eq @records_length + 1
      expect(@results.first.size).to eq number_of_columns
      expect(@results[1]).to include(@users.first.name)
      expect(@results.last).to include(@users.last.name)
    end
  end
end
