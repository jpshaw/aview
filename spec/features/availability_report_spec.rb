# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Availability Report Spec', js: true do
  before do
    # Create User
    create_user_with_all_abilities
    login_as(@user, scope: :user)

    # Create Device
    gateway_category = create(:gateway_category)
    gateway_series = create(:gateway_8300_model, category: gateway_category)
    @gateway = create(:gateway_device, category: gateway_category, series: gateway_series, organization: @organization)

    # Stub Torquebox Queuing
    queue = double('queue')
    expect(TorqueBox).to receive(:fetch).with('/queues/reports').and_return(queue)
    processor = ReportProcessor.new
    expect(queue).to receive(:publish) { |*args| processor.on_message(*args) }
  end

  it 'Generates and displays report.' do
    visit availability_reports_path
    page.click_button('Create')
    page.find('#report-table_wrapper').has_text?(@gateway.mac.to_s)
  end
end
