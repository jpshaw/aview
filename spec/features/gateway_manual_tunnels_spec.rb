# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Gateway Manual Tunnel Commands', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @gateway = create(:gateway_device_with_configuration, :commandable, organization: @organization)
  end

  it 'Gateway manual tunnel command menu contains correct command names' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    manual_tunnels = find('.sub-commands', text: 'Manual Tunnels')
    manual_tunnels.hover
    manual_tunnels.find('.dropdown-menu').all('.send_command').each do |command_link|
      href = command_link[:href]
      if href.include? '#'
        expect(['tunnel_up-command', 'tunnel_down-command', 'tunnel_bounce-command', 'tunnel_status-command']).to include(href.split('#')[1])
      else
        expect(href).to include('tunnels')
      end
    end
  end

  it 'Gateway manual tunnel commands open correct modal' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    manual_tunnels = find('.sub-commands', text: 'Manual Tunnels')
    manual_tunnels.hover
    manual_tunnels.find('.send_command', text: 'Down').click
    find('.modal-title', text: 'Send Down Command')
    find('.modal.inmodal.fade.in').click
    expect(page).to have_selector('.modal-title', text: 'Send Down Command')
  end

  it 'Gateway manual tunnel modal enforces disabling of unselected criteria' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    manual_tunnels = find('.sub-commands', text: 'Manual Tunnels')
    manual_tunnels.hover
    manual_tunnels.find('.send_command', text: 'Up').click
    find('.modal-title', text: 'Send Up Command')
    expect(page).to have_field(placeholder: 'Enter tunnel name', disabled: true)
    page.first('label', text: 'Name').find('input').click
    expect(page).to have_field(text: 'Select a tunnel index', disabled: true)
  end

  it 'Gateway manual tunnel modal valid submit goes to events page' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    manual_tunnels = find('.sub-commands', text: 'Manual Tunnels')
    manual_tunnels.hover
    manual_tunnels.find('.send_command', text: 'Bounce').click
    find('.modal-title', text: 'Send Bounce Command')
    find("option[value='1']").click
    find('.btn-primary', text: 'Submit').click
    wait_for_ajax
    wait_for_page_to_load
    expect(current_path).to include('events')
  end
end
