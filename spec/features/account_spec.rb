# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Account page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  it 'Account page loads correctly' do
    visit account_index_path
    expect(page).to have_css('#page-title-header', text: 'Account')
  end
end
