# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

if RUBY_PLATFORM == 'java'
  RSpec.feature 'Charts Feature Spec', js: true do
    before do
      # Measurement::Base.client.send(:clean)

      create_user_with_all_abilities
      login_as(@user, scope: :user)
      gateway_category = create(:gateway_category)
      gateway_series = create(:gateway_8300_model, category: gateway_category)
      @gateway = create(:gateway_device, category: gateway_category, series: gateway_series, organization: @organization)
      create(:device_modem, device: @gateway)
    end

    after do
      # Measurement::Base.client.send(:clean)
    end

    describe 'Cellular Utilization Chart' do
      before do
        @carrier_1 = Measurement::CellularUtilization.create(
          carrier: carrier_1_name,
          mac: @gateway.mac,
          received: Faker::Number.number(4).to_i,
          transmitted: Faker::Number.number(3).to_i
        )

        @carrier_2 = Measurement::CellularUtilization.create(
          carrier: carrier_2_name,
          mac: @gateway.mac,
          received: Faker::Number.number(4).to_i,
          transmitted: Faker::Number.number(3).to_i
        )

        sleep 10 # TODO: Remove this when Druid can be used synchronously
      end

      let(:carrier_1_name) { 'a' + Faker::Lorem.characters(6) }
      let(:carrier_2_name) { 'z' + Faker::Lorem.characters(6) }

      it 'displays the correct number of points when different intervals are selected' do
        visit device_path(@gateway)

        click_link('Cellular Details')
        wait_for_ajax

        # Carrier 1
        page.find('.highcharts-title').assert_text("Cellular Utilization for #{carrier_1_name}")
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 24)

        page.select('Last 7 Days', from: 'Range')
        wait_for_ajax
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 14)

        page.select('Last 30 Days', from: 'Range')
        wait_for_ajax
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 30)

        # Carrier 2
        page.select(carrier_2_name, from: 'Carrier')
        wait_for_ajax

        page.find('.highcharts-title').assert_text("Cellular Utilization for #{carrier_2_name}")
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 30)

        page.select('Last 7 Days', from: 'Range')
        wait_for_ajax
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 14)

        page.select('Last 24 Hours', from: 'Range')
        wait_for_ajax
        page.first('.cellular-utilization.chart .highcharts-markers').assert_selector('path', count: 24)
      end
    end

    describe 'WAN Utilization Chart' do
      before do
        @future = Measurement::WanUtilization.create(
          mac: @gateway.mac,
          interface: 'eth1',
          upload: Faker::Number.number(2).to_i,
          upload_hwm: Faker::Number.number(3).to_i,
          download: Faker::Number.number(3).to_i,
          download_hwm: Faker::Number.number(4).to_i
        )

        sleep 10 # TODO: Remove this when Druid can be used synchronously
      end

      scenario 'displays WAN Utilization chart' do
        visit device_path(@gateway)
        wait_for_ajax
        page.find('.wan-utilization-charts .title').assert_text('WAN Utilization')
        page.first('.upload .highcharts-markers').assert_selector('path', count: 24)

        page.select('Last 7 Days', from: 'Range')
        wait_for_ajax
        page.first('.upload .highcharts-markers').assert_selector('path', count: 14)

        # TODO: Refactor these tests AV-2609
        page.select('Last 30 Days', from: 'Range')
        wait_for_ajax
        # page.first('.upload .highcharts-markers').assert_selector('path', count: 30)
      end
    end
  end
end
