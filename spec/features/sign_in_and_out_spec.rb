# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Sign in and out' do
  it 'When a non logged in user visits the root url, they redirect to the sign in page' do
    create_user
    logout :user
    visit root_path
    expect(current_path).to eq new_user_session_path
  end

  it 'When a user signs in, they redirect to the root url' do
    create_user
    visit new_user_session_path
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_on 'Login'
    expect(current_path).to eq root_path
  end

  context 'with invalid utf-8 data' do
    before do
      create_user
      logout :user
    end

    it 'does not throw an exception when signing in using GET' do
      expect do
        visit '/users/sign_in?user[email]=%FF%FE%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%32%30%33%29%3C%2F%73%63%72%69%70%74%3E'
      end.not_to raise_error
    end

    it 'does not throw an exception when signing in using POST' do
      visit new_user_session_path
      fill_in 'user_email', with: '%FF%FE%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%32%30%33%29%3C%2F%73%63%72%69%70%74%3E'
      fill_in 'user_password', with: "\xBF'\xBF\""
      expect do
        click_button 'Login'
      end.not_to raise_error
    end

    it 'does not throw an exception when passed as the controller action' do
      expect do
        visit '/users/%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AF%C0%AE%C0%AE%C0%AFwindows%C0%AFwin.ini'
      end.not_to raise_error
    end
  end
end
