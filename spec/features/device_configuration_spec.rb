# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device configuration', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    dial_to_ip_category = create(:dial_to_ip_category)
    dial_to_ip_default = create(:dial_to_ip_5300_DC_model, category: dial_to_ip_category)
    firmware = create(:device_firmware, :with_real_schema, device_model: dial_to_ip_default, version: '16.1.100')
    @group_configuration = create(:device_configuration, :with_basic_settings,
                                  organization: @organization, device_firmware: firmware)
    @device = create(:dial_to_ip_device, category: dial_to_ip_category, series: dial_to_ip_default,
                                         organization: @organization, configuration: @group_configuration)
  end

  it 'Disabling remote control on the group configuration forces direct commands' do
    visit edit_device_configuration_path(@group_configuration)
    find('.field-name', text: 'Services').click
    remote_control = find('.field-name', text: 'Remote control').find(:xpath, './../..')
    remote_control.click
    remote_control.find(:xpath, '//input[@title="Enable"]').click
    find('#save-device-configuration').click

    visit device_path(@device, anchor: :configuration)
    wait_for_page_to_load
    find('#save-device-configuration').click

    page.evaluate_script('window.location.reload()')
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    find('.sub-commands', text: 'Device').hover
    find('.send_command', text: 'Check Status').click
    expect(page).to have_css('#sendCommandModal')
  end

  it 'Changing remote endpoint hostname on the group configuration forces direct commands' do
    visit edit_device_configuration_path(@group_configuration)
    find('.field-name', text: 'IPsec').click
    remote_endpoint_container = find('.field-name', text: 'Accelerated View').find(:xpath, './../..')
    remote_endpoint_container.click
    remote_endpoint = remote_endpoint_container.find('.field-name', text: 'Remote endpoint').find(:xpath, './../..')
    remote_endpoint.click
    remote_endpoint.find(:xpath, '//input[@title="Hostname"]').set('null.accns.com')
    find('#save-device-configuration').click

    visit device_path(@device, anchor: :configuration)
    wait_for_page_to_load
    find('#save-device-configuration').click

    page.evaluate_script('window.location.reload()')
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    find('.sub-commands', text: 'Device').hover
    find('.send_command', text: 'Check Status').click
    expect(page).to have_css('#sendCommandModal')
  end

  it 'Enabling remote control on the individual configuration allows proxied commands' do
    visit edit_device_configuration_path(@group_configuration)
    find('.field-name', text: 'Services').click
    remote_control = find('.field-name', text: 'Remote control').find(:xpath, './../..')
    remote_control.click
    remote_control.find(:xpath, '//input[@title="Enable"]').click
    find('#save-device-configuration').click

    visit device_path(@device, anchor: :configuration)
    find('.field-name', text: 'Services').click
    remote_control = find('.field-name', text: 'Remote control').find(:xpath, './../..')
    remote_control.click
    remote_control.find(:css, '.field-single.first').find('.dropdown-toggle').click
    remote_control.find('.field-override').click
    remote_control.find(:xpath, '//input[@title="Enable"]').click
    find('#save-device-configuration').click

    page.evaluate_script('window.location.reload()')
    wait_for_page_to_load
    wait_for_ajax
    find('#device-commands-menu').click
    find('.sub-commands', text: 'Device').hover
    find('.send_command', text: 'Check Status').click
    wait_for_page_to_load
    expect(current_path).to eq(events_device_path(@device))
    visit device_path(@device, anchor: :configuration) # do not end on the events page
  end
end
