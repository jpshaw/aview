# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device show page', :vcr, js: true do
  context 'when an ARMT controller is logged in' do
    before do
      create_armt_controller_user
      login_as(@user, scope: :user)
      create(:country)
      cellular_category = create(:cellular_category)
      cellular_6300_CX_model = create(:cellular_6300_CX_model, category: cellular_category)
      @cellular_device = create(:cellular_device, category: cellular_category, series: cellular_6300_CX_model, organization: @organization)
      gateway_category = create(:gateway_category)
      gateway_8200_model = create(:gateway_8200_model, category: gateway_category)
      @gateway_device = create(:gateway_device, category: gateway_category, series: gateway_8200_model, hw_version: 'default', organization: @organization)
    end

    it 'The ARMT controller can modify the location of a non-gateway device' do
      visit device_path(@cellular_device, anchor: :location_tab)
      fill_in('location_address_1', with: '1120 E Kennedy Blvd')
      fill_in('location_city', with: 'Tampa')
      fill_in('location_state_abbr', with: 'FL')
      fill_in('location_postal_code', with: '33602')
      find('#select2-location_country_id-container').click
      find('.select2-results__option', text: 'United States').click
      find('#location_submit_button').click
      wait_for_ajax
      expect(page).to have_text("Successfully updated this device's location")
    end

    it 'The ARMT controller cannot modify the location of a gateway device' do
      visit device_path(@gateway_device, anchor: :location_tab)
      expect(page).to have_no_css('#update-device-location')
    end
  end

  context 'when a user with all abilities is logged in' do
    before do
      create_user_with_all_abilities
      login_as(@user, scope: :user)
      cellular_category = create(:cellular_category)
      cellular_6300_CX_model = create(:cellular_6300_CX_model, category: cellular_category)
      @cellular_device = create(:cellular_device, category: cellular_category, series: cellular_6300_CX_model, organization: @organization)
      @new_organization = create(:sub_organization, name: 'AT&T')
    end

    it 'The organization can be changed for a non-gateway device' do
      visit device_path(@cellular_device, anchor: :settings_tab)
      click_on 'Change Organization'
      find('#select2-device_organization_selection-container').click
      find('.select2-results__option', text: 'AT&T').click
      wait_for_ajax
      find('#move-device-to-organization-submit').click
      wait_for_ajax
      expect(page).to have_text('Successfully moved device to AT&T')
      expect(page).to have_selector('#device-organization-name', text: 'AT&T')
    end
  end
end
