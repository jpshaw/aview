# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Advanced Search Feature', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  context 'Device' do
    context 'MAC' do
      before do
        @unmatched_device = create(:device, organization: @organization, mac: '1' * 12)
        @contains_match_device = create(:device, organization: @organization, mac: match_prefix + '1')
        @exact_match_device = create(:device, organization: @organization, mac: match_prefix + '2')
      end

      let(:match_prefix) { '0' * 11 }

      it 'contains' do
        visit new_search_path
        fill_in 'device-search-value', with: match_prefix
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        fill_in 'device-search-value', with: @exact_match_device.mac
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'IP Address' do
      before do
        @unmatched_device = create(:test_device, organization: @organization, host: '2.2.2.2')
        @contains_match_device = create(:test_device, organization: @organization, host: match_prefix + '.0')
        @exact_match_device = create(:test_device, organization: @organization, host: match_prefix + '.1')
      end

      let(:match_prefix) { '1.1.1' }

      it 'contains' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: I18n.t(:primary_ip)).click
        fill_in 'device-search-value', with: match_prefix
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: I18n.t(:primary_ip)).click
        fill_in 'device-search-value', with: @exact_match_device.host
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'Firmware' do
      before do
        @unmatched_device = create(:test_device, organization: @organization, firmware: '14.0.0')
        @contains_match_device = create(:test_device, organization: @organization, firmware: match_prefix + '.0')
        @exact_match_device = create(:test_device, organization: @organization, firmware: match_prefix + '.1')
      end

      let(:match_prefix) { '15.3' }

      it 'contains' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: 'Firmware').click
        fill_in 'device-search-value', with: match_prefix
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: 'Firmware').click
        fill_in 'device-search-value', with: @exact_match_device.firmware
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'Serial' do
      before do
        @unmatched_device = create(:test_device, organization: @organization, serial: '0' * 16)
        @contains_match_device = create(:test_device, organization: @organization, serial: match_prefix + '6')
        @exact_match_device = create(:test_device, organization: @organization, serial: match_prefix + '7')
      end

      let(:match_prefix) { '1' * 15 }

      it 'contains' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: 'Serial').click
        fill_in 'device-search-value', with: match_prefix
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device-search-column-container').click
        find('.select2-results__option', text: 'Serial').click
        fill_in 'device-search-value', with: @exact_match_device.serial
        find('#device-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end
  end

  context 'Modem' do
    context 'Phone Number' do
      before do
        @unmatched_device = create(:netbridge_device, organization: @organization)
        @contains_match_device = create(:netbridge_device, organization: @organization)
        @exact_match_device = create(:netbridge_device, organization: @organization)
        create(:device_modem, device: @unmatched_device, number: '18133334444')
        create(:device_modem, device: @contains_match_device, number: match_prefix + '4')
        create(:device_modem, device: @exact_match_device, number: match_prefix + '5')
      end

      let(:match_prefix) { '1727333444' }

      it 'contains' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'Phone Number').click
        fill_in 'device_modem-search-value', with: match_prefix
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'Phone Number').click
        fill_in 'device_modem-search-value', with: @exact_match_device.modem.number
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'ICCID' do
      before do
        @unmatched_device = create(:netbridge_device, organization: @organization)
        @contains_match_device = create(:netbridge_device, organization: @organization)
        @exact_match_device = create(:netbridge_device, organization: @organization)
        create(:device_modem, device: @unmatched_device, iccid: '89014103276182451200')
        create(:device_modem, device: @contains_match_device, iccid: match_prefix + '1')
        create(:device_modem, device: @exact_match_device, iccid: match_prefix + '2')
      end

      let(:match_prefix) { '8901410327618245126' }

      it 'contains' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'ICCID').click
        fill_in 'device_modem-search-value', with: match_prefix
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'ICCID').click
        fill_in 'device_modem-search-value', with: @exact_match_device.modem.iccid
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'IMEI' do
      before do
        @unmatched_device = create(:netbridge_device, organization: @organization)
        @contains_match_device = create(:netbridge_device, organization: @organization)
        @exact_match_device = create(:netbridge_device, organization: @organization)
        create(:device_modem, device: @unmatched_device, imei: '012615001358600')
        create(:device_modem, device: @contains_match_device, imei: match_prefix + '1')
        create(:device_modem, device: @exact_match_device, imei: match_prefix + '2')
      end

      let(:match_prefix) { '01261500135864' }

      it 'contains' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'IMEI').click
        fill_in 'device_modem-search-value', with: match_prefix
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'IMEI').click
        fill_in 'device_modem-search-value', with: @exact_match_device.modem.imei
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'IMSI' do
      before do
        @unmatched_device = create(:netbridge_device, organization: @organization)
        @contains_match_device = create(:netbridge_device, organization: @organization)
        @exact_match_device = create(:netbridge_device, organization: @organization)
        create(:device_modem, device: @unmatched_device, imsi: '310410618245100')
        create(:device_modem, device: @contains_match_device, imsi: match_prefix + '1')
        create(:device_modem, device: @exact_match_device, imsi: match_prefix + '2')
      end

      let(:match_prefix) { '31041061824512' }

      it 'contains' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'IMSI').click
        fill_in 'device_modem-search-value', with: match_prefix
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'IMSI').click
        fill_in 'device_modem-search-value', with: @exact_match_device.modem.imsi
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'ESN' do
      before do
        @unmatched_device = create(:netbridge_device, organization: @organization)
        @contains_match_device = create(:netbridge_device, organization: @organization)
        @exact_match_device = create(:netbridge_device, organization: @organization)
        create(:device_modem, device: @unmatched_device, esn: '09610549300')
        create(:device_modem, device: @contains_match_device, esn: match_prefix + '1')
        create(:device_modem, device: @exact_match_device, esn: match_prefix + '2')
      end

      let(:match_prefix) { '0961054931' }

      it 'contains' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'ESN').click
        fill_in 'device_modem-search-value', with: match_prefix
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-device_modem-search-column-container').click
        find('.select2-results__option', text: 'ESN').click
        fill_in 'device_modem-search-value', with: @exact_match_device.modem.esn
        find('#device_modem-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end
  end

  context 'Site' do
    context 'Name' do
      before do
        unmatched_site = create(:site, organization: @organization, name: 'Site A')
        contains_match_site = create(:site, organization: @organization, name: match_prefix + '1')
        exact_match_site = create(:site, organization: @organization, name: match_prefix + '2')
        @unmatched_device = create(:test_device, organization: @organization, site: unmatched_site)
        @contains_match_device = create(:test_device, organization: @organization, site: contains_match_site)
        @exact_match_device = create(:test_device, organization: @organization, site: exact_match_site)
      end

      let(:match_prefix) { 'Site B' }

      it 'contains' do
        visit new_search_path
        find('#select2-site-search-column-container').click
        find('.select2-results__option', text: 'Name').click
        fill_in 'site-search-value', with: match_prefix
        find('#site-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-site-search-column-container').click
        find('.select2-results__option', text: 'Name').click
        fill_in 'site-search-value', with: @exact_match_device.site.name
        find('#site-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end
  end

  context 'Location' do
    context 'City' do
      before do
        unmatched_location = create(:location, city: 'Sydney')
        contains_match_location = create(:location, city: match_prefix + 'Francisco')
        exact_match_location = create(:location, city: match_prefix + 'Bernandino')
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { 'San ' }

      it 'contains' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'City').click
        fill_in 'location-search-value', with: match_prefix
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'City').click
        fill_in 'location-search-value', with: @exact_match_device.location.city
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'State' do
      before do
        unmatched_location = create(:location, state: 'Florida')
        contains_match_location = create(:location, state: match_prefix + 'Mexico')
        exact_match_location = create(:location, state: match_prefix + 'Arizona')
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { 'New ' }

      it 'contains' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        all('.select2-results__option', text: 'State').at(0).click
        fill_in 'location-search-value', with: match_prefix
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        all('.select2-results__option', text: 'State').at(0).click
        fill_in 'location-search-value', with: @exact_match_device.location.state
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'State Abbreviation' do
      before do
        unmatched_location = create(:location, state_abbr: 'FL')
        contains_match_location = create(:location, state_abbr: match_prefix + 'M')
        exact_match_location = create(:location, state_abbr: match_prefix + 'A')
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { 'N' }

      it 'contains' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'State Abbreviation').click
        fill_in 'location-search-value', with: match_prefix
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'State Abbreviation').click
        fill_in 'location-search-value', with: @exact_match_device.location.state_abbr
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'Postal Code' do
      before do
        unmatched_location = create(:location, postal_code: '33777')
        contains_match_location = create(:location, postal_code: match_prefix + '77')
        exact_match_location = create(:location, postal_code: match_prefix + '78')
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { '333' }

      it 'contains' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'Postal Code').click
        fill_in 'location-search-value', with: match_prefix
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-location-search-column-container').click
        find('.select2-results__option', text: 'Postal Code').click
        fill_in 'location-search-value', with: @exact_match_device.location.postal_code
        find('#location-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end
  end

  context 'Country' do
    context 'Name' do
      before do
        unmatched_country = create(:country, code: 'CA', name: 'Mexico')
        contains_match_country = create(:country, code: 'AM', name: match_prefix + 'merica')
        exact_match_country = create(:country, code: 'AN', name: match_prefix + 'ntarctica')
        unmatched_location = create(:location, country: unmatched_country)
        contains_match_location = create(:location, country: contains_match_country)
        exact_match_location = create(:location, country: exact_match_country)
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { 'A' }

      it 'contains' do
        visit new_search_path
        find('#select2-country-search-column-container').click
        find('.select2-results__option', text: 'Name').click
        fill_in 'country-search-value', with: match_prefix
        find('#country-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-country-search-column-container').click
        find('.select2-results__option', text: 'Name').click
        fill_in 'country-search-value', with: @exact_match_device.location.country.name
        find('#country-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end

    context 'Code' do
      before do
        unmatched_country = create(:country, name: 'Canada', code: 'MX')
        contains_match_country = create(:country, name: 'America', code: match_prefix + 'M')
        exact_match_country = create(:country, name: 'Antarctica', code: match_prefix + 'N')
        unmatched_location = create(:location, country: unmatched_country)
        contains_match_location = create(:location, country: contains_match_country)
        exact_match_location = create(:location, country: exact_match_country)
        @unmatched_device = create(:test_device, organization: @organization, location: unmatched_location)
        @contains_match_device = create(:test_device, organization: @organization, location: contains_match_location)
        @exact_match_device = create(:test_device, organization: @organization, location: exact_match_location)
      end

      let(:match_prefix) { 'A' }

      it 'contains' do
        visit new_search_path
        find('#select2-country-search-column-container').click
        find('.select2-results__option', text: 'Code').click
        fill_in 'country-search-value', with: match_prefix
        find('#country-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end

      it 'equals' do
        visit new_search_path
        find('#select2-country-search-column-container').click
        find('.select2-results__option', text: 'Code').click
        fill_in 'country-search-value', with: @exact_match_device.location.country.code
        find('#country-search-submit').click
        wait_for_page_to_load
        expect(current_path).to eq searches_path
        expect(page).not_to have_text(@unmatched_device.mac)
        expect(page).not_to have_text(@contains_match_device.mac)
        expect(page).to have_text(@exact_match_device.mac)
      end
    end
  end
end
