# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device index page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
    create_all_categories_and_models
    @gateway = create(:gateway_device, category: @gateway_category, series: @gateway_8200, hw_version: 'default', organization: @organization)
    @gateway_8300_device = create(:gateway_device, category: @gateway_category, series: @gateway_8300, hw_version: '8300', organization: @organization)
    @netreach = create(:netreach_device, category: @netreach_category, series: @netreach_4200_NX, organization: @organization)
    @remote_manager = create(:remote_manager_device, category: @remote_manager_category, series: @remote_manager_5400_RM, organization: @organization)
    @dial_to_ip = create(:dial_to_ip_device, category: @dial_to_ip_category, series: @dial_to_ip_5300_DC, organization: @organization)
  end

  it 'The device index page shows all devices' do
    visit devices_path
    expect(page).to have_text(@gateway.mac)
    expect(page).to have_text(@gateway_8300_device.mac)
    expect(page).to have_text(@dial_to_ip.mac)
    expect(page).to have_text(@netreach.mac)
    expect(page).to have_text(@remote_manager.mac)
  end

  it 'The gateway index page only shows gateway devices' do
    visit vpn_gateway_devices_path
    expect(page).to have_text(@gateway.mac)
    expect(page).to have_text(@gateway_8300_device.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
    expect(page).not_to have_text(@netreach.mac)
    expect(page).not_to have_text(@remote_manager.mac)
  end

  it 'The dial to ip index page only shows dial to ip devices' do
    visit dial_to_ip_devices_path
    expect(page).not_to have_text(@gateway.mac)
    expect(page).not_to have_text(@gateway_8300_device.mac)
    expect(page).to have_text(@dial_to_ip.mac)
    expect(page).not_to have_text(@netreach.mac)
    expect(page).not_to have_text(@remote_manager.mac)
  end

  it 'The remote manager index page only shows remote manager devices' do
    visit remote_manager_devices_path
    expect(page).not_to have_text(@gateway.mac)
    expect(page).not_to have_text(@gateway_8300_device.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
    expect(page).not_to have_text(@netreach.mac)
    expect(page).to have_text(@remote_manager.mac)
  end

  it 'The netreach index page only shows netreach devices' do
    visit wifi_devices_path
    expect(page).not_to have_text(@gateway.mac)
    expect(page).not_to have_text(@gateway_8300_device.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
    expect(page).to have_text(@netreach.mac)
    expect(page).not_to have_text(@remote_manager.mac)
  end

  it 'The visual search bar filters by category and model' do
    visit devices_path
    find('#visual-search-bar').click
    find('.VS-interface').find('li', text: 'category').click
    all('.VS-interface')[1].find('li', text: 'Gateway').click
    all('.VS-interface')[1].find('li', text: 'model').click
    all('.VS-interface')[2].find('li', text: '8300').click
    expect(page).not_to have_text(@gateway.mac)
    expect(page).to have_text(@gateway_8300_device.mac)
    expect(page).not_to have_text(@dial_to_ip.mac)
    expect(page).not_to have_text(@netreach.mac)
    expect(page).not_to have_text(@remote_manager.mac)
  end
end
