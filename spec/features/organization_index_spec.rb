# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Organizations index page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  it 'Organizations index page loads correctly' do
    visit organizations_path
    expect(page).to have_css('#page-title-header', text: 'Organizations')
  end
end
