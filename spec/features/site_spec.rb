# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Site Feature', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
    @site1 = create(:site, organization: @organization, name: '6.1.200+ BUG SPEC 3569')
    @matching_device = create(:test_device, organization: @organization, site: @site1)
    @nonmatching_device = create(:test_device, organization: @organization)
  end

  it 'Clicking a Site Name only shows devices belonging to Site' do
    visit sites_path
    expect(page).to have_text(@site1.name)
    click_animated_element(find_link(@site1.name))

    wait_for_ajax
    expect(page).to have_text(@matching_device.mac)
    expect(page).not_to have_text(@nonmatching_device.mac)
    expect(page).to have_css('#page-title-header', text: '6.1.200+ BUG SPEC 3569')
  end
end
