# frozen_string_literal: true

require 'rails_helper'
require 'cgi'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Gateway Tunnel Commands', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @gateway = create(:gateway_device_with_configuration, :commandable, :with_tunnel, organization: @organization)
  end

  it 'Gateway named tunnel commands are built correctly' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    @gateway.tunnels.each do |tunnel|
      tunnel_menu = find('.sub-commands', text: /#{tunnel.ipsec_iface}/i)
      tunnel_menu.hover
      %w[tunnel_bounce tunnel_status tunnel_down].each do |command|
        link = tunnel_menu.find('a', text: command.split('_')[1].capitalize)
        link_href = URI.decode(link[:href])
        params = CGI.parse(link_href.split('?')[1])
        expect(params['remote_data[index]']).to include(tunnel.ipsec_index.to_s)
        expect(params['remote_data[command]']).to include(command)
      end
    end
  end

  it 'Gateway named tunnel commands are built correctly for mangement tunnel' do
    @gateway.tunnels[0].mode = 4
    @gateway.tunnels[0].save
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    find('#device-commands-menu').click
    @gateway.tunnels.each do |tunnel|
      tunnel_menu = find('.sub-commands', text: /#{tunnel.ipsec_iface}/i)
      tunnel_menu.hover
      %w[tunnel_bounce tunnel_status].each do |command|
        link = tunnel_menu.find('a', text: command.split('_')[1].capitalize)
        link_href = URI.decode(link[:href])
        params = CGI.parse(link_href.split('?')[1])
        expect(params['remote_data[index]']).to include(tunnel.ipsec_index.to_s)
        expect(params['remote_data[command]']).to include(command)
      end
      expect(tunnel_menu).not_to have_selector('a', text: 'Down')
    end
  end
end
