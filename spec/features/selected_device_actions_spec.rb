# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Selected device actions', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
  end

  describe 'Move to data plan' do
    before do
      create_gateway_category_and_models
      @child_organization = create(:sub_organization)
      @data_plan = create(:data_plan, organization: @child_organization)
      @gateway = create(:gateway_device, category: @gateway_category, series: @gateway_8200,
                                         hw_version: 'default', organization: @organization)
      @gateway_8300_device = create(:gateway_device, category: @gateway_category, series: @gateway_8300,
                                                     hw_version: '8300', organization: @child_organization)
    end

    it 'from the device index page with no site organization filter set' do
      visit devices_path

      # there should be only 1 of these, however, data tables makes a duplicate of the table header and "hides"
      # it from everyone except poltergeist
      click_animated_element(all('#data-table-select-all')[0])
      click_animated_element(find('#selected-devices-dropdown'))
      find('#move-to-data-plan-action').click
      find('#select2-organization_id-container').click
      find('.select2-results__option', text: @child_organization.name).click
      wait_for_ajax

      # the following selector is necessary because the select2 box does not have a default option other than blank
      # if it did, it would get the select2-data-plans-drop-container ID like the organization dropdown has
      find(:xpath, "//span[@aria-labelledby='select2-data-plans-drop-container']").click
      find('.select2-results__option', text: @data_plan.name).click
      find('#submit-move-devices-to-data-plan').click

      expect(page).to have_text('Could not assign the data plan to 1 device. Successfully assigned 1 device.')
      @gateway_8300_device.reload
      expect(@gateway_8300_device.data_plan).to eq @data_plan
    end
  end
end
