# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device events page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
    gateway_category = create(:gateway_category)
    gateway_series = create(:gateway_8300_model, category: gateway_category)
    @gateway = create(:gateway_device, category: gateway_category, series: gateway_series, organization: @organization)

    dial_to_ip_category = create(:dial_to_ip_category)
    dial_to_ip_default = create(:dial_to_ip_5300_DC_model, category: dial_to_ip_category)
    dial_to_ip = create(:dial_to_ip_device, category: dial_to_ip_category, series: dial_to_ip_default, organization: @organization)

    @event = create(:device_event, device: @gateway, information: 'test_1')
    @event_2 = create(:device_event, device: dial_to_ip, information: 'test_2')
  end

  it 'The device event page shows only the devices events' do
    visit events_device_path(@gateway)
    wait_for_ajax
    expect(page).to have_text(@event.information)
    expect(page).not_to have_text(@event_2.information)
    visit device_path(@gateway) # this is one way to stop the auto polling that causes problems on db tear down
  end
end
