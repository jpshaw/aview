# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Feedback', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    AdminSettings.support_contact_list = %w[blah@blah.com]
  end

  it 'Send disabled if no message' do
    visit devices_path
    wait_for_page_to_load
    wait_for_ajax
    find('#open-feedback').click
    find('.modal.inmodal.fade.in').click
    expect(page).to have_selector('.modal-header', text: 'Contact Us')
    expect(page).to have_button('Submit', disabled: true)
  end

  it 'Send with message' do
    visit devices_path
    wait_for_page_to_load
    wait_for_ajax
    find('#open-feedback').click
    expect(page).to have_selector('.modal-header', text: 'Contact Us')
    first('textarea').set('Test text')
    find('.modal.inmodal.fade.in').click
    expect(first('textarea').value).to eq('Test text')
    find('.btn-primary', text: 'Submit').click
    expect(page).not_to have_selector('.modal-header', text: 'Contact Us')
  end
end
