# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Users index page', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
  end

  it 'Users page loads correctly' do
    visit users_path
    expect(page).to have_css('#page-title-header', text: 'Users')
  end
end
