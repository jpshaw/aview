# frozen_string_literal: true

require 'rails_helper'
require 'cgi'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Gateway Tunnel Status', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @category = create(:gateway_category)
    @gateway = create(:gateway_device_with_configuration, :support_tunnel_status, :with_tunnel, organization: @organization, category: @category)
    @gateway.details.tunnels_status = 0
    @gateway.details.tunnels_total_count = 5
    @gateway.details.tunnels_total_up = 3
    @gateway.details.save
  end

  it 'Gateway with new status fields enabled displays correctly for online state' do
    visit 'devices/vpn_gateway'
    wait_for_page_to_load
    wait_for_ajax
    row = find('tr', text: @gateway.mac)
    expect(row).to have_selector('.badge-success', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
    visit device_path(@gateway, anchor: :device_details_tab)
    wait_for_page_to_load
    expect(page).to have_selector('.status-up', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
  end

  it 'Gateway with new status fields enabled displays correctly for offline state' do
    @gateway.details.tunnels_status = 10
    @gateway.details.tunnels_total_count = 5
    @gateway.details.tunnels_total_up = 0
    @gateway.details.save
    visit 'devices/vpn_gateway'
    wait_for_page_to_load
    wait_for_ajax
    row = find('tr', text: @gateway.mac)
    expect(row).to have_selector('.badge-danger', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
    visit device_path(@gateway, anchor: :device_details_tab)
    wait_for_page_to_load
    expect(page).to have_selector('.status-down', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
  end

  it 'Gateway with new status fields enabled displays correctly for warning state' do
    @gateway.details.tunnels_status = 5
    @gateway.details.tunnels_total_count = 5
    @gateway.details.tunnels_total_up = 2
    @gateway.details.save
    visit 'devices/vpn_gateway'
    wait_for_page_to_load
    wait_for_ajax
    row = find('tr', text: @gateway.mac)
    expect(row).to have_selector('.badge-warning', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
    visit device_path(@gateway, anchor: :device_details_tab)
    wait_for_page_to_load
    expect(page).to have_selector('.status-notice', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
  end

  it 'Gateway below firmware threshold does not display new status' do
    @gateway.firmware = '6.9.9'
    @gateway.save
    visit 'devices/vpn_gateway'
    wait_for_page_to_load
    wait_for_ajax
    row = find('tr', text: @gateway.mac)
    expect(row).not_to have_selector('.badge', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
    visit device_path(@gateway, anchor: :device_details_tab)
    wait_for_page_to_load
    expect(page).not_to have_selector('td', text: "#{@gateway.details.tunnels_total_up} / #{@gateway.details.tunnels_total_count}")
  end
end
