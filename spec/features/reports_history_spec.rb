# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Reports history page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  it 'Reports history page loads correctly' do
    visit reports_path
    expect(page).to have_css('#page-title-header', text: 'Report History')
  end
end
