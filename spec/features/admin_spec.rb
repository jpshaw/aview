# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Admin page', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    Settings.admin = Config::Options.new(enabled: true)
    allow_any_instance_of(AdminController).to receive(:authorize_admin_user!).and_return(true)
  end

  after do
    Settings.reload!
  end

  it 'Admin page loads correctly' do
    visit admin_path
    expect(page).to have_css('#page-title-header', text: 'Admin Panel')
  end

  it 'Create a DNS Domain' do
    visit admin_path(anchor: 'dns')
    click_on('Create')
    wait_for_ajax
    fill_in('dns_entry_name', with: 'test.domain.net')
    find('#select2-dns_entry_type-container').click
    find('.select2-results__option', text: 'MASTER').click
    click_on('dns-domain-form-submit')
    wait_for_ajax
    expect(page).to have_text('Successfully created domain')
    all('#dns-nameserver-table tr:nth-child(1) > td').each do |td|
      expect([domain.name, 'MASTER', 'Edit', 'Delete']).to include td.text
    end
  end

  it 'Create a DNS SOA' do
    domain = create(:dns_domain)
    visit admin_path(anchor: 'dns')
    click_on('SOA')
    click_on('Create')
    wait_for_ajax
    fill_in('dns_entry_name', with: 'Test SOA')
    fill_in('dns_entry_content', with: 'Test Content')
    click_on('dns-record-form-submit')
    wait_for_ajax
    expect(page).to have_text('Successfully created soa')
    all('#dns-soa-table tr:nth-child(1) > td').each do |td|
      expect(['Test SOA', domain.name, 'Test Content', '30', 'Edit', 'Delete']).to include td.text
    end
  end

  it 'Create a Nameserver' do
    domain = create(:dns_domain)
    visit admin_path(anchor: 'dns')
    click_on('Nameservers')
    click_on('Create')
    wait_for_ajax
    fill_in('dns_entry_name', with: 'Test Nameserver')
    fill_in('dns_entry_content', with: 'Test Content')
    click_on('dns-record-form-submit')
    wait_for_ajax
    expect(page).to have_text('Successfully created nameserver')
    all('#dns-nameserver-table tr:nth-child(1) > td').each do |td|
      expect(['Test Nameserver', domain.name, 'Test Content', '30', 'Edit', 'Delete']).to include td.text
    end
  end
end
