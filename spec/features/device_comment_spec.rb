# frozen_string_literal: true

require 'rails_helper'
require 'cgi'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Device Comment', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @category = create(:gateway_category)
    @gateway = create(:gateway_device_with_configuration, organization: @organization, category: @category)
    @test_comment = 'this is a comment'
  end

  it 'Correctly set comment and edit from both locations' do
    visit devices_path
    wait_for_page_to_load
    wait_for_ajax
    find('.config-link').click
    find('.i-checks', text: 'Comment').first('.icheckbox_square-green').click
    wait_for_ajax
    find('.config-link').click
    row = find('tr', text: @gateway.mac)
    row.find('.comment-icon').click
    find('.modal-body textarea').set(@test_comment)
    find('.modal.inmodal.fade.in').click
    wait_for_ajax
    expect(find('.modal-body textarea').value).to eq(@test_comment)
    find('.modal-footer .btn-primary').click
    wait_for_ajax
    expect(row).to have_selector('.comment-icon.active')
    visit device_path(@gateway, anchor: :device_details_tab)
    wait_for_page_to_load
    find('span', text: @test_comment).click
    find('.editable-input input').set('')
    find('.editable-submit').click
    visit devices_path
    wait_for_page_to_load
    wait_for_ajax
    find('.config-link').click
    find('.i-checks', text: 'Comment').first('.icheckbox_square-green').click
    wait_for_ajax
    find('.config-link').click
    row = find('tr', text: @gateway.mac)
    expect(row).not_to have_selector('.comment-icon.active')
  end
end
