# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Site wide organization filter', js: true do
  before do
    @organization = create(:organization)
    @sub_organization = create(:sub_organization)
    create_user
    login_as(@user, scope: :user)
  end

  it 'The site wide filter can be set' do
    visit root_path
    click_on 'site-wide-organization-filter'
    find('#select2-filter_organization_id-container').click
    find('.select2-results__option', text: @sub_organization.name).click
    click_on 'submit-organization-filter'
    expect(page).to have_text("Now filtering for '#{@sub_organization.name}'")
  end
end
