# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Notifications page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  it 'Notifications page loads correctly' do
    visit notifications_path
    expect(page).to have_css('#page-title-header', text: 'Notifications')
  end
end
