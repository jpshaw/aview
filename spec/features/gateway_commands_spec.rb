# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Gateway Commands', js: true do
  before do
    create_user_with_all_abilities
    login_as(@user, scope: :user)
    @gateway = create(:gateway_device_with_configuration, :commandable, organization: @organization)
  end

  it 'Gateway command links open correct modals' do
    visit device_path(@gateway, anchor: :location)
    wait_for_page_to_load
    wait_for_ajax
    page.evaluate_script('$.support.transition = false')
    modal_commands = [%w[Device cx_access],
                      %w[Networking arpping],
                      %w[Networking ping],
                      %w[Networking tracepath],
                      %w[Networking traceroute],
                      %w[Networking wan_toggle]]

    modal_commands.each do |params|
      find('#device-commands-menu').click
      find('.sub-commands', text: /#{params[0]}/i).hover
      title = if params[1] != 'cx_access'
                params[1].titleize
              else
                'router access'.titleize
              end
      option = all('.send_command', text: /#{title}/i).last
      title = option.text
      expect(option[:href]).to include("##{params[1]}")
      option.click
      find('.modal-title', text: "Send #{title} Command")
      find('.modal.inmodal.fade.in').click
      expect(find('.modal-title').text).to eq("Send #{title} Command")
      find('.close').click
    end
  end
end
