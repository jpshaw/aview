# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe 'Maps page', js: true do
  before do
    create_user
    login_as(@user, scope: :user)
  end

  it 'Maps page loads correctly' do
    visit maps_path
    expect(page).to have_css('#page-title-header', text: 'Maps')
  end
end
