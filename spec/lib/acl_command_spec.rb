# frozen_string_literal: true

require 'rails_helper'
require 'jasper/api/request'

describe AclCommand do
  before do
    allow(Settings).to receive_message_chain(:remote_control, :server, :name).and_return('ipsec.accns.com')
  end

  let(:netbridge) { create(:cellular_device, mac: '0027042c8ef2') }
  let(:netbridge2) { create(:cellular_device, mac: '002704123456') }

  describe '#send!' do
    context 'with no command passed' do
      it 'fails' do
        command = described_class.new(netbridge)
        command.send!
        expect(command.failed?).to eq true
      end
    end

    context 'with a regular command passed' do
      context 'that is unknown' do
        it 'fails' do
          command = described_class.new(netbridge, command: 'unknown_command')
          command.send!
          expect(command.failed?).to eq true
        end
      end

      context 'that is known' do
        context 'with an IP address for a reachable device' do
          before do
            allow(netbridge).to receive(:command_ip_addr).and_return('10.250.0.38')
            allow(netbridge2).to receive(:command_ip_addr).and_return('10.250.0.38')
            server = double('server')
            allow(TCPSocket).to receive(:open).and_return(server)
          end
          context 'the reachable device does not match the expected device' do
            it 'fails' do
              VCR.use_cassette('acl command to reachable device') do
                command = described_class.new(netbridge2, command: 'status')
                command.send!
                expect(command.failed?).to eq true
              end
            end
          end
          context 'the reachable device matches the expected device' do
            it 'succeeds' do
              VCR.use_cassette('acl command to reachable device') do
                command = described_class.new(netbridge, command: 'status')
                command.send!
                expect(command.succeeded?).to eq true
              end
            end
          end
        end
      end
    end

    context 'when using the Jasper API' do
      let(:modem) { create(:device_modem, device: netbridge) }
      let(:response) { '{:send_sms_response=>{:correlation_id=>"89011704252309057948", :version=>"5.90", :build=>"jasper_release_6.38-160308-159940", :timestamp=>Tue, 05 Apr 2016 20:01:09 +0000, :sms_msg_id=>"6410592001", :"@xmlns:ns2"=>"http://api.jasperwireless.com/ws/schema", :"@ns2:request_id"=>"9+TlCB0UajhNGuAg"}}' }
      let(:error_response) { '{:fault=>{:faultcode=>"SOAP-ENV:Server", :faultstring=>"100100", :detail=>{:request_id=>"hnY6xp+F3whihec5", :error=>"Terminal not found", :exception=>"com.jasperwireless.ws.ApiException", :message=>"Terminal not found"}}}' }

      before do
        allow(Settings).to receive_message_chain(:api_keys, :jasper, :enabled?).and_return(true)
        allow(Settings).to receive_message_chain(:api_keys, :jasper, :key).and_return('bogus_key')
        allow(Settings).to receive_message_chain(:api_keys, :jasper, :user).and_return('bogus_user')
        allow(Settings).to receive_message_chain(:api_keys, :jasper, :pass).and_return('bogus_password')
        allow(Settings).to receive_message_chain(:api_keys, :jasper, :base_uri).and_return('bogus_uri')
      end

      context 'with a sms command passed' do
        context 'with no iccid present' do
          it 'fails' do
            modem.update_attribute(:iccid, nil)
            command = described_class.new(netbridge, command: 'sms_reboot')
            command.send!
            expect(command.failed?).to eq true
          end
        end

        context 'with an iccid' do
          it 'succeeds' do
            expect(modem.iccid).not_to eq nil
            allow_any_instance_of(Jasper::API::Request).to receive(:deliver).and_return(response)
            command = described_class.new(netbridge, command: 'sms_reboot')
            command.send!
            expect(command.succeeded?).to eq true
          end
        end

        context 'with a invalid iccid' do
          it 'fails' do
            modem.update_attribute(:iccid, '12345678901')
            allow_any_instance_of(Jasper::API::Request).to receive(:deliver).and_raise(Savon::Error)
            command = described_class.new(netbridge, command: 'sms_reboot')
            command.send!
            expect(command.failed?).to eq true
          end
        end
      end
    end
  end
end
