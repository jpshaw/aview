# frozen_string_literal: true

require 'rails_helper'

describe CSVField do
  describe '#initialize' do
    it 'splits the comma separated string argument into an array' do
      csv_field = described_class.new
      expect(csv_field.__send__(:array)).to eq []

      word      = Faker::Lorem.word
      csv_field = described_class.new(word)
      expect(csv_field.__send__(:array)).to eq [word]

      words     = "#{Faker::Lorem.word},#{Faker::Lorem.word}"
      csv_field = described_class.new(words)
      expect(csv_field.__send__(:array)).to eq words.split(',')
    end
  end

  describe 'Instance methods' do
    describe '#<<' do
      it 'adds a value to the underlying array' do
        expect(subject.__send__(:array)).to eq []
        subject << 'a'
        subject << 'b'
        expect(subject.__send__(:array)).to eq %w[a b]
      end
    end

    describe '#to_s' do
      context 'when empty' do
        it 'returns an empty string' do
          expect(subject.to_s).to eq ''
        end
      end

      context 'with only one value' do
        before do
          subject << 'a'
        end

        it 'returns a string with no commas' do
          expect(subject.to_s).to eq 'a'
        end
      end

      context 'with multiple values' do
        before do
          subject << 'a'
          subject << 'b'
        end

        it 'returns a comma separated string' do
          expect(subject.to_s).to eq 'a,b'
        end
      end

      describe 'duplicates' do
        let(:without_duplicates)  { 'a,b,c' }
        let(:with_duplicates)     { "#{without_duplicates},b,a" }

        context 'when keeping duplicates' do
          let(:csv_field) { described_class.new(with_duplicates, keep_duplicates: true) }

          it 'keeps duplicate values' do
            expect(csv_field.to_s).to eq with_duplicates
          end
        end

        context 'when not keeping duplicates' do
          # Not passing the option defaults to false.
          let(:csv_field) { described_class.new(with_duplicates, keep_duplicates: false) }

          it 'removes duplicate values' do
            expect(csv_field.to_s).to eq without_duplicates
          end
        end
      end

      describe 'nils' do
        let(:string) { 'a,b,c' }

        context 'when keeping nils' do
          let(:csv_field) { described_class.new(string, keep_nils: true) }

          before do
            csv_field << nil
            csv_field << 'd'
          end

          let(:expected) { "#{string},,d" }

          it 'keeps nil values' do
            expect(csv_field.to_s).to eq expected
          end
        end

        context 'when not keeping nils' do
          # Not passing the option defaults to false.
          let(:csv_field) { described_class.new(string, keep_nils: false) }

          before do
            csv_field << nil
            csv_field << 'd'
          end

          let(:expected) { "#{string},d" }

          it 'removes nil values' do
            expect(csv_field.to_s).to eq expected
          end
        end
      end
    end
  end
end
