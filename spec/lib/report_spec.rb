# frozen_string_literal: true

require 'rails_helper'

describe Report do
  describe '#initialize' do
    it 'expects a parameter' do
      expect { described_class.new }.to raise_error(ArgumentError)
      expect { described_class.new(nil) }.not_to raise_error
    end
  end

  # README: This class is not meant to be instantiated directly so we are testing only that it
  #         responds to the inherited public interface and that if asked to generate a report it
  #         will raise an exception.
  context 'when instantiated' do
    subject(:report) { described_class.new(nil) }

    it_behaves_like 'a parameters repo for UI form fields'
    it_behaves_like 'a generic report'

    describe '#generate!' do
      it 'raises a NotImplementedError error' do
        expect { report.generate! }.to raise_error(NotImplementedError)
      end
    end
  end
end
