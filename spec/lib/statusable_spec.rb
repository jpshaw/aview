# frozen_string_literal: true

require 'rails_helper'

describe Statusable do
  include Devices
  before do
    create_device
  end

  # The newly-created device is initially deployed and up.
  context 'a device whose status is up' do
    describe '#status_class' do
      it 'returns the correct classes' do
        expect(@device.status_class).to eq('status-up')
      end
    end

    describe '#status_color' do
      it 'shows the correct status color' do
        expect(@device.status_color).to eq 'green'
      end
    end

    describe '#status_title' do
      it 'shows the correct status title' do
        expect(@device.status_title).to eq 'Up'
      end
    end

    describe '#status_description' do
      it 'shows the correct status description' do
        expect(@device.status_description).to eq 'Device is up'
      end
    end

    describe '#status_indicator' do
      it 'returns the correct classes' do
        expect(@device.status_indicator).to eq('fa fa-circle status-up')
      end
    end
  end
end
