# frozen_string_literal: true

require 'rails_helper'
require 'smx_data_mapper'

describe SmxDataMapper do
  context 'when a new object is created' do
    context 'methods are dynamically generated for each object' do
      let(:data_mapper) { described_class.new({}) }

      it 'creates getter methods for the objects' do
        SmxDataMapper::OBJECTS.each do |object_name|
          expect(data_mapper).to respond_to object_name
        end
      end

      it 'creates boolean methods for each object to indicate if it is new' do
        SmxDataMapper::OBJECTS.each do |object_name|
          expect(data_mapper).to respond_to "new_#{object_name}?"
        end
      end

      it 'sets the boolean flag for each new object to false' do
        SmxDataMapper::OBJECTS.each do |object_name|
          expect(data_mapper.send("new_#{object_name}?")).to be false
        end
      end
    end
  end

  context 'when valid data is passed' do
    let(:data_hash) do
      {
        customer_name: 'TEST COMPANY',

        device: {
          mac: '00D0CF000123',
          host: '1.2.3.4',
          firmware: '5.5.5',
          hw_version: '8200'
        },

        site: {
          name: 'SITE ABC'
        },

        account: 'ANR123',

        details: {
          active_conn_type: 1,
          wan_1_conn_type: 2,
          wan_1_circuit_type: 'IDSL',
          wan_1_cell_extender: '1',
          wan_2_cell_extender: 'N',
          dial_mode: 'A',
          dial_backup_enabled: true,
          dial_backup_type: 'B',
          dial_modem_type: 'C'
        },

        portal_config: {
          password: 'portal_password',
          ssl_password: 'ssl_password',
          ssl_port: 443,
          auth_user: 'authuser',
          auth_password: 'authpassword'
        },

        snmp_config: {
          version: '2c',
          community: 'armt-snmp'
        },

        contact: {
          name: 'TEST CONTACT'
        },

        location: {
          line_1: '1600 Pennsylvania Ave NW',
          city: 'Washington',
          state: 'DC',
          postal: '20500',
          country: 'US'
        }
      }
    end

    describe '.customer' do
      it 'will create a new customer if it is not in the database' do
        expect do
          described_class.new(data_hash).customer
        end.to change(Organization, :count).by(1)
      end

      it 'will get a customer if it exists in the database' do
        organization = create(
          :random_regular_organization,
          import_name: 'TEST COMPANY',
          name:        'Test Company'
        )

        data = described_class.new(data_hash)
        expect(data.customer).to eq organization
      end
    end

    describe '.device' do
      it 'will create a new device if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.device).not_to be nil
        expect(data.device.mac).to eq '00D0CF000123'
        expect(data.new_device?).to be true
      end

      it 'will get a device if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.device).not_to be nil
        expect(data.device.mac).to eq '00D0CF000123'
        expect(data.new_device?).to be true

        new_data = described_class.new(data_hash)
        expect(new_data.device).not_to be nil
        expect(new_data.device.mac).to eq '00D0CF000123'
        expect(new_data.new_device?).to be false
      end
    end

    describe '.device_details' do
      it 'will create a new device_details if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.device.details).to be nil
        data.device_details
        expect(data.device.details).not_to be nil
        expect(data.device.details.id).not_to be nil
      end

      it 'can set device details' do
        data = described_class.new(data_hash)
        expect(data.device.details).to be nil
        data.device.details = data.device_details
        data.device.save!
        expect(data.device.details.id).not_to be nil
      end
    end

    describe '.site' do
      it 'will create a new site if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.site).not_to be nil
        expect(data.site.name).to eq 'SITE ABC'
        expect(data.new_site?).to be true
      end

      it 'will get a site if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.site).not_to be nil
        expect(data.site.name).to eq 'SITE ABC'
        expect(data.new_site?).to be true

        new_data = described_class.new(data_hash)
        expect(new_data.site).not_to be nil
        expect(new_data.site.name).to eq 'SITE ABC'
        expect(new_data.new_site?).to be false
      end
    end

    describe '.location' do
      it 'will get a location if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.location).not_to be nil

        new_data = described_class.new(data_hash)
        new_data.location.reload
        expect(new_data.location).not_to be nil
      end
    end

    describe '.snmp_profile' do
      it 'will create a new snmp profile if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.snmp_profile).not_to be nil
        expect(data.snmp_profile.community).to eq 'armt-snmp'
        expect(data.new_snmp_profile?).to be true
      end

      it 'will get a snmp profile if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.snmp_profile).not_to be nil
        expect(data.snmp_profile.community).to eq 'armt-snmp'
        expect(data.new_snmp_profile?).to be true

        new_data = described_class.new(data_hash)
        expect(new_data.snmp_profile).not_to be nil
        expect(new_data.snmp_profile.community).to eq 'armt-snmp'
        expect(new_data.new_snmp_profile?).to be false
      end
    end

    describe '.portal_profile' do
      it 'will create a new portal profile if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.portal_profile).not_to be nil
        expect(data.portal_profile.auth_user).to eq 'authuser'
        expect(data.new_portal_profile?).to be true
      end

      it 'will get a portal profile if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.portal_profile).not_to be nil
        expect(data.portal_profile.auth_user).to eq 'authuser'
        expect(data.new_portal_profile?).to be true

        new_data = described_class.new(data_hash)
        expect(new_data.portal_profile).not_to be nil
        expect(new_data.portal_profile.auth_user).to eq 'authuser'
        expect(new_data.new_portal_profile?).to be false
      end
    end

    describe '.configuration' do
      it 'will create a new configuration if it is not in the database' do
        data = described_class.new(data_hash)
        expect(data.configuration).not_to be nil
        expect(data.configuration.snmp.community).to eq 'armt-snmp'
        expect(data.configuration.portal.auth_user).to eq 'authuser'
        expect(data.new_configuration?).to be true
      end

      it 'will get a configuration if it exists in the database' do
        data = described_class.new(data_hash)
        expect(data.configuration).not_to be nil
        expect(data.configuration.snmp.community).to eq 'armt-snmp'
        expect(data.configuration.portal.auth_user).to eq 'authuser'
        expect(data.new_configuration?).to be true

        new_data = described_class.new(data_hash)
        expect(new_data.configuration).not_to be nil
        expect(new_data.configuration.snmp.community).to eq 'armt-snmp'
        expect(new_data.configuration.portal.auth_user).to eq 'authuser'
        expect(new_data.new_configuration?).to be false
      end
    end
  end
end
