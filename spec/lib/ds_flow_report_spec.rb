# frozen_string_literal: true

require 'rails_helper'

describe DSFlowReport do
  subject { described_class }

  let(:input_hash) do
    {
      'N1' => '60602',
      'N2' => '487030',
      'N3' => '246373',
      'N4' => '5016470',
      'N5' => '417605255',
      'N6' => '6527987'
    }
  end

  let(:raw_output) do
    {
      N1: 60_602,
      N2: 487_030,
      N3: 246_373,
      N4: 5_016_470,
      N5: 417_605_255,
      N6: 6_527_987
    }
  end

  let(:parsed_output) do
    {
      uptime:           'about 17 hours',
      upload_speed:     '60.88 kB/s',
      download_speed:   '30.80 kB/s',
      bytes_sent:       '5.02 MB',
      bytes_received:   '417.61 MB',
      max_upload_speed: '6.53 MB/s'
    }
  end

  describe '#to_h' do
    context 'when initialized with an empty hash' do
      let(:report) { subject.new({}) }

      it 'returns an empty hash' do
        expect(report.to_h).to eq({})
      end
    end

    context 'when given a ds report hash' do
      let(:report) { subject.new(input_hash) }

      it 'returns a hash with the raw and parsed output' do
        hash = report.to_h
        expect(hash).to be_present
        expect(hash[:raw]).to eq raw_output
        expect(hash[:parsed]).to eq parsed_output
      end
    end
  end

  describe '#uptime' do
    context 'when the uptime value is nil' do
      let(:report) { subject.new('N1' => nil) }

      it 'indicates the uptime is less than a minute' do
        expect(report.n1).to be 0
        expect(report.uptime).to eq 'less than a minute'
      end
    end

    context 'when the uptime value is an integer' do
      let(:report) { subject.new('N1' => 1000) }

      it 'returns the time in words' do
        expect(report.n1).to be > 0
        expect(report.uptime).to eq '17 minutes'
      end
    end
  end

  describe '#upload_speed' do
    context 'when the upload speed is nil' do
      let(:report) { subject.new('N2' => nil) }

      it 'returns zero bytes per second' do
        expect(report.n2).to be 0
        expect(report.upload_speed).to eq '0.00 B/s'
      end
    end

    context 'when the upload speed is an integer' do
      let(:report) { subject.new('N2' => 100_000) }

      it 'returns the formatted upload throughput' do
        expect(report.n2).to be > 0
        expect(report.upload_speed).to eq '12.50 kB/s'
      end
    end
  end

  describe '#download_speed' do
    context 'when the download speed is nil' do
      let(:report) { subject.new('N3' => nil) }

      it 'returns zero bytes per second' do
        expect(report.n3).to be 0
        expect(report.download_speed).to eq '0.00 B/s'
      end
    end

    context 'when the download speed is an integer' do
      let(:report) { subject.new('N3' => 200_000) }

      it 'returns the formatted download throughput' do
        expect(report.n3).to be > 0
        expect(report.download_speed).to eq '25.00 kB/s'
      end
    end
  end

  describe '#bytes_sent' do
    context 'when bytes sent is nil' do
      let(:report) { subject.new('N4' => nil) }

      it 'returns zero bytes' do
        expect(report.n4).to be 0
        expect(report.bytes_sent).to eq '0.00 B'
      end
    end

    context 'when the bytes sent is an integer' do
      let(:report) { subject.new('N4' => 300_000) }

      it 'returns the formatted bytes' do
        expect(report.n4).to be > 0
        expect(report.bytes_sent).to eq '300.00 kB'
      end
    end
  end

  describe '#bytes_received' do
    context 'when bytes received is nil' do
      let(:report) { subject.new('N5' => nil) }

      it 'returns zero bytes' do
        expect(report.n5).to be 0
        expect(report.bytes_received).to eq '0.00 B'
      end
    end

    context 'when the bytes received is an integer' do
      let(:report) { subject.new('N5' => 400_000) }

      it 'returns the formatted bytes' do
        expect(report.n5).to be > 0
        expect(report.bytes_received).to eq '400.00 kB'
      end
    end
  end

  describe '#max_upload_speed' do
    context 'when max upload speed is nil' do
      let(:report) { subject.new('N6' => nil) }

      it 'returns zero bytes per second' do
        expect(report.n6).to be 0
        expect(report.max_upload_speed).to eq '0.00 B/s'
      end
    end

    context 'when the max upload speed is an integer' do
      let(:report) { subject.new('N6' => 5_000_000) }

      it 'returns the formatted max upload throughput' do
        expect(report.n6).to be > 0
        expect(report.max_upload_speed).to eq '5.00 MB/s'
      end
    end
  end

  describe '#max_download_speed' do
    context 'when max download speed is nil' do
      let(:report) { subject.new('N7' => nil) }

      it 'returns zero bytes per second' do
        expect(report.n7).to be 0
        expect(report.max_download_speed).to eq '0.00 B/s'
      end
    end

    context 'when the max download speed is an integer' do
      let(:report) { subject.new('N7' => 6_000_000) }

      it 'returns the formatted max upload throughput' do
        expect(report.n7).to be > 0
        expect(report.max_download_speed).to eq '6.00 MB/s'
      end
    end
  end
end
