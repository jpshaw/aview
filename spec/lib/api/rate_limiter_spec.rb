# frozen_string_literal: true

require 'rails_helper'

describe Api::RateLimiter do
  subject { described_class }

  let(:user)              { create(:user) }
  let(:limiter)           { subject.new(user: user) }
  let(:api_request_limit) { 3 }
  let(:api_time_window)   { 15.seconds.to_i }

  before do
    Rails.cache.clear
    allow(user).to receive(:api_request_limit).and_return(api_request_limit)
    allow(user).to receive(:api_time_window).and_return(api_time_window)
  end

  describe '#count' do
    context 'when the user has never made an api call' do
      before do
        key = limiter.send(:rate_key)
        count = Rails.cache.read(key)
        expect(count).to be nil
      end

      it 'returns 0' do
        expect(limiter.count).to eq 0
      end
    end

    context 'when the user has made a single api call' do
      before do
        limiter.increment
      end

      it 'returns 1' do
        expect(limiter.count).to eq 1
      end
    end

    context 'when the user has made the maximum amount of api calls for the interval' do
      before do
        api_request_limit.times { limiter.increment }
      end

      it 'returns the max count' do
        expect(limiter.count).to eq api_request_limit
      end
    end
  end

  describe '#increment' do
    it 'triggers `fetch_cache` before running' do
      expect(limiter).to callback(:fetch_cache).before(:increment)
    end

    context 'when the user has made no api calls' do
      before do
        expect(limiter.count).to eq 0
      end

      it 'increases the count to 1' do
        expect do
          limiter.increment
        end.to change(limiter, :count).from(0).to(1)
      end
    end

    context 'when the user has made `max_count-1` api calls and attempts one more' do
      before do
        (api_request_limit - 1).times { limiter.increment }
      end

      it 'increases the count to the max' do
        expect do
          limiter.increment
        end.to change(limiter, :count).to(api_request_limit)
      end
    end

    context 'when the user has maxed their api calls for the interval' do
      before do
        api_request_limit.times { limiter.increment }
      end

      it 'does not increases the count' do
        expect do
          limiter.increment
        end.not_to change(limiter, :count)
      end
    end
  end

  describe '#max_requests_reached?' do
    context 'when the user has made no api calls' do
      before do
        expect(limiter.count).to eq 0
      end

      it 'returns false' do
        expect(limiter.max_requests_reached?).to be false
      end
    end

    context 'when the user makes fewer api calls than they are permitted' do
      before do
        limiter.increment
        expect(limiter.count).to be >= 1
        expect(limiter.count).to be < api_request_limit
      end

      it 'returns false' do
        expect(limiter.max_requests_reached?).to be false
      end
    end

    context 'when the user has maxed their api calls for the interval' do
      before do
        api_request_limit.times { limiter.increment }
        expect(limiter.count).to eq api_request_limit
      end

      it 'returns true' do
        expect(limiter.max_requests_reached?).to be true
      end
    end
  end

  describe '#remaining_calls' do
    context 'when the user has made no api calls' do
      before do
        expect(limiter.count).to eq 0
      end

      it 'returns the max number of calls available to the user' do
        expect(limiter.remaining_calls).to eq user.api_request_limit
      end
    end

    context 'when the user makes fewer api calls than they are permitted' do
      before do
        limiter.increment
        expect(limiter.count).to be >= 1
        expect(limiter.count).to be < api_request_limit
      end

      it 'returns the remaining number of calls available to the user' do
        remaining = api_request_limit - limiter.count
        expect(limiter.remaining_calls).to eq remaining
      end
    end

    context 'when the user has maxed their api calls for the interval' do
      before do
        api_request_limit.times { limiter.increment }
        expect(limiter.count).to eq api_request_limit
      end

      it 'returns zero' do
        expect(limiter.remaining_calls).to be 0
      end
    end
  end

  describe '#expires_at' do
    context 'when the user has not made an api call before' do
      before do
        expect(limiter.count).to be 0
      end

      it 'is set to `time window` seconds from now' do
        Timecop.freeze do
          time_window_offset = Time.at(Time.now.to_i + api_time_window).to_i
          expect(limiter.expires_at.to_i).to eq time_window_offset
        end
      end
    end

    context 'when the user has made an api request' do
      before do
        Timecop.freeze
        limiter.increment
        expect(limiter.count).to be > 0
      end

      after do
        Timecop.return
      end

      it 'updates `expired_at` to reflect the different in time' do
        Timecop.travel(5.seconds.from_now)
        difference = limiter.expires_at.to_i - Time.now.utc.to_i
        expect(difference).to eq 10.seconds.to_i
      end
    end
  end
end
