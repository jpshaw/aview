# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Object do
  subject { described_class }

  describe '#ok?' do
    it 'returns true when the result code is `ok`' do
      object = subject.new(result: Gateway::API::Object::OK)
      expect(object.ok?).to be true
    end
  end

  describe '#fail?' do
    it 'returns true when the result code is `fail`' do
      object = subject.new(result: Gateway::API::Object::FAIL)
      expect(object.fail?).to be true
    end
  end
end
