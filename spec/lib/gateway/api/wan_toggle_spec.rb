# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::WanToggle do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#wan_toggle' do
    context 'when no interface is given' do
      let(:options) { { state: 'off', time: '1' } }

      it 'defaults to primary' do
        stub_request(:get, path).with(query: { cmd: :interface_control, interface: 'primary', sub_cmd: 'off', time: 1 })
                                .to_return(body: fixture('gateway/wan_toggle_success.json'), headers: headers)
        client.wan_toggle(options)
      end
    end

    context 'when no state is given' do
      let(:options) { { interface: 'primary', time: '1' } }

      it 'defaults to on' do
        stub_request(:get, path).with(query: { cmd: :interface_control, interface: 'primary', sub_cmd: 'on', time: 1 })
                                .to_return(body: fixture('gateway/wan_toggle_success.json'), headers: headers)
        client.wan_toggle(options)
      end
    end

    context 'when no time is given' do
      let(:options) { { interface: 'secondary', state: 'off' } }

      it 'defaults to 0' do
        stub_request(:get, path).with(query: { cmd: :interface_control, interface: 'secondary', sub_cmd: 'off', time: 0 })
                                .to_return(body: fixture('gateway/wan_toggle_success.json'), headers: headers)
        client.wan_toggle(options)
      end
    end

    context 'when a string time is given' do
      let(:options) { { time: 'NaN' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when a decimal time is given' do
      let(:options) { { time: '1.2' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when a time greater than 1440 minutes is given' do
      let(:options) { { time: '1441' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when a time less than 0 minutes is given' do
      let(:options) { { time: '-1' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when an invalid state is given' do
      let(:options) { { state: 'invalid' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when an invalid interface is given' do
      let(:options) { { interface: 'invalid' } }

      it 'throws an argument error' do
        expect { client.wan_toggle(options) }.to raise_error(ArgumentError)
      end
    end

    context 'when the primary interface is set to turn off for 1 minute' do
      let(:options) { { interface: 'primary', state: 'off', time: '1' } }
      let(:results) { client.wan_toggle(options) }

      before do
        stub_request(:get, path).with(query: { cmd: :interface_control, interface: 'primary', sub_cmd: 'off', time: 1 })
                                .to_return(body: fixture('gateway/wan_toggle_success.json'), headers: headers)
      end

      it 'makes the request' do
        results
        expect(a_request(:get, path).with(query: { cmd: :interface_control, interface: 'primary', sub_cmd: 'off', time: 1 })).to have_been_made
      end

      it 'returns a Gateway::API::WanToggleResults object' do
        expect(results).to be_a(Gateway::API::WanToggleResults)
      end

      it 'is a successful result' do
        expect(results.ok?).to be true
      end

      it 'returns the state and time in the result' do
        expect(results.output).to eq([state: 'off', time: '1', status: nil])
      end
    end

    context 'when the primary interface is set to turn off but is already off' do
      let(:options) { { interface: 'primary', state: 'off', time: '1' } }
      let(:results) { client.wan_toggle(options) }

      before do
        stub_request(:get, path).with(query: { cmd: :interface_control, interface: 'primary', sub_cmd: 'off', time: 1 })
                                .to_return(body: fixture('gateway/wan_toggle_failure.json'), headers: headers)
      end

      it 'is a failed result' do
        expect(results.ok?).to be false
      end

      it 'returns the state and time in the result' do
        expect(results.output).to eq([state: 'off', time: '1', status: "Ignoring command.  The Primary WAN interface has been administratively forced off for another 55 seconds."])
      end

      it 'gives the status of the result' do
        expect(results.status).to eq('Ignoring command.  The Primary WAN interface has been administratively forced off for another 55 seconds.')
      end
    end
  end
end
