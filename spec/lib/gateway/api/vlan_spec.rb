# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Vlan do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#vlanconfig' do
    let(:result) { client.vlanconfig }

    before do
      stub_request(:get, path).with(query: { cmd: :adv_info, section: :vlanconfig })
                              .to_return(body: fixture('gateway/vlanconfig.json'), headers: headers)
    end

    it 'succeeds' do
      expect(result).to be_ok
    end

    it 'returns a Gateway::API::VlanResults object' do
      expect(result).to be_a(Gateway::API::VlanResults)
    end

    context 'when calling save_lan_networks on the result' do
      let(:device) { create(:gateway_device) }

      before do
        result.save_lan_networks(device)
      end

      it 'creates a network record for each LAN port' do
        (1..7).each do |port|
          expect(device.networks.where(network_iface: "lan#{port}").first).to be_present
        end
      end

      it 'sets the VLAN IP address on the first set of vlan ports' do
        (1..4).each do |port|
          expect(device.networks.where(network_iface: "lan#{port}").first.ip_addrs['vlan3']).to eq('10.250.1.1/24')
        end
      end

      it 'sets the VLAN IP address on the second set of vlan ports' do
        (5..7).each do |port|
          expect(device.networks.where(network_iface: "lan#{port}").first.ip_addrs['vlan20']).to eq('10.10.20.1/1.2.3.4')
        end
      end

      it 'sets multiple VLAN IP addresses when present' do
        lan_5_ip_addresses = device.networks.where(network_iface: 'lan5').first.ip_addrs
        expected_ip_addrs = { 'vlan20' => '10.10.20.1/1.2.3.4', 'vlan3' => '10.250.1.1/24' }
        expect(lan_5_ip_addresses).to eq expected_ip_addrs
      end

      it 'removes VLAN addresses if port is disabled' do
        expect(device.networks.where(network_iface: 'lan8').first.ip_addrs).to be_nil
      end

      it 'sets network status to disabled if port is disabled' do
        expect(device.networks.where(network_iface: 'lan8').first.status).to eq('disabled')
      end

      context 'when the port status is connected' do
        let(:device_network) { device.networks.where(network_iface: 'lan5').first }

        it 'sets the status of the network record to connected' do
          expect(device_network.status).to eq('Connected')
        end

        it 'sets the speed of the network record' do
          expect(device_network.speed).to eq('1000Mbit - full duplex')
        end

        it 'sets the tx of the network record to be the total of all VLANs' do
          expect(device_network.tx).to eq(440)
        end

        it 'sets the rx of the network record to be the total of all VLANs' do
          expect(device_network.rx).to eq(220)
        end
      end

      context 'when the port is connected at a slower speed' do
        let(:device_network) { device.networks.where(network_iface: 'lan4').first }

        it 'sets the speed of the network record' do
          expect(device_network.speed).to eq('100Mbit - full duplex')
        end
      end

      context 'when the port status is disconnected' do
        let(:device_network) { device.networks.where(network_iface: 'lan6').first }

        it 'sets the status of the network record to not connected' do
          expect(device_network.status).to eq('Not Connected')
        end

        it 'sets the speed of the network record' do
          expect(device_network.speed).to eq('No Link')
        end

        it 'sets the tx of the network record' do
          expect(device_network.tx).to eq(0)
        end

        it 'sets the rx of the network record' do
          expect(device_network.rx).to eq(0)
        end
      end
    end

    context 'when a LAN network for a Gateway already has VLAN details' do
      let(:device) { create(:gateway_device) }
      let(:device_network) { device.networks.where(network_iface: 'lan1', net_type: 'lan').first_or_initialize }

      before do
        device_network.ip_addrs = { 'vlan3' => '192.168.1.1/24' }
        device_network.save
        result.save_lan_networks(device)
        device_network.reload
      end

      it 'overwrites the VLAN details for the network with the new VLAN addresses' do
        expect(device_network.ip_addrs).to eq('vlan3' => '10.250.1.1/24')
      end
    end
  end
end
