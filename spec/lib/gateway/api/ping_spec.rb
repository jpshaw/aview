# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Ping do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#ping' do
    before do
      @options = { count: 3, size: 100, src_int: Gateway::API::Utils::DEFAULT_IFACE }
    end

    context 'when no target is given' do
      before do
        expect(@options[:target]).to be nil
      end

      it 'throws an argument error' do
        expect { client.ping(@options) }.to raise_error(ArgumentError)
      end
    end

    context 'when an IPv4 target is given' do
      before do
        @options[:target] = '8.8.8.8'
        stub_request(:get, path).with(query: { cmd: :ping }.merge(@options))
                                .to_return(body: fixture('gateway/ping_ipv4.json'), headers: headers)
        @results = client.ping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :ping }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::PingResults object' do
        expect(@results).to be_a(Gateway::API::PingResults)
      end

      it 'reaches the target' do
        expect(@results.target_reached?).to be true
      end

      it 'completes the ping test' do
        expect(@results.test_completed?).to be true
      end

      it 'only pings the `count` number of times' do
        expect(@results.pings.count).to eq @options[:count]
      end

      it 'attempts to ping the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'has packet statistics' do
        expect(@results.packets).to be_a(Gateway::API::PacketStats)
        expect(@results.packets.transmitted).to eq 3
        expect(@results.packets.received).to eq 3
        expect(@results.packets.loss).to eq '0%'
        expect(@results.packets.time).to eq '2009ms'
      end

      it 'has round trip statistics' do
        expect(@results.round_trip).to be_a(Gateway::API::RoundTripStats)
        expect(@results.round_trip.min).to eq 34.105
        expect(@results.round_trip.max).to eq 34.579
        expect(@results.round_trip.avg).to eq 34.36
        expect(@results.round_trip.mdev).to eq 0.289
        expect(@results.round_trip.units).to eq 'ms'
      end
    end

    context 'when an IPv6 target is given' do
      before do
        @options[:target] = '2620:e:4000:4440:e61f:13ff:fe80:7fb8'
        stub_request(:get, path).with(query: { cmd: :ping6 }.merge(@options))
                                .to_return(body: fixture('gateway/ping_ipv6.json'), headers: headers)
        @results = client.ping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :ping6 }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::PingResults object' do
        expect(@results).to be_a(Gateway::API::PingResults)
      end

      it 'reaches the target' do
        expect(@results.target_reached?).to be true
      end

      it 'completes the ping test' do
        expect(@results.test_completed?).to be true
      end

      it 'only pings the `count` number of times' do
        expect(@results.pings.count).to eq @options[:count]
      end

      it 'attempts to ping the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'has packet statistics' do
        expect(@results.packets).to be_a(Gateway::API::PacketStats)
        expect(@results.packets.transmitted).to eq 3
        expect(@results.packets.received).to eq 3
        expect(@results.packets.loss).to eq '0%'
        expect(@results.packets.time).to eq '2005ms'
      end

      it 'has round trip statistics' do
        expect(@results.round_trip).to be_a(Gateway::API::RoundTripStats)
        expect(@results.round_trip.min).to eq 2.359
        expect(@results.round_trip.max).to eq 8.868
        expect(@results.round_trip.avg).to eq 4.608
        expect(@results.round_trip.mdev).to eq 3.014
        expect(@results.round_trip.units).to eq 'ms'
      end
    end

    context 'when an IPv4 hostname target is given' do
      before do
        @options[:target] = 'google.com'
        @target           = '65.196.188.55'

        stub_request(:get, path).with(query: { cmd: :ping }.merge(@options))
                                .to_return(body: fixture('gateway/ping_ipv4_hostname.json'), headers: headers)
        @results = client.ping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :ping }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::PingResults object' do
        expect(@results).to be_a(Gateway::API::PingResults)
      end

      it 'reaches the target' do
        expect(@results.target_reached?).to be true
      end

      it 'completes the ping test' do
        expect(@results.test_completed?).to be true
      end

      it 'only pings the `count` number of times' do
        expect(@results.pings.count).to eq @options[:count]
      end

      it 'attempts to ping the target address' do
        expect(@results.target).to eq @target
      end

      it 'has packet statistics' do
        expect(@results.packets).to be_a(Gateway::API::PacketStats)
        expect(@results.packets.transmitted).to eq 3
        expect(@results.packets.received).to eq 3
        expect(@results.packets.loss).to eq '0%'
        expect(@results.packets.time).to eq '2009ms'
      end

      it 'has round trip statistics' do
        expect(@results.round_trip).to be_a(Gateway::API::RoundTripStats)
        expect(@results.round_trip.min).to eq 34.105
        expect(@results.round_trip.max).to eq 34.579
        expect(@results.round_trip.avg).to eq 34.36
        expect(@results.round_trip.mdev).to eq 0.289
        expect(@results.round_trip.units).to eq 'ms'
      end
    end

    context 'when an IPv6 hostname target is given' do
      before do
        @options[:target] = 'ipv6.google.com'
        @target           = 'iad23s23-in-x03.1e100.net'
        client_opts       = { ipv6: Gateway::API::Utils::IPv6_ON }

        stub_request(:get, path).with(query: { cmd: :ping6 }.merge(@options))
                                .to_return(body: fixture('gateway/ping_ipv6_hostname.json'), headers: headers)
        @results = client.ping(@options.merge(client_opts))
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :ping6 }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::PingResults object' do
        expect(@results).to be_a(Gateway::API::PingResults)
      end

      it 'reaches the target' do
        expect(@results.target_reached?).to be true
      end

      it 'completes the ping test' do
        expect(@results.test_completed?).to be true
      end

      it 'only pings the `count` number of times' do
        expect(@results.pings.count).to eq @options[:count]
      end

      it 'attempts to ping the target address' do
        expect(@results.target).to eq @target
      end

      it 'has packet statistics' do
        expect(@results.packets).to be_a(Gateway::API::PacketStats)
        expect(@results.packets.transmitted).to eq 3
        expect(@results.packets.received).to eq 3
        expect(@results.packets.loss).to eq '0%'
        expect(@results.packets.time).to eq '2005ms'
      end

      it 'has round trip statistics' do
        expect(@results.round_trip).to be_a(Gateway::API::RoundTripStats)
        expect(@results.round_trip.min).to eq 2.359
        expect(@results.round_trip.max).to eq 8.868
        expect(@results.round_trip.avg).to eq 4.608
        expect(@results.round_trip.mdev).to eq 3.014
        expect(@results.round_trip.units).to eq 'ms'
      end
    end

    context 'when an unreachable target is given' do
      before do
        @options[:target] = '1.2.3.4'
        stub_request(:get, path).with(query: { cmd: :ping }.merge(@options))
                                .to_return(body: fixture('gateway/ping_ipv4_unreachable.json'), headers: headers)
        @results = client.ping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :ping }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::PingResults object' do
        expect(@results).to be_a(Gateway::API::PingResults)
      end

      it 'does not reach the target' do
        expect(@results.target_reached?).to be false
      end

      it 'completes the ping test' do
        expect(@results.test_completed?).to be true
      end

      it 'attempts to ping the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'has packet statistics' do
        expect(@results.packets).to be_a(Gateway::API::PacketStats)
        expect(@results.packets.transmitted).to eq 3
        expect(@results.packets.received).to eq 0
        expect(@results.packets.loss).to eq '100%'
        expect(@results.packets.time).to eq '2009ms'
      end

      it 'does not have round trip statistics' do
        expect(@results.round_trip).to be nil
      end
    end
  end
end
