# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Client do
  describe '#base_uri' do
    context 'when no host is set' do
      before do
        expect(subject.host).to be nil
      end

      it 'returns a blank uri' do
        expect(subject.base_uri).to eq 'https://'
      end
    end

    context 'when given an ipv4 host' do
      before do
        subject.host = '1.2.3.4'
      end

      it 'returns the correct uri' do
        expect(subject.base_uri).to eq 'https://1.2.3.4'
      end
    end

    context 'when given an ipv6 host' do
      before do
        subject.host = '2620:000e:4000:8890:0000:0000:0101:57d4'
      end

      it 'returns the correct uri' do
        expect(subject.base_uri).to eq 'https://[2620:000e:4000:8890:0000:0000:0101:57d4]'
      end
    end
  end

  describe '#scheme' do
    context 'when `use_ssl` is true' do
      before do
        subject.use_ssl = true
      end

      it 'returns https' do
        expect(subject.scheme).to eq 'https'
      end
    end

    context 'when `use_ssl` is false' do
      before do
        subject.use_ssl = false
      end

      it 'returns http' do
        expect(subject.scheme).to eq 'http'
      end
    end
  end

  describe '#headers' do
    it 'returns the correct headers' do
      headers = subject.headers
      expect(headers[:content_type]).to eq 'application/json; charset=utf-8'
    end
  end

  describe '#auth' do
    context 'when auth credentials are given' do
      before do
        subject.user = 'authuser'
        subject.password = 'authpass'
      end

      it 'returns the correct auth credentials' do
        auth = subject.auth
        expect(auth[:user]).to eq 'authuser'
        expect(auth[:pass]).to eq 'authpass'
      end

      it 'has auth' do
        expect(subject.has_auth?).to be true
      end
    end

    context 'when no auth credentials are given' do
      before do
        subject.user = nil
        subject.password = nil
      end

      it 'returns nil auth credentials' do
        auth = subject.auth
        expect(auth[:user]).to be nil
        expect(auth[:pass]).to be nil
      end

      it 'does not have auth' do
        expect(subject.has_auth?).to be false
      end
    end
  end
end
