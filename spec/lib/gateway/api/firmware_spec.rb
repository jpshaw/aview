# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Firmware do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#upgrade' do
    let(:command) { :upgrade }

    context 'when a new firmware is not available' do
      before do
        stub_request(:get, path).with(query: { cmd: command })
                                .to_return(body: fixture('gateway/upgrade.json'), headers: headers)
        @upgrade = client.upgrade
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
      end

      it 'returns an Gateway::API::Upgrade object' do
        expect(@upgrade).to be_an(Gateway::API::Upgrade)
      end

      it 'indicates that an upgrade is not available' do
        expect(@upgrade.available?).to be false
      end

      it 'returns the current version' do
        expect(@upgrade.current_version).to eq '6.1.8'
      end

      it 'returns the available version' do
        expect(@upgrade.available_version).to eq '5.6.102'
      end

      it 'does nothing' do
        expect(@upgrade.performed?).to be false
      end
    end

    context 'when a new firmware is available' do
      before do
        stub_request(:get, path).with(query: { cmd: command })
                                .to_return(body: fixture('gateway/upgrade_with_available.json'), headers: headers)
        @upgrade = client.upgrade
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
      end

      it 'returns an Gateway::API::Upgrade object' do
        expect(@upgrade).to be_an(Gateway::API::Upgrade)
      end

      it 'indicates that an upgrade is available' do
        expect(@upgrade.available?).to be true
      end

      it 'returns the current version' do
        expect(@upgrade.current_version).to eq '6.0.0'
      end

      it 'returns the available version' do
        expect(@upgrade.available_version).to eq '6.0.74'
      end

      it 'performs the upgrade' do
        expect(@upgrade.performed?).to be true
      end
    end
  end

  describe '#upgrade_check' do
    let(:command) { :upgrade_check }

    context 'when a new firmware is not available' do
      before do
        stub_request(:get, path).with(query: { cmd: command })
                                .to_return(body: fixture('gateway/upgrade_check.json'), headers: headers)
        @upgrade = client.upgrade_check
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
      end

      it 'returns an Gateway::API::Upgrade object' do
        expect(@upgrade).to be_an(Gateway::API::Upgrade)
      end

      it 'indicates that an upgrade is not available' do
        expect(@upgrade.available?).to be false
      end

      it 'returns the current version' do
        expect(@upgrade.current_version).to eq '6.1.8'
      end

      it 'returns the available version' do
        expect(@upgrade.available_version).to eq '5.6.102'
      end

      it 'does nothing' do
        expect(@upgrade.performed?).to be false
      end
    end

    context 'when a new firmware is available' do
      before do
        stub_request(:get, path).with(query: { cmd: command })
                                .to_return(body: fixture('gateway/upgrade_check_with_available.json'), headers: headers)
        @upgrade = client.upgrade_check
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
      end

      it 'returns an Gateway::API::Upgrade object' do
        expect(@upgrade).to be_an(Gateway::API::Upgrade)
      end

      it 'returns the current version' do
        expect(@upgrade.current_version).to eq '6.0.0'
      end

      it 'returns the available version' do
        expect(@upgrade.available_version).to eq '6.0.74'
      end

      it 'indicates that an upgrade is available' do
        expect(@upgrade.available?).to be true
      end

      # We don't want to perform an upgrade with a `check`
      it 'does nothing' do
        expect(@upgrade.performed?).to be false
      end
    end
  end

  describe '#rollback' do
    let(:command) { :rollback }

    before do
      stub_request(:get, path).with(query: { cmd: command })
                              .to_return(body: fixture('gateway/downgrade_with_available.json'), headers: headers)
      @upgrade = client.rollback
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
    end

    it 'returns an Gateway::API::Downgrade object' do
      expect(@upgrade).to be_an(Gateway::API::Downgrade)
    end

    it 'indicates that an upgrade is available' do
      expect(@upgrade.available?).to be true
    end

    it 'returns the current version' do
      expect(@upgrade.current_version).to eq '6.4.0'
    end

    it 'returns the available version' do
      expect(@upgrade.available_version).to eq '6.0.46'
    end

    it 'performs the upgrade' do
      expect(@upgrade.performed?).to be true
    end
  end

  describe '#rollback_check' do
    let(:command) { :rollback_check }

    before do
      stub_request(:get, path).with(query: { cmd: command })
                              .to_return(body: fixture('gateway/downgrade_check_with_available.json'), headers: headers)
      @upgrade = client.rollback_check
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: command })).to have_been_made
    end

    it 'returns an Gateway::API::Downgrade object' do
      expect(@upgrade).to be_an(Gateway::API::Downgrade)
    end

    it 'returns the currently installed version' do
      expect(@upgrade.current_version).to eq '6.4.0'
    end

    it 'returns the available rollback version' do
      expect(@upgrade.available_version).to eq '6.0.46'
    end

    it 'indicates that an rollback is available' do
      expect(@upgrade.available?).to be true
    end

    # We don't want to perform an upgrade with a `check`
    it 'does nothing' do
      expect(@upgrade.performed?).to be false
    end
  end
end
