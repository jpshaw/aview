# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Arping do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#arping' do
    before do
      @options = { src_int: Gateway::API::Utils::DEFAULT_IFACE }
    end

    context 'when no target is given' do
      before do
        expect(@options[:target]).to be nil
      end

      it 'throws an argument error' do
        expect { client.arping(@options) }.to raise_error(ArgumentError)
      end
    end

    context 'when an unreachable target is given' do
      before do
        @options[:target] = '108.33.27.111'
        stub_request(:get, path).with(query: { cmd: :arpping }.merge(@options))
                                .to_return(body: fixture('gateway/arping_ipv4_unreachable.json'), headers: headers)
        @results = client.arping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :arpping }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::ArpingResults object' do
        expect(@results).to be_a(Gateway::API::ArpingResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'returns the interface used for the arping' do
        expect(@results.interface).to eq 'default'
      end

      it 'indicates an arp failure' do
        expect(@results.arp_failed?).to be true
      end

      it 'completes the arping test' do
        expect(@results.test_completed?).to be true
      end
    end

    context 'when a reachable ipv4 target is given' do
      before do
        @options[:target] = '8.8.8.8'
        stub_request(:get, path).with(query: { cmd: :arpping }.merge(@options))
                                .to_return(body: fixture('gateway/arping_ipv4.json'), headers: headers)
        @results = client.arping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :arpping }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::ArpingResults object' do
        expect(@results).to be_a(Gateway::API::ArpingResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'returns the interface used for the arping' do
        expect(@results.interface).to eq 'eth1'
      end

      it 'does not have an arp failure' do
        expect(@results.arp_failed?).to be false
      end

      it 'completes the arping test' do
        expect(@results.test_completed?).to be true
      end
    end

    context 'when a reachable ipv6 target is given' do
      before do
        @options[:target] = 'fd22:2222:2222:3100:be5f:f4ff:fee1:a36b'
        stub_request(:get, path).with(query: { cmd: :arpping6 }.merge(@options))
                                .to_return(body: fixture('gateway/arping_ipv6.json'), headers: headers)
        @results = client.arping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :arpping6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::ArpingResults object' do
        expect(@results).to be_a(Gateway::API::ArpingResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'returns the interface used for the arping' do
        expect(@results.interface).to eq 'eth0.10'
      end

      it 'does not have an arp failure' do
        expect(@results.arp_failed?).to be false
      end

      it 'completes the arping test' do
        expect(@results.test_completed?).to be true
      end
    end

    context 'when a reachable ipv4 hostname target is given' do
      before do
        @options[:target] = 'google.com'
        @target           = '65.196.188.55'

        stub_request(:get, path).with(query: { cmd: :arpping }.merge(@options))
                                .to_return(body: fixture('gateway/arping_ipv4_hostname.json'), headers: headers)
        @results = client.arping(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :arpping }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::ArpingResults object' do
        expect(@results).to be_a(Gateway::API::ArpingResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'returns the interface used for the arping' do
        expect(@results.interface).to eq 'eth1'
      end

      it 'does not have an arp failure' do
        expect(@results.arp_failed?).to be false
      end

      it 'completes the arping test' do
        expect(@results.test_completed?).to be true
      end
    end

    context 'when a reachable ipv6 hostname target is given' do
      before do
        @options[:target] = 'ipv6.google.com'
        @target           = 'iad23s23-in-x03.1e100.net'
        client_opts       = { ipv6: Gateway::API::Utils::IPv6_ON }

        stub_request(:get, path).with(query: { cmd: :arpping6 }.merge(@options))
                                .to_return(body: fixture('gateway/arping_ipv6_hostname.json'), headers: headers)
        @results = client.arping(@options.merge(client_opts))
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :arpping6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::ArpingResults object' do
        expect(@results).to be_a(Gateway::API::ArpingResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'returns the interface used for the arping' do
        expect(@results.interface).to eq 'eth0.10'
      end

      it 'does not have an arp failure' do
        expect(@results.arp_failed?).to be false
      end

      it 'completes the arping test' do
        expect(@results.test_completed?).to be true
      end
    end
  end
end
