# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::ArpingResults do
  subject { described_class }

  before do
    @body = fixture('gateway/arping_ipv4.json').read
    @results = subject.new(JSON.parse(@body, symbolize_names: true))
  end

  describe '#interface' do
    it 'returns the interface used in the test' do
      expect(@results.interface).to eq 'eth1'
    end
  end

  describe '#target' do
    it 'returns the target used in the test' do
      expect(@results.target).to eq '8.8.8.8'
    end
  end

  describe '#arp_failed?' do
    context 'when the test succeeded' do
      it 'returns false' do
        expect(@results.arp_failed?).to be false
      end
    end

    context 'when the test failed' do
      before do
        @body = fixture('gateway/arping_ipv4_unreachable.json').read
        @results = subject.new(JSON.parse(@body, symbolize_names: true))
      end

      it 'returns true' do
        expect(@results.arp_failed?).to be true
      end
    end
  end

  describe '#test_completed?' do
    it 'returns true when the test has completed' do
      expect(@results.test_completed?).to be true
    end
  end
end
