# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Reboot do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#reboot' do
    before do
      stub_request(:get, path).with(query: { cmd: :reboot })
                              .to_return(body: fixture('gateway/reboot.json'), headers: headers)
      @results = client.reboot
    end

    it 'succeeds' do
      expect(@results.ok?).to be true
    end
  end
end
