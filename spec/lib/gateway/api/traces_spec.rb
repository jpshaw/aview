# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Traces do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#traceroute' do
    before do
      @options = { src_int: Gateway::API::Utils::DEFAULT_IFACE }
    end

    context 'when no target is given' do
      before do
        expect(@options[:target]).to be nil
      end

      it 'throws an argument error' do
        expect { client.traceroute(@options) }.to raise_error(ArgumentError)
      end
    end

    context 'when a reachable ipv4 target is given' do
      before do
        @options[:target] = '8.8.8.8'
        stub_request(:get, path).with(query: { cmd: :traceroute }.merge(@options))
                                .to_return(body: fixture('gateway/traceroute_ipv4.json'), headers: headers)
        @results = client.traceroute(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :traceroute }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'completes the traceroute test' do
        expect(@results.test_completed?).to be true
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv6 target is given' do
      before do
        @options[:target] = '2001:1890:1c00:5403::c:c8'
        stub_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))
                                .to_return(body: fixture('gateway/traceroute_ipv6.json'), headers: headers)
        @results = client.traceroute(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'completes the traceroute test' do
        expect(@results.test_completed?).to be true
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv4 hostname target is given' do
      before do
        @options[:target] = 'google.com'
        @target           = '65.196.188.55'

        stub_request(:get, path).with(query: { cmd: :traceroute }.merge(@options))
                                .to_return(body: fixture('gateway/traceroute_ipv4_hostname.json'), headers: headers)
        @results = client.traceroute(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :traceroute }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'completes the traceroute test' do
        expect(@results.test_completed?).to be true
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv6 hostname target is given' do
      before do
        @options[:target] = 'ipv6.google.com'
        @target           = 'iad23s23-in-x03.1e100.net'
        client_opts       = { ipv6: Gateway::API::Utils::IPv6_ON }

        stub_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))
                                .to_return(body: fixture('gateway/traceroute_ipv6_hostname.json'), headers: headers)
        @results = client.traceroute(@options.merge(client_opts))
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'completes the traceroute test' do
        expect(@results.test_completed?).to be true
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when an unreachable target is given' do
      before do
        @options[:target] = '2001:4860:4860::8888'
        stub_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))
                                .to_return(body: fixture('gateway/traceroute_ipv6_unreachable.json'), headers: headers)
        @results = client.traceroute(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :traceroute6 }.merge(@options))).to have_been_made
      end

      it 'fails' do
        expect(@results.trace_failed?).to be true
      end
    end
  end

  describe '#tracepath' do
    before do
      @options = { src_int: Gateway::API::Utils::DEFAULT_IFACE }
    end

    context 'when no target is given' do
      before do
        expect(@options[:target]).to be nil
      end

      it 'throws an argument error' do
        expect { client.tracepath(@options) }.to raise_error(ArgumentError)
      end
    end

    context 'when a reachable ipv4 target is given' do
      before do
        @options[:target] = '8.8.8.8'
        stub_request(:get, path).with(query: { cmd: :tracepath }.merge(@options))
                                .to_return(body: fixture('gateway/tracepath_ipv4.json'), headers: headers)
        @results = client.tracepath(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tracepath }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv6 target is given' do
      before do
        @options[:target] = '2001:1890:1c00:5403::c:c8'
        stub_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))
                                .to_return(body: fixture('gateway/tracepath_ipv6.json'), headers: headers)
        @results = client.tracepath(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @options[:target]
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv4 hostname target is given' do
      before do
        @options[:target] = 'google.com'
        @target           = '65.196.188.55'

        stub_request(:get, path).with(query: { cmd: :tracepath }.merge(@options))
                                .to_return(body: fixture('gateway/tracepath_ipv4_hostname.json'), headers: headers)
        @results = client.tracepath(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tracepath }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when a reachable ipv6 hostname target is given' do
      before do
        @options[:target] = 'ipv6.google.com'
        @target           = 'iad23s23-in-x03.1e100.net'
        client_opts       = { ipv6: Gateway::API::Utils::IPv6_ON }

        stub_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))
                                .to_return(body: fixture('gateway/tracepath_ipv6_hostname.json'), headers: headers)
        @results = client.tracepath(@options.merge(client_opts))
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))).to have_been_made
      end

      it 'returns an Gateway::API::TraceResults object' do
        expect(@results).to be_a(Gateway::API::TraceResults)
      end

      it 'attempts to send the arp packet to the target address' do
        expect(@results.target).to eq @target
      end

      it 'does not fail' do
        expect(@results.trace_failed?).to be false
      end
    end

    context 'when an unreachable target is given' do
      before do
        @options[:target] = '2001:4860:4860::8888'
        stub_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))
                                .to_return(body: fixture('gateway/tracepath_ipv6_unreachable.json'), headers: headers)
        @results = client.tracepath(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tracepath6 }.merge(@options))).to have_been_made
      end

      it 'fails' do
        expect(@results.trace_failed?).to be true
      end
    end
  end
end
