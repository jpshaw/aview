# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Request do
  subject { described_class }

  let(:client)  { build(:gateway_api_client) }
  let(:path)    { "#{client.base_uri}/path" }
  let(:request) { subject.new(client, :get, '/path') }

  describe '#perform' do
    it 'performs a request' do
      stub_request(:get, path).to_return(body: 'data')
      request.perform
      expect(a_request(:get, path)).to have_been_made
    end

    context 'when JSON is returned' do
      before do
        @data = { a: 'hello', b: 'world' }
        stub_request(:get, path).to_return(body: @data.to_json)
      end

      it 'is parsed' do
        expect(request.perform).to eq @data
      end
    end

    context 'when JSON is not returned' do
      before do
        @data = '<html>Hello World</html>'
        stub_request(:get, path).to_return(body: @data)
      end

      it 'returns the raw body' do
        expect(request.perform.to_s).to eq @data
      end
    end
  end
end
