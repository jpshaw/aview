# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Tunnels do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  before do
    @options = { name: 'SIGZILLA' }
  end

  describe '#tunnel_up' do
    context 'when no tunnel identifier is given' do
      it 'raises an argument error' do
        expect { client.tunnel_up }.to raise_error(ArgumentError)
      end
    end

    context 'when a tunnel identifier is given' do
      context 'when the tunnel is up' do
        before do
          stub_request(:get, path).with(query: { cmd: :tunnel_up }.merge(@options))
                                  .to_return(body: fixture('gateway/tunnel_up_fail.json'), headers: headers)
          @tunnel_command = client.tunnel_up(@options)
        end

        it 'makes the request' do
          expect(a_request(:get, path).with(query: { cmd: :tunnel_up }.merge(@options))).to have_been_made
        end

        it 'returns a Gateway::API::Tunnel object' do
          expect(@tunnel_command).to be_a(Gateway::API::Tunnel)
        end

        it 'does nothing' do
          expect(@tunnel_command.ok?).to be false
        end
      end

      context 'when the tunnel is down' do
        before do
          stub_request(:get, path).with(query: { cmd: :tunnel_up }.merge(@options))
                                  .to_return(body: fixture('gateway/tunnel_up.json'), headers: headers)
          @tunnel_command = client.tunnel_up(@options)
        end

        it 'makes the request' do
          expect(a_request(:get, path).with(query: { cmd: :tunnel_up }.merge(@options))).to have_been_made
        end

        it 'returns a Gateway::API::Tunnel object' do
          expect(@tunnel_command).to be_a(Gateway::API::Tunnel)
        end

        it 'brings the tunnel up' do
          expect(@tunnel_command.ok?).to be true
        end
      end
    end
  end

  describe '#tunnel_down' do
    context 'when no tunnel identifier is given' do
      it 'raises an argument error' do
        expect { client.tunnel_down }.to raise_error(ArgumentError)
      end
    end

    context 'when a tunnel identifier is given' do
      context 'when the tunnel is up' do
        before do
          stub_request(:get, path).with(query: { cmd: :tunnel_down }.merge(@options))
                                  .to_return(body: fixture('gateway/tunnel_down.json'), headers: headers)
          @tunnel_command = client.tunnel_down(@options)
        end

        it 'makes the request' do
          expect(a_request(:get, path).with(query: { cmd: :tunnel_down }.merge(@options))).to have_been_made
        end

        it 'returns a Gateway::API::Tunnel object' do
          expect(@tunnel_command).to be_a(Gateway::API::Tunnel)
        end

        it 'brings the tunnel down' do
          expect(@tunnel_command.ok?).to be true
        end
      end

      context 'when the tunnel is down' do
        before do
          stub_request(:get, path).with(query: { cmd: :tunnel_down }.merge(@options))
                                  .to_return(body: fixture('gateway/tunnel_down_fail.json'), headers: headers)
          @tunnel_command = client.tunnel_down(@options)
        end

        it 'makes the request' do
          expect(a_request(:get, path).with(query: { cmd: :tunnel_down }.merge(@options))).to have_been_made
        end

        it 'returns a Gateway::API::Tunnel object' do
          expect(@tunnel_command).to be_a(Gateway::API::Tunnel)
        end

        it 'does nothing' do
          expect(@tunnel_command.ok?).to be false
        end
      end
    end
  end

  describe '#tunnel_bounce' do
    before do
      stub_request(:get, path).with(query: { cmd: :tunnel_bounce }.merge(@options))
                              .to_return(body: fixture('gateway/tunnel_bounce.json'), headers: headers)
      @tunnel_command = client.tunnel_bounce(@options)
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: :tunnel_bounce }.merge(@options))).to have_been_made
    end

    it 'returns a Gateway::API::Tunnel object' do
      expect(@tunnel_command).to be_a(Gateway::API::Tunnel)
    end

    it 'bounces the tunnel' do
      expect(@tunnel_command.ok?).to be true
    end
  end

  describe '#tunnel_status' do
    context 'when no name or index is given' do
      it 'raises an argument error' do
        expect { client.tunnel_status }.to raise_error(ArgumentError)
      end
    end

    context 'when a tunnel identifier is given' do
      before do
        stub_request(:get, path).with(query: { cmd: :tunnel_status }.merge(@options))
                                .to_return(body: fixture('gateway/tunnel_status.json'), headers: headers)
        @tunnel = client.tunnel_status(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :tunnel_status }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::Tunnel object' do
        expect(@tunnel).to be_a(Gateway::API::Tunnel)
      end

      it 'matches the name' do
        expect(@tunnel.name).to eq @options[:name]
      end

      it 'has the tunnel status' do
        expect(@tunnel.up?).to be true
      end

      it 'has the tunnel uptime' do
        expect(@tunnel.uptime).to eq '2 Day, 20 Hr, 31 Min, 45 Sec'
      end
    end
  end

  describe '#tunnels' do
    before do
      stub_request(:get, path).with(query: { cmd: :tunnel_status, index: 0 })
                              .to_return(body: fixture('gateway/tunnel_status_index_0.json'), headers: headers)
      stub_request(:get, path).with(query: { cmd: :tunnel_status, index: 1 })
                              .to_return(body: fixture('gateway/tunnel_status_index_1.json'), headers: headers)
      stub_request(:get, path).with(query: { cmd: :tunnel_status, index: 2 })
                              .to_return(body: fixture('gateway/tunnel_status_index_2.json'), headers: headers)
      @tunnels = client.tunnels
    end

    it 'makes the requests' do
      expect(a_request(:get, path).with(query: { cmd: :tunnel_status, index: 0 })).to have_been_made
      expect(a_request(:get, path).with(query: { cmd: :tunnel_status, index: 1 })).to have_been_made
      expect(a_request(:get, path).with(query: { cmd: :tunnel_status, index: 2 })).to have_been_made
    end

    it 'returns the current count of tunnels configured for the device' do
      expect(@tunnels.count).to eq 2
    end

    it 'returns a list of tunnel objects' do
      @tunnels.each do |tunnel|
        expect(tunnel).to be_a(Gateway::API::Tunnel)
      end
    end
  end
end
