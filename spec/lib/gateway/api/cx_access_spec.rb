# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::CxAccess do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#cx_access' do
    before do
      @options = {
        access: 'up',
        dst_addr: '192.168.210.2',
        src_addr: '192.168.210.2',
        src_int: 'eth2',
        v4_src: '47.206.66.219',
        v6_src: '::1',
        maintenance_tunnel: 'false',
        drop_interface: 'false'
      }
    end

    context 'when all default options are given' do
      before do
        stub_request(:get, path).with(query: { cmd: :cx_access }.merge(@options))
                                .to_return(body: fixture('gateway/cx_access.json'), headers: headers)
        @results = client.cx_access(@options)
      end

      it 'makes the request' do
        expect(a_request(:get, path).with(query: { cmd: :cx_access }.merge(@options))).to have_been_made
      end

      it 'returns a Gateway::API::CxAccessResults object' do
        expect(@results).to be_a(Gateway::API::CxAccessResults)
      end

      it 'succeeds' do
        expect(@results.ok?).to be true
      end
    end
  end
end
