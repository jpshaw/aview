# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::ProfileQueries do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#full_profile_query' do
    before do
      stub_request(:get, path).with(query: { cmd: :query, type: :full })
                              .to_return(body: fixture('gateway/full_profile_query.json'), headers: headers)
      @results = client.full_profile_query
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: :query, type: :full })).to have_been_made
    end

    it 'returns a Gateway::API::ProfileQuery object' do
      expect(@results).to be_a(Gateway::API::ProfileQuery)
    end

    it 'is successful' do
      expect(@results.ok?).to be true
    end
  end

  describe '#partial_profile_query' do
    before do
      stub_request(:get, path).with(query: { cmd: :query, type: :partial })
                              .to_return(body: fixture('gateway/partial_profile_query.json'), headers: headers)
      @results = client.partial_profile_query
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: :query, type: :partial })).to have_been_made
    end

    it 'returns a Gateway::API::ProfileQuery object' do
      expect(@results).to be_a(Gateway::API::ProfileQuery)
    end

    it 'is successful' do
      expect(@results.ok?).to be true
    end
  end

  describe '#query_status' do
    before do
      stub_request(:get, path).with(query: { cmd: :query, type: :status })
                              .to_return(body: fixture('gateway/query_status.json'), headers: headers)
      @results = client.query_status
    end

    it 'makes the request' do
      expect(a_request(:get, path).with(query: { cmd: :query, type: :status })).to have_been_made
    end

    it 'returns a Gateway::API::ProfileQuery object' do
      expect(@results).to be_a(Gateway::API::ProfileQuery)
    end

    it 'is successful' do
      expect(@results.ok?).to be true
    end

    it 'returns the last query type' do
      expect(@results.type).to eq 'partial'
    end

    it 'returns the last query timestamp' do
      expect(@results.last_queried_at.utc).to eq Time.new(2015, 1, 16, 1, 46, 29).utc
    end

    it 'returns the next query timestamp' do
      expect(@results.next_query_at.utc).to eq Time.new(2015, 1, 17, 1, 42, 20).utc
    end
  end
end
