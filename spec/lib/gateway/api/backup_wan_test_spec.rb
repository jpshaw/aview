# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::BackupWanTest do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  describe '#backup_wan_test' do
    before do
      stub_request(:get, path).with(query: { cmd: :secondary_test })
                              .to_return(body: fixture('gateway/backup_wan_test.json'), headers: headers)
      @results = client.backup_wan_test
    end

    it 'succeeds' do
      expect(@results.ok?).to be true
    end
  end
end
