# frozen_string_literal: true

require 'rails_helper'

describe Gateway::API::Error do
  let(:client)  { build(:gateway_api_client) }
  let(:headers) { client.headers }
  let(:path)    { "#{client.base_uri}/cgi-bin/armt" }

  Gateway::API::Error::ERRORS.each do |status, exception|
    context "when HTTP status is #{status}" do
      before do
        stub_request(:get, path).with(query: { cmd: :upgrade_check })
                                .to_return(status: status, body: '{}', headers: headers)
      end

      it "raises #{exception}" do
        expect { client.upgrade_check }.to raise_error(exception)
      end
    end
  end
end
