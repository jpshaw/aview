# frozen_string_literal: true

require 'rails_helper'

describe AvailabilityReport, type: :integration do
  it_behaves_like 'a report' do
    let(:report_class) { AvailabilityReport }
  end

  describe 'data' do
    subject(:report) { AvailabilityReport.new(device_report) }

    let(:device_report) { create(:device_report, params: { 'stats_headers' => {} }) }
    let(:organization)  { create(:random_regular_organization) }
    let(:device)        { create(:netbridge_device, organization: organization) }
    let(:device_2)        { create(:netbridge_device, organization: organization) }
    let(:start_time) { 24.hours.ago.utc.beginning_of_hour }

    before do
      expect(device).to be_present
      allow(device_report).to receive(:devices).and_return(Device.where(nil))
      stub_report_file

      # Create Device Status Event
      Measurement::Base.client.send(:clean)
      create_netbridge_event_uuids
      Timecop.freeze(start_time.advance(hours: 3))
      device.alert_up
      device_2.alert_down
      Timecop.return

      report.generate!
      device_report.reload
    end
  end
end
