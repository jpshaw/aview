# frozen_string_literal: true

require 'rails_helper'
require 'imports/organizations_info_importer'

describe OrganizationsInfoImporter do
  describe '#import' do
    context 'without a device organization name' do
      it 'raises an error' do
        expect { subject.import({}) }.to raise_error(StandardError)
      end
    end

    context 'with a device organization name' do
      let(:root_organization)   { create(:organization) }
      let(:parent_organization) { create(:random_regular_organization) }
      let(:device_organization) { create(:random_regular_organization) }

      before do
        allow(subject).to receive(:root_org).and_return(root_organization)
      end

      it 'creates an organization for the device organization name if one does not exist' do
        name = Faker::Lorem.word
        expect(Organization.find_by(name: name)).to be nil
        subject.import(device_organization_name: name)
        expect(Organization.find_by(name: name)).to be_present
      end

      it 'creates an organization for the parent organization name if one does not exist' do
        name = Faker::Lorem.word
        expect(Organization.find_by(name: name)).to be nil
        subject.import(device_organization_name: device_organization.name, parent_organization_name: name)
        expect(Organization.find_by(name: name)).to be_present
      end

      it 'returns an organization' do
        expect(subject.import(device_organization_name: device_organization.name).class).to eq Organization
      end

      context 'when an organization does not have an import name' do
        let(:orgs) { [device_organization, parent_organization] }

        before do
          orgs.each { |org| org.update_column(:import_name, nil) }
        end

        it "assigns the organization's name to the import name" do
          orgs.each { |org| expect(org.import_name).not_to eq org.name }
          subject.import(
            device_organization_name: device_organization.name,
            parent_organization_name: parent_organization.name
          )
          [device_organization, parent_organization].each { |org| org.reload; expect(org.import_name).to eq org.name }
        end
      end

      context 'without a parent organization name' do
        let(:args) { { device_organization_name: device_organization.name } }

        before do
          expect(device_organization.parent).to be nil
        end

        it 'makes the device organization a child of the root organization' do
          subject.import(args)
          device_organization.reload
          expect(device_organization.parent).to eq root_organization
        end
      end

      context 'with a parent organization name' do
        let(:args) do
          {
            device_organization_name: device_organization.name,
            parent_organization_name: parent_organization.name
          }
        end

        before do
          expect(device_organization.parent).to be nil
          expect(parent_organization.parent).to be nil
        end

        it 'makes the device organization a child of the parent organization' do
          subject.import(args)
          device_organization.reload
          expect(device_organization.parent).to eq parent_organization
        end

        it 'makes the parent organization a child of the root organization' do
          subject.import(args)
          parent_organization.reload
          expect(parent_organization.parent).to eq root_organization
        end
      end
    end
  end
end
