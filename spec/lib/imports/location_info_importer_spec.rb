# frozen_string_literal: true

require 'rails_helper'
require 'imports/location_info_importer'

describe LocationInfoImporter do
  describe '#import' do
    let(:address_info) do
      {
        address_1:    Faker::Address.street_address,
        address_2:    Faker::Address.secondary_address,
        city:         Faker::Address.city,
        state:        Faker::Address.state,
        postal_code:  Faker::Address.zip_code,
        country:      Faker::Address.country
      }
    end

    context 'without an address_1 value' do
      before do
        address_info[:address_1] = nil
      end

      it 'does not find or create a location' do
        expect(Locations::Upsert).not_to receive(:execute)
        subject.import address_info
      end
    end

    context 'without a city value' do
      before do
        address_info[:city] = nil
      end

      it 'does not find or create a location' do
        expect(Locations::Upsert).not_to receive(:execute)
        subject.import address_info
      end
    end

    context 'with address_1 and city values' do
      before do
        create(:random_country, name: address_info[:country])
      end

      it 'finds or create a location' do
        location = double('Locations::Upsert')
        allow(Locations::Upsert).to receive(:new).and_return(location)
        allow(location).to receive(:execute)
        subject.import address_info
      end

      it 'returns a location if a matching one is found' do
        location = Locations::Upsert.new(address_info).execute
        expect(subject.import(address_info)).to eq location
      end

      it 'creates a location if a matching one is not found' do
        expect(Location.find_by(address_1: address_info[:address_1])).to be nil
        expect(subject.import(address_info).class).to eq Location
        expect(Location.find_by(address_1: address_info[:address_1])).to be_present
      end

      it 'returns a created location if a matching one is not found' do
        expect(Location.find_by(address_1: address_info[:address_1])).to be nil
        location = subject.import(address_info)
        expect(location).to eq Location.find_by(address_1: address_info[:address_1])
      end
    end
  end
end
