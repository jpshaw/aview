# frozen_string_literal: true

require 'rails_helper'
require 'imports/netbridge_configuration_ping_test_info_importer'

describe NetbridgeConfigurationPingTestInfoImporter do
  # TODO: Un-comment when the importer goes active.
  # describe '#import' do

  #   let(:configuration) { create(:netbridge_configuration)}
  #   let(:ping_value)    { Faker::Lorem.word }

  #   before :each do
  #     # to_s because the value could be nil and 'match' would fail.
  #     expect(configuration.ping_test.to_s).not_to match ping_value
  #   end

  #   context 'when ping info is present' do

  #     let(:ping_info)  { {ping_value: ping_value} }

  #     it 'the info is added to the configuration ping_test value' do
  #       subject.import(ping_info, configuration)
  #       configuration.reload
  #       expect(configuration.ping_test).to match ping_value
  #     end

  #   end

  #   context 'when ping info is not present' do

  #     it 'no info is added to the configuration ping_test value' do
  #       ping_test = configuration.ping_test
  #       subject.import({}, configuration)
  #       configuration.reload
  #       expect(configuration.ping_test).to eq ping_test
  #     end

  #   end

  # end
end
