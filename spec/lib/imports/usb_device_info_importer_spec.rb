# frozen_string_literal: true

require 'rails_helper'
require 'imports/smx_device_info_importer'

describe SmxDeviceInfoImporter do
  describe '#import' do
    let(:device)        { create(:netbridge_device) }
    let(:organization)  { device.organization }
    let(:mac)           { device.mac }
    let(:is_deployed)   { true }
    let(:location)      { create(:location, organization: organization) }
    let(:site)          { create(:site,     organization: organization) }
    let(:arguments) do
      [
        { mac: mac, is_deployed: is_deployed },
        organization,
        location,
        site
      ]
    end

    it 'uses an existing Netbridge device for the MAC if one exists' do
      expect do
        subject.import(*arguments)
      end.to change(Netbridge, :count)
    end

    it 'uses a new Netbridge device if one does not exist for the MAC' do
      arguments[0][:mac] = '002704000123'
      expect do
        subject.import(*arguments)
      end.to change(Netbridge, :count).by(1)
    end

    it 'returns a Netbridge device' do
      expect(subject.import(*arguments).class).to eq Netbridge
    end

    it 'knows if the device being used is a new record' do
      allow(Netbridge).to receive(:find_by_mac).and_return(device)
      subject.import(*arguments)
      expect(subject.new_record?).to be false

      mac                   = device.mac
      mac[-1]               = mac.ends_with?('0') ? '1' : '0'
      arguments.first[:mac] = mac

      allow(Netbridge).to receive(:find_by_mac).and_return(nil)
      subject.import(*arguments)
      expect(subject.new_record?).to be true
    end

    it "sets the device's organization" do
      netbridge_device = subject.import(*arguments)
      expect(netbridge_device.organization).to eq organization
    end

    it "sets the device's location" do
      netbridge_device = subject.import(*arguments)
      expect(netbridge_device.location).to eq location
    end

    it "sets the device's site" do
      netbridge_device = subject.import(*arguments)
      expect(netbridge_device.site).to eq site
    end

    it "sets the device's is_deployed value" do
      netbridge_device = subject.import(*arguments)
      expect(netbridge_device.is_deployed).to eq is_deployed
    end
  end
end
