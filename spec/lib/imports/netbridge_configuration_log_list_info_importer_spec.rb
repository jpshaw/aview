# frozen_string_literal: true

require 'rails_helper'
require 'imports/netbridge_configuration_log_list_info_importer'

describe NetbridgeConfigurationLogListInfoImporter do
  describe '#import' do
    let(:configuration) { create(:netbridge_configuration) }
    let(:log_value)     { Faker::Lorem.word }

    before do
      # to_s because the value could be nil and 'match' would fail.
      expect(configuration.log_list.to_s).not_to match log_value
    end

    context 'when log info is present' do
      let(:log_info)  { { log_value: log_value } }

      it 'the info is added to the configuration log_list value' do
        subject.import(log_info, configuration)
        configuration.reload
        expect(configuration.log_list).to match log_value
      end
    end

    context 'when log info is not present' do
      it 'no info is added to the configuration log_list value' do
        log_list = configuration.log_list
        subject.import({}, configuration)
        configuration.reload
        expect(configuration.log_list).to eq log_list
      end
    end
  end
end
