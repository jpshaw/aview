# frozen_string_literal: true

require 'rails_helper'
require 'imports/netbridge_configuration_info_importer'

describe NetbridgeConfigurationInfoImporter do
  def random_boolean
    [true, false].sample
  end

  describe '#import' do
    let(:organization)        { create(:random_regular_organization) }
    let(:configuration)       { create(:netbridge_configuration) }
    let(:configuration_name)  { Faker::Lorem.word }
    let(:attributes_info)     do
      {
        dlm:            Faker::Lorem.word,
        xma:            Faker::Lorem.word,
        xmu:            Faker::Lorem.word,
        xmp:            Faker::Lorem.word,
        rbt:            random_boolean,
        remote_control: random_boolean,
        sticky_apn:     random_boolean,
        ippassthrough:  random_boolean,
        usb_adjust:     Faker::Lorem.word[0, 10],
        gip_secondary:  random_boolean,
        mtu:            Faker::Lorem.word,
        img2:           Faker::Lorem.word,
        cfg:            Faker::Lorem.word,
        hostname:       Faker::Lorem.word,
        xmc:            Faker::Lorem.word,
        dnslist:        Faker::Lorem.word,
        dhcp_lease_max: rand(10),
        drs:            Faker::Lorem.word,
        dre:            Faker::Lorem.word,
        drp:            Faker::Lorem.word,
        drn:            Faker::Lorem.word,
        gip:            Faker::Lorem.word
      }
    end
    let(:configuration_info) { attributes_info.merge(name: configuration_name) }

    context 'when the configuration name already exists for the organization' do
      it 'returns the netbridge configuration for the name' do
        expect(subject.import({ name: configuration.name }, configuration.organization)).to eq configuration
      end
    end

    context 'when the configuration name does not exist for the organization' do
      before do
        expect(organization.netbridge_configurations.find_by(name: configuration_name)).to be nil
      end

      it 'creates a netbridge configuration for the name' do
        subject.import(configuration_info, organization)
        expect(organization.netbridge_configurations.find_by(name: configuration_name)).to be_present
      end

      it 'returns the created netbridge configuration' do
        configuration = subject.import(configuration_info, organization)
        expect(configuration.class).to eq NetbridgeConfiguration
        expect(organization.netbridge_configurations.find_by(name: configuration_name)).to eq configuration
      end
    end

    it 'sets the configuration attributes to the values passed' do
      configuration = subject.import(configuration_info, organization)

      configuration_info.each do |key, value|
        expect(configuration.__send__(key)).to eq value
      end
    end

    it 'makes sure boolean fields have boolean values' do
      expect(subject).to receive(:to_boolean).exactly(5).times
      subject.import(configuration_info, organization)
    end

    it 'translates the ethernet port speed' do
      NetbridgeConfigurationInfoImporter::ETHERNET_PORT_SPEEDS.each do |key, value|
        configuration = subject.import({ name: configuration_name, eth: key }, organization)
        expect(configuration.__send__(:eth)).to eq value
      end
    end

    it 'makes the ethernet port speed value nil if it cannot translate it' do
      configuration = subject.import({ name: configuration_name, eth: 'INVALID_ETH' }, organization)
      expect(configuration.__send__(:eth)).to be nil
    end

    it 'translates the cell network type' do
      NetbridgeConfigurationInfoImporter::CELL_NETWORK_TYPES.each do |key, value|
        configuration = subject.import({ name: configuration_name, rat: key }, organization)
        expect(configuration.__send__(:rat)).to eq value
      end
    end

    it 'makes the cell network type value nil if it cannot translate it' do
      configuration = subject.import({ name: configuration_name, rat: 'INVALID_RAT' }, organization)
      expect(configuration.__send__(:rat)).to be nil
    end
  end

  describe '#to_boolean' do
    it 'returns true when argument is truthy' do
      expect(subject.__send__(:to_boolean, 'Y')).to be true
    end

    it 'returns false when argument is falsy' do
      expect(subject.__send__(:to_boolean, 'N')).to be false
    end

    it 'returns nil when argument is neither truthy nor falsy' do
      expect(subject.__send__(:to_boolean, '')).to be nil
    end
  end
end
