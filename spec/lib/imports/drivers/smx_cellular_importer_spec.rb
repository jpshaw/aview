# frozen_string_literal: true

require 'rails_helper'
require 'imports/drivers/smx_cellular_importer'
require 'imports/smx_device_info_importer'
require 'imports/organizations_info_importer'
require 'imports/location_info_importer'
require 'imports/site_info_importer'
require 'imports/netbridge_configuration_info_importer'
require 'imports/netbridge_configuration_log_list_info_importer'
require 'imports/netbridge_configuration_ping_test_info_importer'

describe SmxCellularImporter do
  it 'cannot be instantiated' do
    expect { described_class.new }.to raise_error(NoMethodError)
  end

  describe 'Class methods' do
    describe '.import_file' do
      let(:file_path) { '/path/to/import/file' }
      let(:importer)  { described_class.__send__(:new, file_path) }

      it 'creates an instance of the importer' do
        allow(importer).to receive(:process)
        allow(described_class).to receive(:new).with(file_path).and_return(importer)
        described_class.import_file(file_path)
        expect(described_class).to have_received(:new).with(file_path)
      end

      it 'processes a CSV import file' do
        allow(CSV).to receive(:foreach) # So we don't get a 'file not found' error.
        expect_any_instance_of(described_class).to receive(:process)
        described_class.import_file(file_path)
      end
    end
  end

  describe 'Instance methods' do
    subject         { described_class.__send__(:new, file_path) }

    let(:file_path) { "#{Rails.root}/spec/support/fixtures/files/smx_cellular_importer.csv" }

    describe '#process' do
      it 'processes a csv file' do
        expect(CSV).to receive(:foreach)
        subject.process
      end

      it 'processes CSV records with device information' do
        expect(subject).to receive(:process_device_row).twice
        subject.process
      end

      it 'processes CSV records with syslog information' do
        expect(subject).to receive(:skipped?).and_return(false).at_least(:once)
        expect(subject).to receive(:process_syslog_row).once
        subject.process
      end

      it 'adds device mac addresses to list of scanned devices' do
        processed = subject.process
        expect(processed[:devices][:scanned]).to be_present
      end

      it 'checks if devices should be processed' do
        expect(subject).to receive(:skippable?).twice
        subject.process
      end

      it 'does not process devices that should be skipped' do
        expect(subject).to receive(:skippable?).twice.and_return(true)
        expect(subject).to receive(:skipped?).exactly(4).times.and_return(true)
        expect(subject).not_to receive(:process_device_row)
        expect(subject).not_to receive(:process_syslog_row)
        expect(subject).not_to receive(:process_ping_row)
        subject.process
      end

      it 'processes devices that should not be skipped' do
        expect(subject).to receive(:skippable?).twice.and_return(false)
        expect(subject).to receive(:skipped?).exactly(4).times.and_return(false)
        expect(subject).to receive(:process_device_row).twice
        expect(subject).to receive(:process_syslog_row)
        # TODO:  Un-comment this line when process_ping_row is enabled in SmxCellularImport.
        # expect(subject).to receive(:process_ping_row)
        subject.process
      end

      it 'deletes unprocessed devices' do
        expect(CSV).to receive(:foreach)
        expect(subject).to receive(:delete_unprocessed_devices)
        subject.process
      end

      it 'returns a hash with information about the processed devices' do
        expect(CSV).to receive(:foreach)
        expect(subject.process.class).to eq Hash
      end
    end

    describe '#device_row?' do
      context 'when the CSV record contains device info' do
        before do
          expect(subject).to receive(:row_type).and_return(SmxCellularImporter::DEVICE_INFO_TYPE)
        end

        it 'returns true' do
          expect(subject.__send__(:device_row?)).to be true
        end
      end

      context 'when the CSV record does not contains device info' do
        before do
          expect(subject).to receive(:row_type).and_return('NOT_DEVICE_INFO_FLAG')
        end

        it 'returns false' do
          expect(subject.__send__(:device_row?)).to be false
        end
      end
    end

    describe '#syslog_row?' do
      context 'when the CSV record contains syslog info' do
        before do
          expect(subject).to receive(:row_type).and_return(SmxCellularImporter::SYSLOG_INFO_TYPE)
        end

        it 'returns true' do
          expect(subject.__send__(:syslog_row?)).to be true
        end
      end

      context 'when the CSV record does not contains syslog info' do
        before do
          expect(subject).to receive(:row_type).and_return('NOT_SYSLOG_INFO_FLAG')
        end

        it 'returns false' do
          expect(subject.__send__(:syslog_row?)).to be false
        end
      end
    end

    describe '#ping_row?' do
      context 'when the CSV record contains ping info' do
        before do
          expect(subject).to receive(:row_type).and_return(SmxCellularImporter::PING_INFO_TYPE)
        end

        it 'returns true' do
          expect(subject.__send__(:ping_row?)).to be true
        end
      end

      context 'when the CSV record does not contains ping info' do
        before do
          expect(subject).to receive(:row_type).and_return('NOT_PING_INFO_FLAG')
        end

        it 'returns false' do
          expect(subject.__send__(:ping_row?)).to be false
        end
      end
    end

    describe '#process_device_row' do
      let(:mac) { '001122334455' }

      before do
        allow_any_instance_of(SmxDeviceInfoImporter).to receive(:import).and_return(Device.new)
        allow(subject).to receive(:device_row)
        allow(subject).to receive(:organization)
        allow(subject).to receive(:location)
        allow(subject).to receive(:site)
        allow(subject).to receive(:configuration)
      end

      it 'gets device row information' do
        expect(subject).to receive(:device_row)
        subject.__send__ :process_device_row
      end

      it 'gets an organization' do
        expect(subject).to receive(:organization)
        subject.__send__ :process_device_row
      end

      it 'gets a location' do
        expect(subject).to receive(:location)
        subject.__send__ :process_device_row
      end

      it 'gets a site' do
        expect(subject).to receive(:site)
        subject.__send__ :process_device_row
      end

      it 'imports a device' do
        expect_any_instance_of(SmxDeviceInfoImporter).to receive(:import)
        subject.__send__ :process_device_row
      end

      context 'when a device is created' do
        before do
          allow(subject).to receive(:mac).and_return(mac)
          subject.__send__ :processed # Set the processed hash so we can check it's contents before testing
        end

        context 'when the device has buildroot platform' do
          let(:device) { Netbridge.new }

          before do
            allow_any_instance_of(SmxDeviceInfoImporter).to receive(:import).and_return(device)
            expect(device.platform).to eq(:buildroot)
          end

          it 'gets a configuration' do
            expect(subject).to receive(:configuration)
            subject.__send__ :process_device_row
          end
        end

        context 'when the device does not have buildroot platform' do
          let(:device) { Device.new }

          before do
            allow_any_instance_of(SmxDeviceInfoImporter).to receive(:import).and_return(device)
            expect(device.platform).not_to eq(:buildroot)
          end

          it 'does not get a configuration' do
            expect(subject).not_to receive(:configuration)
            subject.__send__ :process_device_row
          end
        end

        context 'when the device is new' do
          before do
            expect_any_instance_of(SmxDeviceInfoImporter).to receive(:new_record?).and_return(true)
          end

          it "adds the device's MAC to list of added devices" do
            processed = subject.__send__ :processed
            expect(processed[:devices][:added]).not_to include mac
            subject.__send__ :process_device_row
            processed = subject.__send__ :processed
            expect(processed[:devices][:added]).to include mac
          end
        end

        context 'when the device already existed' do
          before do
            expect_any_instance_of(SmxDeviceInfoImporter).to receive(:new_record?).and_return(false)
          end

          it "does not add the device's MAC to list of added devices" do
            processed = subject.__send__ :processed
            expect(processed[:devices][:added]).not_to include mac
            subject.__send__ :process_device_row
            processed = subject.__send__ :processed
            expect(processed[:devices][:added]).not_to include mac
          end
        end
      end

      context 'when a device is is not created' do
        before do
          expect_any_instance_of(SmxDeviceInfoImporter).to receive(:import).and_return(nil)
        end

        it 'raises an error' do
          expect { subject.__send__ :process_device_row }.to raise_error(StandardError)
        end
      end
    end

    describe '#device_row' do
      let(:row_keys) { %i[mac is_deployed] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with device information' do
        row = subject.__send__ :device_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#organization' do
      before do
        allow(subject).to receive(:organization_row).and_return(
          device_organization_name: Faker::Lorem.word,
          parent_organization_name: Faker::Lorem.word
        )
      end

      it 'imports organizations' do
        expect_any_instance_of(OrganizationsInfoImporter).to receive(:import)
        subject.__send__ :organization
      end

      it 'returns an organization' do
        expect(subject.__send__(:organization).class).to eq Organization
      end
    end

    describe '#organization_row' do
      let(:row_keys) { %i[device_organization_name parent_organization_name] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with organizations information' do
        row = subject.__send__ :organization_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#location' do
      before do
        country = create(:country)
        allow(subject).to receive(:location_row).and_return(
          address_1:    Faker::Address.street_address,
          address_2:    Faker::Address.secondary_address,
          city:         Faker::Address.city,
          state:        Faker::Address.state,
          postal_code:  Faker::Address.zip_code,
          country:      country.name
        )
      end

      it 'imports a location' do
        expect_any_instance_of(LocationInfoImporter).to receive(:import)
        subject.__send__ :location
      end

      it 'returns a location' do
        expect(subject.__send__(:location).class).to eq Location
      end
    end

    describe '#location_row' do
      let(:row_keys) { %i[address_1 address_2 city state postal_code country] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with location information' do
        row = subject.__send__ :location_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#site' do
      let(:organization) { create(:random_regular_organization) }

      before do
        allow(subject).to receive(:site_row).and_return(name: Faker::Lorem.word)
        allow(subject).to receive(:device_organization).and_return(organization)
      end

      it 'imports a site' do
        expect_any_instance_of(SiteInfoImporter).to receive(:import)
        subject.__send__ :site
      end

      it 'returns a site' do
        expect(subject.__send__(:site).class).to eq Site
      end
    end

    describe '#site_row' do
      let(:row_keys) { [:name] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with location information' do
        row = subject.__send__ :site_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#configuration' do
      let(:organization) { create(:random_regular_organization) }

      before do
        allow(subject).to receive(:configuration_row).and_return(name: 'name')
        allow(subject).to receive(:device_organization).and_return(organization)
      end

      it 'imports a netbridge configuration' do
        expect_any_instance_of(NetbridgeConfigurationInfoImporter).to receive(:import)
        subject.__send__ :configuration
      end

      it 'returns a netbridge configuration' do
        expect(subject.__send__(:configuration).class).to eq NetbridgeConfiguration
      end
    end

    describe '#configuration_row' do
      let(:row_keys) do
        %i[
          name dlm xma xmu xmp rbt remote_control sticky_apn ippassthrough
          usb_adjust eth gip_secondary rat mtu img2 cfg hostname xmc dnslist
          dhcp_lease_max drs dre drp drn gip
        ]
      end

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with netbridge configuration information' do
        row = subject.__send__ :configuration_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#process_syslog_row' do
      let(:device) { create(:netbridge_device) }

      before do
        allow(subject).to receive(:syslog_row).and_return({})
        allow(subject).to receive(:device).and_return(device)
      end

      it 'imports a netbridge configuration log list value' do
        expect_any_instance_of(NetbridgeConfigurationLogListInfoImporter).to receive(:import)
        subject.__send__ :process_syslog_row
      end
    end

    describe '#syslog_row' do
      let(:row_keys) { [:log_value] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with syslog information' do
        row = subject.__send__ :syslog_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#process_ping_row' do
      let(:device) { create(:netbridge_device) }

      before do
        allow(subject).to receive(:ping_row).and_return({})
        allow(subject).to receive(:device).and_return(device)
      end

      it 'imports a netbridge configuration ping test value' do
        expect_any_instance_of(NetbridgeConfigurationPingTestInfoImporter).to receive(:import)
        subject.__send__ :process_ping_row
      end
    end

    describe '#ping_row' do
      let(:row_keys) { [:ping_value] }

      before do
        allow(subject).to receive(:row).and_return({})
      end

      it 'returns a hash with syslog information' do
        row = subject.__send__ :ping_row
        row_keys.each { |key| expect(row.key?(key)) }
      end
    end

    describe '#delete_unprocessed_devices' do
      let(:netbridge_device)  { create(:netbridge_device) }
      let(:cellular_device)   { create(:cellular_device) }
      let(:test_device)       { create(:test_device) }

      it 'deletes Netbridge and Cellular devices which MAC is not in the import file' do
        allow(subject).to receive(:removable_macs).and_return([netbridge_device.mac, cellular_device.mac])
        subject.__send__ :delete_unprocessed_devices
        expect(Device.find_by(mac: netbridge_device.mac)).to be nil
        expect(Device.find_by(mac: cellular_device.mac)).to be nil
      end

      it 'does not delete Netbridge or Cellular devices which MAC is in the import file' do
        allow(subject).to receive(:removable_macs).and_return([test_device.mac])
        subject.__send__ :delete_unprocessed_devices
        expect(Device.find_by(mac: netbridge_device.mac)).to be_present
        expect(Device.find_by(mac: cellular_device.mac)).to be_present
      end

      it 'adds the MAC of the deleted devices to the list of removed devices' do
        processed = subject.__send__ :processed
        expect(processed[:devices][:removed]).not_to include netbridge_device.mac
        allow(subject).to receive(:removable_macs).and_return([netbridge_device.mac])
        subject.__send__ :delete_unprocessed_devices
        processed = subject.__send__ :processed
        expect(processed[:devices][:removed]).to include netbridge_device.mac
      end

      it 'does not add the MAC of scanned devices to the list of removed devices' do
        processed = subject.__send__ :processed
        expect(processed[:devices][:removed]).not_to include netbridge_device.mac
        allow(subject).to receive(:removable_macs).and_return([test_device.mac])
        subject.__send__ :delete_unprocessed_devices
        processed = subject.__send__ :processed
        expect(processed[:devices][:removed]).not_to include netbridge_device.mac
      end
    end

    describe '#removable_macs' do
      let(:scanned_mac) { 'SCANNED_MAC' }

      before do
        allow(subject).to receive(:processed).and_return(devices: { scanned: [scanned_mac] })
      end

      it 'gets devices macs' do
        expect(subject).to receive(:devices_macs).and_return([])
        subject.__send__ :removable_macs
      end

      it 'returns a list of devices MACs in the DB that were not in the import file' do
        first_mac   = '111111111111'
        second_mac  = '222222222222'
        allow(subject).to receive(:devices_macs).and_return([first_mac, second_mac, scanned_mac])
        expect(
          subject.__send__(:removable_macs).sort
        ).to eq(
          [first_mac, second_mac]
        )
      end
    end

    describe '#devices_macs' do
      it 'plucks Netbridge devices macs' do
        expect(Netbridge).to receive(:pluck).with(:mac).and_return([])
        subject.__send__ :devices_macs
      end

      it 'plucks Cellular devices macs' do
        expect(Cellular).to receive(:pluck).with(:mac).and_return([])
        subject.__send__ :devices_macs
      end

      it 'returns an array of Netbridge and Cellular devices macs' do
        netbridge_device  = create(:netbridge_device)
        cellular_device   = create(:cellular_device)
        test_device       = create(:test_device) # Control. This device's MAC is not included.
        expect(
          subject.__send__(:devices_macs).sort
        ).to eq(
          [netbridge_device.mac, cellular_device.mac].sort
        )
      end
    end

    describe '#skippable?' do
      let(:mac) { '001122334455' }

      before do
        allow(subject).to receive(:mac).and_return(mac)
      end

      context 'without device organization name' do
        before do
          allow(subject).to receive(:device_organization_name?).and_return(false)
        end

        it 'does not check if device is armt enabled' do
          expect(subject).not_to receive(:armt_enabled?)
          subject.__send__ :skippable?
        end

        it 'adds device mac to the list of skipped devices' do
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).not_to include mac
          subject.__send__ :skippable?
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).to include mac
        end

        it 'returns true' do
          expect(subject.__send__(:skippable?)).to be true
        end
      end

      context 'with armt disabled' do
        before do
          allow(subject).to receive(:device_organization_name?).and_return(true)
          allow(subject).to receive(:armt_enabled?).and_return(false)
        end

        it 'adds device mac to the list of skipped devices' do
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).not_to include mac
          subject.__send__ :skippable?
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).to include mac
        end

        it 'returns true' do
          expect(subject.__send__(:skippable?)).to be true
        end
      end

      context 'with device organization name and with armt enabled' do
        before do
          allow(subject).to receive(:device_organization_name?).and_return(true)
          allow(subject).to receive(:armt_enabled?).and_return(true)
        end

        it 'does not add device mac to the list of skipped devices' do
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).not_to include mac
          subject.__send__ :skippable?
          processed = subject.__send__ :processed
          expect(processed[:devices][:skipped]).not_to include mac
        end

        it 'returns false' do
          expect(subject.__send__(:skippable?)).to be false
        end
      end
    end

    describe '#skipped?' do
      let(:mac) { '001122334455' }

      before do
        allow(subject).to receive(:mac).and_return(mac)
      end

      context 'when mac is in the list of skipped devices' do
        before do
          allow(subject).to receive(:processed).and_return(devices: { skipped: [mac] })
        end

        it 'returns true' do
          expect(subject.__send__(:skipped?)).to be true
        end
      end

      context 'when mac is not in the list of skipped devices' do
        before do
          allow(subject).to receive(:processed).and_return(devices: { skipped: [] })
        end

        it 'returns false' do
          expect(subject.__send__(:skipped?)).to be false
        end
      end
    end
  end
end
