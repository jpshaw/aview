# frozen_string_literal: true

require 'rails_helper'
require 'imports/site_info_importer'

describe SiteInfoImporter do
  describe '#import' do
    context 'when the site name already exists for the organization' do
      let(:site) { create(:site_with_random_regular_organization) }

      it 'returns the site for the name' do
        expect(subject.import({ name: site.name }, site.organization)).to eq site
      end
    end

    context 'when the site name does not exist for the organization' do
      let(:organization)  { create(:random_regular_organization) }
      let(:site_name)     { Faker::Lorem.word }

      before do
        expect(organization.sites.find_by(name: site_name)).to be nil
      end

      it 'creates a site for the name' do
        subject.import({ name: site_name }, organization)
        expect(organization.sites.find_by(name: site_name)).to be_present
      end

      it 'returns the created site' do
        site = subject.import({ name: site_name }, organization)
        expect(site.class).to eq Site
        expect(organization.sites.find_by(name: site_name)).to eq site
      end
    end
  end
end
