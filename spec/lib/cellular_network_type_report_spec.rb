# frozen_string_literal: true

require 'rails_helper'

describe CellularNetworkTypeReport do
  it_behaves_like 'a report' do
    let(:report_class) { described_class } # Used in shared examples.
  end

  describe 'data' do
    subject(:report) { described_class.new(device_report) }

    let(:device_report) { create(:device_report, params: { 'stats_headers' => {} }) }
    let(:organization)  { create(:random_regular_organization) }
    let(:device)        { create(:netbridge_device, organization: organization) }

    before do
      expect(device).to be_present
      allow(device_report).to receive(:devices).and_return(Device.where(nil))
      stub_report_file
      report.generate!
      device_report.reload
    end

    describe '#initial_network' do
      let(:network_name)  { 'A NETWORK NAME' }
      let(:network_event) do
        DeviceEvent.new(
          raw_data: {
            'modem' => {
              CellularHelper::CELLULAR_FLAG => network_name
            }
          },
          created_at: Time.now.utc - 1.day
        )
      end

      before do
        allow_any_instance_of(described_class)
          .to receive(:initial_device_event_for_flag)
          .and_return(network_event)
      end

      context 'when initial down event is before the network event' do
        let(:down_event) { DeviceEvent.new(created_at: network_event.created_at - 1.minute) }

        before do
          expect(down_event.created_at).to be < network_event.created_at

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns the event's network name" do
          expect(report.__send__(:initial_network, device)).to eq(network_name)
        end
      end

      context 'when initial down event happens at the same time as the network event' do
        let(:down_event) { DeviceEvent.new(created_at: network_event.created_at) }

        before do
          expect(down_event.created_at).to eq(network_event.created_at)

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns the event's network name" do
          expect(report.__send__(:initial_network, device)).to eq(network_name)
        end
      end

      context 'when initial down is after the network event' do
        let(:down_event) { DeviceEvent.new(created_at: network_event.created_at + 1.minute) }

        before do
          expect(down_event.created_at).to be > network_event.created_at

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns '#{CellularNetworkTypeReport::NO_NETWORK}' as the network name" do
          expect(report.__send__(:initial_network, device)).to eq(CellularNetworkTypeReport::NO_NETWORK)
        end
      end
    end

    describe '#generate_data' do
      let(:initial_interval) { (Time.now.utc - 2.hours).beginning_of_hour }

      # We only need 2 intervals for our tests. We separate them by 1 hour for convenience.
      let(:intervals_template) do
        {
          initial_interval          => Hash.new(0),
          initial_interval + 1.hour => Hash.new(0)
        }
      end

      before do
        expect(device.events.size).to eq(0)

        # Our report starts 15 minutes before the initial interval for convenience.
        allow(report).to receive(:start_time).and_return(initial_interval - 15.minutes)
        allow(report).to receive(:intervals_template).and_return(intervals_template)
      end

      context 'with network activity between time range' do
        let(:result_intervals) do
          {
            intervals_template.keys.first => {
              CellularNetworkTypeReport::NO_NETWORK => 1800.0,
              'NETWORK B'                           => 1800.0
            },
            intervals_template.keys.last => {
              'NETWORK B' => 3600.0
            }
          }
        end

        let(:table_statistics) do
          {
            'no network'        => 25.0,
            'network b'         => 75.0,
            'most_used_network' => 'network b'
          }
        end

        let(:pie_chart_statistics) do
          [
            ['NETWORK B', 75.0],
            ['NO NETWORK', 25.0]
          ]
        end

        let(:trend_chart_statistics) do
          [
            {
              name: 'NETWORK B',
              data: [
                [intervals_template.keys.first.to_i * 1000, 50.0],
                [intervals_template.keys.last.to_i  * 1000, 100.0]
              ]
            },
            {
              name: 'NO NETWORK',
              data: [
                [intervals_template.keys.first.to_i * 1000, 50.0],
                [intervals_template.keys.last.to_i  * 1000, 0.0]
              ]
            }
          ]
        end

        before do
          device.events.create(
            created_at: initial_interval + 30.minutes,
            raw_data:   { 'modem' => { CellularHelper::CELLULAR_FLAG => 'NETWORK B' } }
          )
          expect(device.events.size).to eq(1)
          allow(report).to receive(:events_for_device).and_return(device.events)
          report.__send__(:generate_data)
        end

        it 'splits the network activity seconds between the different networks' do
          expect(report.__send__(:results).values.first).to eq(result_intervals)
        end

        # NOTE: I chose to test table, pie chart and trend chart contents here indirectly because
        #       setting up the environment for those method calls was going to take quite a long
        #       time.
        it 'generates table percentage statistical information' do
          expect(report.__send__(:table).first.last).to eq(table_statistics)
        end

        it 'generates pie chart percentage statistical information' do
          expect(report.__send__(:pie_chart)[:series].first[:data]).to eq(pie_chart_statistics)
        end

        it 'generates trend chart percentage statistical information' do
          allow(report).to receive(:networks).and_return(
            [
              CellularNetworkTypeReport::NO_NETWORK,
              'NETWORK B'
            ]
          )
          expect(report.__send__(:trend_chart)[:series]).to eq(trend_chart_statistics)
        end
      end

      context 'without network activity between time range' do
        let(:result_intervals) do
          {
            intervals_template.keys.first => {
              CellularNetworkTypeReport::NO_NETWORK => 3600.0
            },
            intervals_template.keys.last => {
              CellularNetworkTypeReport::NO_NETWORK => 3600.0
            }
          }
        end

        let(:table_statistics) do
          {
            'no network' => 100.0,
            'most_used_network' => 'no network'
          }
        end

        let(:pie_chart_statistics) { [['NO NETWORK', 100.0]] }

        let(:trend_chart_statistics) do
          [
            {
              name: 'NO NETWORK',
              data: [
                [intervals_template.keys.first.to_i * 1000, 100.0],
                [intervals_template.keys.last.to_i  * 1000, 100.0]
              ]
            }
          ]
        end

        before do
          report.__send__(:generate_data)
        end

        it "assigns all network activity seconds to the #{CellularNetworkTypeReport::NO_NETWORK} network" do
          expect(report.__send__(:results).values.first).to eq(result_intervals)
        end

        # NOTE: I chose to test table, pie chart and trend chart contents here indirectly because
        #       setting up the environment for those method calls was going to take quite a long
        #       time.
        it 'generates table percentage statistical information' do
          expect(report.__send__(:table).first.last).to eq(table_statistics)
        end

        it 'generates pie chart percentage statistical information' do
          expect(report.__send__(:pie_chart)[:series].first[:data]).to eq(pie_chart_statistics)
        end

        it 'generates trend chart percentage statistical information' do
          expect(report.__send__(:trend_chart)[:series]).to eq(trend_chart_statistics)
        end
      end
    end

    describe '#table' do
      describe 'row' do
        let(:row) { device_report.table.first }

        before do
          expect(device_report.table.size).to eq(1)
          expect(row).to be_present
        end

        it 'contains general device information' do
          expect(row[0]).to eq(device.mac)
          expect(row[1]).to eq(device.host)
          expect(row[2]).to be nil          # Category name: category not set.
          expect(row[3]).to be nil          # Model name: model not set.
          expect(row[4]).to eq(device.organization.name)
          expect(row[6]).to eq('Default')   # Site name: defaults to 'Default'.
          expect(row[7]).to eq(device.deployed? ? 'Deployed' : 'Undeployed')
          expect(row[8]).to eq(device.last_heartbeat_at)
        end

        it 'contains statistical information' do
          expect(row[9]).to be_present
          expect(row[9].keys.sort).to eq(['most_used_network', 'no network'])
        end
      end
    end

    describe '#pie_chart' do
      let(:pie_chart) { device_report.pie_chart }

      it 'contains a hash with information for Hicharts' do
        expect(pie_chart.class).to eq(Hash)
      end

      it 'is of type `pie`' do
        expect(pie_chart[:plotOptions].keys).to include(:pie)
      end

      it 'has title `Cellular Network Type Summary`' do
        expect(pie_chart[:title][:text]).to eq('Cellular Network Type Summary')
      end

      it 'displays network types usage' do
        expect(pie_chart[:series].first[:data].first.first).to eq('NO NETWORK')
      end
    end

    describe '#trend_chart' do
      let(:trend_chart) { device_report.trend_chart }

      it 'contains a hash with information for Hicharts' do
        expect(trend_chart.class).to eq(Hash)
      end

      it 'is of type `spline`' do
        expect(trend_chart[:chart][:type]).to eq('spline')
      end

      it 'has title `Cellular Network Type`' do
        expect(trend_chart[:title][:text]).to eq('Cellular Network Type')
      end

      it 'displays network usage values' do
        expect(trend_chart[:series].first[:name]).to eq('NO NETWORK')
      end
    end
  end
end
