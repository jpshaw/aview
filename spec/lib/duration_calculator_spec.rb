# frozen_string_literal: true

require 'rails_helper'

describe DurationCalculator do
  describe '.calculate' do
    let(:durations) do
      {
        '3w' => 1_814_400,
        '3d' => 259_200,
        '3h' => 10_800,
        '3m' => 180,
        '3s' => 3
      }
    end

    it 'raises an error with an invalid formatted string' do
      expect { described_class.calculate('invalid_formatted_string') }.to raise_error(StandardError)
    end

    it 'raises an error with an invalid duration' do
      expect { described_class.calculate('3x') }.to raise_error(StandardError)
    end

    it 'calculates duration for individual durations' do
      durations.each do |duration, seconds|
        expect(described_class.calculate(duration)).to eq(seconds)
      end
    end

    it 'calculates duration for combined durations' do
      expect(described_class.calculate(durations.keys.join)).to eq(durations.values.reduce(:+))
    end
  end
end
