# frozen_string_literal: true

require 'rails_helper'
require 'vig/spreadsheet'

describe VigSpreadsheet::Row do
  subject { described_class.new(data) }

  let(:data) { {} }

  describe '#name' do
    context 'when a name is not given' do
      before { data.delete(:name) }

      it 'returns a blank string' do
        expect(subject.name).to eq ''
      end
    end

    context 'when a name is given' do
      before { data.merge!(name: 'VARBA210') }

      it 'returns the name' do
        expect(subject.name).to eq 'VARBA210'
      end
    end

    context 'when the name has a dash' do
      before { data.merge!(name: 'VAUSYD10-V6') }

      it 'sets the name to the text before the dash' do
        expect(subject.name).to eq 'VAUSYD10'
      end
    end
  end

  describe '#location' do
    context 'when a location is not given' do
      before { data.delete(:location) }

      it 'returns a blank string' do
        expect(subject.location).to eq ''
      end
    end

    context 'when a location is given' do
      before { data.merge!(location: 'VIG #1 - LYNNWOOD, WA') }

      it 'returns the location' do
        expect(subject.location).to eq 'LYNNWOOD, WA'
      end
    end

    context 'when the an IPv6 location is given' do
      before { data.merge!(location: 'VIG #1 - SYDNEY IPV6') }

      it 'strips the IPV6 text' do
        expect(subject.location).to eq 'SYDNEY'
      end
    end
  end

  describe '#ip_address' do
    context 'when an ip address is not given' do
      before { data.delete(:ip_address) }

      it 'returns a blank string' do
        expect(subject.ip_address).to eq ''
      end
    end

    context 'when an IPv4 address is given' do
      before { data.merge!(ip_address: '166.72.94.16') }

      it 'returns a formatted ip address' do
        expect(subject.ip_address).to eq '166.72.94.16'
      end
    end

    context 'when a zero-padded IPv4 address is given' do
      before { data.merge!(ip_address: '032.103.082.071') }

      it 'returns a formatted ip address with leading zeros stripped' do
        expect(subject.ip_address).to eq '32.103.82.71'
      end
    end

    context 'when an IPv6 address is given' do
      before { data.merge!(ip_address: '2001:1890:111b:83:280:50ff:fe04:d771') }

      it 'returns a formatted ip address' do
        expect(subject.ip_address).to eq '2001:1890:111b:83:280:50ff:fe04:d771'
      end
    end

    context 'when a zero-padded IPv6 address is given' do
      before { data.merge!(ip_address: 'CAFE:1890:0C00:B501:0000:0000:0000:0002') }

      it 'returns a formatted ip address with leading zeros stripped' do
        expect(subject.ip_address).to eq 'cafe:1890:c00:b501::2'
      end
    end
  end

  describe '#header?' do
    context 'when the row name is not equal to the field name token' do
      before { data.merge!(name: 'not equal') }

      it 'returns false' do
        expect(subject.header?).to be false
      end
    end

    context 'when the row name is equal to the field name token' do
      before { data.merge!(name: VigSpreadsheet::FIELDS[:name]) }

      it 'returns true' do
        expect(subject.header?).to be true
      end
    end
  end
end
