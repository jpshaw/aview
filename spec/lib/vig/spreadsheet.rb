# frozen_string_literal: true

require 'rails_helper'
require 'vig/spreadsheet'

describe VigSpreadsheet do
  let(:file_path)           { "#{fixture_path}/files/test_vig_file.xlsx" }
  let(:digest_path)         { "#{fixture_path}/files/last_vig_spreadsheet_digest.txt" }
  let(:invalid_digest_path) { '/invalid/digest/path' }

  let(:current_digest)      { '3a537d3627b0a448a17eb0ebd1d541feef363766704cebfa6104dea81cf6bde1' }
  let(:last_digest)         { '4cf1522f35616951eb0e416404dae10fc3ff9a9a1aee848f5cc3755cb4a1ca3b' }

  before do
    allow(described_class).to receive(:path).and_return(file_path)
    allow(described_class).to receive(:digest_path).and_return(digest_path)
  end

  describe '#rows' do
    it 'returns the correct number of rows' do
      expect(subject.rows.count).to eq 7
    end

    it 'returns an array of VigSpreadsheet::Row objects' do
      expect(subject.rows.map(&:class).uniq).to eq [VigSpreadsheet::Row]
    end
  end

  describe '.exists?' do
    context 'when the spreadsheet exists' do
      it 'returns true' do
        expect(subject.class.exists?).to be true
      end
    end

    context 'when the spreadsheet does not exist' do
      let(:invalid_file_path) { '/path/to/invalid/file' }

      before do
        allow(subject.class).to receive(:path).and_return(invalid_file_path)
      end

      it 'returns false' do
        expect(subject.class.exists?).to be false
      end
    end
  end

  describe '.digest_exists?' do
    context 'when the digest exists' do
      before do
        allow(subject.class).to receive(:digest_path).and_return(digest_path)
      end

      it 'returns true' do
        expect(subject.class.digest_exists?).to be true
      end
    end

    context 'when the digest does not exist' do
      before do
        allow(subject.class).to receive(:digest_path).and_return(invalid_digest_path)
      end

      it 'returns false' do
        expect(subject.class.digest_exists?).to be false
      end
    end
  end

  describe '.current_digest' do
    it 'returns the digest of the current spreadsheet' do
      expect(subject.class.current_digest).to eq current_digest
    end
  end

  describe '.last_digest' do
    before do
      allow(subject.class).to receive(:digest_path).and_return(digest_path)
    end

    it 'returns the digest of the last processed spreadsheet' do
      expect(subject.class.last_digest).to eq last_digest
    end
  end

  describe '.changed?' do
    context 'when the digest does not exist' do
      before do
        allow(subject.class).to receive(:digest_exists?).and_return(false)
      end

      it 'returns true' do
        expect(subject.class.changed?).to be true
      end
    end

    context 'when the digest exists' do
      before do
        allow(subject.class).to receive(:digest_exists?).and_return(true)
      end

      context 'when the current digest is different than the last digest' do
        it 'returns true' do
          expect(subject.class.changed?).to be true
        end
      end

      context 'when the current digest is the same as the last digest' do
        before do
          allow(subject.class).to receive(:last_digest).and_return(current_digest)
          expect(subject.class.current_digest).to eq subject.class.last_digest
        end

        it 'returns false' do
          expect(subject.class.changed?).to be false
        end
      end
    end
  end

  describe '.save_digest' do
    before { File.write(digest_path, last_digest) }

    after  { File.write(digest_path, last_digest) }

    it 'saves the current digest to the digest file' do
      expect(File.read(digest_path)).to eq last_digest
      subject.class.save_digest
      expect(File.read(digest_path)).to eq current_digest
    end
  end
end
