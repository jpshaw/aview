# frozen_string_literal: true

require 'rails_helper'

describe OrganizationTypeaheadSearch do
  describe '#execute' do
    let(:user) { create(:user_with_org_permission) }
    let!(:org_1) { user.organization }
    let!(:org_2) { create(:organization) }
    let(:child_org_1) { create(:organization) }

    before do
      org_1.children << child_org_1
      org_1.save
    end

    context 'when no scope is passed' do
      subject { described_class.new(user, query) }

      let(:result) { subject.execute.map(&:id) }

      context 'when the whole name is given' do
        let(:query) { org_1.name }

        it 'includes permitted organizations with the given name' do
          expect(result).to include org_1.id
          expect(result).not_to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end

      context 'when a filter is not set and part of a name is given' do
        let(:query) { org_1.name[1..5] }

        it 'includes permitted organizations containing the given name partial' do
          expect(result).to include org_1.id
          expect(result).to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end

      context 'when a filter is set and part of a name is given' do
        before do
          user.filter_organization = child_org_1
        end

        let(:query) { org_1.name[1..5] }

        it 'includes all permitted organizations containing the given name partial' do
          expect(result).to include org_1.id
          expect(result).to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end
    end

    context 'when a filtered scope is passed' do
      subject { described_class.new(user, query, :filtered) }

      let(:result) { subject.execute.map(&:id) }

      context 'when the whole name is given' do
        let(:query) { org_1.name }

        it 'includes permitted organizations with the given name' do
          expect(result).to include org_1.id
          expect(result).not_to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end

      context 'when a filter is not set and part of a name is given' do
        let(:query) { org_1.name[1..5] }

        it 'includes permitted organizations containing the given name partial' do
          expect(result).to include org_1.id
          expect(result).to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end

      context 'when a filter is set and part of a name is given' do
        before do
          user.filter_organization = child_org_1
        end

        let(:query) { org_1.name[1..5] }

        it 'includes organizations within the filter hierarchy containing the given name partial' do
          expect(result).not_to include org_1.id
          expect(result).to include child_org_1.id
          expect(result).not_to include org_2.id
        end
      end
    end
  end
end
