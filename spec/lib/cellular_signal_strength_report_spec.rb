# frozen_string_literal: true

require 'rails_helper'

describe CellularSignalStrengthReport do
  it_behaves_like 'a report' do
    let(:report_class) { described_class } # Used in shared examples.
  end

  describe 'data' do
    subject(:report) { described_class.new(device_report) }

    let(:device_report) { create(:device_report, params: { 'stats_headers' => {} }) }
    let(:organization)  { create(:random_regular_organization) }
    let(:device)        { create(:netbridge_device, organization: organization) }

    before do
      expect(device).to be_present
      allow(device_report).to receive(:devices).and_return(Device.where(nil))
      stub_report_file
      report.generate!
      device_report.reload
    end

    describe '#initial_strength' do
      # We avoid the MINIMUM_STRENGTH value as it's used as a 'default strength' in the code under certain conditions.
      let(:strength) { rand((CellularHelper::MINIMUM_STRENGTH + 1)..CellularHelper::MAXIMUM_STRENGTH) }
      let(:strength_event) do
        DeviceEvent.new(
          raw_data: {
            'modem' => {
              CellularHelper::SIGNAL_STRENGTH_FLAG => strength
            }
          },
          created_at: Time.now.utc - 1.day
        )
      end

      before do
        allow_any_instance_of(described_class)
          .to receive(:initial_device_event_for_flag)
          .and_return(strength_event)
      end

      context 'when initial down event is before the strength event' do
        let(:down_event) { DeviceEvent.new(created_at: strength_event.created_at - 1.minute) }

        before do
          expect(down_event.created_at).to be < strength_event.created_at

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns the event's strength" do
          expect(report.__send__(:initial_strength, device)).to eq(strength)
        end
      end

      context 'when initial down event happens at the same time as the strength event' do
        let(:down_event) { DeviceEvent.new(created_at: strength_event.created_at) }

        before do
          expect(down_event.created_at).to eq(strength_event.created_at)

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns the event's strength" do
          expect(report.__send__(:initial_strength, device)).to eq(strength)
        end
      end

      context 'when initial down is after the strength event' do
        let(:down_event) { DeviceEvent.new(created_at: strength_event.created_at + 1.minute) }

        before do
          expect(down_event.created_at).to be > strength_event.created_at

          allow_any_instance_of(described_class)
            .to receive(:initial_down_event)
            .and_return(down_event)
        end

        it "returns '#{CellularHelper::MINIMUM_STRENGTH}' as the strength" do
          expect(report.__send__(:initial_strength, device)).to eq(CellularHelper::MINIMUM_STRENGTH)
        end
      end
    end

    describe '#generate_data' do
      let(:initial_interval) { (Time.now.utc - 2.hours).beginning_of_hour }

      # We only need 2 intervals for our tests. We separate them by 1 hour for convenience.
      let(:intervals_template) do
        {
          initial_interval          => Hash.new(0),
          initial_interval + 1.hour => Hash.new(0)
        }
      end

      before do
        expect(device.events.size).to eq(0)

        # Our report starts 15 minutes before the initial interval for convenience.
        allow(report).to receive(:start_time).and_return(initial_interval - 15.minutes)
        allow(report).to receive(:intervals_template).and_return(intervals_template)
      end

      context 'with strength activity between time range' do
        # We don't randomize the strength here so we can more easily test some of the values.
        let(:strength) { -80 }

        let(:result_intervals) do
          {
            intervals_template.keys.first => {
              CellularHelper::MINIMUM_STRENGTH  => 1800.0,
              strength                          => 1800.0
            },
            intervals_template.keys.last => {
              strength => 3600.0
            }
          }
        end

        let(:table_statistics) do
          {
            'average_signal_strength'   =>  -88.0,
            'average_signal_percentage' =>  38.71
          }
        end

        let(:trend_chart_statistics) do
          [
            {
              name: 'Average %',
              data: [
                [intervals_template.keys.first.to_i * 1000, 25.81],
                [intervals_template.keys.last.to_i  * 1000, 51.61]
              ]
            },
            {
              name: 'Average dBm',
              data: [
                [intervals_template.keys.first.to_i * 1000, -96.0],
                [intervals_template.keys.last.to_i  * 1000, -80.0]
              ]
            }
          ]
        end

        before do
          device.events.create(
            created_at: initial_interval + 30.minutes,
            raw_data:   { 'modem' => { CellularHelper::SIGNAL_STRENGTH_FLAG => strength } }
          )
          expect(device.events.size).to eq(1)
          allow(report).to receive(:events_for_device).and_return(device.events)
          report.__send__(:generate_data)
        end

        it 'splits the strength activity seconds between the different strengths' do
          expect(report.__send__(:results).values.first).to eq(result_intervals)
        end

        # NOTE: I chose to test table and trend chart contents here indirectly because setting up
        #       the environment for those method calls was going to take quite a long time.
        it 'generates table percentage statistical information' do
          expect(report.__send__(:table).first.last).to eq(table_statistics)
        end

        it 'generates trend chart percentage statistical information' do
          expect(report.__send__(:trend_chart)[:series]).to eq(trend_chart_statistics)
        end
      end

      context 'without strength activity between time range' do
        let(:result_intervals) do
          {
            intervals_template.keys.first => {
              CellularHelper::MINIMUM_STRENGTH  => 3600.0
            },
            intervals_template.keys.last => {
              CellularHelper::MINIMUM_STRENGTH  => 3600.0
            }
          }
        end

        let(:table_statistics) do
          {
            'average_signal_strength'   =>  CellularHelper::MINIMUM_STRENGTH,
            'average_signal_percentage' =>  0.0
          }
        end

        let(:trend_chart_statistics) do
          [
            {
              name: 'Average %',
              data: [
                [intervals_template.keys.first.to_i * 1000, 0.0],
                [intervals_template.keys.last.to_i  * 1000, 0.0]
              ]
            },
            {
              name: 'Average dBm',
              data: [
                [intervals_template.keys.first.to_i * 1000, CellularHelper::MINIMUM_STRENGTH],
                [intervals_template.keys.last.to_i  * 1000, CellularHelper::MINIMUM_STRENGTH]
              ]
            }
          ]
        end

        before do
          report.__send__(:generate_data)
        end

        it "assigns all strength activity seconds to the #{CellularHelper::MINIMUM_STRENGTH} strength" do
          expect(report.__send__(:results).values.first).to eq(result_intervals)
        end

        # NOTE: I chose to test table and trend chart contents here indirectly because setting up
        #       the environment for those method calls was going to take quite a long time.
        it 'generates table percentage statistical information' do
          expect(report.__send__(:table).first.last).to eq(table_statistics)
        end

        it 'generates trend chart percentage statistical information' do
          expect(report.__send__(:trend_chart)[:series]).to eq(trend_chart_statistics)
        end
      end
    end

    describe '#table' do
      describe 'row' do
        let(:row) { device_report.table.first }

        before do
          expect(device_report.table.size).to eq(1)
          expect(row).to be_present
        end

        it 'contains general device information' do
          expect(row[0]).to eq(device.mac)
          expect(row[1]).to eq(device.host)
          expect(row[2]).to be nil          # Category name: category not set.
          expect(row[3]).to be nil          # Model name: model not set.
          expect(row[4]).to eq(device.organization.name)
          expect(row[6]).to eq('Default')   # Site name: defaults to 'Default'.
          expect(row[7]).to eq(device.deployed? ? 'Deployed' : 'Undeployed')
          expect(row[8]).to eq(device.last_heartbeat_at)
        end

        it 'contains statistical information' do
          expect(row[9]).to be_present
          expect(row[9].keys.sort).to eq(%w[average_signal_percentage average_signal_strength])
        end
      end
    end

    describe '#pie_chart' do
      it 'does not get generated' do
        expect(device_report.pie_chart).to be nil
      end
    end

    describe '#trend_chart' do
      let(:trend_chart) { device_report.trend_chart }

      it 'contains a hash with information for Hicharts' do
        expect(trend_chart.class).to eq(Hash)
      end

      it 'is of type `spline`' do
        expect(trend_chart[:chart][:type]).to eq('spline')
      end

      it 'has title `Cellular Signal Strength`' do
        expect(trend_chart[:title][:text]).to eq('Cellular Signal Strength')
      end

      it 'displays `Average dBm` and `Average %` values' do
        trend_chart[:series].each do |serie|
          expect(['Average %', 'Average dBm']).to include(serie[:name])
        end
      end
    end
  end
end
