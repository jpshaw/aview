# frozen_string_literal: true

require 'rails_helper'

describe Platforms::UcLinux::Commands::Client do
  describe '.new' do
    it 'sets the host if passed' do
      client = described_class.new({})
      expect(client.__send__(:host)).to be nil

      host    = Faker::Lorem.word
      client  = described_class.new(host: host)
      expect(client.__send__(:host)).to eq(host)
    end

    it 'sets the port if passed' do
      client = described_class.new({})
      expect(client.__send__(:port)).to be nil

      port    = rand(1..65_535)
      client  = described_class.new(port: port)
      expect(client.__send__(:port)).to eq(port)
    end

    it 'sets the user if passed' do
      client = described_class.new({})
      expect(client.__send__(:user)).to be nil

      user    = Faker::Lorem.word
      client  = described_class.new(user: user)
      expect(client.__send__(:user)).to eq(user)
    end

    it 'sets the password if passed' do
      client = described_class.new({})
      expect(client.__send__(:pass)).to be nil

      pass    = Faker::Lorem.word
      client  = described_class.new(pass: pass)
      expect(client.__send__(:pass)).to eq(pass)
    end

    context 'when SSL usage is set to false' do
      let(:client) { described_class.new(use_ssl: false) }

      it 'does not use SSL' do
        expect(client.__send__(:use_ssl)).to be false
      end
    end

    context 'when SSL usage is not set to false (default behavior)' do
      it 'uses SSL' do
        client = described_class.new({})
        expect(client.__send__(:use_ssl)).to be true

        client = described_class.new(use_ssl: true)
        expect(client.__send__(:use_ssl)).to be true

        client = described_class.new(use_ssl: 'SOMETHING')
        expect(client.__send__(:use_ssl)).to be true
      end
    end
  end

  describe '#get' do
    let(:client) { described_class.new({}) }

    describe 'without a command parameter' do
      it 'raises an ArgumentError' do
        expect { client.get }.to raise_error(ArgumentError)
      end
    end

    describe 'with a command parameter' do
      let(:http_client) { HTTP::Client.new }

      before do
        allow_any_instance_of(HTTP::Client).to receive(:basic_auth).and_return(http_client)
        expect(http_client).to receive(:post)
      end

      it 'builds connection headers' do
        expect(client).to receive(:headers)
        client.get(:a_command)
      end

      it 'builds connection path' do
        expect(client).to receive(:path)
        client.get(:a_command)
      end

      it 'sets connection headers' do
        headers = client.__send__ :headers
        expect(headers).to be_present
        expect(HTTP).to receive(:headers).with(headers).and_return(http_client)
        client.get(:a_command)
      end

      it 'sets timeout values' do
        expect(HTTP).to receive(:headers).and_return(http_client)
        expect(http_client).to receive(:timeout).and_return(http_client)
        client.get(:a_command)
      end

      it 'sets basic auth values' do
        expect(HTTP).to receive(:headers).and_return(http_client)
        expect(http_client).to receive(:timeout).and_return(http_client)
        expect(http_client).to receive(:basic_auth).and_return(http_client)
        client.get(:a_command)
      end

      it "sends an HTTP 'get' to the device" do
        # Expectation is already set in 'before :each' block above.
        client.get(:a_command)
      end
    end
  end

  describe 'Private instance methods' do
    let(:client) { described_class.new({}) }

    describe '#headers' do
      it "returns the connection's content type" do
        expect(client.__send__(:headers)).to eq(content_type: 'application/json; charset=utf-8')
      end
    end

    describe '#path' do
      it 'retrieves the connection scheme' do
        expect(client).to receive(:scheme)
        client.__send__ :path
      end

      it 'retrieves the connection domain' do
        expect(client).to receive(:domain)
        client.__send__ :path
      end

      it 'returns a string containing the scheme, the domain and the URI path' do
        scheme    = 'http'
        domain    = '/a/domain'
        uri_path  = client.class::URI_PATH
        expect(client).to receive(:scheme).and_return(scheme)
        expect(client).to receive(:domain).and_return(domain)
        expect(client.__send__(:path)).to eq("#{scheme}://#{domain}#{uri_path}")
      end
    end

    describe '#scheme' do
      context 'when using SSL' do
        it "returns 'https'" do
          expect(client).to receive(:use_ssl).and_return(true)
          expect(client.__send__(:scheme)).to eq('https')
        end
      end

      context 'when not using SSL' do
        it "returns 'http'" do
          expect(client).to receive(:use_ssl).and_return(false)
          expect(client.__send__(:scheme)).to eq('http')
        end
      end
    end

    describe '#domain' do
      context 'when host is an IPv6 address' do
        let(:ipv6_address) { 'fd22:2222:2222:3100:be5f:f4ff:fee1:a36b' }

        before do
          allow(client).to receive(:host).and_return(ipv6_address)
        end

        it 'returns the IPv6 address surrounded by square brackets' do
          expect(client.__send__(:domain)).to eq("[#{ipv6_address}]")
        end
      end

      context 'when host is not an IPv6 address' do
        let(:host) { 'a.host.value' }

        before do
          allow(client).to receive(:host).and_return(host)
        end

        it 'returns host value as is' do
          expect(client.__send__(:domain)).to eq(host)
        end
      end

      context 'when a port value is present' do
        let(:host) { 'a.host.value' }
        let(:port) { 443 }

        before do
          allow(client).to receive(:host).and_return(host)
          allow(client).to receive(:port).and_return(port)
        end

        it 'returns the port value in the domain string' do
          expect(client.__send__(:domain)).to eq("#{host}:#{port}")
        end
      end

      context 'when a port value is not present' do
        let(:host) { 'a.host.value' }

        before do
          allow(client).to receive(:host).and_return(host)
          allow(client).to receive(:port).and_return(nil)
        end

        it 'does not return the port value in the domain string' do
          expect(client.__send__(:domain)).to eq(host)
        end
      end
    end
  end
end
