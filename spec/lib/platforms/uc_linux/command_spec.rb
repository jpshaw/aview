# frozen_string_literal: true

require 'rails_helper'

describe Platforms::UcLinux::Command do
  let(:device)    { Device.new }
  let(:user)      { 'user' }
  let(:password)  { 'password' }

  let(:command) do
    described_class.new(
      device:   device,
      command:  :a_command,
      user:     user,
      password: password
    )
  end

  describe '.new' do
    it 'raises ArgumentError without a valid device' do
      expect { described_class.new({}) }.to raise_error(ArgumentError)
    end

    it 'raises ArgumentError without a valid command' do
      expect { described_class.new(device: device) }.to raise_error(ArgumentError)
    end

    it 'raises MissingCredentialsError without user' do
      expect do
        described_class.new(device: device, command: :a_command)
      end.to raise_error(Platforms::UcLinux::Command::MissingCredentialsError)
    end

    it 'raises MissingCredentialsError without password' do
      expect do
        described_class.new(device: device, command: :a_command, user: user)
      end.to raise_error(Platforms::UcLinux::Command::MissingCredentialsError)
    end

    it 'does not raise error with valid arguments' do
      expect do
        described_class.new(device: device, command: :a_command, user: user, password: password)
      end.not_to raise_error
      expect do
        described_class.new(device: device, command: 'a_command', user: user, password: password)
      end.not_to raise_error
    end

    it 'sets the data if passed' do
      data    = Faker::Lorem.word
      command = described_class.new(
        device: device,
        command: :a_comamnd,
        user: user,
        password: password,
        data: data
      )
      expect(command.__send__(:data)).to eq(data)
    end

    it 'sets the data to an empty hash if not passed' do
      expect(command.__send__(:data)).to eq({})
    end
  end

  describe '#send!' do
    let(:client) { Platforms::UcLinux::Commands::Client.new }

    before do
      allow(command).to receive(:log_event)
      allow(device).to receive(:command_ip_addr).and_return('127.0.0.1')
    end

    it 'instantiates a client for the platform' do
      expect(Platforms::UcLinux::Commands::Client).to receive(:new)
      command.send!
    end

    it "sends a 'get' command through the client" do
      expect(Platforms::UcLinux::Commands::Client).to receive(:new).and_return(client)
      expect(client).to receive(:get)
      command.send!
    end

    it 'processes the response from the client' do
      allow(Platforms::UcLinux::Commands::Client).to receive(:new).and_return(client)
      allow(client).to receive(:get)
      expect(command).to receive(:process_response)
      command.send!
    end
  end

  # Private methods
  describe '#process_response' do
    context 'when the response is OK' do
      before do
        allow(command).to receive(:ok?).and_return(true)
      end

      it 'logs an event' do
        expect(command).to receive(:log_event)
        command.__send__ :process_response
      end

      it 'caches the device credentials' do
        allow(command).to   receive(:log_event)
        expect(command).to  receive(:cache_credentials)
        command.__send__ :process_response
      end
    end

    context 'when the response is not OK' do
      before do
        allow(command).to receive(:ok?).and_return(false)
      end

      it 'logs an event' do
        expect(command).to receive(:log_event)
        command.__send__ :process_response
      end

      it 'does not cache the device credentials' do
        allow(command).to       receive(:log_event)
        expect(command).not_to  receive(:cache_credentials)
        command.__send__ :process_response
      end
    end
  end
end
