# frozen_string_literal: true

require 'rails_helper'

describe Platforms::EventProcessor do
  subject { described_class }

  let(:device)           { create(:test_device) }
  let(:event_method)     { :test_event_method }
  let(:params)           { { type: event_method } }
  let(:event)            { Platforms::Event.new(params) }
  let(:processor)        { subject.new(device, event) }
  let(:default_method)   { processor.send(:default_method) }
  let(:event_types)      { subject.event_types }
  let(:before_callbacks) { subject.event_callbacks[event_method][:before] }
  let(:after_callbacks)  { subject.event_callbacks[event_method][:after] }

  after do
    subject._event_types = nil
    subject._event_callbacks = nil
    subject.send(:remove_method, event_method) if subject.method_defined?(event_method)
  end

  describe '#run' do
    it 'runs callbacks' do
      expect(processor).to receive(:run_callbacks)
      processor.run
    end

    context 'when the event type is declared' do
      before do
        subject.event_type(event_method)
        expect(processor.event_method).to eq event_method
      end

      context 'when the event method is defined' do
        before do
          subject.send(:define_method, event_method, -> {})
          expect(processor).to respond_to event_method
        end

        it 'runs the event method' do
          expect(processor).to receive event_method
          processor.run
        end
      end

      context 'when the event method is not defined' do
        before do
          expect(processor).not_to respond_to event_method
        end

        it 'raises a `method not found` exception' do
          expect { processor.run }.to raise_error(NoMethodError)
        end
      end
    end

    context 'when the event type is not declared' do
      before do
        expect(subject.event_types).to be_empty
        expect(processor.event_method).to eq default_method
      end

      context 'when the event method is defined' do
        before do
          subject.send(:define_method, event_method, -> {})
          expect(processor).to respond_to event_method
        end

        it 'runs the default method' do
          expect(processor).to receive default_method
          processor.run
        end
      end

      context 'when the event method is not defined' do
        before do
          expect(processor).not_to respond_to event_method
        end

        it 'runs the default method' do
          expect(processor).to receive default_method
          processor.run
        end
      end
    end
  end

  describe '#event_method' do
    context 'when the event type is defined' do
      before do
        subject.event_type(event_method)
      end

      it 'returns the event method' do
        expect(processor.event_method).to eq event_method
      end
    end

    context 'when the event type is not defined' do
      before do
        expect(subject.event_types).to be_empty
      end

      it 'returns the default method name' do
        expect(processor.event_method).to eq default_method
      end
    end
  end

  describe '.event_types' do
    it 'returns a hash' do
      expect(subject.event_types).to be_a Hash
    end
  end

  describe '.event_type' do
    context 'when given nil' do
      it 'does not update the event types' do
        subject.event_type(nil)
        expect(event_types).to be_empty
      end
    end

    context 'when given an event type' do
      let(:event_type) { :event_type_a }

      it 'adds the event type' do
        subject.event_type(event_type)
        expect(event_types).to include(event_type)
      end
    end

    context 'when given a list of event types' do
      let(:event_types) { %i[event_type_a event_type_b] }

      it 'adds the event types' do
        subject.event_type(*event_types)
        expect(event_types).to include(*event_types)
      end
    end
  end

  describe '.alias_event' do
    let(:from_method) { :from_method }
    let(:to_method)   { :to_method }

    it 'adds an event type entry mapping the first argument to the second' do
      expect do
        subject.alias_event(from_method, to_method)
      end.to change { event_types[from_method] }.to(to_method)
    end
  end

  describe '.event_callbacks' do
    it 'returns a hash' do
      expect(subject.event_callbacks).to be_a Hash
    end
  end

  describe '.before_event' do
    context 'when given nil as a callback' do
      it 'does not update the callback list' do
        subject.before_event(event_method, nil)
        expect(before_callbacks).to be_empty
      end
    end

    context 'when given a callback' do
      let(:callback) { :a_method }

      it 'adds the method to the callback list' do
        subject.before_event(event_method, callback)
        expect(before_callbacks).to contain_exactly(callback)
      end
    end

    context 'when given a list of callbacks' do
      let(:callbacks) { %i[a_method another_method] }

      it 'adds the methods to the callback list' do
        subject.before_event(event_method, *callbacks)
        expect(before_callbacks).to contain_exactly(*callbacks)
      end
    end

    context 'when given duplicate callbacks' do
      let(:callback)  { :a_method }
      let(:callbacks) { [callback, callback] }

      it 'only adds the callback once' do
        subject.before_event(event_method, *callbacks)
        expect(before_callbacks).to contain_exactly(callback)
      end
    end
  end

  describe '.after_event' do
    context 'when given nil as a callback' do
      it 'does not update the callback list' do
        subject.after_event(event_method, nil)
        expect(after_callbacks).to be_empty
      end
    end

    context 'when given a callback' do
      let(:callback) { :a_method }

      it 'adds the method to the callback list' do
        subject.after_event(event_method, callback)
        expect(after_callbacks).to contain_exactly(callback)
      end
    end

    context 'when given multiple callbacks' do
      let(:callbacks) { %i[a_method another_method] }

      it 'adds the methods to the callback list' do
        subject.after_event(event_method, *callbacks)
        expect(after_callbacks).to contain_exactly(*callbacks)
      end
    end

    context 'when given duplicate callbacks' do
      let(:callback)  { :a_method }
      let(:callbacks) { [callback, callback] }

      it 'only adds the callback once' do
        subject.after_event(event_method, *callbacks)
        expect(after_callbacks).to contain_exactly(callback)
      end
    end
  end

  describe '.before_callbacks' do
    context 'when given nil' do
      it 'returns an empty array' do
        expect(subject.before_callbacks(nil)).to eq []
      end
    end

    context 'when given a method that has callbacks defined' do
      let(:callbacks) { %i[a_method another_method] }

      before do
        subject.before_event(event_method, *callbacks)
      end

      it 'returns the callbacks' do
        expect(subject.before_callbacks(event_method)).to eq callbacks
      end
    end
  end

  describe '.after_callbacks' do
    context 'when given nil' do
      it 'returns an empty array' do
        expect(subject.after_callbacks(nil)).to eq []
      end
    end

    context 'when given a method that has callbacks defined' do
      let(:callbacks) { %i[a_method another_method] }

      before do
        subject.after_event(event_method, *callbacks)
      end

      it 'returns the callbacks' do
        expect(subject.after_callbacks(event_method)).to eq callbacks
      end
    end
  end

  describe 'Inheritance' do
    context 'when two processor classes are defined' do
      class TestProcessorA < Platforms::EventProcessor
        event_type :test_method_a
        before_event :test_method_a, :callback_a_1
        after_event  :test_method_a, :callback_a_2
      end

      class TestProcessorB < Platforms::EventProcessor
        event_type :test_method_b
        before_event :test_method_b, :callback_b_1
        after_event  :test_method_b, :callback_b_2
      end

      it 'does not add event types to the base processor class' do
        expect(subject.event_types).to be_empty
      end

      it 'does not add event callbacks to the base processor class' do
        expect(subject.event_callbacks).to be_empty
      end

      it 'ensures that event types are only known to their respective classes' do
        expect(TestProcessorA.event_types).to include(:test_method_a)
        expect(TestProcessorA.event_types).not_to include(:test_method_b)
        expect(TestProcessorB.event_types).to include(:test_method_b)
        expect(TestProcessorB.event_types).not_to include(:test_method_a)
      end

      it 'ensures that event callbacks are only known to their respective classes' do
        expect(TestProcessorA.event_callbacks).to include(:test_method_a)
        expect(TestProcessorA.event_callbacks).not_to include(:test_method_b)
        expect(TestProcessorB.event_callbacks).to include(:test_method_b)
        expect(TestProcessorB.event_callbacks).not_to include(:test_method_a)
      end
    end
  end
end
