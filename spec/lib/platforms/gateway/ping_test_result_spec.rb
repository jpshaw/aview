# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::PingTestResult do
  subject { described_class.new(data) }

  let(:mac)  { '002704239bbe' }
  let(:data) do
    {
      mac: mac,
      evTrapCreatedTime: 1_461_872_814_000_000,
      evResult: '0',
      evSrcV4: '0.0.0.0',
      evDestV4: '0.0.0.0',
      evSrcV6: '0000:0000:0000:0000:0000:0000:0000:0000',
      evDestV6: '0000:0000:0000:0000:0000:0000:0000:0000',
      evResponseMax: '50',
      evFailureCount: '1',
      evDescription: '',
      evSendTrapWhen: '0'
    }
  end
  let(:ipv4_address) { '8.8.8.8' }
  let(:ipv6_address) { '2620:000e:4000:4440:e61f:001a:4a7e:5808' }

  describe '#mac' do
    it 'ensures the mac is upcased' do
      expect(subject.mac).to eq mac.upcase
    end
  end

  describe '#result' do
    it 'returns the result code' do
      expect(subject.result).to eq 0
    end
  end

  describe '#success?' do
    context 'when the result is a success' do
      before { data[:evResult] = Platforms::Gateway::PingTestResult::SUCCESS }

      it 'returns true' do
        expect(subject.success?).to be true
      end
    end

    context 'when the result is a failure' do
      before { data[:evResult] = Platforms::Gateway::PingTestResult::FAILURE }

      it 'returns false' do
        expect(subject.success?).to be false
      end
    end
  end

  describe '#failure?' do
    context 'when the result is a success' do
      before { data[:evResult] = Platforms::Gateway::PingTestResult::SUCCESS }

      it 'returns false' do
        expect(subject.failure?).to be false
      end
    end

    context 'when the result is a failure' do
      before { data[:evResult] = Platforms::Gateway::PingTestResult::FAILURE }

      it 'returns true' do
        expect(subject.failure?).to be true
      end
    end
  end

  describe '#description' do
    context 'when a description is set' do
      let(:description) { 'BAD IP ADDRESS' }

      before { data[:evDescription] = description }

      it 'titlecases the string' do
        expect(subject.description).to eq description.titlecase
      end
    end
  end

  describe '#source' do
    context 'when using IPv4 addresses' do
      before do
        allow(subject).to receive(:ipv4?).and_return(true)
        data[:evSrcV4] = ipv4_address
      end

      it 'returns the IPv4 source address' do
        expect(subject.source).to eq ipv4_address
      end
    end

    context 'when using IPv6 addresses' do
      before do
        allow(subject).to receive(:ipv4?).and_return(false)
        data[:evSrcV6] = ipv6_address
      end

      it 'returns the IPv6 source address' do
        expect(subject.source).to eq ipv6_address
      end
    end
  end

  describe '#destination' do
    context 'when using IPv4 addresses' do
      before do
        allow(subject).to receive(:ipv4?).and_return(true)
        data[:evDestV4] = ipv4_address
      end

      it 'returns the IPv4 destination address' do
        expect(subject.destination).to eq ipv4_address
      end
    end

    context 'when using IPv6 addresses' do
      before do
        allow(subject).to receive(:ipv4?).and_return(false)
        data[:evDestV6] = ipv6_address
      end

      it 'returns the IPv6 destination address' do
        expect(subject.destination).to eq ipv6_address
      end
    end
  end

  describe '#response_max' do
    it 'returns the response max' do
      expect(subject.response_max).to eq 50
    end
  end

  describe '#failure_count' do
    it 'returns the failure count' do
      expect(subject.failure_count).to eq 1
    end
  end

  describe '#created_at' do
    it 'returns a time object for the given unix epoch' do
      expect(subject.created_at).to eq Time.utc(2016, 4, 28, 19, 46, 54)
    end
  end
end
