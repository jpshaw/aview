# frozen_string_literal: true

require 'rails_helper'
require 'devices/gateway'

describe Platforms::Gateway::WanUtilizationReport do
  before do
    create(:gateway_device_with_configuration, mac: '00D0CF100015') 
  end 

  subject { described_class.new(data, interface) }

  let(:mac) { '00d0cf100015' }
  let(:data) do
    {
      mac: mac,
      evTrapCreatedTime: 1_461_769_776_000_000,
      evDataStatsInterv: '60',
      evDataSamplesToAvg: '5',
      evDataUpBandw: '100',
      evDataDownBandw: '5000',
      evDataUp1: '35',
      evDataUp2: '29',
      evDataUp3: '50',
      evDataUp4: '41',
      evDataUp5: '89',
      evDataUp6: '62',
      evDataUp7: '32',
      evDataUp8: '45',
      evDataUp9: '63',
      evDataUp10: '71',
      evDataUp11: '35',
      evDataUp12: '43',
      evDataDown1: '3500',
      evDataDown2: '2901',
      evDataDown3: '5023',
      evDataDown4: '4145',
      evDataDown5: '8956',
      evDataDown6: '6267',
      evDataDown7: '3278',
      evDataDown8: '4589',
      evDataDown9: '6301',
      evDataDown10: '7112',
      evDataDown11: '3523',
      evDataDown12: '4334',
      ngifActiveWanIface: interface
    }
  end

  let(:interface) { 'eth1' }

  describe '#mac' do
    it 'ensures the mac is upcased' do
      expect(subject.mac).to eql mac.upcase
    end
  end

  describe '#upload_hwm' do
    it 'returns the upload high watermark as an integer' do
      expect(subject.upload_hwm).to be 100
    end
  end

  describe '#download_hwm' do
    it 'returns the download high watermark as an integer' do
      expect(subject.download_hwm).to be 5000
    end
  end

  describe '#created_at' do
    it 'returns a time object for the given unix epoch' do
      expect(subject.created_at).to eql Time.utc(2016, 4, 27, 15, 9, 36)
    end
  end

  describe '#upload_measurements' do
    it 'returns the upload measurements in the correct order' do
      expect(subject.upload_measurements).to eql [35, 29, 50, 41, 89, 62, 32, 45, 63, 71, 35, 43]
    end
  end

  describe '#download_measurements' do
    it 'returns the download measurements in the correct order' do
      expect(subject.download_measurements).to eql [3500, 2901, 5023, 4145, 8956, 6267, 3278, 4589, 6301, 7112, 3523, 4334]
    end
  end

  describe '#save_metrics' do
    it 'saves the wan utilization measurements to the metrics database' do
      expect(Measurement::WanUtilization).to receive(:save_sample).exactly(subject.interval_count).times
      subject.save_metrics
    end
  end

  describe '#report_interval' do
    it 'returns the report interval in seconds' do
      expect(subject.report_interval).to be 3600
    end
  end

  describe '#sampling_interval' do
    it 'returns the sampling interval in seconds' do
      expect(subject.sampling_interval).to be 300
    end
  end
end
