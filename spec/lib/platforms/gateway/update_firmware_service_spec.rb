# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::UpdateFirmwareService do
  describe '#execute' do
    subject { described_class.new(device, firmware) }

    let(:device)   { create(:gateway_device) }
    let(:firmware) { '7.0.10' }

    context 'when the firmware changes' do
      let(:firmware_was) { '6.5.10' }

      before { device.update(firmware: firmware_was) }

      it 'updates the firmware' do
        expect do
          subject.execute
        end.to change(device, :firmware).from(firmware_was).to(firmware)
      end
    end

    context 'when the firmware does not change' do
      before { device.update(firmware: firmware) }

      it 'does not update the firmware' do
        expect { subject.execute }.not_to change(device, :firmware)
      end
    end
  end
end
