# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::UpdateWanInterfaceService do
  describe '#execute' do
    subject { described_class.new(device, interface_data) }

    let(:device) { create(:gateway_device) }
    let(:interface_data) { {} }

    context 'when given ipv4 data' do
      before do
        interface_data.merge!(
          ipv4_ip: '192.168.1.223',
          ipv4_iface: 'eth2'
        )
      end

      it 'updates the primary ip address' do
        expect do
          subject.execute
        end.to change(device, :host).from(nil).to(interface_data[:ipv4_ip])
      end

      it 'updates the primary wan interface' do
        expect do
          subject.execute
        end.to change(device, :primary_wan_iface).from(nil).to(interface_data[:ipv4_iface])
      end
    end

    context 'when given ipv6 data' do
      before do
        interface_data.merge!(
          ipv6_ip: '2001:1890:123f:391:0:0:0:3d',
          ipv6_iface: 'eth1'
        )
      end

      it 'updates the primary ip address' do
        expect do
          subject.execute
        end.to change(device, :host).from(nil).to(interface_data[:ipv6_ip])
      end

      it 'updates the primary wan interface' do
        expect do
          subject.execute
        end.to change(device, :primary_wan_iface).from(nil).to(interface_data[:ipv6_iface])
      end
    end
  end
end
