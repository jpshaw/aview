# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::ModemResults do
  subject { described_class.new(device, results) }

  let(:device)  { create(:gateway_device) }
  let(:results) do
    {
      '1.3.6.1.4.1.74.1.30.1.20.0'     => '00D0CF15BE9B',
      '1.3.6.1.4.1.74.1.30.1.1.0'      => '8200',
      '1.3.6.1.4.1.74.1.30.1.2.0'      => '6.3.14',
      '1.3.6.1.4.1.74.1.30.2.1.0'      => 'eth1',
      '1.3.6.1.4.1.74.1.30.1.7.0'      => '2',
      '1.3.6.1.4.1.74.1.30.1.3.0'      => 'ATAP',
      '1.3.6.1.4.1.74.1.30.2.2.0'      => '1515247412',
      '1.3.6.1.4.1.74.1.30.2.3.0'      => '2227797204',
      '1.3.6.1.4.1.74.1.30.2.5.0'      => '0:00:00.00',
      '1.3.6.1.4.1.74.1.30.5.1.1.2.1'  => '1',
      '1.3.6.1.4.1.74.1.30.5.1.1.3.1'  => 'USB Sprint Tri-Mode USB',
      '1.3.6.1.4.1.74.1.30.5.1.1.4.1'  => 'Franklin Wireless Corp.',
      '1.3.6.1.4.1.74.1.30.5.1.1.5.1'  => 'U770',
      '1.3.6.1.4.1.74.1.30.5.1.1.6.1'  => 'U770S35.01.M799',
      '1.3.6.1.4.1.74.1.30.5.1.1.7.1'  => '990000953642396',
      '1.3.6.1.4.1.74.1.30.5.1.1.12.1' => '8137842001',
      '1.3.6.1.4.1.74.1.30.5.1.1.17.1' => '89011200000179806814',
      '1.3.6.1.4.1.74.1.30.5.1.1.16.1' => @modem_status,
      '1.3.6.1.4.1.74.1.30.5.1.1.19.1' => '00:00:01:01:00:00:00:00',
      '1.3.6.1.4.1.74.1.30.5.1.1.27.1' => '-1'
    }
  end

  describe '#process' do
    context 'when a modem is present' do
      before do
        @modem_status = Platforms::Gateway::Snmp::ModemResults::MODEM_PRESENT
      end

      it 'creates a modem object for the device' do
        expect { subject.process }.to change { device.modems.count }.from(0).to(1)
      end

      context 'the device does not already have a modem object' do
        it 'creates an event stating a modem was added' do
          subject.process
          expect(device).to have_event_info 'Cellular modem detected'
        end
      end

      context 'the device already has a record for the modem' do
        before do
          expect(device.modems.count).to eq 0
          device.modems.where(active: true, iccid: 89_011_200_000_179_806_814).create
        end

        it 'does not create an event stating a modem was added' do
          subject.process
          expect(device).not_to have_event_info 'Cellular modem detected'
        end
      end
    end

    context 'when a modem is not present' do
      before do
        @modem_status = Platforms::Gateway::Snmp::ModemResults::MODEM_MISSING
      end

      context 'the device does not already have a modem object' do
        it 'does not create a modem object for the device' do
          subject.process
          expect(device.modems.count).to eq 0
        end

        it 'does not create an event stating the modem was removed' do
          subject.process
          expect(device).not_to have_event_info 'Cellular modem removed'
        end
      end

      context 'the device already has a modem association' do
        before do
          expect(device.modems.count).to eq 0
          device.modems.create
        end

        it 'deletes all modem objects for the device' do
          expect { subject.process }.to change { device.modems.count }.from(1).to(0)
        end

        it 'creates an event stating the modem was removed' do
          subject.process
          expect(device).to have_event_info 'Cellular modem removed'
        end
      end
    end
  end
end
