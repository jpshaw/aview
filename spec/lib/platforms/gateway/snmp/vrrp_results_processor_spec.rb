# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::VRRPResultsProcessor do
  HARDWARE_MAC = Platforms::Gateway::Snmp::Objects::HARDWARE_MAC
  INTERFACE    = Platforms::Gateway::Snmp::Objects::VRRP_INTERFACE
  STATUS       = Platforms::Gateway::Snmp::Objects::VRRP_STATUS
  VRRPResult   = Platforms::Gateway::Snmp::VRRPResult

  let(:device)  { create(:gateway_device, :firmware_6_2, :with_details) }
  let(:results) { {} }

  let(:not_running) { VRRPResult::NOT_RUNNING_STR }
  let(:master)      { VRRPResult::MASTER_STR }
  let(:backup)      { VRRPResult::BACKUP_STR }

  before do
    results[HARDWARE_MAC] = device.mac
  end

  describe '.run' do
    context 'when given results without vrrp data' do
      it 'sets the vrrp state to :not_running' do
        expect do
          subject.run(device, results)
        end.to change {
          device.details.vrrp_state
        }.from(nil).to(not_running)
      end
    end

    context 'when given results with a master status' do
      before do
        results.merge!(
          "#{STATUS}.1" => VRRPResult::BACKUP,
          "#{STATUS}.2" => VRRPResult::MASTER
        )
      end

      it 'sets the vrrp state to :master' do
        expect do
          subject.run(device, results)
        end.to change {
          device.details.vrrp_state
        }.from(nil).to(master)
      end
    end

    context 'when given results without a master status' do
      before do
        results.merge!(
          "#{STATUS}.1" => VRRPResult::BACKUP,
          "#{STATUS}.2" => VRRPResult::BACKUP
        )
      end

      it 'sets the vrrp state to the first result' do
        expect do
          subject.run(device, results)
        end.to change {
          device.details.vrrp_state
        }.from(nil).to(backup)
      end
    end
  end
end
