# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::ConnectedDevicesResults do
  subject { described_class.new(device, results) }

  let(:device)  { create(:gateway_device) }
  let(:results) { {} }

  describe '#process' do
    context 'when the results are nil' do
      let(:results) { nil }

      it 'does not process any interface associations' do
        expect(subject).not_to receive(:process_interface_association)
        subject.process
      end
    end

    context 'when given connected device macs' do
      let(:wan1_mac) { generate(:mac_address) }
      let(:wan2_mac) { generate(:mac_address) }
      let(:results) do
        build(:gateway_connected_devices_snmp_results,
              wan1_device_mac: wan1_mac,
              wan2_device_mac: wan2_mac)
      end

      it 'processes the interface associations' do
        expect(subject).to receive(:process_interface_association).twice
        subject.process
      end

      context 'when there are no associations for the interfaces' do
        it 'associates to the macs' do
          expect do
            subject.process
          end.to associate_device_to_macs(device, wan1_mac, wan2_mac)
        end
      end

      context 'when associations to the macs exist for the interfaces' do
        before do
          device.associate_with_mac(wan1_mac)
          device.associate_with_mac(wan2_mac)
        end

        it 'does not dissociate the device from the macs' do
          expect do
            subject.process
          end.not_to dissociate_device_from_macs(device, wan1_mac, wan2_mac)
        end
      end

      context 'when associations to other macs exists for the interfaces' do
        let(:other_wan1_mac) { generate(:mac_address) }
        let(:other_wan2_mac) { generate(:mac_address) }

        before do
          device.associate_with_mac(other_wan1_mac, source_interface: :eth1)
          device.associate_with_mac(other_wan2_mac, source_interface: :eth2)
        end

        it 'dissociates from the other macs' do
          expect do
            subject.process
          end.to dissociate_device_from_macs(device, other_wan1_mac, other_wan2_mac)
        end

        it 'associates to the macs' do
          expect do
            subject.process
          end.to associate_device_to_macs(device, wan1_mac, wan2_mac)
        end
      end
    end

    context 'when given blank macs' do
      let(:results) do
        build(:gateway_connected_devices_snmp_results,
              wan1_device_mac: '',
              wan2_device_mac: '')
      end

      it 'processes the interface associations' do
        expect(subject).to receive(:process_interface_association).twice
        subject.process
      end

      context 'when there are no associations for the interfaces' do
        it 'does nothing' do
          expect do
            subject.process
          end.not_to change(DeviceAssociation, :count)
        end
      end

      context 'when associations exist for the interfaces' do
        let(:wan1_mac) { generate(:mac_address) }
        let(:wan2_mac) { generate(:mac_address) }

        before do
          device.associate_with_mac(wan1_mac, source_interface: :eth1)
          device.associate_with_mac(wan2_mac, source_interface: :eth2)
        end

        it 'dissociates from the macs' do
          expect do
            subject.process
          end.to dissociate_device_from_macs(device, wan1_mac, wan2_mac)
        end
      end
    end
  end
end
