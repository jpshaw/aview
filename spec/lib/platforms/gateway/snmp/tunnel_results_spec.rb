# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::TunnelResults do
  subject { described_class.new(device, results) }

  let(:device)  { create(:gateway_device) }
  let(:results) do
    {
      '1.3.6.1.4.1.74.1.30.1.20.0'     => '00D0CF15BE9B',
      '1.3.6.1.4.1.74.1.30.1.1.0'      => '8200',
      '1.3.6.1.4.1.74.1.30.1.2.0'      => '6.3.14',
      '1.3.6.1.4.1.74.1.30.2.1.0'      => 'eth1',
      '1.3.6.1.4.1.74.1.30.1.7.0'      => '2',
      '1.3.6.1.4.1.74.1.30.1.3.0'      => 'ATAP',
      '1.3.6.1.4.1.74.1.30.2.2.0'      => '1515247412',
      '1.3.6.1.4.1.74.1.30.2.3.0'      => '2227797204',
      '1.3.6.1.4.1.74.1.30.2.5.0'      => '0:00:00.00',
      '1.3.6.1.4.1.74.1.30.6.1.1.2.1'  => 'ipsec0',
      '1.3.6.1.4.1.74.1.30.6.1.1.5.1'  => '12:31:28.00',
      '1.3.6.1.4.1.74.1.30.6.1.1.6.1'  => '32.224.252.6',
      '1.3.6.1.4.1.74.1.30.6.1.1.19.1' => '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00',
      '1.3.6.1.4.1.74.1.30.6.1.1.7.1'  => '20',
      '1.3.6.1.4.1.74.1.30.6.1.1.13.1' => '1',
      '1.3.6.1.4.1.74.1.30.6.1.1.16.1' => '1',
      '1.3.6.1.4.1.74.1.30.6.1.1.17.1' => '3',
      '1.3.6.1.4.1.74.1.30.6.1.1.18.1' => '1',
      '1.3.6.1.4.1.74.1.30.6.1.1.2.2'  => 'ipsec1',
      '1.3.6.1.4.1.74.1.30.6.1.1.5.2'  => '19:54:39.00',
      '1.3.6.1.4.1.74.1.30.6.1.1.6.2'  => '0.0.0.0',
      '1.3.6.1.4.1.74.1.30.6.1.1.19.2' => '20:01:04:70:00:26:03:07:a0:39:b3:fd:6d:d0:7e:4e',
      '1.3.6.1.4.1.74.1.30.6.1.1.7.2'  => '6',
      '1.3.6.1.4.1.74.1.30.6.1.1.13.2' => '1',
      '1.3.6.1.4.1.74.1.30.6.1.1.16.2' => '1',
      '1.3.6.1.4.1.74.1.30.6.1.1.17.2' => '3',
      '1.3.6.1.4.1.74.1.30.6.1.1.18.2' => '4',
      '1.3.6.1.4.1.74.1.30.6.1.1.24.2' => 'AVIEW-TUNNEL',
      '1.3.6.1.4.1.74.1.30.5.1.1.2.1'  => '1',
      '1.3.6.1.4.1.74.1.30.5.1.1.3.1'  => 'USB Sprint Tri-Mode USB',
      '1.3.6.1.4.1.74.1.30.5.1.1.4.1'  => 'Franklin Wireless Corp.',
      '1.3.6.1.4.1.74.1.30.5.1.1.5.1'  => 'U770',
      '1.3.6.1.4.1.74.1.30.5.1.1.6.1'  => 'U770S35.01.M799',
      '1.3.6.1.4.1.74.1.30.5.1.1.7.1'  => '990000953642396',
      '1.3.6.1.4.1.74.1.30.5.1.1.12.1' => '8137842001',
      '1.3.6.1.4.1.74.1.30.5.1.1.17.1' => '89011200000179806814',
      '1.3.6.1.4.1.74.1.30.5.1.1.16.1' => '1',
      '1.3.6.1.4.1.74.1.30.5.1.1.19.1' => '00:00:01:01:00:00:00:00',
      '1.3.6.1.4.1.74.1.30.5.1.1.27.1' => '-1'
    }
  end

  describe '#process' do
    it 'creates tunnel objects for the device' do
      expect { subject.process }.to change { device.tunnels.count }.from(0).to(2)
    end

    # The IPv4 tunnel is on IPSEC0
    it 'creates a tunnel with a formatted ipv4 address' do
      subject.process
      tunnel = device.tunnels.where(ipsec_iface: 'IPSEC0').first
      expect(tunnel).to be_present
      expect(tunnel.endpoint_ip_address).to eq '32.224.252.6'
    end

    # The IPv6 tunnel is on IPSEC1
    it 'creates a tunnel with a formatted ipv6 address' do
      subject.process
      tunnel = device.tunnels.where(ipsec_iface: 'IPSEC1').first
      expect(tunnel).to be_present
      expect(tunnel.endpoint_ip_address).to eq '2001:470:26:307:a039:b3fd:6dd0:7e4e'
    end

    it 'saves the tunnel name' do
      subject.process
      tunnel = device.tunnels.where(ipsec_iface: 'IPSEC1').first
      expect(tunnel).to be_present
      expect(tunnel.name).to eq 'AVIEW-TUNNEL'
    end

    it 'destroys obsolete tunnels' do
      iface = 'IPSEC5'
      device.tunnels.create!(ipsec_iface: iface)
      subject.process
      expect(device.tunnels.where(ipsec_iface: iface).count).to eq 0
    end

    it 'updates the device tunnel counter' do
      expect(device.tunnels_count).to eq 0
      subject.process
      expect(device.tunnels_count).to eq 2
    end
  end
end
