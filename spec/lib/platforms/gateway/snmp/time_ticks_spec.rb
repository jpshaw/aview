# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::TimeTicks do
  subject { described_class.new(input) }

  describe '#to_i' do
    context 'when given nil' do
      let(:input) { nil }

      it 'returns zero' do
        expect(subject.to_i).to eq 0
      end
    end

    context 'when given a blank string' do
      let(:input) { '' }

      it 'returns zero' do
        expect(subject.to_i).to eq 0
      end
    end

    context 'when given a timetick string value of less than a day' do
      let(:input) { '4:07:43.00' }

      it 'returns the number of seconds represented in the string' do
        expect(subject.to_i).to eq 14_863
      end
    end

    context 'when given a timetick string value of more than a day' do
      let(:input) { '22 days, 1:45:00.00' }

      it 'returns the number of seconds represented in the string' do
        expect(subject.to_i).to eq 1_907_100
      end
    end
  end
end
