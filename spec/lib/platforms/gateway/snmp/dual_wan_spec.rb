# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::Results do
  HARDWARE_MAC   = Platforms::Gateway::Snmp::Objects::HARDWARE_MAC
  DUAL_WAN_STATE = Platforms::Gateway::Snmp::Objects::DUAL_WAN_STATE
  WAN_IPV4_STATE = Platforms::Gateway::Snmp::Objects::WAN_IPV4_STATE
  WAN_IPV6_STATE = Platforms::Gateway::Snmp::Objects::WAN_IPV6_STATE

  DUAL_WAN_DISABLED = ::GatewayDualWANService::DISABLED.to_s
  DUAL_WAN_ENABLED  = ::GatewayDualWANService::ENABLED.to_s

  subject { described_class.new(device, results) }

  let(:device)  { create(:gateway_device, :firmware_6_2, :with_details) }
  let(:results) { { HARDWARE_MAC => device.mac } }

  describe '#process' do
    context 'when dual wan is disabled' do
      before { results[DUAL_WAN_STATE] = DUAL_WAN_DISABLED }

      it 'sets the device to no be dual wan enabled' do
        subject.process
        expect(device).not_to be_dual_wan_enabled
      end
    end

    context 'when dual wan is enabled' do
      before { results[DUAL_WAN_STATE] = DUAL_WAN_ENABLED }

      it 'sets the device to be dual wan enabled' do
        subject.process
        expect(device).to be_dual_wan_enabled
      end
    end

    it 'sets device wan ipv4 state' do
      results[WAN_IPV4_STATE] = '1'
      results[WAN_IPV6_STATE] = '0'
      subject.process
      expect(device.details.wan_ipv4_state).to eq 1
      expect(device.details.wan_ipv6_state).to be nil
      expect(device.dual_wan_wan1_up_and_wan2_down?).to be true
    end

    it 'sets device wan ipv6 state' do
      results[WAN_IPV4_STATE] = '0'
      results[WAN_IPV6_STATE] = '2'
      subject.process
      expect(device.details.wan_ipv4_state).to eq nil
      expect(device.details.wan_ipv6_state).to eq 2
      expect(device.dual_wan_wan1_down_and_wan2_up?).to be true
    end
  end
end
