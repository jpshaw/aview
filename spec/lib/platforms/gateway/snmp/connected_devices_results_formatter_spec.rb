# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::Snmp::ConnectedDevicesResultsFormatter do
  subject { described_class.new(input) }

  describe '#execute' do
    let(:results) { subject.execute }

    context 'when given nil' do
      let(:input) { nil }

      it 'returns an empty hash' do
        expect(results).to eq({})
      end
    end

    context 'when given connected device results' do
      let(:eth1_mac) { generate(:mac_address) }
      let(:eth2_mac) { generate(:mac_address) }
      let(:input)    do
        build(:gateway_connected_devices_snmp_results,
              wan1_device_mac: eth1_mac,
              wan2_device_mac: eth2_mac)
      end

      it 'sets eth1 to the mac' do
        expect(results[:eth1]).to eq eth1_mac
      end

      it 'sets eth2 to the mac' do
        expect(results[:eth2]).to eq eth2_mac
      end
    end

    context 'when a connected device mac is blank' do
      let(:eth1_mac) { generate(:mac_address) }
      let(:eth2_mac) { ' ' }
      let(:input)    do
        build(:gateway_connected_devices_snmp_results,
              wan1_device_mac: eth1_mac,
              wan2_device_mac: eth2_mac)
      end

      it 'sets eth1 to the mac' do
        expect(results[:eth1]).to eq eth1_mac
      end

      it 'sets eth2 to blank' do
        expect(results[:eth2]).to be_blank
      end
    end
  end
end
