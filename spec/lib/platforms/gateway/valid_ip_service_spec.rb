# frozen_string_literal: true

require 'rails_helper'

describe Platforms::Gateway::ValidIpService do
  subject { described_class.new(ip) }

  describe '#execute' do
    context 'when given nil' do
      let(:ip) { nil }

      it 'returns false' do
        expect(subject.execute).to be false
      end
    end

    context 'when given an empty string' do
      let(:ip) { '' }

      it 'returns false' do
        expect(subject.execute).to be false
      end
    end

    context 'when given a valid ipv4 address' do
      let(:ip) { '192.168.1.223' }

      it 'returns true' do
        expect(subject.execute).to be true
      end
    end

    context 'when given a valid ipv6 address' do
      let(:ip) { '2001:1890:123f:391:0:0:0:3d' }

      it 'returns true' do
        expect(subject.execute).to be true
      end
    end

    context 'when given an invalid ipv4 address' do
      let(:ip) { '127.0.0.1' }

      it 'returns false' do
        expect(subject.execute).to be false
      end
    end

    context 'when given an invalid ipv6 address' do
      let(:ip) { '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01' }

      it 'returns false' do
        expect(subject.execute).to be false
      end
    end
  end
end
