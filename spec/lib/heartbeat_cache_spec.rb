# frozen_string_literal: true

require 'rails_helper'

describe HeartbeatCache do
  let(:subject) { described_class.instance }
  let(:cache)   { double('cache') }
  let(:key)     { 'a_key'     }
  let(:value)   { 'a_value'   }

  before do
    allow(subject).to receive(:cache).and_return(cache)
  end

  describe '#keys' do
    it 'delegates to cache object' do
      expect(cache).to receive(:keys)
      subject.keys
    end
  end

  describe '#get' do
    it 'delegates to cache object' do
      expect(cache).to receive(:get).with(key)
      subject.get(key)
    end
  end

  describe '#put' do
    it 'delegates to cache object' do
      expect(cache).to receive(:put).with(key, value)
      subject.put(key, value)
    end
  end

  describe '#remove' do
    it 'delegates to cache object' do
      expect(cache).to receive(:remove).with(key)
      subject.remove(key)
    end
  end

  describe '#clustering_mode' do
    it 'delegates to cache object' do
      expect(cache).to receive(:clustering_mode)
      subject.clustering_mode
    end
  end

  describe '#eviction_strategy' do
    it 'delegates to cache object' do
      expect(cache).to receive(:eviction_strategy)
      subject.eviction_strategy
    end
  end

  describe '#max_entries' do
    it 'delegates to cache object' do
      expect(cache).to receive(:max_entries)
      subject.max_entries
    end
  end

  describe '.put' do
    it "calls 'put' on an instance of HeartbeatCache" do
      expect(described_class).to receive(:instance).and_return(subject)
      expect(subject).to receive(:put).with(key, value)
      described_class.put(key, value)
    end
  end
end
