# frozen_string_literal: true

require 'rails_helper'

describe DeviceCommand do
  describe '#initialize' do
    context 'with no params' do
      it 'raises an ArgumentError error' do
        expect { described_class.new }.to raise_error ArgumentError
      end
    end

    context 'with a device and no data' do
      it 'will not raise an error' do
        expect { described_class.new(Device.new) }.not_to raise_error
      end
    end

    context 'with a device and data' do
      it 'will not raise an error' do
        expect { described_class.new(Device.new, command: 'reboot') }.not_to raise_error
      end
    end
  end

  describe '#send!' do
    context 'with a device and no data' do
      it 'will raise a NotImplementedError error' do
        expect { described_class.new(Device.new).send! }.to raise_error NotImplementedError
      end
    end

    context 'with a device and data' do
      it 'will raise a NotImplementedError error' do
        expect { described_class.new(Device.new, command: 'reboot').send! }.to raise_error NotImplementedError
      end
    end
  end

  describe '#succeeded?' do
    let(:command) { described_class.new(Device.new) }

    context 'when @state is nil' do
      it 'returns false' do
        expect(command.succeeded?).to eq false
      end
    end

    context 'when @state is :succeeded' do
      it 'returns true' do
        command.send('command_succeeded!')
        expect(command.succeeded?).to eq true
      end
    end

    context 'when @state is :failed' do
      it 'returns false' do
        command.send('command_failed!')
        expect(command.succeeded?).to eq false
      end
    end
  end

  describe '#failed?' do
    let(:command) { described_class.new(Device.new) }

    context 'when @state is nil' do
      it 'returns false' do
        expect(command.failed?).to eq false
      end
    end

    context 'when @state is :succeeded' do
      it 'returns false' do
        command.send('command_succeeded!')
        expect(command.failed?).to eq false
      end
    end

    context 'when @state is :failed' do
      it 'returns true' do
        command.send('command_failed!')
        expect(command.failed?).to eq true
      end
    end
  end

  describe '#result_message' do
    let(:command) { described_class.new(Device.new) }

    context "when @message hasn't been set" do
      it 'returns an empty string' do
        expect(command.result_message).to eq ''
      end
    end
  end
end
