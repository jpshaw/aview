# frozen_string_literal: true

require 'rails_helper'

describe GatewayCommand do
  let(:gateway) { create(:gateway_device) }

  context 'with no command passed' do
    it 'fails' do
      command = described_class.new(gateway)
      command.send!
      expect(command.failed?).to eq true
    end
  end

  context 'with a regular command passed' do
    context 'that is unknown' do
      it 'fails' do
        command = described_class.new(gateway, command: 'unknown_command')
        command.send!
        expect(command.failed?).to eq true
      end
    end

    context 'that checks status' do
      let(:command) { described_class.new(gateway, command: 'status') }

      context 'with an unreachable device' do
        it 'fails' do
          allow_any_instance_of(SnmpProber).to receive(:execute!).and_return(nil)
          allow_any_instance_of(SnmpProber).to receive(:successful?).and_return(false)
          command.send!
          expect(command.failed?).to eq true
        end
      end

      context 'with an available device' do
        it 'succeeds' do
          allow_any_instance_of(SnmpProber).to receive(:execute!).and_return(nil)
          allow_any_instance_of(SnmpProber).to receive(:successful?).and_return(true)
          allow_any_instance_of(Gateway).to receive(:process_snmp_results).and_return(true)
          command.send!
          expect(command.succeeded?).to eq true
        end
      end

      context 'with an error' do
        it 'fails' do
          allow_any_instance_of(SnmpProber).to receive(:execute!).and_raise('Error')
          command.send!
          expect(command.failed?).to eq true
        end
      end
    end

    context 'that checks configuration' do
      let(:command) { described_class.new(gateway, command: 'config') }

      before do
        allow_any_instance_of(Gateway).to receive(:portal_config).and_return({})
      end

      context 'of an unreachable device' do
        it 'fails' do
          allow_any_instance_of(GatewayPortalManager).to receive(:update_config!).and_return('partial')
          allow_any_instance_of(GatewayPortalManager).to receive(:command_succeeded?).and_return(false)
          allow_any_instance_of(GatewayPortalManager).to receive(:state_desc).and_return('Device Unreachable')
          command.send!
          expect(command.failed?).to eq true
        end
      end

      context 'of an available device' do
        it 'succeeds' do
          allow_any_instance_of(GatewayPortalManager).to receive(:update_config!).and_return('partial')
          allow_any_instance_of(GatewayPortalManager).to receive(:command_succeeded?).and_return(true)
          allow_any_instance_of(GatewayPortalManager).to receive(:state_desc).and_return('Successfully executed the command.')
          command.send!
          expect(command.succeeded?).to eq true
        end
      end

      context 'while causing an error' do
        it 'fails' do
          allow_any_instance_of(GatewayPortalManager).to receive(:update_config!).and_raise('Error')
          command.send!
          expect(command.failed?).to eq true
        end
      end
    end

    context 'that attempts to reboot' do
      let(:command) { described_class.new(gateway, command: 'reboot') }

      before do
        allow_any_instance_of(Gateway).to receive(:portal_config).and_return({})
      end

      context 'an unreachable device' do
        it 'fails' do
          allow_any_instance_of(GatewayPortalManager).to receive(:reboot!).and_return('full')
          allow_any_instance_of(GatewayPortalManager).to receive(:command_succeeded?).and_return(false)
          allow_any_instance_of(GatewayPortalManager).to receive(:state_desc).and_return('Failed')
          command.send!
          expect(command.failed?).to eq true
        end
      end

      context 'an available device' do
        it 'succeeds' do
          allow_any_instance_of(GatewayPortalManager).to receive(:reboot!).and_return('full')
          allow_any_instance_of(GatewayPortalManager).to receive(:command_succeeded?).and_return(true)
          allow_any_instance_of(GatewayPortalManager).to receive(:state_desc).and_return('Successfully executed the command.')
          command.send!
          expect(command.succeeded?).to eq true
        end
      end

      context 'while causing an error' do
        it 'fails' do
          allow_any_instance_of(GatewayPortalManager).to receive(:reboot!).and_raise('Error')
          command.send!
          expect(command.failed?).to eq true
        end
      end
    end
  end
end
