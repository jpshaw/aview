# frozen_string_literal: true

require 'rails_helper'

describe DeviceTypeaheadSearch do
  describe '#execute' do
    subject { described_class.new(user, query) }

    let(:result) { subject.execute.map(&:id) }

    let(:user) { create(:user_with_org_permission) }
    let(:org_1) { user.organization }
    let(:org_2) { create(:organization) }
    let(:child_org_1) { create(:organization) }
    let!(:device_1) { create(:test_device, organization: org_1) }
    let!(:device_2) { create(:test_device, organization: org_2) }
    let!(:child_device_1) { create(:test_device, organization: child_org_1) }

    before do
      org_1.children << child_org_1
      org_1.save
    end

    context 'when a filter is NOT set' do
      context 'when the whole mac is given' do
        let(:query) { device_1.mac }

        it 'includes permitted devices containing the given mac' do
          expect(result).to include device_1.id
          expect(result).not_to include child_device_1.id
          expect(result).not_to include device_2.id
        end
      end

      context 'when part of a mac is given' do
        let(:query) { device_1.mac[1..5] }

        it 'includes permitted devices containing the given mac partial' do
          expect(result).to include device_1.id
          expect(result).to include child_device_1.id
          expect(result).not_to include device_2.id
        end
      end
    end

    context 'when a filter is set' do
      before do
        user.filter_organization = child_org_1
      end

      let(:query) { device_1.mac[1..5] }

      it 'includes devices within the filter hierarchy containing the given mac partial' do
        expect(result).not_to include device_1.id
        expect(result).to include child_device_1.id
        expect(result).not_to include device_2.id
      end
    end
  end
end
