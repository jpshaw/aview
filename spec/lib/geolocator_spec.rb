# frozen_string_literal: true

require 'rails_helper'

describe Geolocator do
  describe '.can_get_cell_location?' do
    context 'when given valid data' do
      it 'returns true' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '52025601', lac: '65534')
        expect(result).to be true
      end
    end

    context 'when mcc is not present' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mnc: 480, cid: '52025601', lac: '65534')
        expect(result).to be false
      end
    end

    context 'when mnc is not present' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, cid: '52025601', lac: '65534')
        expect(result).to be false
      end
    end

    context 'when cid is not present' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, lac: '65534')
        expect(result).to be false
      end
    end

    context 'when cid is not a string' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: 52_025_601, lac: '65534')
        expect(result).to be false
      end
    end

    context 'when cid is not a number as a string' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: 'abc123', lac: '65534')
        expect(result).to be false
      end
    end

    context 'when cid is 0' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '0', lac: '65534')
        expect(result).to be false
      end
    end

    context 'when lac is not present' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '52025601')
        expect(result).to be false
      end
    end

    context 'when lac is not a string' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '52025601', lac: 65_534)
        expect(result).to be false
      end
    end

    context 'when lac is not a number as a string' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '52025601', lac: 'abc123')
        expect(result).to be false
      end
    end

    context 'when lac is 0' do
      it 'returns false' do
        result = described_class.can_get_cell_location?(mcc: 311, mnc: 480, cid: '52025601', lac: '0')
        expect(result).to be false
      end
    end
  end

  describe '.get_location' do
    context 'when using valid data with the google service' do
      it 'returns the location' do
        # expected values are the results returned from the cassette
        expected_latitude = 42.1159134
        expected_longitude = -88.1763149
        result = nil
        VCR.use_cassette('geolocation from google') do
          result = described_class.get_location(mcc: 311, mnc: 480, cid: '52025601', lac: '65534')
        end
        expect(result.latitude).to eq expected_latitude
        expect(result.longitude).to eq expected_longitude
      end
    end

    context 'when using invalid data with the google service' do
      it 'returns nil' do
        result = nil
        VCR.use_cassette('geolocation invalid_from google') do
          result = described_class.get_location(mcc: 0, mnc: 0, cid: '0', lac: '0')
        end
        expect(result).to be nil
      end
    end
  end
end
