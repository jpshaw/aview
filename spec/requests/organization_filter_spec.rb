# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

describe 'Organization Filter', type: :request do
  before do
    @user = create(:user_with_org_permission)
    @organization = Organization.find(@user.organization_id)
    login_as(@user, scope: :user)
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(@user)
  end

  it 'tracks the organization filter settings across requests' do
    get root_path
    expect(response.status).to eq(200)
    expect(@user.filter_organization).to be_nil

    patch update_filter_api_v2_account_path(@user, filter_organization_id: @organization.id, format: :js)

    get root_path
    expect(response.status).to eq(200)
    expect(@user.filter_organization).to eq(@organization)
  end
end
