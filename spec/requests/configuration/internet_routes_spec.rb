# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe Configuration::InternetRoutesController do
  let(:user) { create(:user) }
  let(:device) { create(:test_device) }

  before do
    login_as(user, scope: :user)
  end

  describe 'GET InternetRoutes configuration for resource' do
    let!(:internet_routes_configuration) { create(:internet_routes_configuration, resource: device) }

    it 'returns UI JSON of InternetRoutes configuration for resource' do
      mapped_response = internet_routes_configuration.data.dup
      mapped_response[:internet_routes] = mapped_response.delete(:routes)
      get device_configuration_internet_routes_path(device)
      expect(JSON.parse(response.body)).to include(mapped_response)
    end
  end
end
