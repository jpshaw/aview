# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.describe Configuration::CascadedNetworksController do
  let(:user) { create(:user) }
  let(:device) { create(:test_device) }

  before do
    login_as(user, scope: :user)
  end

  describe 'GET CascadedNetworks configuration for resource' do
    let!(:cascaded_networks_configuration) { create(:cascaded_networks_configuration, resource: device) }

    it 'returns UI JSON of CascadedNetworks configuration for resource' do
      get device_configuration_cascaded_networks_path(device)
      expect(JSON.parse(response.body)).to include(cascaded_networks_configuration.data)
    end
  end
end
