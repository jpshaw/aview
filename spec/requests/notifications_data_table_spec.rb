# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

describe NotificationsDataTable, type: :request do
  before do
    @user = create(:user)
    login_as(@user, scope: :user)
    event_1 = create(:event, :for_model_8100)
    event_2 = create(:event, :for_model_5300_dc)
    @notification_1 = create(:notification, event: event_1, user: @user, delivered_at: 1.month.ago)
    @notification_2 = create(:notification, event: event_2, user: @user, delivered_at: 1.day.ago)
  end

  let(:mock_columns) do
    { '0' => {
      data: 'mac',
      name: 'with_device_mac',
      searchable: 'true',
      orderable: 'true',
      search: { regex: 'false', value: '' }
    } }
  end

  let(:mock_order) { { '0' => { column: '0', dir: 'desc', sortScope: 'sort_by_delivered_at' } } }
  let(:mock_table_search) { { value: '', regex: 'false' } }
  let(:mock_params) do
    {
      draw: 4,
      start: 0,
      length: 2,
      columns: mock_columns,
      order: mock_order,
      search: mock_table_search
    }
  end

  context 'when given valid parameters with no search criteria' do
    it 'includes all notifications sorted by created_at' do
      post notifications_data_tables_path(format: :json), mock_params
      result = JSON.parse response.body
      data = result['data']
      expect(data.first['mac']).to include @notification_2.event.device.mac
      expect(data.second['mac']).to include @notification_1.event.device.mac
    end
  end

  context 'when given empty parameters' do
    it 'returns no notifications' do
      post notifications_data_tables_path(format: :json), {}
      result = JSON.parse response.body
      expect(result['data']).to be_empty
    end
  end

  context 'when given search parameters' do
    before do
      mock_columns['0'][:search][:value] = "[\"#{@notification_2.event.device.mac}\"]"
    end

    it 'returns only the searched for notification' do
      post notifications_data_tables_path(format: :json), mock_params
      result = JSON.parse response.body
      data = result['data']
      expect(data.first['mac']).to include @notification_2.event.device.mac
      expect(data.size).to eq 1
    end
  end
end
