# frozen_string_literal: true

require 'rails_helper'

describe 'FooterSettingFlows' do
  subject { page }

  before do
    create_user
    # log_in_user @user
  end

  skip 'Settings.footer' do
    before do
      visit root_path
    end

    it { current_path.should match '/' }

    it 'does not show a footer if Settings.footer returns nil' do
      Settings.footer = nil
      visit root_path
      expect(find('#footer')).not_to have_content(Time.now.year.to_s)
    end

    it "shows a footer but with no link if Settings.footer.name is set but Settings.footer.link isn't" do
      Settings.footer = Config::Options.new(name: 'AT&T')
      visit root_path
      expect(find('#footer')).to have_content(Time.now.year.to_s)
      expect(find('#footer')).to have_content('AT&T')
    end

    it 'shows a footer with a link if Settings.footer.name and Settings.footer.link are both set' do
      Settings.footer = Config::Options.new(name: 'AT&T', link: 'http://att.com')
      visit root_path
      expect(find('#footer')).to have_content(Time.now.year.to_s)
      expect(find('#footer')).to have_content('AT&T')
      expect(find('#footer')).to have_link('AT&T')
    end
  end
end
