# frozen_string_literal: true

require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

describe DeviceConfigurationDataTable, type: :request do
  before do
    create_user
    login_as(@user, scope: :user)
    create_dial_to_ip_device_configurations
    allow_any_instance_of(User).to receive(:device_configurations).and_return(DeviceConfiguration.all)
    allow_any_instance_of(DeviceConfigurationPolicy).to receive(:modify?).and_return(true)
  end

  let(:mock_columns) do
    { '0' => {
      data: 'with_name',
      name: 'with_name',
      searchable: 'false',
      orderable: 'true',
      search: { regex: 'false', value: '' }
    } }
  end

  let(:mock_order) { { '0' => { column: '0', dir: 'desc', sortScope: 'sort_by_name' } } }
  let(:mock_table_search) { { value: '', regex: 'false' } }
  let(:mock_params) do
    {
      draw: 4,
      start: 0,
      length: 2,
      columns: mock_columns,
      order: mock_order,
      search: mock_table_search
    }
  end

  context 'when given valid parameters' do
    it 'includes all configurations sorted by created_at' do
      get '/device_configurations/dial_to_ip.json', mock_params
      result = JSON.parse response.body
      data = result['data']
      expect(data.first['with_name']).to include @device_config_2.name
      expect(data.second['with_name']).to include @device_config_1.name
    end
  end

  context 'when given empty parameters' do
    it 'returns no events' do
      get '/device_configurations/dial_to_ip.json', {}
      result = JSON.parse response.body
      expect(result['data']).to be_empty
    end
  end
end
