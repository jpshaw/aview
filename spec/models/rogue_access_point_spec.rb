# frozen_string_literal: true

require 'rails_helper'

describe RogueAccessPoint do
  let(:access_point) { create(:rogue_access_point) }

  describe '#formatted_mac' do
    context 'when a basic mac is set' do
      before do
        access_point.mac = '00c0ca1a2b3c'
      end

      it 'returns a formatted mac' do
        expect(access_point.formatted_mac).to eq '00:C0:CA:1A:2B:3C'
      end
    end

    context 'when a formatted mac is already set' do
      before do
        access_point.mac = '00:c0:ca:1a:2b:3c'
      end

      it 'returns a formatted mac' do
        expect(access_point.formatted_mac).to eq '00:C0:CA:1A:2B:3C'
      end
    end
  end

  describe '.from_syslog' do
    let(:device) { create(:netreach_device) }

    context 'when given an empty string' do
      it 'returns an empty array' do
        expect(device.rogue_access_points.from_syslog('')).to eq []
      end
    end

    context 'when given a rogue access point' do
      before do
        @message = 'SSID=tacocat,MAC=00c0ca49d277,IP=10.10.10.11'
      end

      it 'returns an array with a single persisted access point record' do
        access_points = device.rogue_access_points.from_syslog(@message)
        expect(access_points.count).to eq 1
        record = access_points.first
        expect(record.persisted?).to eq true
        expect(record.ssid).to eq 'tacocat'
        expect(record.mac).to eq '00c0ca49d277'
        expect(record.ip).to eq '10.10.10.11'
      end
    end

    context 'when given many rogue access points' do
      before do
        @message = 'SSID=tacocat,MAC=00c0ca49d277,IP=10.10.10.11~' \
                   'SSID=clearer,MAC=002704012345,IP=10.10.10.100'
      end

      it 'returns an array of persisted access point records' do
        access_points = device.rogue_access_points.from_syslog(@message)
        expect(access_points.count).to eq 2
        access_points.each do |ap|
          expect(ap.persisted?).to eq true
        end
      end
    end
  end
end
