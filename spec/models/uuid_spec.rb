# frozen_string_literal: true

require 'rails_helper'

describe DeviceEventUUID do
  describe 'methods' do
    let(:uuids) { EventUUIDS.constants_hash.keys.sort.select { |k| k.to_s.starts_with? 'gateway' } }
    let(:uuid)  { uuids.first }

    before do
      create_gateway_event_uuids
      expect(uuid.is_a?(Symbol)).to be true
    end

    describe '.find_by_symbol' do
      it 'returns nil if nil is passed' do
        expect(described_class.find_by_symbol(nil)).to be nil
      end

      it 'returns nil unless a symbol is passed' do
        uuid_string = uuid.to_s
        expect(described_class.find_by_symbol(uuid_string)).to be nil
      end

      it 'returns a uuid object if a valid symbol is passed' do
        expect(described_class.find_by_symbol(uuid)).not_to be nil
      end
    end
  end
end
