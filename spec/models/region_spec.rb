# frozen_string_literal: true

require 'rails_helper'

describe Region do
  describe 'method: ' do
    describe 'self.select_list' do
      it 'returns an array' do
        expect(described_class.select_list(233).class).to eql Array
      end

      it 'returns all US states in the table plus one' do
        expect(described_class.select_list(233).count).to eql described_class.where(country_id: 233).count + 1
      end
    end
  end
end
