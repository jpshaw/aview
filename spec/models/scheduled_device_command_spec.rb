# frozen_string_literal: true

require 'rails_helper'

describe ScheduledDeviceCommand do
  subject { described_class }

  describe '.all_for_current_interval' do
    it 'returns an enumerable' do
      expect(subject.all_for_current_interval).to be_an Enumerable
    end
  end

  describe '.gateway_vlan_commands' do
    context 'when not enumerated' do
      it 'returns an enumerable' do
        expect(subject.gateway_vlan_commands).to be_an Enumerable
      end
    end

    context 'when there are no devices scheduled for vlan commands' do
      it 'returns an empty collection' do
        expect(subject.gateway_vlan_commands.to_a).to be_empty
      end
    end

    context 'when there are devices scheduled for vlan commands' do
      let(:organization) { create(:organization, gateway_vlan_collection_enabled: true) }
      let(:scheduled_devices) { create_list(:gateway_device_with_configuration, 1, :vlan_commandable, organization: organization) }

      before do
        expect(scheduled_devices.count).to be > 0
      end

      it 'returns a collection of hashes containing vlan command data' do
        commands = subject.gateway_vlan_commands.to_a
        expect(commands).not_to be_empty
        commands.each do |data|
          expect(data).to be_a Hash
          expect(data[:command]).to eq Gateway::FETCH_VLAN_CONFIG
        end
      end
    end
  end
end
