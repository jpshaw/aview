# frozen_string_literal: true

require 'rails_helper'

describe Import do
  include ActionView::Helpers::NumberHelper

  before do
    @import = create(:qa_import)
  end

  it {
    expect(subject).to enumerize(:the_type).in(:accelerated,
                                               :smx,
                                               :configuration,
                                               :organization,
                                               :smx_cellular).with_default(:smx)
  }

  it {
    expect(subject).to enumerize(:the_trigger).in(:job,
                                                  :admin).with_default(:job)
  }

  it {
    expect(subject).to enumerize(:the_action).in(:append,
                                                 :sync,
                                                 :update).with_default(:append)
  }

  it {
    expect(subject).to enumerize(:the_source_name).in(:settings,
                                                      :upload).with_default(:settings)
  }

  # Validations
  describe '#import_file_size' do
    context 'during creation' do
      before do
        expect(subject).not_to be_persisted
      end

      context 'without a valid attachment' do
        before do
          subject.import_file = nil
        end

        it 'is not checked' do
          expect(subject).not_to receive(:import_file_size)
          subject.valid?
        end
      end

      context 'with a valid attachment' do
        before do
          subject.import_file.cache!(
            File.open(
              File.join(
                Rails.root, 'spec', 'support', 'fixtures', 'att_qa_smx_netgate_list.tab'
              )
            )
          )
        end

        it 'is checked' do
          expect(subject).to receive(:import_file_size)
          subject.valid?
        end

        it 'must be within the limits set in uploader' do
          allow(subject.import_file.file).to receive(:size) { ImportFileUploader::FILE_SIZE_LIMIT + 1 }
          subject.valid?
          expect(subject.errors[:import_file]).to include("is over the size limit of #{number_to_human_size(ImportFileUploader::FILE_SIZE_LIMIT, precision: 2)}.")
        end
      end
    end

    context 'during update' do
      before do
        expect(@import).to be_persisted
      end

      it 'is not checked' do
        expect(@import).not_to receive(:import_file_size)
        subject.valid?
      end
    end
  end

  # Methods

  describe '#run!' do
    context 'when the import has already run' do
      it 'raises an exception indicating the import has already run' do
        @import.send :start_running!
        expect { @import.run! }.to raise_error(Import::ImportRunError, 'Import has already run')
      end
    end

    # Note: There is only one processor available for imports, so if an import
    # encounters 'another is running', it is most likely due to a failed import
    # or service interuption (jboss restart). It is OK to encounter this.
    context 'when another import is already running' do
      it 'successfully imports' do
        import = create(:qa_import)
        import.run!
        expect(import.the_state).to eq 'success'
      end
    end

    context 'when the source file is not found' do
      it 'sets the state to `file not found`' do
        File.delete(@import.import_file.path)
        @import.run!
        expect(@import.the_state).to eq 'file_not_found'
      end
    end

    context 'when an unspecified error occurs during the import' do
      before do
        @klass = @import.send(:import_class)
        eval <<-EVAL
          class #{@klass}
            class << self
              alias old_import_file import_file
              def import_file(x, y, z)
                raise "Import error"
              end
            end
          end
        EVAL
      end

      after do
        eval <<-EVAL
          class #{@klass}
            class << self
              alias import_file old_import_file
            end
          end
        EVAL
      end

      it 'sets the state to `failure`' do
        @import.run!
        expect(@import.the_state).to eq 'failure'
      end
    end

    context 'when an import successfully runs' do
      it 'sets the state to `success`' do
        @import.run!
        expect(@import.the_state).to eq 'success'
      end
    end
  end

  describe '#finished?' do
    context 'when the import is initializing or running' do
      it 'returns false' do
        expect(@import.the_state).to eq 'initializing'
        expect(@import.finished?).to be false
        @import.send :start_running!
        expect(@import.the_state).to eq 'running'
        expect(@import.finished?).to be false
      end
    end

    context 'when the import is in a finished state' do
      it 'returns true' do
        @import.send :start_running!
        @import.send :success!
        expect(@import.the_state).to eq 'success'
        expect(@import.finished?).to be true
      end
    end
  end

  describe '.finished' do
    context 'when a single import is finished and another one is running' do
      it 'returns the finished import' do
        import_1 = @import
        import_2 = create(:qa_import)
        import_1.run!
        expect(import_1.finished?).to be true
        expect(import_2.finished?).to be false
        expect(described_class.finished.count).to eq 1
      end
    end
  end

  # Functionality that happens in private methods.

  describe '#method_missing' do
    context 'when a method ending in `_ids` is called' do
      it 'returns an empty array' do
        expect(@import.testing_ids).to eq []
      end
    end

    context 'when a method ending in `_ids=` is called' do
      it 'sets the value for the method name' do
        @import.testing_ids = [1, 2, 3]
        expect(@import.testing_ids).to eq [1, 2, 3]
      end
    end

    context 'when a method not defined for the object and not ending in `_ids` is called' do
      it 'throws a NoMethodError exception' do
        expect { @import.this_method_does_not_exist }.to raise_error(NoMethodError)
      end
    end
  end

  describe '#purge_old_imports' do
    context 'when a new import is created' do
      context 'when the import count is below the limit' do
        it 'does nothing' do
          expect(described_class.count).to eq 1
          create(:qa_import)
          expect(described_class.count).to eq 2
        end
      end

      context 'when the import count is at the limit' do
        it 'keeps the count at the limit' do
          described_class.destroy_all # Remove any existing imports
          stub_const('Import::RECORD_LIMIT', 2)
          Import::RECORD_LIMIT.times do
            build(:blank_import).save(validate: false)
          end
          expect(described_class.count).to eq Import::RECORD_LIMIT
          create(:qa_import)
          expect(described_class.count).to eq Import::RECORD_LIMIT
        end
      end

      context 'when the import count is beyond the limit' do
        it 'keeps the count at the limit' do
          stub_const('Import::RECORD_LIMIT', 2)
          original_limit = Import::RECORD_LIMIT
          new_limit = Import::RECORD_LIMIT + 2

          stub_const('Import::RECORD_LIMIT', new_limit)

          Import::RECORD_LIMIT.times do
            build(:blank_import).save(validate: false)
          end

          expect(described_class.count > original_limit).to be true
          expect(described_class.count).to eq new_limit

          stub_const('Import::RECORD_LIMIT', original_limit)

          create(:qa_import)
          expect(described_class.count).to eq original_limit
        end
      end
    end
  end

  context 'when an import transitions to a finished state' do
    it 'sets the `finished_at` timestamp' do
      described_class.destroy_all
      described_class.finished_states.each do |state|
        @import = create(:qa_import)
        @import.send(:start_running!)
        expect(@import.the_state).to eq 'running'
        expect(@import.finished_at).to be nil
        @import.send("#{state}!")
        expect(@import.finished_at).not_to be nil
        expect(@import.finished_at).to be_a(Time)
        @import.destroy
      end
    end
  end

  describe '#remove_empty_organizations' do
    after do
      Settings.reload!
    end

    context 'when the feature is disabled' do
      before do
        settings = { imports: { remove_empty_organizations: false } }
        Settings.imports = {}
        Settings.merge!(settings)
      end

      it 'does not remove any organizations that have no devices' do
        @organization = create(:sub_organization)
        expect(@organization.devices.count).to eq 0
        expect do
          @import.send(:remove_empty_organizations)
        end.not_to change(Organization, :count)
      end
    end

    context 'when the feature is enabled' do
      before do
        settings = { imports: { remove_empty_organizations: true } }
        Settings.imports = {}
        Settings.merge!(settings)
      end

      it 'removes any organizations that have no devices' do
        @organization = create(:sub_organization)
        expect(@organization.devices.count).to eq 0
        expect do
          @import.send(:remove_empty_organizations)
        end.to change(Organization, :count).by(-1)
      end
    end
  end
end
