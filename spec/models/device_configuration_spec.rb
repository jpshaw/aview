# frozen_string_literal: true

require 'rails_helper'

describe DeviceConfiguration do
  it_behaves_like 'a searchable model'

  describe 'Validations' do
    describe '#fetch_value' do
      let(:organization)  { create(:organization) }
      let(:configuration) { create(:device_configuration_with_data, organization: organization) }
      let(:device)        { create(:remote_manager_device, organization: organization, configuration: configuration) }

      context 'when given nil' do
        it 'returns nil' do
          expect(device.configuration.fetch_value(nil)).to eq nil
        end
      end

      context 'when given a path that exists' do
        it 'returns the value' do
          expect(device.configuration.fetch_value(:schema, :version)).to eq '58'
        end
      end

      context 'when given a path that does not exist' do
        it 'returns nil' do
          expect(device.configuration.fetch_value(:a, :b, :c)).to eq nil
        end
      end
    end

    describe 'settings' do
      context 'with invalid JSON' do
        it 'errors out' do
          subject.settings = 'invalid_json'
          subject.valid?
          expect(subject.errors[:settings]).to include('is not valid JSON')
        end
      end

      context 'with valid JSON' do
        it 'passes validations' do
          subject.settings = { a: 'a' }.to_json
          subject.valid?
          expect(subject.errors[:settings]).to be_empty
        end
      end
    end

    describe 'grace_period_duration' do
      it 'is not validated if empty' do
        expect(subject.grace_period_duration).to be nil
        expect(subject).not_to receive(:valid_grace_period_duration?)
        subject.valid?
      end

      it 'is validated if present' do
        subject.grace_period_duration = 'present'
        expect(subject).to receive(:valid_grace_period_duration?)
        subject.valid?
      end

      it 'must have a valid duration string value' do
        subject.grace_period_duration = 'invalid_duration'
        subject.valid?
        expect(subject.errors.keys).to include(:grace_period_duration)

        subject.grace_period_duration = '3d'
        subject.valid?
        expect(subject.errors.keys).not_to include(:grace_period_duration)

        subject.grace_period_duration = '3d4h'
        subject.valid?
        expect(subject.errors.keys).not_to include(:grace_period_duration)
      end
    end

    describe 'settings_do_not_violate_firmware_schema' do
      context 'when the configuration firmware does not exist' do
        let(:config) { build(:device_configuration, settings: '{ "config": { "certificate_url": 1, "secure_port": 123 } }') }

        before { allow(config).to receive(:device_firmware).and_return(nil) }

        it 'does not validate the settings against a schema and allows the save' do
          expect(config.valid?).to be true
        end
      end

      context 'when the firmware schema does not exist' do
        let(:config) { build(:device_configuration, settings: '{ "config": { "certificate_url": 1, "secure_port": 123 } }') }

        before { allow(config.device_firmware).to receive(:schema_file).and_return(nil) }

        it 'does not validate the settings against a schema and allows the save' do
          expect(config.valid?).to be true
        end
      end

      context 'when the configuration firmware exists and has a schema' do
        it 'is valid if the settings do not violate the firmware schema' do
          valid_settings = '{ "config": { "certificate_url": "any string", "secure_port": 123 } }'
          config = build(:device_configuration, settings: valid_settings)
          expect(config.valid?).to be true
        end
      end
    end
  end

  describe '#destroy' do
    context 'when it has children' do
      let(:parent_config) { create(:device_configuration) }
      let!(:child_config) { create(:device_configuration, :with_sequenced_device_model, parent: parent_config) }

      it 'destroys the children' do
        expect { parent_config.destroy }.to change(described_class, :count).by(-2)
      end
    end
  end

  # TODO:  Remove this method tests when all device types are moved from configurations to
  #       device_configurations. The #devices method will disappear and these tests will no longer
  #       be needed.
  describe '#devices' do
    let(:device) { create(:dial_to_ip_device, configuration_id: nil, model_id: nil) }
    let(:device_model) { create(:test_model) }
    let(:device_configuration) { create(:device_configuration, :with_sequenced_device_model) }

    before do
      device_configuration.settings = '{}'
      device_configuration.save!
      allow(device_configuration).to receive(:device_model) { device_model }
      device  # Commit device to DB
      expect(Device.count).to be > 0
    end

    context 'with different configuration and different model' do
      before do
        expect(device.configuration_id).not_to  eq(device_configuration.id)
        expect(device.model_id).not_to          eq(device_configuration.device_model.id)
      end

      it 'cannot be found' do
        expect(device_configuration.devices.count).to be_zero
      end
    end

    context 'with different configuration and same model' do
      before do
        device.update_attribute(:model_id, device_model.id)
        device.reload
        expect(device.configuration_id).not_to  eq(device_configuration.id)
        expect(device.model_id).to              eq(device_configuration.device_model.id)
      end

      it 'cannot be found' do
        expect(device_configuration.devices.count).to be_zero
      end
    end

    context 'with same configuration and different model' do
      before do
        device.update_attribute(:configuration_id, device_configuration.id)
        device.reload
        expect(device.configuration_id).to  eq(device_configuration.id)
        expect(device.model_id).not_to      eq(device_configuration.device_model.id)
      end

      it 'cannot be found' do
        expect(device_configuration.devices.count).to be_zero
      end
    end

    context 'with same configuration and same model' do
      before do
        device.update_attribute(:configuration_id, device_configuration.id)
        device.update_attribute(:model_id, device_model.id)
        device.reload
        expect(device.configuration_id).to  eq(device_configuration.id)
        expect(device.model_id).to          eq(device_configuration.device_model.id)
      end

      it 'are found' do
        expect(device_configuration.devices.count).to be > 0
      end
    end
  end

  describe '#all_associated_devices' do
    let(:dial_to_ip_model) { create(:dial_to_ip_5300_DC_model) }
    let(:firmware) { create(:device_firmware, device_model: dial_to_ip_model) }
    let(:device_configuration) { create(:device_configuration, device_firmware: firmware) }
    let(:child_configuration) { create(:device_configuration, device_firmware: firmware, parent: device_configuration) }
    let!(:device_1) { create(:test_device, series: dial_to_ip_model, configuration_id: device_configuration.id) }
    let!(:device_2) { create(:test_device, series: dial_to_ip_model, configuration_id: child_configuration.id) }
    let!(:device_3) { create(:test_device) }

    let(:result) { device_configuration.all_associated_devices }

    it 'returns devices with the configuration or one of the child configurations' do
      expect(result).to include device_1
      expect(result).to include device_2
      expect(result).not_to include device_3
    end
  end

  describe '#status_freq' do
    context 'when settings have invalid JSON' do
      before do
        subject.settings = 'invalid_json'
        expect { JSON.parse(subject.settings) }.to raise_error(JSON::ParserError)
      end

      it 'returns nil' do
        expect(subject.status_freq).to be nil
      end
    end

    context 'when settings have valid JSON' do
      context 'without a status_freq setting' do
        let(:device_firmware) { DeviceFirmware.new }

        before do
          allow(subject).to receive(:device_firmware).and_return(device_firmware)
          subject.settings = {}.to_json
          expect { JSON.parse(subject.settings) }.not_to raise_error
        end

        it 'gets the value from the device firwmare schema' do
          expect(subject).to receive(:frequency_from_schema)
          subject.status_freq
        end

        context 'when the device firmware has no schema file' do
          it 'returns nil' do
            expect(subject.status_freq).to be nil
          end
        end

        context 'when the device firmware has schema file' do
          context 'when the schema file does not have valid JSON' do
            before do
              allow(device_firmware).to receive_message_chain(:schema_file, :read).and_return('invalid_json')
            end

            it 'returns nil' do
              expect(subject.status_freq).to be nil
            end
          end

          context 'when schema has valid JSON' do
            context 'without a status_freq setting' do
              before do
                allow(device_firmware).to receive_message_chain(:schema_file, :read).and_return({}.to_json)
              end

              it 'returns nil' do
                expect(subject.status_freq).to be nil
              end
            end

            context 'with a status_freq setting' do
              # The status frequency is found at properties.action.properties.accns_monitor.properties.reload[:default] in the JSON document.
              let(:json) do
                {
                  'properties' => {
                    'action' => {
                      'properties' => {
                        'accns_monitor' => {
                          'properties' => {
                            'reload' => {
                              'default' => '30m'
                            }
                          }
                        }
                      }
                    }
                  }
                }.to_json
              end

              before do
                allow(device_firmware).to receive_message_chain(:schema_file, :read).and_return(json)
              end

              it 'returns the setting value' do
                expect(subject.status_freq).to eq('30m')
              end
            end
          end
        end
      end

      context 'with a status_freq setting' do
        # The status frequency is found at action.accns_monitor.reload in the JSON document.
        let(:json) do
          {
            'action' => {
              'accns_monitor' => {
                'reload' => '15s'
              }
            }
          }.to_json
        end

        before do
          subject.settings = json
          expect { JSON.parse(subject.settings) }.not_to raise_error
        end

        it 'returns the setting value' do
          expect(subject.status_freq).to eq('15s')
        end
      end
    end
  end

  describe '#heartbeat_frequency' do
    it 'returns nil if no status_freq value' do
      allow(subject).to receive(:status_freq).and_return(nil)
      expect(subject.heartbeat_frequency).to be nil
    end

    it 'converts status_freq value to seconds' do
      allow(subject).to receive(:status_freq).and_return('1h1m')
      subject.save(validate: false)
      expect(subject.heartbeat_frequency).to eq(3660)
    end
  end

  describe '#grace_period' do
    it 'returns nil if no grace_period_duration value' do
      allow(subject).to receive(:grace_period_duration).and_return(nil)
      subject.save(validate: false)
      expect(subject.heartbeat_grace_period).to be nil
    end

    it 'converts grace_period_duration value to seconds' do
      allow(subject).to receive(:grace_period_duration).and_return('1h1m')
      subject.save(validate: false)
      expect(subject.heartbeat_grace_period).to eq(3660)
    end
  end

  describe '#credentials' do
    context 'when cached' do
      let(:user)        { Faker::Lorem.word }
      let(:password)    { Faker::Lorem.word }
      let(:credentials) { { user: user, password: password } }

      before do
        allow(Rails).to receive_message_chain(:cache, :read).and_return(credentials)
      end

      it 'returns an OpenStruct object with the cached credentials' do
        credentials = subject.credentials
        expect(credentials.class).to eq OpenStruct
        expect(credentials.user).to eq user
        expect(credentials.password).to eq password
      end
    end

    context 'when not cached' do
      before do
        allow(Rails).to receive_message_chain(:cache, :read).and_return(nil)
      end

      it 'returns an empty OpenStruct object' do
        credentials = subject.credentials
        expect(credentials.class).to eq OpenStruct
        expect(credentials.user).to be nil
        expect(credentials.password).to be nil
      end
    end
  end

  # Private methods.
  describe '#remote_control_disabled?' do
    context 'when enabled in description JSON' do
      before do
        allow(subject).to receive(:fetch_value)
          .with(:service, :remote_control, :enable)
          .and_return(true)
      end

      it 'returns false' do
        expect(subject.__send__(:remote_control_disabled?)).to be false
      end
    end

    context 'when not set in description JSON' do
      before do
        allow(subject).to receive(:fetch_value)
          .with(:service, :remote_control, :enable)
          .and_return(nil)
      end

      it 'returns false' do
        expect(subject.__send__(:remote_control_disabled?)).to be false
      end
    end

    context 'when disabled in description JSON' do
      before do
        allow(subject).to receive(:fetch_value)
          .with(:service, :remote_control, :enable)
          .and_return(false)
      end

      it 'returns true' do
        expect(subject.__send__(:remote_control_disabled?)).to be true
      end
    end
  end

  describe 'scopes' do
    describe '.with_category' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given a category' do
        let(:result) { described_class.with_category(@device_config_1.category) }

        it 'includes configurations with the given category' do
          expect(result).to include @device_config_1
          expect(result).not_to include @device_config_2
          expect(result).not_to include @device_config_3
        end
      end
    end

    describe '.with_organization' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model,
                                             :with_sub_organization)
      end

      context 'when given an organization with hierarchy' do
        let(:result) do
          described_class.with_organization(organization_id: @device_config_1.organization.id,
                                            include_hierarchy: 'true')
        end

        it 'includes configurations within suborganizations' do
          expect(result).to include @device_config_1
          expect(result).not_to include @device_config_2
          expect(result).to include @device_config_3
        end
      end

      context 'when given an organization without hierarchy' do
        let(:result) do
          described_class.with_organization(organization_id: @device_config_1.organization.id,
                                            include_hierarchy: 'false')
        end

        it 'returns configurations without suborganizations' do
          expect(result).to include @device_config_1
          expect(result).not_to include @device_config_2
          expect(result).not_to include @device_config_3
        end
      end
    end

    describe '.sort_by_name' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_name 'asc' }

        it 'sorts configurations with lower names first and higher names last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_name 'desc' }

        it 'sorts configurations with higher names first and lower names last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end

    describe '.sort_by_organizations_name' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_organizations_name 'asc' }

        it 'sorts configurations with lower organization names first and higher names last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_organizations_name 'desc' }

        it 'sorts configurations with higher organization names first and lower names last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end

    describe '.sort_by_device_categories_name' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_device_categories_name 'asc' }

        it 'sorts configurations with lower device category names first and higher names last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_device_categories_name 'desc' }

        it 'sorts configurations with higher device category names first and lower names last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end

    describe '.sort_by_device_models_name' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_device_models_name 'asc' }

        it 'sorts configurations with lower device model names first and higher names last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_device_models_name 'desc' }

        it 'sorts configurations with higher device model names first and lower names last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end

    describe '.sort_by_device_firmware' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_device_firmware 'asc' }

        it 'sorts configurations with lower device firmware first and higher firmware last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_device_firmware 'desc' }

        it 'sorts configurations with higher device firmware first and lower firmware last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end

    describe '.sort_by_updated_at' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model,
                                             updated_at: 1.month.ago)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model,
                                             updated_at: 1.week.ago)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_updated_at 'asc' }

        it 'sorts configurations with later updated times first and null times last' do
          expect(result.first).to eq @device_config_1
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_updated_at 'desc' }

        it 'sorts configurations with null times first and later updated times last' do
          expect(result.first).to eq @device_config_3
          expect(result.second).to eq @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model,
                                             :with_sub_organization)
      end

      context 'when given an organization with hierarchy and sorting by device models name' do
        let(:result) do
          described_class.search(
            with_organization: { organization_id: @device_config_1.organization.id, include_hierarchy: 'true' },
            sort_by_device_models_name: 'desc'
          )
        end

        it 'includes devices within suborganizations sorted correctly' do
          expect(result.first).to eq @device_config_3
          expect(result).not_to include @device_config_2
          expect(result.last).to eq @device_config_1
        end
      end

      context 'when searching all device configurations' do
        let(:result) { described_class.search(all: nil) }

        it 'returns all netbridge configurations' do
          expect(result).to include @device_config_1
          expect(result).to include @device_config_2
          expect(result).to include @device_config_3
        end
      end
    end

    describe '.search!' do
      before do
        @device_config_1 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_2 = create(:device_configuration, :with_sequenced_device_model)
        @device_config_3 = create(:device_configuration, :with_sequenced_device_model,
                                             :with_sub_organization)
      end

      context 'when given an organization with hierarchy and sorting by device categories name' do
        let(:result) do
          described_class.search!(
            with_organization: { organization_id: @device_config_1.organization.id, include_hierarchy: 'true' },
            sort_by_device_categories_name: 'asc'
          )
        end

        it 'includes devices within suborganizations sorted correctly' do
          expect(result.first).to eq @device_config_1
          expect(result).not_to include @device_config_2
          expect(result.last).to eq @device_config_3
        end
      end
    end
  end
end
