# frozen_string_literal: true

require 'rails_helper'

describe Search do
  let(:current_user)      { create(:user) }
  let(:valid_model)       { Search::MODELS_COLUMNS.keys.first }
  let(:valid_column)      { Search::MODELS_COLUMNS.first.last.keys.first }
  let(:valid_match_type)  { Search::MATCH_TYPES.keys.first }
  let(:invalid)           { 'INVALID' }

  describe 'Validations' do
    describe 'model' do
      context 'when not present' do
        it 'is not validated' do
          search = described_class.new(current_user, {})
          search.valid?
          expect(search.errors.keys).not_to include(:model)
        end
      end

      context 'when present' do
        context 'when it is one of the tracked models' do
          it 'is valid' do
            Search::MODELS_COLUMNS.keys.each do |model|
              search = described_class.new(current_user, model: model)
              search.valid?
              expect(search.errors.keys).not_to include(:model)
            end
          end
        end

        context 'when it is not one o the tracked models' do
          it 'is invalid' do
            search = described_class.new(current_user, model: invalid)
            search.valid?
            expect(search.errors.keys).to include(:model)
          end
        end
      end
    end

    describe 'column' do
      context 'without a model' do
        it 'is not validated' do
          search = described_class.new(current_user, column: invalid)
          expect(search).not_to receive(:valid_model_column?)
          search.valid?
          expect(search.errors.keys).not_to include(:column)
        end

        it 'gets a default value' do
          search = described_class.new(current_user, column: invalid)
          search.valid?
          expect(search.__send__(:column)).to eq Search::DEFAULT_COLUMN
        end
      end

      context 'with a valid model' do
        context 'when it is one of the tracked columns for the model' do
          it 'is valid' do
            Search::MODELS_COLUMNS[valid_model].keys.each do |column|
              search = described_class.new(current_user, model: valid_model, column: column)
              search.valid?
              expect(search.errors.keys).not_to include(:column)
            end
          end
        end

        context 'when it is not one of the tracked columns for the model' do
          it 'is invalid' do
            search = described_class.new(current_user, model: valid_model, column: invalid)
            search.valid?
            expect(search.errors.keys).to include(:column)
          end
        end
      end

      context 'with an invalid model' do
        it 'is not validated' do
          search = described_class.new(current_user, model: invalid, column: valid_column)
          expect(search).not_to receive(:valid_model_column?)
          search.valid?
          expect(search.errors.keys).not_to include(:column)
        end
      end
    end

    describe 'match_type' do
      context 'when not present' do
        it 'gets a default value' do
          search = described_class.new(current_user, {})
          search.valid?
          expect(search.__send__(:match_type)).to eq Search::DEFAULT_MATCH_TYPE
        end
      end

      context 'when it is one of the tracked match types' do
        it 'is valid' do
          Search::MATCH_TYPES.keys.each do |match_type|
            search = described_class.new(current_user, match_type: match_type)
            search.valid?
            expect(search.errors.keys).not_to include(:match_type)
          end
        end
      end

      context 'when it does not match one of the MODELS_COLUMNS keys' do
        it 'is invalid' do
          search = described_class.new(current_user, match_type: invalid)
          search.valid?
          expect(search.errors.keys).to include(:match_type)
        end
      end
    end

    describe 'value' do
      context 'when present' do
        it 'is valid' do
          search = described_class.new(current_user, value: 'A_VALUE')
          search.valid?
          expect(search.errors.keys).not_to include(:value)
        end

        it 'removes colons and trailing/leading spaces from the search value' do
          expect(described_class.new(current_user, value: ' :VA:LUE ').description).to include('VALUE')
        end
      end

      context 'when it does not match one of the MODELS_COLUMNS keys' do
        it 'is invalid' do
          search = described_class.new(current_user, {})
          search.valid?
          expect(search.errors.keys).to include(:value)

          search = described_class.new(current_user, value: nil)
          search.valid?
          expect(search.errors.keys).to include(:value)

          search = described_class.new(current_user, value: '')
          search.valid?
          expect(search.errors.keys).to include(:value)
        end
      end
    end
  end

  describe '#devices' do
    let(:arguments) do
      {
        model:      nil,
        column:     nil,
        match_type: Search::MATCH_TYPES.keys.first,
        value:      'a_value'
      }
    end

    context 'without a model' do
      subject { described_class.new(current_user, arguments) }

      it 'runs a basic query' do
        allow(subject).to receive(:basic_query)
        allow(subject).to receive(:device_query)
        allow(subject).to receive(:site_query)
        allow(subject).to receive(:location_query)
        allow(subject).to receive(:country_query)
        allow(subject).to receive(:device_modem_query)
        subject.devices
        expect(subject).to      have_received(:basic_query)
        expect(subject).not_to  have_received(:device_query)
        expect(subject).not_to  have_received(:site_query)
        expect(subject).not_to  have_received(:location_query)
        expect(subject).not_to  have_received(:country_query)
        expect(subject).not_to  have_received(:device_modem_query)
      end
    end

    context "when the model is 'device'" do
      subject { described_class.new(current_user, arguments) }

      let(:model) { 'device' }

      before do
        arguments[:model]  = model
        arguments[:column] = Search::MODELS_COLUMNS[model].keys.first
      end

      it 'runs a device based query' do
        allow(subject).to receive(:device_query).and_return(Device)
        allow(subject).to receive(:site_query)
        subject.devices
        expect(subject).to      have_received(:device_query)
        expect(subject).not_to  have_received(:site_query)
      end
    end

    context "when the model is 'site'" do
      subject { described_class.new(current_user, arguments) }

      let(:model) { 'site' }

      before do
        arguments[:model]  = model
        arguments[:column] = Search::MODELS_COLUMNS[model].keys.first
      end

      it 'runs a site based query' do
        allow(subject).to receive(:device_query)
        allow(subject).to receive(:site_query).and_return(Device)
        allow(subject).to receive(:location_query)
        subject.devices
        expect(subject).to      have_received(:site_query)
        expect(subject).not_to  have_received(:location_query)
      end
    end

    context "when the model is 'location'" do
      subject { described_class.new(current_user, arguments) }

      let(:model) { 'location' }

      before do
        arguments[:model]  = model
        arguments[:column] = Search::MODELS_COLUMNS[model].keys.first
      end

      it 'runs a location based query' do
        allow(subject).to receive(:device_query)
        allow(subject).to receive(:site_query)
        allow(subject).to receive(:location_query).and_return(Device)
        allow(subject).to receive(:country_query)
        subject.devices
        expect(subject).to      have_received(:location_query)
        expect(subject).not_to  have_received(:country_query)
      end
    end

    context "when the model is 'country'" do
      subject { described_class.new(current_user, arguments) }

      let(:model) { 'country' }

      before do
        arguments[:model]  = model
        arguments[:column] = Search::MODELS_COLUMNS[model].keys.first
      end

      it 'runs a country based query' do
        allow(subject).to receive(:device_query)
        allow(subject).to receive(:site_query)
        allow(subject).to receive(:location_query)
        allow(subject).to receive(:country_query).and_return(Device)
        allow(subject).to receive(:device_modem_query)
        subject.devices
        expect(subject).to      have_received(:country_query)
        expect(subject).not_to  have_received(:device_modem_query)
      end
    end

    context "when the model is 'device_modem'" do
      subject { described_class.new(current_user, arguments) }

      let(:model) { 'device_modem' }

      before do
        arguments[:model]  = model
        arguments[:column] = Search::MODELS_COLUMNS[model].keys.first
      end

      it 'runs a device modem based query' do
        allow(subject).to receive(:device_query)
        allow(subject).to receive(:site_query)
        allow(subject).to receive(:location_query)
        allow(subject).to receive(:country_query)
        allow(subject).to receive(:device_modem_query).and_return(Device)
        subject.devices
        expect(subject).to have_received(:device_modem_query)
      end
    end

    it 'returns a Device relation' do
      arguments[:model]  = Search::MODELS_COLUMNS.keys.first
      arguments[:column] = Search::MODELS_COLUMNS.first.last.keys.first
      return_value = described_class.new(current_user, arguments).devices
      expect(return_value.class).to eq Device::ActiveRecord_Relation
      expect(return_value.name).to  eq 'Device'
    end
  end

  describe '#description' do
    let(:arguments) do
      {
        model:      Search::MODELS_COLUMNS.keys.first,
        column:     Search::MODELS_COLUMNS.first.last.keys.first,
        match_type: Search::MATCH_TYPES.keys.first,
        value:      'a_value'
      }
    end

    it 'returns a string describing the search' do
      expect(described_class.new(current_user, arguments).description.class).to eq String
    end
  end
end
