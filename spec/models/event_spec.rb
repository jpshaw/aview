# frozen_string_literal: true

require 'rails_helper'

describe DeviceEvent do
  describe 'methods' do
    before do
      create_device
    end

    describe '#info' do
      it 'returns an empty string if information is nil' do
        event = create_device_event(information: nil)
        expect(event.info).to eq ''
      end

      it 'returns a string of information if set' do
        event = create_device_event(information: 'Hello World')
        expect(event.info).to eq 'Hello World'
      end
    end

    describe '#lvl' do
      it 'returns the default event level if nil is set' do
        event = create_device_event(level: nil)
        expect(event.level).to be nil
        expect(event.lvl).to eq DeviceEvent::DEFAULT_LEVEL_NAME
      end

      it 'returns an event level if it is set' do
        event = create_device_event
        event.level = EventLevels::CRITICAL
        expect(event.lvl).to eq 'Critical'
      end

      it 'returns the correct name for all available event levels' do
        event = create_device_event
        levels = EventLevels.constants_hash
        default = DeviceEvent::DEFAULT_LEVEL_NAME
        levels.each do |key, value|
          event.level = value
          expect(event.lvl).to eq(I18n.t(key, default: default))
        end
      end
    end

    describe '#type' do
      it 'returns the default event type if nil is set' do
        event = create_device_event(event_type: nil)
        expect(event.event_type).to be nil
        expect(event.type).to eq DeviceEvent::DEFAULT_TYPE_NAME
      end

      it 'returns an event type if it is set' do
        event = create_device_event
        event.event_type = EventTypes::CONFIG
        expect(event.type).to eq 'Config'
      end

      it 'returns the correct name for all available event types' do
        event = create_device_event
        types = EventTypes.constants_hash
        default = DeviceEvent::DEFAULT_TYPE_NAME
        types.each do |key, value|
          event.event_type = value
          expect(event.type).to eq(I18n.t(key, default: default))
        end
      end
    end

    describe '#notify_subscribers' do
      before do
        create_profile_for_immediate_gateway_notifications
        create_subscriptions_for_gateway_device
      end

      context 'when a device is undeployed' do
        it 'does not generate a notification' do
          @device.update is_deployed: false
          expect(@device.deployed?).to be false
          create_device_event(uuid_id: @uuids.first.id)
          expect(@event).not_to be nil
          expect(Notification.find_by(event_id: @event.id)).to be nil
        end
      end
    end

    describe '.type_code_for_symbol' do
      it 'returns the default event type if passed nil' do
        type = described_class.type_code_for_symbol(nil)
        expect(type).not_to be nil
        expect(type).to eq DeviceEvent::DEFAULT_TYPE_CODE
      end

      it 'returns the correct type code for a valid symbol' do
        code_name  = :FIRMWARE
        code_sym   = code_name.downcase
        code_value = eval("EventTypes::#{code_name}")
        type = described_class.type_code_for_symbol(code_sym)
        expect(type).not_to be nil
        expect(type).to eq code_value
      end
    end

    describe '.level_code_for_symbol' do
      it 'returns the default event level if passed nil' do
        level = described_class.level_code_for_symbol(nil)
        expect(level).not_to be nil
        expect(level).to eq DeviceEvent::DEFAULT_LEVEL_CODE
      end

      it 'returns the correct level code for a valid symbol' do
        code_name  = :WARNING
        code_sym   = code_name.downcase
        code_value = eval("EventLevels::#{code_name}")
        level = described_class.level_code_for_symbol(code_sym)
        expect(level).not_to be nil
        expect(level).to eq code_value
      end
    end
  end
end
