# frozen_string_literal: true

require 'rails_helper'

describe Organization do
  it_behaves_like 'a manageable resource' do
    let(:manageable) { create(:random_regular_organization) }
  end

  it_behaves_like 'a manager of resources' do
    let(:parent_organization) { create(:random_regular_organization) }
    let(:organization)        { create(:random_regular_organization) }
    let(:manager)             { organization }
  end

  describe 'methods' do
    before do
      create_organization
    end

    describe '#self_managing_permission' do
      specify 'manageable should be itself' do
        expect(@organization.self_managing_permission.manageable).to eq(@organization)
      end

      specify 'manager should be itself' do
        expect(@organization.self_managing_permission.manager).to eq(@organization)
      end

      specify "role should be 'admin'" do
        expect(@organization.self_managing_permission.role).to eq(@organization.admin_role)
      end
    end

    describe '#logo_path' do
      context 'When the organization has a logo' do
        before do
          allow(@organization).to receive(:logo).and_return('a_logo')
          allow(@organization).to receive_message_chain(:self_and_ancestors, :where, :limit).and_return([@organization])
        end

        it "returns the organization's logo" do
          expect(@organization.logo_path).to eq('a_logo')
        end
      end

      context 'When the organization has no logo' do
        let(:parent)        { create(:random_regular_organization) }
        let(:grand_parent)  { create(:random_regular_organization) }

        before do
          grand_parent.children << parent
          parent.children       << @organization

          allow(@organization).to receive(:logo).and_return(nil)
          allow(parent).to receive(:logo).and_return(nil)
        end

        context 'When the organization has no logo but acestor organizations do' do
          before do
            allow(grand_parent).to receive(:logo).and_return('grand_parent_logo')
          end

          it "returns the closest ancestor organization's logo" do
            allow(@organization).to receive_message_chain(:self_and_ancestors, :where, :limit).and_return([grand_parent])
            expect(@organization.logo_path).to eq('grand_parent_logo')

            allow(parent).to receive(:logo).and_return('parent_logo')
            allow(@organization).to receive_message_chain(:self_and_ancestors, :where, :limit).and_return([parent, grand_parent])
            expect(@organization.logo_path).to eq('parent_logo')
          end
        end

        context 'When no organization has a logo' do
          before do
            allow(grand_parent).to receive(:logo).and_return(nil)
          end

          it 'returns the default logo from the Settings' do
            expect(@organization.logo_path).to eq(Settings.logo.image)
          end
        end
      end
    end

    describe '#footer_text' do
      context 'When the organization has a footer' do
        let(:footer) { 'a_footer' }

        before do
          @organization.update_attribute(:footer, footer)
        end

        it "returns the organization's footer" do
          expect(@organization.footer_text).to eq(footer)
        end
      end

      context 'When the organization has no footer' do
        let(:parent)        { create(:random_regular_organization) }
        let(:grand_parent)  { create(:random_regular_organization) }

        before do
          grand_parent.children << parent
          parent.children       << @organization

          @organization.update_attribute(:footer, nil)
          parent.update_attribute(:footer, nil)
        end

        context 'When the organization has no footer but acestor organizations do' do
          before do
            grand_parent.update_attribute(:footer, 'grand_parent_footer')
          end

          it "returns the closest ancestor organization's footer" do
            expect(@organization.footer_text).to eq('grand_parent_footer')
            parent.update_attribute(:footer, 'parent_footer')
            expect(@organization.footer_text).to eq('parent_footer')
          end
        end

        context 'When no organization has a footer' do
          let(:settings_footer) { 'a_settings_footer' }

          before do
            allow(grand_parent).to receive(:footer).and_return(nil)
            allow(Settings).to receive(:footer).and_return(settings_footer)
          end

          it 'returns the default footer from the Settings' do
            expect(@organization.footer_text).to eq(settings_footer)
          end
        end
      end
    end

    describe '#service_name' do
      let(:service_name)      { 'A_SERVICE_NAME' }
      let(:customer_service)  { CustomerService.new(name: service_name) }

      context 'when the device organization belongs to a customer service' do
        before do
          allow(subject).to receive(:customer_service).and_return(customer_service)
        end

        it "returns the service name of the organization's customer service" do
          expect(subject.service_name).to eq(service_name)
        end
      end

      context 'when the device organization does not belongs to a customer service' do
        let(:parent_organization) { described_class.new }

        before do
          expect(subject).to receive(:customer_service).and_return(nil)
          allow(subject).to receive(:parent).and_return(parent_organization)
        end

        context 'when an organization in the hierarchy belongs to a customer service' do
          before do
            expect(parent_organization).to receive(:customer_service).and_return(customer_service)
          end

          it "returns the service name of the parent organization's customer service" do
            expect(subject.service_name).to eq(service_name)
          end
        end

        context 'when no organization in the hierarchy belongs to a customer service' do
          before do
            expect(parent_organization).to receive(:customer_service).and_return(nil)
          end

          it 'returns nil' do
            expect(subject.service_name).to be nil
          end
        end
      end
    end
  end

  describe '#self_and_descendant_devices' do
    before do
      # Create organizations
      @root    = create(:organization)
      @child_1 = create(:random_boundary_organization)
      @child_2 = create(:random_boundary_organization)
      @grandchild_1 = create(:random_boundary_organization)

      # Create hierarchy
      @root.add_child(@child_1)
      @root.add_child(@child_2)
      @child_1.add_child(@grandchild_1)

      # Add devices
      create(:test_device, organization_id: @root.id)
      create(:test_device, organization_id: @child_1.id)
      create(:test_device, organization_id: @child_2.id)
      create(:test_device, organization_id: @grandchild_1.id)
    end

    context 'when called for a child organization' do
      it 'returns devices for the child and grandchild' do
        expected_devices = Device.where(organization_id: [@child_1.id, @grandchild_1.id])
        expect(@child_1.self_and_descendant_devices.sort).to eq expected_devices.sort
      end

      it 'does not return devices for other organizations' do
        unexpected_devices = Device.where(organization_id: [@root.id, @child_2.id])
        expect(@child_1.self_and_descendant_devices).not_to include(unexpected_devices)
      end
    end
  end

  describe 'creating a new organization' do
    before do
      create_organization
    end

    it 'creates a default site when it is a boundary organization' do
      create_child_boundary_organization
      expect(@new_boundary_organization.sites.first.name).to eq 'Default'
      expect(@new_boundary_organization.sites.count).to eq 1
    end
  end

  describe '#check_name' do
    context "when the organization name contains the string 'at&t' (ignoring case)" do
      let(:lowercased_name) { '>>>>> at&t <<<<<' }
      let(:uppercased_name) { '>>>>> AT&T <<<<<' }

      before do
        subject.name = lowercased_name
      end

      it "converts the string 'at&t' (ignoring case) to 'AT&T'" do
        subject.__send__(:check_name)
        expect(subject.name).to eq uppercased_name
      end
    end

    context "when the organization name does not contains the string 'at&t'" do
      let(:name) { 'A company name' }

      before do
        subject.name = name
      end

      it 'does not change the name' do
        subject.__send__(:check_name)
        expect(subject.name).to eq name
      end
    end
  end

  describe '#organization_count' do
    it 'returns the number of descendants of the organization' do
      3.times do
        count = Random.rand(100)
        allow(subject).to receive_message_chain(:descendants, :count).and_return(count)
        expect(subject.organization_count).to eq(count)
      end
    end
  end

  describe '#ancestor_id_list' do
    let(:parent)  { create(:random_regular_organization) }
    let(:child)   { create(:random_regular_organization) }
    let(:ids)     { [parent.id, child.id] }

    before do
      parent.children << child
    end

    context 'when including self' do
      it "returns the IDs of its ancestors and itelf's" do
        expect(child.ancestor_id_list(true).sort).to eq(ids.sort)
      end
    end

    context 'when not including self' do
      it 'returns the IDs of its ancestors' do
        expect(child.ancestor_id_list(false)).to eq([parent.id])
      end
    end
  end

  describe '#sorted_hash_tree_organizations' do
    # Using names that help with sort testing.
    let(:parent)              { create(:random_regular_organization, name: "Gary and Ella's parent") }
    let(:first_child)         { create(:random_regular_organization, name: 'Gary, father of Beth') }
    let(:first_grand_child)   { create(:random_regular_organization, name: 'Beth, daughter of Gary') }
    let(:second_child)        { create(:random_regular_organization, name: 'Ella, mother of Amy') }
    let(:second_grand_child)  { create(:random_regular_organization, name: 'Amy,  daughter of Ella') }

    before do
      # Using creation sequence that helps sort testing by ID values.
      parent.children       << first_child
      first_child.children  << first_grand_child
      parent.children       << second_child
      second_child.children << second_grand_child

      # Verify that sorting the organizations by name and by ID produces different results.
      orgs = [parent, first_child, first_grand_child, second_child, second_grand_child]
      expect(orgs.sort_by(&:name)).not_to eq(orgs.sort_by(&:id))
    end

    it "returns the organization's hash tree in an array" do
      expected = [parent, first_child, first_grand_child, second_child, second_grand_child]
      expect(parent.sorted_hash_tree_organizations.sort_by(&:name)).to eq(expected.sort_by(&:name))
    end

    it 'sorts the organizations first by hierarchy, then by name by default' do
      # This expected sequence is sorted first by hierarchy, then by name.
      expected = [parent, second_child, second_grand_child, first_child, first_grand_child]
      expect(parent.sorted_hash_tree_organizations).to eq(expected)
    end

    it 'sorts the organizations first by hierarchy, then by value when specified' do
      # This expected sequence is sorted first by hierarchy, then by ID.
      expected = [parent, first_child, first_grand_child, second_child, second_grand_child]
      expect(parent.sorted_hash_tree_organizations(:id)).to eq(expected)
    end

    it 'can return different results depending on sorting value' do
      expect(parent.sorted_hash_tree_organizations(:id)).not_to eq(parent.sorted_hash_tree_organizations(:name))
    end
  end

  # Class methods
  describe '.head_organizations_in' do
    context 'when receiving a single organization' do
      let(:organization)  { described_class.new }
      let(:list)          { organization }

      it 'returns an array containing the organization' do
        expect(described_class.head_organizations_in(list)).to eq([organization])
      end
    end

    context 'when receiving an array with a single organization' do
      let(:organization)  { create(:random_regular_organization) }
      let(:list)          { [organization] }

      it 'returns an array containing the organization' do
        expect(described_class.head_organizations_in(list)).to eq(list)
      end
    end

    context 'when receiving organizations from different (sub)branches' do
      let(:first_head)  { create(:random_regular_organization) }
      let(:second_head) { create(:random_regular_organization) }
      let(:list)        { [first_head, second_head] }

      before do
        expect(first_head.descendants).not_to   include(second_head)
        expect(second_head.descendants).not_to  include(first_head)
      end

      it 'returns an array containing all received organizations' do
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(list.sort_by(&:id))
      end
    end

    context 'when receiving organizations from same (sub)branches' do
      let(:first_head)          { create(:random_regular_organization) }
      let(:first_child)         { create(:random_regular_organization) }
      let(:first_grand_child)   { create(:random_regular_organization) }

      let(:second_head)         { create(:random_regular_organization) }
      let(:second_child)        { create(:random_regular_organization) }
      let(:second_grand_child)  { create(:random_regular_organization) }

      let(:third_head)          { create(:random_regular_organization) }
      let(:third_child)         { create(:random_regular_organization) }
      let(:third_grand_child)   { create(:random_regular_organization) }

      before do
        # Setup hierarchy branches.
        first_head.children   << first_child
        first_child.children  << first_grand_child

        second_head.children  << second_child
        second_child.children << second_grand_child

        third_head.children   << third_child
        third_child.children  << third_grand_child
      end

      it 'returns only the highest organization in each (sub)branch' do
        list = [
          first_head,   first_child,  first_grand_child,
          second_head,  second_child, second_grand_child,
          third_head,   third_child,  third_grand_child
        ]
        expected = [first_head, second_head, third_head].sort_by(&:id)
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(expected)

        list = [
          first_head,   first_child,  first_grand_child,
          second_child, second_grand_child,
          third_grand_child
        ]
        expected = [first_head, second_child, third_grand_child].sort_by(&:id)
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(expected)

        # Notice that order or organizations in the list does not matter.
        list = [
          first_grand_child,  first_child, first_head,
          second_grand_child, second_child, second_head,
          third_grand_child,  third_child,  third_head
        ]
        expected = [first_head, second_head, third_head].sort_by(&:id)
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(expected)

        list = [
          first_grand_child,  first_child,
          third_child
        ]
        expected = [first_child, third_child].sort_by(&:id)
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(expected)

        list = [
          first_grand_child,
          second_grand_child,
          third_child,
          second_head
        ]
        expected = [first_grand_child, second_head, third_child].sort_by(&:id)
        expect(described_class.head_organizations_in(list).sort_by(&:id)).to eq(expected)
      end
    end
  end

  describe '#destroy' do
    context 'when the organization has data plans' do
      let(:organization) { create(:organization) }

      before do
        create(:data_plan, organization: organization)
      end

      it 'destroys the data plans so that the organization can be destroyed' do
        expect do
          organization.destroy
        end.to change(described_class, :count).by(-1)
      end

      it 'destroys the organization' do
        expect do
          organization.destroy
        end.to change(DataPlan, :count).by(-1)
      end
    end
  end
end
