# frozen_string_literal: true

require 'rails_helper'

describe AdminSettings do
  describe '.valid?' do
    it 'validates the threshold percentage value' do
      expect(described_class).to receive(:valid_threshold_percentage?)
      described_class.valid?
    end
  end

  describe '.valid_threshold_percentage?' do
    let(:first_range_value)         { AdminSettings::THRESHOLD_PERCENTAGE_VALID_RANGE.first }
    let(:last_range_value)          { AdminSettings::THRESHOLD_PERCENTAGE_VALID_RANGE.last }
    let(:middle_range_value)        { ((last_range_value.to_i - first_range_value.to_i + 1) / 2).to_i }
    let(:valid_range_values)        { [first_range_value, middle_range_value, last_range_value] }

    let(:before_first_range_value)  { first_range_value.to_i - 1 }
    let(:after_last_range_value)    { last_range_value.to_i + 1 }
    let(:invalid_threshold_value)   { 'INVALID' }
    let(:invalid_range_values)      { [before_first_range_value, after_last_range_value, invalid_threshold_value] }

    context 'with a valid range value' do
      it 'returns true' do
        valid_range_values.each do |value|
          described_class.threshold_percentage = value
          expect(described_class.valid_threshold_percentage?).to be true
        end
      end
    end

    context 'with a valid character range value' do
      it 'returns true' do
        valid_range_values.each do |value|
          described_class.threshold_percentage = value.to_s
          expect(described_class.valid_threshold_percentage?).to be true
        end
      end

      it 'converts character value to integer' do
        valid_range_values.each do |value|
          described_class.threshold_percentage = value.to_s
          expect(described_class.threshold_percentage.class).to be String
          described_class.valid_threshold_percentage?
          expect(described_class.threshold_percentage).to be_an Integer
        end
      end
    end

    context 'with an invalid range value' do
      it 'returns false' do
        invalid_range_values.each do |value|
          described_class.threshold_percentage = value
          expect(described_class.valid_threshold_percentage?).to be false
        end
      end
    end
  end

  describe '.errors' do
    context 'with errors' do
      before do
        described_class.threshold_percentage = nil
        described_class.valid?
      end

      it 'returns an array of error messages' do
        expect(described_class.errors.class).to be(Array)
        expect(described_class.errors).not_to be_empty
      end
    end

    context 'without errors' do
      before do
        described_class.threshold_percentage = AdminSettings::THRESHOLD_PERCENTAGE_VALID_RANGE.first
        described_class.valid?
      end

      it 'returns an empty array' do
        expect(described_class.errors).to eq []
      end
    end
  end
end
