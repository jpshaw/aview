# frozen_string_literal: true

require 'rails_helper'

describe DialToIp do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', DeviceConfiguration.new
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:dial_to_ip_device) }
  end

  it_behaves_like 'a uclinux device' do
    let(:platform) { :uc_linux }
    let(:test_serial) { '5300019999999907' }
    let(:device)   { create(:dial_to_ip_device) }
  end
  it_behaves_like 'a uclinux device with modem' do
    let(:platform) { :uc_linux }
    let(:device)   { create(:dial_to_ip_device) }
  end
  it_behaves_like 'it has a cellular modem'

  describe '#cellular_modem_supported?' do
    # Currently only the 5300-DC model supports cellular modems.
    let(:cellular_model)     { DialToIp::CELLULAR_MODELS.sample }
    let(:non_cellular_model) { DialToIp::DC_5301_MODEL }

    context 'when called on a device that supports cellular modems' do
      let(:device) { described_class.new(hw_version: cellular_model) }

      it 'returns true' do
        expect(device.cellular_modem_supported?).to be true
      end
    end

    context 'when called on a device that does not support cellular modems' do
      let(:device) { described_class.new(hw_version: non_cellular_model) }

      it 'returns false' do
        expect(device.cellular_modem_supported?).to be false
      end
    end
  end
end
