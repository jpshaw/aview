# frozen_string_literal: true

require 'rails_helper'

describe DeviceNetwork do
  context 'when a network exists for the device with the same network_iface' do
    let(:device) { create(:test_device) }

    before do
      described_class.create(device_id: device.id, network_iface: 'lan1')
    end

    it 'does not create a new device network' do
      expect do
        described_class.create(device_id: device.id, network_iface: 'lan1')
      end.not_to change(described_class, :count)
    end
  end

  describe '#ip_addresses' do
    let(:ip1) { '192.168.210.1/24' }
    let(:ip2) { '169.254.100.100/16' }
    let(:ip3) { 'fd00:2704::227:4ff:fe03:278/64' }

    let(:ip_index_map) { { '1' => ip1, '2' => ip2, '3' => ip3 } }

    before { subject.ip_addrs = {} }

    context 'when no ip entries are stored' do
      before { expect(subject.ip_addrs).to be_blank }

      it 'returns an empty hash' do
        expect(subject.ip_addresses).to eq({})
      end
    end

    context 'when ip entries are stored' do
      before do
        subject.ip_addrs[:ip1] = ip1
        subject.ip_addrs[:ip2] = ip2
        subject.ip_addrs[:ip3] = ip3
      end

      it 'returns a hash of the ip index mapped to the address' do
        expect(subject.ip_addresses).to eq(ip_index_map)
      end
    end

    context 'when some ip entries do not have a valid index' do
      before do
        subject.ip_addrs[:ip1] = ip1
        subject.ip_addrs[:ipa] = ip1
        subject.ip_addrs[:ip2] = ip2
        subject.ip_addrs[:ipb] = ip2
        subject.ip_addrs[:ip3] = ip3
        subject.ip_addrs[:ipc] = ip3
      end

      it 'returns a hash for entries with a valid index' do
        expect(subject.ip_addresses).to eq(ip_index_map)
      end
    end

    context 'when an ip entry has a blank address' do
      before do
        subject.ip_addrs[:ip1] = ip1
        subject.ip_addrs[:ip2] = ''
      end

      it 'is not returned' do
        expect(subject.ip_addresses).to eq('1' => ip1)
      end
    end
  end
end
