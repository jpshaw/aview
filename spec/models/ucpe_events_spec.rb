# frozen_string_literal: true

require 'rails_helper'

# Tests the mixin located at app/concerns/events.rb.
describe Ucpe do
  describe 'events' do
    before do
      create_device
    end

    describe '#event' do
      it 'returns nil if nil is passed' do
        expect(@device.event(nil)).to be nil
      end

      it 'returns nil if something other than a hash is passed' do
        expect(@device.event([1, 2, 3])).to be nil
      end

      it 'returns a generic event if nothing is passed' do
        expect(@device.event).not_to be nil
      end

      describe ':info' do
        it 'returns a generic event if info is nil' do
          event = @device.event(info: nil)
          expect(event).not_to be nil
          expect(event.info).to eq ''
        end

        it 'returns a generic event if info is set' do
          info = 'Hello world'
          event = @device.event(info: info)
          expect(event).not_to be nil
          expect(event.info).to eq info
        end
      end

      describe ':type' do
        it 'returns a generic event for default type, if type is nil' do
          event = @device.event(type: nil)
          expect(event).not_to be nil
          expect(event.type).to eq DeviceEvent::DEFAULT_TYPE_NAME
        end

        it 'returns a generic event if type is set' do
          event = @device.event(type: :firmware)
          expect(event).not_to be nil
          expect(event.type).to eq 'Firmware'
        end
      end

      describe ':level' do
        it 'returns a generic event for default level, if level is nil' do
          event = @device.event(level: nil)
          expect(event).not_to be nil
          expect(event.lvl).to eq DeviceEvent::DEFAULT_LEVEL_NAME
        end

        it 'returns a generic event if level is set' do
          event = @device.event(level: :critical)
          expect(event).not_to be nil
          expect(event.lvl).to eq 'Critical'
        end
      end

      describe ':data' do
        it 'returns a generic event for default data, if data is nil' do
          event = @device.event(data: nil)
          expect(event).not_to be nil
          expect(event.raw_data).to eq('status' => '')
        end

        it 'returns a generic event if data is set' do
          event = @device.event(data: { hello: :world })
          expect(event).not_to be nil
          expect(event.raw_data).to eq('hello' => 'world')
        end
      end

      describe ':uuid' do
        it 'returns a generic event with no uuid_id if uuid is nil' do
          event = @device.event(uuid: nil)
          expect(event).not_to be nil
          expect(event.uuid_id).to be nil
        end

        it 'returns a generic event with no uuid, if uuid is set but invalid' do
          event = @device.event(uuid: :hello_world)
          expect(event).not_to be nil
          expect(event.uuid).to be nil
        end

        it 'returns a generic event with uuid, if uuid is set and valid' do
          create_ucpe_event_uuids
          event = @device.event(uuid: :ucpe_ip_changed)
          expect(event).not_to be nil
          expect(event.uuid).not_to be nil
        end
      end
    end
  end
end
