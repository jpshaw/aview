# frozen_string_literal: true

require 'rails_helper'

describe RoleAbility do
  describe 'Validations' do
    it 'returns error if combination of role and ability already exists' do
      ra      = create(:role_ability)
      ra_dup  = ra.dup
      expect(ra_dup).not_to be_valid
      expect(ra_dup.errors.full_messages).to include('Ability already exists for the role')
    end
  end
end
