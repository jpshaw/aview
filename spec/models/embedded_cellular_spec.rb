# frozen_string_literal: true

require 'rails_helper'

describe EmbeddedCellular do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', DeviceConfiguration.new
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:embedded_cellular_device) }
  end
  it_behaves_like 'a uclinux device' do
    let(:platform) { :uc_linux }
    let(:test_serial) { '6300010030051578' }
    let(:device) { create(:embedded_cellular_device) }
  end
end
