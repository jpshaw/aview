# frozen_string_literal: true

require 'rails_helper'

describe DeviceAssociation, models: true do
  subject { DeviceAssociation }

  describe '#set_missing_device_attributes' do
    let(:existing_device) { create(:test_device) }
    let(:association)     { subject.new }

    context 'when source_id is blank' do
      before { association.source_id = nil }

      context 'when source_mac matches an existing device mac' do
        before { association.source_mac = existing_device.mac }

        it 'sets source_id to the existing device id' do
          expect do
            association.save
          end.to change(association, :source_id).to existing_device.id
        end
      end

      context 'when source_mac does not match an existing device mac' do
        before { association.source_mac = generate(:mac_address) }

        it 'does not set source_id' do
          expect do
            association.save
          end.not_to change(association, :source_id)
        end
      end
    end

    context 'when source_mac is blank' do
      before { association.source_mac = nil }

      context 'when source_id matches an existing device id' do
        before { association.source_id = existing_device.id }

        it 'sets source_mac to the existing device mac' do
          expect do
            association.save
          end.to change(association, :source_mac).to existing_device.mac
        end
      end

      context 'when the source_id does not match an existing device id' do
        before { association.source_id = (existing_device.id + 1) }

        it 'does not set source_mac' do
          expect do
            association.save
          end.not_to change(association, :source_mac)
        end
      end
    end
  end

  describe '#is_inversed_for?' do
    let(:source_device) { create(:test_device) }
    let(:target_device) { create(:test_device) }

    let(:association) do
      subject.create(
        source_id: source_device.id,
        source_mac: source_device.mac,
        target_id: target_device.id,
        target_mac: target_device.mac
      )
    end

    context 'when given nil' do
      it 'raises an argument exception' do
        expect do
          association.is_inversed_for?(nil)
        end.to raise_error(ArgumentError)
      end
    end

    context 'when given a device that matches the target' do
      it 'returns true' do
        expect(association.is_inversed_for?(target_device)).to be true
      end
    end

    context 'when given a device that does not match the target' do
      it 'returns false' do
        expect(association.is_inversed_for?(source_device)).to be false
      end
    end

    context 'when given an id that matches the target_id' do
      it 'returns true' do
        expect(association.is_inversed_for?(target_device.id)).to be true
      end
    end

    context 'when given an id that does not match the target_id' do
      it 'returns false' do
        expect(association.is_inversed_for?(source_device.id)).to be false
      end
    end

    context 'when given a mac that matches the target_mac' do
      it 'returns true' do
        expect(association.is_inversed_for?(target_device.mac)).to be true
      end
    end

    context 'when given a mac that does not match the target_mac' do
      it 'returns false' do
        expect(association.is_inversed_for?(source_device.mac)).to be false
      end
    end
  end

  describe '.by_device_id' do
    let(:device_1) { create(:test_device) }
    let(:device_2) { create(:test_device) }
    let(:device_3) { create(:test_device) }

    let(:outbound) { subject.create(source_id: device_1.id, target_id: device_2.id) }
    let(:inbound)  { subject.create(source_id: device_3.id, target_id: device_1.id) }
    let(:outside)  { subject.create(source_id: device_2.id, target_id: device_3.id) }

    it 'returns associations for the device id' do
      expect(DeviceAssociation.by_device_id(device_1.id)).to include(outbound, inbound)
    end

    it 'excludes associations not related to the device id' do
      expect(DeviceAssociation.by_device_id(device_1.id)).not_to include(outside)
    end
  end

  describe '.by_device_mac' do
    let(:device_1) { create(:test_device) }
    let(:device_2) { create(:test_device) }
    let(:device_3) { create(:test_device) }

    let(:outbound) { subject.create(source_mac: device_1.mac, target_mac: device_2.mac) }
    let(:inbound)  { subject.create(source_mac: device_3.mac, target_mac: device_1.mac) }
    let(:outside)  { subject.create(source_mac: device_2.mac, target_mac: device_3.mac) }

    it 'returns associations for the device mac' do
      expect(DeviceAssociation.by_device_mac(device_1.mac)).to include(outbound, inbound)
    end

    it 'excludes associations not related to the device mac' do
      expect(DeviceAssociation.by_device_mac(device_1.mac)).not_to include(outside)
    end
  end

  describe '.by_source_id_and_target_mac' do
    let(:source_device) { create(:test_device) }
    let(:target_device) { create(:test_device) }
    let(:other_device)  { create(:test_device) }

    context 'when the association exists' do
      let(:association) do
        subject.create(
          source_id: source_device.id, target_mac: target_device.mac
        )
      end

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_mac(source_device.id, target_device.mac)
        ).to include(association)
      end
    end

    context 'when an inverse association exists' do
      let(:association) do
        subject.create(
          source_mac: target_device.mac, target_id: source_device.id
        )
      end

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_mac(source_device.id, target_device.mac)
        ).to include(association)
      end
    end

    context 'when the association matches source_id but not target_mac' do
      let(:association) do
        subject.create(
          source_id: source_device.id, target_mac: target_device.mac
        )
      end

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_mac(source_device.id, other_device.mac)
        ).not_to include(association)
      end
    end

    context 'when the association matches target_mac but not source_id' do
      let(:association) do
        subject.create(
          source_id: source_device.id, target_mac: target_device.mac
        )
      end

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_mac(other_device.id, target_device.mac)
        ).not_to include(association)
      end
    end
  end

  describe '.by_source_id_and_target_id' do
    let(:source_id) { 1 }
    let(:target_id) { 2 }

    context 'when the association is normal' do
      let(:association) { create(:device_association, source_id: source_id, target_id: target_id) }

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_id(source_id, target_id)
        ).to include(association)
      end
    end

    context 'when the association is inversed' do
      let(:association) { create(:device_association, source_id: target_id, target_id: source_id) }

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_id(source_id, target_id)
        ).to include(association)
      end
    end

    context 'when the association matches source_id but not target_id' do
      let(:association) { create(:device_association, source_id: source_id, target_id: 3) }

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_id(source_id, target_id)
        ).not_to include(association)
      end
    end

    context 'when the association matches target_id but not source_id' do
      let(:association) { create(:device_association, source_id: 4, target_id: target_id) }

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_id_and_target_id(source_id, target_id)
        ).not_to include(association)
      end
    end
  end

  describe '.by_source_mac_and_target_mac' do
    let(:source_mac) { generate(:mac_address) }
    let(:target_mac) { generate(:mac_address) }

    context 'when the association is normal' do
      let(:association) { create(:device_association, source_mac: source_mac, target_mac: target_mac) }

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_mac_and_target_mac(source_mac, target_mac)
        ).to include(association)
      end
    end

    context 'when the association is inversed' do
      let(:association) { create(:device_association, source_mac: target_mac, target_mac: source_mac) }

      it 'returns the association' do
        expect(
          DeviceAssociation.by_source_mac_and_target_mac(source_mac, target_mac)
        ).to include(association)
      end
    end

    context 'when the association matches source_mac but not target_mac' do
      let(:association) { create(:device_association, source_mac: source_mac, target_mac: generate(:mac_address)) }

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_mac_and_target_mac(source_mac, target_mac)
        ).not_to include(association)
      end
    end

    context 'when the association matches target_mac but not source_mac' do
      let(:association) { create(:device_association, source_mac: generate(:mac_address), target_mac: target_mac) }

      it 'does not return the association' do
        expect(
          DeviceAssociation.by_source_mac_and_target_mac(source_mac, target_mac)
        ).not_to include(association)
      end
    end
  end

  describe '.find_by_source_and_target' do
    let(:association) { create(:device_association) }

    context 'when given source_id and target_id' do
      it 'finds the association normally' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_id: association.source_id,
            target_id: association.target_id
          )
        ).to eq association
      end

      it 'finds the association inversely' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_id: association.target_id,
            target_id: association.source_id
          )
        ).to eq association
      end
    end

    context 'when given source_id and target_mac' do
      it 'finds the association' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_id: association.source_id,
            target_mac: association.target_mac
          )
        ).to eq association
      end
    end

    context 'when given source_mac and target_id' do
      it 'finds the association' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_mac: association.source_mac,
            target_id: association.target_id
          )
        ).to eq association
      end
    end

    context 'when given source_mac and target_mac' do
      it 'finds the association normally' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_mac: association.source_mac,
            target_mac: association.target_mac
          )
        ).to eq association
      end

      it 'finds the association inversely' do
        expect(
          DeviceAssociation.find_by_source_and_target(
            source_mac: association.target_mac,
            target_mac: association.source_mac
          )
        ).to eq association
      end
    end
  end
end
