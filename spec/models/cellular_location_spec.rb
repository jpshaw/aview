# frozen_string_literal: true

require 'rails_helper'

describe CellularLocation, type: :model do
  describe 'scopes' do
    describe '.find_by_modem' do
      before do
        device = create(:test_device)
        @modem = create(:device_modem, device: device)
        @cellular_location_1 = create(:cellular_location, mcc: @modem.mcc, mnc: @modem.mnc, lac: @modem.lac, cid: @modem.cid)
        @cellular_location_2 = create(:cellular_location)
      end

      context 'when given a modem' do
        let(:result) { CellularLocation.find_by_modem @modem }

        it 'includes the cellular location that matches the given modem' do
          expect(result).to include @cellular_location_1
          expect(result).not_to include @cellular_location_2
        end
      end
    end
  end
end
