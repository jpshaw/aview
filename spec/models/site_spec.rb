# frozen_string_literal: true

require 'rails_helper'

describe Site do
  before do
    allow_any_instance_of(Organization).to receive(:setup_associations).and_return(nil)
  end

  it_behaves_like 'a searchable model'

  describe '#reset_devices_sites' do
    it 'sets all of the devices sites to the organizations default site' do
      site         = create(:site)
      default_site = site.organization.default_site
      devices      = create_list(:test_device, 3,
                                            organization: site.organization,
                                            site:         site)

      site.destroy

      # Reload devices
      devices = Device.where(id: devices.map(&:id))

      devices.each do |device|
        expect(device.site).to eq default_site
      end
    end
  end

  describe 'scopes' do
    describe '.with_names' do
      before do
        @site_1 = create(:site, name: 'Site 1')
        @site_2 = create(:site, name: 'Site 2')
        @site_3 = create(:site, name: 'Site 3')
      end

      context 'when given a name' do
        let(:result) { described_class.with_names @site_1.name }

        it 'includes sites with the given name' do
          expect(result).to include @site_1
          expect(result).not_to include @site_2
          expect(result).not_to include @site_3
        end
      end

      context 'when given multiple names' do
        let(:result) { described_class.with_names [@site_1.name, @site_2.name] }

        it 'includes sites with one of the given names' do
          expect(result).to include @site_1
          expect(result).to include @site_2
          expect(result).not_to include @site_3
        end
      end
    end

    describe '.with_organization' do
      before do
        @site_1 = create(:site)
        @site_2 = create(:site)
        @site_3 = create(:site_with_sub_organization)
      end

      context 'when given an organization without hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @site_1.organization.id, include_hierarchy: 'false') }

        it 'includes sites without their suborganizations' do
          expect(result).to include @site_1
          expect(result).not_to include @site_2
          expect(result).not_to include @site_3
        end
      end

      context 'when given an organization with hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @site_1.organization.id, include_hierarchy: 'true') }

        it 'includes sites with their suborganizations' do
          expect(result).to include @site_1
          expect(result).not_to include @site_2
          expect(result).to include @site_3
        end
      end
    end

    describe '.sort_by_name' do
      before do
        @site_1 = create(:site, name: 'Site 1')
        @site_2 = create(:site, name: 'Site 2')
        @site_3 = create(:site, name: 'Site 3')
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_name 'asc' }

        it 'sorts lower site names first, and higher site names last' do
          expect(result.first).to eq @site_1
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_name 'desc' }

        it 'sorts higher site names first, and lower site names last' do
          expect(result.first).to eq @site_3
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_1
        end
      end
    end

    describe '.sort_by_organizations_name' do
      before do
        @site_1 = create(:site)
        @site_2 = create(:site)
        @site_3 = create(:site)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_organizations_name 'asc' }

        it 'sorts lower organization names first, and higher organization names last' do
          expect(result.first).to eq @site_1
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_organizations_name 'desc' }

        it 'sorts higher organization names first, and lower organization names last' do
          expect(result.first).to eq @site_3
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_1
        end
      end
    end

    describe '.sort_by_created_at' do
      before do
        @site_1 = create(:site, created_at: 3.days.ago)
        @site_2 = create(:site, created_at: 2.days.ago)
        @site_3 = create(:site, created_at: 1.day.ago)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_created_at 'asc' }

        it 'sorts later created sites first, and earlier created sites last' do
          expect(result.first).to eq @site_1
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_created_at 'desc' }

        it 'sorts earlier created sites first, and later created sites last' do
          expect(result.first).to eq @site_3
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_1
        end
      end
    end

    describe '.sort_by_device_count' do
      before do
        @site_1 = create(:site)
        @site_2 = create(:site)
        @site_3 = create(:site)
        create(:test_device, organization: @site_1.organization, site: @site_1)
        create(:test_device, organization: @site_2.organization, site: @site_2)
        create(:test_device, organization: @site_1.organization, site: @site_1)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.with_device_counts.sort_by_device_count 'asc' }

        it 'sorts sites with the fewest devices first, and most devices last' do
          expect(result.first).to eq @site_3
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_1
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.with_device_counts.sort_by_device_count 'desc' }

        it 'sorts sites with the most devices first, and least devices last' do
          expect(result.first).to eq @site_1
          expect(result.second).to eq @site_2
          expect(result.last).to eq @site_3
        end
      end
    end
  end

  describe 'searchable' do
    before do
      @site_1 = create(:site, name: 'Site 1')
      @site_2 = create(:site, name: 'Site 2')
      @site_3 = create(:site_with_sub_organization, name: 'Site 3')
    end

    describe '.search' do
      context 'when given multiple names and sort by name ascending' do
        let(:result) { described_class.search(with_names: [@site_1.name, @site_2.name], sort_by_name: 'asc') }

        it 'returns sites with one of the given names in the correct order' do
          expect(result.first).to eq @site_1
          expect(result.second).to eq @site_2
          expect(result).not_to include @site_3
        end
      end

      context 'when given an organization and sort by name' do
        let(:result) do
          described_class.search(with_organization: { organization_id: @site_1.organization.id, include_hierarchy: 'true' },
                                 sort_by_name: 'desc')
        end

        it 'returns sites with the given organization in the correct order' do
          expect(result.first).to eq @site_3
          expect(result.last).to eq @site_1
          expect(result).not_to include @site_2
        end
      end

      context 'when searching all sites' do
        let(:result) { described_class.search(all: nil) }

        it 'returns all sites' do
          expect(result).to include @site_1
          expect(result).to include @site_2
          expect(result).to include @site_3
        end
      end
    end

    describe '.search!' do
      context 'when given multiple names and sort by name descending' do
        let(:result) { described_class.search(with_names: [@site_1.name, @site_2.name], sort_by_name: 'desc') }

        it 'returns sites with one of the given names in the correct order' do
          expect(result.first).to eq @site_2
          expect(result.second).to eq @site_1
          expect(result).not_to include @site_3
        end
      end
    end
  end
end
