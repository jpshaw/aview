# frozen_string_literal: true

require 'rails_helper'

describe Netreach do
  let(:device) { create(:netreach_device) }

  it_behaves_like 'a base device class'
  it_behaves_like 'a monitorable device'
  it_behaves_like 'an associatable device'
  it_behaves_like 'an alpha device' do
    let(:platform) { :alpha }
    let(:device)   { create(:netreach_device) }
  end

  describe '#default_status_freq' do
    it 'returns the value of constant DEFAULT_STATUS_FREQ' do
      expect(subject.__send__(:default_status_freq)).to eq(subject.class::DEFAULT_STATUS_FREQ)
    end
  end

  describe '#grace_period' do
    it 'returns the value of constant DEFAULT_GRACE_PERIOD_DURATION' do
      expect(subject.__send__(:default_grace_period_duration)).to eq(subject.class::DEFAULT_GRACE_PERIOD_DURATION)
    end
  end
end
