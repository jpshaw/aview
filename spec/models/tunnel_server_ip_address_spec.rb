# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TunnelServerIpAddress, type: :model do
  describe '.formatted_string' do
    let(:valid_ipv4_address)     { '10.20.030.40' }
    let(:invalid_ipv4_address)   { '999.999.999.999' }
    let(:formatted_ipv4_address) { '10.20.30.40' }

    let(:valid_ipv6_address)     { '2001:cdba:0000:0000:0000:0000:3257:9652' }
    let(:invalid_ipv6_address)   { 'h:i:j:k:l:m:n' }
    let(:formatted_ipv6_address) { '2001:cdba::3257:9652' }

    context 'when given nil' do
      it 'raises an exception' do
        expect { subject.class.formatted_string(nil) }.to raise_exception(ArgumentError)
      end
    end

    context 'when given a valid IPv4 address' do
      it 'returns the formatted address' do
        expect(subject.class.formatted_string(valid_ipv4_address)).to eq formatted_ipv4_address
      end
    end

    context 'when given an invalid ip IPv4 address' do
      it 'raises an exception' do
        expect { subject.class.formatted_string(invalid_ipv4_address) }.to raise_exception(ArgumentError)
      end
    end

    context 'when given a valid IPv6 address' do
      it 'returns the formatted address' do
        expect(subject.class.formatted_string(valid_ipv6_address)).to eq formatted_ipv6_address
      end
    end

    context 'when given an invalid ip IPv6 address' do
      it 'raises an exception' do
        expect { subject.class.formatted_string(invalid_ipv6_address) }.to raise_exception(ArgumentError)
      end
    end
  end
end
