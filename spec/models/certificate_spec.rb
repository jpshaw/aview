# frozen_string_literal: true

require 'rails_helper'

describe Certificate do
  describe '.md5' do
    it 'digests an incoming string' do
      expect(Digest::MD5).to receive(:hexdigest)
      described_class.md5('a_string_to_digest')
    end

    it 'returns a digested string' do
      md5 = Digest::MD5.hexdigest('a_string_to_digest')
      expect(described_class.md5('a_string_to_digest')).to eq(md5)
    end
  end

  describe '#generate_md5' do
    context 'when cert is present' do
      let(:cert)  { 'a_cert_value' }
      let(:md5)   { described_class.md5(cert) }

      before do
        subject.cert = cert
      end

      it 'calculates a new md5' do
        expect(subject.md5).to be nil
        subject.__send__(:generate_md5)
        expect(subject.md5).to eq(md5)
      end
    end

    context 'when cert is not present' do
      before do
        subject.cert  = nil
        subject.md5   = 'a_md5_value'
      end

      it 'makes md5 nil' do
        expect(subject.md5).not_to be nil
        subject.__send__(:generate_md5)
        expect(subject.md5).to be nil
      end
    end
  end
end
