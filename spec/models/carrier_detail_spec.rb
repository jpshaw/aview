# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarrierDetail, type: :model do
  context '#regexp' do
    it 'returns a Regexp when present' do
      carrier_detail = create(:carrier_detail, regexp: 'AT&T')
      expect(carrier_detail.regexp).to be_a Regexp
    end

    it 'returns nil when nil' do
      carrier_detail = create(:carrier_detail, regexp: nil)
      expect(carrier_detail.regexp).to be_nil
    end
  end
end
