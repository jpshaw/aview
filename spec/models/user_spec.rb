# frozen_string_literal: true

require 'rails_helper'

describe User do
  let(:user) { build(:user) }

  it_behaves_like 'a manager of resources' do
    # Prepare objects needed by the shared examples.
    let(:parent_organization) { create(:random_regular_organization) }
    let(:organization)        { create(:random_regular_organization) }
    let(:manager)             { create(:user, organization: organization) }
  end

  it_behaves_like 'an api authenticatable resource' do
    let(:resource) { create(:user) }
  end

  it_behaves_like 'a searchable model'

  describe 'default timezone' do
    before { @timezone = create(:timezone) }

    it 'sets the timezone on a new user object' do
      user = described_class.new
      expect(user.timezone).to eq @timezone
    end

    it 'sets the timezone on a newly created object' do
      user = described_class.create
      expect(user.timezone).to eq @timezone
    end

    it 'does not change the timezone of a found object' do
      timezone = create(:not_default_timezone)
      user = create(:user, timezone: timezone)
      expect(user.timezone).not_to eq @timezone
    end
  end

  describe 'pagination limits' do
    it 'returns default limit for key if set to nil' do
      default_limit = user.default_pagination_limit
      user.pagination_limits = nil
      expect(user.pagination_limit(:devices)).to eq default_limit
    end

    it 'returns default limit if set to anything other than a hash' do
      default_limit = user.default_pagination_limit
      user.pagination_limits = [1, 2, 3]
      expect(user.pagination_limit(:devices)).to eq default_limit
    end

    it 'returns value if key is found' do
      user.set_pagination_limit(:devices, 10)
      expect(user.pagination_limit(:devices)).to eq 10
    end

    it 'returns default limit if value passed is not a number' do
      default_limit = user.default_pagination_limit
      user.set_pagination_limit(:devices, 'abcdefg123')
      expect(user.pagination_limit(:devices)).to eq default_limit
    end

    it 'returns default limit if value is set to extremely large number' do
      default_limit = user.default_pagination_limit
      user.set_pagination_limit(:devices, 100_000_000)
      expect(user.pagination_limit(:devices)).to eq default_limit
    end

    it 'returns default limit if value is set to a number less than one' do
      default_limit = user.default_pagination_limit
      user.set_pagination_limit(:devices, -1)
      expect(user.pagination_limit(:devices)).to eq default_limit
    end

    it 'allows pagination to be set if pagination_limits is nil' do
      user.pagination_limits = nil
      user.set_pagination_limit(:devices, 25)
      expect(user.pagination_limit(:devices)).to eq 25
    end

    it 'allows pagination to be set if pagination_limits is not a hash' do
      user.pagination_limits = 'hello world'
      user.set_pagination_limit(:devices, 25)
      expect(user.pagination_limit(:devices)).to eq 25
    end
  end

  describe '#filter_organization' do
    before do
      @user = create(:user)
    end

    context 'when the user has a filter organization id set' do
      before do
        @filter_organization = create(:random_regular_organization)
        @user.filter_organization_id = @filter_organization.id
      end

      context 'when the filter organization is within the user scope' do
        before do
          @user.organization.add_child(@filter_organization)
          Permission.create(manager: @user, manageable: @filter_organization, role: Role.new)
        end

        it 'returns the organization' do
          expect(@user.filter_organization).to eq @filter_organization
        end
      end
    end

    context 'when the user does not have a filter organization id set' do
      before do
        @user.filter_organization_id = nil
      end

      it 'returns nil' do
        expect(@user.filter_organization).to be nil
      end
    end
  end

  describe 'scopes' do
    describe '.last_activity_since' do
      context 'when given a date two weeks ago' do
        before do
          @user_1 = create(:user, last_activity_at: 1.month.ago)
          @user_2 = create(:user, last_activity_at: 1.week.ago)
          @user_3 = create(:user, last_activity_at: 1.day.ago)
        end

        let(:result) { described_class.last_activity_since 2.weeks.ago }

        it 'includes users with last activity two weeks or sooner' do
          expect(result).not_to include @user_1
          expect(result).to include @user_2
          expect(result).to include @user_3
        end
      end

      context 'when given nil' do
        before do
          @user_1 = create(:user)
          @user_2 = create(:user)
          @user_3 = create(:user, last_activity_at: 1.day.ago)
        end

        let(:result) { described_class.last_activity_since nil }

        it 'includes users with no last activity' do
          expect(result).to include @user_1
          expect(result).to include @user_2
          expect(result).not_to include @user_3
        end
      end
    end

    describe '.with_organization' do
      before do
        @user_1 = create(:user)
        @user_2 = create(:user)
        @user_3 = create(:non_root_user)
      end

      context 'when given an organization id without hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @user_1.organization.id, include_hierarchy: 'false') }

        it 'includes users with the given organization only' do
          expect(result).to include @user_1
          expect(result).not_to include @user_2
          expect(result).not_to include @user_3
        end
      end

      context 'when given an organization id with hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @user_1.organization.id, include_hierarchy: 'true') }

        it 'includes users within the given organization and its hierarchy' do
          expect(result).to include @user_1
          expect(result).not_to include @user_2
          expect(result).to include @user_3
        end
      end
    end

    describe '.sort_by_email' do
      before do
        @user_1 = create(:user)
        @user_2 = create(:user)
        @user_3 = create(:user)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_email 'asc' }

        it 'sorts users with lower emails first and higher emails last' do
          expect(result.first).to eq @user_1
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_email 'desc' }

        it 'sorts users with higher emails first and lower emails last' do
          expect(result.first).to eq @user_3
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_1
        end
      end
    end

    describe '.sort_by_organizations_name' do
      before do
        @user_1 = create(:user)
        @user_2 = create(:user)
        @user_3 = create(:user)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_organizations_name 'asc' }

        it 'sorts users with lower organization names first and higher organization names last' do
          expect(result.first).to eq @user_1
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_organizations_name 'desc' }

        it 'sorts users with higher organization names first and lower organization names last' do
          expect(result.first).to eq @user_3
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_1
        end
      end
    end

    describe '.sort_by_timezone_name' do
      before do
        @user_1 = create(:user, :with_alaska_timezone)
        @user_2 = create(:user)
        @user_3 = create(:user, :with_non_default_timezone)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_timezone_name 'asc' }

        it 'sorts users with lower timezone names first and higher timezone names last' do
          expect(result.first).to eq @user_2
          expect(result.second).to eq @user_1
          expect(result.third).to eq @user_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_timezone_name 'desc' }

        it 'sorts users with higher timezone names first and lower timezone names last' do
          expect(result.first).to eq @user_3
          expect(result.second).to eq @user_1
          expect(result.third).to eq @user_2
        end
      end
    end

    describe '.sort_by_last_sign_in_at' do
      before do
        @user_1 = create(:user, last_sign_in_at: 1.month.ago)
        @user_2 = create(:user, last_sign_in_at: 1.week.ago)
        @user_3 = create(:user, last_sign_in_at: 1.day.ago)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_last_sign_in_at 'asc' }

        it 'sorts users with the most recent login first and the least recent login last' do
          expect(result.first).to eq @user_1
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_3
        end
      end

      context 'sorts given descending' do
        let(:result) { described_class.sort_by_last_sign_in_at 'desc' }

        it 'returns users with the most recent login first and the least recent login last' do
          expect(result.first).to eq @user_3
          expect(result.second).to eq @user_2
          expect(result.third).to eq @user_1
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      before do
        @user_1 = create(:user, last_activity_at: 1.month.ago)
        @user_2 = create(:user, last_activity_at: 1.week.ago)
        @user_3 = create(:user, last_activity_at: 1.day.ago)
      end

      context 'when given last activity since and sort by email' do
        let(:result) { described_class.search(last_activity_since: 2.weeks.ago, sort_by_email: 'asc') }

        it 'returns users with activity since a given date sorted by email correctly' do
          expect(result).not_to include @user_1
          expect(result.first).to eq @user_2
          expect(result.last).to eq @user_3
        end
      end

      context 'when searching all users' do
        let(:result) { described_class.search(all: nil) }

        it 'returns all users' do
          expect(result).to include @user_1
          expect(result).to include @user_2
          expect(result).to include @user_3
        end
      end
    end

    describe '.search!' do
      before do
        @user_1 = create(:user, last_sign_in_at: 1.month.ago)
        @user_2 = create(:user, last_sign_in_at: 1.week.ago)
        @user_3 = create(:non_root_user, last_sign_in_at: 1.day.ago)
      end

      context 'when given with organization and sort by last sign in at' do
        let(:result) do
          described_class.search!(with_organization: { organization_id: @user_1.organization.id,
                                                       include_hierarchy: 'true' },
                                  sort_by_last_sign_in_at: 'desc')
        end

        it 'returns users with the given organization sorted by last sign in at correctly' do
          expect(result.first).to eq @user_3
          expect(result.last).to eq @user_1
          expect(result).not_to include @user_2
        end
      end
    end
  end
end
