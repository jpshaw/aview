# frozen_string_literal: true

require 'rails_helper'

describe GatewayTunnel do
  describe '#check_device_tunnels_count' do
    before do
      @device = create(:blank_gateway_device)
    end

    context 'when creating tunnels' do
      context "if there's 2 tunnels but the tunnels_count is 4" do
        it 'resets the tunnel count to the correct number' do
          @device.tunnels.create
          @device.tunnels.create
          expect(@device.tunnels(true).count).to eq 2
          @device.update tunnels_count: 4
          expect(@device.tunnels_count).to eq 4
          @device.tunnels.create
          @device.reload
          expect(@device.tunnels_count).to eq 3
        end
      end

      context "if there's 2 tunnels but the tunnels_count is -4" do
        it 'resets the tunnel count to the correct number' do
          @device.tunnels.create
          @device.tunnels.create
          expect(@device.tunnels(true).count).to eq 2
          @device.update tunnels_count: -4
          expect(@device.tunnels_count).to eq -4
          @device.tunnels.create
          @device.reload
          expect(@device.tunnels_count).to eq 3
        end
      end
    end

    context 'when destroying tunnels' do
      context "if there's 2 tunnels but the tunnel count is 6" do
        it 'resets the tunnel count to the correct number' do
          @device.tunnels.create
          @device.tunnels.create
          expect(@device.tunnels(true).count).to eq 2
          @device.update tunnels_count: 6
          expect(@device.tunnels_count).to eq 6
          @device.tunnels.first.destroy
          @device.reload
          expect(@device.tunnels_count).to eq 1
        end
      end

      context "if there's 2 tunnels but the tunnel count is -6" do
        it 'resets the tunnel count to the correct number' do
          @device.tunnels.create
          @device.tunnels.create
          expect(@device.tunnels(true).count).to eq 2
          @device.update tunnels_count: -6
          expect(@device.tunnels_count).to eq -6
          @device.tunnels.first.destroy
          @device.reload
          expect(@device.tunnels_count).to eq 1
        end
      end
    end
  end
end
