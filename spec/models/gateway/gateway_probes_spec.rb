# frozen_string_literal: true

require 'rails_helper'
require 'devices/gateway'

describe Gateway do
  # TODO: Add SNMP Request/Response tests here.
  let(:mac) { '00D0CF012345' }
  let(:device) { create(:blank_gateway_device_with_details, mac: mac) }

  let(:results) do
    {
      '1.3.6.1.2.1.1.5.0'          => mac,
      '1.3.6.1.4.1.74.1.30.1.1.0'  => '8100',    # hardware level
      '1.3.6.1.4.1.74.1.30.1.2.0'  => '5.4.126', # firmware
      '1.3.6.1.4.1.74.1.30.2.1.0'  => 'eth1',    # wan interface
      '1.3.6.1.4.1.74.1.30.1.7.0'  => 2,         # wan connection, dhcp
      '1.3.6.1.4.1.74.1.30.1.3.0'  => 'ANRTEST', # smx
      '1.3.6.1.4.1.74.1.30.1.21.0' => '6.1.999'
    }
  end

  let(:gateway_tunnel_limit) { Gateway::TUNNEL_LIMIT }

  describe '#snmp_objects' do
    it 'returns an array' do
      expect(device.snmp_objects).to be_an Array
    end

    it 'includes the base gateway snmp objects' do
      expect(device.snmp_objects).to include(*Platforms::Gateway::Snmp::Objects::BASE_OBJECTS)
    end

    context 'for determining the mac oid' do
      context 'when the device firmware is nil' do
        before { device.update(firmware: nil) }

        it 'returns the default mac oid' do
          expect(device.firmware).to eq nil
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::DEFAULT_MAC_OID)
        end
      end

      context 'when the device firmware is below 6.2' do
        before { device.update(firmware: '6.1.999') }

        it 'returns the system name oid' do
          expect(device.firmware).to eq '6.1.999'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::SYSTEM_NAME)
        end
      end

      context 'when the device firmware is 6.2' do
        before { device.update(firmware: '6.2.0') }

        it 'returns the hardware mac oid' do
          expect(device.firmware).to eq '6.2.0'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::HARDWARE_MAC)
        end
      end

      context 'when the device firmware is above 6.2' do
        before { device.update(firmware: '6.2.1') }

        it 'returns the hardware mac oid' do
          expect(device.firmware).to eq '6.2.1'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::HARDWARE_MAC)
        end
      end
    end

    context 'for checking backup mode' do
      context 'when the device firmware is nil' do
        before { device.update(firmware: nil) }

        it 'does not include the backup mode oid' do
          expect(device.firmware).to eq nil
          expect(device.snmp_objects).not_to include(Platforms::Gateway::Snmp::Objects::TIME_IN_BACKUP)
        end
      end

      context 'when the device firmware is below 6.1' do
        before { device.update(firmware: '6.0.999') }

        it 'does not include the backup mode oid' do
          expect(device.firmware).to eq '6.0.999'
          expect(device.snmp_objects).not_to include(Platforms::Gateway::Snmp::Objects::TIME_IN_BACKUP)
        end
      end

      context 'when the device firmware is at 6.1' do
        before { device.update(firmware: '6.1.0') }

        it 'includes the backup mode oid' do
          expect(device.firmware).to eq '6.1.0'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::TIME_IN_BACKUP)
        end
      end

      context 'when the device firmware is above 6.1' do
        before { device.update(firmware: '6.1.1') }

        it 'includes the backup mode oid' do
          expect(device.firmware).to eq '6.1.1'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::TIME_IN_BACKUP)
        end
      end
    end

    context 'for checking firmware rollback' do
      context 'when the device firmware is nil' do
        before { device.update(firmware: nil) }

        it 'does not include the rollback firmware oid' do
          expect(device.firmware).to eq nil
          expect(device.snmp_objects).not_to include(Platforms::Gateway::Snmp::Objects::FIRMWARE_ROLLBACK)
        end
      end

      context 'when the device firmware is below 6.2' do
        before { device.update(firmware: '6.1.999') }

        it 'does not include the rollback firmware  oid' do
          expect(device.firmware).to eq '6.1.999'
          expect(device.snmp_objects).not_to include(Platforms::Gateway::Snmp::Objects::FIRMWARE_ROLLBACK)
        end
      end

      context 'when the device firmware is at 6.2' do
        before { device.update(firmware: '6.2.0') }

        it 'includes the rollback firmware  oid' do
          expect(device.firmware).to eq '6.2.0'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::FIRMWARE_ROLLBACK)
        end
      end

      context 'when the device firmware is above 6.2' do
        before { device.update(firmware: '6.2.1') }

        it 'includes the rollback firmware  oid' do
          expect(device.firmware).to eq '6.2.1'
          expect(device.snmp_objects).to include(Platforms::Gateway::Snmp::Objects::FIRMWARE_ROLLBACK)
        end
      end
    end

    it 'includes tunnel objects' do
      Platforms::Gateway::Snmp::Objects::TUNNEL_OBJECTS.each do |tunnel_oid|
        expect(
          device.snmp_objects.any? { |snmp_oid| snmp_oid =~ /#{tunnel_oid}/ }
        ).to be true
      end
    end

    context 'for cellular modem data collection' do
      context 'when it is enabled' do
        before do
          allow(device).to receive(:cell_modem_collection_enabled?).and_return(true)
        end

        it 'includes cellular modem objects' do
          Platforms::Gateway::Snmp::Objects::CELL_MODEM_OBJECTS.each do |cell_oid|
            expect(
              device.snmp_objects.any? { |snmp_oid| snmp_oid =~ /#{cell_oid}/ }
            ).to be true
          end
        end
      end

      context 'when it is disabled' do
        before do
          allow(device).to receive(:cell_modem_collection_enabled?).and_return(false)
        end

        it 'does not include cellular modem objects' do
          Platforms::Gateway::Snmp::Objects::CELL_MODEM_OBJECTS.each do |cell_oid|
            expect(
              device.snmp_objects.any? { |snmp_oid| snmp_oid =~ /#{cell_oid}/ }
            ).to be false
          end
        end
      end
    end
  end

  describe '#process_snmp_results' do
    context 'when given a valid mac address' do
      before do
        expect(device.mac).to eq mac
      end

      it 'updates the device' do
        expect(device).to receive(:heartbeat!)
        device.process_snmp_results(results)
      end
    end

    context 'when given an invalid mac address' do
      before do
        @results = results.merge('1.3.6.1.2.1.1.5.0' => '00D0CF00TEST')
      end

      it 'does not update the device' do
        expect(device).not_to receive(:heartbeat!)
        device.process_snmp_results(@results)
      end

      it 'sets the management host to nil' do
        device.mgmt_host = '1.2.3.4'
        device.process_snmp_results(@results)
        expect(device.mgmt_host).to be nil
      end
    end

    it "sets the device's hardware version" do
      expect(device.hw_version).to eq '8200' # the default hw_version
      device.process_snmp_results(results)
      expect(device.hw_version).to eq '8100'
    end

    context "when the device's firmware is nil" do
      it 'sets the firmware and generates an event' do
        expect(device.firmware).to be nil
        device.process_snmp_results(results)
        expect(device.firmware).to eq '5.4.126'
        event = device.events.where("information like '%5.4.126%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Firmware changed from (none) to 5.4.126'
      end
    end

    context "when the device's firmware is outdated" do
      it 'updates the firmware and generates an event' do
        device.update(firmware: '5.3.2')
        device.process_snmp_results(results)
        expect(device.firmware).to eq '5.4.126'
        event = device.events.where("information like '%5.4.126%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Firmware changed from 5.3.2 to 5.4.126'
      end
    end

    context "when the device's firmware is unchanged" do
      it 'does not generate an event' do
        device.update(firmware: '5.4.126')
        device.process_snmp_results(results)
        expect(device.firmware).to eq '5.4.126'
        event = device.events.where("information like '%5.4.126%'").first
        expect(event).to be nil
      end
    end

    context "when the device's firmware rollback is nil" do
      it 'sets the firmware and generates an event' do
        expect(device.firmware_rollback).to be nil
        device.process_snmp_results(results)
        expect(device.firmware_rollback).to eq '6.1.999'
        event = device.events.where("information like '%6.1.999%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Firmware rollback version changed to 6.1.999'
      end
    end

    context "when the device's firmware rollback is different" do
      it 'updates the firmware and generates an event' do
        device.firmware_rollback = '6.0.0'
        device.process_snmp_results(results)
        expect(device.firmware_rollback).to eq '6.1.999'
        event = device.events.where("information like '%6.1.999%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Firmware rollback version changed from 6.0.0 to 6.1.999'
      end
    end

    context "when the device's firmware rollback is unchanged" do
      it 'does not generate an event' do
        device.firmware_rollback = '6.1.999'
        device.process_snmp_results(results)
        expect(device.firmware_rollback).to eq '6.1.999'
        event = device.events.where("information like '%6.1.999%'").first
        expect(event).to be nil
      end
    end

    context "when the devices's wan interface is nil" do
      it 'sets the active wan interface and generates an event' do
        device.details.active_wan_iface = nil
        device.process_snmp_results(results)
        expect(device.details.active_wan_iface).to eq 'eth1'
        event = device.events.where("information like '%eth1%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Primary connection interface changed from (none) to eth1'
      end
    end

    context "when the devices's wan interface is set to eth0" do
      it 'updates the active wan interface and generates an event' do
        device.details.active_wan_iface = 'eth0'
        device.process_snmp_results(results)
        expect(device.details.active_wan_iface).to eq 'eth1'
        event = device.events.where("information like '%eth1%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Primary connection interface changed from eth0 to eth1'
      end
    end

    context "when the devices's wan interface is unchanged" do
      it "doesn't generate an event" do
        device.details.active_wan_iface = 'eth1'
        device.process_snmp_results(results)
        expect(device.details.active_wan_iface).to eq 'eth1'
        event = device.events.where("information like '%eth1%'").first
        expect(event).to be nil
      end
    end

    context "when the devices's primary connection type is nil" do
      it 'sets the type and generates an event' do
        expect(device.details.primary_connection_type_name).to eq 'Unknown'
        device.process_snmp_results(results)
        expect(device.details.primary_connection_type_name).to eq 'DHCP'
        event = device.events.where("information like '%DHCP%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Primary connection type has changed to DHCP'
      end
    end

    context "when the devices's primary connection type is set to static" do
      it 'changes the type and generates an event' do
        device.details.active_conn_type = 1
        device.process_snmp_results(results)
        expect(device.details.primary_connection_type_name).to eq 'DHCP'
        event = device.events.where("information like '%DHCP%'").first
        expect(event).not_to be nil
        expect(event.info).to eq 'Primary connection type has changed from Static to DHCP'
      end
    end

    context "when the devices's primary connection type is unchanged" do
      it "doesn't generate an event" do
        device.details.active_conn_type = 2
        device.process_snmp_results(results)
        expect(device.details.primary_connection_type_name).to eq 'DHCP'
        event = device.events.where("information like '%DHCP%'").first
        expect(event).to be nil
      end
    end

    context 'when no tunnel data is given' do
      it 'does not create any tunnels' do
        expect(device.tunnels.count.zero?).to be true
        device.process_snmp_results(results)
        expect(device.tunnels.count.zero?).to be true
      end

      it 'removes any tunnels a device may have' do
        device.tunnels.create
        device.tunnels.create
        device.tunnels.create
        device.tunnels.create
        expect(device.tunnels.count).to eq 4
        device.process_snmp_results(results)
        expect(device.tunnels(true).count).to eq 0
      end
    end

    context 'when tunnel data is given' do
      let(:single_tunnel_results) { snmp_results_for_tunnel_count(1, mac) }

      it 'sets the ipsec interface for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.ipsec_iface).to eq 'IPSEC0'
      end

      it 'sets the endpoint address for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.endpoint).to eq '135.120.40.51'
      end

      it 'sets the ipv6 endpoint address for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.endpoint_ipv6).to eq 'fe80::1293:e9ff:fe09:cd48'
      end

      it 'sets the endpoint type for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.endpoint_name).to eq 'Linux SIG'
      end

      it 'sets the auth type for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.auth_type_name).to eq 'Service Manager'
      end

      it 'sets the agns managed boolean for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.managed_by_agns).to eq 'Yes'
      end

      it 'sets the initiator for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.initiator_name).to eq 'Auto Logon'
      end

      it 'sets the mode for the tunnel' do
        device.process_snmp_results(single_tunnel_results)
        expect(device.tunnels.first.mode_name).to eq 'Management'
      end
    end

    context 'when blank endpoints are given' do
      before do
        @result = snmp_results_for_tunnel_count(1, mac)
        @result['1.3.6.1.4.1.74.1.30.6.1.1.6.1']  = '0.0.0.0'
        @result['1.3.6.1.4.1.74.1.30.6.1.1.19.1'] = '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00'
      end

      it 'does not create any tunnels' do
        expect do
          device.process_snmp_results(@result)
        end.not_to change(GatewayTunnel, :count)
      end
    end

    context 'when data for the maximum number of device tunnels is given' do
      it 'creates the maximum number of tunnel records for the device' do
        expect(device.tunnels.count.zero?).to be true
        results = snmp_results_for_tunnel_count(gateway_tunnel_limit, mac)
        device.process_snmp_results(results)
        expect(device.tunnels.count).to eq gateway_tunnel_limit
      end
    end

    context 'when data for more than the maximum number of device tunnels is given' do
      it 'only creates the maxmimum number tunnel records for the device' do
        expect(device.tunnels.count.zero?).to be true
        results = snmp_results_for_tunnel_count(gateway_tunnel_limit + 4, mac)
        device.process_snmp_results(results)
        expect(device.tunnels.count).to eq gateway_tunnel_limit
      end
    end

    context 'when data for less than the maximum number of device tunnels is given' do
      it 'ensures the given tunnels are set and removes any extra tunnels' do
        device.tunnels.create(ipsec_iface: 'IPSEC3')
        device.tunnels.create(ipsec_iface: 'IPSEC4')
        expect(device.tunnels.count).to eq 2
        results = snmp_results_for_tunnel_count(gateway_tunnel_limit - 1, mac)
        device.process_snmp_results(results)
        expect(device.tunnels(true).count).to eq(gateway_tunnel_limit - 1)
        device.tunnels.order(:ipsec_iface).each_with_index do |t, i|
          expect(t.ipsec_iface).to eq "IPSEC#{i}"
        end
      end
    end

    context "when the device's tunnels count is out of sync" do
      it 'resets the tunnels_count to the correct value' do
        device.update tunnels_count: 10
        expect(device.tunnels_count).to eq 10
        results = snmp_results_for_tunnel_count(2, mac)
        device.process_snmp_results(results)
        expect(device.reload.tunnels_count).to eq 2
      end
    end

    context 'when a device connected to a VIG tunnel connects to a different VIG' do
      it 'disassociates to the old VIG and associates to the new VIG' do
        orig_vig = device.tunnels.create!(ipsec_iface: 'IPSEC0', endpoint_type: GatewayTunnel::ENDPOINT_VIG, endpoint: '1.2.3.4')
        expect(device.connected_to_vig?).to be true
        expect(device.tunnels).to include orig_vig
        results = snmp_results_for_tunnel_count(2, mac, GatewayTunnel::ENDPOINT_VIG)
        device.process_snmp_results(results)
        device.reload
        expect(device.connected_to_vig?).to be true
        expect(device.tunnels.count).to eq 2
        expect(device.tunnels).not_to include orig_vig
      end
    end
  end
end
