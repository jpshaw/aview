# frozen_string_literal: true

require 'rails_helper'

describe GatewayDetails do
  before do
    @details = create(:gateway_details)
  end

  describe '#cell_extender_name' do
    context 'when the interface is invalid' do
      it 'returns nil' do
        expect(@details.cell_extender_name(:wan_1234)).to be nil
      end
    end

    context 'when the cell extender field is nil' do
      before do
        allow(@details).to receive(:wan_1_cell_extender).and_return(nil)
      end

      it 'returns "N/A"' do
        expect(@details.cell_extender_name(:wan_1)).to eq 'N/A'
      end
    end

    context 'when the cell extender field is set to a known value to indicate a 6200-FX' do
      before do
        allow(@details).to receive(:wan_1_cell_extender).and_return('1')
      end

      it 'returns a string describing the extender' do
        expect(@details.cell_extender_name(:wan_1)).to eq '6200-FX'
      end
    end

    context 'when the cell extender field is set to a known value to indicate a 6300-CX or LX' do
      before do
        allow(@details).to receive(:wan_1_cell_extender).and_return('2')
      end

      it 'returns a string describing the extender' do
        expect(@details.cell_extender_name(:wan_1)).to eq '6300-CX/LX'
      end
    end
  end
end
