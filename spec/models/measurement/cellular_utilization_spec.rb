# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Measurement::CellularUtilization, type: :model do
  subject { Measurement::CellularUtilization }

  let(:mac) { 'AAA' }
  let(:carrier) { 'at&t' }
  let(:data_plan_name) { 'Test' }
  let(:interface) { 'PPP0' }
  let(:received) { 1000 }
  let(:transmitted) { 10 }
  let(:time) { Time.now }
  let(:valid_attributes) do
    {
      mac: mac,
      carrier: carrier,
      data_plan: data_plan_name,
      interface: interface,
      rx: received,
      tx: transmitted,
      time: time
    }
  end

  let(:formatted_valid_attributes) do
    {
      tags: { mac: mac, carrier: carrier, data_plan: data_plan_name, interface: interface },
      values: { received: received, transmitted: transmitted },
      timestamp: time.to_i
    }
  end

  before do
    @device = build(:test_device)
  end

  context '.new' do
    let(:result) { subject.create(valid_attributes) }

    it 'uses existing Carrier when carrier name already exists' do
      create(:carrier, name: 'AT&T')
      allow(subject).to receive(:save_sample).with(formatted_valid_attributes)
      expect { result }.not_to change(Carrier, :count)
    end
  end

  describe '.points_by_carrier_for_mac' do
    it 'calls query with the correct arguments' do
      allow(subject).to receive(:query).with(legacy: { name: :points_by_carrier_for_mac, args: [@device.mac, '24h', '1h', '0'] })
      subject.points_by_carrier_for_mac(@device.mac)
    end
  end

  context '.range_from_allowed_ranges' do
    it 'returns the passed in range when valid' do
      result = subject.range_from_allowed_ranges('30d')
      expect(result).to eq('30d')
    end

    it 'returns the default when invalid range' do
      result = subject.range_from_allowed_ranges('1d')
      expect(result).to eq(Measurement::CellularUtilization::DEFAULT_RANGE)
    end
  end

  context '.interval_by_range' do
    it 'returns an interval when passed a range' do
      result = subject.interval_by_range('24h')
      expect(result).to eq('1h')
    end
  end

  context '.utilization_totals' do
    it 'calls query with the correct arguments' do
      args = %w[AAA 24h]
      allow(subject).to receive(:query).with(legacy: { name: :utilization_totals, args: [args] })
      subject.utilization_totals(args)
    end
  end

  describe '.data_plan_usage' do
    it 'calls query with the correct arguments' do
      time = 3.days.ago
      allow(subject).to receive(:query).with(legacy: { name: :data_plan_usage, args: [{ start_time: time, data_plan_id: 1 }] })
      subject.data_plan_usage(start_time: time, data_plan_id: 1)
    end
  end
end
