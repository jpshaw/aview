# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Measurement::WanUtilization do
  subject { described_class }

  context '.new' do
    let(:params) do
      {
        mac: 'AABB',
        interface: interface,
        upload: 1,
        upload_hwm: 10,
        download: 2,
        download_hwm: 20
      }
    end

    context 'with nil interface' do
      let(:interface) {}

      it 'keeps interface nil' do
        expect(subject.new(params).interface).to eq interface
      end
    end

    context 'with lowercase interface' do
      let(:interface) { 'eth1' }

      it 'upcases interface' do
        expect(subject.new(params).interface).to eq interface.upcase
      end
    end
  end

  context '.range_by_interval_by_field' do
    let(:options) { { mac: 'AABB', interval: '1h', range: '24h' } }

    it 'returns the correctly formatted results' do
      expect(subject).to receive(:query).with(legacy: { name: :range_by_interface_and_interval_points, args: [options] })
      subject.range_by_interval_by_field(options)
    end
  end
end
