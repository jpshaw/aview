# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Measurement::DeviceAvailability do
  describe 'ClassMethods' do
    subject { described_class }

    describe '.get' do
      let(:mac_1) { Faker::Lorem.characters(10) }
      let(:mac_2) { Faker::Lorem.characters(10) }
      let(:macs) { [mac_1, mac_2] }
      let(:range) { '30d' }
      let(:intervals) { '1h' }
      let(:opts) { { macs: macs, range: range, intervals: intervals } }

      it 'calls .query on MetricsService::Client' do
        expect(subject).to receive(:query).with(legacy: { name: :get_device_availability, args: [opts] })
        subject.get(opts)
      end
    end
  end
end
