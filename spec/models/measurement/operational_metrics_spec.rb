# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Measurement::OperationalMetrics do
  subject { described_class }

  let(:valid_attributes) do
    {
      event_time: time,
      device_statuses: device_statuses,
      queues: queues,
      system: system,
      influx: influx,
      jboss: jboss,
      mysql: mysql
    }
  end

  let(:time) { Time.now.utc.to_i }

  let(:device_statuses) { [{ 'gateway_unknown' => 1 }, { 'gateway_up' => 2 }, { 'gateway_down' => 3 }] }

  let(:queues) do
    [
      { name: '/queues/abc', count: 1 },
      { name: '/queues/xyz', count: 2 }
    ]
  end

  let(:system) do
    [
      { system_load: 1 },
      { system_free_memory: 2 },
      { system_free_disk: 3 }
    ]
  end

  let(:influx) do
    [
      { influx_disk: 1 },
      { influx_memory: 2 }
    ]
  end

  let(:jboss) do
    [
      { jboss_disk: 1 },
      { jboss_memory: 2 }
    ]
  end

  let(:mysql) do
    [
      { mysql_disk: 1 },
      { mysql_memory: 2 }
    ]
  end

  let(:output) do
    { tags: nil, timestamp: time, values: values }
  end
  let(:values) do
    {
      'gateway_unknown' => 1,
      'gateway_up' => 2,
      'gateway_down' => 3,
      'abc_queue' => 1,
      'xyz_queue' => 2,
      'system_load' => 1,
      'system_free_memory' => 2,
      'system_free_disk' => 3,
      'influx_disk' => 1,
      'influx_memory' => 2,
      'jboss_disk' => 1,
      'jboss_memory' => 2,
      'mysql_disk' => 1,
      'mysql_memory' => 2
    }
  end

  context '.create' do
    it 'sends the correct argument to write_point' do
      expect(described_class).to receive(:save_sample).with(output)
      subject.create(valid_attributes)
    end
  end
end
