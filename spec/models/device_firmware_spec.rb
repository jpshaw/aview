# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'a compatible firmwares finder' do
  it 'returns firmwares with the same schema_version' do
    expect(first_firmware.compatible_firmwares).to include(first_firmware)
  end

  it 'returns firmwares with a higher schema_version' do
    expect(first_firmware.compatible_firmwares).to include(second_firmware)
  end

  it 'does not return firmwares with a lower schema_version' do
    expect(second_firmware.compatible_firmwares).not_to include(first_firmware)
  end

  it 'returns firmwares with a migration_versions containing the schema_version' do
    second_firmware.update_column(:migration_versions, "#{DeviceFirmware::SEPARATOR}1#{DeviceFirmware::SEPARATOR}")
    expect(first_firmware.compatible_firmwares).to include(second_firmware)
  end

  it 'always returns itself' do
    # Active
    expect(first_firmware.compatible_firmwares).to include(first_firmware)
    # Inactive
    first_firmware.update_column(:deprecated, true)
    expect(first_firmware.compatible_firmwares).to include(first_firmware)
  end
end

describe DeviceFirmware do
  # This block for delegations exists because the shoulda-matcher 'delegates_method' doesn't seem to work.
  describe 'Delegations' do
    before do
      subject.device_model = create(:test_model)
    end

    describe '#category' do
      it 'delegates to device model' do
        # To check for the method being called after the fact (have_received) the method must be stubbed first.
        allow(subject.device_model).to receive(:category)
        subject.category
        expect(subject.device_model).to have_received(:category)
      end
    end
  end

  describe 'Validations' do
    describe '#version' do
      let(:firmware) { create(:device_firmware) }

      it 'must be unique scoped to the device_model_id' do
        subject.version = firmware.version

        # With device_model_id collision.
        subject.device_model_id = firmware.device_model.id
        subject.valid?
        expect(subject.errors.keys).to include(:version)

        # Without device_model_id collision.
        subject.device_model_id = firmware.device_model.id + 1
        subject.valid?
        expect(subject.errors.keys).not_to include(:version)
      end

      it 'must be a dot separated list of numeric values' do
        subject.device_model = firmware.device_model

        # With an invalid format.
        subject.version = 'invalid_value'
        subject.valid?
        expect(subject.errors.keys).to include(:version)

        subject.version = '10.10'
        subject.valid?
        expect(subject.errors.keys).not_to include(:version)

        subject.version = '10.10.10'
        subject.valid?
        expect(subject.errors.keys).not_to include(:version)
      end
    end
  end

  describe 'Scopes' do
    before do
      subject.version = '10.10.1'
    end

    describe '#active' do
      it 'does not select non-production flagged records' do
        subject.production = false

        [true, false].each do |value|
          subject.deprecated = value
          subject.save!
          expect(described_class.active).not_to include(subject)
        end
      end

      it 'does not select deprecated flagged records' do
        subject.deprecated = true

        [true, false].each do |value|
          subject.production = value
          subject.save!
          expect(described_class.active).not_to include(subject)
        end
      end

      it 'selects production and non-deprecated flagged records' do
        subject.production = true
        subject.deprecated = false
        subject.save!
        expect(described_class.active).to include(subject)
      end
    end
  end

  describe '#compatible_firmwares' do
    let(:model_name)      { 'a_model_name' }
    let(:device_model)    { create(:test_model, name: model_name) }
    let(:first_firmware)  { device_model.device_firmwares.create(version: '1.1.1', schema_version: 1, production: true, deprecated: false) }
    let(:second_firmware) { device_model.device_firmwares.create(version: '1.1.2', schema_version: 2, production: true, deprecated: false) }

    context 'with no user' do
      let(:the_user)  { nil }

      it_behaves_like 'a compatible firmwares finder' do
        let(:user) { the_user }
      end

      it 'does not return non-active firmwares other than itself' do
        second_firmware.update_column(:deprecated, true)
        expect(first_firmware.compatible_firmwares(the_user)).not_to include(second_firmware)
      end
    end

    context 'when user cannot assign non-production firmwares' do
      let(:the_user) { User.new }

      before do
        allow(the_user).to receive(:has_ability?).with('Assign Non-Production Firmwares').and_return(false)
      end

      it_behaves_like 'a compatible firmwares finder' do
        let(:user) { the_user }
      end

      it 'does not return non-active firmwares other than itself' do
        second_firmware.update_column(:deprecated, true)
        expect(first_firmware.compatible_firmwares(the_user)).not_to include(second_firmware)
      end
    end

    context 'when user can assign non-production firmwares' do
      let(:the_user) { User.new }

      before do
        allow(the_user).to receive(:has_ability?).with('Assign Non-Production Firmwares').and_return(true)
      end

      it_behaves_like 'a compatible firmwares finder' do
        let(:user) { the_user }
      end

      it 'returns non-active firmwares' do
        second_firmware.update_column(:deprecated, true)
        expect(first_firmware.compatible_firmwares(the_user)).to include(second_firmware)
      end
    end
  end

  describe '#<=>' do
    it 'compares firmwares by the semantic version value' do
      df1         = described_class.new
      df2         = described_class.new
      df1.version = '2'
      df2.version = '11'

      # When comparing the string values of the version, df1 > df2.
      expect(df1.version).to be > df2.version

      # When comparing the schemas themselves, df1 < df2.
      expect(df1).to be < df2
    end
  end
end
