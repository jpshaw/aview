# frozen_string_literal: true

require 'rails_helper'

describe DialTestResult do
  describe 'save' do
    context 'when 10 test results exist for the device' do
      let(:device) { create(:test_device) }

      before do
        @results = []
        10.times do
          @results << described_class.create(device_id: device.id)
        end
      end

      it 'deletes the oldest test' do
        expect do
          described_class.create(device_id: device.id)
        end.not_to change { described_class.where(device_id: device.id).count }
        sorted_results = @results.sort_by &:id
        expect { sorted_results.first.reload }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
