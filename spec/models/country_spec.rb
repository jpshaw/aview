# frozen_string_literal: true

require 'rails_helper'

describe Country do
  describe 'method: ' do
    describe 'self.select_list' do
      it 'returns an array' do
        expect(described_class.select_list.class).to eq Array
      end

      it 'returns all countries in the table plus one' do
        expect(described_class.select_list.count).to eq described_class.count + 1
      end
    end
  end
end
