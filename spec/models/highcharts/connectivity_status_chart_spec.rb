# frozen_string_literal: true

require 'rails_helper'

describe Highcharts::SignalStrengthChart do
  context '#response' do
    subject(:total) { chart.response[:total_count] }

    before do
      @user = create(:user_with_org_permission)
    end

    let(:chart) { Highcharts::ConnectivityStatusChart.new(@user) }

    let(:series) { chart.response[:series] }

    let(:up) { series[0][:y] }

    let(:down) { series[1][:y] }

    let(:unreachable) { series[2][:y] }

    let(:links) { chart.response[:links] }

    context 'with no devices' do
      it 'has an empty series' do
        expect(series).to eq []
      end
    end

    context 'with one up device' do
      before do
        create(:test_device, :up, organization_id: @user.organization_id)
      end

      context 'series' do
        it 'returns three series objects: up: 1, down: 0, unreachable: 0' do
          expect(up).to eq 1
          expect(down).to eq 0
          expect(unreachable).to eq 0
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::ConnectivityStatusChart::STATES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one down device' do
      before do
        create(:test_device, :down, organization_id: @user.organization_id)
      end

      context 'series' do
        it 'returns three series objects: up: 0, down: 1, unreachable: 0' do
          expect(up).to eq 0
          expect(down).to eq 1
          expect(unreachable).to eq 0
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::ConnectivityStatusChart::STATES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one unreachable device' do
      before do
        create(:test_device, :unreachable, organization_id: @user.organization_id)
      end

      context 'series' do
        it 'returns three series objects: up: 0, down: 0, unreachable: 1' do
          expect(up).to eq 0
          expect(down).to eq 0
          expect(unreachable).to eq 1
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::ConnectivityStatusChart::STATES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with undeployed devices' do
      before do
        up = create(:test_device, :up, organization_id: @user.organization_id)
        up.undeploy
        down = create(:test_device, :down, organization_id: @user.organization_id)
        down.undeploy
        unreachable = create(:test_device, :unreachable, organization_id: @user.organization_id)
        unreachable.undeploy
      end

      context 'series' do
        it 'has an empty series' do
          expect(series).to eq []
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links).not_to be
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).not_to be
        end
      end
    end

    context 'with multiple devices' do
      before do
        create_list(:test_device, 4, :up, organization_id: @user.organization_id)
        create_list(:test_device, 3, :down, organization_id: @user.organization_id)
        create_list(:test_device, 5, :unreachable, organization_id: @user.organization_id)
      end

      it 'returns summed counts of each device state' do
        expect(up).to eq 4
        expect(down).to eq 3
        expect(unreachable).to eq 5
      end
    end
  end
end
