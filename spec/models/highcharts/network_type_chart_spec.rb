# frozen_string_literal: true

require 'rails_helper'

describe Highcharts::NetworkTypeChart do
  context '#response' do
    subject(:total) { chart.response[:total_count] }

    before do
      @user = create(:user_with_org_permission)
    end

    let(:chart) { described_class.new(@user) }

    let(:series) { chart.response[:series] }

    let(:four_g) { series[0][:y] }

    let(:three_g) { series[1][:y] }

    let(:two_g) { series[2][:y] }

    let(:unknown) { series[3][:y] }

    let(:links) { chart.response[:links] }

    context 'with no device modems' do
      it 'has an empty series' do
        expect(series).to eq []
      end
    end

    context 'with one 4G device' do
      before do
        device = create(:test_device, :deployed, organization_id: @user.organization_id)
        create(:device_modem, device: device, cnti: 'LTE')
      end

      context 'series' do
        it 'returns four series objects: 4G: 1, 3G: 0, 2G: 0, unknown: 0' do
          expect(four_g).to eq 1
          expect(three_g).to be_nil
          expect(two_g).to be_nil
          expect(unknown).to be_nil
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::NetworkTypeChart::NETWORK_TYPES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one 3G device' do
      before do
        device = create(:test_device, :deployed, organization_id: @user.organization_id)
        create(:device_modem, device: device, cnti: DeviceModem::THREE_G_NETWORK.first)
      end

      context 'series' do
        it 'returns four series objects: 4G: 0, 3G: 1, 2G: 0, unknown: 0' do
          expect(four_g).to be_nil
          expect(three_g).to eq 1
          expect(two_g).to be_nil
          expect(unknown).to be_nil
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::NetworkTypeChart::NETWORK_TYPES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one 2G device' do
      before do
        device = create(:test_device, :deployed, organization_id: @user.organization_id)
        create(:device_modem, device: device, cnti: 'CDMA-1X')
      end

      context 'series' do
        it 'returns four series objects: 4G: 0, 3G: 0, 2G: 1, unknown: 0' do
          expect(four_g).to be_nil
          expect(three_g).to be_nil
          expect(two_g).to eq 1
          expect(unknown).to be_nil
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::NetworkTypeChart::NETWORK_TYPES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one unknown device' do
      before do
        device = create(:test_device, :deployed, organization_id: @user.organization_id)
        create(:device_modem, device: device, cnti: '---')
      end

      context 'series' do
        it 'returns four series objects: 4G: 0, 3G: 0, 2G: 0, unknown: 1' do
          expect(four_g).to be_nil
          expect(three_g).to be_nil
          expect(two_g).to be_nil
          expect(unknown).to eq 1
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq Highcharts::NetworkTypeChart::NETWORK_TYPES.size
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with multiple device modems' do
      before do
        cntis = [
          DeviceModem::FOUR_G_NETWORK.first,
          DeviceModem::TWO_G_NETWORK.first,
          DeviceModem::TWO_G_NETWORK.last,
          '---',
          DeviceModem::THREE_G_NETWORK.first,
          DeviceModem::THREE_G_NETWORK.last,
          DeviceModem::FOUR_G_NETWORK.last
        ]

        cntis.each do |cnti|
          device = create(:test_device, :deployed, organization_id: @user.organization_id)
          create(:device_modem, device: device, cnti: cnti)
        end
      end

      it 'returns summed counts of each network' do
        expect(four_g).to eq 2
        expect(three_g).to eq 2
        expect(two_g).to eq 2
        expect(unknown).to eq 1
      end
    end
  end
end
