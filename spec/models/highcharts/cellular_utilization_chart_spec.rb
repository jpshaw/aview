# frozen_string_literal: true

require 'rails_helper'

describe Highcharts::CellularUtilizationChart do
  context '#response' do
    context 'series_by_carrier with one carrier outside range and two inside range' do
      subject(:carrier_2_tx) { @response[:series_by_carrier][1][:carrier_series][1][:data] }

      before do
        allow(Measurement::CellularUtilization).to receive(:points_by_carrier_for_mac).and_return(points_by_carrier_for_mac)
        allow(Measurement::CellularUtilization).to receive(:utilization_totals).and_return(utilization_totals)
        @response = described_class.new(id: mac, range: '30d').response
      end

      let(:mac) { 'AAA' }
      let(:points_by_carrier_for_mac) do
        [
          { 'name' => 'cellular_utilization', 'tags' => { 'carrier' => 'AT&T' }, 'values' => points_by_carrier_for_mac_values },
          { 'name' => 'cellular_utilization', 'tags' => { 'carrier' => 'Verizon' }, 'values' => points_by_carrier_for_mac_values }
        ]
      end
      let(:points_by_carrier_for_mac_values) do
        points = []
        30.times { |_i| points << { 'time' => Time.now.utc.to_i, 'received' => 100, 'transmitted' => 10 } }
        points
      end
      let(:utilization_totals) do
        [
          { 'name' => 'cellular_utilization', 'tags' => { 'carrier' => 'AT&T' }, 'values' => [{ 'time' => Time.now.utc.to_i, 'received' => 100, 'transmitted' => 10 }] },
          { 'name' => 'cellular_utilization', 'tags' => { 'carrier' => 'Verizon' }, 'values' => [{ 'time' => Time.now.utc.to_i, 'received' => 100, 'transmitted' => 10 }] }
        ]
      end

      let(:carrier_1_series) { @response[:series_by_carrier][0][:carrier_series] }

      let(:carrier_2_series) { @response[:series_by_carrier][1][:carrier_series] }

      let(:carrier_1_rx) { @response[:series_by_carrier][0][:carrier_series][0][:data] }

      let(:carrier_1_tx) { @response[:series_by_carrier][0][:carrier_series][1][:data] }

      let(:carrier_2_rx) { @response[:series_by_carrier][1][:carrier_series][0][:data] }

      it 'returns two carriers' do
        expect(@response[:series_by_carrier].size).to eq(2)
      end

      it 'has two series per carrier' do
        expect(carrier_1_series.size).to eq(2)
        expect(carrier_2_series.size).to eq(2)
      end

      it 'has a point for each day of the range' do
        expect(carrier_1_rx.size).to eq(30)
        expect(carrier_1_tx.size).to eq(30)
        expect(carrier_2_rx.size).to eq(30)
        expect(carrier_2_tx.size).to eq(30)
      end
    end
  end
end
