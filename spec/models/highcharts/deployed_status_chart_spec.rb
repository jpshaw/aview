# frozen_string_literal: true

require 'rails_helper'

describe Highcharts::DeployedStatusChart do
  context '#response' do
    subject(:total) { chart.response[:total_count] }

    before do
      @user = create(:user_with_org_permission)
    end

    let(:chart) { described_class.new(@user) }

    let(:series) { chart.response[:series] }

    let(:deployed) { series[0][:y] }

    let(:undeployed) { series[1][:y] }

    let(:links) { chart.response[:links] }

    context 'with no devices' do
      it 'has an empty series' do
        expect(series).to eq []
      end
    end

    context 'with one deployed device' do
      before do
        create(:test_device, :deployed, organization_id: @user.organization_id)
      end

      context 'series' do
        it 'returns two series objects: deployed: 1, undeployed: 0' do
          expect(deployed).to eq 1
          expect(undeployed).to eq 0
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq(Highcharts::DeployedStatusChart::DEPLOYMENT_STATUSES.size)
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end

    context 'with one undeployed device' do
      before do
        create(:test_device, :undeployed, organization_id: @user.organization_id)
      end

      context 'series' do
        it 'returns two series objects: deployed: 0, undeployed: 1' do
          expect(deployed).to eq 0
          expect(undeployed).to eq 1
        end
      end

      context 'links' do
        it 'has the correct number of links' do
          expect(links.size).to eq(Highcharts::DeployedStatusChart::DEPLOYMENT_STATUSES.size)
        end
      end

      context 'total count' do
        it 'has the correct total count' do
          expect(total).to eq 1
        end
      end
    end
  end
end
