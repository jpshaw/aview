# frozen_string_literal: true

require 'rails_helper'

describe DeviceState do
  describe 'transitions' do
    before do
      allow(subject).to receive_message_chain(:device, :alert_down)
    end

    describe ':down' do
      it 'transition device state from UP to DOWN' do
        subject.update_attribute(:state, DeviceState::States::UP)
        subject.reload
        expect(subject.state).to eq(DeviceState::States::UP)

        subject.state_machine.trigger(:down)

        subject.reload
        expect(subject.state).to eq(DeviceState::States::DOWN)
      end

      it 'transition device state from UNREACHABLE to DOWN' do
        subject.update_attribute(:state, DeviceState::States::UNREACHABLE)
        subject.reload
        expect(subject.state).to eq(DeviceState::States::UNREACHABLE)

        subject.state_machine.trigger(:down)

        subject.reload
        expect(subject.state).to eq(DeviceState::States::DOWN)
      end
    end
  end
end
