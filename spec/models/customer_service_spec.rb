# frozen_string_literal: true

require 'rails_helper'

describe CustomerService do
  describe 'Class methods' do
    describe '.all_for_select' do
      before do
        create_list(:customer_service, 3)
      end

      it 'returns an array' do
        expect(described_class.all_for_select.class).to be Array
      end

      it 'returns as many elements as records exist in the table' do
        expect(described_class.all_for_select.size).to eq(described_class.count)
      end

      it 'returns elements containing the name and the id of the records' do
        all_for_select = described_class.all_for_select
        described_class.all.each do |cs|
          expect(all_for_select).to include([cs.name, cs.id])
        end
      end
    end
  end
end
