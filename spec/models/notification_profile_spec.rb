# frozen_string_literal: true

require 'rails_helper'

describe NotificationProfile do
  describe 'methods' do
    let(:user) { create_user }
    let(:immediate_profile) { create_profile_for_immediate_gateway_notifications }
    let(:default_profile) { create_default_profile_for_immediate_gateway_notifications }
    let(:no_events_profile) { create_profile_with_no_subscribed_events }

    describe '#set_uuids' do
      it 'does nothing if nil is passed' do
        immediate_profile.set_uuids(nil)
        immediate_profile.subscribed_events(true).each do |event|
          expect(event.immediate?).to be true
        end
      end

      it 'does nothing if a hash is not passed' do
        immediate_profile.set_uuids([1, 2, 3])
        immediate_profile.subscribed_events(true).each do |event|
          expect(event.immediate?).to be true
        end
      end

      it 'does nothing if uuids and frequencies are not valid' do
        uuids = {
          'abc' => '123',
          'def' => '456'
        }
        immediate_profile.set_uuids(uuids)
        immediate_profile.subscribed_events(true).each do |event|
          expect(event.immediate?).to be true
        end
      end

      it 'sets frequencies for valid uuids' do
        profile = create_profile_for_immediate_gateway_notifications
        profile.subscribed_events.each do |event|
          expect(event.immediate?).to be true
        end

        uuids = {}
        @uuids.each do |uuid|
          uuids[uuid.id] = NotificationProfile::Frequencies::NEVER
        end

        profile.set_uuids(uuids)

        # Reload profile
        profile.subscribed_events(true).each do |event|
          expect(event.never?).to be true
        end
      end
    end

    describe '#subscribed_event_settings' do
      it 'returns an empty hash if the profile has no subscribed events' do
        expect(no_events_profile.subscribed_event_settings).to eq({})
      end

      it 'returns a hash of uuids => frequencies for the subscribed events of a profile' do
        settings = immediate_profile.subscribed_event_settings
        expect(settings.is_a?(Hash)).to be true
        settings.each do |key, value|
          event = immediate_profile.subscribed_events.where(uuid_id: key, frequency: value).first
          expect(event).not_to be nil
        end
      end
    end
  end
end
