# frozen_string_literal: true

require 'rails_helper'

describe CertificateAuthority do
  describe '#new' do
    context 'with an invalid request' do
      let(:request) { 'invalid_request' }

      it 'raises an error' do
        expect { described_class.new request }.to raise_error(StandardError)
      end
    end

    context 'with a valid request' do
      let(:request) { File.read("#{Rails.root}/spec/support/fixtures/certificate_request.crt.req") }
      let(:certificate_authority) { described_class.new(request) }

      it 'returns a CertificateAuthority instance' do
        expect(certificate_authority.class).to eq(described_class)
      end

      it 'instantiates an SSL request' do
        expect(certificate_authority.instance_variable_get(:@request).class).to eq(OpenSSL::X509::Request)
      end
    end
  end

  describe '#details' do
    let(:request) { File.read("#{Rails.root}/spec/support/fixtures/certificate_request.crt.req") }
    let(:certificate_authority) { described_class.new(request) }

    it 'returns an SSL request object' do
      expect(certificate_authority.details.class).to eq(OpenSSL::X509::Request)
    end
  end

  describe '#common_name' do
    let(:request) { File.read("#{Rails.root}/spec/support/fixtures/certificate_request.crt.req") }
    let(:certificate_authority) { described_class.new(request) }

    it "returns an SSL request's common name" do
      # This value is known because we know this is a real certificate.
      expect(certificate_authority.common_name).to eq('002704010abb')
    end
  end

  describe '#sign_request' do
    let(:request) { File.read("#{Rails.root}/spec/support/fixtures/certificate_request.crt.req") }
    let(:certificate_authority) { described_class.new(request) }

    before do
      allow(Settings.certificates).to receive(:ca_crt_path).and_return("#{Rails.root}/spec/support/fixtures/certificate_authority.crt")
      allow(Settings.certificates).to receive(:ca_key_path).and_return("#{Rails.root}/spec/support/fixtures/certificate_authority.pem")
    end

    it 'returns a signed request, different from the original' do
      expect(certificate_authority.sign_request).not_to eq(request)
    end

    it 'is not idempotent (returns different results for the same request)' do
      first_signed_request = expect(certificate_authority.sign_request).not_to eq(request)
      expect(certificate_authority.sign_request).not_to eq(first_signed_request)
    end
  end
end
