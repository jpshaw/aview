# frozen_string_literal: true

require 'rails_helper'

describe Gateway do
  it_behaves_like 'a base device class'
  it_behaves_like 'a monitorable device'
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:gateway_device) }
  end
  it_behaves_like 'a gateway device' do
    let(:platform) { :gateway }
    let(:device)   { create(:gateway_device) }
  end
  it_behaves_like 'a device with dual wan capabilities'

  context 'given a gateway device' do
    before do
      @device = create(:gateway_device_with_configuration, :up)
    end

    describe '#can_send_commands?' do
      context 'when the device has a management host, firmware and portal config' do
        before do
          @device.mgmt_host = '1.2.3.4'
          allow(@device).to receive(:has_portal_config?).and_return(true)
        end

        context 'with valid firmware' do
          before { @device.firmware = '6.3.4' }

          it 'returns true' do
            expect(@device.can_send_commands?).to be true
          end
        end

        context 'with invalid firmware' do
          before { @device.firmware = 'UNKNOWN' }

          it 'returns false' do
            expect(@device.can_send_commands?).to be false
          end
        end
      end

      context 'when the device does not have a management host, firmware or portal config' do
        before do
          @device.mgmt_host = nil
          @device.firmware = nil
          allow(@device).to receive(:has_portal_config?).and_return(false)
        end

        it 'returns false' do
          expect(@device.can_send_commands?).to be false
        end
      end
    end

    describe '#can_receive_sms_commands?' do
      it 'returns false' do
        expect(@device.can_receive_sms_commands?).to be false
      end
    end

    describe '#supports_rollback_commands?' do
      context 'when the device supports json commands' do
        before do
          allow(@device).to receive(:supports_json_commands?).and_return(true)
        end

        # Note: Firmware minimum for rollback commands is 6.4
        context 'with valid firmware' do
          context 'when the device firmware is less than the required minimum' do
            before { @device.firmware = '6.3.0' }

            it 'returns false' do
              expect(@device.supports_rollback_commands?).to be false
            end
          end

          context 'when the device firmware is equal to the required minimum' do
            before { @device.firmware = '6.4.0' }

            it 'returns true' do
              expect(@device.supports_rollback_commands?).to be true
            end
          end

          context 'when the device firmware is greater than the required minimum' do
            before { @device.firmware = '6.5.0' }

            it 'returns true' do
              expect(@device.supports_rollback_commands?).to be true
            end
          end
        end

        context 'with invalid firmware' do
          before { @device.firmware = 'UNKNOWN' }

          it 'returns false' do
            expect(@device.supports_rollback_commands?).to be false
          end
        end
      end

      context 'when the device does not support json commands' do
        before do
          allow(@device).to receive(:supports_json_commands?).and_return(false)
        end

        it 'returns false' do
          expect(@device.supports_rollback_commands?).to be false
        end
      end
    end

    describe '#snmp_config' do
      it 'uses a longer snmp timeout for an up device' do
        snmp_timeout_up = @device.snmp_config[:timeout]
        @device.timeout!
        snmp_timeout_down = @device.snmp_config[:timeout]
        expect(snmp_timeout_up).to be > snmp_timeout_down
      end
    end

    describe '#in_backup_mode?' do
      let(:details) { create(:gateway_details, device: @device) }

      context 'when the device has a backup mode timestamp' do
        before { allow(@device.details).to receive(:backup_started_at).and_return(Time.now) }

        it 'returns true' do
          expect(@device).to be_in_backup_mode
        end
      end

      context 'when the device does not have a backup mode timestamp' do
        before { allow(@device.details).to receive(:backup_started_at).and_return(nil) }

        it 'returns false' do
          expect(@device).not_to be_in_backup_mode
        end
      end
    end

    describe '#vrrp_master?' do
      let(:details) { create(:gateway_details, device: @device) }

      context 'when the device has vrrp state of Master mode' do
        before { allow(@device.details).to receive(:vrrp_state).and_return('Master') }

        it 'returns true' do
          expect(@device).to be_vrrp_master
        end
      end

      context 'when the device has an unknown vrrp state' do
        before { allow(@device.details).to receive(:vrrp_state).and_return('Unknown') }

        it 'returns false' do
          expect(@device).not_to be_vrrp_master
        end
      end

      context 'when the device has a blank vrrp state' do
        before { allow(@device.details).to receive(:vrrp_state).and_return(nil) }

        it 'returns false' do
          expect(@device).not_to be_vrrp_master
        end
      end
    end

    context 'with a domain name' do
      before do
        @example_domain_name = '010203.device.accns.com'
        @device.domain_name = @example_domain_name
      end

      describe 'remote configs' do
        it 'uses the domain name when available' do
          expect(@device.snmp_config[:host]).to eq @example_domain_name
          expect(@device.portal_config[:host]).to eq @example_domain_name
        end
      end
    end

    describe '#supports_json_commands?' do
      context 'when the device has a firmware and portal config' do
        before do
          allow(@device).to receive(:has_portal_config?).and_return(true)
        end

        context 'when the device firmware is within the json legacy range' do
          context 'with the minimum json legacy firmware' do
            before { @device.firmware = Gateway::JSON_LEGACY_FIRMWARE_RANGE.first }

            it 'returns true' do
              expect(@device.supports_json_commands?).to be true
            end
          end

          context 'with the maximum json legacy firmware' do
            before { @device.firmware = Gateway::JSON_LEGACY_FIRMWARE_RANGE.last }

            it 'returns true' do
              expect(@device.supports_json_commands?).to be true
            end
          end
        end

        # Note: Firmware minimum for json commands is 6.1.8
        context 'with valid firmware' do
          context 'when the firmware is less than the required minimum' do
            before { @device.firmware = '6.1.7' }

            it 'returns false' do
              expect(@device.supports_json_commands?).to be false
            end
          end

          context 'when the firmware is equal to the required minimum' do
            before { @device.firmware = '6.1.8' }

            it 'returns true' do
              expect(@device.supports_json_commands?).to be true
            end
          end

          context 'when the firmware is greater than the required minimum' do
            before { @device.firmware = '6.1.9' }

            it 'returns true' do
              expect(@device.supports_json_commands?).to be true
            end
          end
        end

        context 'with invalid firmware' do
          before { @device.firmware = 'UNKNOWN' }

          it 'returns false' do
            expect(@device.supports_json_commands?).to be false
          end
        end
      end

      context 'when the device does not have a firmware or portal config' do
        before do
          @device.firmware = nil
          allow(@device).to receive(:has_portal_config?).and_return(false)
        end

        it 'returns false' do
          expect(@device.supports_json_commands?).to be false
        end
      end
    end
  end

  describe '#connected_to_vig?' do
    let(:device) { create(:gateway_device) }

    context 'when the device has no tunnels' do
      before do
        expect(device.tunnels.count).to eq 0
      end

      it 'returns false' do
        expect(device.connected_to_vig?).to be false
      end
    end

    context 'when the device has no tunnels connected to a vig' do
      before do
        device.tunnels.create!(endpoint_type: nil)
      end

      it 'returns false' do
        expect(device.connected_to_vig?).to be false
      end
    end

    context 'when the device has a tunnel connected to a vig' do
      before do
        device.tunnels.create!(endpoint_type: GatewayTunnel::ENDPOINT_VIG)
      end

      it 'returns true' do
        expect(device.connected_to_vig?).to be true
      end
    end
  end

  describe '#default_status_freq' do
    it 'returns the value of constant DEFAULT_STATUS_FREQ' do
      expect(subject.__send__(:default_status_freq)).to eq(subject.class::DEFAULT_STATUS_FREQ)
    end
  end

  describe '#grace_period' do
    it 'returns the value of constant DEFAULT_GRACE_PERIOD_DURATION' do
      expect(subject.__send__(:default_grace_period_duration)).to eq(subject.class::DEFAULT_GRACE_PERIOD_DURATION)
    end
  end

  describe '#supports_intelliflow' do
    context 'when intelliflow is enabled in the configuration' do
      subject { device.supports_intelliflow? }

      let(:configuration) { build(:network_configuration, data: { intelliflow_enabled: true }) }
      let(:device) { build(:gateway_device, network_configuration: configuration) }

      context 'when the firmware is blank' do
        it { is_expected.to be false }
      end

      context 'when the firmware is equal to the minimum netflow version' do
        before { device.firmware = '6.5.0' }

        it { is_expected.to be true }
      end

      context 'when the minor version of the firmware is lower than the minimum netflow version' do
        before { device.firmware = '6.4.0' }

        it { is_expected.to be false }
      end

      context 'when the major version of the firmware is lower than the minimum netflow version' do
        before { device.firmware = '5.5.0' }

        it { is_expected.to be false }
      end

      context 'when the build of the firmware is higher than the minimum netflow version' do
        before { device.firmware = '6.5.1' }

        it { is_expected.to be true }
      end

      context 'when the minor version of the firmware is higher than the minimum netflow version' do
        before { device.firmware = '6.6.0' }

        it { is_expected.to be true }
      end

      context 'when the major version of the firmware is higher than the minimum netflow version' do
        before { device.firmware = '7.5.0' }

        it { is_expected.to be true }
      end

      context 'when there is no build version and the minor version of the firmware is higher than the minimum netflow version' do
        before { device.firmware = '6.6' }

        it { is_expected.to be true }
      end

      context 'when there is no build version and the minor version of the firmware is lower than the minimum netflow version' do
        before { device.firmware = '6.4' }

        it { is_expected.to be false }
      end

      context 'when there is no build version and the minor version of the firmware is equal to the minimum netflow version' do
        before { device.firmware = '6.5' }

        it { is_expected.to be true }
      end

      context 'when there is no build version and the major version of the firmware is higher than the minimum netflow version' do
        before { device.firmware = '7.5' }

        it { is_expected.to be true }
      end

      context 'when there is no build version and the minor version of the firmware is lower than the minimum netflow version' do
        before { device.firmware = '5.5' }

        it { is_expected.to be false }
      end

      context 'when the firmware is in development and the version is equal to the required firmware' do
        before { device.firmware = '6.5.0-82E2D4A-DIRTY' }

        it { is_expected.to be true }
      end
    end
  end

  describe 'scopes' do
    describe '.sort_by_device_tunnels_count' do
      before do
        @gateway_1 = create(:gateway_device, tunnels_count: 1)
        @gateway_2 = create(:gateway_device, tunnels_count: 2)
        @gateway_3 = create(:gateway_device, tunnels_count: 3)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_device_tunnels_count 'asc' }

        it 'sorts lower tunnel counts first and higher tunnel counts last' do
          expect(result.first).to eq @gateway_1
          expect(result.second).to eq @gateway_2
          expect(result.last).to eq @gateway_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_device_tunnels_count 'desc' }

        it 'sorts higher tunnel counts first and lower tunnel counts last' do
          expect(result.first).to eq @gateway_3
          expect(result.second).to eq @gateway_2
          expect(result.last).to eq @gateway_1
        end
      end
    end

    describe '.sort_by_active_connection_type' do
      before do
        @gateway_1 = create(:gateway_device, :static_connection_type)
        @gateway_2 = create(:gateway_device, :dhcp_connection_type)
        @gateway_3 = create(:gateway_device)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_active_connection_type 'asc' }

        it 'sorts null tunnel counts first and higher tunnel counts last' do
          expect(result.first).to eq @gateway_3
          expect(result.second).to eq @gateway_1
          expect(result.last).to eq @gateway_2
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_active_connection_type 'desc' }

        it 'sorts higher tunnel counts first and null tunnel counts last' do
          expect(result.first).to eq @gateway_2
          expect(result.second).to eq @gateway_1
          expect(result.last).to eq @gateway_3
        end
      end
    end
  end
end
