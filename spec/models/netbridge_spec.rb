# frozen_string_literal: true

require 'rails_helper'

describe Netbridge do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', NetbridgeConfiguration.new
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:netbridge_device) }
  end
  it_behaves_like 'a buildroot device' do
    let(:platform) { :buildroot }
    let(:device)   { create(:netbridge_device) }
  end
  it_behaves_like 'it has a cellular modem'
end
