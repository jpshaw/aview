# frozen_string_literal: true

require 'rails_helper'

describe DeviceReport do
  before do
    create_user
  end

  describe '.create' do
    it 'does not create a valid report without params' do
      report = @user.reports.create(params: nil)
      expect(report.valid?).to be false
    end

    it 'serializes params and be able to access them' do
      params = { hello: :world }
      report = @user.reports.create(params: params)
      expect(report.params).to eql params
    end
  end

  describe '#table' do
    it 'returns an empty array if set to nil' do
      report = @user.reports.create(params: { hello: :world })
      report.table = nil
      report.save
      expect(report.table).to eql []
    end

    it 'returns an empty array if passed an object that is not an array' do
      report = @user.reports.create(params: { hello: :world })
      report.table = 'hello world'
      report.save
      expect(report.table).to eql []
    end

    it 'returns a results array when passed valid data' do
      results = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12]
      ]
      stub_report_file
      report = @user.reports.create(params: { hello: :world })
      report.table = results
      report.save
      expect(report.table).to eql results
    end
  end

  describe '#report_type' do
    let(:report) { described_class.new(params: {}) }

    it 'returns the value of `report_type` field in params column' do
      expect(report.report_type).to be nil
      report_type_value = 'availability'
      report.params[:report_type] = report_type_value
      expect(report.report_type).to eq(report_type_value)
    end
  end

  describe '#title' do
    let(:report) { described_class.new }

    it 'returns the report type value in title format' do
      report_type = 'availability'
      allow(report).to receive(:report_type).and_return(report_type)
      expect(report.title).to eq(report_type.titleize)
    end
  end

  describe '#stats_headers' do
    let(:report) { described_class.new(params: {}) }

    it 'returns the value of `stats_headers` field in params column' do
      expect(report.stats_headers).to be nil
      stats_headers_value = 'stats headers'
      report.params[:stats_headers] = stats_headers_value
      expect(report.stats_headers).to eq(stats_headers_value)
    end
  end

  describe '#is_done?' do
    let(:report)  { described_class.new }

    context 'when finished_at value is not present' do
      it 'returns false' do
        expect(report.finished_at).to be nil
        expect(report.is_done?).to be false
      end
    end

    context 'when finished_at value is present' do
      it 'returns true' do
        report.finished_at = Time.now
        expect(report.finished_at).to be_present
        expect(report.is_done?).to be true
      end
    end
  end

  describe '#remove_extras' do
    context 'when a new report is created' do
      let(:params) { { text: true } }

      context 'when the total number of reports is below the report limit' do
        before do
          (DeviceReport::REPORT_LIMIT - 1).times do
            @user.reports.create!(params: params)
          end
        end

        it 'does not remove any reports' do
          expect do
            @user.reports.create!(params: params)
          end.to change(described_class, :count).by(1)
        end
      end

      context 'when the total number of reports is greater than the report limit' do
        before do
          DeviceReport::REPORT_LIMIT.times do
            @user.reports.create!(params: params)
          end

          @first_report = @user.reports.order('id asc').first
        end

        it 'keeps the report count at the limit' do
          expect do
            @user.reports.create!(params: params)
          end.not_to change(described_class, :count)
        end

        it 'removes the oldest reports' do
          @user.reports.create!(params: params)
          expect(described_class.find_by(id: @first_report.id)).not_to be_present
        end
      end
    end
  end
end
