# frozen_string_literal: true

require 'rails_helper'

describe Permission do
  describe 'Validations' do
    it 'returns errors when associations are not present' do
      permission = described_class.new
      expect(permission).not_to be_valid
      expect(permission.errors.keys.sort).to eq(%i[manageable manageable_type manager manager_type role])
    end

    it 'returns an error if the manager is a user and his/her organization does not manage the resource' do
      permission = described_class.new(manageable: Organization.new, manager: User.new, role: Role.new)
      expect(permission).not_to be_valid
      expect(permission.errors.full_messages).to include("The user's organization must also manage this organization")
    end

    it 'returns an error if the manageable is not of the correct type' do
      permission = build(:permission, manageable: RoleAbility.new)
      expect(permission).not_to be_valid
      expect(permission.errors.full_messages).to include('Manageable type is not valid')
    end

    it 'returns an error if the manager is not of the correct type' do
      permission = build(:permission, manager: RoleAbility.new)
      expect(permission).not_to be_valid
      expect(permission.errors.full_messages).to include('Manager type is not valid')
    end

    it 'returns an error if combination of manager and manageable already exists' do
      default_permission  = create(:permission)
      permission          = described_class.new(
        manageable: default_permission.manageable,
        manager:    default_permission.manager
      )
      expect(permission).not_to be_valid
      expect(permission.errors.full_messages).to include(
        "The #{default_permission.manager.class.to_s.downcase} already manages the #{default_permission.manageable.class.to_s.downcase} resource"
      )
    end

    it 'does not return errors when associations are present, the manager is a user and his/her organization manages the resource' do
      default_permission  = create(:permission)
      permission          = described_class.new(
        manageable: default_permission.manageable,
        manager:    build(:user, organization: default_permission.manager),
        role:       Role.new
      )
      expect(permission).to be_valid
      expect(permission.errors.keys).to eq([])
    end

    describe '#managed_by_manager_organization' do
      before do
        @root = create(:organization)
      end

      context 'when the manager is also the manageable' do
        before do
          @manager = Organization.new
        end

        it 'does not return any errors' do
          permission = described_class.new(manager: @manager, manageable: @manager, role: Role.new)
          expect(permission).to be_valid
          expect(permission.errors.keys).to eq([])
        end
      end

      context 'when the manager does not respond to organization' do
        before do
          @manager    = Organization.new
          @manageable = Organization.new
        end

        it 'does not return any errors' do
          permission = described_class.new(manager: @manager, manageable: @manageable, role: Role.new)
          expect(permission).to be_valid
          expect(permission.errors.keys).to eq([])
        end
      end

      context 'when the manager has an organization with a permission on the manageable' do
        before do
          @manager = create(:user, organization: @root)
          @manageable = create(:sub_organization)
        end

        it 'does not return any errors' do
          permission = described_class.new(manager: @manager, manageable: @manageable, role: Role.new)
          expect(permission).to be_valid
          expect(permission.errors.keys).to eq([])
        end
      end

      context 'when the manager has an organization without a permission on the manageable' do
        before do
          @manager = create(:user, organization: @root)
          @manageable = create(:random_regular_organization)
        end

        it 'returns errors' do
          permission = described_class.new(manager: @manager, manageable: @manageable, role: Role.new)
          expect(permission).not_to be_valid
          expect(permission.errors.full_messages).to include("The user's organization must also manage this organization")
        end
      end
    end
  end

  describe 'Abilities' do
    # Roles. Model: Role
    let(:grandpa_role)  { create(:admin_role_with_sequenced_name) }
    let(:parent_role)   { create(:admin_role_with_sequenced_name, organization_id: grandpa_role.organization_id) }
    let(:child_role)    { create(:admin_role_with_sequenced_name, organization_id: parent_role.organization_id) }

    # Abilities. Model: Ability
    let(:site_ability)          { create(:site_ability) }
    let(:organization_ability)  { create(:organization_ability) }
    let(:user_ability)          { create(:user_ability) }
    let(:device_ability)        { create(:device_ability) }

    before do
      # Grandpa role abilities. Model: RoleAbility
      create(:role_ability, role: grandpa_role, ability: site_ability)
      create(:role_ability, role: grandpa_role, ability: organization_ability)

      # Parent role abilities. Model: RoleAbility
      create(:role_ability, role: parent_role, ability: site_ability)
      create(:role_ability, role: parent_role, ability: user_ability)

      # Child role abilities. Model: RoleAbility
      create(:role_ability, role: child_role, ability: site_ability)
      create(:role_ability, role: child_role, ability: user_ability)
      create(:role_ability, role: child_role, ability: device_ability)
    end

    # Permissions
    let(:grandpa_permission)  { create(:permission, role: grandpa_role) }
    let(:parent_permission)   { create(:permission, role: parent_role, parent: grandpa_permission) }
    let(:child_permission)    { create(:permission, role: child_role, parent: parent_permission) }

    describe '#allowed_abilities' do
      it "onlies return the intersection of own and ancestor's abilities" do
        # grandpa abilities
        expect(grandpa_permission.abilities.count).to be(2)
        expect(grandpa_permission.abilities).to include(site_ability)
        expect(grandpa_permission.abilities).to include(organization_ability)
        # parent abilities
        expect(parent_permission.abilities.count).to be(2)
        expect(parent_permission.abilities).to include(site_ability)
        expect(parent_permission.abilities).to include(user_ability)
        # child abilities
        expect(child_permission.abilities.count).to be(3)
        expect(child_permission.abilities).to include(site_ability)
        expect(child_permission.abilities).to include(user_ability)
        expect(child_permission.abilities).to include(device_ability)
        # child allowed abilities: intersection of all abilities in ancestry.
        expect(child_permission.allowed_abilities.count).to be(1)
        expect(child_permission.abilities).to include(site_ability)
      end
    end
  end

  describe '.all_by_manager' do
    context 'when nil is given' do
      it 'returns an empty arrray' do
        expect(described_class.all_by_manager(nil)).to eq []
      end
    end

    context 'when a manager is given' do
      before do
        @manager = create(:user)
        @manageable = create(:random_regular_organization)
        @role = @manager.organization.roles.create name: 'Test'
        described_class.create(manager: @manager.organization, manageable: @manageable, role: @role)
      end

      context 'when the manager has a permission' do
        before do
          @permission = described_class.create!(manager: @manager, manageable: @manageable, role: @role)
        end

        it 'returns the permission for the device' do
          expect(described_class.all_by_manager(@manager)).to eq [@permission]
        end
      end

      context 'when the manager does not have any permissions' do
        it 'returns an empty array' do
          expect(described_class.all_by_manager(@manager)).to eq []
        end
      end
    end
  end
end
