# frozen_string_literal: true

require 'rails_helper'

describe NetworkConfiguration do
  subject { create(:network_configuration) }

  describe 'intelliflow_enabled?' do
    it_behaves_like 'an inheritable configuration boolean', :intelliflow_enabled
  end

  describe 'ids_enabled?' do
    it_behaves_like 'an inheritable configuration boolean', :ids_enabled
  end
end
