# frozen_string_literal: true

require 'rails_helper'

# Tests the mixin located at app/concerns/subscriptions.rb.
describe Device do
  let(:device) { create_device }
  let(:uuids) { create_gateway_event_uuids }

  describe '#notifiable_subscriptions' do
    it 'returns an empty array if nil is passed' do
      subscriptions = device.notifiable_subscriptions(nil)
      expect(subscriptions).to be_empty
    end

    it 'returns an empty array for an invalid uuid' do
      subscriptions = device.notifiable_subscriptions(99_999)
      expect(subscriptions).to be_empty
    end

    it 'returns an empty array for a device that has no subscriptions' do
      subscriptions = device.notifiable_subscriptions(uuids.first)
      expect(subscriptions).to be_empty
    end

    it 'returns a list of subscriptions given a uuid' do
      create_subscriptions_for_gateway_device
      subscriptions = @device.notifiable_subscriptions(@uuids.first)
      expect(subscriptions).to eq @subscriptions
    end
  end
end
