# frozen_string_literal: true

require 'rails_helper'

describe Device do
  before do
    @device = create(:test_device)
  end

  it_behaves_like 'a base device class'
  it_behaves_like 'a monitorable device'
  it_behaves_like 'an identifiable device'
  it_behaves_like 'a searchable model'

  # Everything below this comment needs to be reworked

  describe 'custom validations' do
    it 'allows devices to have the same serial number' do
      device_1 = create(:test_device, serial: '6300010777201644')
      device_2 = create(:test_device, serial: '6300010777201644')
      expect(device_1).to be
      expect(device_2).to be
      expect(device_1).not_to eq device_2
    end

    context 'when a device is saved with a data plan within its organization' do
      let(:data_plan) { create(:data_plan, organization: @device.organization) }

      it 'saves the device' do
        @device.data_plan = data_plan
        @device.save
        expect(@device.errors.count).to eq 0
      end
    end

    context 'when a device is saved with a data plan outside its organization' do
      let(:organization) { create(:sub_organization) }
      let(:data_plan) { create(:data_plan, organization: organization) }

      it 'does not save the device' do
        @device.data_plan = data_plan
        @device.save
        expect(@device.errors.count).to eq 1
      end
    end
  end

  describe '#process_event' do
    let(:event) { Platforms::Event.new }

    it 'raises an exception' do
      expect { @device.process_event(event) }.to raise_error(NotImplementedError)
    end
  end

  describe '#heartbeat!' do
    context 'when the device has never had a heartbeat' do
      it 'activates the device' do
        expect(@device.last_heartbeat_at).to be nil
        expect(@device.activated_at).to be nil
        expect(@device.deployed?).to be(false)
        @device.heartbeat!
        expect(@device.last_heartbeat_at).to be_a(Time)
        expect(@device.activated_at).to be_a(Time)
        expect(@device.deployed?).to be(true)
      end
    end

    context 'when the device has had a heartbeat' do
      before do
        @device.touch :last_heartbeat_at
      end

      context 'when the device is undeployed' do
        before do
          @device.undeploy
        end

        it 'does not deploy the device if it has been activated' do
          @device.touch :activated_at
          expect(@device.deployed?).to be(false)
          @device.heartbeat!
          expect(@device.deployed?).to be(false)
        end
      end

      it 'does not touch activated_at' do
        old_time = Time.local(2013, 9, 1, 12, 0, 0)
        @device.update(activated_at: old_time)
        @device.heartbeat!
        expect(@device.activated_at).to eq old_time
      end
    end

    it 'caches the heartbeat' do
      expect(@device).to receive(:cache_heartbeat!)
      @device.heartbeat!
    end
  end

  describe '#cache_heartbeat!', torquebox: true do
    let(:heartbeat_cache) { Object.new          }
    let(:cache_info)      { { a: 'a', b: 'b' }  }

    before do
      expect(@device).to receive(:cache_info).and_return(cache_info)
    end

    it 'updates the cache with device information' do
      expect(HeartbeatCache).to receive(:put).with(@device.mac, cache_info)
      @device.__send__(:cache_heartbeat!)
    end
  end

  describe '#cache_info' do
    it "returns a hash with device's last heartbeat, heartbeat frequency and grace period values" do
      now = Time.now
      expect(@device).to receive(:last_heartbeat_at).and_return(now)
      expect(@device).to receive(:heartbeat_frequency).and_return(1)
      expect(@device).to receive(:grace_period).and_return(2)
      expect(@device.__send__(:cache_info)).to eq(
        last_heartbeat_at:    now,
        heartbeat_frequency:  1,
        grace_period:         2
      )
    end
  end

  describe '#down!' do
    it 'delegates a :down action to the state machine' do
      expect(@device.state_machine).to receive(:trigger).with(:down)
      @device.down!
    end
  end

  describe '.deployed' do
    it 'returns only deployed devices' do
      described_class.destroy_all
      create_list(:test_device, 5, :deployed)
      expect(described_class.deployed.count).to eq 5
    end
  end

  describe '.undeployed' do
    it 'returns only undeployed devices' do
      described_class.destroy_all
      create_list(:test_device, 5, :undeployed)
      expect(described_class.undeployed.count).to eq 5
    end
  end

  context 'when a device scope is passed' do
    before do
      create_list(:test_device, 3, :up)
      create_list(:test_device, 4, :down)
      create_list(:test_device, 5, :unreachable)
    end

    describe '.good' do
      it 'returns only devices considered to be up' do
        expect(described_class.good.count).to eq 3
      end
    end

    describe '.alert' do
      it 'returns only devices considered to be in alert status' do
        expect(described_class.alert.count).to eq 4
      end
    end

    describe '.unreachable' do
      it 'returns only unreachable devices' do
        expect(described_class.unreachable.count).to eq 5
      end
    end
  end

  describe '#deployed?' do
    it 'returns false if it is not deployed' do
      device = create(:test_device, :undeployed)
      expect(device.deployed?).to be(false)
    end

    it 'returns true if it is deployed' do
      device = create(:test_device, :deployed)
      expect(device.deployed?).to be(true)
    end
  end

  describe '.probes_for_current_minute' do
    it 'returns devices that should be probed this minute' do
      device = create(:test_device, :deployed)
      Timecop.freeze(Time.utc(1955, 11, 5, 4, 0, 0)) do |_time|
        device.probe_mask = device.send('mask_for_frequency_and_offset', 15, 0)
        device.probed_at = nil
        device.mgmt_host = '1.1.1.1'
        device.save!
        expect(described_class.probes_for_current_minute.count).to eq 1
      end
    end
  end

  describe '#mask_for_frequency_and_offset' do
    it 'returns a mask with the appropriate frequency and offset' do
      # Minute 0 is the left-most digit; minute 59 is the right-most.
      expect(@device.send('mask_for_frequency_and_offset', 5, 1))
        .to eq(0b0000100001000010000100001000010000100001000010000100001000010)
    end
  end

  describe '#mask_for_frequency' do
    it 'returns a mask with the appropriate frequency' do
      # Minute 0 is the left-most digit; minute 59 is the right-most.
      allow(Random).to receive(:rand).and_return 0, 1
      expect(@device.send('mask_for_frequency', 15))
        .to eq(0b000000000000001000000000000001000000000000001000000000000001)
      expect(@device.send('mask_for_frequency', 5))
        .to eq(0b0000100001000010000100001000010000100001000010000100001000010)
    end
  end

  describe '.set_category_and_model' do
    context 'when a category and model exists for the device' do
      before do
        @model = create(:test_model)
      end

      it 'updates the category and model' do
        @device.category_id = nil
        @device.model_id = nil
        @device.set_category_and_model
        expect(@device.series).to eq @model
        expect(@device.category).to eq @model.category
      end
    end
  end

  describe '#cellular_modem_supported?' do
    context 'when a device has a modem association' do
      let(:device) { create(:test_device_with_modem_association) }

      before do
        expect(device).to respond_to(:modem)
      end

      it 'returns true' do
        expect(device.cellular_modem_supported?).to be true
      end
    end

    context 'when a device does not have a modem association' do
      let(:device) { create(:test_device) }

      before do
        expect(device).not_to respond_to(:modem)
      end

      it 'returns false' do
        expect(device.cellular_modem_supported?).to be false
      end
    end
  end

  describe '#has_modem?' do
    context 'when a device has a modem' do
      let(:device) { create(:test_device_with_modem_association) }

      before do
        device.modems.first_or_create
        expect(device).to respond_to(:modem)
        expect(device.modem).to be_present
      end

      it 'returns true' do
        expect(device.has_modem?).to be true
      end
    end

    context 'when a device does not have a modem' do
      let(:device) { create(:test_device_with_modem_association) }

      before do
        expect(device).to respond_to(:modem)
        expect(device.modem).to be nil
      end

      it 'returns false' do
        expect(device.has_modem?).to be false
      end
    end

    context 'when a device does not have a modem association' do
      let(:device) { create(:test_device) }

      before do
        expect(device).not_to respond_to(:modem)
      end

      it 'returns false' do
        expect(device.has_modem?).to be false
      end
    end
  end

  context 'when a device does not yet have a heartbeat within its lifetime.' do
    before do
      @test_time = Time.now
      @device = create(:test_device, :down)
      Timecop.freeze(@test_time - (@device.lifetime + 1)) do
        @device.heartbeat!
      end
    end

    describe '.lifetime_expired?' do
      it 'is expired when there is not a new heartbeat' do
        expect(@device.lifetime_expired?).to eq true
      end
      it 'is not expired when there is a new heartbeat' do
        @device.heartbeat!
        expect(@device.lifetime_expired?).to eq false
      end
    end

    describe '.lifetime_expired_at?' do
      it "is expired at a time greater than the device's lifetime past the heartbeat" do
        expect(@device.lifetime_expired_at?(@test_time + 2)).to eq true
      end
      it "is not expired at a time less than the device's lifetime past the heartbeat" do
        expect(@device.lifetime_expired_at?(@test_time - 2)).to eq false
      end
    end
  end

  describe '#category_name' do
    context 'when the device does not have a category' do
      before do
        @device.category = nil
      end

      it 'returns nil' do
        expect(@device.category).not_to be_present
        expect(@device.category_name).to be nil
      end
    end

    context 'when the device has a category' do
      before do
        @device.category = DeviceCategory.new(name: 'Test')
      end

      it 'returns the category name' do
        expect(@device.category).to be_present
        expect(@device.category_name).to eq 'Test'
      end
    end
  end

  describe '#check_site' do
    context 'when the device has an organization' do
      context 'when the site is not within the device organizations scope' do
        before do
          @default_site = @device.organization.default_site
          @site = create(:site, organization: create(:random_regular_organization))
          expect(@site.organization).not_to eq @device.organization
        end

        it 'sets the site to the organizations default' do
          @device.site = @site
          @device.send(:check_site)
          expect(@device.site).to eq @default_site
        end
      end

      context 'when the site is within the devices organization scope' do
        before do
          @site = @device.organization.sites.create name: 'Test'
        end

        it 'keeps the site' do
          @device.site = @site
          @device.send(:check_site)
          expect(@device.site).to eq @site
        end
      end
    end

    context 'when the device does not have an organization' do
      before do
        @device.organization = nil
      end

      it 'sets the site id to nil' do
        @device.site = create(:site)
        @device.send(:check_site)
        expect(@device.site).to be nil
      end
    end
  end

  describe '#check_configuration_association' do
    context 'when the device changes configurations' do
      let(:device) { create(:remote_manager_device) }
      let(:new_configuration) { create(:device_configuration) }

      it 'changes the configuration_associated_at field' do
        expect { device.update_attribute(:configuration, new_configuration) }.to change(device, :configuration_associated_at)
      end
    end

    context 'when the device removes the configuration' do
      let(:configuration) { create(:device_configuration) }
      let(:device) { create(:remote_manager_device, configuration: configuration) }

      it 'sets the configuration_associated_at field to nil' do
        expect { device.update_attribute(:configuration, nil) }.to change(device, :configuration_associated_at)
        expect(device.configuration_associated_at).to eq nil
      end
    end

    context 'when the device does not change the configuration' do
      let(:configuration) { create(:device_configuration) }
      let(:device) { create(:remote_manager_device, configuration: configuration) }

      it 'does not change the configuration_associated_at field' do
        expect { device.save }.not_to change(device, :configuration_associated_at)
      end
    end
  end

  describe '#default_status_freq' do
    it 'returns the default status frequency from the settings converted to seconds' do
      allow(Settings).to receive_message_chain(:devices, :default_status_freq).and_return('1m')
      expect(subject.__send__(:default_status_freq)).to eq('1m')

      allow(Settings).to receive_message_chain(:devices, :default_status_freq).and_return('1h')
      expect(subject.__send__(:default_status_freq)).to eq('1h')
    end
  end

  describe '#default_grace_period_duration' do
    it 'returns the default grace period duration from the settings converted to seconds' do
      allow(Settings).to receive_message_chain(:devices, :default_grace_period_duration).and_return('1m')
      expect(subject.__send__(:default_grace_period_duration)).to eq('1m')

      allow(Settings).to receive_message_chain(:devices, :default_grace_period_duration).and_return('1h')
      expect(subject.__send__(:default_grace_period_duration)).to eq('1h')
    end
  end

  describe '#service_name' do
    let(:organization) { Organization.new }

    before do
      allow(subject).to receive(:organization).and_return(organization)
    end

    it 'delegates to its organization' do
      expect(organization).to receive(:service_name)
      subject.service_name
    end
  end

  describe 'scopes' do
    describe '.with_categories' do
      before do
        @device_1 = create(:test_device, :with_sequenced_category)
        @device_2 = create(:test_device, :with_sequenced_category)
        @device_3 = create(:test_device, :with_sequenced_category)
      end

      context 'when given a category' do
        let(:result) { described_class.with_categories @device_1.category.id }

        it 'includes devices with the given category' do
          expect(result).to include @device_1
          expect(result).not_to include @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when given multiple categories' do
        let(:result) { described_class.with_categories [@device_1.category.id, @device_2.category.id] }

        it 'includes devices with one of the given categories' do
          expect(result).to include @device_1
          expect(result).to include @device_2
          expect(result).not_to include @device_3
        end
      end
    end

    describe '.with_models' do
      before do
        @device_1 = create(:test_device, :model_8100)
        @device_2 = create(:test_device, :model_5300_dc)
        @device_3 = create(:test_device, :model_5400_rm)
      end

      context 'when given a model' do
        let(:result) { described_class.with_models @device_1.series.id }

        it 'includes devices with the given model' do
          expect(result).to include @device_1
          expect(result).not_to include @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when given multiple models' do
        let(:result) { described_class.with_models [@device_1.series.id, @device_3.series.id] }

        it 'includes devices with one of the given models' do
          expect(result).to include @device_1
          expect(result).to include @device_3
          expect(result).not_to include @device_2
        end
      end
    end

    describe '.with_status' do
      before do
        @device_1 = create(:test_device, :up)
        @device_2 = create(:test_device, :down)
        @device_3 = create(:test_device, :undeployed)
      end

      context 'when given an UP status' do
        let(:result) { described_class.with_status @device_1.status }

        it 'includes devices with the given status' do
          expect(result).to include @device_1
          expect(result).not_to include @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when given a DOWN status' do
        let(:result) { described_class.with_status @device_2.status }

        it 'includes devices with the given status' do
          expect(result).to include @device_2
          expect(result).not_to include @device_1
          expect(result).not_to include @device_3
        end
      end

      context 'when given an invalid status' do
        let(:result) { described_class.with_status @device_3.status }

        it 'includes all devices' do
          expect(result).to include @device_1
          expect(result).to include @device_2
          expect(result).to include @device_3
        end
      end
    end

    describe '.with_deployment' do
      before do
        @device_1 = create(:test_device, :up)
        @device_2 = create(:test_device, :down)
        @device_3 = create(:test_device, :undeployed)
      end

      context 'when given deployed' do
        let(:result) { described_class.with_deployment Devices::Deployment::DEPLOYED }

        it 'includes deployed devices' do
          expect(result).to include @device_1
          expect(result).to include @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when given undeployed' do
        let(:result) { described_class.with_deployment Devices::Deployment::UNDEPLOYED }

        it 'includes undeployed devices' do
          expect(result).to include @device_3
          expect(result).not_to include @device_1
          expect(result).not_to include @device_2
        end
      end

      context 'when given all deployments' do
        let(:result) { described_class.with_deployment Devices::Deployment::ALL }

        it 'includes all devices' do
          expect(result).to include @device_1
          expect(result).to include @device_2
          expect(result).to include @device_3
        end
      end
    end

    describe '.with_organization' do
      before do
        @device_1 = create(:test_device, :with_random_regular_organization_site)
        @device_2 = create(:test_device, :with_random_regular_organization_site)
        @device_3 = create(:test_device, :with_sub_organization_site)
      end

      context 'when given an organization with hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @device.organization.id, include_hierarchy: 'true') }

        it 'includes devices within suborganizations' do
          expect(result).not_to include @device_1
          expect(result).not_to include @device_2
          expect(result).to include @device_3
        end
      end

      context 'when given an organization without hierarchy' do
        let(:result) { described_class.with_organization(organization_id: @device_1.organization.id, include_hierarchy: 'false') }

        it 'returns devices without suborganizations' do
          expect(result).to include @device_1
          expect(result).not_to include @device_2
          expect(result).not_to include @device_3
        end
      end
    end

    describe '.with_mac' do
      before do
        @device_1 = create(:test_device, :with_random_regular_organization_site)
        create(:test_device, :with_random_regular_organization_site)
      end

      context 'when given a mac' do
        let(:result) { described_class.with_mac @device_1.mac }

        it 'returns only the Device with a matching mac' do
          expect(result.size).to eq(1)
          expect(result).to include @device_1
        end
      end
    end

    describe '.with_modem' do
      before do
        @device_1 = create(:test_device_with_modem_association)
        create(:device_modem, device: @device_1)
        create(:test_device, :with_random_regular_organization_site)
      end

      let(:result) { described_class.with_modem }

      it 'returns only the Device with a modem' do
        expect(result.size).to eq 1
        expect(result).to include @device_1
      end
    end

    describe '.with_network' do
      before do
        @device_4G = create(:test_device_with_modem_association, :deployed)
        create(:device_modem, device: @device_4G, cnti: DeviceModem::FOUR_G_NETWORK.first)

        @device_3G = create(:test_device_with_modem_association, :deployed)
        create(:device_modem, device: @device_3G, cnti: DeviceModem::THREE_G_NETWORK.first)

        @device_2G = create(:test_device_with_modem_association, :deployed)
        create(:device_modem, device: @device_2G, cnti: DeviceModem::TWO_G_NETWORK.first)

        @device_unknown = create(:test_device_with_modem_association, :deployed)
        create(:device_modem, device: @device_unknown, cnti: 'garbage')
      end

      context 'when given \'4G\'' do
        let(:result) { described_class.with_network('4G') }

        it 'returns only the Device with a cnti in the 4G network' do
          expect(result.size).to eq 1
          expect(result).to include @device_4G
        end
      end

      context 'when given \'3G\'' do
        let(:result) { described_class.with_network('3G') }

        it 'returns only the Device with a cnti in the 3G network' do
          expect(result.size).to eq 1
          expect(result).to include @device_3G
        end
      end

      context 'when given \'2G\'' do
        let(:result) { described_class.with_network('2G') }

        it 'returns only the Device with a cnti in the 2G network' do
          expect(result.size).to eq 1
          expect(result).to include @device_2G
        end
      end

      context 'when given \'unknown\'' do
        let(:result) { described_class.with_network('unknown') }

        it 'returns only the Device with a cnti with an unknown network' do
          expect(result.size).to eq 1
          expect(result).to include @device_unknown
        end
      end
    end

    describe '.sort_by_device_states_state' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :up)
        @device_2 = create(:test_device, :down)
        @device_3 = create(:test_device, :undeployed)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_device_states_state 'asc' }

        it 'sorts up devices first, down second, and non deployed devices last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_device_states_state 'desc' }

        it 'sorts down devices first, up second, and non deployed devices last' do
          expect(result.first).to eq @device_2
          expect(result.second).to eq @device_1
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_devices_mac' do
      before do
        @device.destroy
        @device_1 = create(:test_device)
        @device_2 = create(:test_device)
        @device_3 = create(:test_device)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_devices_mac 'asc' }

        it 'sorts lower mac addresses first, and higher addresses last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_devices_mac 'desc' }

        it 'sorts higher mac addresses first, and lower addresses last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_devices_host' do
      before do
        @device.destroy
        @device_1 = create(:test_device, host: '192.168.1.1')
        @device_2 = create(:test_device, host: '192.168.1.2')
        @device_3 = create(:test_device, host: '192.168.1.3')
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_devices_host 'asc' }

        it 'sorts lower hosts first, and higher hosts last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_devices_host 'desc' }

        it 'sorts higher hosts first, and lower hosts last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_devices_hostname' do
      before do
        @device.destroy
        @device_1 = create(:test_device, hostname: 'a')
        @device_2 = create(:test_device, hostname: 'b')
        @device_3 = create(:test_device, hostname: 'c')
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_devices_hostname 'asc' }

        it 'sorts lower hostnames first, and higher hostnames last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_devices_hostname 'desc' }

        it 'sorts higher hostnames first, and lower hostnames last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_device_categories_name' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :with_sequenced_category)
        @device_2 = create(:test_device, :with_sequenced_category)
        @device_3 = create(:test_device, :with_sequenced_category)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_device_categories_name 'asc' }

        it 'sorts lower category names first, and higher category names last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_device_categories_name 'desc' }

        it 'sorts higher category names first, and lower category names last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_device_models_name' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :model_8100)
        @device_2 = create(:test_device, :model_5300_dc)
        @device_3 = create(:test_device, :model_5400_rm)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_device_models_name 'asc' }

        it 'sorts lower model names first, and higher model names last' do
          expect(result.first).to eq @device_2
          expect(result.second).to eq @device_3
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_device_models_name 'desc' }

        it 'sorts higher model names first, and lower model names last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_3
          expect(result.last).to eq @device_2
        end
      end
    end

    describe '.sort_by_organizations_name' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :with_random_regular_organization_site)
        @device_2 = create(:test_device, :with_random_regular_organization_site)
        @device_3 = create(:test_device, :with_random_regular_organization_site)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_organizations_name 'asc' }

        it 'sorts lower organization names first, and higher organization names last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_organizations_name 'desc' }

        it 'sorts higher organization names first, and lower organization names last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_sites_name_ascending' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :with_random_regular_organization_site)
        @device_2 = create(:test_device, :with_random_regular_organization_site)
        @device_3 = create(:test_device, :with_random_regular_organization_site)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_sites_name 'asc' }

        it 'sorts lower site names first, and higher site names last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_sites_name 'desc' }

        it 'sorts higher site names first, and lower site names last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_devices_firmware' do
      before do
        @device.destroy
        @device_1 = create(:test_device, firmware: 1)
        @device_2 = create(:test_device, firmware: 2)
        @device_3 = create(:test_device, firmware: 3)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_devices_firmware 'asc' }

        it 'sorts lower firmware first, and higher firmware last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_devices_firmware 'desc' }

        it 'sorts higher firmware first, and lower firmware last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_countries_name' do
      before do
        @device.destroy
        @device_1 = create(:test_device, :with_random_location)
        @device_2 = create(:test_device, :with_random_location)
        @device_3 = create(:test_device, :with_random_location)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_countries_name 'asc' }

        it 'sorts lower country names first, and higher country names last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_countries_name 'desc' }

        it 'sorts higher country names first, and lower country names last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end
    end

    describe '.sort_by_devices_last_heartbeat_at' do
      before do
        @device.destroy
        @device_1 = create(:test_device, last_heartbeat_at: 1.day.ago)
        @device_2 = create(:test_device, last_heartbeat_at: 2.days.ago)
        @device_3 = create(:test_device, last_heartbeat_at: 3.days.ago)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_devices_last_heartbeat_at 'asc' }

        it 'sorts later heartbeats first, and earlier heartbeats last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_devices_last_heartbeat_at 'desc' }

        it 'sorts later heartbeats last, and earlier heartbeats first' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_device_modems_data_used' do
      before do
        @device.destroy
        @device_1 = create(:test_device)
        @modem_1 = create(:device_modem, device: @device_1, data_used: 10)
        @device_2 = create(:test_device)
        @modem_2 = create(:device_modem, device: @device_2, data_used: 1)
        @device_3 = create(:test_device)
      end

      context 'when sorted ascending' do
        let(:result) { described_class.sort_by_device_modems_data_used 'asc' }

        it 'sorts lower data used first, and higher data used last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { described_class.sort_by_device_modems_data_used 'desc' }

        it 'sorts higher data used first, and lower data used last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      context 'when searching multiple categories, within user organization and sorting by country name ascending' do
        before do
          @device_1 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @device_2 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @device_3 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @user = create(:user_with_org_permission)
          @user.organization.children << @device_1.organization
          @user.organization.children << @device_2.organization
          @user.organization.children << @device_3.organization
        end

        let(:result) do
          @user.devices.search(
            with_categories: [@device_1.category.id, @device_2.category.id],
            sort_by_countries_name: 'asc'
          )
        end

        it 'returns the devices within the user organization with the given categories in the correct order' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when searching by multiple models and status' do
        before do
          @device_1 = create(:test_device, :model_8100, :up)
          @device_2 = create(:test_device, :model_5300_dc, :down)
          @device_3 = create(:test_device, :model_5400_rm, :undeployed)
        end

        let(:result) do
          described_class.search(with_models: [@device_1.series.id, @device_2.series.id],
                                 with_status: @device_2.status)
        end

        it 'returns the devices with the given models and the given status' do
          expect(result).to include @device_2
          expect(result).not_to include @device_1
          expect(result).not_to include @device_3
        end
      end

      context 'when searching by deployment and sorting by site name' do
        before do
          @device_1 = create(:test_device, :with_random_regular_organization_site, :up)
          @device_2 = create(:test_device, :with_random_regular_organization_site, :down)
          @device_3 = create(:test_device, :with_random_regular_organization_site, :undeployed)
        end

        let(:result) do
          described_class.search(with_deployment: Devices::Deployment::DEPLOYED,
                                 sort_by_sites_name: 'asc')
        end

        it 'returns the devices with the given deployment in the correct order' do
          expect(result.first).to eq @device_1
          expect(result.last).to eq @device_2
          expect(result).not_to include @device_3
        end
      end

      context 'when searching all devices' do
        before do
          @device_1 = create(:test_device)
          @device_2 = create(:test_device)
          @device_3 = create(:test_device)
        end

        let(:result) { described_class.search(all: nil) }

        it 'returns all devices' do
          expect(result).to include @device_1
          expect(result).to include @device_2
          expect(result).to include @device_3
        end
      end
    end

    describe '.search!' do
      context 'searching multiple categories, within user organization and sorting by country name descending' do
        before do
          @device_1 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @device_2 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @device_3 = create(:test_device, :with_sequenced_category, :with_random_regular_organization_site, :with_random_location)
          @user = create(:user_with_org_permission)
          @user.organization.children << @device_1.organization
          @user.organization.children << @device_2.organization
          @user.organization.children << @device_3.organization
        end

        let(:result) do
          @user.devices.search!(
            with_categories: [@device_1.category.id, @device_2.category.id],
            sort_by_countries_name: 'desc'
          )
        end

        it 'returns devices within the given user organization with the given categories in the correct order' do
          expect(result.first).to eq @device_2
          expect(result.second).to eq @device_1
          expect(result).not_to include @device_3
        end
      end
    end
  end
end
