# frozen_string_literal: true

require 'rails_helper'

describe Configuration do
  describe 'inherited_data' do
    context 'when a single level hierarchy exists' do
      let(:parent_config) { create(:configuration, data: parent_data) }
      let(:child_config) { create(:configuration, data: child_data, parent_id: parent_config.id) }
      let(:result) { child_config.inherited_data }

      context 'when there are no duplicate keys' do
        let(:parent_data) { { key_1: 'parent value' } }
        let(:child_data) { { key_2: 'child value' } }
        let(:expected_result) { { key_1: 'parent value', key_2: 'child value' }.with_indifferent_access }

        it 'combines the keys into a big hash' do
          expect(result).to eq expected_result
        end
      end

      context 'when there are duplicate nested keys' do
        let(:parent_data) { { key_1: { foo: ['parent value'], bar: 'baz 1' }, key_2: 'foo' } }
        let(:child_data) { { key_1: { foo: ['child value'] } } }
        let(:expected_result) { { key_1: { foo: ['child value'], bar: 'baz 1' }, key_2: 'foo' }.with_indifferent_access }

        it 'takes the child nested value if it exists and the parent value when it does not exist in the child' do
          expect(result).to eq expected_result
        end
      end

      context 'when the duplicate nested key value is blank in the child' do
        let(:parent_data) { { key_1: { foo: ['parent value'], bar: 'baz 1' }, key_2: 'foo' } }
        let(:child_data) { { key_1: { foo: ['child value'], bar: '' } } }
        let(:expected_result) { { key_1: { foo: ['child value'], bar: 'baz 1' }, key_2: 'foo' }.with_indifferent_access }

        it 'takes the parent value' do
          expect(result).to eq expected_result
        end
      end
    end
  end

  describe 'parent_inherited_data' do
    context 'when the device has a model and system that have different configuration data' do
      let(:system_config) { create(:configuration, data: system_data) }
      let(:model_config) { create(:configuration, data: model_data, parent_id: system_config.id) }
      let(:device_config) { create(:configuration, parent_id: model_config.id) }

      let(:system_data) { json_fixture('configurations/system_data.json') }
      let(:model_data) { json_fixture('configurations/group_data.json') }
      let(:expected_result) { json_fixture('configurations/merged_data.json') }
      let(:result) { device_config.parent_inherited_data }

      it 'returns the merged data of the model and system for the device' do
        expect(result).to eq expected_result
      end
    end
  end
end
