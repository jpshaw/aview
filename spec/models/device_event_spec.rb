# frozen_string_literal: true

require 'rails_helper'

describe DeviceEvent do
  it_behaves_like 'a searchable model'

  describe '#port_number_from_info' do
    let(:port_number) { '2' }

    context 'with port number info' do
      before do
        subject.information = "Serial Port #{port_number}: ..."
      end

      it 'returns the port number' do
        expect(subject.port_number_from_info).to eq port_number
      end
    end

    context 'with no port number info' do
      before do
        subject.information = Faker::Lorem.sentence
      end

      it 'returns nil' do
        expect(subject.port_number_from_info).to be nil
      end
    end
  end

  describe '#sysloggable_event?' do
    before do
      subject.device_id = 1
    end

    context "when aots syslog is enabled and the event's UUID is sysloggable" do
      before do
        allow(Settings).to receive_message_chain(:aots, :enabled?).and_return(true)
        allow(subject).to receive_message_chain(:uuid, :code).and_return EventUUIDS::SYSLOGGABLE.first
      end

      it 'returns true' do
        expect(subject.sysloggable_event?).to be true
      end
    end

    context 'when aots syslog is not enabled' do
      before do
        allow(Settings).to receive_message_chain(:aots, :enabled?).and_return(false)
        allow(subject).to receive_message_chain(:uuid, :code).and_return EventUUIDS::SYSLOGGABLE.first
      end

      it 'returns false' do
        expect(subject.sysloggable_event?).to be false
      end
    end

    context "when the event's UUID is not sysloggable" do
      let(:invalid_uuid) { -99_999 }

      before do
        allow(Settings).to receive_message_chain(:aots, :enabled?).and_return(true)
        allow(subject).to receive_message_chain(:uuid, :code).and_return invalid_uuid
      end

      it 'returns false' do
        expect(subject.sysloggable_event?).to be false
      end
    end
  end

  describe 'Callbacks' do
    describe 'after_commit' do
      before do
        subject.device_id = 1
      end

      describe '#queue_syslog_event' do
        context 'when the event is sysloggable' do
          before do
            allow(subject).to receive(:sysloggable_event?).and_return(true)
          end

          it 'runs' do
            expect(subject).to receive(:queue_syslog_event)
            subject.save!
          end
        end

        context 'when the event is not sysloggable' do
          before do
            allow(subject).to receive(:sysloggable_event?).and_return(false)
          end

          it 'does not run' do
            expect(subject).not_to receive(:queue_syslog_event)
            subject.save!
          end
        end
      end
    end
  end

  describe 'scopes' do
    describe '.with_type' do
      before do
        @event_1 = create(:event, event_type: EventTypes::CONFIG)
        @event_2 = create(:event, event_type: EventTypes::REBOOT)
        @event_3 = create(:event, event_type: EventTypes::CONFIG)
      end

      context 'when given a type' do
        let(:result) { described_class.with_type EventTypes::CONFIG }

        it 'includes events with the given type' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).to include @event_3
        end
      end
    end

    describe '.with_level' do
      before do
        @event_1 = create(:event, level: EventLevels::NOTICE)
        @event_2 = create(:event, level: EventLevels::ALERT)
        @event_3 = create(:event, level: EventLevels::INFO)
      end

      context 'when given a level' do
        let(:result) { described_class.with_level EventLevels::ALERT }

        it 'includes events with the given level' do
          expect(result).not_to include @event_1
          expect(result).to include @event_2
          expect(result).not_to include @event_3
        end
      end

      context 'when given multiple levels' do
        let(:result) { described_class.with_level [EventLevels::INFO, EventLevels::NOTICE] }

        it 'includes events with the given level' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).to include @event_3
        end
      end
    end

    describe '.with_device_categories' do
      before do
        @event_1 = create(:event, :with_sequenced_category)
        @event_2 = create(:event, :with_sequenced_category)
        @event_3 = create(:event, :with_sequenced_category)
      end

      context 'when given a category' do
        let(:result) { described_class.with_device_categories @event_1.device.category.id }

        it 'includes events with the given device category' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).not_to include @event_3
        end
      end

      context 'when given multiple categories' do
        let(:result) { described_class.with_device_categories [@event_1.device.category.id, @event_2.device.category.id] }

        it 'includes events with one of the given device categories' do
          expect(result).to include @event_1
          expect(result).to include @event_2
          expect(result).not_to include @event_3
        end
      end
    end

    describe '.with_device_models' do
      before do
        @event_1 = create(:event, :for_model_8100)
        @event_2 = create(:event, :for_model_5300_dc)
        @event_3 = create(:event, :for_model_5400_rm)
      end

      context 'when given a model' do
        let(:result) { described_class.with_device_models @event_1.device.series.id }

        it 'includes events with the given device model' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).not_to include @event_3
        end
      end

      context 'when given multiple models' do
        let(:result) { described_class.with_device_models [@event_1.device.series.id, @event_2.device.series.id] }

        it 'includes events with one of the given device models' do
          expect(result).to include @event_1
          expect(result).to include @event_2
          expect(result).not_to include @event_3
        end
      end
    end

    describe '.with_device_organization' do
      before do
        @event_1 = create(:event)
        @event_2 = create(:event)
        @event_3 = create(:event, :with_sub_organization)
      end

      context 'when given an organization with hierarchy' do
        let(:result) do
          described_class.with_device_organization(organization_id: @event_1.device.organization.id,
                                                   include_hierarchy: 'true')
        end

        it 'includes events with devices within sub organizations' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).to include @event_3
        end
      end

      context 'when given an organization without hierarchy' do
        let(:result) do
          described_class.with_device_organization(organization_id: @event_1.device.organization.id,
                                                   include_hierarchy: 'false')
        end

        it 'includes events with devices without sub organizations' do
          expect(result).to include @event_1
          expect(result).not_to include @event_2
          expect(result).not_to include @event_3
        end
      end
    end

    describe '.created_since' do
      before do
        @event_1 = create(:event, created_at: 1.month.ago)
        @event_2 = create(:event, created_at: 1.week.ago)
        @event_3 = create(:event, created_at: 1.day.ago)
      end

      context 'when given a date' do
        let(:result) { described_class.created_since 2.weeks.ago }

        it 'includes events created since the given dates' do
          expect(result).not_to include @event_1
          expect(result).to include @event_2
          expect(result).to include @event_3
        end
      end
    end

    describe '.sort_by_created_at' do
      before do
        @event_1 = create(:event, created_at: 1.month.ago)
        @event_2 = create(:event, created_at: 1.week.ago)
        @event_3 = create(:event, created_at: 1.day.ago)
      end

      context 'when sorting asc' do
        let(:result) { described_class.sort_by_created_at 'asc' }

        it 'sorts events created earlier first and events created later last' do
          expect(result.first).to eq @event_1
          expect(result.second).to eq @event_2
          expect(result.last).to eq @event_3
        end
      end

      context 'when sorting desc' do
        let(:result) { described_class.sort_by_created_at 'desc' }

        it 'sorts events created later first and events created earlier last' do
          expect(result.first).to eq @event_3
          expect(result.second).to eq @event_2
          expect(result.last).to eq @event_1
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      before do
        @event_1 = create(:event, level: EventLevels::NOTICE, created_at: 1.month.ago)
        @event_2 = create(:event, level: EventLevels::ALERT, created_at: 1.week.ago)
        @event_3 = create(:event, level: EventLevels::INFO, created_at: 1.day.ago)
      end

      context 'when searching a given level and created since' do
        let(:result) do
          described_class.search(with_level: [EventLevels::INFO, EventLevels::NOTICE],
                                 created_since: 2.weeks.ago)
        end

        it 'returns events with the given level created since the given date' do
          expect(result).not_to include @event_1
          expect(result).not_to include @event_2
          expect(result).to include @event_3
        end
      end

      context 'when searching all' do
        let(:result) { described_class.search(all: nil) }

        it 'returns all events' do
          expect(result).to include @event_1
          expect(result).to include @event_2
          expect(result).to include @event_3
        end
      end
    end

    describe '.search!' do
      before do
        @event_1 = create(:event, event_type: EventTypes::CONFIG, created_at: 1.month.ago)
        @event_2 = create(:event, event_type: EventTypes::REBOOT, created_at: 1.week.ago)
        @event_3 = create(:event, event_type: EventTypes::CONFIG, created_at: 1.day.ago)
      end

      context 'when searching a given type and sorting by created at' do
        let(:result) do
          described_class.search(with_type: EventTypes::CONFIG,
                                 sort_by_created_at: 'desc')
        end

        it 'returns events with a given type sorted in the given direction' do
          expect(result.first).to eq @event_3
          expect(result).not_to include @event_2
          expect(result.last).to eq @event_1
        end
      end
    end
  end
end
