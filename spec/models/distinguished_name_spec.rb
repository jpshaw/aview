# frozen_string_literal: true

require 'rails_helper'

describe DistinguishedName do
  subject { described_class.new(attributes) }

  let(:attributes) do
    {
      'CN' => '00270427c04a',
      'OU' => 'hardware',
      'O'  => 'Accelerated Concepts',
      'L'  => 'Tampa',
      'ST' => 'Florida',
      'C'  => 'US'
    }
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(subject.to_h).to be_a Hash
    end

    it 'returns a hash that matches the input' do
      expect(subject.to_h).to eq attributes
    end
  end

  describe '#to_s' do
    it 'returns a string' do
      expect(subject.to_s).to be_a String
    end

    it 'returns the attributes separated by comma' do
      expect(subject.to_s).to include(',')
    end
  end

  describe '#common_name_matches?' do
    let(:name) { attributes['CN'] }

    it 'returns false when given nothing' do
      expect(subject.common_name_matches?).to be false
    end

    it 'returns false when given nil' do
      expect(subject.common_name_matches?(nil)).to be false
    end

    it 'returns false when given a blank string' do
      expect(subject.common_name_matches?('')).to be false
    end

    context 'with a case-sensitive matching name' do
      it 'returns true' do
        expect(subject.common_name).to eq name
        expect(subject.common_name_matches?(name)).to be true
      end
    end

    context 'with a case-insensitive matching name' do
      let(:name) { attributes['CN'].upcase }

      it 'returns true' do
        expect(subject.common_name).not_to eq name
        expect(subject.common_name_matches?(name.upcase)).to be true
      end
    end

    context 'with multiple names' do
      let(:names) { [name, 'hello', 'world'] }

      it 'does not raise an error' do
        expect do
          subject.common_name_matches?(*names)
        end.not_to raise_error
      end

      it 'returns true if any of the names match' do
        expect(subject.common_name_matches?(*names)).to be true
      end
    end

    context 'when common name is nil' do
      before do
        allow(subject).to receive(:common_name).and_return(nil)
      end

      it 'returns false' do
        expect(subject.common_name_matches?(name)).to be false
      end
    end
  end

  describe '.parse' do
    subject { described_class }

    it 'returns a DistinguishedName object when given nil' do
      expect(subject.parse(nil)).to be_a described_class
    end

    it 'returns a DistinguishedName object when given a blank string' do
      expect(subject.parse('')).to be_a described_class
    end

    context 'when a distinguished name string is provided' do
      shared_examples_for 'a parsed DistinguishedName object' do
        let(:distinguished_name) { subject.parse(distinguished_name_string) }

        it 'assigns the attributes' do
          expect(distinguished_name.common_name).to eq '00270427c04a'
          expect(distinguished_name.organization).to eq 'Accelerated Concepts'
          expect(distinguished_name.organizational_unit).to eq 'hardware'
          expect(distinguished_name.locality).to eq 'Tampa'
          expect(distinguished_name.state).to eq 'Florida'
          expect(distinguished_name.country).to eq 'US'
        end
      end

      context 'with commas' do
        it_behaves_like 'a parsed DistinguishedName object' do
          let(:distinguished_name_string) { 'CN=00270427c04a,OU=hardware,O=Accelerated Concepts,L=Tampa,ST=Florida,C=US' }
        end
      end

      context 'with forward-slashes' do
        it_behaves_like 'a parsed DistinguishedName object' do
          let(:distinguished_name_string) { 'CN=00270427c04a/OU=hardware/O=Accelerated Concepts/L=Tampa/ST=Florida/C=US' }
        end
      end
    end
  end
end
