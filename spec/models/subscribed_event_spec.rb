# frozen_string_literal: true

require 'rails_helper'

describe SubscribedEvent do
  describe 'methods' do
    let(:event) { create(:subscribed_event) }

    describe '#immediate?' do
      it 'returns true if the frequency is set to immediate' do
        event.frequency = NotificationProfile::Frequencies::IMMEDIATELY
        expect(event.immediate?).to be true
      end

      it 'returns false if the frequency is nil' do
        event.frequency = nil
        expect(event.immediate?).to be false
      end
    end

    describe '#never?' do
      it 'returns true if the frequency is set to never' do
        event.frequency = NotificationProfile::Frequencies::NEVER
        expect(event.never?).to be true
      end

      it 'returns false if the frequency is nil' do
        event.frequency = nil
        expect(event.never?).to be false
      end
    end
  end
end
