# frozen_string_literal: true

require 'rails_helper'

describe Notification do
  it_behaves_like 'a searchable model'

  describe 'methods' do
    let(:notification) { create_immediate_notification_for_device_subscription }

    describe '#delivered!' do
      it 'marks a notification as delivered if not already' do
        notification.delivered_at = nil
        expect(notification.delivered?).to be false
        notification.delivered!
        expect(notification.delivered?).to be true
      end

      it 'does nothing if the notification is already delivered' do
        timestamp = Time.now.utc
        notification.delivered_at = timestamp
        expect(notification.delivered?).to be true
        notification.delivered!
        expect(notification.delivered_at).to eq timestamp
      end
    end

    describe '#deliver_if_immediate' do
      it 'is called after a notification is created' do
        create_subscriptions_for_gateway_device
        event = create_device_event
        notification = @device_subscription.notifications.new(user_id: @user.id, event_id: event.id)
        expect(notification).to receive(:deliver_if_immediate!)
        notification.save!
      end
    end

    describe '.notifiable' do
      it 'returns an array of notifications available to send out' do
        notification = create_immediate_notification_for_device_subscription
        notifications = described_class.notifiable
        expect(notifications).to eq [notification]
      end
    end

    describe '.available_for_minute_of_day' do
      it 'returns an empty array if passed nil' do
        create_immediate_notification_for_device_subscription
        notifications = described_class.available_for_minute_of_day(nil)
        expect(notifications).to be_empty
      end

      it 'returns an empty array if anything but an integer is passed' do
        create_immediate_notification_for_device_subscription
        notifications = described_class.available_for_minute_of_day('hello world')
        expect(notifications).to be_empty
      end

      it 'returns an empty array if a number less than zero is passed' do
        create_immediate_notification_for_device_subscription
        notifications = described_class.available_for_minute_of_day(-1)
        expect(notifications).to be_empty
      end

      it 'returns an empty array if a number greater than 1425 (11:45 PM)' do
        create_immediate_notification_for_device_subscription
        notifications = described_class.available_for_minute_of_day(1426)
        expect(notifications).to be_empty
      end

      it 'returns notifications for a valid minute of the day' do
        user = create(:user)
        device = create_device
        profile = create_profile_for_hourly_gateway_notifications
        uuid = @uuids.first
        subscription = create(:device_subscription, user: @user, publisher_id: device.id, profile: profile)
        notification = subscription.notifications.create(uuid_id: uuid.id, user_id: user.id)
        notifications = described_class.available_for_minute_of_day(60)
        expect(notifications).to be_present
        hash = { subscription => [notification] }
        expect(notifications).to eq hash
      end

      it 'does not retrieve any immediate notifications' do
        user   = create(:user)
        device = create(:gateway_device)
        create_gateway_event_uuids

        up_uuid   = DeviceEventUUID.find_by(code: EventUUIDS::GATEWAY_UP)
        down_uuid = DeviceEventUUID.find_by(code: EventUUIDS::GATEWAY_DOWN)

        profile = create(:notification_profile, name: 'Test', user_id: user.id)
        subscription = profile.subscriptions.create!(user_id: user.id,
                                                     publisher_id: device.id,
                                                     publisher_type: NotificationSubscription::Types::DEVICE)

        SubscribedEvent.create!(profile_id: profile.id,
                                uuid_id: up_uuid.id,
                                frequency: NotificationProfile::Frequencies::IMMEDIATELY)

        SubscribedEvent.create!(profile_id: profile.id,
                                uuid_id: down_uuid.id,
                                frequency: NotificationProfile::Frequencies::EVERY_15_MINUTES)

        up_event = 'Test Up'
        down_event = 'Test Down'

        subscription.notifications.create!(user_id: user.id, device_id: device.id, message: up_event, uuid_id: up_uuid.id)
        subscription.notifications.create!(user_id: user.id, device_id: device.id, message: down_event, uuid_id: down_uuid.id)

        notifications = described_class.available_for_minute_of_day(0)

        expect(notifications[subscription]).not_to be_empty
        expect(notifications[subscription].map(&:message)).not_to include(up_event)
      end
    end
  end

  describe 'scopes' do
    describe '.with_device_categories' do
      before do
        @device_1 = create(:test_device, :with_sequenced_category)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_1.id)
        @device_2 = create(:test_device, :with_sequenced_category)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_2.id)
        @device_3 = create(:test_device, :with_sequenced_category)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_3.id)
      end

      context 'when given a device category' do
        let(:result) { described_class.with_device_categories @device_1.category_id }

        it 'includes notifications from devices with the given category' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).not_to include @notification_3
        end
      end

      context 'when given multiple device categories' do
        let(:result) { described_class.with_device_categories [@device_1.category.id, @device_2.category.id] }

        it 'includes notifications from devices with the given category' do
          expect(result).to include @notification_1
          expect(result).to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.with_device_models' do
      before do
        @device_1 = create(:test_device, :model_8100)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_1.id)
        @device_2 = create(:test_device, :model_5300_dc)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_2.id)
        @device_3 = create(:test_device, :model_5400_rm)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_3.id)
      end

      context 'when given a device model' do
        let(:result) { described_class.with_device_models @device_1.model_id }

        it 'includes notifications from devices with the given model' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).not_to include @notification_3
        end
      end

      context 'when given multiple device models' do
        let(:result) { described_class.with_device_models [@device_1.series.id, @device_2.series.id] }

        it 'includes notifications from devices with the given model' do
          expect(result).to include @notification_1
          expect(result).to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.with_event_type' do
      before do
        @device_1 = create(:test_device)
        @event_type_1 = 1
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_1.id,
                                                           event_type: @event_type_1)
        @device_2 = create(:test_device)
        @event_type_2 = 1
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_2.id,
                                                           event_type: @event_type_2)
        @device_3 = create(:test_device)
        @event_type_3 = 2
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_3.id,
                                                           event_type: @event_type_3)
      end

      context 'when given an event type' do
        let(:result) { described_class.with_event_type @event_type_1 }

        it 'includes notifications with the given type' do
          expect(result).to include @notification_1
          expect(result).to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.with_event_level' do
      before do
        @device_1 = create(:test_device)
        @level_1 = 1
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_1.id,
                                                           severity: @level_1)
        @device_2 = create(:test_device)
        @level_2 = 2
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_2.id,
                                                           severity: @level_2)
        @device_3 = create(:test_device)
        @level_3 = 3
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1,
                                                           device_id: @device_3.id,
                                                           severity: @level_3)
      end

      context 'when given an event level' do
        let(:result) { described_class.with_event_level @level_1 }

        it 'includes notifications with the given level' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).not_to include @notification_3
        end
      end

      context 'when given multiple event levels' do
        let(:result) { described_class.with_event_level [@level_1, @level_2] }

        it 'includes notifications with one of the given levels' do
          expect(result).to include @notification_1
          expect(result).to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.delivered_since' do
      before do
        @device_1 = create(:test_device)
        @event_1 = create(:event, device: @device_1)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_1.id, delivered_at: 1.month.ago)
        @device_2 = create(:test_device)
        @event_2 = create(:event, device: @device_2)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_2.id, delivered_at: 1.week.ago)
        @device_3 = create(:test_device)
        @event_3 = create(:event, device: @device_3)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_3.id, delivered_at: 1.day.ago)
      end

      context 'when given a date' do
        let(:result) { described_class.delivered_since 2.weeks.ago }

        it 'includes notifications that were delivered on or after the given date' do
          expect(result).not_to include @notification_1
          expect(result).to include @notification_2
          expect(result).to include @notification_3
        end
      end
    end

    describe '.with_device_organization' do
      before do
        @device_1 = create(:test_device)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_1.id)
        @device_2 = create(:test_device)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_2.id)
        @device_3 = create(:test_device, :with_sub_organization)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_3.id)
      end

      context 'when given an organization with hierarchy' do
        let(:result) do
          described_class.with_device_organization(organization_id: @device_1.organization_id,
                                                   include_hierarchy: 'true')
        end

        it 'includes notifications within suborganizations' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).to include @notification_3
        end
      end

      context 'when given an organization without hierarchy' do
        let(:result) do
          described_class.with_device_organization(organization_id: @device_1.organization_id,
                                                   include_hierarchy: 'false')
        end

        it 'returns devices without suborganizations' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.with_device_mac' do
      before do
        @device_1 = create(:test_device)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_1.id)
        @device_2 = create(:test_device)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_2.id)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, device_id: @device_1.id)
      end

      context 'when given a valid mac address' do
        let(:result) { described_class.with_device_mac(@device_1.mac) }

        it 'includes notifications with for devices with the given mac' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).to include @notification_3
        end
      end
    end

    describe '.sort_by_delivered_at' do
      before do
        @event_1 = create(:event)
        @notification_1 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_1.id, delivered_at: 1.month.ago)
        @event_2 = create(:event)
        @notification_2 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_2.id, delivered_at: 1.week.ago)
        @event_3 = create(:event)
        @notification_3 = create(:notification, notification_subscription_id: 1,
                                                           user_id: 1, event_id: @event_3.id, delivered_at: 1.day.ago)
      end

      context 'when sorting asc' do
        let(:result) { described_class.sort_by_delivered_at('asc') }

        it 'includes all notifications with more recent delivered at last' do
          expect(result.first).to eq @notification_1
          expect(result.second).to eq @notification_2
          expect(result.last).to eq @notification_3
        end
      end

      context 'when sorting desc' do
        let(:result) { described_class.sort_by_delivered_at('desc') }

        it 'includes all notifications with more recent delivered at first' do
          expect(result.first).to eq @notification_3
          expect(result.second).to eq @notification_2
          expect(result.last).to eq @notification_1
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      context 'when searching multiple categories, with organization' do
        before do
          @device_1 = create(:test_device, :with_sequenced_category)
          @notification_1 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1, device_id: @device_1.id)
          @device_2 = create(:test_device, :with_sequenced_category)
          @notification_2 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1, device_id: @device_2.id)
          @device_3 = create(:test_device, :with_sequenced_category, :with_sub_organization)
          @notification_3 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1, device_id: @device_3.id)
        end

        let(:result) do
          described_class.search(with_device_categories: [@device_1.category.id, @device_3.category.id],
                                 with_device_organization: { organization_id: @device_1.organization.id, include_hierarchy: 'false' })
        end

        it 'returns the notifications with the given categories and organization' do
          expect(result).to include @notification_1
          expect(result).not_to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end

    describe '.search!' do
      context 'when searching by event type and delivered since' do
        before do
          @device_1 = create(:test_device)
          @event_type_1 = 1
          @notification_1 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1,
                                                             device_id: @device_1.id,
                                                             delivered_at: 1.month.ago,
                                                             event_type: @event_type_1)
          @device_2 = create(:test_device)
          @notification_2 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1,
                                                             device_id: @device_2.id,
                                                             delivered_at: 1.week.ago,
                                                             event_type: @event_type_1)
          @device_3 = create(:test_device)
          @event_type_3 = 2
          @notification_3 = create(:notification, notification_subscription_id: 1,
                                                             user_id: 1,
                                                             device_id: @device_3.id,
                                                             delivered_at: 1.day.ago,
                                                             event_type: @event_type_3)
        end

        let(:result) { described_class.search!(with_event_type: 1, delivered_since: 2.weeks.ago) }

        it 'returns the notifications with the given event type since the given date' do
          expect(result).not_to include @notification_1
          expect(result).to include @notification_2
          expect(result).not_to include @notification_3
        end
      end
    end
  end
end
