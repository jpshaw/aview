# frozen_string_literal: true

require 'rails_helper'

describe Ucpe do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', DeviceConfiguration.new
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:ucpe_device) }
  end
  it_behaves_like 'a uclinux device' do
    let(:platform) { :uc_linux }
    let(:test_serial) { '9400019999999965' }
    let(:device)   { create(:ucpe_device) }
  end
  it_behaves_like 'a uclinux device with modem' do
    let(:platform) { :uc_linux }
    let(:device)   { create(:ucpe_device) }
  end
  it_behaves_like 'it has a cellular modem'
end
