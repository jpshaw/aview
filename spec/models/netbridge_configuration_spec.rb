# frozen_string_literal: true

require 'rails_helper'

describe NetbridgeConfiguration do
  it_behaves_like 'a searchable model'

  describe 'grace_period_duration' do
    context 'when locked' do
      before do
        subject.grace_period_duration_locked = true
      end

      it 'is not validated' do
        expect(subject).not_to receive(:valid_grace_period_duration?)
        subject.valid?
      end
    end

    context 'when unlocked' do
      before do
        subject.grace_period_duration_locked = false
      end

      it 'is validated' do
        expect(subject).to receive(:valid_grace_period_duration?)
        subject.valid?
      end

      context 'with an invalid duration value' do
        before do
          subject.grace_period_duration = 'invalid_duration'
        end

        it 'is invalid' do
          subject.valid?
          expect(subject.errors.keys).to include(:grace_period_duration)
        end
      end

      context 'with a valid duration value' do
        before do
          subject.grace_period_duration = '3d'
        end

        it 'is valid' do
          subject.valid?
          expect(subject.errors.keys).not_to include(:grace_period_duration)
        end
      end
    end
  end

  context 'With a user belonging to a sub organization' do
    before do
      @organization = create(:organization)
      @organization_sub = create(:sub_organization)
      @user = create(:user, organization: @organization_sub)
      p = Permission.new
      p.manager = @user
      p.manageable = @organization_sub
      p.role = Role.first
      p.save!
    end

    context 'when user manages some netbridge_configurations' do
      before do
        create(:netbridge_configuration,
                  organization: @organization)
        create_list(:netbridge_configuration,
                       2,
                       organization: @organization_sub)
        expect(described_class.count).to be(3)
      end

      it 'returns only netbridge_configurations managed by user' do
        expect(Pundit.policy_scope(@user, described_class)
               .to_a.size).to eq(2)
      end
    end
  end

  context 'with a dependent device' do
    before do
      @organization = create(:organization)
      @config = create(:netbridge_configuration,
                                  organization: @organization)
      @device = create(:netbridge_device,
                                  organization: @organization,
                                  configuration: @config)
    end

    it 'does not allow destroy' do
      expect(@config.destroy).to be(false)
    end
  end

  context 'with parent and child configurations' do
    before do
      @organization = create(:organization)
      @parent = create(:netbridge_configuration,
                                  organization: @organization)
      @child = create(:netbridge_configuration,
                                 organization: @organization)
      @parent.children << @child
    end

    it 'does not allow destroy' do
      expect(@parent.destroy).to be(false)
    end
  end

  describe '#config_values' do
    before do
      @organization = create(:random_regular_organization)
      @attributes   = random_attributes
      @config       = described_class.create!(
        { name:            'Test',
          organization_id: @organization.id }.merge(@attributes)
      )
    end

    context 'when a key is locked' do
      before do
        @key = @attributes.keys.sample.to_s
        @config.key_meta[@key] ||= {}
        @config.key_meta[@key]['locked'] = true
      end

      it 'is not included in the returned hash' do
        expect(@config.config_values).not_to have_key(@key)
      end
    end

    context 'when a key is not locked' do
      before do
        @key = @attributes.keys.sample.to_s
        @config.key_meta[@key] ||= {}
        @config.key_meta[@key]['locked'] = false
      end

      it 'is included in the returned hash' do
        expect(@config.config_values).to have_key(@key)
      end
    end

    it 'only includes config attributes' do
      expect(@config.config_values).not_to have_key(:name)
    end
  end

  describe '#values_from_ancestors' do
    before do
      @organization = create(:random_regular_organization)

      allow_any_instance_of(described_class).to receive(:set_heartbeat_fields)

      # Set up attributes
      @child_attrs       = { cfg: 'child' }
      @parent_attrs      = { cfg: 'parent',      xma: nil }
      @grandparent_attrs = { cfg: 'grandparent', xma: 'grandparent' }

      # Set up configs
      @grandparent_config = described_class.create!(
        { name:            'Grandparent',
          organization_id: @organization.id }.merge(@grandparent_attrs)
      )

      @parent_config = described_class.create!(
        { name:            'Parent',
          organization_id: @organization.id,
          parent_id:       @grandparent_config.id }.merge(@parent_attrs)
      )

      @child_config = described_class.create!(
        { name:            'Child',
          organization_id: @organization.id,
          parent_id:       @parent_config.id }.merge(@child_attrs)
      )

      # Set up locks
      @child_config.cfg_locked = false
      @child_config.xma_locked = true
      @child_config.save

      @parent_config.cfg_locked = false
      @parent_config.xma_locked = true
      @parent_config.save

      @grandparent_config.cfg_locked = false
      @grandparent_config.xma_locked = false
      @grandparent_config.save
    end

    it 'does not include any fields for the child config' do
      ancestor_keys = @child_config.values_from_ancestors.keys
      expect(ancestor_keys - @child_attrs.keys).to eq ancestor_keys
    end

    it 'includes fields not in the child config' do
      expect(@child_config.values_from_ancestors['cfg']).to be_present
    end

    it 'selects values from the closest ancestor' do
      values = @child_config.values_from_ancestors
      expect(values['cfg']).to eq 'parent'
      expect(values['xma']).to eq 'grandparent'
    end
  end

  describe '#config_values_with_inherited' do
    before do
      @organization = create(:random_regular_organization)

      allow_any_instance_of(described_class).to receive(:set_heartbeat_fields)

      # Set up attributes
      @child_attrs       = { cfg: 'child' }
      @parent_attrs      = { cfg: 'parent',      xma: 'parent' }
      @grandparent_attrs = { cfg: 'grandparent', xma: 'grandparent', xmc: 'grandparent' }

      # Set up configs
      @grandparent_config = described_class.create!(
        { name:            'Grandparent',
          organization_id: @organization.id }.merge(@grandparent_attrs)
      )

      @parent_config = described_class.create!(
        { name:            'Parent',
          organization_id: @organization.id,
          parent_id:       @grandparent_config.id }.merge(@parent_attrs)
      )

      @child_config = described_class.create!(
        { name:            'Child',
          organization_id: @organization.id,
          parent_id:       @parent_config.id }.merge(@child_attrs)
      )

      # Set up locks
      @child_config.cfg_locked = false
      @child_config.xma_locked = true
      @child_config.xmc_locked = true
      @child_config.save

      @parent_config.cfg_locked = false
      @parent_config.xma_locked = false
      @parent_config.xmc_locked = true
      @parent_config.save

      @grandparent_config.cfg_locked = false
      @grandparent_config.xma_locked = false
      @grandparent_config.xmc_locked = false
      @grandparent_config.save
    end

    it 'renders a final config' do
      values = @child_config.config_values_with_inherited
      expect(values['cfg']).to eq 'child'
      expect(values['xma']).to eq 'parent'
      expect(values['xmc']).to eq 'grandparent'
    end
  end

  describe '#apply_attributes' do
    before do
      @organization = create(:random_regular_organization)
      @config       = described_class.create!(
        name:            'Test',
        organization_id: @organization.id
      )
    end

    # This should set all config values to nil and successfully update the object.
    context 'when given nil' do
      it 'returns true' do
        expect(@config.apply_attributes(nil, nil)).to be true
      end
    end

    context 'when given attributes' do
      before do
        @attributes = random_attributes
      end

      it 'sets the values for the keys' do
        retval = @config.apply_attributes(@attributes, {})
        expect(retval).to be true
        @attributes.each do |key, value|
          expect(@config.send(key)).to eq value
        end
      end

      context 'when in invalid value is set for an attribute' do
        before do
          @attributes[:name] = nil
        end

        it 'fails to update the config and returns false' do
          retval = @config.apply_attributes(@attributes, {})
          expect(retval).to be false
        end
      end

      context 'when given locks' do
        before do
          locked_values = [NetbridgeConfiguration::LOCKED_VALUE,
                           NetbridgeConfiguration::UNLOCKED_VALUE]
          @locks = {}
          @attributes.keys.each do |key|
            @locks["#{key}_locked"] = locked_values.sample
          end
        end

        it 'sets the `locked` meta value for the key' do
          @config.apply_attributes(@attributes, @locks)
          @attributes.keys.each do |key|
            locked_value = @locks["#{key}_locked"] == NetbridgeConfiguration::LOCKED_VALUE
            expect(@config.key_meta[key]['locked']).to eq locked_value
          end
        end

        it 'sets locked values to nil and does nothing to unlocked values' do
          @config.apply_attributes(@attributes, @locks)
          @attributes.each do |key, value|
            locked_value = @locks["#{key}_locked"] == NetbridgeConfiguration::LOCKED_VALUE
            if locked_value == true
              expect(@config.send(key)).to be nil
            else
              expect(@config.send(key)).to eq value
            end
          end
        end
      end

      context 'when no locks are given' do
        context 'when the value is nil' do
          it 'sets the key locks to the default' do
            @nil_attributes = @attributes.each { |key, _| @attributes[key] = nil }
            @config.apply_attributes(@nil_attributes, {})
            @nil_attributes.keys.each do |key|
              expect(@config.key_meta[key]['locked']).to eq NetbridgeConfiguration::LOCKED_BY_DEFAULT
            end
          end
        end

        context 'when the value is not nil' do
          it 'sets the key lock to false' do
            @config.apply_attributes(@attributes, {})
            @attributes.each do |key, value|
              # It's possible that one of our attributes is nil (based on type)
              # from the `random_attributes` helper method.
              if value.nil?
                expect(@config.key_meta[key]['locked']).to eq NetbridgeConfiguration::LOCKED_BY_DEFAULT
              else
                expect(@config.key_meta[key]['locked']).to be false
              end
            end
          end
        end
      end
    end
  end

  describe '#key_locked?' do
    before do
      @organization = create(:random_regular_organization)
      @config       = described_class.create!(
        name:            'Test',
        organization_id: @organization.id
      )
    end

    context 'when the key has a meta entry' do
      before do
        @config.key_meta['test'] = {}
      end

      context 'when locked is true' do
        before do
          @config.key_meta['test']['locked'] = true
        end

        it 'returns true' do
          expect(@config.key_locked?('test')).to be true
        end
      end

      context 'when locked is false' do
        before do
          @config.key_meta['test']['locked'] = false
        end

        it 'returns false' do
          expect(@config.key_locked?('test')).to be false
        end
      end
    end

    context 'when the key does not have a meta entry' do
      it 'returns the default lock value' do
        expect(@config.key_locked?('test')).to eq NetbridgeConfiguration::LOCKED_BY_DEFAULT
      end
    end

    # Test accessing the key directly, such as test_locked + test_locked?
    context 'when the key is accessed directly' do
      before do
        @config.key_meta['test'] = { 'locked' => true }
      end

      context 'when checking _locked' do
        it 'returns true' do
          expect(@config.test_locked).to be true
        end
      end

      context 'when checking locked?' do
        it 'returns true' do
          expect(@config.test_locked?).to be true
        end
      end
    end
  end

  describe '#*_locked=' do
    before do
      @organization = create(:random_regular_organization)
      @config       = described_class.create!(
        name:            'Test',
        organization_id: @organization.id
      )
    end

    context 'when given true' do
      it 'sets locked to true for the key' do
        @config.cfg_locked = true
        expect(@config.cfg_locked?).to be true
      end
    end

    context 'when given false' do
      it 'sets locked to false for the key' do
        @config.cfg_locked = false
        expect(@config.cfg_locked?).to be false
      end
    end
  end

  describe '#to_keyval' do
    before do
      @organization = create(:random_regular_organization)
      @config       = described_class.create!(
        name:            'Test',
        organization_id: @organization.id
      )
    end

    context 'when the config is empty' do
      it 'returns the default config endpoint with a newline' do
        expect(@config.to_keyval).to eq "cfg=#{NetbridgeConfiguration::DEFAULT_CONFIG_ENDPOINT}\n"
      end
    end
  end

  describe '#heartbeat_frequency' do
    it 'returns own status_freq value in seconds when present' do
      allow(subject).to receive(:status_freq).and_return('1h1m')
      subject.save(validate: false)
      expect(subject.heartbeat_frequency).to eq(3660)
    end

    it 'returns inherited status_freq value in seconds when present and no own status_freq value' do
      allow(subject).to receive(:status_freq).and_return(nil)
      allow(subject).to receive(:config_values_with_inherited).and_return('status_freq' => '2m')
      subject.save(validate: false)
      expect(subject.heartbeat_frequency).to eq(120)
    end

    it 'returns a default status freq value in seconds when no own status_freq value and no inherited value' do
      allow(subject).to receive(:status_freq).and_return(nil)
      subject.save(validate: false)
      expect(subject.heartbeat_frequency).to eq(DurationCalculator.calculate(subject.class::DEFAULT_STATUS_FREQ))
    end
  end

  describe '#grace_period' do
    it 'returns own grace_period_duration value in seconds when present' do
      allow(subject).to receive(:grace_period_duration).and_return('1h1m')
      subject.save(validate: false)
      expect(subject.heartbeat_grace_period).to eq(3660)
    end

    it 'returns inherited grace_period_duration value in seconds when present and no own grace_period_duration value' do
      allow(subject).to receive(:grace_period_duration).and_return(nil)
      allow(subject).to receive(:config_values_with_inherited).and_return(grace_period_duration: '2m')
      subject.save(validate: false)
      expect(subject.heartbeat_grace_period).to eq(120)
    end

    it 'returns nil if no own nor inherited grace_period_duration value' do
      allow(subject).to receive(:grace_period_duration).and_return(nil)
      subject.save(validate: false)
      expect(subject.heartbeat_grace_period).to be nil
    end
  end

  describe 'scopes' do
    describe '.with_organization' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration)
        @netbridge_config_2 = create(:netbridge_configuration)
        @netbridge_config_3 = create(:netbridge_configuration, :with_sub_organization)
      end

      context 'when given an organization with hierarchy' do
        let(:result) do
          described_class.with_organization(organization_id: @netbridge_config_1.organization.id,
                                            include_hierarchy: 'true')
        end

        it 'includes devices within suborganizations' do
          expect(result).to include @netbridge_config_1
          expect(result).not_to include @netbridge_config_2
          expect(result).to include @netbridge_config_3
        end
      end

      context 'when given an organization without hierarchy' do
        let(:result) do
          described_class.with_organization(organization_id: @netbridge_config_1.organization.id,
                                            include_hierarchy: 'false')
        end

        it 'returns devices without suborganizations' do
          expect(result).to include @netbridge_config_1
          expect(result).not_to include @netbridge_config_2
          expect(result).not_to include @netbridge_config_3
        end
      end
    end

    describe '.sort_by_name' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration)
        @netbridge_config_2 = create(:netbridge_configuration)
        @netbridge_config_3 = create(:netbridge_configuration)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_name 'asc' }

        it 'sorts configurations with lower names first and higher names last' do
          expect(result.first).to eq @netbridge_config_1
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_name 'desc' }

        it 'sorts configurations with higher names first and lower names last' do
          expect(result.first).to eq @netbridge_config_3
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_1
        end
      end
    end

    describe '.sort_by_organizations_name' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration)
        @netbridge_config_2 = create(:netbridge_configuration)
        @netbridge_config_3 = create(:netbridge_configuration)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_organizations_name 'asc' }

        it 'sorts configurations with lower organization names first and higher names last' do
          expect(result.first).to eq @netbridge_config_1
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_organizations_name 'desc' }

        it 'sorts configurations with higher organization names first and lower names last' do
          expect(result.first).to eq @netbridge_config_3
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_1
        end
      end
    end

    describe '.sort_by_updated_at' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration, updated_at: 1.month.ago)
        @netbridge_config_2 = create(:netbridge_configuration, updated_at: 1.day.ago)
        @netbridge_config_3 = create(:netbridge_configuration)
      end

      context 'when given ascending' do
        let(:result) { described_class.sort_by_updated_at 'asc' }

        it 'sorts configurations with later updated times first and null times last' do
          expect(result.first).to eq @netbridge_config_1
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_3
        end
      end

      context 'when given descending' do
        let(:result) { described_class.sort_by_updated_at 'desc' }

        it 'sorts configurations with null times first and later updated times last' do
          expect(result.first).to eq @netbridge_config_3
          expect(result.second).to eq @netbridge_config_2
          expect(result.last).to eq @netbridge_config_1
        end
      end
    end
  end

  describe 'searchable' do
    describe '.search' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration)
        @netbridge_config_2 = create(:netbridge_configuration)
        @netbridge_config_3 = create(:netbridge_configuration, :with_sub_organization)
      end

      context 'when searching with organization and sorting by name descending' do
        let(:result) do
          described_class.search(
            with_organization: { organization_id: @netbridge_config_1.organization.id, include_hierarchy: 'true' },
            sort_by_name: 'desc'
          )
        end

        it 'includes netbridge configurations within suborganizations sorted correctly' do
          expect(result.first).to eq @netbridge_config_3
          expect(result).not_to include @netbridge_config_2
          expect(result.last).to eq @netbridge_config_1
        end
      end

      context 'when searching all netbridge configurations' do
        let(:result) { described_class.search(all: nil) }

        it 'returns all netbridge configurations' do
          expect(result).to include @netbridge_config_1
          expect(result).to include @netbridge_config_2
          expect(result).to include @netbridge_config_3
        end
      end
    end

    describe '.search!' do
      before do
        @netbridge_config_1 = create(:netbridge_configuration)
        @netbridge_config_2 = create(:netbridge_configuration)
        @netbridge_config_3 = create(:netbridge_configuration, :with_sub_organization)
      end

      context 'when searching with organization and sorting by name ascending' do
        let(:result) do
          described_class.search!(
            with_organization: { organization_id: @netbridge_config_1.organization.id, include_hierarchy: 'true' },
            sort_by_name: 'asc'
          )
        end

        it 'includes netbridge configurations within suborganizations' do
          expect(result.first).to eq @netbridge_config_1
          expect(result).not_to include @netbridge_config_2
          expect(result.last).to eq @netbridge_config_3
        end
      end
    end
  end

  describe '.find_by_device_mac' do
    subject { described_class.find_by_device_mac(mac) }

    context 'with nil' do
      let(:mac) { nil }

      it { is_expected.to be nil }
    end

    context 'with a mac for a device that does not exist' do
      let(:mac) { generate(:netbridge_fx_mac) }

      it { is_expected.to be nil }
    end

    context 'with a mac for an existing device that does not have a configuration' do
      let!(:device) { create(:netbridge_device, :fx) }
      let(:mac)     { device.mac }

      before { expect(device.configuration).to be_blank }

      it { is_expected.to be nil }
    end

    context 'with a mac for an existing device that has a configuration' do
      let!(:device)        { create(:netbridge_device, :configurable, :fx) }
      let!(:configuration) { device.configuration }
      let(:mac)            { device.mac }

      before { expect(configuration).to be_present }

      it 'returns the configuration' do
        expect(subject).to eq configuration
      end
    end
  end
end
