# frozen_string_literal: true

require 'rails_helper'

describe UsbCellular do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', DeviceConfiguration.new
  it_behaves_like 'a uclinux device' do
    let(:platform) { :uc_linux }
    let(:test_serial) { '5300029999999955' }
    let(:device) { create(:usb_cellular_device) }
  end
end
