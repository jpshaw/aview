# frozen_string_literal: true

require 'rails_helper'

describe NotificationSubscription do
  describe 'notification delivery counts' do
    let(:subscription) { create(:organization_subscription) }

    before do
      create_list(:notification, 3, notification_subscription: subscription, delivered_at: 1.hour.ago)
      create_list(:notification, 2, notification_subscription: subscription, delivered_at: 2.days.ago)
      create_list(:notification, 1, notification_subscription: subscription, delivered_at: 3.weeks.ago)
    end

    describe '#notification_delivery_count_for_last_day' do
      it 'returns the correct count' do
        expect(subscription.notification_delivery_count_for_last_day).to eq 3
      end
    end

    describe '#notification_delivery_count_for_last_week' do
      it 'returns the correct count' do
        expect(subscription.notification_delivery_count_for_last_week).to eq 5
      end
    end

    describe '#notification_delivery_count_for_last_month' do
      it 'returns the correct count' do
        expect(subscription.notification_delivery_count_for_last_month).to eq 6
      end
    end
  end

  describe 'methods' do
    before do
      create_profile_for_immediate_gateway_notifications
      create_subscriptions_for_gateway_device
    end

    context 'for device publishers' do
      before do
        @subscription = @user.notification_subscriptions.devices.first
      end

      describe '#publisher_class' do
        it 'returns the class name of the publisher' do
          expect(@subscription.publisher_class).to eq Device
        end
      end

      describe '#publisher' do
        it 'returns the publisher object' do
          publisher = @subscription.publisher
          expect(publisher).not_to be nil
          expect(publisher.id).to eq @device.id
        end
      end

      describe '#publisher_name' do
        it 'returns the name of the publisher object' do
          expect(@subscription.publisher_name).to eq @device.name
        end
      end

      describe '#type' do
        it 'returns the string type for the publisher' do
          expect(@subscription.type).to eq 'Device'
        end
      end
    end

    context 'for organization publishers' do
      before do
        @subscription = @user.notification_subscriptions.organizations.first
      end

      describe '#publisher_class' do
        it 'returns the class name of the publisher' do
          expect(@subscription.publisher_class).to eq Organization
        end
      end

      describe '#publisher' do
        it 'returns the publisher object' do
          publisher = @subscription.publisher
          expect(publisher).not_to be nil
          expect(publisher.id).to eq @organization.id
        end
      end

      describe '#publisher_name' do
        it 'returns the name of the publisher object' do
          expect(@subscription.publisher_name).to eq @organization.name
        end
      end

      describe '#type' do
        it 'returns the string type for the publisher' do
          expect(@subscription.type).to eq 'Organization'
        end
      end
    end

    describe '.notifiable' do
      it 'returns subscriptions that have notifiable subscribed events' do
        subscriptions = described_class.notifiable
        expect(subscriptions).to match_array(@subscriptions)
      end
    end

    describe '.devices' do
      it 'returns device subscriptions' do
        subscriptions = described_class.devices
        expect(subscriptions).to eq [@device_subscription]
      end
    end

    describe '.organizations' do
      it 'returns organization subscriptions' do
        subscriptions = described_class.organizations
        expect(subscriptions).to eq [@org_subscription]
      end
    end

    describe '.for_device_and_uuid' do
      it 'returns an empty array if device_id and uuid_id are nil' do
        subscriptions = described_class.for_device_and_uuid(nil, nil)
        expect(subscriptions).to be_empty
      end

      it 'returns subscriptions interested in a device and uuid' do
        subscriptions = described_class.for_device_and_uuid(@device.id, @uuids.first)
        expect(subscriptions).to eq [@device_subscription]
      end
    end

    describe '.for_organizations_and_uuid' do
      it 'returns an empty array if organization_id_list and uuid_id are nil' do
        subscriptions = described_class.for_organizations_and_uuid(nil, nil)
        expect(subscriptions).to be_empty
      end

      it 'returns subscriptions interested in a device and uuid' do
        subscriptions = described_class.for_organizations_and_uuid(@organization.ancestor_id_list, @uuids.first)
        expect(subscriptions).to eq [@org_subscription]
      end
    end

    describe '.for_publisher_and_uuid' do
      it 'returns an empty array if a publisher scope is not applied' do
        subscriptions = described_class.for_publisher_and_uuid(@device.id, @uuids.first)
        expect(subscriptions).to be_empty
      end

      it 'returns subscriptions if a publisher scope is applied' do
        subscriptions = described_class.devices.for_publisher_and_uuid(@device.id, @uuids.first)
        expect(subscriptions).to eq [@device_subscription]
      end
    end
  end
end
