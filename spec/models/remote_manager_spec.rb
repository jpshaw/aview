# frozen_string_literal: true

require 'rails_helper'

describe RemoteManager do
  it_behaves_like 'a base device class'
  it_behaves_like 'a configuration monitorable device', DeviceConfiguration.new
  it_behaves_like 'an associatable device' do
    let(:device)   { create(:remote_manager_device) }
  end
  it_behaves_like 'a uclinux device' do
    let(:platform) { :uc_linux }
    let(:test_serial) { '5300029999999955' }
    let(:device)   { create(:remote_manager_device) }
  end
  it_behaves_like 'a uclinux device with modem' do
    let(:platform) { :uc_linux }
    let(:device)   { create(:remote_manager_device) }
  end
  it_behaves_like 'it has a cellular modem'
end
