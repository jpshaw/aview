# frozen_string_literal: true

require 'rails_helper'

describe DeviceModem do
  describe 'Attributes' do
    describe '#cnti' do
      let(:modem) { create(:device_modem, :with_device, cnti: @cnti) }

      context 'when lowercase' do
        it 'upcases cnti' do
          @cnti = 'lte'
          expect(modem.cnti).to eq @cnti.upcase
        end
      end

      context 'when nil' do
        it 'saves with cnti nil' do
          @cnti = nil
          expect(modem.cnti).to be nil
        end
      end
    end
  end

  describe 'Class methods' do
    let(:official_carriers) do
      %w[AT&T BellCA Mobinil Rogers Sprint Telecom Telus T-Mobile Verizon Vodafone]
    end

    describe '.carrier' do
      context 'when the value is known' do
        let(:known_carriers) do
          %w[at&t bell.ca mobinil rogers sprint telecom telus t-mobile verizon vodafone]
        end

        it 'translates known value to official carrier' do
          known_carriers.each do |carrier|
            official_carrier = described_class.carrier(carrier)
            expect(official_carrier).not_to eq(carrier)
            expect(official_carriers).to include(official_carrier)
          end
        end
      end

      context 'when the value is unknown' do
        let(:unknown_carrier) { Faker::Lorem.word }

        it 'is returned as-is' do
          expect(described_class.carrier(unknown_carrier)).to eq(unknown_carrier)
        end
      end
    end

    describe '.percent_to_signal and .signal_to_percent' do
      it 'converts percentages to and from signals' do
        (0..100).each do |signal_strength_percent|
          signal = described_class.percent_to_signal(signal_strength_percent)
          percent = described_class.signal_to_percent(signal)
          expect(percent).to eq signal_strength_percent
        end
      end
    end
  end

  describe 'Instance methods' do
    before do
      @device = create(:test_device)
      @modem = create(:device_modem, device: @device, lac: nil, cid: nil, mcc: nil, mnc: nil)
    end

    describe '#adjust_signal' do
      context 'when signal is nil' do
        it 'signal is still set to nil' do
          @modem.signal = nil
          @modem.adjust_signal
          expect(@modem.signal).to eq nil
        end
      end

      context 'when signal is set below -112' do
        it 'sets signal to -112' do
          @modem.signal = -200
          @modem.adjust_signal
          expect(@modem.signal).to eq(-112)
        end
      end

      context 'when signal is set above -50' do
        it 'sets signal to -50' do
          @modem.signal = 100
          @modem.adjust_signal
          expect(@modem.signal).to eq(-50)
        end
      end
    end

    describe '#sent_sms_recently?' do
      context 'with sms_sent_at set to nil' do
        it 'returns false' do
          @modem.sms_sent_at = nil
          expect(@modem.sent_sms_recently?).to eq false
        end
      end

      context 'with sms_sent_at set within 60 seconds from now' do
        it 'returns true' do
          @modem.sms_sent_at = Time.now - 5.seconds
          expect(@modem.sent_sms_recently?).to eq true
        end
      end

      context 'with sms_sent_at set over 60 seconds from now' do
        it 'returns false' do
          @modem.sms_sent_at = Time.now - 5.minutes
          expect(@modem.sent_sms_recently?).to eq false
        end
      end
    end

    describe '#time_left_til_sms_allowed' do
      it 'returns the amount of time left until a sms can be sent' do
        # Expect it to return 30 seconds
        time = Time.now
        @modem.sms_sent_at = time - 30.seconds
        allow(Time).to receive(:now).and_return(time)
        expect(@modem.time_left_til_sms_allowed).to eq 30
      end
    end

    describe '#signal_below_threshold?' do
      before do
        allow(subject).to receive(:signal_percent_value).and_return(50)
      end

      context 'with signal percentage value below threshold' do
        before do
          allow(subject).to receive(:signal_strength_threshold).and_return(60)
        end

        it 'returns true' do
          expect(subject.signal_below_threshold?).to be true
        end
      end

      context 'with signal percentage value same as threshold' do
        before do
          allow(subject).to receive(:signal_strength_threshold).and_return(50)
        end

        it 'returns false' do
          expect(subject.signal_below_threshold?).to be false
        end
      end

      context 'with signal percentage value above as threshold' do
        before do
          allow(subject).to receive(:signal_strength_threshold).and_return(40)
        end

        it 'returns false' do
          expect(subject.signal_below_threshold?).to be false
        end
      end
    end

    describe 'Private methods' do
      describe '#signal_strength_threshold' do
        context 'When threshold percentage setting exist' do
          let(:threshold_percentage) { DeviceModem::MINIMUM_SIGNAL_STRENGTH_PERCENTAGE + 1 }

          before do
            AdminSettings.threshold_percentage = threshold_percentage
          end

          it 'returns the signal strength threshold percentage setting' do
            expect(subject.__send__(:signal_strength_threshold)).to eq threshold_percentage
          end
        end

        context 'When threshold percentage setting does not exist' do
          before do
            AdminSettings.threshold_percentage = nil
          end

          it "returns #{DeviceModem::MINIMUM_SIGNAL_STRENGTH_PERCENTAGE}" do
            expect(subject.__send__(:signal_strength_threshold)).to eq DeviceModem::MINIMUM_SIGNAL_STRENGTH_PERCENTAGE
          end
        end
      end

      describe '#update_cell_location' do
        context 'when the modem is created' do
          it 'updates the cell location' do
            expect_any_instance_of(described_class).to receive(:update_cell_location)
            create(:device_modem, device: @device)
          end
        end

        context 'when the modem lac and cid are changed' do
          it 'updates the cell location' do
            expect(@modem.lac).to be_nil
            expect(@modem.cid).to be_nil
            expect(@modem).to receive(:update_cell_location)
            @modem.lac = 65_534
            @modem.cid = 116_235_024
            @modem.mcc = 310
            @modem.mnc = 410
            @modem.save!
          end
        end

        context 'when the modem lac and cid are not changed' do
          it 'does not update the cell location' do
            expect(@modem).not_to receive(:update_cell_location)
            @modem.save!
          end
        end
      end
    end
  end

  describe 'Scopes' do
  end

  describe 'Callbacks' do
    let(:device) { create(:cellular_device) }

    before { subject.update(device_id: device.id, rx: 0, tx: 0) }

    describe '.save_cellular_utilization' do
      context 'when utilization fields have changed' do
        before do
          subject.rx = 1_000
          subject.tx = 1_000
        end

        it 'stores the changes in the metrics service' do
          expect(Measurement::CellularUtilization).to receive(:create)
          subject.save
        end
      end
    end
  end
end
