# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DataPlan, type: :model do
  describe 'before destroy' do
    before do
      organization = create(:organization)
      @data_plan = create(:data_plan, organization: organization)
      @device = create(:test_device, data_plan_id: @data_plan.id, organization: organization)
    end

    it 'removes all devices with the data plan' do
      @data_plan.destroy
      @device.reload
      expect(@device.data_plan).to be nil
    end
  end

  describe 'data plan updates' do
    let!(:data_plan) { create(:data_plan, data_used: 50, cycle_start_day: 15) }

    it 'resets data usage if cycle start date is changed' do
      expect {
        data_plan.update(cycle_start_day: 10)
      }.to change {
        data_plan.data_used
      }.to(0)
    end

    it 'does not reset data usage if cycle start date is not changed' do
      expect {
        data_plan.update(name: 'hello')
      }.to_not change {
        data_plan.data_used
      }
    end
  end

  describe '#convert_data_limits_to_bytes' do
    before do
      @data_plan = build(:data_plan, data_limit: 5, individual_data_limit: 1)
    end

    it 'converts the set data limits from megabytes to bytes' do
      @data_plan.convert_data_limits_to_bytes
      expect(@data_plan.data_limit).to eq 5_000_000
      expect(@data_plan.individual_data_limit).to eq 1_000_000
    end
  end

  describe '#current_period_start_time' do
    let(:start_day) { 30 }
    let(:data_plan) { build(:data_plan, cycle_start_day: start_day) }
    let(:result) { data_plan.current_period_start_time }

    context 'when the cycle start time is in the same month as the current date' do
      before { Timecop.freeze(DateTime.new(2015, 1, start_day + 1)) }
      after  { Timecop.return }

      it 'returns midnight of the start day of the same month' do
        expected = DateTime.new(2015, 1, start_day)
        expect(result).to eq(expected)
      end
    end

    context 'when the cycle start time is in the previous month' do
      before do
        fake_now_time = DateTime.new(2015, 4, start_day - 1)
        allow(Time).to receive(:now).and_return(fake_now_time)
      end

      it 'returns midnight on the start day of last month' do
        expected = DateTime.new(2015, 3, start_day)
        expect(result).to eq(expected)
      end
    end

    context 'when the cycle start time is the current day' do
      before { Timecop.freeze(DateTime.new(2015, 4, start_day)) }
      after  { Timecop.return }

      it 'returns midnight today' do
        expected = DateTime.new(2015, 4, start_day)
        expect(result).to eq(expected)
      end
    end

    context 'when the current month is January and the cycle started in December' do
      before { Timecop.freeze(DateTime.new(2015, 1, start_day - 1)) }
      after  { Timecop.return }

      it 'returns midnight of December of last year' do
        expected = DateTime.new(2014, 12, start_day)
        expect(result).to eq(expected)
      end
    end

    context 'when the start day is greater than the last day of the month' do
      before { Timecop.freeze(DateTime.new(2015, 3, start_day - 1)) }
      after  { Timecop.return }

      it 'returns midnight of the last day of the month' do
        expected = DateTime.new(2015, 2, 28)
        expect(result).to eq(expected)
      end
    end
  end

  describe '#exceeds_individual_limit_for_first_time?' do
    context 'when the given device has a modem' do
      let!(:device) { create(:test_device_with_modem_association) }
      let!(:modem) { create(:device_modem, device: device) }

      context 'when the individual data limit is set' do
        before do
          @data_plan = create(:data_plan, individual_data_limit: 10, cycle_start_day: time.day)
        end

        let(:time) { Time.now.utc }
        let(:formatted_start) { Time.new(time.year, time.month, time.day) }
        let(:measurement_attributes) { { data_plan_id: @data_plan.id, start_time: formatted_start, mac: device.mac } }
        let(:result) { @data_plan.exceeds_individual_limit_for_first_time?(device, 110) }

        it 'is true when the given device data usage is greater than the individual data limit' do
          expect(result).to be true
        end
      end

      context 'when the individual data limit is not set' do
        before do
          @data_plan = create(:data_plan, individual_data_limit: nil, cycle_start_day: 2.days.ago.day)
        end

        let(:measurement_attributes) { { data_plan_id: @data_plan.id, start_time: Time.now.utc, mac: device.mac } }
        let(:result) { @data_plan.exceeds_individual_limit_for_first_time?(device, 0) }

        it 'is false' do
          expect(Measurement::CellularUtilization).not_to receive(:data_plan_usage)
          expect(result).to be false
        end
      end
    end
  end
end
