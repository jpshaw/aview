# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Carrier, type: :model do
  context '.after_create' do
    it 'calls send_creation_notification' do
      expect_any_instance_of(Carrier).to receive(:send_creation_notification).and_return(nil)
      create(:carrier)
    end
  end

  context '.before_save' do
    it 'lowercases name' do
      carrier_1 = create(:carrier, name: 'AT&T')
      expect(carrier_1.name).to eq 'at&t'
    end
  end

  context '.create' do
    before do
      @carrier_detail = create(:carrier_detail, regexp: 'at(|&)t')
    end

    it 'assigns a carrier_detail if there is match' do
      carrier = create(:carrier, name: 'AT&T')
      expect(carrier.carrier_detail).to eq @carrier_detail
    end

    it 'does not assign a carrier_detail if there is no match' do
      carrier = create(:carrier, name: 'Verizon')
      expect(carrier.carrier_detail).to be nil
    end
  end

  context '#color' do
    subject { @carrier.color }

    it 'returns carrier_detail#color when present' do
      @carrier = build(:carrier, :with_carrier_detail)
      expect(subject).to be
    end

    it 'returns nil when carrier_detail#color not present' do
      carrier_detail = create(:carrier_detail, color: nil)
      @carrier = build(:carrier, carrier_detail: carrier_detail)
      expect(subject).not_to be
    end

    it 'returns nil when carrier_detail not present' do
      @carrier = build(:carrier)
      expect(subject).not_to be
    end
  end

  context '#display_name' do
    subject { @carrier.display_name }

    let(:display_name) { 'AT&T' }
    let(:carrier_name) { 'att' }

    it 'returns carrier_detail#display_name when present' do
      carrier_detail = create(:carrier_detail, display_name: display_name)
      @carrier = build(:carrier, carrier_detail: carrier_detail, name: carrier_name)
      expect(subject).to eq(display_name)
    end

    it 'returns carrier name when carrier_detail not present' do
      @carrier = build(:carrier, name: carrier_name)
      expect(subject).to eq(carrier_name)
    end
  end
end
