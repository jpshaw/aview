# frozen_string_literal: true

require 'rails_helper'

describe SignalStrength do
  subject { described_class.__send__ :new }

  describe 'Validations' do
    describe '#enforce_singleton!' do
      before do
        subject.threshold_percentage = 50
      end

      context 'when a record already exist in the table' do
        before do
          allow(described_class).to receive(:count).and_return(1)
        end

        it 'generates an error' do
          expect(subject.errors).to be_empty
          subject.save
          expect(subject.errors).not_to be_empty
          expect(subject.errors.keys).to include(:base)
        end

        it 'does not create a record' do
          count = described_class.all.count
          subject.save
          expect(described_class.all.count).to eq(count)
        end
      end

      context 'when a record does not exist in the table' do
        before do
          allow(described_class).to receive(:count).and_return(0)
        end

        it 'does not generates an error' do
          expect(subject.errors).to be_empty
          subject.save
          expect(subject.errors).to be_empty
        end

        it 'creates a record' do
          count = described_class.all.count
          subject.save
          expect(described_class.all.count).to eq(count + 1)
        end
      end
    end
  end

  describe '.new' do
    it 'raises an error' do
      expect { described_class.new }.to raise_error(NoMethodError)
    end
  end

  describe '.instance' do
    before do
      expect(described_class.count).to eq 0
      subject.threshold_percentage = 50
    end

    context 'when a record does not exist in the table' do
      it 'returns nil' do
        expect(described_class.instance).to be nil
      end
    end

    context 'when a record already exist in the table' do
      before do
        subject.save!
        expect(described_class.count).to eq 1
      end

      it 'returns the existing table record' do
        expect(described_class.instance).to eq subject
      end
    end
  end
end
