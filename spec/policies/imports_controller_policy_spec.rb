# frozen_string_literal: true

require 'rails_helper'

describe Admin::ImportsControllerPolicy do
  subject { described_class }

  let(:imports_controller) { Admin::ImportsController.new }

  permissions :view? do
    before do
      @user = create(:user)
    end

    context 'when the imports setting is enabled' do
      before do
        Settings.imports = Config::Options.new(enabled: true)
      end

      it 'grants access' do
        expect(subject).to permit(@user, imports_controller)
      end
    end

    context 'when the imports setting is disabled' do
      before do
        Settings.imports = Config::Options.new(enabled: false)
      end

      it 'denies access' do
        expect(subject).not_to permit(@user, imports_controller)
      end
    end
  end
end
