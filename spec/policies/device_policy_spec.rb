# frozen_string_literal: true

require 'rails_helper'

describe DevicePolicy do
  subject { described_class }

  let(:organization) { Organization.new }
  let(:user)         { User.new }
  let(:device)       { build_stubbed(:device, organization: organization) }

  permissions(:modify?)         { it_behaves_like 'a device modifier' }
  permissions(:edit?)           { it_behaves_like 'a device modifier' }
  permissions(:deploy?)         { it_behaves_like 'a device deployer' }
  permissions(:remote?)         { it_behaves_like 'a device modifier' }
  permissions(:send_commands?)  { it_behaves_like 'a device commander' }
  permissions(:replace?)        { it_behaves_like 'a device replacer' }
  permissions(:import?)         { it_behaves_like 'a device importer' }

  permissions :change_organization? do
    it_behaves_like 'a device modifier'
  end

  context 'With policy scope' do
    before do
      # We start with 2 devices in the DB and the user not managing any devices and scoped (with
      # restricted access).
      create_list(:device_with_random_regular_organization, 2)
      expect(Device.count).to be(2)
      expect(user.devices.count).to be(0)
    end

    it 'returns no devices if user is scoped and does not manage devices' do
      # User is still scoped here and still does not manage any accounts.
      expect(Pundit.policy_scope(user, Device).count).to eq(0)
    end

    it 'returns all devices managed by user if user is scoped and manages devices' do
      # User is still scoped here and stub its managed devices to be the first device.
      allow(user).to receive(:devices).and_return([Device.first])
      expect(Pundit.policy_scope(user, Device).count).to eq(1)
    end
  end
end
