# frozen_string_literal: true

require 'rails_helper'

describe DeviceConfigurationPolicy do
  subject { described_class }

  let(:user)                  { User.new                }
  let(:device_configuration)  { DeviceConfiguration.new }

  permissions(:modify?)                 { it_behaves_like 'a device configuration modifier' }
  permissions(:show?)                   { it_behaves_like 'a device configuration modifier' }
  permissions(:new?)                    { it_behaves_like 'a device configuration modifier' }
  permissions(:create?)                 { it_behaves_like 'a device configuration modifier' }
  permissions(:edit?)                   { it_behaves_like 'a device configuration modifier' }
  permissions(:update?)                 { it_behaves_like 'a device configuration modifier' }
  permissions(:destroy?)                { it_behaves_like 'a device configuration modifier' }
  permissions(:edit_linked_devices?)    { it_behaves_like 'a device configuration modifier' }
  permissions(:update_linked_devices?)  { it_behaves_like 'a device configuration modifier' }
  permissions(:copy?)                   { it_behaves_like 'a device configuration modifier' }

  # TODO: Restore thee tests after we move from paperclip to carrierwave.
  context 'With policy scope', broken: true do
    before do
      # We start with 2 device configurations in the DB and the user not managing any of them and
      # scoped (with restricted access).
      create(:device_configuration)
      expect(DeviceConfiguration.count).to be(1)
      expect(user.device_configurations.count).to be(0)
    end

    it 'returns no device configurations if user is scoped and does not manage device configurations' do
      # User is still scoped here and still does not manage any device configurations.
      expect(Pundit.policy_scope(user, DeviceConfiguration).count).to eq(0)
    end

    it 'returns all device configurations managed by user if user is scoped and manages device configurations' do
      # User is still scoped here and stub its managed device configurations to be the first one in DB.
      allow(user).to receive(:device_configurations).and_return([DeviceConfiguration.first])
      expect(Pundit.policy_scope(user, DeviceConfiguration).count).to eq(1)
    end
  end
end
