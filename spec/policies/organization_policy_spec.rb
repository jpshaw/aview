# frozen_string_literal: true

require 'rails_helper'

describe OrganizationPolicy do
  subject { described_class }

  let(:user)          { User.new }
  let(:organization)  { Organization.new }

  permissions(:new?)            { it_behaves_like 'an organization modifier' }
  permissions(:create?)         { it_behaves_like 'an organization modifier' }
  permissions(:modify?)         { it_behaves_like 'an organization modifier' }
  permissions(:destroy?)        { it_behaves_like 'an organization destroyer' }
  permissions(:modify_users?)   { it_behaves_like 'an organization user modifier' }
  permissions(:create_users?)   { it_behaves_like 'an organization user modifier' }

  context 'with policy scope' do
    before do
      # We start with 2 organizations in the DB and the user not managing any organization.
      create_list(:random_regular_organization, 2)
      expect(Organization.count).to be(2)
      expect(user.organizations.count).to be(0)
    end

    it 'returns no organizations if user does not manage organizations' do
      expect(Pundit.policy_scope(user, Organization).count).to eq(0)
    end

    it 'returns all organizations managed by user if the user manages organizations' do
      # Stub users managed organizations to be the first organization.
      allow(user).to receive(:permitted_resources_of_type).and_return([Organization.first])
      expect(Pundit.policy_scope(user, Organization).count).to eq(1)
    end
  end
end
