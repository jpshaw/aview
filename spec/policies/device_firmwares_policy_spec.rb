# frozen_string_literal: true

require 'rails_helper'

describe DeviceFirmwarePolicy do
  subject { DeviceConfigurationPolicy }

  let(:user) { User.new }

  describe '#assign_non_production?' do
    let(:ability) { 'Assign Non-Production Firmwares' }

    context 'with ability to assign non-production firmwares' do
      before do
        allow(user).to receive(:has_ability?).with(ability).and_return(true)
      end

      it 'returns true' do
        expect(Pundit.policy(user, DeviceFirmware).assign_non_production?).to be true
      end
    end

    context 'without ability to assign non-production firmwares' do
      before do
        allow(user).to receive(:has_ability?).with(ability).and_return(false)
      end

      it 'returns false' do
        expect(Pundit.policy(user, DeviceFirmware).assign_non_production?).to be false
      end
    end
  end
end
