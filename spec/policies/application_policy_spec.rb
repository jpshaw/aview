# frozen_string_literal: true

require 'rails_helper'

describe ApplicationPolicy do
  subject { described_class }

  let(:user) { User.new }

  describe '#user_has_ability?' do
    let(:ability) { 'Test Ability' }

    context 'when the record exists' do
      let(:record) { create(:organization) }

      before do
        @policy = subject.new(user, record)
      end

      context 'when the ability is nil' do
        it 'returns false' do
          expect(@policy.user_has_ability?(nil)).to be false
        end
      end

      context 'when the user does not have the ability on the record' do
        before do
          allow(user).to receive(:has_ability_on?).with(record, to: ability).and_return(false)
        end

        it 'returns false' do
          expect(@policy.user_has_ability?(ability)).to be false
        end
      end

      context 'when the user has the ability on the record' do
        before do
          allow(user).to receive(:has_ability_on?).with(record, to: ability).and_return(true)
        end

        it 'returns true' do
          expect(@policy.user_has_ability?(ability)).to be true
        end
      end
    end

    context 'when the record is nil' do
      let(:record) { nil }

      before do
        @policy = subject.new(user, record)
      end

      context 'when the user has the ability' do
        before do
          allow(user).to receive(:has_ability_on?).with(record, to: ability).and_return(true)
        end

        it 'returns false' do
          expect(@policy.user_has_ability?(ability)).to be false
        end
      end
    end

    context 'when the record does not exist' do
      context 'when the record is a manageable class' do
        before do
          @policy = subject.new(user, subject)
          allow(user).to receive(:manageables)
            .with(of_type: Permission::MANAGEABLE_TYPE_ORGANIZATION,
                  for_ability: ability)
            .and_return([1, 2, 3])
        end

        it 'returns true' do
          expect(user.manageables(of_type: Permission::MANAGEABLE_TYPE_ORGANIZATION,
                                  for_ability: ability)).not_to be_empty
          expect(@policy.user_has_ability?(ability)).to be true
        end
      end
    end
  end

  describe '#root_user?' do
    before do
      @root = create(:organization)
      @user = create(:user, organization: @root)
    end

    context 'when the user has no managed organizations' do
      it 'returns false' do
        expect(@user.organizations.count).to eq 0
        expect(subject.new(@user, nil).root_user?).to be false
      end
    end

    context 'when the user can not manage the root organization' do
      before do
        @organization = create(:random_regular_organization)
        @root.add_child(@organization)
        Permission.create(manager: @user, manageable: @organization, role: Role.new)
      end

      it 'returns false' do
        expect(@user.organizations.count).to be > 0
        expect(subject.new(@user, nil).root_user?).to be false
      end
    end

    context 'when the user can manage the root organization' do
      before do
        Permission.create(manager: @user, manageable: @root, role: Role.new)
      end

      it 'returns true' do
        expect(@user.organizations.count).to be > 0
        expect(subject.new(@user, nil).root_user?).to be true
      end
    end
  end
end
