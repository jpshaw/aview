# frozen_string_literal: true

require 'rails_helper'

describe AdminControllerPolicy do
  subject { described_class }

  let(:admin_controller) { AdminController.new }

  permissions :view? do
    context 'when the admin setting is enabled' do
      before do
        Settings.admin = Config::Options.new(enabled: true)
      end

      context 'when the user does not belong to root' do
        it 'denies access' do
          user = create(:non_root_user)
          expect(user.organization.root?).to be(false)
          expect(subject).not_to permit(user, admin_controller)
        end
      end

      context 'when the user belongs to root' do
        it 'denies access' do
          user = create(:root_user)
          expect(user.organization.root?).to be(true)
          expect(subject).not_to permit(user, admin_controller)
        end
      end
    end

    context 'when the admin setting is disabled' do
      before do
        Settings.admin = Config::Options.new(enabled: false)
      end

      context 'when the user does not belong to root' do
        it 'denies access' do
          user = create(:non_root_user)
          expect(user.organization.root?).to be(false)
          expect(subject).not_to permit(user, admin_controller)
        end
      end

      context 'when the user belongs to root' do
        it 'denies access' do
          user = create(:root_user)
          expect(user.organization.root?).to be(true)
          expect(subject).not_to permit(user, admin_controller)
        end
      end
    end
  end
end
