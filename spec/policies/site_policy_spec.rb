# frozen_string_literal: true

require 'rails_helper'

describe SitePolicy do
  subject { described_class }

  let(:user)  { User.new }
  let(:site)  { Site.new }

  permissions(:edit?)   { it_behaves_like 'a site modifier' }
  permissions(:update?) { it_behaves_like 'a site modifier' }
  permissions(:delete?) { it_behaves_like 'a site modifier' }

  context 'With policy scope' do
    before do
      # We start with 2 sites in the DB and the user not managing any sites and scoped (with
      # restricted access).
      expect(Site.count).to be(0)
      create_list(:site_with_random_regular_organization, 2)
      # The organization created a default site when it was born.
      # The other, we explicitely created.
      expect(Site.count).to be(4)
      expect(user.sites.count).to be(0)
    end

    it 'returns no sites if user is scoped and does not manage sites' do
      # User is still scoped here and still does not manage any accounts.
      expect(Pundit.policy_scope(user, Site).count).to eq(0)
    end

    it 'returns all sites managed by user if user is scoped and manages sites' do
      # User is still scoped here and stub its managed accounts to be the first account.
      allow(user).to receive(:sites).and_return([Site.first])
      expect(Pundit.policy_scope(user, Site).count).to eq(1)
    end
  end
end
