# frozen_string_literal: true

require 'rails_helper'

describe GatewayPolicy do
  subject { described_class }

  let(:user) { User.new }

  context 'With policy scope' do
    before do
      # We start with 2 Gateway devices in the DB and the user not managing any devices and scoped
      # (with restricted access).
      create_list(:gateway_device, 2)
      expect(Device.count).to be(2)

      expect(user.devices.count).to be(0)
    end

    it 'returns no devices if user is scoped and does not manage devices' do
      # User is still scoped here and still does not manage any devices.
      expect(Pundit.policy_scope(user, Gateway).count).to eq(0)
    end

    it 'returns all devices managed by user if user is scoped and manages devices' do
      # User is still scoped here and stub its managed devices to be the first device.
      allow(user).to receive(:devices).and_return([Gateway.first])
      expect(Pundit.policy_scope(user, Gateway).count).to eq(1)
    end
  end
end
