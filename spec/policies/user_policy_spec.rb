# frozen_string_literal: true

require 'rails_helper'

describe UserPolicy do
  subject { described_class }

  let(:user)          { User.new }
  let(:managed_user)  { User.new }
  let(:organization)  { Organization.new }

  permissions(:show?)    { it_behaves_like 'a user modifier' }
  permissions(:new?)     { it_behaves_like 'a user modifier' }
  permissions(:create?)  { it_behaves_like 'a user modifier' }
  permissions(:edit?)    { it_behaves_like 'a user modifier' }
  permissions(:update?)  { it_behaves_like 'a user modifier' }
  permissions(:destroy?) { it_behaves_like 'a user modifier' }

  context 'with policy scope' do
    before do
      # We start with 2 users in the DB and the user not managing any users.
      create_list(:user_with_random_regular_organization, 2)
      expect(User.count).to be(2)
      expect(user.users.count).to be(0)
    end

    it 'returns no users if user does not manage users' do
      # User does not manage any users.
      expect(Pundit.policy_scope(user, User).count).to eq(0)
    end

    it 'returns all users managed by user' do
      # User manages other users.
      allow(user).to receive(:users).and_return([User.first])
      expect(Pundit.policy_scope(user, User).count).to eq(1)
    end
  end
end
