# frozen_string_literal: true

require 'rails_helper'

describe DeviceEventPolicy do
  subject { described_class }

  let(:user) { User.new }

  context 'With policy scope' do
    before do
      # We start with 2 devices, each with one event in the DB and the user not managing any devices
      # and scoped (with restricted access).
      create_list(:event_with_device_with_random_regular_organization, 2)
      expect(Device.count).to       be(2)
      expect(DeviceEvent.count).to  be(2)
      Device.all.each { |d| expect(d.events.count).to be(1) }

      expect(user.devices.count).to be(0)
    end

    it 'returns no device events if user is scoped and does not manage devices' do
      # User is still scoped here and still does not manage any devices.
      expect(Pundit.policy_scope(user, DeviceEvent).count).to eq(0)
    end
  end
end
