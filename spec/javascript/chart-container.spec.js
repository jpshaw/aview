import ChartContainer from 'vvig-pnc/chart-container.vue'
import { mount } from '@vue/test-utils'
import VueTestUtils from '@vue/test-utils'

var mountChartContainer = (vlans) => {
  return mount( ChartContainer, {
    stubs: ['chart-group'],
    propsData: {
      organization: "Test Account",
      showDetailed: false,
      chartData: {
        account_id: "Test",
        vlans: vlans
      }
    }
  })
}

describe('chart-container.vue', () => {
  describe("Chart Groups", () => {
    it("does not render when vlans are empty", () => {
      let cmp = mountChartContainer([])
      expect(cmp.findAll('chart-group-stub').length).toBe(0)
    })

    it("renders the correct number when vlans are not empty", () => {
      let cmp = mountChartContainer(["test", "testing"])
      expect(cmp.findAll('chart-group-stub').length).toBe(2)
    })
  })

  describe("Container Title", () => {
    it("render the correct title", () => {
      let cmp = mountChartContainer([])
      expect(cmp.text()).toMatch("Test Account VIG Utilization Reports")
    })
  })
})
