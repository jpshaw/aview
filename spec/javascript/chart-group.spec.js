import ChartGroup from 'vvig-pnc/chart-group.vue'
import { mount } from '@vue/test-utils'
import VueTestUtils from '@vue/test-utils'

var mountChartGroup = (vigs) => {
  return mount( ChartGroup, {
    stubs: ['chart'],
    propsData: {
      organization: "Test Account",
      showDetailed: false,
      chartData: {
        account_id: "Test",
        vigs: vigs
      }
    }
  })
}

describe('chart-container.vue', () => {
  describe("Charts", () => {
    it("does not render when vigs are empty", () => {
      let cmp = mountChartGroup([])
      expect(cmp.findAll('chart-stub').length).toBe(0)
    })

    it("renders the correct number of charts when vigs are not empty", () => {
      let cmp = mountChartGroup(["test", "testing"])
      expect(cmp.findAll('chart-stub').length).toBe(2)
    })
  })

  describe("Chart Group Title", () => {
    it("render the correct title", () => {
      let cmp = mountChartGroup([])
      expect(cmp.text()).toMatch("VIG Utilization Report - Test Account")
    })
  })
})
