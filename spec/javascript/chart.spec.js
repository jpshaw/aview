import Chart from 'charts/chart.vue'
import { mount } from '@vue/test-utils'
import VueTestUtils from '@vue/test-utils'

describe("chart.vue", () => {
  let cmp, vm

  beforeEach(() => {
    cmp = mount(Chart, {
      propsData: {
        vlan: "TESTAB",
        showDetailed: false,
        chartData: {
          vig_id: "15",
          vig_location: "Some Location, USA",
          data: {
            capacity: [
              [1534046400000, 235],
              [1534068000000, 255],
              [1534089600000, 275]
            ],
            txMax: [
              [1534046400000, 215],
              [1534068000000, 211],
              [1534089600000, 287]
            ],
            rxMax: [
              [1534046400000, 222],
              [1534068000000, 288],
              [1534089600000, 356]
            ],
            rxAvg: [
              [1534046400000, 222],
              [1534068000000, 288],
              [1534089600000, 356]
            ],
            txAvg: [
              [1534046400000, 222],
              [1534068000000, 288],
              [1534089600000, 356]
            ]
          }
        }
      },
      stubs: ["highcharts"]
    })
  })

  describe("Chart", () => {
    it("renders a single chart", () => {
      expect(cmp.findAll("highcharts-stub").length).toBe(1)
    })
  })

  describe("Highcharts Options", () => {
    it("returns the proper title", () => {
      let title = cmp.vm.getChartTitle()
      expect(title).toBe("TESTAB-15 (Some Location, USA)")
    })

    it("returns expected chart data set", () => {
      let dataset = cmp.vm.getChartData()
      let keys = Object.keys(dataset)
      expect(keys).toContain("capacity")
      expect(keys).toContain("txMax")
      expect(keys).toContain("rxMax")
    })

    it("contains expected series configuration", () => {
      expect(cmp.vm.chartOptions.series.length).toBe(5)
      let capacityData = cmp.vm.chartOptions.series.filter( (item) => {
        return item.name === "capacity"
      })[0]
      expect(capacityData.step).toBe(true)
      expect(capacityData.type).toBe("line")
      expect(capacityData.color).toBe("#FF355E")
      let txMaxData = cmp.vm.chartOptions.series.filter( (item) => {
        return item.name === "max egress"
      })[0]
      expect(txMaxData.type).toBe("line")
      expect(txMaxData.color).toBe("#87CEFF")
      let rxMaxData = cmp.vm.chartOptions.series.filter( (item) => {
        return item.name === "max ingress"
      })[0]
      expect(rxMaxData.type).toBe("line")
      expect(rxMaxData.color).toBe("#A9A9A9")
      for(var outer = 0; outer < 3; outer++){
        for(var inner = 0; inner < 3; inner ++){
          expect(capacityData.data[outer][inner]).toBe(cmp.vm.chartData.data.capacity[outer][inner])
          expect(txMaxData.data[outer][inner]).toBe(cmp.vm.chartData.data.txMax[outer][inner])
          expect(rxMaxData.data[outer][inner]).toBe(cmp.vm.chartData.data.rxMax[outer][inner])
        }
      }
    })
  })
})