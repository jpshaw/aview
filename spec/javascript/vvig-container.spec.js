import VvigContainer from 'vvig-pnc/vvig-container.vue'
import ChartContainer from 'vvig-pnc/chart-container'
import { mount } from '@vue/test-utils'
import VueTestUtils from '@vue/test-utils'

describe('vvig-container.vue', () => {
  let cmp, vm

  beforeEach(() => {
    global.AV = {
      UTIL: {
        AJAX: {
          get: function(url, params, cb){
            var response = {
              data: [
                {
                  "organization_id": 123,
                  "vlans": []
                },
                {
                  "organization_id": 345,
                  "vlans": []
                }
              ]
            };
            cb(response);
          }
        }
      }
    }

    cmp = mount(VvigContainer, {
      propsData: {
        organizations: [{
          value: 123,
          text: "Selected Org"
        }]
      },
      stubs: [
        'chart-container',
        'vue-select'
      ]
    });
  })

  describe('Ready', () => {
    it("returns false when no account is selected and no data present", () => {
      expect(cmp.vm.ready).toBe(false);
    })

    it("returns false when no account selected and data present", () => {
      cmp.vm.chartData = [
        {
          "organization_id": 123,
          "vlans": []
        }
      ]
      expect(cmp.vm.ready).toBe(false);
    })

    it("returns true when account selected and data present", () => {
      cmp.vm.selectedOrg.push("Selected Org")
      expect(cmp.vm.ready).toBe(true);
    })
  })

  describe('Chart Container', () => {
    it("does not render when no account and no data", () => {
      expect(cmp.findAll('chart-container-stub').length).toBe(0)
    })

    it("does not render when no account selected and data present", () => {
      cmp.vm.chartData = [
        {
          "organization_id": 123,
          "vlans": []
        }
      ]
      expect(cmp.findAll('chart-container-stub').length).toBe(0)
    })

    it("renders when account selected and data present", () => {
      cmp.vm.selectedOrg.push("Selected Org")
      cmp.vm.$nextTick(() => {
        expect(cmp.findAll('chart-container-stub').length).toBe(2)
      })
    })
  })
});