# frozen_string_literal: true

require 'rails_helper'

describe GeolocatorProcessor do
  it 'tries to find the device modem' do
    expect(DeviceModem).to receive(:find_by)
    subject.on_message({})
  end

  context 'when the device modem is found' do
    let(:device) { create(:test_device) }
    let(:device_modem) { build(:device_modem, device: device, mcc: 310, mnc: 410) }

    before do
      allow(DeviceModem).to receive(:find_by).and_return(device_modem)
    end

    it 'creates a device event' do
      allow(Geolocator).to receive(:get_location).and_return(Geolocator.new(latitude: 0, longitude: 0, accuracy: 0))
      expect {
        subject.on_message(lac: '21991', cid: '66126862', mcc: 310, mnc: 410)
      }.to change { DeviceEvent.count }.by(1)
    end

    it 'tries to find an existing cellular location' do
      expect(CellularLocation).to receive(:where).and_return([])
      subject.on_message({})
    end

    context 'when a cellular location exists' do
      let(:new_cellular_location) { build(:cellular_location) }

      before do
        allow(CellularLocation).to receive(:where).and_return([new_cellular_location])
      end

      it 'does not call the geolocation service' do
        expect(Geolocator).not_to receive(:get_location)
        subject.on_message({})
      end

      it 'sets the modem location to the existing cellular location' do
        expect(device_modem.cellular_location).to be_nil
        subject.on_message({})
        expect(device_modem.cellular_location).to eq(new_cellular_location)
      end
    end

    context 'when a matching cellular location does not exist' do
      it 'checks if the geolocator can get the cell location' do
        expect(Geolocator).to receive(:can_get_cell_location?)
        subject.on_message({})
      end

      it 'gets the location from the geolocation service' do
        expect(Geolocator).to receive(:get_location)
        subject.on_message(lac: '21991', cid: '66126862', mcc: 310, mnc: 410)
      end

      it 'creates a new cellular location' do
        allow(Geolocator).to receive(:get_location).and_return(Geolocator.new(latitude: 0, longitude: 0, accuracy: 0))
        expect { subject.on_message(lac: '21991', cid: '66126862', mcc: 310, mnc: 410) }.to change(CellularLocation, :count).by(1)
      end
    end
  end

  context 'when the device is not found' do
    before do
      allow(DeviceModem).to receive(:find_by).and_return(nil)
    end

    it 'does not try to find a cellular location' do
      expect(CellularLocation).not_to receive(:find_by_modem)
      expect(Geolocator).not_to receive(:get_location)
      subject.on_message({})
    end
  end
end
