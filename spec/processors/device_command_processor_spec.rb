# frozen_string_literal: true

require 'rails_helper'

describe DeviceCommandProcessor do
  it 'tries to find the device' do
    expect(Device).to receive(:find_by)
    subject.on_message({})
  end

  context 'when the device is found' do
    let(:device) { Device.new }

    before do
      allow(Device).to receive(:find_by).and_return(device)
    end

    it 'sends the command' do
      expect(device).to receive(:send_command)
      subject.on_message({})
    end
  end

  context 'when the device is not found' do
    before do
      allow(Device).to receive(:find_by).and_return(nil)
      allow_any_instance_of(Device).to receive(:send_command)
    end

    it 'does not send the command' do
      expect_any_instance_of(Device).not_to receive(:send_command)
      subject.on_message({})
    end
  end
end
