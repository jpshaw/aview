# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'a firmware file url uploader' do
  context 'When the firmware file for the firmware exists' do
    before do
      allow_any_instance_of(DeviceFirmware).to receive_message_chain(:firmware_file, :file, :exists?).and_return(true)
    end

    it 'does not assign a firmware file url to the firmware' do
      expect_any_instance_of(DeviceFirmware).not_to receive(:remote_firmware_file_url=)
      subject.on_message({}) # arguments)
    end
  end

  context 'When the firmware file for the firmware does not exist' do
    before do
      allow_any_instance_of(DeviceFirmware).to receive_message_chain(:firmware_file, :file, :exists?).and_return(false)
    end

    it 'assigns a firmware file url to the firmware' do
      expect_any_instance_of(DeviceFirmware).to receive(:remote_firmware_file_url=)
      subject.on_message({}) # arguments)
    end
  end
end

shared_examples_for 'a schema file url uploader' do
  context 'When there is a schema file for the firmware' do
    before do
      allow(subject).to receive(:schema_file?).and_return(true)
    end

    context 'When the schema file for the firmware exists' do
      before do
        allow_any_instance_of(DeviceFirmware).to receive_message_chain(:schema_file, :file, :exists?).and_return(true)
      end

      it 'does not assign a schema file url to the firmware' do
        expect_any_instance_of(DeviceFirmware).not_to receive(:remote_schema_file_url=)
        subject.on_message({}) # arguments)
      end
    end

    context 'When the schema file for the firmware does not exist' do
      before do
        allow_any_instance_of(DeviceFirmware).to receive_message_chain(:schema_file, :file, :exists?).and_return(false)
      end

      it 'assigns a schema file url to the firmware' do
        expect_any_instance_of(DeviceFirmware).to receive(:remote_schema_file_url=)
        subject.on_message({}) # arguments)
      end
    end
  end

  context 'When there is no schema file for the firmware' do
    before do
      allow(subject).to receive(:schema_file?).and_return(false)
    end

    it 'does not assign a schema url to the firmware' do
      expect_any_instance_of(DeviceFirmware).not_to receive(:remote_schema_file_url=)
      subject.on_message({}) # arguments)
    end
  end
end

describe FirmwareSyncProcessor do
  context 'When model does not exist' do
    before do
      expect(DeviceModel).to receive(:find_by).and_return(nil)
    end

    it 'logs an error if model cannot be found' do
      expect(subject).to receive(:log_error)
      subject.on_message(device_model_id: nil)
    end

    it 'does not sync firmware' do
      expect(subject).not_to receive(:sync_firmware)
      subject.on_message({})
    end
  end

  context 'When model exists' do
    let(:device_model)  { create(:test_model) }
    let(:api_firmware)  do
      {
        'firmware' => {
          'firmware_version'    => '10.10.1',
          'schema_version'      => 1,
          'migration_versions'  => '~1~',
          'production'          => true,
          'deprecated'          => true
        }
      }
    end

    before do
      expect(DeviceModel).to receive(:find_by).and_return(device_model)
    end

    it 'syncs firmware' do
      expect(subject).to receive(:sync_firmware)
      subject.on_message({})
    end

    it 'retrieves firmware from API' do
      expect(subject).to receive(:api_firmware)
      subject.on_message({})
    end

    context 'When the call to the API fails' do
      before do
        expect(subject).to receive(:call_api).and_raise
      end

      it 'does not save a firmware' do
        expect_any_instance_of(DeviceFirmware).not_to receive(:update_attributes)
        expect_any_instance_of(DeviceFirmware).not_to receive(:save!)
        subject.on_message({})
      end

      it 'logs an error' do
        expect(subject).to receive(:log_error)
        subject.on_message({})
      end
    end

    context 'When the firmware exists' do
      let(:device_firmware) { create(:device_firmware, device_model_id: device_model.id) }

      before do
        allow(subject).to receive(:api_firmware).and_return(api_firmware)
        allow(subject).to receive(:firmware_url).and_return('a_firmware_url')
        allow(subject).to receive(:schema_url).and_return('a_schema_url')
        allow(device_model).to receive_message_chain(:device_firmwares, :find_by_version).and_return(device_firmware)
      end

      it 'is updated' do
        expect(device_firmware).to receive(:update_attributes)
        subject.on_message({})
      end

      it_behaves_like 'a firmware file url uploader'
      it_behaves_like 'a schema file url uploader'
    end

    context 'When firmware does not exist' do
      before do
        allow(subject).to receive(:api_firmware).and_return(api_firmware)
        allow(subject).to receive(:firmware_url).and_return('a_firmware_url')
        allow(subject).to receive(:schema_url).and_return('a_schema_url')

        # We need to stub 'save!' here because the uploaders will fail validations.
        expect_any_instance_of(DeviceFirmware).to receive(:save!)
      end

      it_behaves_like 'a firmware file url uploader'
      it_behaves_like 'a schema file url uploader'

      it 'saves a firmware' do
        # The test of 'save!' being called is done in the 'before :each' block, in the mock, needed in other tests.
        subject.on_message({})
      end

      it 'does not log an error' do
        expect(subject).not_to receive(:log_error)
        subject.on_message({})
      end
    end
  end
end
