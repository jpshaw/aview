# frozen_string_literal: true

require 'rails_helper'

describe ImportProcessor do
  context 'when the given import exists' do
    let(:import) { build(:qa_import) }

    before do
      allow(Import).to receive(:find_by).and_return(import)
    end

    it 'runs the import' do
      expect(import).to receive(:run!)
      subject.on_message({})
    end

    it 'does not send any alert emails' do
      import_mailer = class_double('ImportMailer').as_stubbed_const(transfer_nested_constants: true)
      expect(import_mailer).not_to receive(:multiple_imports_running)
      expect(import_mailer).not_to receive(:long_runner)
      subject.on_message({})
    end

    context 'when the import takes longer than the long running time' do
      before do
        Settings.imports.long_running_time = 0.01
      end

      it 'allows the import to finish' do
        expect(import).to receive(:success!)
        subject.on_message({})
      end
    end

    context 'when the import is faster than the long running time' do
      before do
        Settings.imports.long_running_time = 2.hours
      end

      it 'does not send an email' do
        import_mailer = class_double('ImportMailer').as_stubbed_const(transfer_nested_constants: true)
        expect(import_mailer).not_to receive(:long_runner)
        subject.on_message({})
      end

      it 'allows the import to finish' do
        expect(import).to receive(:success!)
        subject.on_message({})
      end
    end

    context 'when the import throws an error' do
      before do
        Settings.imports.long_running_time = 1.second
        expect_any_instance_of(Import).to receive(:run!).and_raise('Data import job failed with error')
      end

      it 'does not send an email' do
        import_mailer = class_double('ImportMailer').as_stubbed_const(transfer_nested_constants: true)
        expect(import_mailer).not_to receive(:long_runner)
        subject.on_message({})
        sleep(1) # give the email time to trigger
      end
    end

    context 'when an import starts while another import is running/initializing' do
      before do
        # default state of an import is "initializing"
        Import.create!(import_file: File.open(Rails.root.join('spec', 'support', 'fixtures', 'att_qa_smx_netgate_list.tab').to_s))
      end

      it 'allows the import to finish' do
        expect(import).to receive(:success!)
        subject.on_message({})
      end
    end
  end
end
