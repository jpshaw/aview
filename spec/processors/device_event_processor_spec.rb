# frozen_string_literal: true

require 'rails_helper'

describe DeviceEventProcessor do
  let(:processor) { described_class.new }
  let(:mac)       { 'abcdef012345' }
  let(:device)    { Device.new(mac: mac) }

  describe '#on_message' do
    let(:json_string) { { 'mac' => mac }.to_json }

    it 'uses logs on errors' do
      allow(JSON).to receive(:parse).and_raise(StandardError)
      expect(processor.logger).to receive(:error)
      processor.on_message(json_string)
    end

    it 'logs an error with an invalid event' do
      allow_any_instance_of(Platforms::Event).to receive(:valid?).and_return(false)
      expect(processor.logger).to receive(:error)
      processor.on_message('')
    end

    it 'returns nil when device cannot be found for the mac value' do
      expect(processor.on_message(json_string)).to be nil
    end

    it 'delegates to the device to process the event when the device is found' do
      allow(Device).to receive(:find_by).and_return(device)
      expect(device).to receive(:heartbeat!).and_return(nil)
      expect(device).to receive(:process_event)
      processor.on_message(json_string)
    end

    context 'when the device is not found' do
      before do
        allow(Device).to receive(:find_by).and_return(nil)
      end

      it 'logs a warning with the device mac' do
        expect(processor.logger).to receive(:warn).with(/#{mac}/)
        processor.on_message(json_string)
      end
    end
  end
end
