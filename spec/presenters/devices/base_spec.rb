# frozen_string_literal: true

require 'rails_helper'

describe Presenters::Devices::Base do
  before do
    allow(view).to receive(:current_user) { create(:user) }
    @presenter = described_class.new(view)
  end

  describe '#column_data' do
    context 'with direction' do
      subject { @presenter.column_data(column)[:direction] }

      let(:column) do
        {
          direction: 'asc',
          name: 'my_col',
          sort_scope: 'my_scope'
        }
      end

      it 'uses direction if provided' do
        expect(subject).to eq(column[:direction])
      end
    end

    context 'without direction' do
      subject { @presenter.column_data(column)[:direction] }

      let(:column) do
        {
          name: 'my_col',
          sort_scope: 'my_scope'
        }
      end

      it 'uses direction if provided' do
        expect(subject).to be nil
      end
    end
  end

  describe '#facets' do
    it 'includes the specified keys' do
      expect(@presenter.facets.keys).to include(:category, :deployment, :model, :status)
    end

    context '[:category] value' do
      subject { @presenter.facets[:category][:facet_values].keys }

      let(:category_value) { DeviceCategory::GATEWAY_CATEGORY }

      before do
        DeviceCategory.create!(name: category_value, class_name: category_value)
      end

      it 'is a device_category name' do
        expect(subject).to include(category_value)
      end
    end

    context '[:model] value' do
      subject { @presenter.facets[:model][:facet_values].keys }

      let(:model_value) { @model.model_version }

      before do
        @model = create(:test_model)
      end

      it 'is a model name' do
        expect(subject).to include(model_value)
      end
    end
  end
end
