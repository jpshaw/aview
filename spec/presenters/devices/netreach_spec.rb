# frozen_string_literal: true

require 'rails_helper'
require Rails.root.join('app/presenters/devices/netreach')

describe Presenters::Devices::Netreach do
  it_behaves_like 'a device subcategory presenter'
end
