# frozen_string_literal: true

require 'rails_helper'
require Rails.root.join('app/presenters/devices/gateway')

describe Presenters::Devices::Gateway do
  it_behaves_like 'a device subcategory presenter'
end
