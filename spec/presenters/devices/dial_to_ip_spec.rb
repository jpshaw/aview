# frozen_string_literal: true

require 'rails_helper'
require Rails.root.join('app/presenters/devices/dial_to_ip')

describe Presenters::Devices::DialToIp do
  it_behaves_like 'a device subcategory presenter'
end
