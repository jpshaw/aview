# frozen_string_literal: true

require 'rails_helper'
require Rails.root.join('app/presenters/devices/cellular')

describe Presenters::Devices::Cellular do
  it_behaves_like 'a device subcategory presenter'
end
