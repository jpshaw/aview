# frozen_string_literal: true

require 'rails_helper'

describe Presenters::Reports::New do
  describe '#device_categories' do
    before do
      create_all_categories
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'includes the dial_to_ip, remote_manager, ucpe, and gateway categories' do
      expect(subject.device_categories).to include @dial_to_ip_category
      expect(subject.device_categories).to include @gateway_category
      expect(subject.device_categories).not_to include @netbridge_category
      expect(subject.device_categories).not_to include @netreach_category
      expect(subject.device_categories).to include @remote_manager_category
      expect(subject.device_categories).to include @ucpe_category
    end
  end

  describe '#device_categories_for_select' do
    before do
      @dial_to_ip_category = create(:dial_to_ip_category)
      @remote_manager_category = create(:remote_manager_category)
      @ucpe_category = create(:ucpe_category)
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'returns a collection of device categories usable for a select' do
      expected_result = [[@dial_to_ip_category.name, @dial_to_ip_category.id],
                         [@remote_manager_category.name, @remote_manager_category.id],
                         [@ucpe_category.name, @ucpe_category.id]]
      result = subject.device_categories_for_select
      expect(result).to eq expected_result
    end
  end

  describe '#device_models_for_select' do
    before do
      create_remote_manager_category_and_models
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'returns a collection of device models with their category usable for a select' do
      expected_result = [[@remote_manager_category.name,
                          [[@remote_manager_5400_RM.name, @remote_manager_5400_RM.id]]]]
      result = subject.device_models_for_select
      expect(result).to eq expected_result
    end
  end

  describe '#report_count' do
    before do
      create_user
      @report = create(:device_report, user: @user)
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'returns the total number of reports for the given user' do
      result = subject.report_count @user
      expect(result).to eq 1
    end
  end
end
