# frozen_string_literal: true

require 'rails_helper'

describe Presenters::Reports::Vig do
  describe '#device_categories' do
    before do
      create_all_categories
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'includes only the gateway category' do
      expect(subject.device_categories).not_to include @dial_to_ip_category
      expect(subject.device_categories).to include @gateway_category
      expect(subject.device_categories).not_to include @netbridge_category
      expect(subject.device_categories).not_to include @netreach_category
      expect(subject.device_categories).not_to include @remote_manager_category
      expect(subject.device_categories).not_to include @ucpe_category
    end
  end
end
