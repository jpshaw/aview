# frozen_string_literal: true

require 'rails_helper'

describe Presenters::Reports::Availability do
  describe '#device_categories' do
    before do
      create_all_categories
    end

    let(:subject) { @presenter = described_class.new(view) }

    it 'returns all device categories in order by name' do
      expect(subject.device_categories.first).to eq @dial_to_ip_category
      expect(subject.device_categories.second).to eq @netbridge_category
      expect(subject.device_categories.third).to eq @remote_manager_category
      expect(subject.device_categories.fourth).to eq @ucpe_category
      expect(subject.device_categories.fifth).to eq @gateway_category
    end
  end
end
