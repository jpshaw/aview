# frozen_string_literal: true

require 'rails_helper'

describe ConfigurationsHelper do
  let(:user) { create(:user) }

  before do
    allow(helper).to receive(:current_user).and_return(user)
  end

  describe '#grouped_configurations' do
    context 'when given nil' do
      it 'returns an optgroup array with only `No Configuration`' do
        grouped = helper.grouped_configurations(nil)
        expect(grouped).to eq [['Select configuration ...', [['No Configuration', nil]]]]
      end
    end

    context 'when given a list of configurations' do
      before do
        @organization   = user.organization
        @configuration  = NetbridgeConfiguration.create!(name: 'Test', organization_id: @organization.id)
        @configurations = [@configuration]
      end

      it 'returns an optgroup array' do
        grouped = helper.grouped_configurations(@configurations)
        expected = [
          ['Select configuration ...', [['No Configuration', nil]]],
          ["Organization #{@organization.name}", [
            ['Test', @configuration.id]
          ]]
        ]
        expect(grouped).to eq expected
      end
    end
  end
end
