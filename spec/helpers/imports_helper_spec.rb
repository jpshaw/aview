# frozen_string_literal: true

require 'rails_helper'

describe ImportsHelper do
  describe '#any_old_imports?' do
    context 'when no old imports exist' do
      before do
        create(:qa_import)
      end

      it 'returns false' do
        expect(helper.any_old_imports?).to be false
      end
    end

    context 'when old imports exist' do
      before do
        create(:finished_qa_import)
      end

      it 'returns true' do
        expect(helper.any_old_imports?).to be true
      end
    end
  end
end
