# frozen_string_literal: true

require 'rails_helper'

describe LayoutHelper do
  describe '#current_organization_id' do
    before do
      create_user
      expect(helper).to receive(:current_user).and_return(@user)
    end

    let(:result) { helper.current_organization_id }

    context 'with no organization set' do
      it 'is nil' do
        expect(result).to be_nil
      end
    end

    context 'with a filter organization set on the current user' do
      before do
        @organization = create(:organization)
        @user.filter_organization = @organization
      end

      it 'returns the given organization id' do
        expect(result).to eq(@organization.id)
      end
    end
  end

  describe '#current_organization_name' do
    before do
      create_user
      expect(helper).to receive(:current_user).and_return(@user).at_least(:once)
    end

    let(:result) { helper.current_organization_name }

    context 'with no organization set' do
      it 'equals \'All Organizations\'' do
        expect(result).to eq('All Organizations')
      end
    end

    context 'with a filter organization set on the current user' do
      before do
        @organization = create(:organization)
        @user.filter_organization = @organization
      end

      it 'returns the current organization name' do
        expect(result).to eq(@organization.name)
      end
    end
  end

  describe '#filter_dropdown_class' do
    before do
      create_user
      expect(helper).to receive(:current_user).and_return(@user).at_least(:once)
    end

    let(:result) { helper.filter_dropdown_class.to_s }

    context 'with no organization set' do
      it 'is nil' do
        expect(result.blank?).to be true
      end
    end

    context 'with a filter organization set on the current user' do
      before do
        @organization = create(:organization)
        @user.filter_organization = @organization
      end

      it 'returns \'active\'' do
        expect(result).to eq('active')
      end
    end
  end

  describe '#flash_messages' do
    before do
      flash[:error] = 'Something went wrong.'
      flash[:timedout] = 'Session expired.'
    end

    context 'with no exceptions' do
      let(:result) { helper.flash_messages }

      it 'returns all of the flash messages' do
        expect(result).to have_key(:error)
        expect(result).to have_key(:timedout)
      end
    end

    context 'with exceptions' do
      let(:result) { helper.flash_messages(:timedout) }

      it 'does not return the exception' do
        expect(result).to have_key(:error)
        expect(result).not_to have_key(:timedout)
      end
    end
  end

  describe '#hierarchy_state' do
    let(:result) { helper.hierarchy_state }

    context 'with no params' do
      it 'is nil' do
        expect(result).to be_nil
      end
    end

    context 'with exclude_hierarchy set true' do
      before do
        params[:exclude_hierarchy] = 1
      end

      it 'returns the current organization id' do
        expect(result).to eq(1)
      end
    end
  end
end
