# frozen_string_literal: true

require 'rails_helper'

describe GatewayHelper do
  before do
    @device = create(:gateway_device)
  end

  describe '#has_cell_extender?' do
    context 'when the device does not have details' do
      before do
        allow(@device).to receive(:details).and_return(nil)
      end

      it 'returns false' do
        expect(@device.details).to be nil
        expect(helper.has_cell_extender?(:wan_1)).to be false
      end
    end

    context 'when the device has details' do
      before do
        create(:gateway_details_with_extenders, device: @device)
      end

      context 'when the cell extender interface does not exist' do
        it 'returns false' do
          expect(helper.has_cell_extender?(:wan_123)).to be false
        end
      end

      context 'when the cell extender interfaces exist' do
        it 'returns true' do
          expect(helper.has_cell_extender?(:wan_1)).to be true
        end
      end
    end
  end
end
