# frozen_string_literal: true

require 'rails_helper'

describe DataPlansHelper do
  describe '#get_data_plan_options_for_select2' do
    let(:data_plan) { build(:data_plan, id: 1) }

    it 'sets the data plans in a format intended for filling a select2 set of options with a blank option' do
      expect(DataPlan).to receive(:for_organization).and_return([data_plan])
      expected_result = [{ id: ' ', text: '-- No Plan --' }, { id: data_plan.id, text: data_plan.name }]
      result = helper.get_data_plan_options_for_select2
      expect(result).to eq(expected_result)
    end
  end
end
