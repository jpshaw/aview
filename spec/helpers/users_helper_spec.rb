# frozen_string_literal: true

require 'rails_helper'

describe UsersHelper do
  let(:current_user) { create(:user) }

  before do
    allow(helper).to receive(:current_user).and_return(current_user)
  end

  describe '#email_required?' do
    context 'when given nil' do
      it 'calls the method on the current user' do
        expect(current_user).to receive(:email_required?)
        helper.email_required?(nil)
      end
    end

    context 'when given a user' do
      let(:user) { create(:user) }

      it 'calls the method on the given user' do
        expect(user).to receive(:email_required?)
        helper.email_required?(user)
      end
    end

    context 'when @user is set' do
      before do
        @user = create(:user)
      end

      context 'when the method is not given any arguments' do
        it 'calls the method on @user' do
          expect(@user).to receive(:email_required?)
          helper.email_required?
        end
      end

      context 'when the method is given nil' do
        it 'calls the method on the current user' do
          expect(current_user).to receive(:email_required?)
          helper.email_required?(nil)
        end
      end

      context 'when the method is given a user' do
        let(:user) { create(:user) }

        it 'calls the method on the given user' do
          expect(user).to receive(:email_required?)
          helper.email_required?(user)
        end
      end
    end
  end
end
