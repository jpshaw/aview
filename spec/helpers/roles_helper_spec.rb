# frozen_string_literal: true

require 'rails_helper'

describe RolesHelper do
  before do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    allow(helper).to receive(:current_user).and_return(@user)
  end

  describe '#roles_list_for' do
    context 'when nil is given' do
      it 'returns an empty array' do
        expect(helper.roles_list_for(nil)).to eq []
      end
    end

    context 'when the organization has no roles' do
      before do
        allow(@organization).to receive(:roles).and_return(Role.limit(0))
      end

      it 'returns an empty array' do
        expect(helper.roles_list_for(@organization)).to eq []
      end
    end

    context 'when the organization has roles' do
      before do
        @role = create(:role, name: 'Test', organization: @organization)
        allow(@organization).to receive(:roles).and_return(Role.where(id: @role))
      end

      it 'returns an array for use in select boxes' do
        expect(helper.roles_list_for(@organization)).to eq [[@role.name, @role.id]]
      end
    end
  end
end
