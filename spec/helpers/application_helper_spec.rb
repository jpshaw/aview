# frozen_string_literal: true

require 'rails_helper'

describe ApplicationHelper do
  describe '#app_version' do
    it 'returns a valid version' do
      current = Versionomy.parse(helper.app_version)
      min = Versionomy.parse('0.0.0')
      expect(current).to be > min
    end
  end

  describe '#footer_text' do
    it 'contains the copyright year' do
      copyright_year = helper.copyright_year
      expect(copyright_year).to be_present
      expect(helper.footer_text).to include(copyright_year)
    end

    it 'contains the application version' do
      footer_version = helper.footer_version
      expect(footer_version).to be_present
      expect(helper.footer_text).to include(footer_version)
    end

    context 'with settings sha value' do
      let(:sha_value) { 'd14680c9488129805f65d46e5d5c4fd1242a5efc' }

      before do
        allow(Rails.env).to receive(:staging?).and_return(true)
        allow(helper).to receive(:git_sha_link).and_return(sha_value)
      end

      it 'contains the sha value' do
        expect(helper.footer_text).to include(sha_value)
      end
    end

    context 'with no settings sha value' do
      before do
        allow(helper).to receive(:git_sha_link).and_return(nil)
      end

      it 'does not contain a sha value' do
        footer_parts = helper.footer_text.split(' - ')
        expect(footer_parts.size).to eq 2
        expect(footer_parts.first).to eq(helper.copyright_year)
        expect(footer_parts.last).to  eq(helper.footer_version)
      end
    end
  end

  describe '#copyright_year' do
    it 'returns the HTML copyright value (&copy;)' do
      expect(helper.copyright_year).to include('&copy;')
    end

    it 'returns the current year' do
      stubbed_year = 2000
      allow(Time).to receive_message_chain(:now, :year).and_return(stubbed_year)
      expect(helper.copyright_year).to include(stubbed_year.to_s)
    end
  end

  describe '#footer_version' do
    it 'returns the application version' do
      app_version = helper.app_version
      expect(app_version).to be_present
      expect(helper).to receive(:app_version).and_return(app_version)
      expect(helper.footer_version).to eq("Version #{app_version}")
    end
  end

  describe '#git_sha_link' do
    it 'returns nil if Settings.git.sha is not set' do
      Settings.git.sha = nil
      link = helper.git_sha_link
      expect(link).to be nil
    end

    it 'returns a link_to string when Settings.git.sha is set' do
      Settings.git.sha = 'd14680c9488129805f65d46e5d5c4fd1242a5efc'
      link = helper.git_sha_link
      expect(link).not_to be nil
      text = "<a target=\"_blank\" href=\"#{Settings.git.repo}/commits/#{Settings.git.sha}\">#{Settings.git.sha}</a>"
      expect(link).to eq text
    end
  end

  describe '#logout_link' do
    it 'returns nil when Devise.logout_enabled is false' do
      Devise.logout_enabled = false
      link = helper.logout_link
      expect(link).to be nil
    end

    it 'returns html and a link when Devise.logout_enabled is true' do
      Devise.logout_enabled = true
      link = helper.logout_link
      expect(link).not_to be nil
      expect(link).to include('Logout')
    end
  end

  describe '#link_to_object_show' do
    it 'returns an empty string if nil values are passed' do
      link = helper.link_to_object_show(nil, nil, nil)
      expect(link).not_to be nil
      expect(link).to eq ''
    end

    context 'with valid object' do
      before do
        create_device
      end

      it 'returns a link with text from a method on the object' do
        site = @device.site
        expect(site).not_to be nil
        link = link_to_object_show(:name, :site_path, site)
        expect(link).not_to be nil
        expect(link).to include(site.name)
      end

      it 'returns a link with custom text' do
        site = @device.site
        expect(site).not_to be nil
        text = 'View site'
        link = link_to_object_show(text, :site_path, site)
        expect(link).not_to be nil
        expect(link).to include(text)
      end

      it 'returns a link with default text if the method is not found for the object' do
        site = @device.site
        expect(site).not_to be nil
        link = link_to_object_show(:nonexistent_method, :site_path, site)
        expect(link).not_to be nil
        expect(link).to include('View')
      end

      it 'returns a link with default text if something other than a string or symbol is passed' do
        site = @device.site
        expect(site).not_to be nil
        link = link_to_object_show([1, 2, 3], :site_path, site)
        expect(link).not_to be nil
        expect(link).to include('View')
      end

      it 'returns an empty string if the path is invalid' do
        site = @device.site
        expect(site).not_to be nil
        link = link_to_object_show('View site', :nonexistent_path, site)
        expect(link).not_to be nil
        expect(link).to eq ''
      end

      it 'returns an empty string if the objects id is nil' do
        @device.site_id = nil
        site = @device.site
        expect(site).to be nil
        link = link_to_object_show(:name, :site_path, site)
        expect(link).not_to be nil
        expect(link).to eq ''
      end
    end
  end

  describe '#nested_value' do
    let(:foobar) do
      class Foo
        def name
          'Mr Foo'
        end
      end

      class Bar
        def foo
          Foo.new
        end
      end

      Bar.new
    end

    context 'when the object is nil' do
      it 'returns an empty string' do
        expect(helper.nested_value(nil, 'foo.name')).to eq ''
      end
    end

    context 'when the method chain is nil' do
      it 'returns an empty string' do
        expect(helper.nested_value(foobar, nil)).to eq ''
      end
    end

    context 'when the object does not respond to the method chain' do
      it 'returns an empty string' do
        expect(helper.nested_value(foobar, 'foo.abc')).to eq ''
      end
    end

    context 'when the object responds to the method chain' do
      it 'returns the method value' do
        expect(helper.nested_value(foobar, 'foo.name')).to eq 'Mr Foo'
      end
    end
  end

  describe '#device_configuration_list' do
    let(:user) { User.new }

    before do
      allow(helper).to receive(:current_user) { user }
      allow(helper).to receive_message_chain(:controller, :action_name).and_return('index')
    end

    describe 'Legacy Cellular configurations' do
      let(:cellular_configurations_link) { Regexp.new('<a href=\"\/legacy_configurations\">Legacy Cellular<\/a>') }

      before do
        allow(helper).to receive_message_chain(:controller, :controller_name).and_return('configurations')
      end

      context 'when user has access to cellular categories' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(false)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(true)
          allow(user).to receive(:device_categories).and_return(DeviceCategory.all)
          create(:device_category, class_name: 'Legacy Cellular', name: 'Legacy Cellular')
        end

        it 'returns a link to the cellular configurations index page' do
          expect(helper.device_configuration_list).to match(cellular_configurations_link)
        end
      end

      context 'when user does not have access to cellular categories' do
        before do
          allow(user).to receive(:device_categories).and_return([])
          allow(user).to receive(:device_categories).and_return(DeviceCategory.all)
          create(:device_category, class_name: 'Not Cellular', name: 'Not Cellular')
        end

        it 'does not return a link to the cellular configurations index page' do
          expect(helper.device_configuration_list).not_to match(cellular_configurations_link)
        end
      end
    end

    describe 'Dial-To-IP configurations' do
      let(:dial_to_ip_device_configurations_link) { Regexp.new('<a href=\"\/device_configurations\/dial_to_ip\">Dial-To-IP<\/a>') }

      before do
        allow(helper).to receive_message_chain(:controller, :controller_name).and_return('device_configurations')
      end

      context 'when user has the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(true)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
          allow(user).to receive(:device_categories).and_return(DeviceCategory.all)
          create(:device_category, class_name: 'DialToIp', name: 'Dial-To-IP')
        end

        it 'returns a link to the dial-to-ip configurations index page' do
          expect(helper.device_configuration_list).to match(dial_to_ip_device_configurations_link)
        end
      end

      context 'when user does not have the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(false)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
        end

        it 'does not return a link to the device configurations index page' do
          expect(helper.device_configuration_list).not_to match(dial_to_ip_device_configurations_link)
        end
      end
    end

    describe 'uCPE configurations' do
      let(:ucpe_device_configurations_link) { Regexp.new('<a href=\"\/device_configurations\/ucpe\">uCPE<\/a>') }

      before do
        allow(helper).to receive_message_chain(:controller, :controller_name).and_return('device_configurations')
      end

      context 'when user has the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(true)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
          allow(user).to receive(:device_categories).and_return(DeviceCategory.all)
          create(:device_category, class_name: 'uCPE', name: 'uCPE')
        end

        it 'returns a link to the ucpe configurations index page' do
          expect(helper.device_configuration_list).to match(ucpe_device_configurations_link)
        end
      end

      context 'when user does not have the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(false)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
        end

        it 'does not return a link to the device configurations index page' do
          expect(helper.device_configuration_list).not_to match(ucpe_device_configurations_link)
        end
      end
    end

    describe 'Remote Manager configurations' do
      let(:remote_manager_device_configurations_link) { Regexp.new('<a href=\"\/device_configurations\/remote_manager\">Remote Manager<\/a>') }

      before do
        allow(helper).to receive_message_chain(:controller, :controller_name).and_return('device_configurations')
      end

      context 'when user has the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(true)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
          allow(user).to receive(:device_categories).and_return(DeviceCategory.all)
          create(:device_category, class_name: 'Remote Manager', name: 'Remote Manager')
        end

        it 'returns a link to the dial-to-ip configurations index page' do
          expect(helper.device_configuration_list).to match(remote_manager_device_configurations_link)
        end
      end

      context 'when user does not have the ability to modify device configurations' do
        before do
          allow(user).to receive(:has_ability?).with('Modify Device Configurations').and_return(false)
          allow(user).to receive(:has_ability?).with('Modify Configurations').and_return(false)
        end

        it 'does not return a link to the device configurations index page' do
          expect(helper.device_configuration_list).not_to match(remote_manager_device_configurations_link)
        end
      end
    end
  end

  describe '#is_page_active?' do
    context 'when nil is passed' do
      it 'returns false' do
        expect(helper.is_page_active?(nil)).to be false
      end
    end

    context 'when action_name and controller_name is nil' do
      before do
        allow(helper).to receive(:action_name).and_return(nil)
        allow(helper).to receive(:controller_name).and_return(nil)
      end

      it 'returns false' do
        expect(helper.is_page_active?({})).to be false
      end
    end

    context 'when the controller and action names are set' do
      before do
        allow(helper).to receive(:controller_name).and_return('tests')
        allow(helper).to receive(:action_name).and_return('show')
      end

      context 'when the current page is not in the list of given actions' do
        it 'returns false' do
          expect(helper.is_page_active?(tests: [:index])).to be false
        end
      end

      context 'when the current page is in the list of given actions' do
        it 'returns true' do
          expect(helper.is_page_active?(tests: [:show])).to be true
        end
      end

      context 'when a POST action is given' do
        before do
          allow(helper).to receive(:action_name).and_return('update')
        end

        context 'when the `page` param is set' do
          before do
            allow(helper).to receive(:params).and_return(page: 'show')
          end

          it 'returns true' do
            expect(helper.is_page_active?(tests: [:show])).to be true
          end
        end

        context 'when the `page` param is not set' do
          before do
            allow(helper).to receive(:params).and_return({})
          end

          it 'returns false' do
            expect(helper.is_page_active?(tests: [:show])).to be false
          end
        end
      end
    end
  end

  describe '#active_page' do
    context 'when nil is given' do
      it 'returns an empty string' do
        expect(helper.active_page(nil)).to eq ''
      end
    end

    context 'when the given hash matches the current page' do
      before do
        allow(helper).to receive(:controller_name).and_return('tests')
        allow(helper).to receive(:action_name).and_return('show')
      end

      it 'returns `active`' do
        expect(helper.active_page(tests: [:show])).to eq 'active'
      end
    end

    context 'when the given hash does not match the current page' do
      before do
        allow(helper).to receive(:controller_name).and_return('tests')
        allow(helper).to receive(:action_name).and_return('index')
      end

      it 'returns `active`' do
        expect(helper.active_page(tests: [:show])).to eq ''
      end
    end
  end

  describe '#organizations_select_list' do
    before do
      @user = create(:user)
      allow(helper).to receive(:current_user).and_return(@user)
    end

    context 'when given nil' do
      it 'returns an empty array' do
        expect(helper.organizations_select_list).to eq []
      end
    end

    context 'when `prepend_all` is false' do
      it 'does not have an `All` entry' do
        expect(helper.organizations_select_list(prepend_all: false)).not_to include(['All', 0])
      end
    end

    context 'when `prepend_all` is true' do
      it 'has an `All` entry' do
        expect(helper.organizations_select_list(prepend_all: true)).to include(['All', 0])
      end
    end

    context 'when `for` is nil' do
      context 'when the current user has no managed organizations' do
        it 'returns an empty array' do
          expect(helper.organizations_select_list(for: nil)).to eq []
        end
      end

      context 'when the current user has managed organizations' do
        before do
          Permission.create(manager: @user, manageable: @user.organization, role: Role.new)
          @managed_orgs = create_list(:random_regular_organization, 2)
          @managed_orgs.each { |o| @user.organization.add_child(o) }
        end

        context 'when the user does not have a filter organization set' do
          it 'returns a list of managed organizations for the user' do
            list = []
            list << [@user.organization.name, @user.organization.id]
            @managed_orgs.each { |o| list << [o.name, o.id] }

            @user.filter_organization_id = nil
            expect(@user.filter_organization).not_to be_present
            expect(helper.organizations_select_list(for: nil).sort).to eq list.sort
          end
        end

        context 'when the user has a filter organization set' do
          it 'returns a list of managed organizations for the filter organization' do
            filter_org = @managed_orgs.sample
            @user.filter_organization_id = filter_org.id
            expect(@user.filter_organization).to be_present
            expect(helper.organizations_select_list(for: nil).sort).to eq [[filter_org.name, filter_org.id]].sort
          end
        end
      end
    end

    context 'when `for` is a user' do
      it 'returns a list of managed organizations for the user' do
        # Create permission for user to manage it's own organization. This lets
        # it manage any organizations underneath it.
        Permission.create(manager: @user, manageable: @user.organization, role: Role.new)

        managed_orgs = create_list(:random_regular_organization, 2)

        list = []
        list << [@user.organization.name, @user.organization.id]

        managed_orgs.each do |o|
          @user.organization.add_child o
          list << [o.name, o.id]
        end

        expect(helper.organizations_select_list(for: @user).sort).to eq list.sort
      end
    end

    context 'when `for` is an organization' do
      it 'returns a list of managed organizations for the manager' do
        parent       = create(:random_regular_organization)
        managed_orgs = create_list(:random_regular_organization, 2)

        list = []
        list << [parent.name, parent.id]

        managed_orgs.each do |o|
          parent.add_child o
          list << [o.name, o.id]
        end

        expect(helper.organizations_select_list(for: parent).sort).to eq list.sort
      end
    end
  end

  describe '#previous_and_next' do
    before do
      allow(helper).to receive(:url_for).and_return '/'
    end

    context 'when given nil' do
      it 'returns nil' do
        expect(helper.previous_and_next(nil, nil)).to be nil
      end
    end

    context 'when count is less than limit' do
      before do
        @count = 9
        @limit = 10
      end

      it 'does not render next' do
        expect(helper.previous_and_next(@count, @limit)).to eq ''
      end
    end

    context 'when count is equal to limit' do
      before do
        @count = 10
        @limit = 10
      end

      it 'does not render next' do
        expect(helper.previous_and_next(@count, @limit)).to include 'Next'
      end
    end

    context 'when count is greater than limit' do
      before do
        @count = 11
        @limit = 10
      end

      it 'renders next' do
        expect(helper.previous_and_next(@count, @limit)).to include 'Next'
      end
    end
  end
end
