# frozen_string_literal: true

require 'rails_helper'

describe TimezoneHelper do
  describe '#timestamp_in_users_timezone' do
    let(:user) { create_user }

    it 'returns nil if nil values are passed' do
      expect(helper.timestamp_in_users_timezone(nil, nil)).to be nil
    end

    it "returns a string formatted for the timestamp in the user's timezone" do
      timestamp = Time.now.utc
      str = helper.timestamp_in_users_timezone(timestamp, user)
      expect(str).not_to be nil
    end
  end
end
