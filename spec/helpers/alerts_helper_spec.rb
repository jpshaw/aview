# frozen_string_literal: true

require 'rails_helper'

describe AlertsHelper do
  let(:message) { 'testing' }

  describe '#alert_success' do
    it 'renders a success alert with a message' do
      alert_html = helper.alert_success(message: message)
      expect(alert_html).to include('alert-success')
      expect(alert_html).to include(message)
    end
  end

  describe '#alert_warning' do
    it 'renders a warning alert with a message' do
      alert_html = helper.alert_warning(message: message)
      expect(alert_html).to include('alert-warning')
      expect(alert_html).to include(message)
    end
  end

  describe '#alert_error' do
    it 'renders an error alert with a message' do
      alert_html = helper.alert_error(message: message)
      expect(alert_html).to include('alert-danger')
      expect(alert_html).to include(message)
    end
  end

  describe '#alert_dismissable' do
    context 'when given a :type' do
      it 'renders an alert for that type' do
        alert_html = helper.alert_dismissable(type: 'testing')
        expect(alert_html).to include('alert-testing')
      end
    end

    context 'when given a :message' do
      it 'renders an alert for the message' do
        alert_html = helper.alert_dismissable(message: message)
        expect(alert_html).to include(message)
      end
    end
  end
end
