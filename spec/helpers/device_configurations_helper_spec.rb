# frozen_string_literal: true

require 'rails_helper'

describe DeviceConfigurationsHelper do
  let(:user) { create(:user) }

  before do
    allow(helper).to receive(:current_user).and_return(user)
  end

  describe '#can_modify_device_configurations?' do
    context 'when the user is authorized to modify device configurations' do
      before do
        allow_any_instance_of(DeviceConfigurationPolicy).to receive(:modify?).and_return(true)
      end

      it 'returns true' do
        expect(helper.can_modify_device_configurations?).to be true
      end
    end

    context 'when the user is not authorized to modify device configurations' do
      before do
        allow_any_instance_of(DeviceConfigurationPolicy).to receive(:modify?).and_return(false)
      end

      it 'returns false' do
        expect(helper.can_modify_device_configurations?).to be false
      end
    end
  end
end
