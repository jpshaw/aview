# frozen_string_literal: true

require 'rails_helper'

describe OrganizationsHelper do
  before do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
    allow(helper).to receive(:current_user).and_return(@user)
  end

  describe '#can_create_organizations?' do
    context 'when the user has the ability to create organizations' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, create?: true)
      end

      it 'returns true' do
        expect(helper.can_create_organizations?).to be true
      end
    end

    context 'when the user does not have the ability to create organizations' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, create?: false)
      end

      it 'returns false' do
        expect(helper.can_create_organizations?).to be false
      end
    end
  end

  describe '#can_modify_organizations?' do
    context 'when the user has the ability to modify organizations' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify?: true)
      end

      it 'returns true' do
        expect(helper.can_modify_organizations?).to be true
      end
    end

    context 'when the user does not have the ability to modify organizations' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify?: false)
      end

      it 'returns false' do
        expect(helper.can_modify_organizations?).to be false
      end
    end
  end

  describe '#can_modify_organization?' do
    context 'when the user has the ability to modify the given organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify?: true)
      end

      it 'returns true' do
        expect(helper.can_modify_organization?(@organization)).to be true
      end
    end

    context 'when the user does not have the ability to modify the given organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify?: false)
      end

      it 'returns false' do
        expect(helper.can_modify_organization?(@organization)).to be false
      end
    end
  end

  describe '#can_modify_users_for_organization?' do
    context 'when the user has the ability to modify users for the given organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify_users?: true)
      end

      it 'returns true' do
        expect(helper.can_modify_users_for_organization?(@organization)).to be true
      end
    end

    context 'when the user does not have the ability to modify users for the given organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify_users?: false)
      end

      it 'returns false' do
        expect(helper.can_modify_users_for_organization?(@organization)).to be false
      end
    end
  end

  describe '#can_modify_users_for_organizations?' do
    context 'when the user has the ability to create users for some organizations' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify_users?: true)
      end

      it 'returns true' do
        expect(helper.can_modify_users_for_organizations?).to be true
      end
    end

    context 'when the user does not have the ability to modify users for any organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, modify_users?: false)
      end

      it 'returns false' do
        expect(helper.can_modify_users_for_organizations?).to be false
      end
    end
  end

  describe '#can_destroy_organization?' do
    context 'when the user has the ability to destroy the organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, destroy?: true)
      end

      it 'returns true' do
        expect(helper.can_destroy_organization?(@organization)).to be true
      end
    end

    context 'when the user does not have the ability to destroy the organization' do
      before do
        allow(helper).to receive(:policy).and_return double(:policy, destroy?: false)
      end

      it 'returns false' do
        expect(helper.can_destroy_organization?(@organization)).to be false
      end
    end
  end

  describe '#managed_orgs_list_for' do
    context 'when nil is given' do
      it 'returns an empty array' do
        expect(helper.managed_orgs_list_for(nil)).to eq []
      end
    end

    context 'when the manager has no managed organizations' do
      before do
        allow(@user).to receive(:organizations).and_return(Organization.limit(0))
      end

      it 'returns an empty array' do
        expect(helper.managed_orgs_list_for(@user)).to eq []
      end
    end

    context 'when the manager has managed organizations' do
      before do
        @org = create(:random_regular_organization)
        allow(@user).to receive(:head_manageable_organizations).and_return(Organization.where(id: @org))
      end

      it 'returns an array for use in select boxes' do
        expect(helper.managed_orgs_list_for(@user)).to eq [[@org.name, @org.id]]
      end
    end
  end
end
