# frozen_string_literal: true

require 'rails_helper'

describe DevicesHelper do
  before do
    @organization = create(:organization)
    @user         = create(:user, organization: @organization)
    allow(helper).to receive(:current_user).and_return(@user)
  end

  describe '#can_modify_devices?' do
    context 'when the user is not authorized to modify devices' do
      it 'returns false' do
        expect(helper.can_modify_devices?).to be false
      end
    end

    context 'when the user is authorized to modify devices' do
      before do
        role = @organization.roles.create name: 'Test'
        Permission.create!(manager: @user, manageable: @organization, role: role)
        role.abilities << Ability.create(name: 'Modify Devices')
      end

      it 'returns true' do
        expect(helper.can_modify_devices?).to be true
      end
    end
  end

  # NOTE:  We are excluding Gateway configurations because they are not displayed in the UI and they
  #       don't have a name, which is used to generate the dropdown in the UI.
  describe '#device_configurations' do
    before do
      expect(helper.assigns[:organization]).not_to be nil
    end

    it 'returns an empty array if organization is not present' do
      assign(:organization, nil)
      expect(helper.assigns[:organization]).to be nil
      expect(helper).not_to receive(:configurations)
      expect(helper.device_configurations.class).to be Array
      expect(helper.device_configurations).to be_empty
    end

    context 'for configurations with no model association' do
      it 'uses correct configurations for the device type' do
        [Netbridge, Netreach].each do |device_class|
          device      = device_class.new
          association = "#{device.object_type.underscore}_configurations".to_sym
          assign(:device, device)
          expect(helper.assigns[:organization]).to receive(association).and_return([])
          helper.device_configurations
        end
      end

      it 'returns an array with info of all configurations for the organization' do
        [Netbridge, Netreach].each do |device_class|
          device                = device_class.new
          configuration_symbol  = "#{device.object_type.underscore}_configuration".to_sym
          assign(:device, device)
          expect(helper).to receive(:configurations).and_return(
            create_list(configuration_symbol, 2, organization: helper.assigns[:organization])
          )
          expect(helper.device_configurations.size).to be 3
        end
      end
    end

    context 'for configurations with model association' do
      it 'uses correct configurations for the device type' do
        [DialToIp, RemoteManager, Ucpe].each do |device_class|
          device = device_class.new
          assign(:device, device)
          expect(helper.assigns[:organization]).to receive(:device_configurations).and_return(DeviceConfiguration.all)
          helper.device_configurations
        end
      end

      it "returns an array with info of only configurations whose model matches the device's" do
        test_configuration = create(:device_configuration, organization: helper.assigns[:organization])
        device_category_and_class = { DialToIp => '5300_DC',
                                      RemoteManager => '5400_RM',
                                      Ucpe          => '9400_UA' }

        device_category_and_class.each do |k, v|
          device        = k.new
          model_symbol  = "#{device.object_type.underscore}_#{v}_model".to_sym
          device_model  = build(model_symbol)
          device.series = device_model

          assign(:device, device)

          # Cant use a factory here. Need to build configuration by hand because a class down the
          # line gives a 'Validation failed: Class name has already been taken' error message.
          device_configuration = DeviceConfiguration.new(
            name: Faker::Name.name,
            device_firmware_id: 1
          )
          device_configuration.organization = helper.assigns[:organization]
          device_configuration.settings     = '{}'
          device_configuration.save!

          allow(device_configuration).to receive(:device_model).and_return(device_model)

          allow(helper).to receive(:configurations).and_return([device_configuration, test_configuration])
          configuration_options = helper.device_configurations
          expect(configuration_options.size).to be 2 # blank option is included
          configuration_option = configuration_options.second
          expect(configuration_option.first).to eq(device_configuration.name)
          expect(configuration_option.first).not_to eq(test_configuration.name)
        end
      end
    end
  end

  describe '#the_device_configuration_path' do
    context 'for a DeviceConfiguration instance' do
      let(:device_configuration) do
        dc    = DeviceConfiguration.new
        dc.id = 1
        dc
      end

      it 'returns a device_configuration_path' do
        expect(helper.__send__(:the_device_configuration_path, device_configuration)).to match('/device_configurations/')
      end
    end

    context 'for a non-DeviceConfiguration instance' do
      let(:device_configuration) do
        dc    = NetbridgeConfiguration.new
        dc.id = 1
        dc
      end

      it 'returns a legacy_configuration_path' do
        expect(helper.__send__(:the_device_configuration_path, device_configuration)).to match('/legacy_configurations/')
      end
    end
  end
end
