# frozen_string_literal: true
$VERBOSE = nil

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)

# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
require 'capybara/rspec'
require 'webmock/rspec'
require 'pundit/rspec'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

def fixture_path
  "#{::Rails.root}/spec/support/fixtures"
end

def fixture(file)
  File.new(fixture_path + '/' + file)
end

def json_fixture(file)
  JSON.parse(File.read(fixture_path + '/' + file)).with_indifferent_access
end

# Checks for pending migrations before tests are run.
ActiveRecord::Migration.check_pending!

# Create parallel cache directories
FileUtils.mkdir_p Rails.root.join("tmp", "cache", "parallel-tests#{ENV['TEST_ENV_NUMBER']}")

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.include Devise::TestHelpers, type: :controller
  config.include CreateResourceHelper
  config.include LogInHelper, type: :request
  config.include AlertHelper, type: :request
  config.include DeviceHelper, type: :request
  config.include LocationHelper, type: :request
  config.include ContactHelper, type: :request
  config.include GatewayTunnelHelper
  config.include NetbridgeConfigurationHelper
  config.include LayoutHelper, type: :view
  config.include ReportHelper
  config.include Capybara::DSL
  config.include ActionDispatch::TestProcess
  config.include ActionView::TestCase::Behavior, file_path: %r{spec/presenters}
  config.include RSpecHtmlMatchers
  config.include PunditViewPolicy, type: :helper
  config.include FeatureHelper, type: :feature

  config.fixture_path = fixture_path
  config.use_transactional_fixtures = false

  config.raise_errors_for_deprecations!
  config.infer_spec_type_from_file_location!

  # Stub functionality for TorqueBox classes
  # TODO: Move this to specs that actually need it
  config.before do |example|
    unless example.metadata[:torquebox] == true
      allow_any_instance_of(Device).to receive(:cache_heartbeat!)
    end
  end

  config.before(:each) do
    Rails.cache.clear
    helpers = double('helpers')
    allow(ActionController::Base).to receive(:helpers).and_return(helpers)
    allow(helpers).to receive(:asset_path).and_return('')
    allow_any_instance_of(Kafka::AsyncProducer).to receive(:produce)
    allow_any_instance_of(Kafka::Producer).to receive(:produce)
    allow(DeviceEventProducer).to receive(:call) do |device, event|
      DeviceEventProducerStub.call(device, event)
    end
    allow(Measurement::CellularUtilization).to receive(:create)
  end
end

at_exit do
  if ParallelTests.first_process?
    ParallelTests.wait_for_other_processes_to_finish
    FileUtils.rm_rf(Dir["#{Rails.root}/spec/support/uploads"]) if Rails.env.test?
  end
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/support/fixtures/cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.ignore_localhost = true
  config.ignore_hosts 'codeclimate.com'

  config.define_cassette_placeholder('<ARMT_BING_KEY>') do
    "key=#{Settings.api_keys.maps}"
  end
end
