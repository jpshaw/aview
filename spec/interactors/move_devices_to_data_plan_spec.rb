# frozen_string_literal: true

require 'rails_helper'

describe MoveDevicesToDataPlan do
  let!(:organization) { create(:organization) }
  let!(:data_plan) { create(:data_plan, organization: organization) }
  let!(:device_1) { create(:test_device_with_modem_association, organization: organization) }
  let!(:device_2) { create(:test_device_with_modem_association, organization: organization) }
  let(:bad_data_plan_id) { '123' }
  let(:good_data_plan_id) { data_plan.id }
  let(:bad_mac_1) { '123' }
  let(:bad_mac_2) { '456' }

  describe '#call' do
    context 'when given no mac addresses' do
      let(:result) { described_class.call(data_plan_id: good_data_plan_id) }

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when given a data plan id that does not exist in the database' do
      let(:result) { described_class.call(data_plan_id: bad_data_plan_id, device_macs: "#{device_1.mac},#{device_2.mac}") }

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when given a mac address that does not exist in the database' do
      let(:result) { described_class.call(data_plan_id: good_data_plan_id, device_macs: "#{bad_mac_1},#{bad_mac_2}") }

      it 'fails' do
        expect(result).to be_a_failure
      end

      it 'returns an error for each device not found' do
        expect(result.errors.count).to eq(2)
      end
    end

    context 'when given a valid data plan id and a valid list of mac addresses' do
      let(:result) do
        described_class.call(
          data_plan_id: good_data_plan_id,
          device_macs: "#{device_1.mac},#{device_2.mac}"
        )
      end

      context 'when the save is successful' do
        it 'succeeds and assigns the data plan to devices' do
          expect(result).to be_a_success
          expect(device_1.reload.data_plan).to eq(data_plan)
          expect(device_2.reload.data_plan).to eq(data_plan)
        end

        it 'assigns the number of devices assigned' do
          expect(result.device_count).to eq(2)
        end
      end

      context 'when the save is not successful on one device but is successful on the other' do
        before { device_2.update(organization_id: 0) }

        it 'fails' do
          expect(result).to be_a_failure
        end

        it 'returns an error for each device that did not save correctly' do
          expect(result.errors.count).to eq(1)
        end
      end
    end

    context 'when given an empty data plan id' do
      before do
        allow(device_1).to receive(:save).and_return(true)
        allow(device_2).to receive(:save).and_return(true)
        device_1.data_plan = data_plan
        device_2.data_plan = data_plan
      end

      it 'succeeds' do
        result = described_class.call(data_plan_id: '', device_macs: "#{device_1.mac},#{device_2.mac}")
        expect(result).to be_a_success
      end

      it 'removes any data plan associated with the given devices' do
        expect(device_1.data_plan).to eq(data_plan)
        expect(device_2.data_plan).to eq(data_plan)
        described_class.call(data_plan_id: '', device_macs: "#{device_1.mac},#{device_2.mac}")
        expect(device_1.reload.data_plan).to eq(nil)
        expect(device_2.reload.data_plan).to eq(nil)
      end
    end
  end
end
