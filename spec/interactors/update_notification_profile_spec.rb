# frozen_string_literal: true

require 'rails_helper'

describe UpdateNotificationProfile do
  subject { described_class }

  let(:organization) { create(:organization) }
  let(:user)         { create(:user, organization: organization) }
  let(:profile)      { create(:notification_profile, user: user) }

  context 'when new subscribed events are given' do
    let(:subscribed_events_attributes) { [attributes_for(:subscribed_event)] }
    let(:params) { { subscribed_events_attributes: subscribed_events_attributes } }

    it 'creates the subscribed events' do
      expect do
        subject.call(profile: profile, params: params)
      end.to change { profile.reload.subscribed_events.count }.by(1)
    end

    it 'succeeds' do
      expect(subject.call(profile: profile, params: params)).to be_a_success
    end
  end

  context 'when subscribed events are removed' do
    let!(:subscribed_events) { create_list(:subscribed_event, 3, profile_id: profile.id) }
    let!(:subscribed_events_attributes) { profile.subscribed_events.select(:id).map(&:attributes) }
    let!(:params) { { subscribed_events_attributes: subscribed_events_attributes } }

    before do
      subscribed_events_attributes.each do |attrs|
        attrs[:_destroy] = true
      end
    end

    it 'removes the subscribed events' do
      expect do
        subject.call(profile: profile, params: params)
      end.to change { profile.reload.subscribed_events.count }.by(-3)
    end

    it 'succeeds' do
      expect(subject.call(profile: profile, params: params)).to be_a_success
    end
  end
end
