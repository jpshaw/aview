# frozen_string_literal: true

require 'rails_helper'

describe CreateOrganization do
  describe '#call' do
    let(:parent)          { create(:random_regular_organization) }
    let(:valid_params)    { { name:   'Organization name',  label: 0 } }
    let(:invalid_params)  { { name:   nil,                  label: nil } }
    let(:valid_context)   { { params: valid_params,     parent: parent } }
    let(:invalid_context) { { params: invalid_params,   parent: parent } }

    it 'fails when organization validations fail' do
      result = described_class.call(invalid_context)
      expect(result).to be_a_failure
      expect(result.organization.errors[:name]).to be_present
    end

    context 'when the organization saves successfully' do
      it 'assigns the parent to the organization when the parent is present' do
        result = described_class.call(valid_context)
        expect(result).to be_a_success
        expect(result.organization.parent).to eq(parent)
      end

      it 'does not assign the parent to the organization when the parent is not present' do
        valid_context.delete(:parent)
        result = described_class.call(valid_context)
        expect(result).to be_a_success
        expect(result.organization.parent).not_to be_present
      end
    end
  end

  describe '#rollback' do
    it 'destroys the organization record' do
      organization  = Organization.new
      interactor    = described_class.new(organization: organization)
      expect(organization).to receive(:destroy)
      interactor.rollback
    end
  end
end
