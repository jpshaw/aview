# frozen_string_literal: true

require 'rails_helper'

describe SaveDeviceConfiguration do
  context 'when nothing is given' do
    let(:result) { described_class.call({}) }

    it 'fails' do
      expect(result).to be_a_failure
    end
  end

  context 'when no group configuration is given' do
    let(:device) { create(:test_device) }

    let(:result) { described_class.call(device: device) }

    it 'fails' do
      expect(result).to be_a_failure
    end
  end

  context 'when the device does not have a configuration' do
    let(:device) { create(:remote_manager_device) }

    context 'when the group configuration is empty' do
      let(:result) do
        described_class.call(device: device,
                             group_configuration_id: '')
      end

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when the group configuration is -1' do
      let(:result) do
        described_class.call(device: device,
                             group_configuration_id: '-1')
      end

      it 'succeeds but does nothing' do
        expect(result).to be_a_success
        expect(device.configuration).to be_nil
      end
    end

    context 'when the group configuration is not found' do
      let(:result) do
        described_class.call(device: device,
                             group_configuration_id: '1')
      end

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when the group configuration is valid' do
      let(:old_settings) { '{ "test_setting": "old setting", "nested_setting": { "test_nest": "old nest" } }' }
      let(:group_configuration) do
        create(:device_configuration,
               organization: device.organization,
               device_firmware_locked: false,
               grace_period_duration_locked: false,
               grace_period_duration: '',
               settings: old_settings,
               compiled_settings: old_settings)
      end

      context 'when an individual configuration already exists for the device' do
        before do
          existing_config = group_configuration.dup
          existing_config.name = "#{device.mac} Config"
          existing_config.save
        end

        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: group_configuration.device_firmware.id,
                               settings: group_configuration.settings)
        end

        it 'is a success' do
          expect(result).to be_a_success
        end

        it 'updates the available individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end
      end

      context 'when no settings are given' do
        let(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id)
        end

        it 'fails' do
          expect(result).to be_a_failure
        end
      end

      context 'when the given settings are malformed json' do
        let(:malformed_settings) { '4' }
        let(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               settings: malformed_settings)
        end

        it 'fails' do
          expect(result).to be_a_failure
        end
      end

      context 'when no firmware id is given' do
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               settings: group_configuration.settings)
        end

        it 'does not lock the configuration firmware' do
          device.reload
          expect(device.configuration.device_firmware_locked).to be false
        end

        it 'assigns the group configuration to the device' do
          device.reload
          expect(device.configuration).to eq(group_configuration)
        end
      end

      context 'when a firmware id is given' do
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: group_configuration.device_firmware.id,
                               settings: group_configuration.settings)
        end

        it 'creates an individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end

        it 'locks the configuration firmware' do
          device.reload
          expect(device.configuration.device_firmware_locked).to be true
        end
      end

      context 'when no grace period duration is given' do
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               settings: group_configuration.settings)
        end

        it 'does not lock the grace period duration' do
          device.reload
          expect(device.configuration.grace_period_duration_locked).to be false
        end

        it 'assigns the group configuration to the device' do
          device.reload
          expect(device.configuration).to eq group_configuration
        end
      end

      context 'when a grace period duration is given' do
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               grace_period_duration: group_configuration.grace_period_duration,
                               settings: group_configuration.settings)
        end

        it 'creates an individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end

        it 'locks the grace period duration' do
          device.reload
          expect(device.configuration.grace_period_duration_locked).to be true
        end
      end

      context 'when the given firmware is different from the group configuration' do
        let(:new_firmware) { create(:device_firmware, device_model: device.series) }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: new_firmware.id,
                               settings: group_configuration.settings)
        end

        it 'creates an individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end

        it 'assigns the new firmware to the device configuration' do
          device.reload
          expect(device.configuration.device_firmware).to eq(new_firmware)
        end

        it 'does not change the firmware of the group configuration' do
          group_configuration.reload
          expect(group_configuration.device_firmware).not_to eq(new_firmware)
        end

        it 'locks the firmware on the device configuration' do
          device.reload
          expect(device.configuration.device_firmware_locked).to be true
        end

        it 'sets the group configuration as the parent of the device configuration' do
          device.reload
          expect(device.configuration.parent).to eq(group_configuration)
        end
      end

      context 'when the given grace period is different from the group configuration grace period' do
        let(:new_grace_period) { '23m' }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: group_configuration.device_firmware.id,
                               settings: group_configuration.settings,
                               grace_period_duration: new_grace_period)
        end

        it 'creates an individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end

        it 'assigns the new grace period to the device configuration' do
          device.reload
          expect(device.configuration.grace_period_duration).to eq(new_grace_period)
        end

        it 'does not change the grace period of the group configuration' do
          group_configuration.reload
          expect(group_configuration.grace_period_duration).not_to eq(new_grace_period)
        end

        it 'locks the grace period on the device configuration' do
          device.reload
          expect(device.configuration.grace_period_duration_locked).to be true
        end

        it 'sets the group configuration as the parent of the device configuration' do
          device.reload
          expect(device.configuration.parent).to eq(group_configuration)
        end
      end

      context 'when the given settings are different from the group configuration settings' do
        let(:new_settings) { '{ "test_setting": "new setting", "nested_setting": { "test_nest": "old nest" } }' }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: group_configuration.device_firmware.id,
                               settings: new_settings)
        end

        it 'creates an individual configuration and assigns it to the device' do
          device.reload
          expect(device.configuration).not_to eq(group_configuration)
          expect(device.configuration.individual?).to be true
        end

        it 'assigns the new settings to the device configuration compiled settings' do
          device.reload
          expect(device.configuration.compiled_settings).to eq(new_settings)
        end

        it 'assigns only the changed new settings to the device configuration settings' do
          device.reload
          changed_settings = '{"test_setting":"new setting"}'
          expect(device.configuration.settings).to eq(changed_settings)
        end

        it 'does not change the settings of the group configuration' do
          group_configuration.reload
          expect(group_configuration.settings).to eq(old_settings)
          expect(group_configuration.compiled_settings).to eq(old_settings)
        end

        it 'sets the group configuration as the parent of the device configuration' do
          device.reload
          expect(device.configuration.parent).to eq(group_configuration)
        end
      end
    end
  end

  context "when the device's configuration is the given group configuration" do
    let(:old_settings) { '{ "test_setting": "old setting", "nested_setting": { "test_nest": "old nest", "test_nest_2": "old nest 2" } }' }
    let(:group_configuration) do
      create(:device_configuration,
             settings: old_settings,
             compiled_settings: old_settings)
    end
    let!(:device) { create(:remote_manager_device, configuration: group_configuration) }

    context 'when the group configuration is -1' do
      let(:result) do
        described_class.call(device: device,
                             group_configuration_id: '-1')
      end

      it 'removes the configuration from the device but does not destroy the group configuration' do
        expect { result }.not_to change(DeviceConfiguration, :count)
        device.reload
        expect(device.configuration).to be_nil
      end
    end

    context 'when the firmware is updated' do
      let(:new_firmware) { create(:device_firmware, device_model: device.series) }
      let!(:result) do
        described_class.call(device: device,
                             group_configuration_id: '',
                             firmware_id: new_firmware.id,
                             settings: group_configuration.settings)
      end

      it 'creates an individual configuration and assigns it to the device' do
        device.reload
        expect(device.configuration).not_to eq(group_configuration)
        expect(device.configuration.individual?).to be true
      end

      it 'assigns the new firmware to the device configuration' do
        device.reload
        expect(device.configuration.device_firmware).to eq(new_firmware)
      end

      it 'does not change the firmware of the group configuration' do
        group_configuration.reload
        expect(group_configuration.device_firmware).not_to eq(new_firmware)
      end

      it 'locks the firmware on the device configuration' do
        device.reload
        expect(device.configuration.device_firmware_locked).to be true
      end

      it 'sets the group configuration as the parent of the device configuration' do
        device.reload
        expect(device.configuration.parent).to eq(group_configuration)
      end
    end

    context 'when the grace period is updated' do
      let(:new_grace_period) { '23m' }
      let!(:result) do
        described_class.call(device: device,
                             group_configuration_id: '',
                             firmware_id: group_configuration.device_firmware.id,
                             settings: group_configuration.settings,
                             grace_period_duration: new_grace_period)
      end

      it 'creates an individual configuration and assigns it to the device' do
        device.reload
        expect(device.configuration).not_to eq(group_configuration)
        expect(device.configuration.individual?).to be true
      end

      it 'assigns the new grace period to the device configuration' do
        device.reload
        expect(device.configuration.grace_period_duration).to eq(new_grace_period)
      end

      it 'does not change the grace period of the group configuration' do
        group_configuration.reload
        expect(group_configuration.grace_period_duration).not_to eq(new_grace_period)
      end

      it 'locks the grace period on the device configuration' do
        device.reload
        expect(device.configuration.grace_period_duration_locked).to be true
      end

      it 'sets the group configuration as the parent of the device configuration' do
        device.reload
        expect(device.configuration.parent).to eq(group_configuration)
      end
    end

    context 'when a setting is updated' do
      let(:new_settings) { '{ "test_setting": "new setting", "nested_setting": { "test_nest": "new nest" } }' }
      let!(:result) do
        described_class.call(device: device,
                             group_configuration_id: group_configuration.id,
                             firmware_id: group_configuration.device_firmware.id,
                             settings: new_settings)
      end

      it 'creates an individual configuration and assigns it to the device' do
        device.reload
        expect(device.configuration).not_to eq(group_configuration)
        expect(device.configuration.individual?).to be true
      end

      it 'assigns the new settings to the device configuration compiled settings' do
        device.reload
        expect(device.configuration.compiled_settings).to eq(new_settings)
      end

      it 'assigns only the changed new settings to the device configuration settings' do
        device.reload
        changed_settings = '{"test_setting":"new setting","nested_setting":{"test_nest":"new nest"}}'
        expect(device.configuration.settings).to eq(changed_settings)
      end

      it 'does not change the settings of the group configuration' do
        group_configuration.reload
        expect(group_configuration.settings).to eq(old_settings)
        expect(group_configuration.compiled_settings).to eq(old_settings)
      end

      it 'sets the group configuration as the parent of the device configuration' do
        device.reload
        expect(device.configuration.parent).to eq(group_configuration)
      end
    end
  end

  context "when the device's configuration is an individual configuration" do
    let(:old_settings) { '{ "test_setting": "old setting", "nested_setting": { "test_nest": "old nest" } }' }
    let(:firmware) { create(:device_firmware, migration_versions: '~1~2~') }
    let(:group_configuration) do
      create(:device_configuration,
             device_firmware: firmware,
             settings: old_settings,
             compiled_settings: old_settings)
    end
    let(:individual_configuration) do
      create(:device_configuration,
             device_firmware: firmware,
             settings: old_settings,
             compiled_settings: old_settings,
             parent: group_configuration,
             record_type: 'individual')
    end
    let!(:device) { create(:remote_manager_device, configuration: individual_configuration) }

    context 'when the group configuration is -1' do
      let(:result) do
        described_class.call(device: device,
                             group_configuration_id: '-1')
      end

      it 'removes the configuration from the device and destroys it' do
        expect { result }.to change(DeviceConfiguration, :count).by(-1)
        device.reload
        expect(device.configuration).to be_nil
      end
    end

    context 'when the given configuration is the parent of the device configuration' do
      context 'when the given firmware is different from both configurations' do
        let(:new_firmware) { create(:device_firmware, device_model: device.series) }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: new_firmware.id,
                               settings: individual_configuration.compiled_settings)
        end

        it 'assigns the new firmware to the device configuration' do
          device.reload
          expect(device.configuration.device_firmware).to eq(new_firmware)
        end

        it 'does not change the firmware of the group configuration' do
          group_configuration.reload
          expect(group_configuration.device_firmware).not_to eq(new_firmware)
        end

        it 'locks the firmware on the device configuration' do
          device.reload
          expect(device.configuration.device_firmware_locked).to be true
        end
      end

      context 'when the grace period is updated' do
        let(:new_grace_period) { '23m' }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: '',
                               firmware_id: individual_configuration.device_firmware.id,
                               settings: group_configuration.compiled_settings,
                               grace_period_duration: new_grace_period)
        end

        it 'assigns the new grace period to the device configuration' do
          device.reload
          expect(device.configuration.grace_period_duration).to eq(new_grace_period)
        end

        it 'does not change the grace period of the group configuration' do
          group_configuration.reload
          expect(group_configuration.grace_period_duration).not_to eq(new_grace_period)
        end

        it 'locks the grace period on the device configuration' do
          device.reload
          expect(device.configuration.grace_period_duration_locked).to be true
        end
      end

      context 'when nothing is different from the parent config' do
        let(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               settings: group_configuration.compiled_settings)
        end

        it 'deletes the individual configuration' do
          expect { result }.to change(DeviceConfiguration, :count).by(-1)
        end

        it 'assigns the group configuration to the device' do
          result
          device.reload
          expect(device.configuration).to eq(group_configuration)
        end
      end
    end

    context 'when the given group configuration is not the parent of the device configuration' do
      let!(:new_group_configuration) do
        create(:device_configuration,
               device_firmware: firmware,
               settings: old_settings,
               compiled_settings: old_settings)
      end

      context 'when nothing else has changed' do
        let(:result) do
          described_class.call(device: device,
                               group_configuration_id: new_group_configuration.id,
                               settings: old_settings)
        end

        it 'sets the new configuration as the device configuration' do
          result
          device.reload
          expect(device.configuration).to eq(new_group_configuration)
        end

        it 'deletes the individual configuration' do
          expect { result }.to change(DeviceConfiguration, :count).by(-1)
        end
      end

      context 'when the given firmware is different from both configurations' do
        let(:new_firmware) { create(:device_firmware, device_model: device.series) }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: new_group_configuration.id,
                               firmware_id: new_firmware.id,
                               settings: individual_configuration.compiled_settings)
        end

        it 'sets the new configuration as the parent of the device configuration' do
          device.reload
          expect(device.configuration.parent).to eq(new_group_configuration)
        end

        it 'assigns the new firmware to the device configuration' do
          device.reload
          expect(device.configuration.device_firmware).to eq(new_firmware)
        end

        it 'does not change the firmware of the old or new group configuration' do
          group_configuration.reload
          expect(group_configuration.device_firmware).not_to eq(new_firmware)
          new_group_configuration.reload
          expect(new_group_configuration.device_firmware).not_to eq(new_firmware)
        end

        it 'locks the firmware on the device configuration' do
          device.reload
          expect(device.configuration.device_firmware_locked).to be true
        end
      end
    end

    context 'when the parent is disconnected' do
      let(:old_firmware) { create(:device_firmware, device_model: firmware.device_model, version: '13.12.01', migration_versions: '~1~') }

      before do
        individual_configuration.update(parent_disconnect: true, device_firmware: old_firmware)
      end

      context 'when the child sets the firmware to a version that matches the parent' do
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: firmware.id,
                               settings: individual_configuration.compiled_settings)
        end

        it 'removes the parent disconnect flag' do
          expect(individual_configuration.parent_disconnect).to eq false
        end
      end

      context 'when the child updates a setting but leaves the firmware alone' do
        let(:new_settings) { '{ "test_setting": "new setting", "nested_setting": { "test_nest": "new nest" } }' }
        let!(:result) do
          described_class.call(device: device,
                               group_configuration_id: group_configuration.id,
                               firmware_id: old_firmware.id,
                               settings: new_settings)
        end

        it 'keeps the parent disconnect flag' do
          expect(individual_configuration.parent_disconnect).to eq true
        end

        it 'updates the setting' do
          expect(individual_configuration.compiled_settings).to eq(new_settings)
        end
      end
    end
  end
end
