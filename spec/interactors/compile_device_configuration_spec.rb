# frozen_string_literal: true

require 'rails_helper'

describe CompileDeviceConfiguration do
  context 'when nothing is given' do
    let(:result) { described_class.call({}) }

    it 'fails' do
      expect(result).to be_a_failure
    end
  end

  context 'when a configuration is given' do
    # TODO: this can be reimplemented when we have a proper firmware schema validator
    context 'when the settings are invalid' do
      # let(:invalid_settings) { '{ "config": { "secure_port": "wrong" } }' }
      # let(:config) { build(:device_configuration, :with_sequenced_device_model, settings: invalid_settings) }
      #
      # let(:result) { CompileDeviceConfiguration.call({ device_configuration: config }) }
      #
      # it 'fails' do
      #   expect(result).to be_a_failure
      # end
    end

    context 'when the settings are valid' do
      let(:parent_settings) do
        '{
          "schema": {
            "version": "2"
          },
          "foo": "fooparent",
          "bar": {
            "baz": "bazparent",
            "barparent": {
              "quux": "quuxparent"
            }
          },
          "corge": "grault"
        }'
      end
      let(:child_settings) do
        '{
          "schema": {
            "version": "1",
            "override": {
              "oldfoo": true,
              "bar": {
                "baz": true,
                "barchild": true
              },
              "corge": true,
              "grault": true
            }
          },
          "oldfoo": "foochild",
          "bar": {
            "barchild": {
              "quux": "quuxchild"
            }
          },
          "corge": "grault"
        }'
      end

      context 'when the configuration does not have a parent' do
        let(:config) do
          create(:device_configuration, :with_sequenced_device_model_and_test_firmware,
                 settings: parent_settings)
        end

        let!(:result) { described_class.call(device_configuration: config) }

        it 'succeeds' do
          expect(result).to be_a_success
        end

        it 'sets the configuration settings as the compiled settings' do
          expect(config.compiled_settings).to eq(parent_settings)
        end
      end

      context 'when the configuration has a child' do
        let(:parent_config) do
          create(:device_configuration, :with_sequenced_device_model_and_test_firmware,
                 settings: parent_settings, compiled_settings: parent_settings)
        end
        let!(:child_config) do
          create(:device_configuration, :with_sequenced_device_model_and_test_firmware,
                 settings: child_settings, compiled_settings: child_settings,
                 parent: parent_config)
        end

        let!(:result) { described_class.call(device_configuration: parent_config) }

        before do
          child_config.reload
        end

        it 'succeeds' do
          expect(result).to be_a_success
        end

        it 'sets the schema version to that of the latest schema migration' do
          expect(child_config.parsed_compiled_settings['schema']['version']).to eq('2')
        end

        it 'runs the migrations in the schema' do
          expect(child_config.parsed_compiled_settings['foo']).to eq('foochild')
        end

        it 'keeps settings that exist in both parent and child' do
          expect(child_config.parsed_compiled_settings['corge']).to eq('grault')
        end

        it 'takes child settings that do not exist in the parent' do
          expect(child_config.parsed_compiled_settings['bar']['barchild']['quux']).to eq('quuxchild')
        end

        it 'takes parent settings that do not exist in the child' do
          expect(child_config.parsed_compiled_settings['bar']['barparent']['quux']).to eq('quuxparent')
        end

        it 'takes the child settings when a conflict exists' do
          expect(child_config.parsed_compiled_settings['foo']).to eq('foochild')
        end

        context 'when the parent is compiled again after its settings have changed' do
          before do
            parent_config.reload
            updated_parent_settings = '{
                                        "schema": {
                                          "version": "2"
                                        },
                                        "foo": "fooparent",
                                        "bar": {
                                          "baz": "bazparent",
                                          "barparent": {
                                            "quux": "quuxparent"
                                          }
                                        },
                                        "corge": "garply",
                                        "waldo": "fred"
                                      }'
            parent_config.settings = updated_parent_settings
            described_class.call(device_configuration: parent_config)
            child_config.reload
          end

          it 'compiles the child such that the child does not take changes that were already overridden by the child' do
            expect(child_config.parsed_compiled_settings['corge']).to eq('grault')
          end

          it 'compiles the child such that the child takes changes that are not set individually for the child' do
            expect(child_config.parsed_compiled_settings['waldo']).to eq('fred')
          end
        end

        context 'when the child has locked its firmware to a lower version than the parent' do
          let(:old_firmware) do
            create(:device_firmware, device_model: parent_config.device_model,
                                     migration_versions: '~1~')
          end
          let(:new_firmware) do
            create(:device_firmware, device_model: parent_config.device_model,
                                     migration_versions: '~1~2~')
          end

          before do
            parent_config.update(device_firmware: new_firmware)
            child_config.update(device_firmware: old_firmware, device_firmware_locked: true,
                                settings: child_settings, compiled_settings: child_settings)
            parent_config.reload
            described_class.call(device_configuration: parent_config)
            child_config.reload
          end

          it 'does not compile the settings' do
            expect(child_config.parsed_compiled_settings['schema']['version']).to eq('1')
          end

          it 'sets the parent disconnect flag on the child' do
            expect(child_config.parent_disconnect).to be true
          end
        end
      end
    end

    context 'when a new parent firmware schema and setting conflicts with a child firmware schema causing an invalid configuration in the child' do
      let(:parent_settings) do
        '{ "schema": { "version": "126" }, "firmware": { "version": "16.1.100" }, "config": { "secure_port": "443" }, "auth": { "user": { "root": { "password": "abc123" } } } }'
      end
      let(:child_settings) do
        '{ "schema": { "version": "126" }, "firmware": { "version": "15.1.100" }, "auth": { "user": { "root": { "password": "xyz789" }, "test": { "password": "def456" } } } }'
      end
      let(:old_firmware) do
        create(:device_firmware, device_model: parent_config.device_model,
                                 migration_versions: '~1~')
      end
      let(:parent_config) do
        create(:device_configuration, :with_sequenced_device_model_and_alternate_firmware,
               settings: parent_settings, compiled_settings: parent_settings)
      end
      let!(:child_config) do
        create(:device_configuration, parent: parent_config,
                                      settings: child_settings, compiled_settings: child_settings,
                                      device_firmware: old_firmware, device_firmware_locked: true)
      end

      context 'when the parent is compiled with new settings' do
        let(:new_firmware) do
          create(:device_firmware, :with_alternate_schema, migration_versions: '~1~2~',
                                                           device_model: parent_config.device_model)
        end
        let(:updated_parent_settings) do
          '{ "schema": { "version": "126" }, "firmware": { "version": "16.1.100" }, "config": { "secure_port": "443" }, "auth": { "user": { "root": { "password": "newPass" } } } }'
        end

        let(:result) { described_class.call(device_configuration: parent_config) }

        before do
          parent_config.settings = updated_parent_settings
          parent_config.device_firmware = new_firmware
          result
        end

        it 'succeeds' do
          expect(result).to be_a_success
        end

        it 'does not merge with the child configuration' do
          expect(child_config.parsed_compiled_settings).not_to have_key('config')
          expect(child_config.compiled_settings).to eq(child_settings)
        end

        it 'sets parent disconnect on the child' do
          child_config.reload
          expect(child_config.parent_disconnect).to be true
        end

        it 'continues to update its own valid settings' do
          expect(parent_config.compiled_settings).to eq(updated_parent_settings)
        end

        it 'adds the validation errors on the child to the context' do
          error = result.errors.first
          expect(error[:configuration_name]).to eq(child_config.name)
          expect(error[:message]).to eq('firmware cannot be migrated')
        end

        it 'adds a message to the context giving the number of failed and successful children' do
          expect(result.message).to eq('Could not compile 1 device_configuration. Successfully compiled 0 device_configurations.')
        end
      end
    end

    context 'when the parent selects a new firmware and the child has not locked its firmware' do
      let(:parent_config) { create(:device_configuration) }
      let!(:child_config) { create(:device_configuration, :with_sequenced_device_model, parent: parent_config) }

      let!(:result) { described_class.call(device_configuration: parent_config) }

      it 'compiles the child such that the child gets the parent firmware' do
        child_config.reload
        expect(child_config.device_firmware).to eq(parent_config.device_firmware)
      end

      context 'when the parent updates the firmware and the child has not' do
        let(:new_parent_firmware) { create(:device_firmware, :with_sequenced_device_model) }

        before do
          parent_config.device_firmware = new_parent_firmware
          parent_config.save
          parent_config.reload
          described_class.call(device_configuration: parent_config)
        end

        it 'compiles the child such that the child gets the new parent firmware' do
          child_config.reload
          expect(child_config.device_firmware).to eq(new_parent_firmware)
        end
      end
    end

    context 'when the child sets a new firmware' do
      let(:new_child_firmware) { create(:device_firmware, :with_sequenced_device_model) }
      let(:parent_config) { create(:device_configuration) }
      let(:child_config) do
        create(:device_configuration, device_firmware: new_child_firmware,
                                      device_firmware_locked: true, parent: parent_config)
      end

      before { described_class.call(device_configuration: child_config) }

      it 'takes the new firmware' do
        expect(child_config.device_firmware).to eq(new_child_firmware)
      end

      context 'when the parent also selects a new firmware' do
        let(:new_parent_firmware) { create(:device_firmware, :with_sequenced_device_model) }

        before do
          parent_config.device_firmware = new_parent_firmware
          parent_config.save
          parent_config.reload
          described_class.call(device_configuration: parent_config)
        end

        it 'does not take changes made to the parent firmware' do
          child_config.reload
          expect(child_config.device_firmware).to eq(new_child_firmware)
        end
      end
    end

    context 'when the parent selects a new grace period and the child has not set a grace period' do
      let(:parent_grace_period) { '10m' }
      let(:parent_config) { create(:device_configuration, grace_period_duration: parent_grace_period) }
      let!(:child_config) { create(:device_configuration, :with_sequenced_device_model, parent: parent_config) }

      let!(:result) { described_class.call(device_configuration: parent_config) }

      it 'compiles the child such that the child gets the parent grace period' do
        child_config.reload
        expect(child_config.grace_period_duration).to eq(parent_grace_period)
      end

      context 'when the parent updates the grace period and the child has not' do
        let(:new_parent_grace_period) { '20m' }

        before do
          parent_config.grace_period_duration = new_parent_grace_period
          parent_config.save
          parent_config.reload
          described_class.call(device_configuration: parent_config)
        end

        it 'compiles the child such that the child gets the new parent grace period' do
          child_config.reload
          expect(child_config.grace_period_duration).to eq(new_parent_grace_period)
        end
      end
    end

    context 'when the child sets a new grace period duration' do
      let(:child_grace_period) { '5m' }
      let(:parent_grace_period) { '10m' }
      let(:parent_config) { create(:device_configuration, grace_period_duration: parent_grace_period) }
      let!(:child_config) do
        create(:device_configuration, :with_sequenced_device_model,
               grace_period_duration: child_grace_period, grace_period_duration_locked: true,
               parent: parent_config)
      end

      before { described_class.call(device_configuration: child_config) }

      it 'takes the new grace period duration' do
        expect(child_config.grace_period_duration).to eq(child_grace_period)
      end

      context 'when the parent also selects a new grace period duration' do
        let(:new_parent_grace_period) { '20m' }

        before do
          parent_config.grace_period_duration = new_parent_grace_period
          parent_config.save
          parent_config.reload
          described_class.call(device_configuration: parent_config)
        end

        it 'does not take changes made to the parent grace period duration' do
          child_config.reload
          expect(child_config.grace_period_duration).to eq(child_grace_period)
        end
      end
    end
  end
end
