# frozen_string_literal: true

require 'rails_helper'

describe CreateRole do
  subject { described_class }

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when arguments are given' do
    before do
      @organization = create(:random_regular_organization)
      @user         = create(:user, organization: @organization)
      @root_role    = @organization.admin_role

      @device_ability = create(:device_ability)
      @root_role.abilities << @device_ability

      @params       = { name: 'Testing', description: 'Test role' }

      @attributes   = {
        user:         @user,
        organization: @organization,
        params:       @params
      }
    end

    it 'succeeds' do
      result = subject.call(@attributes)
      expect(result).to be_a_success
    end

    it 'creates a new role' do
      expect do
        subject.call(@attributes)
      end.to change(Role, :count).by(1)
    end

    it 'sets the role name' do
      result = subject.call(@attributes)
      expect(result.role.name).to eq 'Testing'
    end

    it 'sets the role description' do
      result = subject.call(@attributes)
      expect(result.role.description).to eq 'Test role'
    end

    context 'when an invalid name is given' do
      before do
        @params[:name] = ''
      end

      it 'fails' do
        result = subject.call(@attributes)
        expect(result).to be_a_failure
        expect(result.role.errors.full_messages).to include("Name can't be blank")
      end
    end

    context 'when ability attributes are given' do
      before do
        # Create abilities that are available to the user.
        @user_role = create(:role, organization: @user.organization)
        @user_role.abilities << @device_ability

        # Create a permission with the desired role.
        Permission.create(manager: @user, manageable: @user.organization, role: @user_role)

        # Create an ability that is not available to the user.
        @configuration_ability = create(:configuration_ability)

        # Ensure we have abilities inside and outside user's scope.
        expect(@user_role.abilities.include?(@device_ability)).to be true
        expect(@user_role.abilities.include?(@configuration_ability)).to be false

        @params['abilities_attributes'] = {
          @device_ability.id.to_s        => { id: @device_ability.id        },
          @configuration_ability.id.to_s => { id: @configuration_ability.id }
        }
      end

      it 'allows abilities within the user`s scope' do
        result = subject.call(@attributes)
        expect(result.role.abilities).to include(@device_ability)
      end

      it 'does not allow abilities outside the user`s scope' do
        result = subject.call(@attributes)
        expect(result.role.abilities).not_to include(@configuration_ability)
      end
    end

    context 'when no abilities attributes are given' do
      it 'has no abilities' do
        result = subject.call(@attributes)
        expect(result.role.abilities).to be_empty
      end
    end
  end
end
