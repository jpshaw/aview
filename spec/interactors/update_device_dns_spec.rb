# frozen_string_literal: true

require 'rails_helper'

describe UpdateDeviceDns do
  subject { described_class }

  let(:host)             { '10.20.30.40'  }
  let(:mac)              { '002704000000' }
  let(:dns_hostname)     { "#{mac}.#{Settings.powerdns.domain.name}" }
  let(:ipv4_record_type) { UpdateDeviceDns::IPV4_RECORD_TYPE }
  let(:ipv6_record_type) { UpdateDeviceDns::IPV6_RECORD_TYPE }
  let(:params)           { { host: host, mac: mac } }

  context 'when given an empty hash' do
    it 'fails' do
      expect(subject.call({})).to be_a_failure
    end
  end

  context 'when given an invalid host' do
    before do
      params[:host] = 'invalid_host'
    end

    it 'fails' do
      expect(subject.call(params)).to be_a_failure
    end
  end

  context 'when given valid params' do
    it 'succeeds' do
      expect(subject.call(params)).to be_a_success
    end

    it 'sets the dns record content to the host' do
      subject.call(params)
      expect(PowerDnsRecordManager::Record.last.content).to eq host
    end

    context 'when the device has no dns records' do
      it 'creates one' do
        expect do
          subject.call(params)
        end.to change { PowerDnsRecordManager::Record.count }.by(1)
      end
    end

    context 'when the device has a dns record for IPv4' do
      before do
        @record = PowerDnsRecordManager::Record.create!(name: dns_hostname,
                                                        type: ipv4_record_type,
                                                        content: '1.2.3.4')
      end

      it 'does not create a new one' do
        expect do
          subject.call(params)
        end.not_to change { PowerDnsRecordManager::Record.count }
      end

      it 'updates the record content' do
        expect do
          subject.call(params)
        end.to change { @record.reload.content }.to(host)
      end
    end

    context 'when the device has a dns record for IPv6' do
      before do
        @record = PowerDnsRecordManager::Record.create!(name: dns_hostname,
                                                        type: ipv6_record_type,
                                                        content: '2620:0:1cfe:face:b00c::3')
      end

      it 'destroys it' do
        subject.call(params)
        expect(PowerDnsRecordManager::Record.where(id: @record.id)).not_to exist
      end

      it 'creates an IPv4 record' do
        subject.call(params)
        expect(PowerDnsRecordManager::Record.last.type).to eq ipv4_record_type
      end
    end
  end
end
