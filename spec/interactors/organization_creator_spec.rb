# frozen_string_literal: true

require 'rails_helper'

describe OrganizationCreator do
  it 'runs 3 interactors' do
    expect(described_class.organized.size).to be 3
  end

  it 'runs the CreateOrganization interactor first' do
    expect(described_class.organized[0]).to eq(CreateOrganization)
  end

  it 'runs the CreateOrganizationAdminRole interactor second' do
    expect(described_class.organized[1]).to eq(CreateOrganizationAdminRole)
  end

  it 'runs the CreateOrganizationSelfManagingPermission interactor third' do
    expect(described_class.organized[2]).to eq(CreateOrganizationSelfManagingPermission)
  end
end
