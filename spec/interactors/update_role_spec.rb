# frozen_string_literal: true

require 'rails_helper'

describe UpdateRole do
  subject { described_class }

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when arguments are given' do
    before do
      @user   = create(:user)
      @role   = create(:role)
      @params = { name: 'Testing', description: 'Test role' }

      @attributes = {
        user:   @user,
        role:   @role,
        params: @params
      }
    end

    context 'when the arguments are valid' do
      it 'succeeds' do
        expect(subject.call(@attributes)).to be_a_success
      end
    end

    context 'when given an invalid name' do
      before do
        @params[:name] = nil
      end

      it 'fails' do
        result = subject.call(@attributes)
        expect(result).to be_a_failure
        expect(result.role.errors.full_messages).to include("Name can't be blank")
      end
    end

    context 'when given arguments to add abilities to the role' do
      before do
        @user_role = create(:role, organization: @user.organization)

        @abilities = []

        @abilities << create(:organization_ability)
        @abilities << create(:user_ability)
        @abilities << create(:device_ability)

        @ability_not_available_to_user = create(:configuration_ability)

        @abilities.each do |ability|
          @user_role.abilities << ability
        end

        Permission.create(manager: @user.organization, manageable: @role.organization, role: @user_role)
        Permission.create(manager: @user, manageable: @role.organization, role: @user_role)

        @params['abilities_attributes'] = {}

        @abilities.each do |ability|
          @params['abilities_attributes'][ability.id.to_s] = ability.id
        end
      end

      it 'only adds abilities available to the user' do
        expect(@role.abilities).to eq []
        result = subject.call(@attributes)
        expect(result).to be_a_success
        expect(result.role.abilities.sort).to eq @abilities.sort
      end

      it 'does not add abilities not available to the user' do
        @params['abilities_attributes'][@ability_not_available_to_user.id.to_s] = @ability_not_available_to_user.id
        expect(@role.abilities).to eq []
        result = subject.call(@attributes)
        expect(result).to be_a_success
        expect(result.role.abilities).not_to include(@ability_not_available_to_user)
      end

      context 'when removing abilities' do
        before do
          @abilities.each do |ability|
            @role.abilities << ability
          end

          @role.abilities << @ability_not_available_to_user

          @params['abilities_attributes'] = {}
        end

        it 'does not remove abilities not available to the user' do
          result = subject.call(@attributes)
          expect(result).to be_a_success
          expect(result.role.abilities).to eq([@ability_not_available_to_user])
        end
      end
    end
  end
end
