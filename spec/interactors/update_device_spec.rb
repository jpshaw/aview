# frozen_string_literal: true

require 'rails_helper'

describe UpdateDevice do
  subject { described_class }

  let(:organization)  { create(:random_regular_organization) }
  let(:old_site_name) { 'Test Site 01' }
  let(:old_mgmt_host) { '2620:000e:4000:8890:0000:0000:0101:5ba6' }
  let(:site)          { create(:site, organization: organization, name: old_site_name) }
  let(:device)        { create(:netbridge_device, organization: organization, site: site, mgmt_host: old_mgmt_host) }
  let(:user)          { create(:user, organization: organization) }
  let(:configuration) { create(:netbridge_configuration, organization: organization) }
  let(:new_org)       { create(:random_regular_organization) }
  let(:new_mgmt_host) { '2620:000e:4000:8890:0000:0000:0101:5ba7' }
  let(:params)        do
    { organization_id: new_org.id,
      configuration_id: configuration.id,
      mgmt_host: new_mgmt_host }
  end
  let(:attributes) { { device: device, params: params, user: user } }

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when attributes are given' do
    it 'succeeds' do
      result = subject.call(attributes)
      expect(result).to be_a_success
    end
  end

  context 'when the user has no permissions' do
    before do
      allow(user).to receive(:has_ability_on?).and_return(false)
    end

    it 'does not update the device' do
      result = subject.call(attributes)
      device.reload
      expect(device.organization).to eq organization
      expect(device.configuration).to be nil
      expect(device.site_name).to eq old_site_name
      expect(device.mgmt_host).to eq old_mgmt_host
    end
  end

  context 'when the user has the ability to modify configurations' do
    before do
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Devices').and_return(false)
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Configurations').and_return(true)
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Organizations').and_return(false)
    end

    it 'updates the configuration' do
      result = subject.call(attributes)
      device.reload
      expect(device.configuration).to eq configuration
    end
  end

  context 'when a data plan is passed with the params' do
    let(:data_plan)  { create(:data_plan, organization: organization) }
    let(:params)     { { data_plan_id: data_plan.id } }
    let(:attributes) { { device: device, params: params, user: user } }

    before do
      allow(user).to receive(:has_ability_on?).and_return(true)
    end

    it 'updates the data plan' do
      result = subject.call(attributes)
      expect(result).to be_a_success
      device.reload
      expect(device.data_plan).to eq data_plan
    end
  end
end
