# frozen_string_literal: true

require 'rails_helper'

describe UpdateUser do
  subject { described_class }

  let(:user) { create(:user) }

  # Creates a hash of accessible attributes set for the test user.
  let(:params) do
    params     = {}
    attributes = user.class.accessible_attributes.to_a
    attributes.each do |a|
      value = begin
                user.send(a)
              rescue StandardError
                nil
              end
      next if value.blank?
      params[a] = value
    end
    params
  end

  context 'when nothing is passed' do
    it 'fails' do
      result = subject.call
      expect(result).to be_a_failure
    end
  end

  context 'when just the params are passed' do
    it 'fails' do
      result = subject.call(params: params)
      expect(result).to be_a_failure
    end
  end

  context 'when user and params are passed' do
    context 'when valid params are passed' do
      it 'succeeds' do
        result = subject.call(user: user, params: params)
        expect(result).to be_a_success
      end
    end

    context 'when permission params are passed' do
      context 'with valid permission params' do
        before do
          parent_organization = user.organization
          @role = parent_organization.roles.create name: 'Test'
          @new_role = parent_organization.roles.create name: 'Test 2'

          # Manageable permission to update
          @manageable_org_1 = create(:sub_organization)
          @org_1_permission = user.manager_permissions.create! do |p|
            p.manageable = @manageable_org_1
            p.role       = @role
          end

          # Manageable permission to create
          @manageable_org_2 = create(:sub_organization)

          # Manageable permission to destroy
          @manageable_org_3 = create(:sub_organization)
          user.manager_permissions.create! do |p|
            p.manageable = @manageable_org_3
            p.role       = @role
          end

          permission_params = {
            @org_1_permission.id.to_s => {
              'manageable_id'   => @manageable_org_1.id.to_s,
              'manageable_type' => Permission::MANAGEABLE_TYPE_ORGANIZATION,
              'role_id'         => @new_role.id.to_s
            },
            '0' => {
              'manageable_id' => @manageable_org_2.id.to_s,
              'manageable_type' => Permission::MANAGEABLE_TYPE_ORGANIZATION,
              'role_id' => @new_role.id.to_s
            }
          }

          params.merge!('manager_permissions_attributes' => permission_params)
        end

        it 'succeeds' do
          result = subject.call(user: user, params: params)
          expect(result).to be_a_success
        end

        it 'creates a new permission' do
          expect do
            subject.call(user: user, params: params)
          end.to change {
            user.manager_permissions
                .where(manageable_id: @manageable_org_2.id)
                .count
          }.by(1)
        end

        it 'updates an existing permission' do
          expect(@org_1_permission.role).to eq @role
          subject.call(user: user, params: params)
          @org_1_permission.reload
          expect(@org_1_permission.role).to eq @new_role
        end

        it 'removes permissions not passed in the params' do
          expect do
            subject.call(user: user, params: params)
          end.to change {
            user.manager_permissions
                .where(manageable_id: @manageable_org_3.id)
                .count
          }.by(-1)
        end
      end

      context 'with invalid permission params' do
        context 'when the manageable is not in the user`s scope' do
          before do
            parent_organization = user.organization
            role = parent_organization.roles.create name: 'Test'

            # Manageable permission to update
            manageable_org_1 = create(:sub_organization)
            org_1_permission = user.manager_permissions.create! do |p|
              p.manageable = manageable_org_1
              p.role       = role
            end

            manageable_org_2 = create(:random_regular_organization)

            permission_params = {
              org_1_permission.id.to_s => {
                'manageable_id'   => manageable_org_1.id.to_s,
                'manageable_type' => Permission::MANAGEABLE_TYPE_ORGANIZATION,
                'role_id'         => role.id.to_s
              },
              '0' => {
                'manageable_id'   => manageable_org_2.id.to_s,
                'manageable_type' => Permission::MANAGEABLE_TYPE_ORGANIZATION,
                'role_id'         => role.id.to_s
              }
            }

            params.merge!('manager_permissions_attributes' => permission_params)
          end

          it 'fails to find the manageable' do
            result = subject.call(user: user, params: params)
            expect(result).to be_a_failure
            has_error_message = user.errors.full_messages.any? { |e| e['Unable to find record'] }
            expect(has_error_message).to be true
          end
        end
      end
    end
  end
end
