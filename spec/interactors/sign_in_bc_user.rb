# frozen_string_literal: true

require 'rails_helper'

describe SignInBcUser do
  subject { described_class }

  let(:params) do
    attributes_for(:user).merge(
      timezone_id: create(:timezone).id
    )
  end

  let(:organization) { create(:random_regular_organization) }

  context 'when nothing is passed' do
    it 'fails' do
      result = subject.call
      expect(result).to be_a_failure
    end
  end
end
