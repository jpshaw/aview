# frozen_string_literal: true

require 'rails_helper'

describe MoveDevicesToOrganization do
  let(:user) { create(:user_with_all_abilities, organization: old_organization) }
  let(:old_organization) { create(:organization) }
  let(:new_organization) { create(:organization) }
  let(:device_1) { create(:dial_to_ip_device, organization: old_organization) }
  let(:device_2) { create(:dial_to_ip_device, organization: old_organization) }
  let(:bad_organization_id) { '9999' }
  let(:bad_mac_1) { '123' }
  let(:bad_mac_2) { '456' }

  before do
    role = create(:role, organization: new_organization)
    create(:permission, manager: old_organization, manageable: new_organization)
    create(:permission, manager: user, manageable: new_organization, role: role)
  end

  describe '#call' do
    context 'when given no mac addresses' do
      let(:result) do
        described_class.call(current_user: user,
                             organization_id: new_organization.id)
      end

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when given an organization id that does not exist in the database' do
      let(:result) do
        described_class.call(current_user: user,
                             organization_id: bad_organization_id,
                             device_macs: "#{device_1.mac},#{device_2.mac}")
      end

      it 'fails' do
        expect(result).to be_a_failure
      end
    end

    context 'when given a mac address that does not exist in the database' do
      let(:result) do
        described_class.call(current_user: user,
                             organization_id: new_organization.id,
                             device_macs: "#{bad_mac_1},#{bad_mac_2}")
      end

      it 'fails' do
        expect(result).to be_a_failure
      end

      it 'returns an error for each device not found' do
        expect(result.errors.count).to eq(2)
      end
    end

    context 'when given a valid organization id and a valid list of mac addresses' do
      context 'when the save is successful' do
        let!(:result) do
          described_class.call(current_user: user,
                               organization_id: new_organization.id,
                               device_macs: "#{device_1.mac},#{device_2.mac}")
        end

        it 'succeeds' do
          expect(result).to be_a_success
        end

        it 'assigns the given organization to each given device' do
          device_1.reload
          device_2.reload
          expect(device_1.organization).to eq(new_organization)
          expect(device_2.organization).to eq(new_organization)
        end

        it 'adds to the device count the number of devices successfully assigned' do
          expect(result.device_count).to eq(2)
        end
      end

      context 'when copy site id is true' do
        let!(:old_site) { create(:site, name: 'Old Site', organization: old_organization) }
        let(:device_1) { create(:netbridge_device, organization: old_organization, site: old_site) }
        let(:device_2) { create(:netbridge_device, organization: old_organization, site: old_site) }

        let(:result) do
          described_class.call(current_user: user,
                               organization_id: new_organization.id,
                               device_macs: "#{device_1.mac},#{device_2.mac}",
                               copy_site_id: true)
        end

        context 'when the target organization does not have a site with the same name' do
          it 'creates a new site with the old name for the new organization and assigns it to the device' do
            expect(result).to be_a_success
            device_1.reload
            device_2.reload
            new_site = new_organization.sites.find_by(name: old_site.name)
            expect(device_1.site).to eq(new_site)
            expect(device_2.site).to eq(new_site)
          end
        end

        context 'when the target organization has a site with the same name' do
          let!(:existing_site) { create(:site, name: old_site.name, organization: new_organization) }

          it 'assigns the existing site from the new organization to the device' do
            expect(result).to be_a_success
            device_1.reload
            device_2.reload
            expect(device_1.site).to eq(existing_site)
            expect(device_2.site).to eq(existing_site)
          end
        end
      end

      context 'when copy site id is not present' do
        let!(:old_site) { create(:site, name: 'Old Site', organization: old_organization) }
        let(:device_1) { create(:netbridge_device, organization: old_organization, site: old_site) }
        let(:device_2) { create(:netbridge_device, organization: old_organization, site: old_site) }

        let(:result) do
          described_class.call(current_user: user,
                               organization_id: new_organization.id,
                               device_macs: "#{device_1.mac},#{device_2.mac}")
        end

        it 'assigns the new organization default site to the device' do
          expect(result).to be_a_success
          device_1.reload
          device_2.reload
          expect(device_1.site).to eq(new_organization.default_site)
          expect(device_2.site).to eq(new_organization.default_site)
        end
      end

      context 'when copy configuration is not present' do
        let(:firmware) { create(:device_firmware, device_model: device_1.series) }
        let(:parent_config) { create(:device_configuration, device_firmware: firmware) }
        let(:child_config) { create(:device_configuration, device_firmware: firmware, record_type: 'individual') }

        context 'when the device is not a gateway' do
          before do
            device_1.update(configuration_id: parent_config.id)
            device_2.update(configuration_id: child_config.id)
          end

          let(:result) do
            described_class.call(current_user: user,
                                 organization_id: new_organization.id,
                                 device_macs: "#{device_1.mac},#{device_2.mac}")
          end

          it 'removes the configuration from the device' do
            expect(result).to be_a_success
            device_1.reload
            device_2.reload
            expect(device_1.configuration).to be_nil
            expect(device_2.configuration).to be_nil
          end

          it 'destroys individual configurations' do
            expect do
              result
            end.to change(DeviceConfiguration, :count).by(-1)
          end
        end

        context 'when the device is a gateway' do
          let!(:old_configuration) { create(:gateway_configuration, organization: old_organization) }
          let(:gateway_device) { create(:gateway_device, organization: old_organization, configuration: old_configuration) }

          let(:result) do
            described_class.call(current_user: user,
                                 organization_id: new_organization.id,
                                 device_macs: gateway_device.mac.to_s)
          end

          it 'copies the gateway configuration to the new organization' do
            expect(result).to be_a_success
            gateway_device.reload
            expect(gateway_device.configuration.organization).to eq(new_organization)
          end
        end
      end

      context 'when copy data plan is true' do
        context 'when the device has a data plan' do
          let!(:old_data_plan) { create(:data_plan, organization: old_organization) }
          let(:device_1) { create(:netbridge_device, organization: old_organization, data_plan: old_data_plan) }
          let(:device_2) { create(:netbridge_device, organization: old_organization, data_plan: old_data_plan) }

          let(:result) do
            described_class.call(current_user: user,
                                 organization_id: new_organization.id,
                                 device_macs: "#{device_1.mac},#{device_2.mac}",
                                 copy_data_plan: true)
          end

          context 'when the target organization does not have a data plan with the same name' do
            it 'copies the data plan with the old name for the new organization and assigns it to the device' do
              expect(result).to be_a_success
              device_1.reload
              device_2.reload
              new_data_plan = new_organization.data_plans.find_by(name: old_data_plan.name)
              expect(device_1.data_plan).to eq(new_data_plan)
              expect(device_2.data_plan).to eq(new_data_plan)
            end
          end

          context 'when the target organization has a data plan with the same name' do
            let!(:existing_data_plan) { create(:data_plan, organization: new_organization, name: old_data_plan.name) }

            it 'assigns the existing data plan from the new organization to the device' do
              expect(result).to be_a_success
              device_1.reload
              device_2.reload
              expect(device_1.data_plan).to eq(existing_data_plan)
              expect(device_2.data_plan).to eq(existing_data_plan)
            end
          end
        end

        context 'when the device does not have a data plan' do
          let(:result) do
            described_class.call(current_user: user,
                                 organization_id: new_organization.id,
                                 device_macs: "#{device_1.mac},#{device_2.mac}",
                                 copy_data_plan: true)
          end

          it 'assigns the organization and does nothing with data plans' do
            expect(result).to be_a_success
            expect(DataPlan.count).to eq 0
          end
        end
      end

      context 'when copy data plan is not present' do
        let!(:old_data_plan) { create(:data_plan, organization: old_organization) }
        let(:device_1) { create(:netbridge_device, organization: old_organization, data_plan: old_data_plan) }
        let(:device_2) { create(:netbridge_device, organization: old_organization, data_plan: old_data_plan) }

        let(:result) do
          described_class.call(current_user: user,
                               organization_id: new_organization.id,
                               device_macs: "#{device_1.mac},#{device_2.mac}")
        end

        it 'removes the data plan from the device' do
          expect(result).to be_a_success
          device_1.reload
          device_2.reload
          expect(device_1.data_plan).to be_nil
          expect(device_2.data_plan).to be_nil
        end
      end

      context 'when copy site, copy configuration, and copy data plan are present' do
        let!(:old_site) { create(:site, name: 'Old Site', organization: old_organization) }
        let!(:old_configuration) { create(:netbridge_configuration, organization: old_organization) }
        let!(:old_data_plan) { create(:data_plan, organization: old_organization) }
        let(:device_1) do
          create(:netbridge_device, organization: old_organization, site: old_site,
                                    configuration: old_configuration, data_plan: old_data_plan)
        end
        let(:device_2) do
          create(:netbridge_device, organization: old_organization, site: old_site,
                                    configuration: old_configuration, data_plan: old_data_plan)
        end

        context 'when the new duplicate data plan is not created successfully' do
          before do
            allow_any_instance_of(DataPlan).to receive(:valid?).and_return(false)
            @result = described_class.call(current_user: user,
                                           organization_id: new_organization.id,
                                           device_macs: "#{device_1.mac},#{device_2.mac}",
                                           copy_site_id: true, copy_configuration: true, copy_data_plan: true)
          end

          it 'fails' do
            expect(@result).to be_a_failure
          end

          it 'adds an error to the context that explains the Data Plan failed for each device that failed' do
            expect(@result.errors).to include("#{device_1.mac}: Failed to save new data plan")
            expect(@result.errors).to include("#{device_2.mac}: Failed to save new data plan")
          end

          it 'does not create a site and configuration' do
            new_site = Site.where(organization: new_organization, name: old_site.name)
            new_configuration = NetbridgeConfiguration.where(organization: new_organization, name: old_configuration.name)
            expect(new_site).to be_empty
            expect(new_configuration).to be_empty
          end
        end
      end
    end

    context 'when given an empty organization id' do
      let!(:result) do
        described_class.call(current_user: user,
                             organization_id: '',
                             device_macs: "#{device_1.mac},#{device_2.mac}")
      end

      it 'fails' do
        expect(result).to be_a_failure
      end

      it 'does not update the organization' do
        device_1.reload
        device_2.reload
        expect(device_1.organization).to eq(old_organization)
        expect(device_2.organization).to eq(old_organization)
      end
    end
  end
end
