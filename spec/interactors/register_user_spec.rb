# frozen_string_literal: true

require 'rails_helper'

describe RegisterUser do
  subject { described_class }

  let(:params) do
    attributes_for(:user).merge(
      timezone_id: create(:timezone).id
    )
  end

  let(:organization) { create(:random_regular_organization) }

  context 'when nothing is passed' do
    it 'fails' do
      result = subject.call
      expect(result).to be_a_failure
    end
  end

  context 'when just the organization is passed' do
    it 'fails' do
      result = subject.call(organization: organization)
      expect(result).to be_a_failure
    end
  end

  context 'when just the params are passed' do
    it 'fails' do
      result = subject.call(params: params)
      expect(result).to be_a_failure
    end
  end

  context 'when organization and params are passed' do
    context 'when valid params are passed' do
      it 'succeeds' do
        result = subject.call(organization: organization, params: params)
        expect(result).to be_a_success
      end

      context 'when permission params are passed' do
        before do
          role = create(:role, name: 'Test', organization: organization)
          Permission.create(role: role, manageable: organization, manager: organization)
          params.merge!(
            manager_permissions_attributes: {
              '1' => {
                manageable_type: Permission::MANAGEABLE_TYPE_ORGANIZATION,
                manageable_id:   organization.id,
                role_id:         role.id
              }
            }
          )
        end

        it 'adds the permission' do
          expect do
            subject.call(organization: organization, params: params)
          end.to change(Permission, :count).by(1)
        end
      end
    end

    context 'when invalid params are passed' do
      let(:params) { {} }

      it 'fails' do
        result = subject.call(organization: organization, params: params)
        expect(result).to be_a_failure
      end
    end
  end
end
