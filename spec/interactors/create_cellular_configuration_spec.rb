# frozen_string_literal: true

require 'rails_helper'

describe CreateCellularConfiguration do
  subject { described_class }

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when attributes are given' do
    before do
      @organization = create(:organization)
      @current_user = create(:user, organization: @organization)

      @config_attributes = random_attributes

      @locks  = {}
      @params = {
        name:            'Test',
        organization_id: @organization.id
      }.merge(@config_attributes)

      @attributes = {
        params: @params,
        locks:  @locks,
        user:   @current_user
      }
    end

    context 'when the organization id is within the user scope' do
      before do
        Permission.create!(
          manager:    @current_user,
          manageable: @organization,
          role:       Role.new
        )
        expect(@current_user.organizations).to include(@organization)
      end

      it 'succeeds' do
        result = subject.call(@attributes)
        expect(result).to be_a_success
      end

      it 'creates a new configuration' do
        expect do
          subject.call(@attributes)
        end.to change(NetbridgeConfiguration, :count).by(1)
      end

      context 'when the parent configuration is within the user scope' do
        before do
          parent = NetbridgeConfiguration.create!(
            name:            'Parent',
            organization_id: @organization.id
          )
          @params[:parent_id] = parent.id
        end

        it 'succeeds' do
          result = subject.call(@attributes)
          expect(result).to be_a_success
        end
      end

      context 'when the parent configuration is not within the user scope' do
        before do
          organization = create(:random_regular_organization)
          parent = NetbridgeConfiguration.create!(
            name:            'Parent',
            organization_id: organization.id
          )
          @params[:parent_id] = parent.id
        end

        it 'fails' do
          result = subject.call(@attributes)
          expect(result).to be_a_failure
        end
      end
    end

    context 'when the organization id is outside the user scope' do
      before do
        organization = create(:random_regular_organization)
        @params[:organization_id] = organization.id
        expect(@current_user.organizations).not_to include(organization)
      end

      it 'fails' do
        result = subject.call(@attributes)
        expect(result).to be_a_failure
      end
    end
  end
end
