# frozen_string_literal: true

require 'rails_helper'

describe UpdateOrganization do
  subject { described_class }

  before do
    @root_org     = create(:organization)
    @current_user = create(:user, organization: @root_org)

    root_role = @root_org.admin_role
    root_role.abilities << create(:organization_ability)

    Permission.create(manager: @current_user, manageable: @root_org, role: root_role)

    @parent_organization = create(:random_regular_organization)
    @organization        = create(:random_regular_organization)

    @params = {}

    @attributes = {
      organization: @organization,
      user:         @current_user,
      params:       @params
    }
  end

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when valid attributes are given' do
    before do
      @params[:name] = 'Test Name'
    end

    it 'succeeds' do
      result = subject.call(@attributes)
      expect(result).to be_a_success
    end

    context 'when changing the parent organization' do
      before do
        @new_parent = create(:random_regular_organization)
        @parent_organization.add_child(@organization)

        @params[:parent_id] = @new_parent.id
      end

      context 'when the parent is the same as the organization' do
        before do
          @params[:parent_id] = @organization.id
        end

        it 'does not change the parent' do
          result = subject.call(@attributes)
          expect(result.organization.parent).to eq @parent_organization
        end
      end

      context 'when the organization is a root' do
        before do
          @organization              = Organization.root
          @attributes[:organization] = @organization
        end

        it 'does not change the parent' do
          result = subject.call(@attributes)
          expect(result.organization.parent).to eq @organization.parent
        end
      end

      context 'when the organization is not a root' do
        before do
          expect(@organization.root?).to be false
        end

        context 'when the user cannot modify the organization`s current parent' do
          before do
            expect(@current_user.modifiable_organizations).not_to include(@organization.parent)
          end

          it 'does not change the parent' do
            result = subject.call(@attributes)
            expect(result.organization.parent).to eq @parent_organization
          end
        end

        context 'when the user can modify the organization`s current parent' do
          before do
            @root_org.add_child(@parent_organization)
            expect(@current_user.modifiable_organizations).to include(@organization.parent)
          end

          context 'when the user cannot access the new parent' do
            before do
              expect(@current_user.modifiable_organizations).not_to include(@new_parent)
            end

            it 'does not change the parent' do
              result = subject.call(@attributes)
              expect(result.organization.parent).to eq @parent_organization
            end
          end

          context 'when the user can access the new parent' do
            before do
              @root_org.add_child(@new_parent)
            end

            it 'changes the parent' do
              result = subject.call(@attributes)
              expect(result.organization.parent).to eq @new_parent
            end
          end
        end
      end
    end

    context 'when changing the logo' do
      before do
        @attributes[:user].update_attribute(:organization, @attributes[:organization])
        @params['logo'] = nil # It only needs the key for the test.

        # Enabling caching.
        ActionController::Base.perform_caching  = true
        ActionController::Base.cache_store      = :file_store, 'tmp/cache'
      end

      after do
        # Disabling caching and clean up.
        ActionController::Base.perform_caching = false
      end

      it 'expires the cache for the logo' do
        expect(Rails.cache).to receive(:delete).with("#{@attributes[:organization].id}_logo")
        subject.call(@attributes)
      end
    end

    context 'when removing the logo' do
      before do
        @attributes[:user].update_attribute(:organization, @attributes[:organization])
        @params['remove_logo'] = nil # It only needs the key for the test.

        # Enabling caching.
        ActionController::Base.perform_caching  = true
        ActionController::Base.cache_store      = :file_store, 'tmp/cache'
      end

      after do
        # Disabling caching and clean up.
        ActionController::Base.perform_caching = false
      end

      it 'expires the cache for the logo' do
        expect(Rails.cache).to receive(:delete).with("#{@attributes[:organization].id}_logo")
        subject.call(@attributes)
      end
    end

    context 'when changing the footer' do
      before do
        @attributes[:user].update_attribute(:organization, @attributes[:organization])
        @params['footer'] = nil # It only needs the key for the test.

        # Enabling caching.
        ActionController::Base.perform_caching  = true
        ActionController::Base.cache_store      = :file_store, 'tmp/cache'
      end

      after do
        # Disabling caching and clean up.
        ActionController::Base.perform_caching = false
      end

      it 'expires the cache for the footer' do
        expect(Rails.cache).to receive(:delete).with("#{@attributes[:organization].id}_footer")
        subject.call(@attributes)
      end
    end
  end

  context 'when invalid attributes are given' do
    before do
      @params[:name] = nil
    end

    it 'fails' do
      result = subject.call(@attributes)
      expect(result).to be_a_failure
    end
  end
end
