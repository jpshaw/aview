# frozen_string_literal: true

require 'rails_helper'

describe DeviceAvailabilityMetrics do
  describe '#run' do
    it 'measures total processing time' do
      expect(Operations::Metrics).to receive(:measure).with('jobs.device.availability.metrics.elapsed')
      subject.run
    end

    context 'up' do
      let!(:device_1) { create(:test_device, :up) }

      let(:args) { {
        mac: device_1.mac,
        organization_id: device_1.organization_id,
        state: 1,
        count: 1
      } }

      it 'writes metrics for up devices' do
        expect(Measurement::DeviceAvailability).to receive(:create).once.with(args)
        subject.run
      end
    end

    context 'down' do
      let(:device_1) { create(:test_device, :down) }
      let(:args) { {
        mac: device_1.mac,
        organization_id: device_1.organization_id,
        state: 0,
        count: 1
      } }

      it 'writes metrics for each down devices' do
        expect(Measurement::DeviceAvailability).to receive(:create).once.with(args)
        subject.run
      end
    end

    context 'timeout' do
      before do
        create(:test_device, :deployed, :up)
        create(:test_device, :deployed, :up)
      end

      it 'only processes current device when timeout has been triggered' do
        expect(Device.count).to eq 2
        expect(Measurement::DeviceAvailability).to receive(:create).once
        subject.timeout = true
        subject.run
      end
    end
  end
end
