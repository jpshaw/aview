# frozen_string_literal: true

require 'rails_helper'

describe FirmwaresSync do
  def log_error_check
    expect(subject).to receive(:log_error)
    subject.run
  end

  def enqueue_firmware_info_check
    expect(subject).not_to receive(:queue)
    subject.run
  end

  describe '#run' do
    it 'gets firmwares info' do
      expect(subject).to receive(:get_firmwares_info)
      expect(subject.class).to receive(:get).with('/list')
      subject.run
    end

    context 'when it fails with an unknown error' do
      before do
        allow(subject).to receive(:get_firmwares_info).and_raise
      end

      it 'logs an error' do
        log_error_check
      end

      it 'does not enqueue firmwares for processing' do
        enqueue_firmware_info_check
      end
    end

    context 'with invalid firmwares info' do
      before do
        allow(subject).to receive(:get_firmwares_info)
        allow(subject).to receive_message_chain(:response, :message)
        allow(subject).to receive_message_chain(:response, :code).and_return(400)
      end

      it 'logs an error' do
        log_error_check
      end

      it 'does not enqueue firmwares for processing' do
        enqueue_firmware_info_check
      end
    end

    context 'with valid firmwares info' do
      let(:series_name)       { '5500-RM' }
      let(:firmware_version)  { '14.10.1' }
      let(:firmwares_info)    do
        { 'firmwares' => {
          series_name => [
            {
              'firmware_version'  => firmware_version,
              'production'        => true,
              'deprecated'        => false
            }
          ]
        } }
      end

      before do
        allow(subject).to receive_message_chain(:response, :code).and_return(200)
        allow(subject).to receive(:get_firmwares_info).and_return(firmwares_info)
      end

      context 'when a device model cannot be found' do
        before do
          allow(DeviceModel).to receive(:find_by_name).and_return(nil)
        end

        it 'logs an error' do
          log_error_check
        end

        it 'does not enqueue firmwares for processing' do
          enqueue_firmware_info_check
        end
      end

      context 'when a device model is found' do
        let(:device_model_id) { 2 }
        let(:device_model)    { build(:test_model, id: device_model_id, name: series_name) }
        let(:queue)           { double('queue') }

        before do
          allow(DeviceModel).to receive(:find_by).and_return(device_model)
          allow(subject).to receive(:queue).and_return(queue)
          allow(queue).to receive(:publish)
        end

        it 'enqueues firmwares info for processing' do
          expect(queue).to receive(:publish)
          subject.run
        end
      end
    end
  end
end
