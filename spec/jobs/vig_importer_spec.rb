# frozen_string_literal: true

require 'rails_helper'

# Here is the test data in test_vig_file.xlsx:
#
# ATAP,     VARBA110,    VIG #1 - BUENOS AIRES,             32.115.8.51
# ATAP,     VARBA210,    VIG #2 - BUENOS AIRES,             32.115.8.52
# ATAP,     VARBA210,    VIG #2 - BUENOS AIRES,             32.115.8.53
# ATAP,     VARBA310,    VIG #2 - LOS ANGELES,              32.115.8.53
# ATAP,     VARBA310,    VIG #2 - LOS ANGELES,              32.115.8.54
# ATAP,     VUSLSP10-V6, VIG #1 - LITHIA SPRINGS, GA IPV6,  21:1890:E0:3400:00:00:00:11

describe VigImporter do
  let(:file_path)   { "#{fixture_path}/files/test_vig_file.xlsx" }
  let(:digest_path) { "#{fixture_path}/files/test_vig_file_digest.txt" }

  before do
    allow(VigSpreadsheet).to receive(:path).and_return(file_path)
    allow(VigSpreadsheet).to receive(:digest_path).and_return(digest_path)
  end

  describe '#run' do
    context 'when the vig file does not exist' do
      before { allow(VigSpreadsheet).to receive(:exists?).and_return(false) }

      it 'does not raise an exception' do
        expect { subject.run }.not_to raise_exception
      end

      it 'does not process the vig file' do
        expect(subject).not_to receive(:process_spreadsheet)
        subject.run
      end
    end

    context 'when the vig file exists' do
      context 'when the file has not changed' do
        before { allow(VigSpreadsheet).to receive(:changed?).and_return(false) }

        it 'does not process the vig file' do
          expect(subject).not_to receive(:process_spreadsheet)
          subject.run
        end
      end

      context 'when the file has changed' do
        before { allow(VigSpreadsheet).to receive(:changed?).and_return(true) }

        after  { File.delete(digest_path) if File.exist?(digest_path) }

        it 'processes the vig file' do
          expect(subject).to receive(:process_spreadsheet)
          subject.run
        end

        # There are 6 entries, 2 of them have the same vig name and location, so
        # 4 tunnel server objects should be created.
        it 'creates a unique list of tunnel servers' do
          expect { subject.run }.to change(TunnelServer, :count).by(4)
        end

        # There are 6 entries with 1 duplicate ip address, so 5 tunnel server
        # ip address objects should be created.
        it 'creates tunnel server ip addresses' do
          expect { subject.run }.to change(TunnelServerIpAddress, :count).by(5)
        end

        # The ip 32.115.8.53 is assigned to vig VARBA210 and VARBA310. It should
        # only associate with the latest, which is VARBA310.
        it 'assigns ip addresses to their most recent tunnel server' do
          subject.run
          ip_address = TunnelServerIpAddress.find_by(ip_address: '32.115.8.53')
          expect(ip_address.tunnel_server.name).to eq 'VARBA310'
        end

        it 'saves the file digest' do
          expect { subject.run }.to change { File.exist?(digest_path) }.from(false).to(true)
        end

        context 'when there are stale objects' do
          before do
            @tunnel_server = create(:tunnel_server)
            @tunnel_server_ip_address = create(:tunnel_server_ip_address, tunnel_server: @tunnel_server)
          end

          it 'removes them' do
            subject.run
            expect(TunnelServer.find_by(id: @tunnel_server.id)).to be nil
            expect(TunnelServerIpAddress.find_by(id: @tunnel_server_ip_address.id)).to be nil
          end
        end
      end
    end
  end
end
