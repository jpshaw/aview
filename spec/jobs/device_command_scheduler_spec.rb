# frozen_string_literal: true

require 'rails_helper'

describe DeviceCommandScheduler do
  subject { described_class }

  let(:job)    { subject.new }
  let(:logger) { job.logger }
  let(:queue)  { double('queue', publish: true) }

  before do
    allow(job).to receive(:queue).and_return(queue)
  end

  describe '#run' do
    context 'when there are device commands to be scheduled' do
      let(:organization) { create(:organization, gateway_vlan_collection_enabled: true) }
      let!(:scheduled_devices) { create_list(:gateway_device_with_configuration, 1, :vlan_commandable, organization: organization) }

      before do
        expect(scheduled_devices.count).to be > 0
      end

      it 'publishes the commands to the queue' do
        expect(queue).to receive(:publish).with kind_of Hash
        job.run
      end

      it 'logs the number of commands scheduled' do
        expect(logger).to receive(:info).with(/#{scheduled_devices.count}/)
        job.run
      end

      context 'when the job times out' do
        before { job.on_timeout }

        it 'does not publish the next command' do
          expect(queue).not_to receive(:publish)
          job.run
        end

        it 'logs the timeout error' do
          expect(logger).to receive(:error).with(/timed out/)
          job.run
        end
      end
    end
  end
end
