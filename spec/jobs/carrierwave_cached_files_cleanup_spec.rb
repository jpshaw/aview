# frozen_string_literal: true

require 'rails_helper'
require 'fileutils'

describe CarrierwaveCachedFilesCleanup do
  describe '#initialize' do
    it 'uses seconds value when passed' do
      seconds = CarrierwaveCachedFilesCleanup::ONE_DAY_IN_SECONDS + 1
      cleaner = described_class.new(seconds: seconds)
      expect(cleaner.instance_variable_get(:@seconds)).to eq(seconds)
    end

    it "defaults seconds to 1 day's worth if value is not passed" do
      cleaner = described_class.new
      expect(cleaner.instance_variable_get(:@seconds)).to eq(CarrierwaveCachedFilesCleanup::ONE_DAY_IN_SECONDS)
    end
  end

  describe '#run' do
    # Using carrierwave's test folder (from one of the uploaders). It will be cleaned up after each test.
    let(:tmp_path)  { ImportFileUploader.new.cache_dir    }
    let(:dir_path)  { File.join(tmp_path, 'test_folder')  }
    let(:file_path) { File.join(dir_path, 'test_file')    }

    before do
      # Create test file.
      FileUtils.mkdir_p(dir_path)
      FileUtils.touch(file_path)
      expect(File.exist?(file_path)).to be(true)

      # Override tmp_path so we don't polute production's folders.
      expect(subject).to receive(:tmp_path).at_least(:once).and_return(tmp_path)
      expect(subject.__send__(:tmp_path)).to eq(tmp_path)
    end

    context 'when a file is old' do
      before do
        time_limit = Time.now + 1.day

        # We make the time limit in the future to force the file to be old.
        expect(subject).to receive(:limit_time).at_least(:once).and_return(time_limit)
        expect(subject.__send__(:limit_time)).to eq(time_limit)
      end

      it 'is deleted' do
        subject.run
        expect(File.exist?(file_path)).to be(false)
      end
    end

    context 'when a file is not old' do
      it 'is not deleted' do
        subject.run
        expect(File.exist?(file_path)).to be(true)
      end
    end

    context 'when a folder does not end up empty' do
      it 'is not deleted' do
        subject.run
        expect(Dir.exist?(dir_path)).to be(true)
      end
    end

    context 'when a folder ends up empty' do
      it 'is deleted' do
        File.delete(file_path)
        expect(File.exist?(file_path)).to be(false)
        subject.run
        expect(Dir.exist?(dir_path)).to be(false)
      end
    end
  end
end
