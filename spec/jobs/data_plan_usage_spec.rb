# frozen_string_literal: true

require 'rails_helper'

describe DataPlanUsage do
  describe '#run' do
    let(:organization) { create(:organization) }
    let(:time) { 2.days.ago }
    let(:start_time) { Time.new(time.year, time.month, time.day) }
    let(:data_plan) do
      create(:data_plan,
        organization: organization,
        data_limit: 1000,
        individual_data_limit: 100,
        cycle_start_day: time.day,
        notify_enabled: true)
    end
    let(:device_1) do
      create(:test_device_with_modem_association,
        organization: organization,
        data_plan: data_plan)
    end

    def usage_results(device, rx: 0, tx: 0, used: 0)
      [
        {
          'version' => 'v1',
          'timestamp' => Time.now.utc.to_s,
          'event' => {
            'mac' => device.mac,
            'received'    => rx,
            'transmitted' => tx,
            'data_used'   => used
          }
        }
      ]
    end

    let(:usage) { 110 }

    before do
      allow_any_instance_of(DataPlan).to receive(:current_usage).and_return(usage_results(device_1, used: usage))
    end

    context 'when a data plan has usage' do
      before { create(:device_modem, device: device_1) }

      it 'saves the data usage to the data plan' do
        expect { subject.run }.to change { data_plan.reload.data_used }.to(usage)
      end

      it 'saves the data usage to the device modem' do
        expect { subject.run }.to change { device_1.modem.reload.data_used }.to(usage)
      end
    end

    context 'when a single device extends the individual allowed usage of the data plan' do
      context 'when the device is not already over the limit' do
        before do
          allow_any_instance_of(DataPlan).to receive(:exceeds_individual_limit_for_first_time?).and_return(true)
        end

        it 'generates a device event' do
          expect { subject.run }.to change { DeviceEvent.count }.by(1)
        end
      end

      context 'when the device is already over the limit' do
        before do
          allow_any_instance_of(DataPlan).to receive(:exceeds_individual_limit_for_first_time?).and_return(false)
        end

        it 'does not generate a device event' do
          expect { subject.run }.to_not change { DeviceEvent.count }
        end
      end
    end

    context 'when a single device does not exceed the individual allowed usage of the data plan' do
      it 'does not generate a device event' do
        expect do
          subject.run
        end.not_to change(DeviceEvent, :count)
      end
    end

    context 'when the data plan has notify enabled' do
      context 'when a single device exceeds the total allowed usage of the data plan' do
        let(:user_1) { create(:user, organization: organization) }
        let(:user_2) { create(:user, organization: organization) }
        let(:user_3) { create(:user, organization: organization, notify_enabled: false) }

        before do
          allow_any_instance_of(DataPlan).to receive(:exceeds_total_limit_for_first_time?).and_return(true)
        end

        it 'sends a notification to all users within the organization that are notifiable' do
          expect(UserMailer).to receive(:data_plan_over_limit).with(user_1, data_plan, usage)
          expect(UserMailer).to receive(:data_plan_over_limit).with(user_2, data_plan, usage)
          expect(UserMailer).not_to receive(:data_plan_over_limit).with(user_3, data_plan, usage)
          subject.run
        end

        context 'when the data plan is already over the limit' do
          before { data_plan.update(data_used: 5_000) }

          it 'does not send another notification' do
            subject.run
            expect(ActionMailer::Base.deliveries.size).to eq 0
          end
        end
      end

      context 'when several devices together exceed the total allowed usage of the data plan' do
        let(:user_1) { create(:user, organization: organization) }
        let(:user_2) { create(:user) }
        let(:user_3) { create(:user, organization: organization, notify_enabled: false) }
        let(:device_2) { create(:test_device_with_modem_association, organization: organization, data_plan: data_plan) }

        before do
          allow_any_instance_of(DataPlan).to receive(:exceeds_total_limit_for_first_time?).and_return(true)
        end

        it 'sends a notification to all users within the organization that are notifiable' do
          expect(UserMailer).to receive(:data_plan_over_limit).with(user_1, data_plan, usage)
          expect(UserMailer).not_to receive(:data_plan_over_limit).with(user_2, data_plan, usage)
          expect(UserMailer).not_to receive(:data_plan_over_limit).with(user_3, data_plan, usage)
          subject.run
        end
      end
    end
  end
end
