# frozen_string_literal: true

require 'rails_helper'

describe UserMailer do
  after do
    ActionMailer::Base.deliveries.clear
  end

  # TODO: Test for out of bounds errors for these functions.

  describe '.subscription_email' do
    it 'does nothing if nil is passed' do
      described_class.subscription_email(nil).deliver_now
      expect(ActionMailer::Base.deliveries.count.zero?).to be true
    end

    it 'creates an email if a valid object is passed' do
      create_immediate_notification_for_device_subscription
      described_class.subscription_email(@notification).deliver_now
      email = ActionMailer::Base.deliveries.first
      expect(email).not_to be nil
      expect(email.to).to eq [@user.email]
    end
  end

  describe '.bulk_subscription_email' do
    it 'does nothing if nil is passed' do
      described_class.bulk_subscription_email(nil, nil).deliver_now
      expect(ActionMailer::Base.deliveries.count.zero?).to be true
    end

    it 'creates an email if valid data is passed' do
      create_subscriptions_for_gateway_device
      subscription = @user.notification_subscriptions.devices.first
      notifications = 5.times.map do |i|
        subscription.notifications.create(user_id: @user.id,
                                          event_id: create_device_event(information: "Test event ##{i}").id)
      end
      described_class.bulk_subscription_email(subscription, notifications).deliver_now
      email = ActionMailer::Base.deliveries.first
      expect(email).not_to be nil
      expect(email.to).to eq [@user.email]
    end
  end

  describe '.data_plan_over_limit' do
    it 'does nothing if nil is passed' do
      described_class.data_plan_over_limit(nil, nil, nil).deliver_now
      expect(ActionMailer::Base.deliveries.count.zero?).to be true
    end

    context 'when given a user and data plan' do
      let(:organization) { create(:organization) }
      let(:data_plan) { create(:data_plan, organization: organization) }
      let(:user) { build(:user) }

      it 'creates an email' do
        described_class.data_plan_over_limit(user, data_plan, 10).deliver_now
        email = ActionMailer::Base.deliveries.first
        expect(email).not_to be nil
        expect(email.to).to eq [user.email]
      end
    end
  end
end
