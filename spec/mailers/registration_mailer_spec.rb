# frozen_string_literal: true

require 'rails_helper'

describe RegistrationMailer do
  after do
    ActionMailer::Base.deliveries.clear
  end

  let(:token) { 'token' }

  describe '.welcome_email' do
    it 'creates an email if a valid user is passed' do
      @user = create(:user)
      described_class.welcome_email(@user, token).deliver_now
      email = ActionMailer::Base.deliveries.first
      expect(email).not_to be nil
      expect(email.to).to eq [@user.email]
    end
  end
end
