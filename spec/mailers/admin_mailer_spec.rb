# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdminMailer, type: :mailer do
  after do
    ActionMailer::Base.deliveries.clear
  end

  context '.new_carrier' do
    it 'creates an email' do
      AdminMailer.new_carrier(build(:carrier)).deliver_now
      email = ActionMailer::Base.deliveries.first
      expect(email).not_to eq nil
      expect(email.to).to eq [Settings.email_addresses.aview]
    end
  end
end
