# frozen_string_literal: true

require 'rspec/expectations'

# Device#inspect displays too much information for descriptions
def basic_description_for(*devices)
  devices.map do |device|
    "#<#{device.class} id: #{device.id}, mac: \"#{device.mac}\">"
  end.join(', ')
end

# Checks if a device is associated to all given devices.
#
# Example
#
#   expect(device).to be_associated_with_devices(other_device1, other_device2)
#
RSpec::Matchers.define :be_associated_with_devices do |*other_devices|
  match do |device|
    other_devices.all? do |other_device|
      ::DeviceAssociation.by_source_id_and_target_id(device.id, other_device.id).exists?
    end
  end

  description do
    "associated with #{basic_description_for *other_devices}"
  end

  failure_message do |device|
    "expected #{basic_description_for device} to " \
      "be associated with #{basic_description_for *other_devices}"
  end

  failure_message_when_negated do |device|
    "expected that #{basic_description_for device} would not " \
      "be associated with #{basic_description_for *other_devices}"
  end
end

# Checks if a device is associated with another device.
#
# Example
#
#   expect(device).to be_associated_with_device(other_device)
#
RSpec::Matchers.alias_matcher :be_associated_with_device, :be_associated_with_devices

# Checks if a device is associated to all given macs.
#
# Example
#
#   expect(device).to be_associated_with_macs(mac1, mac2, mac3)
#
RSpec::Matchers.define :be_associated_with_macs do |*macs|
  match do |device|
    macs.all? do |mac|
      ::DeviceAssociation.by_source_id_and_target_mac(device.id, mac).exists?
    end
  end

  description do
    "associated with #{macs.join(', ')}"
  end

  failure_message do |device|
    "expected #{basic_description_for device} to be associated with #{macs.join(', ')}"
  end

  failure_message_when_negated do |device|
    "expected that #{basic_description_for device} would not " \
      "be associated with #{macs.join(', ')}"
  end
end

# Checks if a device is associated with a mac address.
#
# Example
#
#   expect(device).to be_associated_with_mac(mac)
#
RSpec::Matchers.alias_matcher :be_associated_with_mac, :be_associated_with_macs

# Checks if a block of code creates an association for the device to all given devices.
#
# Example
#
#   expect {
#     device.associate_with_device(other_device)
#   }.to associate_devices(device, other_device)
#
#
#   expect {
#     device.associate_with_device(other_device1)
#     device.associate_with_device(other_device2)
#   }.to associate_devices(device, other_device1, other_device2)
#
RSpec::Matchers.define :associate_devices do |device, *other_devices|
  supports_block_expectations

  match do |block|
    expect(device).not_to be_associated_with_devices(*other_devices)
    block.call
    expect(device).to be_associated_with_devices(*other_devices)
  end

  description do
    "associate #{description_for device} " \
      "with #{description_for *other_devices}"
  end

  failure_message do
    "expected #{basic_description_for device} to " \
      "associate with #{basic_description_for *other_devices}"
  end

  failure_message_when_negated do
    "expected that #{basic_description_for device} would not " \
      "associate with #{basic_description_for *other_devices}"
  end
end

# Checks if a block of code removes a device's associations to all given devices.
#
# Example
#
#   expect {
#     device.dissociate_from_device(other_device)
#   }.to dissociate_devices(device, other_device)
#
#   expect {
#     device.dissociate_from_device(other_device1)
#     device.dissociate_from_device(other_device2)
#   }.to dissociate_devices(device, other_device1, other_device2)
#
RSpec::Matchers.define :dissociate_devices do |device, *other_devices|
  supports_block_expectations

  match do |block|
    expect(device).to be_associated_with_devices(*other_devices)
    block.call
    expect(device).not_to be_associated_with_devices(*other_devices)
  end

  description do
    "dissociate #{description_for device} " \
      "from #{description_for *other_devices}"
  end

  failure_message do
    "expected #{basic_description_for device} to " \
      "dissociate from #{basic_description_for *other_devices}"
  end

  failure_message_when_negated do
    "expected that #{basic_description_for device} would not " \
      "dissociate from #{basic_description_for *other_devices}"
  end
end

# Checks if a block of code creates associations for the device to all given macs.
#
# Example
#
#   expect {
#     device.associate_with_mac(mac1)
#     device.associate_with_mac(mac2)
#   }.to associate_device_to_mac(device, mac1, mac2)
#
RSpec::Matchers.define :associate_device_to_macs do |device, *macs|
  supports_block_expectations

  match do |block|
    expect(device).not_to be_associated_with_macs(*macs)
    block.call
    expect(device).to be_associated_with_macs(*macs)
  end

  description do
    "associate #{description_for device} with #{macs.join(', ')}"
  end

  failure_message do
    "expected #{basic_description_for device} to " \
      "associate with #{macs.join(', ')}"
  end

  failure_message_when_negated do
    "expected that #{basic_description_for device} would not " \
      "associate with #{macs.join(', ')}"
  end
end

# Checks if a block of code creates an association for the given device and mac.
#
# Example
#
#   expect {
#     device.associate_with_mac(mac)
#   }.to associate_device_to_mac(device, mac)
#
RSpec::Matchers.alias_matcher :associate_device_to_mac, :associate_device_to_macs

# Checks if a block of code removes a device's associations to all given macs.
#
# Example
#
#   expect {
#     device.dissociate_from_mac(mac1)
#     device.dissociate_from_mac(mac2)
#   }.to dissociate_device_from_macs(device, mac1, mac2)
#
RSpec::Matchers.define :dissociate_device_from_macs do |device, *macs|
  supports_block_expectations

  match do |block|
    expect(device).to be_associated_with_macs(*macs)
    block.call
    expect(device).not_to be_associated_with_macs(*macs)
  end

  description do
    "dissociate #{description_for device} from #{macs.join(', ')}"
  end

  failure_message do
    "expected #{basic_description_for device} to dissociate from #{macs.join(', ')}"
  end

  failure_message_when_negated do
    "expected that #{basic_description_for device} would not dissociate from #{macs.join(', ')}"
  end
end

# Checks if a block of code removes an association for the given device and mac.
#
# Example
#
#   expect {
#     device.dissociate_from_mac(mac)
#   }.to dissociate_device_from_mac(device, mac)
#
RSpec::Matchers.alias_matcher :dissociate_device_from_mac, :dissociate_device_from_macs
