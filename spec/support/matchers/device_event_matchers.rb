# frozen_string_literal: true

require 'rspec/expectations'

RSpec::Matchers.define :have_event_info do |expected|
  match do |device|
    device.events.pluck(:information).join(' ').include?(expected)
  end
end
