# frozen_string_literal: true

require 'rspec/expectations'

RSpec::Matchers.define :be_on do |_expected|
  match do |actual|
    actual.on? == true
  end
end

RSpec::Matchers.define :be_off do |_expected|
  match do |actual|
    actual.off? == true
  end
end
