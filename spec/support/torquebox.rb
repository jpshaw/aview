module TorqueBox
  def self.fetch(*args)
    DummyResource.new
  end

  module Injectors
    def fetch(*args)
      DummyResource.new
    end
  end

  class DummyResource
    def publish(*args)
      # no-op
    end
    def receive(*args)
      # no-op
    end
  end

  module Messaging
    module Backgroundable
      def self.included(base)
        base.extend(BackgroundableClassMethods)
      end

      def background(options = {})
        self
      end

      module BackgroundableClassMethods
        def always_background(*methods)
          # no-op
        end

        def background(options = {})
          self
        end
      end
    end

    class MessageProcessor
      include ::Operations::Metrics::Helpers
    end

    class Queue
      def initialize(*args)
        # no-op
      end

      def publish(*args)
        # no-op
      end

      def receive(*args)
        # no-op
      end
    end

    class Message
      class << self
        def register_encoding(klass)
          # no-op
        end
      end
    end
  end
end
