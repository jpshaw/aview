# frozen_string_literal: true

shared_examples_for 'an inheritable configuration boolean' do |field|
  let(:default) { false }
  let(:result)  { subject.send("#{field}?") }

  context 'when no parent is set' do
    before { expect(subject.parent).to be_blank }

    context 'when the child field is set to true' do
      before { subject.update(field => true) }

      it 'returns true' do
        expect(result).to be true
      end
    end

    context 'when the child field is set to false' do
      before { subject.update(field => false) }

      it 'returns false' do
        expect(result).to be false
      end
    end

    context 'when the child field is not set' do
      before { subject.update(field => nil) }

      it 'returns the default' do
        expect(result).to be default
      end
    end
  end

  context 'when a parent is set' do
    let(:parent) { create(subject.class.to_s.underscore) }

    before do
      subject.parent = parent
      subject.save!
    end

    context 'when the child field is set to true' do
      before { subject.update(field => true) }

      context 'when the parent field is set to true' do
        before { parent.update(field => true) }

        it 'returns true' do
          expect(result).to be true
        end
      end

      context 'when the parent field is set to false' do
        before { parent.update(field => false) }

        it 'returns true' do
          expect(result).to be true
        end
      end
    end

    context 'when the child field is set to false' do
      before { subject.update(field => false) }

      context 'when the parent field is set to true' do
        before { parent.update(field => true) }

        it 'returns false' do
          expect(result).to be false
        end
      end

      context 'when the parent field is set to false' do
        before { parent.update(field => false) }

        it 'returns false' do
          expect(result).to be false
        end
      end
    end

    context 'when the child field is not set' do
      before { subject.update(field => nil) }

      context 'when the parent field is set to true' do
        before { parent.update(field => true) }

        it 'returns true' do
          expect(result).to be true
        end
      end

      context 'when the parent field is set to false' do
        before { parent.update(field => false) }

        it 'returns false' do
          expect(result).to be false
        end
      end

      context 'when the parent field is not set' do
        before { parent.update(field => nil) }

        it 'returns the default' do
          expect(result).to be default
        end
      end
    end
  end
end
