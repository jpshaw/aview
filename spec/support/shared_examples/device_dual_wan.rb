# frozen_string_literal: true

shared_examples_for 'a device with dual wan capabilities' do
  let(:device)   { create(:gateway_device, :firmware_6_2, :with_details) }
  let(:dual_wan) { double('dual wan') }

  before { allow(device).to receive(:dual_wan).and_return(dual_wan) }

  describe 'dual_wan_blank?' do
    it 'returns true when wan states are blank' do
      allow(dual_wan).to receive(:blank?).and_return(true)
      expect(device.dual_wan_blank?).to be true
    end
  end

  describe 'dual_wan_both_up?' do
    it 'returns true when both wans are up' do
      allow(dual_wan).to receive(:both_up?).and_return(true)
      expect(device.dual_wan_both_up?).to be true
    end
  end

  describe 'dual_wan_both_down?' do
    it 'returns true when both wans are down' do
      allow(dual_wan).to receive(:both_down?).and_return(true)
      expect(device.dual_wan_both_down?).to be true
    end
  end

  describe 'dual_wan_wan1_up_and_wan2_down?' do
    it 'returns true when wan1 is up and wan2 is down' do
      allow(dual_wan).to receive(:wan1_up_and_wan2_down?).and_return(true)
      expect(device.dual_wan_wan1_up_and_wan2_down?).to be true
    end
  end

  describe 'dual_wan_wan1_down_and_wan2_up?' do
    it 'returns true when wan1 is up and wan2 is down' do
      allow(dual_wan).to receive(:wan1_down_and_wan2_up?).and_return(true)
      expect(device.dual_wan_wan1_down_and_wan2_up?).to be true
    end
  end
end
