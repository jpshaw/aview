# frozen_string_literal: true

shared_examples_for 'a device modifier' do
  it 'grants access if user has ability to modify the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Modify Devices').and_return(true)
    expect(subject).to permit(user, device)
  end

  it 'denies access if user does not have ability to modify the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Modify Devices').and_return(false)
    expect(subject).not_to permit(user, device)
  end
end

shared_examples_for 'a device deployer' do
  it 'grants access if user has ability to deploy the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Deploy Devices').and_return(true)
    expect(subject).to permit(user, device)
  end

  it 'denies access if user does not have ability to deploy the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Deploy Devices').and_return(false)
    expect(subject).not_to permit(user, device)
  end
end

shared_examples_for 'a device commander' do
  it 'grants access if user has ability to deploy the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Send Device Commands').and_return(true)
    expect(subject).to permit(user, device)
  end

  it 'denies access if user does not have ability to deploy the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Send Device Commands').and_return(false)
    expect(subject).not_to permit(user, device)
  end
end

shared_examples_for 'a device replacer' do
  it 'grants access if user has ability to replace the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Replace Devices').and_return(true)
    expect(subject).to permit(user, device)
  end

  it 'denies access if user does not have ability to replace the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Replace Devices').and_return(false)
    expect(subject).not_to permit(user, device)
  end
end

shared_examples_for 'a device importer' do
  it 'grants access if user has ability to import the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Import Devices').and_return(true)
    expect(subject).to permit(user, device)
  end

  it 'denies access if user does not have ability to import the device' do
    allow(user).to receive(:has_ability_on?).with(device.organization, to: 'Import Devices').and_return(false)
    expect(subject).not_to permit(user, device)
  end
end
