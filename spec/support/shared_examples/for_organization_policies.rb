# frozen_string_literal: true

shared_examples_for 'an organization modifier' do
  before do
    allow(organization).to receive(:persisted?).and_return(true)
  end

  it 'grants access if user has ability to modify the organization' do
    allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Organizations').and_return(true)
    expect(subject).to permit(user, organization)
  end

  it 'denies access if user does not have ability to modify the organization' do
    allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Organizations').and_return(false)
    expect(subject).not_to permit(user, organization)
  end
end

shared_examples_for 'an organization destroyer' do
  before do
    @organization = create(:organization)
    @user = create(:user, organization: @organization)
  end

  context 'when the user has the ability to modify the organization' do
    context 'when the organization is root' do
      before do
        allow(@user).to receive(:has_ability_on?)
          .with(@organization, to: 'Modify Organizations')
          .and_return(true)
      end

      context 'when the user belongs to the organization' do
        it 'denies access' do
          expect(subject.new(@user, @organization).modify?).to be true
          expect(@user.organization).to eq @organization
          expect(@organization.root?).to be true
          expect(subject).not_to permit(@user, @organization)
        end
      end

      context 'when the user does not belong to the organization' do
        before do
          @user.organization = create(:sub_organization)
        end

        it 'denies access' do
          expect(subject.new(@user, @organization).modify?).to be true
          expect(@user.organization).not_to eq @organization
          expect(@organization.root?).to be true
          expect(subject).not_to permit(@user, @organization)
        end
      end
    end

    context 'when the organization is not root' do
      before do
        @organization = create(:sub_organization)
        allow(@user).to receive(:has_ability_on?)
          .with(@organization, to: 'Modify Organizations')
          .and_return(true)
      end

      context 'when the user belongs to the organization' do
        before do
          @user.organization = @organization
        end

        it 'denies access' do
          expect(subject.new(@user, @organization).modify?).to be true
          expect(@user.organization).to eq @organization
          expect(@organization.root?).to be false
          expect(subject).not_to permit(@user, @organization)
        end
      end

      context 'when the user does not belong to the organization' do
        before do
          @user.organization = create(:sub_organization)
        end

        it 'grants access' do
          expect(subject.new(@user, @organization).modify?).to be true
          expect(@user.organization).not_to eq @organization
          expect(@organization.root?).to be false
          expect(subject).to permit(@user, @organization)
        end
      end
    end
  end

  context 'when the user does not have the ability to modify the organization' do
    before do
      allow(@user).to receive(:has_ability_on?)
        .with(@organization, to: 'Modify Organizations')
        .and_return(false)
    end

    it 'denies access' do
      expect(subject).not_to permit(@user, @organization)
    end
  end
end

shared_examples_for 'an organization user modifier' do
  let(:ability) { 'Modify Users' }
  # Need a persisted organization
  let(:organization) { create(:organization) }

  context 'when creating users is enabled' do
    before do
      settings = { users: { enabled: true } }
      Settings.users = {}
      Settings.merge!(settings)
    end

    after do
      Settings.reload!
    end

    context 'when the user has the ability for an organization' do
      before do
        allow(user).to receive(:has_ability_on?)
          .with(organization, to: ability)
          .and_return(true)
      end

      it 'grants access' do
        expect(Settings.users.enabled).to be true
        expect(subject).to permit(user, organization)
      end
    end

    context 'when the user does not have the ability for an organization' do
      before do
        allow(user).to receive(:has_ability_on?)
          .with(organization, to: ability)
          .and_return(false)
      end

      it 'denies access' do
        expect(Settings.users.enabled).to be true
        expect(subject).not_to permit(user, organization)
      end
    end

    context 'when the user has the ability for any organization' do
      before do
        allow(user).to receive(:manageables).with(of_type: Organization.to_s,
                                                  for_ability: ability)
                                            .and_return([organization])
      end

      it 'grants access' do
        expect(Settings.users.enabled).to be true
        expect(subject).to permit(user, Organization)
      end
    end

    context 'when the user does not have the ability for any organization' do
      before do
        allow(user).to receive(:manageables).with(of_type: Organization.to_s,
                                                  for_ability: ability)
                                            .and_return([])
      end

      it 'denies access' do
        expect(Settings.users.enabled).to be true
        expect(subject).not_to permit(user, Organization)
      end
    end
  end

  context 'when creating users is disabled' do
    before do
      settings = { users: { enabled: false } }
      Settings.users = {}
      Settings.merge!(settings)
    end

    after do
      Settings.reload!
    end

    context 'when given an organization object' do
      it 'denies access' do
        expect(Settings.users.enabled).to be false
        expect(subject).not_to permit(user, @organization)
      end
    end

    context 'when given the organization class' do
      it 'denies access' do
        expect(Settings.users.enabled).to be false
        expect(subject).not_to permit(user, Organization)
      end
    end
  end
end

shared_examples_for 'an organization device modifier' do
  context 'when the user has the ability to modify devices for an organization' do
    before do
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Devices').and_return(true)
    end

    it 'grants access' do
      expect(subject).to permit(user, organization)
    end
  end

  context 'when the user does not have the ability to modify devices for an organization' do
    before do
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Devices').and_return(false)
    end

    it 'denies access' do
      expect(subject).not_to permit(user, organization)
    end
  end
end
