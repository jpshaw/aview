# frozen_string_literal: true

shared_examples_for 'an action that gets an organization from params' do |verb, action|
  it 'redirects to the organization index path if params is empty' do
    params = { id: 'test' }
    send verb, action, params
    expect(response).to redirect_to organizations_path
  end
end
