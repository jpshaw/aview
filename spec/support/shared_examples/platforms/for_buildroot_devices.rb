# frozen_string_literal: true

shared_examples_for 'a buildroot device' do
  include_examples 'acts as platform'
  include_examples 'buildroot configuration events'
  include_examples 'buildroot firmware events'
  include_examples 'buildroot modem status events'
  include_examples 'buildroot network events'
  include_examples 'buildroot power events'
  include_examples 'buildroot reboot events'
  include_examples 'buildroot remote events'
  include_examples 'buildroot tunnel events'
  include_examples 'buildroot usb events'
  include_examples 'buildroot commands'
end
