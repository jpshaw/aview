# frozen_string_literal: true

shared_examples_for 'a uclinux device' do
  include_examples 'acts as platform'
  include_examples 'uclinux configuration events'
  include_examples 'uclinux dhcpserver events'
  include_examples 'uclinux firmware events'
  include_examples 'uclinux location events'
  include_examples 'uclinux network events'
  include_examples 'uclinux port signal events'
  include_examples 'uclinux reboot events'
  include_examples 'uclinux status events'
  include_examples 'uclinux test events'
  include_examples 'uclinux user events'
  include_examples 'uclinux commands'
end
