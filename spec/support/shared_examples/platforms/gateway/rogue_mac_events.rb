# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway rogue mac events' do
  describe Platforms::Gateway::DialEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#rogue_mac_detected' do
      let(:type)   { :ngevRogueMacDetected }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevRogueMac: '00D0CFABCDEF',
          ngevRogueIp: '108.66.14.112',
          ngevRogueInterface: 'eth2',
          ngevTrapDescription: 'This is a test',
          ngevIPv6RogueIp: '2001:470:26:307:3b1b:056d:b749:600e'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating a rogue mac was detected' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('Rogue MAC address detected')
      end
    end

    describe '#rogue_mac_no_file' do
      let(:type)   { :ngevRogueMacNoFile }
      let(:params) do
        {
          mac: device.mac,
          type: type
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating the rogue mac file was not found' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('file needs to be provided')
      end
    end
  end
end
