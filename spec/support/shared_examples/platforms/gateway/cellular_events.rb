# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway cellular events' do
  describe Platforms::Gateway::CellularEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#cell_modem_required' do
      let(:params) do
        {
          mac:               device.mac,
          type:              :ngevCellModemRequired,
          ng3gModel:         'Shockwave',
          ng3gManufacturer:  'Sierra Wireless',
          ng3gID:            '1231414115',
          ng3gRevision:      '1.2.3',
          ng3gSIMCardNumber: '123456',
          ng3gPhoneNum:      '8131234567',
          ngevCardNotFoundTime: DateTime.now
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating the cell modem is missing' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('Cellular modem has been removed')
      end
    end
  end
end
