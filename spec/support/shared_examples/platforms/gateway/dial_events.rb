# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway dial events' do
  describe Platforms::Gateway::DialEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#dial_test_result' do
      context 'when the device is not given' do
        let(:params) do
          {
            type: :ngevDialAutotestFailure,
            ngevDialTestFailReason: 'success',
            ngevDialTestFailCategory:  Gateway::DIAL_PROBLEMS.keys.sample,
            ngdiLastTestResult: Gateway::DIAL_TEST_RESULT.keys.sample,
            evTestSrc: Gateway::DIAL_TEST_SOURCE.keys.sample
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'does not raise an exception' do
          expect do
            subject.run(nil, event)
          end.not_to raise_error
        end
      end

      context 'when the test result is given' do
        let(:params) do
          {
            mac: device.mac,
            type: :ngevDialAutotestFailure,
            ngevDialTestFailReason: 'success',
            ngevDialTestFailCategory:  Gateway::DIAL_PROBLEMS.keys.sample,
            ngdiLastTestResult: Gateway::DIAL_TEST_RESULT.keys.sample,
            evTestSrc: Gateway::DIAL_TEST_SOURCE.keys.sample
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end

        it 'indicates the test result' do
          subject.run(device, event)
          expect(device.events.last.info).to include("Backup WAN test result: #{Gateway::DIAL_TEST_RESULT[params[:ngdiLastTestResult]]}")
        end
      end

      context 'when the test result is not given' do
        let(:params) do
          {
            mac: device.mac,
            type: :ngevDialAutotestFailure,
            ngevDialTestFailReason: 'Reason the test failed',
            ngevDialTestFailCategory:  Gateway::DIAL_PROBLEMS.keys.sample
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'creates an event indicating the test failed' do
          subject.run(device, event)
          expect(device.events.last.info).to include('Backup WAN test result: Failure')
        end
      end

      context 'when the test source is not given' do
        let(:params) do
          {
            mac: device.mac,
            type: :ngevDialAutotestFailure,
            ngevDialTestFailReason: 'Reason the test failed',
            ngevDialTestFailCategory:  Gateway::DIAL_PROBLEMS.keys.sample,
            ngdiLastTestResult: Gateway::DIAL_TEST_RESULT.keys.sample
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'creates an event indicating the test result without a source' do
          subject.run(device, event)
          expect(device.events.last.info).not_to include('Source')
        end
      end
    end

    describe '#excessive_dial' do
      let(:params) do
        {
          mac: device.mac,
          type: :ngevExcessiveDial,
          ngevTimeInDial: 1800,
          ngifTimeInDial: '2:10:30.40',
          ngevMaxTimeInDialUntilAlert: '01:00:00.00',
          ngifActiveWanIface: 'eth1'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating backup mode has reached the threshold' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('in backup mode longer than the allowed threshold')
      end
    end
  end
end
