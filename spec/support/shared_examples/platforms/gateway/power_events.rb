# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway power events' do
  describe Platforms::Gateway::PowerEvents do
    subject { Platforms::Gateway::InformProcessor }

    let(:blank_ipv4) { '0.0.0.0' }
    let(:blank_ipv6) { '0' }

    describe '#reboot' do
      let(:type)   { :ngevDeviceReboot }
      let(:params) {
        {
          mac: device.mac,
          type: type,
          ngevRequestorIp: blank_ipv4,
          ngevIPv6RequestorIp: blank_ipv6,
          sysUpTimeInstance: '25 days, 01:23:45.00',
          evRebootReason: 4
        }
      }
      let(:event) { Platforms::Event.new(params) }

      it 'updates last_restarted_at to time of received power event' do
        expect {
          subject.run(device, event)
        }.to change { device.last_restarted_at }
        expect(device.last_restarted_at.strftime "%Y-%m-%d").to_not eq(25.days.ago.strftime "%Y-%m-%d")
      end

      it 'generates an event indicating the device is rebooting' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('Device is rebooting')
      end

      it 'generates an event indicating reason for the reboot' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('MTU or MSS Change')
      end

      it 'updates the last restart timestamp' do
        Timecop.freeze do
          time = Time.now.utc
          expect do
            subject.run(device, event)
          end.to change(device, :last_restarted_at).to(time)
        end
      end

      context 'when requested by an ipv4 address' do
        let(:host) { '172.16.3.56' }

        before do
          params[:ngevRequestorIp] = host
        end

        it 'generates an event with the ipv4 address' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include(host)
        end
      end

      context 'when requested by an ipv6 address' do
        let(:host) { '2001:470:26:307:3b1b:056d:b749:600e' }

        before do
          params[:ngevIPv6RequestorIp] = host
        end

        it 'generates an event with the ipv6 address' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include(host)
        end
      end

      context 'when the device has tunnels' do
        before do
          device.tunnels.create!
        end

        it 'removes them' do
          expect do
            subject.run(device, event)
          end.to change { device.tunnels.count }.from(1).to(0)
        end
      end
    end

    describe '#shutdown' do
      let(:type)   { :ngevDeviceShutdown }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevRequestorIp: blank_ipv4,
          ngevIPv6RequestorIp: blank_ipv6
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'generates an event indicating the device is shutting down' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('Device shutdown')
      end

      context 'when requested by an ipv4 address' do
        let(:host) { '172.16.3.56' }

        before do
          params[:ngevRequestorIp] = host
        end

        it 'generates an event with the ipv4 address' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include(host)
        end
      end

      context 'when requested by an ipv6 address' do
        let(:host) { '2001:470:26:307:3b1b:056d:b749:600e' }

        before do
          params[:ngevIPv6RequestorIp] = host
        end

        it 'generates an event with the ipv6 address' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include(host)
        end
      end

      context 'when the device has tunnels' do
        before do
          device.tunnels.create!
        end

        it 'removes them' do
          expect do
            subject.run(device, event)
          end.to change { device.tunnels.count }.from(1).to(0)
        end
      end
    end
  end
end
