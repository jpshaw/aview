# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway user events' do
  describe Platforms::Gateway::UserEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#ssh_login' do
      let(:type)   { :ngevSshLogin }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevSshSourceIp: '0.0.0.0',
          ngevIPv6RequestorIp: '0'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating an ssh attempt occurred' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('SSH login detected')
      end

      context 'when given a valid ipv4 address' do
        let(:host) { '108.26.64.112' }

        before do
          params[:ngevSshSourceIp] = host
        end

        it 'includes the source ip in the event' do
          subject.run(device, event)
          expect(device.events.first.info).to include(host)
        end
      end

      context 'when given a valid ipv6 address' do
        let(:host) { '2001:470:26:307:3b1b:056d:b749:600e' }

        before do
          params[:ngevIPv6RequestorIp] = host
        end

        it 'includes the source ip in the event' do
          subject.run(device, event)
          expect(device.events.first.info).to include(host)
        end
      end
    end

    describe '#secondary_auth_attempt' do
      let(:type)   { :ngevSecondaryAuthAttempt }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevLoginResult: '0',
          ngevRequestorIp: '108.24.16.112',
          ngevAccount: 'ATAP',
          ngevUserid: 'ATTDEMO',
          ngevNewLocalID: 'root',
          ngevIPv6RequestorIp: '0'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating an ssh login attempt occurred' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('SSH login attempt detected')
      end
    end

    describe '#ssh_user_switch' do
      let(:type)   { :ngevSwitchUseridAttempt }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevRequestorIp: '108.24.16.112',
          ngevAccount: 'ATAP',
          ngevUserid: 'ATTDEMO',
          ngevLoginResult: '1',
          ngevNewLocalID: 'root',
          ngevOldLocalID: 'test',
          ngevIPv6RequestorIp: '0'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating a change user attempt occured' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('SSH user switch detected')
      end
    end
  end
end
