# frozen_string_literal: true

require 'rails_helper'

# rspec './spec/models/gateway_spec.rb[1:4:7]'
shared_examples_for 'gateway network events' do
  describe Platforms::Gateway::NetworkEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#dhcp_pool_exhausted' do
      let(:params) do
        {
          mac: device.mac,
          type: :ngevDhcpPoolExhausted,
          sysUpTimeInstance: '25 days, 01:23:45.00',
          ngvlId: 1
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'updates the last_restarted_at timestamp based on system uptime' do
        device.update_attribute(:last_restarted_at, 60.days.ago)
        subject.run(device, event)
        expect(device.last_restarted_at.strftime "%Y-%m-%d").to eq(25.days.ago.strftime "%Y-%m-%d")
      end

      it 'creates an event indicating the dhcp pool is exhausted' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('DHCP pool exhausted')
      end
    end

    describe '#firewall_full' do
      let(:params) do
        {
          mac: device.mac,
          type: :ngevIpConntrackTableFull
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating the firewall connection table is full' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('Firewall connections table is full')
      end
    end

    describe '#ip_conn_track_table' do
      let(:params) do
        {
          mac: device.mac,
          type: :ngevIpConntrackTable,
          ngevIpConntrackCurrentPercentage: 95,
          ngevIpConntrackMaxPercentage: 80
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating the firewall connection threshold has been exceeded' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('connections threshold exceeded')
      end
    end

    describe '#vrrp_state_change' do
      let(:params) do
        {
          mac: device.mac,
          type: :evVrrpStateChange,
          ngvlId: 1,
          ngvrId: 4091,
          ngvrMasterStatus: 1
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating the vrrp state has changed' do
        expect { subject.run(device, event) }.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('VRRP state changed')
        expect(device.vrrp_state).to eq('Master')
      end
    end

    describe '#wan_ip_changed' do
      let(:type)       { :ngevActiveWanIpChanged }
      let(:interface)  { 'eth1' }
      let(:blank_ipv4) { '0.0.0.0' }
      let(:blank_ipv6) { '0' } # Set to zero if not used, specified by AT&T

      before do
        device.create_details(active_wan_iface: interface)
      end

      context 'when given an ipv4 wan address' do
        let(:host)   { '108.36.112.14' }
        let(:params) do
          {
            mac: device.mac,
            type: type,
            ngifActiveWanIface: interface,
            ngevActiveWanIp: host,
            ngevIPv6ActiveWanIp: blank_ipv6
          }
        end
        let(:event) { Platforms::Event.new(params) }

        context 'when the host changes' do
          let(:previous_host) { '127.0.0.1' }

          before do
            device.update_attribute(:host, previous_host)
          end

          it 'updates the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(previous_host).to(host)
          end

          it 'creates an event indicating the host changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }.by(1)
            expect(device.events.first.info).to include('Primary connection IP changed')
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, host)
          end

          it 'does not change the device host' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :host)
          end

          it 'does not create any events' do
            expect do
              subject.run(device, event)
            end.not_to change { device.events.count }
          end
        end
      end

      context 'when given an ipv6 wan address' do
        let(:host)   { '2001:470:26:307:3b1b:056d:b749:600e' }
        let(:params) do
          {
            mac: device.mac,
            type: type,
            ngifActiveWanIface: interface,
            ngevActiveWanIp: blank_ipv4,
            ngevIPv6ActiveWanIp: host
          }
        end
        let(:event) { Platforms::Event.new(params) }

        context 'when the host changes' do
          let(:previous_host) { '2280:470:26:307:6e1d:a2c9:1bfc:701d' }

          before do
            device.update_attribute(:host, previous_host)
          end

          it 'updates the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(previous_host).to(host)
          end

          it 'creates an event indicating the host changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }.by(1)
            expect(device.events.first.info).to include('Primary connection IP changed')
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, host)
          end

          it 'does not change the device host' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :host)
          end

          it 'does not create any events' do
            expect do
              subject.run(device, event)
            end.not_to change { device.events.count }
          end
        end
      end

      context 'when not given a valid wan address' do
        let(:params) do
          {
            mac: device.mac,
            type: type,
            ngifActiveWanIface: interface,
            ngevActiveWanIp: blank_ipv4,
            ngevIPv6ActiveWanIp: blank_ipv6
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'does not change the device host' do
          expect do
            subject.run(device, event)
          end.not_to change(device, :host)
        end

        it 'does not create any events' do
          expect do
            subject.run(device, event)
          end.not_to change { device.events.count }
        end
      end

      context 'when the wan interface changes' do
        let(:new_interface) { 'eth2' }
        let(:params) do
          {
            mac: device.mac,
            type: type,
            ngifActiveWanIface: new_interface,
            ngevActiveWanIp: blank_ipv4,
            ngevIPv6ActiveWanIp: blank_ipv6
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'updates the device wan interface' do
          expect do
            subject.run(device, event)
          end.to change { device.details.active_wan_iface }.from(interface).to(new_interface)
        end

        it 'creates an event indicating the wan interface changed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device).to have_event_info('Primary connection interface changed ')
        end
      end

      context 'when the wan interface does not change' do
        let(:params) do
          {
            mac: device.mac,
            type: type,
            ngifActiveWanIface: interface,
            ngevActiveWanIp: blank_ipv4,
            ngevIPv6ActiveWanIp: blank_ipv6
          }
        end
        let(:event) { Platforms::Event.new(params) }

        it 'does not change the device wan interface' do
          expect do
            subject.run(device, event)
          end.not_to change { device.details.active_wan_iface }
        end

        it 'does not create any events' do
          expect do
            subject.run(device, event)
          end.not_to change { device.events.count }
        end
      end
    end

    describe '#wan2_connection_error' do
      let(:params) do
        {
          mac: device.mac,
          type: :evDualWanIfaceProblem
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'creates an event indicating there was an error bringing up a WAN connection' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('there was a problem')
      end
    end
  end
end
