# frozen_string_literal: true

require 'rails_helper'
shared_examples_for 'gateway utilization events' do
  describe Platforms::Gateway::UtilizationEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#wan_utilization_report' do
      let(:type)   { :evDataUsageReport }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          evTrapCreatedTime: 1_461_769_776_000_000,
          ngifActiveWanIface: mib_interface,
          evDataUpBandw: '100',
          evDataDownBandw: '5000',
          evDataUp1: '35',
          evDataUp2: '29',
          evDataUp3: '50',
          evDataUp4: '41',
          evDataUp5: '89',
          evDataUp6: '62',
          evDataUp7: '32',
          evDataUp8: '45',
          evDataUp9: '63',
          evDataUp10: '71',
          evDataUp11: '35',
          evDataUp12: '43',
          evDataDown1: '3500',
          evDataDown2: '2901',
          evDataDown3: '5023',
          evDataDown4: '4145',
          evDataDown5: '8956',
          evDataDown6: '6267',
          evDataDown7: '3278',
          evDataDown8: '4589',
          evDataDown9: '6301',
          evDataDown10: '7112',
          evDataDown11: '3523',
          evDataDown12: '4334'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      before do
        device.build_details
        allow_any_instance_of(Platforms::Gateway::WanUtilizationReport).to receive(:save_metrics)
      end

      context "when interface is not 'lo'" do
        before { device.details.active_wan_iface = device_interface }

        let(:device_interface) { 'eth2' }
        let(:mib_interface) { 'eth1' }

        it 'saves the wan utilization metrics' do
          expect_any_instance_of(Platforms::Gateway::WanUtilizationReport).to receive(:save_metrics)
          subject.run(device, event)
        end

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('WAN Utilization Report')
        end
      end

      context "when interface is 'lo'" do
        let(:mib_interface) { 'lo' }

        it 'does not saves the wan utilization metrics' do
          expect_any_instance_of(Platforms::Gateway::WanUtilizationReport).not_to receive(:save_metrics)
          subject.run(device, event)
        end

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('WAN Utilization Report')
        end
      end

      context 'when mib interface is nil but device interface is present' do
        before { device.details.active_wan_iface = device_interface }

        let(:mib_interface) {}
        let(:device_interface) { 'eth2' }

        it 'saves the wan utilization metrics using the device interface' do
          expect_any_instance_of(Platforms::Gateway::WanUtilizationReport).to receive(:save_metrics)
          subject.run(device, event)
        end

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('WAN Utilization Report')
        end
      end

      context 'when mib interface and device interface are nil' do
        before { device.details.active_wan_iface = device_interface }

        let(:mib_interface) {}
        let(:device_interface) {}

        it 'saves the wan utilization metrics using the device interface' do
          expect_any_instance_of(Platforms::Gateway::WanUtilizationReport).to receive(:save_metrics)
          subject.run(device, event)
        end

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('WAN Utilization Report')
        end
      end
    end

    describe '#bandwidth_test_result' do
      let(:type) { :evBandwTestResult }
      let(:upload_result) { '10240' }
      let(:download_result) { '7728' }
      let(:upload_hwm) { '10241' }
      let(:download_hwm) { '7729' }
      let(:interface) { 'ETH1' }
      let(:bandwidth_type) { 1 }
      let(:bandwidth_test_status) { 1 }
      let(:evInterface) { interface }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          evTrapCreatedTime: 1_461_769_776_000_000,
          ngcoWanMtu: '1499',
          ngcoLastTestTimeDAT: 1_461_769_776_000_000,
          ngcoLastUpstreamResult: upload_result,
          ngcoLastDownstreamResult: download_result,
          evDataUpBandw: upload_hwm,
          evDataDownBandw: download_hwm,
          coCurrentBandwSvr: '12.67.1.68',
          evTestingIfaceDescr: evInterface,
          evBandwTestIfacePri: 0,
          ngcoEnabled: bandwidth_test_status,
          evBandwType: bandwidth_type
        }
      end
      let(:event) { Platforms::Event.new(params) }
      let(:act) { subject.run(device, event) }
      let(:measurement_params) do
        {
          mac: device.mac,
          organization_id: device.organization_id,
          interface: interface,
          upload_result: upload_result.to_i,
          download_result: download_result.to_i,
          upload_hwm: upload_hwm.to_i,
          download_hwm: download_hwm.to_i,
          bandwidth_type: bandwidth_type.to_i,
          bandwidth_test_status: bandwidth_test_status.to_i
        }
      end

      before do
        allow(Measurement::BandwidthTestResult).to receive(:create)
      end

      it 'creates an event indicating a bandwidth test result event was detected' do
        act
        expect(device.events.first.info).to include('Bandwidth Test Result Event')
      end

      context 'when evTestingIfaceDescr is present' do
        it 'creates a measurement' do
          expect(Measurement::BandwidthTestResult).to receive(:create).with(measurement_params)
          act
        end
      end

      context 'when evTestingIfaceDescr is nil' do
        let(:evInterface) {}

        before do
          expect_any_instance_of(Platforms::Gateway::BandwidthTestResult).to receive(:active_wan_iface).and_return(interface)
        end

        it 'creates a measurement when heuristic conditions are met' do
          expect(Measurement::BandwidthTestResult).to receive(:create).with(measurement_params)
          act
        end
      end
    end
  end
end
