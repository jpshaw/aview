# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway tunnel events' do
  describe Platforms::Gateway::TunnelEvents do
    subject { Platforms::Gateway::InformProcessor }

    let(:mgmt_host) { '2001:470:26:307:3b1b:56d:b749:600e' }

    describe '#no_tunnel_endpoints' do
      let(:type)   { :ngevNoTeps }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngtuIpsecIface: 'ipsec1'
        }
      end
      let(:event) { Platforms::Event.new(params) }

      it 'generates an event indicating the interface could not create any tunnels' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
        expect(device.events.first.info).to include('Unable to bring up a tunnel')
      end
    end

    describe '#tunnel_up' do
      let(:type) { :ngevTunnelUp }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevTunConnectionId: '13969214482388741232',
          ngevTunnelDateAndTime: 1_410_895_140_500_000,
          ngtuTunMode: '3',
          ngevConnectionType: '0',
          ngevAccount: 'ATTDEMO',
          ngevUserid: 'ACCEL6',
          ngtuEndpoint: '56.249.155.6',
          ngtuIPv6Endpoint: '0:0:0:0:0:0:0:0',
          ngtuAuthType: '0',
          ngtuAuthProt: '1',
          ngtuEndpointType: '9',
          ngevTunConReasonNUM: '0',
          nghwWanConnectionMethod: '7',
          ngtuTunInitiator: '2',
          ngtuIpsecIface: 'eth2',
          ngevVPNServiceIPv4Address: '106.178.181.5',
          ngevVPNServiceIPv6Address: mgmt_host
        }
      end
      let(:event) { Platforms::Event.new(params) }

      context 'when the tunnel was successfully established' do
        before do
          params[:ngevTunConReason] = 'Successful Logon'
        end

        it 'creates a new tunnel' do
          expect do
            subject.run(device, event)
          end.to change { device.tunnels.count }.by(1)
        end

        it 'generates an event indicating the tunnel was successfully established' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Tunnel established')
        end

        context 'for tunnel timestamp' do
          let(:time) { Time.new(2016, 6, 25, 11, 30, 25, 0) }

          context 'when given in microseconds' do
            before do
              params[:ngevTunnelDateAndTime] = time.to_i * 1_000_000
            end

            it 'correctly parses the timestamp' do
              subject.run(device, event)
              expect(device.tunnels.first.started_at).to eq time
            end
          end

          context 'when given in seconds' do
            before do
              params[:ngevTunnelDateAndTime] = time.to_i
            end

            it 'correctly parses the timestamp' do
              subject.run(device, event)
              expect(device.tunnels.first.started_at).to eq time
            end
          end
        end

        context 'when given a management tunnel' do
          before do
            params[:ngtuTunMode] = GatewayTunnel::MANAGEMENT_MODE
          end

          it 'updates the management host' do
            expect do
              subject.run(device, event)
            end.to change(device, :mgmt_host).to mgmt_host
          end

          it 'updates the dns record for the device' do
            subject.run(device, event)
            expect(PowerDnsRecordManager::Record.last.content).to eq mgmt_host
          end
        end

        context 'when given a tunnel name' do
          before do
            params[:tunName] = 'AVIEW-TUNNEL'
          end

          it 'saves the tunnel name' do
            subject.run(device, event)
            expect(device.tunnels.last.tunnel_name).to eq 'AVIEW-TUNNEL'
          end
        end
      end

      context 'when the tunnel failed to establish' do
        before do
          params[:ngevTunConReason] = '!Error 255 Tunnel server is not responding to the IKE negotiation request.'
        end

        it 'does not create any tunnels' do
          expect do
            subject.run(device, event)
          end.not_to change { device.tunnels.count }
        end

        it 'generates an event indicating the tunnel failed to establish' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Tunnel Error')
        end
      end
    end

    describe '#tunnel_down' do
      let(:type)   { :ngevTunnelDown }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          ngevTunConnectionId: '13969214482388741232',
          ngevTunnelDateAndTime: 1_410_895_140_500_000

        }
      end
      let(:event) { Platforms::Event.new(params) }

      context 'when given a tunnel heartbeat' do
        before do
          params[:ngtuReasonCode] = GatewayTunnel::REASON_TUNNEL_ALIVE
        end

        it 'creates an event for the tunnel heartbeat' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Tunnel heartbeat')
        end

        context 'when the tunnel exists' do
          let(:tunnel_interface) { 'IPSEC4' }

          before do
            device.tunnels.create!(
              ipsec_iface:   tunnel_interface,
              connection_id: '13969214482388741232'
            )
          end

          it 'includes the tunnel interface in the event' do
            subject.run(device, event)
            expect(device.events.first.info).to include(tunnel_interface)
          end

          context 'when given a heartbeat for a management tunnel' do
            before do
              params[:ngtuTunMode] = GatewayTunnel::MANAGEMENT_MODE
              params[:ngevVPNServiceIPv6Address] = mgmt_host
            end

            it 'updates the device management host' do
              expect do
                subject.run(device, event)
              end.to change(device, :mgmt_host).to mgmt_host
            end

            it 'updates the device dns record' do
              subject.run(device, event)
              expect(PowerDnsRecordManager::Record.last.content).to eq mgmt_host
            end
          end
        end
      end

      context 'when given a tunnel down' do
        before do
          params[:ngtuReasonCode] = GatewayTunnel::REASON_CODES.keys.first
        end

        it 'creates an event for the tunnel down' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Tunnel disconnected')
        end

        context 'when the tunnel exists' do
          let(:tunnel_interface) { 'IPSEC4' }

          before do
            device.tunnels.create!(
              ipsec_iface:   tunnel_interface,
              connection_id: '13969214482388741232'
            )
          end

          it 'includes the tunnel interface in the event' do
            subject.run(device, event)
            expect(device.events.first.info).to include(tunnel_interface)
          end

          it 'destroys the tunnel' do
            expect do
              subject.run(device, event)
            end.to change { device.tunnels.count }.from(1).to(0)
          end
        end
      end
    end
  end
end
