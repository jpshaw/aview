# frozen_string_literal: true

shared_examples_for 'gateway commands' do
  describe '#send_command' do
    let(:command_stub) { GatewayCommand.new(subject, {}) }

    before do
      allow(command_stub).to receive(:send!)
    end

    it 'instantiates a GatewayCommand' do
      expect(GatewayCommand).to receive(:new).and_return(command_stub)
      subject.send_command({})
    end

    it 'sends the command' do
      allow(GatewayCommand).to receive(:new).and_return(command_stub)
      expect(command_stub).to receive(:send!)
      subject.send_command({})
    end
  end
end
