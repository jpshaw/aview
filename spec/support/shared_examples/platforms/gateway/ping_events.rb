# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway ping events' do
  describe Platforms::Gateway::PingEvents do
    subject { Platforms::Gateway::InformProcessor }

    describe '#ping_test_result' do
      let(:ipv4_address) { '8.8.8.8' }
      let(:ipv6_address) { '2620:000e:4000:4440:e61f:001a:4a7e:5808' }

      let(:type)   { :evPingTestResult }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          evTrapCreatedTime: 1_461_872_814_000_000,
          evResult: '0',
          evSrcV4: '0.0.0.0',
          evDestV4: '0.0.0.0',
          evSrcV6: '0000:0000:0000:0000:0000:0000:0000:0000',
          evDestV6: '0000:0000:0000:0000:0000:0000:0000:0000',
          evResponseMax: '50',
          evFailureCount: '0',
          evDescription: '',
          evSendTrapWhen: '0'
        }
      end

      let(:event) { Platforms::Event.new(params) }

      context 'when the ping test succeeded' do
        before do
          @ping_test_succeeded_uuid = create(:device_event_uuid, code: EventUUIDS::GATEWAY_PING_TEST_SUCCEEDED)
          params[:evResult] = Platforms::Gateway::PingTestResult::SUCCESS
        end

        it 'creates an event indicating the test succeeded' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to match(/ping test .* succeeded/i)
        end

        it 'generates a notification uuid for the event' do
          subject.run(device, event)
          expect(device.events.first.uuid).to eq @ping_test_succeeded_uuid
        end
      end

      context 'when the ping test failed' do
        before do
          @ping_test_failed_uuid = create(:device_event_uuid, code: EventUUIDS::GATEWAY_PING_TEST_FAILED)
          params[:evResult] = Platforms::Gateway::PingTestResult::FAILURE
        end

        it 'creates an event indicating the test failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to match(/ping test .* failed/i)
        end

        it 'generates a notification uuid for the event' do
          subject.run(device, event)
          expect(device.events.first.uuid).to eq @ping_test_failed_uuid
        end
      end

      context 'when the destination is a valid IPv4 address' do
        before do
          params.merge! evDestV4: ipv4_address
        end

        it 'includes the IPv4 address in the event' do
          subject.run(device, event)
          expect(device.events.first.info).to include(ipv4_address)
        end
      end

      context 'when the destination is a valid IPv6 address' do
        before do
          params.merge! evDestV6: ipv6_address
        end

        it 'includes the IPv6 address in the event' do
          subject.run(device, event)
          expect(device.events.first.info).to include(ipv6_address)
        end
      end
    end
  end
end
