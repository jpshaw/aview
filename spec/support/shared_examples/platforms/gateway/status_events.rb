# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'gateway status events' do
  describe Platforms::Gateway::StatusEvents do
    subject { Platforms::Gateway::InformProcessor }

    # rspec './spec/models/gateway_spec.rb[1:4:10:1]'
    describe '#profile_not_found_or_discontinued' do
      let(:type)   { :ngevDeviceDiscoOrNotFound }
      let(:params) do
        {
          mac: device.mac,
          type: type
        }
      end
      let(:event) { Platforms::Event.new(params) }

      context 'when given an unknown problem id' do
        before do
          params[:ngevQueryProblem] = 99
        end

        it 'does not generate an event' do
          expect do
            subject.run(device, event)
          end.not_to change { device.events.count }
        end
      end

      context 'when given a problem id indicating the proile is missing' do
        before do
          params[:ngevQueryProblem] = ::Gateway::PROFILE_NOT_FOUND
        end

        it 'generates an event indicating the profile was not found' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('profile not found')
        end
      end

      context 'when given a problem id indicating the device is discontinued' do
        before do
          params[:ngevQueryProblem] = ::Gateway::DISCONTINUED_DEVICE
        end

        it 'generates an event indicating the device is discontinued' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Device discontinued')
        end
      end

      context 'when given a problem id indicating a bandwidth test failed' do
        before do
          params[:ngevQueryProblem] = ::Gateway::BANDWIDTH_NO_RESULTS
        end

        it 'generates an event indicating a bandwidth test failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Bandwidth test failed')
        end
      end
    end

    # rspec './spec/models/gateway_spec.rb[1:4:10:2]'
    describe '#oob_cable_required' do
      let(:type)   { :evOOBCableRequired }
      let(:params) do
        {
          mac: device.mac,
          type: type,
          evFoundOOB: 0,
          evExpectedOOB: 0
        }
      end
      let(:event) { Platforms::Event.new(params) }

      context 'the expected number of cables are found' do
        before do
          params[:evFoundOOB]    = 1
          params[:evExpectedOOB] = 1
        end

        it 'generates an event indicating the cables are all present' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('OOB cables are present')
        end
      end

      context 'fewer cables found than expected' do
        before do
          params[:evFoundOOB]    = 1
          params[:evExpectedOOB] = 2
        end

        it 'generates an event indicating that cables are missing' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('OOB cables are missing - Found: 1, Expected: 2')
        end
      end
    end

    # rspec './spec/models/gateway_spec.rb[1:4:10:3]'
    describe '#successful_query' do
      let(:type)   { :evSuccessfulQuery }
      let(:params) { { mac: device.mac, type: type } }
      let(:event)  { Platforms::Event.new(params) }

      # evQueryType OBJECT-TYPE
      #   SYNTAX      INTEGER {
      #                 initialBootQuery(1),
      #                 dailyQuery(2),
      #                 loginQuery(3),
      #                 dapSetupQuery(4),
      #                 userInitiatedQuery(5),
      #                 partialUserInitiatedQuery(6),
      #                 zeroTouchQuery(8)
      #               }
      let(:query_types) do
        {
          initial_boot: 1,
          daily: 2,
          login: 3,
          dap_setup: 4,
          user_initiated: 5,
          partial_user_initiated: 6,
          zero_touch: 8
        }
      end

      # evQueryScope OBJECT-TYPE
      #   SYNTAX      INTEGER {
      #                 fullQuery(0),
      #                 partialQuery(1)
      #               }
      let(:query_scopes) { { full: 0, partial: 1 } }

      context 'for `full` scope' do
        before { params[:evQueryScope] = query_scopes[:full] }

        it 'generates an event indicating the query scope is `full`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Scope: Full')
        end
      end

      context 'for `partial` scope' do
        before { params[:evQueryScope] = query_scopes[:partial] }

        it 'generates an event indicating the query scope is `partial`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Scope: Partial')
        end
      end

      context 'for `initial boot` type' do
        before { params[:evQueryType] = query_types[:initial_boot] }

        it 'generates an event indicating the query type is `initial boot`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Initial Boot')
        end
      end

      context 'for `daily` type' do
        before { params[:evQueryType] = query_types[:daily] }

        it 'generates an event indicating the query type is `daily`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Daily')
        end
      end

      context 'for `login` type' do
        before { params[:evQueryType] = query_types[:login] }

        it 'generates an event indicating the query type is `login`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Login')
        end
      end

      context 'for `dap setup` type' do
        before { params[:evQueryType] = query_types[:dap_setup] }

        it 'generates an event indicating the query type is `dap setup`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Setting up Dial as Primary')
        end
      end

      context 'for `user initiated` type' do
        before { params[:evQueryType] = query_types[:user_initiated] }

        it 'generates an event indicating the query type is `user initiated`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('User Initiated')
        end
      end

      context 'for `partial user initiated` type' do
        before { params[:evQueryType] = query_types[:partial_user_initiated] }

        it 'generates an event indicating the query type is `partial user initiated`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('User Initiated')
        end
      end

      context 'for `zero touch` type' do
        before { params[:evQueryType] = query_types[:zero_touch] }

        it 'generates an event indicating the query type is `zero touch`' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Zero Touch')
        end
      end
    end

    # rspec './spec/models/gateway_spec.rb[1:4:10:4]'
    describe '#zero_touch_query_fail' do
      let(:type)   { :evZeroTouchQueryFail }
      let(:params) { { mac: device.mac, type: type } }
      let(:event)  { Platforms::Event.new(params) }

      it 'generates an event indicating the zero touch query failed' do
        expect { subject.run(device, event) }.to change { device.events.count }.by(1)
        expect(device.events.last.info).to include('Zero Touch query failed')
      end
    end

    # rspec './spec/models/gateway_spec.rb[1:4:10:5]'
    describe '#port_auth_or_fail' do
      let(:type)   { :evPortAuthOrFail }
      let(:params) { { mac: device.mac, type: type } }
      let(:event)  { Platforms::Event.new(params) }

      # evResult  OBJECT-TYPE
      #   SYNTAX      INTEGER {
      #                 failure(0),
      #                 success(1),
      #                 reset(2)
      #               }
      let(:results) { { failure: 0, success: 1, reset: 2 } }

      context 'for success' do
        before { params[:evResult] = results[:success] }

        it 'generates an event indicating the port auth succeeded' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Port Auth Succeeded')
        end
      end

      context 'for failure' do
        before { params[:evResult] = results[:failure] }

        it 'generates an event indicating the port auth failed' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Port Auth Failed')
        end
      end

      context 'for reset' do
        before { params[:evResult] = results[:reset] }

        it 'generates an event indicating the port auth reset' do
          expect { subject.run(device, event) }.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Port Auth Reset')
        end
      end
    end

    # rspec './spec/models/gateway_spec.rb[1:4:10:6]'
    describe '#boot' do
      let(:type)   { :evBoot }
      let(:params) { { mac: device.mac, type: type } }
      let(:event)  { Platforms::Event.new(params) }

      it 'creates an event indicating boot is complete' do
        subject.run(device, event)
        expect(device).to have_event_info 'Boot complete'
      end

      context 'when firmware is given' do
        let(:firmware) { '7.0.10' }

        before { params[:nghwCodeVersion] = firmware }

        it 'updates the firmware' do
          expect { subject.run(device, event) }.to change(device, :firmware).to(firmware)
        end
      end

      context 'when interface data is given' do
        let(:ip)    { '192.168.15.14' }
        let(:iface) { 'eth2' }

        before do
          params[:ifIPv4ActiveWanIfaceIP] = ip
          params[:ifIPv4ActiveWanIfaceDescr] = iface
        end

        it 'updates the primary ip' do
          expect { subject.run(device, event) }.to change(device, :host).to(ip)
        end

        it 'updates the primary interface' do
          expect { subject.run(device, event) }.to change(device, :primary_wan_iface).to(iface)
        end
      end
    end
  end
end
