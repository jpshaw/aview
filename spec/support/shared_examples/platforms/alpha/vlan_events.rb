# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha vlan events' do
  describe Platforms::Alpha::VlanEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#auto_vlan' do
      let(:message) { 'Vlan=4~webip=10.23.199.229' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :auto_vlan) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end

      context 'when the device host is given' do
        it 'updates the device host' do
          expect do
            subject.run(device, event)
          end.to change(device, :host).to('10.23.199.229')
        end
      end
    end
  end
end
