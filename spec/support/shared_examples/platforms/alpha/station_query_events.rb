# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha station query events' do
  describe Platforms::Alpha::StationQueryEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#station_query' do
      context 'when given station query details' do
        let(:message) { 'RawData(Ch SSID BSSID Security SiganlPct W-Mode)=1 FDS210 b4:c7:99:03:08:82 WPA1PSKWPA2PSK/TKIPAES 24 11b/g/n~1 b4:c7:99:03:08:88 WPA1WPA2/TKIPAES 29 11b/g/n NONE~1 0c:68:03:eb:5a:b3 WPA2PSK/AES 44 11b/g/n NONE~1 1CloudConnect 0c:27:24:ab:23:f5 WPA2/AES 24 11b/g/n~1 1CloudConnect 0c:68:03:eb:5a:b5 WPA2/AES 44 11b/g/n~1 FDS020 b4:c7:99:03:08:81 WEP 15 11b/g~1 b4:c7:99:03:08:87 WPA1WPA2/TKIPAES 24 11b/g/n NONE~1 !Free_WellingtonGreen 0c:27:24:51:22:66 NONE 20 11b/g/n~1 Taubman WiFi 0c:27:24:51:22:61 WPA2/AES 5~1 0c:27:24:ab:23:f2 WPA2PSK/TKIPAES 24 11b/g/n NONE~1 0c:68:03:eb:5a:b2 WPA2PSK/TKIPAES 39 11b/g/n NONE~1 !Free_WellingtonGreen 0c:27:24:ab:23:f6 NONE 29 11b/g/n~1 0c:27:24:51:22:63 WPA2PSK/AES 10 11b/g/n NONE~1 !Free_WellingtonGreen 0c:68:03:eb:5a:b6 NONE 44 11b/g/n~1 Taubman WiFi 0c:27:24:ab:23:f1 WPA2/AES 29~1 0c:27:24:ab:14:b3 WPA2PSK/AES 15 11b/g/n NONE~1 0c:27:24:ab:23:f3 WPA2PSK/AES 29 11b/g/n NONE~1 0c:27:24:51:22:62 WPA2PSK/TKIPAES 5 11b/g/n NONE~6 Yogurbella 00:24:93:57:ab:20 NONE 100 11b/g~6 toojaysfreewireless 00:26:42:4d:5c:00 NONE 39 11b/g~6 0c:68:03:b9:92:12 WPA2PSK/TKIPAES 44 11b/g/n NONE~6 AMJ 00:24:c9:a9:a0:90 WEP 0 11b/g~6 !Free_WellingtonGreen 0c:68:03:b9:92:16 NONE 39 11b/g/n~6 00:18:0a:6d:ea:00 WPAPSK/TKIP 10 11b/g NONE~6 Taubman WiFi 0c:68:03:b9:92:11 WPA2/AES 39~6 06:18:0a:6d:ea:00 WPAPSK/TKIP 15 11b/g NONE~6 0c:68:03:b9:92:13 WPA2PSK/AES 39 11b/g/n NONE~6 98:fc:11:5c:25:06 WPA2PSK/AES 24 11b/g/n NONE~6 1CloudConnect 0c:68:03:b9:92:15 WPA2/AES 39 11b/g/n~6 1CloudConnect 0c:27:24:ab:1c:45 WPA2/AES 0 11b/g/n~6 0a:18:0a:6d:ea:00 WPA1WPA2/TKIPAES 10 11b/g/n NONE~6 Nails 00:24:93:49:cf:60 WEP 0 11b/g~6 b4:c7:99:02:d7:d8 WPA1WPA2/TKIPAES 0 11b/g/n NONE~6 0c:27:24:ab:1c:42 WPA2PSK/TKIPAES 0 11b/g/n NONE~6 b4:c7:99:02:d7:d7 WPA1WPA2/TKIPAES 0 11b/g/n NONE~11 06:18:0a:6d:f9:70 WPAPSK/TKIP 5 11b/g NONE~11 00:18:0a:6d:f9:70 WPAPSK/TKIP 5 11b/g NONE' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :staquery) }

        it 'creates an event indicating station details were given' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Station Query Details')
        end
      end

      context 'when rogue access points are detected' do
        let(:message) { 'Rogue APs detected on network: SSID=tacocat,MAC=00c0ca49d277,IP=10.10.10.11~SSID=clearer,MAC=002704012345,IP=10.10.10.100' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :staquery) }

        context 'when the device has not already associated with the access points' do
          before do
            expect(device.rogue_access_points).to be_empty
          end

          it 'creates and associates with the rogue access points' do
            expect do
              subject.run(device, event)
            end.to change { device.rogue_access_points.count }.by(2)
            device.reload
          end

          it 'creates events for the new access points' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }.by(2)
            device.events.each do |event|
              expect(event.info).to include('Rogue AP detected on network')
            end
          end
        end

        context 'when the device has already associated with the access points' do
          before do
            @ap_1 = create(:rogue_access_point,
                                      device_id: device.id,
                                      ip:        '10.10.10.11',
                                      mac:       '00c0ca49d277',
                                      ssid:      'tacocat')
            @ap_2 = create(:rogue_access_point,
                                      device_id: device.id,
                                      ip:        '10.10.10.101',
                                      mac:       '002704012345',
                                      ssid:      'clearer')
          end

          it 'does not create any events' do
            expect do
              subject.run(device, event)
            end.not_to change { device.events.count }
          end

          it 'does not create any rogue access points' do
            expect do
              subject.run(device, event)
            end.not_to change { device.rogue_access_points.count }
          end

          it 'updates access point information' do
            expect(@ap_2.ip).to eq '10.10.10.101'
            subject.run(device, event)
            @ap_2.reload
            expect(@ap_2.ip).to eq '10.10.10.100'
          end
        end

        context 'when an existing rogue access point is not given' do
          before do
            @ap_1 = create(:rogue_access_point,
                                      device_id: device.id,
                                      ip:        '192.168.1.3',
                                      mac:       '002704ABCDEF',
                                      ssid:      'testing')
          end

          it 'is destroyed' do
            expect do
              subject.run(device, event)
            end.to change {
              device.rogue_access_points.where(id: @ap_1.id).to_a
            }.from([@ap_1]).to([])
          end
        end
      end

      context 'when rogue access points are given with spaced groupings' do
        let(:message) { 'Rogue APs detected on network: SSID=tacocat MAC=00c0ca49d277 IP=10.10.10.11~SSID=clearer MAC=002704012345 IP=10.10.10.100' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :staquery) }

        before do
          expect(device.rogue_access_points).to be_empty
        end

        it 'creates and associates with the rogue access points' do
          expect do
            subject.run(device, event)
          end.to change { device.rogue_access_points.count }.by(2)
          device.reload
        end

        it 'creates events for the new access points' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(2)
          device.events.each do |event|
            expect(event.info).to include('Rogue AP detected on network')
          end
        end
      end

      context 'when no rogue access points are detected' do
        before do
          @rogue_ap_alert_uuid = create(:device_event_uuid, code: EventUUIDS::NETREACH_ROGUE_ACCESS_POINT)
        end

        let(:message) { 'Rogue APs detected on network: ' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :staquery) }

        it 'creates an event indicating no access points were detected' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('No Rogue APs detected on network')
        end

        context 'when the device had no previous access points' do
          before do
            expect(device.rogue_access_points.count).to eq 0
          end

          it 'does not tag the event as alertable' do
            subject.run(device, event)
            expect(device.events.first.uuid).to be nil
          end
        end

        context 'when the device had previous access points' do
          before do
            @ap_1 = create(:rogue_access_point,
                                      device_id: device.id,
                                      ip:        '192.168.1.3',
                                      mac:       '002704ABCDEF',
                                      ssid:      'testing')
          end

          it 'tags the event as alertable' do
            subject.run(device, event)
            expect(device.events.first.uuid).to eq @rogue_ap_alert_uuid
          end

          it 'destroys the access points' do
            expect do
              subject.run(device, event)
            end.to change { device.rogue_access_points.count }.to(0)
          end
        end
      end
    end
  end
end
