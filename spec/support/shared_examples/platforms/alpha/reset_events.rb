# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha reset events' do
  describe Platforms::Alpha::ResetEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#reset' do
      context 'when a reset event is triggered' do
        let(:message) { 'Reset button pressed. Starting reset back to default config!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Reset') }

        it 'creates an event indicating the device is restoring defaults' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Device restoring system defaults')
        end
      end

      context 'when the device has restored to default settings' do
        let(:message) { 'Flash update completed successfully. Default config loaded' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Reset') }

        it 'creates an event indicating the device has restored to the default settings' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Default settings restored on unit')
        end
      end

      context 'when the device reboots after a reset' do
        let(:message) { 'Rebooting...' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Reset') }

        it 'creates an event indicating the device is rebooting' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Device attempting to reboot')
        end
      end

      context 'when a device reset encounters a failure' do
        let(:message) { 'critical error!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Reset') }

        it 'creates an event indicating the device was unable to be reset' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Unable to restore default config')
        end
      end
    end
  end
end
