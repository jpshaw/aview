# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha configuration events' do
  describe Platforms::Alpha::ConfigurationEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#configuration' do
      context 'when given firmware information' do
        context 'when the given firmware is the same' do
          let(:message) { 'date=Mon May 11 02:43:24 EDT 2015~firmware=1.621.81~status=ok' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

          before do
            device.update_attribute :firmware, '1.621.81'
          end

          it 'does not change the firmware' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :firmware)
          end

          it 'does not create an event indicating the firmware changed' do
            subject.run(device, event)

            device.events.each do |event|
              expect(event.info).not_to include('Firmware changed')
            end
          end
        end

        context 'when the given firmware is different' do
          let(:message) { 'date=Mon May 11 02:43:24 EDT 2015~firmware=1.621.81~status=ok' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

          before do
            device.update_attribute :firmware, '1.0.0'
          end

          it 'updates the device firmware' do
            expect do
              subject.run(device, event)
            end.to change(device, :firmware).to('1.621.81')
          end

          it 'creates an event indicating the firmware change' do
            subject.run(device, event)
            expect(device.events.first.info).to eq 'Firmware changed from 1.0.0 to 1.621.81'
          end
        end

        context 'when the firmware status is ok' do
          let(:message) { 'date=Mon May 11 02:43:24 EDT 2015~firmware=1.621.81~status=ok' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

          it 'creates an event indicating the firmware was successfully loaded' do
            subject.run(device, event)
            expect(device.events.last.info).to eq 'Configuration applied'
          end
        end

        context 'when the firmware status is error' do
          let(:message) { 'date=Mon May 11 02:43:24 EDT 2015~firmware=1.621.81~status=error' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

          it 'creates an event indicating the firmware failed to load' do
            subject.run(device, event)
            expect(device.events.last.info).to eq 'Failed to get configuration'
          end
        end
      end

      context 'when the config is up to date' do
        let(:message) { 'Configuration up to date' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

        it 'creates an event indicating the config is up to date' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Configuration up to date')
        end
      end

      context 'when the config download is successful' do
        let(:message) { 'SMx config download successful' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :smxquery) }

        it 'creates an event indicating the config download was successful' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Configuration downloaded')
        end
      end

      context 'when the config download failed' do
        let(:message) { 'Error: unable to download configuration' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :smxquery) }

        it 'creates an event indicating the config download failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Configuration download failed')
        end
      end

      context 'when given a restart message' do
        let(:message) { 'Cause of last restart: (Configuration update)' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

        it 'creates a reboot event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.reboots.count }.by(1)
        end

        it 'updates the device restarted timestamp' do
          Timecop.freeze do
            expect do
              subject.run(device, event)
            end.to change(device, :last_restarted_at).to(Time.now.utc)
          end
        end
      end

      context 'when given an indication the device will restart' do
        let(:message) { 'rebooting (Configuration update)' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

        it 'creates a reboot event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.reboots.count }.by(1)
          expect(device.events.last.info).to include('Device attempting to reboot ...')
        end
      end

      context 'when given an unrecognized config message' do
        let(:message) { 'Listening for HTTP requests at address 172.19.202.210' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

        it 'creates an event with the message' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to eq message
        end
      end
    end
  end
end
