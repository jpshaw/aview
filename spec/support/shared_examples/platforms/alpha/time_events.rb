# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha time events' do
  describe Platforms::Alpha::TimeEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#smxtime' do
      context 'when given time information' do
        let(:message) { 'date=Sat Apr 25 22:30:58 EDT 2015~server1=time.accns.com~server2=108.166.125.69' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :smxtime) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end

      context 'when there is a time error' do
        let(:message) { 'Failed to establish date/time!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :smxtime) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end
    end
  end
end
