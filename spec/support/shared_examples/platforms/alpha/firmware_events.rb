# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha firmware events' do
  describe Platforms::Alpha::FirmwareEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#firmware' do
      context 'when the firmware version is given' do
        context 'when the given firmware is different' do
          let(:message) { "img '1.672.91' is current" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

          before do
            device.update_attribute :firmware, '1.0.0'
          end

          it 'updates the device firmware' do
            expect do
              subject.run(device, event)
            end.to change(device, :firmware).to('1.672.91')
          end

          it 'creates an event indicating the firmware change' do
            subject.run(device, event)
            event = device.events.first
            expect(event).to be_present
            expect(event.info).to eq 'Firmware changed from 1.0.0 to 1.672.91'
          end
        end

        context 'when the given firmware is the same' do
          let(:message) { "img '1.672.91' is current" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

          before do
            device.update_attribute :firmware, '1.672.91'
          end

          it 'does not change the firmware' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :firmware)
          end

          it 'does not create an event indicating the firmware changed' do
            subject.run(device, event)

            device.events.each do |event|
              expect(event.info).not_to include('Firmware changed')
            end
          end
        end
      end

      context 'when a new firmware is ready to be applied' do
        let(:message) { 'new version is ready to apply' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event indicating the firmware is ready to be applied' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Firmware downloaded')
        end
      end

      context 'when updating the firmware' do
        let(:message) { 'Updating firmware. DO NOT POWER OFF THE UNIT' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Apply') }

        it 'creates an event indicating the firmware is being updated' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Upgrading firmware now')
        end
      end

      context 'when the firmware update was successful' do
        let(:message) { 'Firmware update successful.' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates an event indicating the firmware update was successful' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Firmware upgrade successful')
        end
      end

      context 'when a firmware check fails' do
        let(:message) { 'wget from (firmware.accns.com) failed!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event indicating the firmware check failure' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Failed to check for new firmware')
        end
      end

      context 'when decrypting the firmware fails' do
        let(:message) { 'Unable to decrypt (/tmp/update.mac)!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event indicating the firmware descryption failure' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Failed to decrypt firmware')
        end
      end

      context 'when the firmware image is unknown' do
        let(:message) { 'img unknown!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates an event indicating the firmware image is unknown' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Unknown firmware image')
        end
      end

      context 'when getting a firmware image fails' do
        let(:message) { 'wget of (1.481.81.bin) failed!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event indicating the firmware upgrade failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Failed to upgrade firmware')
        end
      end

      context 'when getting a firmware checksum fails' do
        let(:message) { 'wget of (1.481.81.md5) failed!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event indicating the firmware upgrade failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Failed to upgrade firmware')
        end
      end

      context 'when given an unrecognized firmware message' do
        let(:message) { 'Downloading firmware configuration' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: 'Update-Check') }

        it 'creates an event with the message' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to eq message
        end
      end
    end
  end
end
