# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha status events' do
  describe Platforms::Alpha::StatusEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#status' do
      let(:message) { 'cnt=1127~rx=1876023~tx=306449~rx10=1363~tx10=258~uptime= 21:10:13 up 7 days  19:41   load average: 0.00  0.00  0.00' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end

      it 'stores the status data' do
        subject.run(device, event)
        data = device.events.last.raw_data
        expect(data['status']['cnt']).to eq '1127'
        expect(data['status']['rx']).to eq '1876023'
        expect(data['status']['tx']).to eq '306449'
      end
    end
  end
end
