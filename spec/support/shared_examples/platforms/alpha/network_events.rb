# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha network events' do
  describe Platforms::Alpha::NetworkEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#network' do
      context 'when given a client summary' do
        let(:message) { 'Associated client summary: [28e34728188c#0123059b74e3303]' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'creates an event indicating the list of clients was updated' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('List of client devices updated')
        end
      end

      context 'when an unrecognized network event is given' do
        let(:message) { 'Client connected: 8863df784da7' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'creates an event with the message' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to eq message
        end
      end
    end
  end
end
