# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'alpha dhcp events' do
  describe Platforms::Alpha::DhcpEvents do
    subject { Platforms::Alpha::SyslogProcessor }

    describe '#dhcp' do
      context 'when given dhcp data' do
        let(:message) { 'GatewayLanIP=10.201.181.1~GatewayMAC=00:D0:CF:1D:12:BC~GatewayWanIP=99.16.224.185~WebIP=10.201.181.210~DNS=8.8.8.8 4.2.2.4' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcp) }

        context 'when the device host changes' do
          before do
            device.update_attribute(:host, '1.2.3.4')
          end

          it 'creates an event indicating the host changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Device host changed from')
          end

          it 'changes the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).to('10.201.181.210')
          end
        end

        context "when the device host doesn't change" do
          before do
            device.update_attribute(:host, '10.201.181.210')
          end

          it 'does not change the device host' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :host)
          end

          it 'does not create any events indicating the host has changed' do
            subject.run(device, event)
            device.events.each do |event|
              expect(event.info).not_to include('Device host changed from')
            end
          end
        end

        context 'when associating with a new gateway' do
          it 'adds the device association' do
            expect do
              subject.run(device, event)
            end.to change { device.device_associations.count }.by(1)
          end

          it 'associates with the given device mac' do
            subject.run(device, event)
            expect(device).to be_associated_with_mac '00D0CF1D12BC'
          end

          it 'stores the association meta data' do
            subject.run(device, event)
            association = device.device_associations.first
            expect(association.ip_address).to eq '10.201.181.1'
          end
        end

        context 'when already associated with the given device' do
          before do
            create(:device_association,
                   source_id: device.id,
                   target_mac: '00D0CF1D12BC')
          end

          it 'does not create a new association' do
            expect do
              subject.run(device, event)
            end.not_to change { device.device_associations.count }
          end
        end

        context 'when a client MAC is within the range of a known VPN Gateway device' do
          let(:client_device) { create(:gateway_device, mac: '00D0CF1D12BC', organization: device.organization) }
          let(:message) { 'GatewayLanIP=10.201.181.1~GatewayMAC=00:D0:CF:1D:12:BF~GatewayWanIP=99.16.224.185~WebIP=10.201.181.210~DNS=8.8.8.8 4.2.2.4' }

          it 'associates the device to the client device' do
            expect do
              subject.run(device, event)
            end.to associate_devices(device, client_device)
          end
        end
      end
    end
  end
end
