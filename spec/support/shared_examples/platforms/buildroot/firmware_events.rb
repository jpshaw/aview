# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot firmware events' do
  describe Platforms::Buildroot::FirmwareEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#firmware' do
      context 'when given firmware data' do
        let(:message) { 'version=2.391.31~serial=6200010070381321~vector=33.0.9.0.0.0~date=Sun Apr 12 02:26:30 UTC 2015' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        context 'when the firmware changes' do
          it 'updates the device firmware' do
            expect do
              subject.run(device, event)
            end.to change(device, :firmware).to('2.391.31')
          end

          it 'generates an event indicating the firmware change' do
            expect do
              subject.run(device, event)
            end.to change { device.events.firmwares.count }.by(1)
            expect(device.events.last.info).to include('2.391.31')
          end
        end

        context 'when the firmware does not change' do
          before do
            device.update_attribute(:firmware, '2.391.31')
          end

          it 'does not update the device firmware' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :firmware)
          end

          it 'generates an event with the current firmware' do
            expect do
              subject.run(device, event)
            end.to change { device.events.firmwares.count }.by(1)
            expect(device.events.last.info).to include('using firmware version 2.391.31')
          end
        end

        it 'sets the device serial' do
          expect do
            subject.run(device, event)
          end.to change(device, :serial).to('6200010070381321')
        end
      end

      context 'when given a download message' do
        context 'when downloading a firmware config' do
          let(:message) { 'download=configuration.accns.com/configurations/002704010158/device_config,try=3' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

          it 'creates a firmware config event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.configs.count }.by(1)
            expect(device.events.last.info).to include('firmware config')
          end
        end

        context 'when downloading a firmware file' do
          let(:message) { 'download=aview.accns.com/device_firmware/AcceleratedConcepts/6200-FX/2.391.31.tar' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

          it 'creates an event indicating it is downloading the firmware' do
            expect do
              subject.run(device, event)
            end.to change { device.events.firmwares.count }.by(1)
            expect(device.events.last.info).to include('2.391.31')
          end
        end

        context 'when it is given an unrecognized download message' do
          let(:message) { 'download=testing.accelecon/Testing/0.0.0.bin' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

          it 'creates an event with the download path' do
            download_path = message.gsub('download=', '')
            expect do
              subject.run(device, event)
            end.to change { device.events.count }.by(1)
            expect(device.events.last.info).to include(download_path)
          end
        end
      end

      context 'when given an error message' do
        let(:message) { 'Getfile error! Config download failed' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates an event indicating the firmware action failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to eq 'Unable to check configuration'
        end
      end

      context 'when given an ssl certificate error' do
        let(:message) { 'No certificate.' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'indicates an error with the ssl certificates' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to include('missing SSL certificate files')
        end
      end

      context 'when given an unrecognized firmware message' do
        let(:message) { 'unrecognized event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates a generic firmware event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to eq 'Firmware status'
        end
      end

      context 'when given a write message' do
        let(:message) { 'write=rootfs.squashfs:/dev/mtd6' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'indicates the firmware is being written' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to include('Applying firmware')
        end
      end
    end
  end
end
