# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot modem status events' do
  describe Platforms::Buildroot::ModemStatusEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    before do
      allow(Measurement::CellularUtilization).to receive(:save_sample)
    end

    describe '#modem_status' do
      context 'when given modem data' do
        let(:message) { 'manufacturer=Pantech~model=290~revision=L0290VWBB12F.248~provider=verizon~imei=990000479162713~imsi=311480023569585~iccid=89148000000232074634~phone=2678384151~apn=vzwinternet' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :modem) }

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        it 'sets the modem name' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:name) }.to('Pantech 290')
        end

        it 'sets the correct carrier' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:carrier) }.to('Verizon')
        end

        context 'when the device has a modem' do
          before do
            expect(device.modem).to be_blank
            @modem = device.modems.first_or_create(active: true)
          end

          it 'creates a second modem for the device' do
            subject.run(device, event)
            expect(device.modems.count).to eq(2)
          end

          it 'sets the new modem as active' do
            subject.run(device, event)
            expect(device.modems.find_by(iccid: '89148000000232074634').active).to eq(true)
          end

          it 'sets older modem as inactive' do
            expect(@modem.active).to eq(true)
            subject.run(device, event)
            expect(@modem.active).to eq(false)
          end
        end
      end

      context 'when given a modem status' do
        let(:message) { 'cnt=1~rx=3315~tx=4381~dbm=-84~ecio=0.000~cnti=LTE~rssi=-53~rsrq=-8.000~snr=-99.000~sinr=0.000~mtu=1428~cid=~lac=9B00~usb=12' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        context 'when the signal strength changes and falls below the set threshold' do
          before do
            allow_any_instance_of(DeviceModem).to receive(:signal_strength_threshold).and_return(90)
            device.modems.first_or_create
          end

          it 'generates an event indicating the signal change' do
            subject.run(device, event)
            expect(device.events.last.info).to include('Signal below threshold')
          end
        end
      end

      context 'when given a sticky apn' do
        let(:message) { 'STICKY APN (Current APN does not match configured central APN)' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates an event indicating the apn status' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('incorrect APN')
        end
      end

      context 'when a reboot status is given' do
        let(:message) { 'restart forced restart' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates a reboot event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.reboots.count }.by(1)
        end
      end

      context 'when given an unrecognized modem status' do
        let(:message) { 'unrecognized modem status event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates a basic modem status event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.modems.count }.by(1)
          expect(device.events.last.info).to include('Modem status')
        end
      end
    end
  end
end
