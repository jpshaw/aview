# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot remote events' do
  describe Platforms::Buildroot::RemoteEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#arping' do
      let(:message) { 'ARPING command given, but DHCP handshake not completed.  Trying to ping anyways.' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :arping) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end

    describe '#remote' do
      let(:message) { 'Heard [status]. Sending status now.' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :remote) }

      it 'creates a remote event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.remotes.count }.by(1)
      end
    end

    describe '#signal' do
      let(:message) { 'Excellent (-51 dBm) (100%) (LTE CNTI) (-51 RSSI) (-71 RSRP) (-8 RSRQ) (-99.000 SNR) (23.400 SINR) (0.000 Ec/Io)' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :signal) }

      it 'creates a remote event that includes the signal data' do
        expect do
          subject.run(device, event)
        end.to change { device.events.remotes.count }.by(1)
        expect(device.events.remotes.last.info).to include('Excellent -51 dBm (100%)')
      end
    end

    describe '#sms' do
      let(:message) { 'SMS reboot~message received.~Sent 15/04/07,12:40' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :sms) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end

    describe '#speed' do
      let(:message) { 'TX1:0 0.2621 mbps~TX2:0 retrans mbps~TX3:0 0.2621 mbps~TX4:0 retrans mbps~TX5:0 0.2621 mbps~TX6:0 retrans mbps~TX7:0 0.2622 mbps~TX8:0 retrans mbps~TX9:0 0.2621 mbps~TX10:0 retrans mbps~RX1:0 0.5243 mbps~RX2:0 retrans mbps~RX3:0 0.2621 mbps~RX4:0 retrans mbps~RX5:0 0.5244 mbps~RX6:0 retrans mbps~RX7:0 0.2621 mbps~RX8:0 retrans mbps~RX9:0 0.5244 mbps~RX10:0 retrans mbps' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :speed) }

      it 'creates a remote event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.remotes.count }.by(1)
      end
    end
  end
end
