# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot network events' do
  describe Platforms::Buildroot::NetworkEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#dhcp' do
      context 'when given dhcp data' do
        let(:message) { 'pppl=100.64.161.76~dns1=198.224.181.135~dns2=198.224.178.135' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcp) }

        context 'when the host changes' do
          before do
            expect(device.host).to be nil
          end

          it 'creates a dhcp event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
          end

          it 'sets the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(nil).to('100.64.161.76')
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, '100.64.161.76')
          end

          it 'creates a basic dhcp event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('DHCP status')
          end
        end
      end

      context 'when given a dhcp message' do
        let(:message) { 'waiting for dhcp handshake to begin' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcp) }

        it 'creates a dhcp event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.dhcps.count }.by(1)
        end
      end
    end

    describe '#dsflow' do
      let(:message) { 'N1=60602~N2=487030~N3=246373~N4=5016470~N5=417605255~N6=652798772' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dsflow) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end

    describe '#eth0' do
      context 'when a state is given' do
        let(:message) { 'state=active' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :eth0) }

        it 'updates the ethernet cable state' do
          expect do
            subject.run(device, event)
          end.to change(device, :eth_state).from(nil).to('active')
        end

        it 'generates an event indicating the ethernet cable state' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('active')
        end
      end
    end

    describe '#mac' do
      context 'when given only eth0' do
        let(:message) { 'eth0=00:27:04:01:06:e9' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :mac) }

        before do
          @eth_macs = { eth0: '0027040106E9' }
        end

        it 'generates a generic dhcp event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.dhcps.count }.by(1)
          expect(device.events.first.info).to include('Ethernet status')
        end

        it 'updates the device details to include the mac' do
          expect(device.eth_macs).to be_blank
          subject.run(device, event)
          expect(device.eth_macs).to eq @eth_macs
        end

        context 'when eth macs already exist for the device' do
          before do
            @existing_macs = { eth0: '002704011111',
                               eth1: '002704022222' }
            device.build_details
            device.details.eth_macs = @existing_macs
            device.details.save
          end

          it 'clears them and only sets eth0' do
            expect do
              subject.run(device, event)
            end.to change(device, :eth_macs).from(@existing_macs).to(@eth_macs)
          end
        end
      end

      context 'when given all mac data' do
        let(:message) { 'eth0=00:27:04:01:06:e9~eth1=c0:ea:e4:8a:5c:4f~ip=10.173.91.172~lease=300' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :mac) }

        before do
          @eth_macs = { eth0: '0027040106E9',
                        eth1: 'C0EAE48A5C4F' }
        end

        it 'sets the client ip' do
          expect do
            subject.run(device, event)
          end.to change(device, :client_ip).from(nil).to('10.173.91.172')
        end

        it 'sets the eth macs' do
          expect do
            subject.run(device, event)
          end.to change(device, :eth_macs).from(nil).to(@eth_macs)
        end

        it 'generates a dhcp event for the eth1 interface' do
          expect do
            subject.run(device, event)
          end.to change { device.events.dhcps.count }.by(1)
          expect(device.events.first.info).to include('C0EAE48A5C4F')
        end

        it 'creates an association with the client device' do
          expect do
            subject.run(device, event)
          end.to associate_device_to_mac(device, 'C0EAE48A5C4F')
        end

        context 'when a client MAC is within the range of a known VPN Gateway device' do
          let(:client_device) { create(:gateway_device, mac: '00D0CF1D12BC', organization: device.organization) }
          let(:message) { 'eth0=00:27:04:01:06:e9~eth1=00:d0:cf:1d:12:be~ip=10.173.91.172~lease=300' }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client device' do
            expect do
              subject.run(device, event)
            end.to associate_devices(device, client_device)
          end
        end
      end
    end

    describe 'network_usage' do
      context 'when the device is unable to check network usage' do
        let(:message) { 'Unable to check data usage. SMS not supported' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :NetworkUsage) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end

      context 'when given invalid network usage data' do
        let(:message) { 'remaining=~used=~unit_measurement=' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :NetworkUsage) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end

      context 'when given valid network usage data' do
        let(:message) { 'remaining=5058.61~used=61.3901~unit_measurement=MB~through_date=2015-04-20T17:42:45.000Z' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :NetworkUsage) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end
    end

    describe '#sshd' do
      let(:message) { 'SSH session started. Session ends in 10 minutes' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :sshd) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end
  end
end
