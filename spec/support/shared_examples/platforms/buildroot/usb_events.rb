# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot usb events' do
  describe Platforms::Buildroot::UsbEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#usb' do
      before do
        device.build_details
      end

      context 'when a usb hub is connected' do
        let(:message) { 'bo=y~usb1_2=0424:2412~usb1_4=0f3d:68aa' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :usb) }

        it 'updates the device details to indicate the hub is connected' do
          expect(device.details.usb_hub).to be nil
          subject.run(device, event)
          expect(device.details.usb_hub).to eq 'Connected'
        end

        it 'populates the usb list' do
          expect(device.details.usb_list).to be nil
          subject.run(device, event)
          expect(device.details.usb_list).to eq '0424:2412,0f3d:68aa'
        end

        it 'creates a device event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end

      context 'when a usb hub is removed' do
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: '', type: :usb) }

        before do
          device.details.update!(
            usb_hub: 'Connected',
            usb_list: '0424:2412,0f3d:68aa'
          )
        end

        it 'indicates the usb hub is not present' do
          subject.run(device, event)
          expect(device.details.usb_hub).to be nil
        end

        it 'removes the usb list' do
          subject.run(device, event)
          expect(device.details.usb_list).to be nil
        end

        it 'creates a device event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end
    end
  end
end
