# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot tunnel events' do
  describe Platforms::Buildroot::TunnelEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#ipsec' do
      context 'when a tunnel is established' do
        let(:message) { 'phase1 with 184.106.213.137 established.' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :ipsec) }

        it 'creates an event indicating the tunnel was established' do
          expect do
            subject.run(device, event)
          end.to change { device.events.tunnels.count }.by(1)
          expect(device.events.tunnels.first.info).to include('established')
        end

        it 'saves the tunnel status as part of the netbridge details' do
          subject.run(device, event)
          expect(device.details.tunnel_status).to eq 'Connected'
        end
      end

      context 'when a tunnel is destroyed' do
        let(:message) { 'phase1 with 184.106.213.137 down.' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :ipsec) }

        it 'creates an event indicating the tunnel is down' do
          expect do
            subject.run(device, event)
          end.to change { device.events.tunnels.count }.by(1)
          expect(device.events.tunnels.first.info).to include('is down')
        end

        it 'saves the tunnel status as part of the netbridge details' do
          subject.run(device, event)
          expect(device.details.tunnel_status).to eq 'Not Connected'
        end
      end

      context 'when a tunnel is attempting to reestablish' do
        let(:message) { 'phase1 with 184.106.213.137 died; attempting to rescue' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :ipsec) }

        it 'creates an event indicating an attempt to rescue the tunnel' do
          expect do
            subject.run(device, event)
          end.to change { device.events.tunnels.count }.by(1)
          expect(device.events.tunnels.first.info).to include('Attempting to reestablish')
        end

        it 'saves the tunnel status as part of the netbridge details' do
          subject.run(device, event)
          expect(device.details.tunnel_status).to eq 'Not Connected'
        end
      end

      context 'when a tunnel is rescued' do
        let(:message) { 'Rescued phase1 with 184.106.213.137.' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :ipsec) }

        it 'creates an event indicating the tunnel was rescued' do
          expect do
            subject.run(device, event)
          end.to change { device.events.tunnels.count }.by(1)
          expect(device.events.tunnels.first.info).to include('Rescued IPSec tunnel')
        end

        it 'saves the tunnel status as part of the netbridge details' do
          subject.run(device, event)
          expect(device.details.tunnel_status).to eq 'Connected'
        end
      end

      context 'when an unrecognized tunnel message is given' do
        let(:message) { 'unrecognized tunnel event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :ipsec) }

        it 'creates a generic tunnel event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.tunnels.count }.by(1)
        end
      end
    end
  end
end
