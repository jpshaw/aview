# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot power events' do
  describe Platforms::Buildroot::PowerEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#info' do
      let(:message) { 'No Remote Power device detected! Unable to send command.' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :info) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end

    describe '#power' do
      context 'when given the power outlet port status' do
        let(:message) { 'port1=OFF~port2=ON' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :power) }

        it 'creates power outlet records for the device' do
          expect do
            subject.run(device, event)
          end.to change { device.power_outlets.count }.from(0).to(2)
        end

        it 'sets the correct state of the power outlets' do
          subject.run(device, event)
          port_1 = device.power_outlets.find_by(port: 1)
          port_2 = device.power_outlets.find_by(port: 2)
          expect(port_1).to be_off
          expect(port_2).to be_on
        end

        it 'creates an event for each port' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(2)
        end

        context 'when an outlet record already exists' do
          before do
            @outlet = create(:power_outlet,
                                        device_id: device.id, the_state: :on, port: 1)
            expect(device.power_outlets.count).to be > 0
          end

          it 'updates the state' do
            expect do
              subject.run(device, event)
            end.to change { @outlet.reload.on? }.from(true).to(false)
          end
        end
      end

      context 'when no remote power device is detected' do
        let(:message) { 'no remote power device detected' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :power) }

        context 'when outlets exist for the device' do
          before do
            create(:power_outlet, device_id: device.id)
            expect(device.power_outlets.count).to be > 0
          end

          it 'creates an event indicating no power devices are detected' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }.by(1)
            expect(device.events.first.info).to include('No remote power devices detected')
          end

          it 'destroys the outlets' do
            expect do
              subject.run(device, event)
            end.to change { device.power_outlets.count }.from(1).to(0)
          end
        end
      end

      context 'when a port changes state' do
        let(:message) { 'Set outlet [1] to state [1] on Remote Power unit [2-2]' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :power) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
        end
      end
    end
  end
end
