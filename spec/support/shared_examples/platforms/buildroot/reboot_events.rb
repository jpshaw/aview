# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot reboot events' do
  describe Platforms::Buildroot::RebootEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#restart' do
      let(:message) { 'msg=unmanaged' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :restart) }

      it 'sets the restart timestamp for the device' do
        Timecop.freeze do
          time = Time.now.utc
          expect do
            subject.run(device, event)
          end.to change(device, :last_restarted_at).to(time)
        end
      end

      context 'when a restart reason is given' do
        it 'creates a restart event with the reason' do
          expect do
            subject.run(device, event)
          end.to change { device.events.reboots.count }.by(1)
          expect(device.events.reboots.last.info).to include('unmanaged')
        end
      end

      context 'when a restart reason is not given' do
        let(:event) { Platforms::Event.new(mac: device.mac, raw: '', type: :restart) }

        it 'creates a basic restart event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.reboots.count }.by(1)
        end
      end
    end
  end
end
