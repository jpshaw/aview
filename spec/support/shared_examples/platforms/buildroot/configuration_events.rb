# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'buildroot configuration events' do
  describe Platforms::Buildroot::ConfigurationEvents do
    subject { Platforms::Buildroot::SyslogProcessor }

    describe '#certificates' do
      let(:message) { 'Certificate URL changed. Generating new certificate.' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :certificates) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }.by(1)
      end
    end

    describe '#config' do
      context 'when a configuration is successfully loaded' do
        let(:message) { 'gip_secondary=1~img=1.542.01~img2=2.361.91~log=108.166.111.162~sticky_apn=0~var=A' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :config) }

        it 'creates a config event indicating a successful config load' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.first.info).to include('Configuration successfully loaded')
        end
      end

      context 'when there is an error with the firmware' do
        before do
          @failure_messages = [
            'No passphrase present in 2.120.42.tar',
            'Cannot decrypt passphrase in 2.120.42.tar',
            'Cannot decrypt package in 2.120.42.tar',
            'Corrupted file; cannot extract.',
            'Missing certificate; stopping.',
            'Missing signature; stopping.',
            'Missing package; stopping.',
            'Untrusted certificate; stopping.',
            'Package not signed by certificate owner; stopping.',
            'Mismatching public key; stopping.',
            'Could not decrypt 2.120.42.tar',
            'Could not verify authenticity of 2.120.42.tar'
          ]
        end

        it 'creates a config event indicating a firmware error' do
          @failure_messages.each do |message|
            event = Platforms::Event.new(mac: device.mac, raw: message, type: :config)
            expect do
              subject.run(device, event)
            end.to change { device.events.configs.count }.by(1)
            expect(device.events.configs.first.info).to include('Failed to upgrade firmware')
          end
        end
      end

      context 'when there is an error with certificates' do
        let(:message) { 'Certificate signing failed' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :config) }

        it 'creates a firmware event indicating a certificate error' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.firmwares.first.info).to include('SSL certificates')
        end
      end

      context 'when an unrecognized config message is given' do
        let(:message) { 'unrecognized config event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :config) }

        it 'creates a generic config event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
        end
      end
    end
  end
end
