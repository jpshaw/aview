# frozen_string_literal: true

shared_examples_for 'a gateway device' do
  include_examples 'acts as platform'
  include_examples 'gateway cellular events'
  include_examples 'gateway dial events'
  include_examples 'gateway network events'
  include_examples 'gateway power events'
  include_examples 'gateway rogue mac events'
  include_examples 'gateway status events'
  include_examples 'gateway tunnel events'
  include_examples 'gateway user events'
  include_examples 'gateway commands'
  include_examples 'gateway utilization events'
  include_examples 'gateway ping events'
end
