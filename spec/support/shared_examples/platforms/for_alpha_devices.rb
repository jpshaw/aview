# frozen_string_literal: true

shared_examples_for 'an alpha device' do
  include_examples 'acts as platform'
  include_examples 'alpha configuration events'
  include_examples 'alpha dhcp events'
  include_examples 'alpha firmware events'
  include_examples 'alpha network events'
  include_examples 'alpha reset events'
  include_examples 'alpha station query events'
  include_examples 'alpha status events'
  include_examples 'alpha time events'
  include_examples 'alpha vlan events'
end
