# frozen_string_literal: true

shared_examples_for 'uclinux modem status events' do
  describe Platforms::UcLinux::ModemEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    before do
      allow(Measurement::CellularUtilization).to receive(:save_sample)
    end

    describe '#modem_status' do
      context 'when given modem data' do
        let(:message) { 'idx=0~apn=vzwinternet~imei=359225050557818~imsi=311480017903663~phone=8136290627~iccid=89148000000176270115~provider=Verizon~carrier=Verizon~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.03 r22385 carmd-fwbuild1 2014/06/04 15:01:26~usb=480~sid=unknown~nid=unknown~sku=1102016~sim_slot=1' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :modem) }

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        it 'sets the modem as active' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:active) }.to(true)
        end

        it 'sets the modem name' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:name) }.to('SierraWirelessIncorporated MC7354')
        end

        it 'sets the correct carrier' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:carrier) }.to('Verizon')
        end

        it 'sets the SKU' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:sku) }.to('1102016')
        end

        it 'sets the SIM slot' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:sim_slot) }.to(1)
        end

        context 'when the device has a modem' do
          before do
            expect(device.modem).to be_blank
            @modem = device.modems.first_or_create(active: true)
          end

          it 'creates a second modem for the device' do
            subject.run(device, event)
            expect(device.modems.count).to eq(2)
          end

          it 'sets the new modem as active' do
            subject.run(device, event)
            expect(device.modems.find_by(iccid: '89148000000176270115').active).to eq(true)
          end

          it 'sets older modem as inactive' do
            expect(@modem.active).to eq(true)
            subject.run(device, event)
            expect(@modem.active).to eq(false)
          end
        end
      end

      context 'when given a modem status' do
        let(:message) { 'type=modem~idx=0~intf=wwan0~dbm=-65~per=78~cnti=lte~rx=9766833153~tx=7766833153~evdo_rssi=-125.00~evdo_ecio=-2.50~evdo_sinr=9.00~evdo_io=-106.00~lte_rssi=-65.00~rssi=-65.00~lte_rsrq=-11.00~rsrq=-11.00~lte_rsrp=-94.00~rsrp=-94.00~lte_snr=6.40~snr=6.40~bars=5' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        it 'saves the signal bars' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:bars) }.to(5)
        end

        context 'when the cnti does not change' do
          before do
            device.modems.where(active: true, cnti: 'lte').first_or_create
          end

          it 'creates a basic modem status event' do
            expect do
              subject.run(device, event)
            end.not_to change { device.modem.cnti }
            expected_event = device.events.where('information LIKE "%Modem status%"')
            expect(expected_event.count).to eq(1)
          end
        end

        context 'when the signal strength changes and falls below the set threshold' do
          before do
            allow_any_instance_of(DeviceModem).to receive(:signal_strength_threshold).and_return(90)
            device.modems.first_or_create
          end

          it 'generates an event indicating the signal change' do
            subject.run(device, event)
            expect(device.events.last.info).to include('Signal below threshold')
          end
        end
      end

      context 'when given a modem status with a newline character' do
        let(:message) { "type=modem~rx=1234~tx=5678\n" }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :modem) }

        it 'does not throw an exception' do
          expect { subject.run(device, event) }.not_to raise_error
        end
      end

      context 'when given a modem status, followed by a modem data, and another modem status' do
        let(:message1) { 'type=modem~idx=0~intf=wwan0~dbm=-65~per=78~cnti=lte~rx=9766833153~tx=7766833153~evdo_rssi=-125.00~evdo_ecio=-2.50~evdo_sinr=9.00~evdo_io=-106.00~lte_rssi=-65.00~rssi=-65.00~lte_rsrq=-11.00~rsrq=-11.00~lte_rsrp=-94.00~rsrp=-94.00~lte_snr=6.40~snr=6.40~bars=5' }
        let(:event1)   { Platforms::Event.new(mac: device.mac, raw: message1, type: :status) }

        let(:message2) { 'idx=0~apn=vzwinternet~imei=359225050557818~imsi=311480017903663~phone=8136290627~iccid=89148000000176270115~provider=Verizon~carrier=Verizon~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.03 r22385 carmd-fwbuild1 2014/06/04 15:01:26~usb=480~sid=unknown~nid=unknown~sku=1102016~sim_slot=1' }
        let(:event2)   { Platforms::Event.new(mac: device.mac, raw: message2, type: :modem) }

        let(:message3) { 'type=modem~idx=0~intf=wwan0~dbm=-65~per=78~cnti=lte~rx=9866933253~tx=7776833266~evdo_rssi=-125.00~evdo_ecio=-2.50~evdo_sinr=9.00~evdo_io=-106.00~lte_rssi=-65.00~rssi=-65.00~lte_rsrq=-11.00~rsrq=-11.00~lte_rsrp=-94.00~rsrp=-94.00~lte_snr=6.40~snr=6.40~bars=5' }
        let(:event3)   { Platforms::Event.new(mac: device.mac, raw: message3, type: :status) }

        it 'creates one modem' do
          expect(device.modem).to be_blank
          subject.run(device, event1)
          subject.run(device, event2)
          subject.run(device, event3)
          expect(device.modem).to be_present
        end

        context 'and a modem already exists' do
          before do
            expect(device.modem).to be_blank
            @modem = device.modems.first_or_create(active: true)
          end

          it 'creates a second modem from the modem data event' do
            expect(device.modems.find_by(iccid: '89148000000176270115')).not_to be_present
            subject.run(device, event1)
            subject.run(device, event2)
            subject.run(device, event3)
            expect(device.modems.find_by(iccid: '89148000000176270115')).to be_present
          end

          it 'marks the new modem from the modem data event as active' do
            expect(device.modems.find_by(iccid: '89148000000176270115')).not_to be_present
            subject.run(device, event1)
            subject.run(device, event2)
            subject.run(device, event3)
            expect(device.modems.find_by(iccid: '89148000000176270115').active).to eq(true)
          end

          it 'marks the old/original modem as inactive' do
            expect(@modem.active).to eq(true)
            subject.run(device, event1)
            subject.run(device, event2)
            subject.run(device, event3)
            expect(@modem.active).to eq(false)
          end

          context 'and given another modem data event, but for the original modem' do
            before do
              @modem.iccid = '8901260805749938054'
              @modem.save!
            end

            let(:message4) { 'idx=0~apn=fast.t-mobile.com~imei=359225050557818~imsi=311480017903663~phone=17326422900~iccid=8901260805749938054~provider=T-Mobile~carrier=T-Mobile~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.03 r22385 carmd-fwbuild1 2014/06/04 15:01:26~usb=480~sid=unknown~nid=unknown~sku=1102016~sim_slot=2' }
            let(:event4)   { Platforms::Event.new(mac: device.mac, raw: message4, type: :modem) }

            it 'creates a new modem and updates the original one' do
              expect(device.modems.find_by(iccid: '89148000000176270115')).not_to be_present
              expect(device.modems.find_by(iccid: '8901260805749938054').sim_slot).to eq(nil)
              subject.run(device, event1)
              subject.run(device, event2)
              subject.run(device, event3)
              subject.run(device, event4)
              expect(device.modems.count).to eq(2)
              expect(device.modems.find_by(iccid: '89148000000176270115')).to be_present
              expect(device.modems.find_by(iccid: '8901260805749938054').sim_slot).to eq(2)
            end

            it 'marks the new modem from the first modem data event as inactive' do
              expect(device.modems.find_by(iccid: '89148000000176270115')).not_to be_present
              subject.run(device, event1)
              subject.run(device, event2)
              subject.run(device, event3)
              subject.run(device, event4)
              expect(device.modems.find_by(iccid: '89148000000176270115').active).to eq(false)
            end

            it 'marks the original modem from the second modem data event as active' do
              subject.run(device, event1)
              subject.run(device, event2)
              subject.run(device, event3)
              subject.run(device, event4)
              expect(device.modems.find_by(iccid: '8901260805749938054').active).to eq(true)
            end
          end
        end
      end
    end
  end
end
