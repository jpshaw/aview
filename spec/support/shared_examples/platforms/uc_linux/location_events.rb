# frozen_string_literal: true

shared_examples_for 'uclinux location events' do
  describe Platforms::UcLinux::LocationEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    before do
      allow(Country).to receive(:find_by_code).and_return(Country.new)
    end

    describe '#location' do
      context 'when given geolocation data' do
        let(:message) { 'type=modem~idx=0~lat=28.337973~lon=-81.555043~alt=39.000000' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :location) }

        context 'when the device does not have a modem or cellular location' do
          before do
            expect(device.modem).not_to be_present
            CellularLocation.where(lat: 1.0, lon: 2.0).first_or_create # create random location entry to be sure we're not just getting lucky an fetching the only location record in the database
          end

          it 'creates a location with the given data' do
            expect do
              subject.run(device, event)
            end.to change(CellularLocation, :count).by(1)
            expect(device.cellular_location).to be_present
            expect(device.cellular_location.lat).to eq 28.338
            expect(device.cellular_location.lon).to eq -81.555
          end

          it 'creates an event for the new location' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Created device cellular location')
          end
        end

        context 'when the device has a location' do
          let(:location) { create(:cellular_location) }

          before do
            device.modems.first_or_create
            device.reload
            device.modem.cellular_location = location
            device.modem.save!
          end

          it 'creates a new location' do
            subject.run(device, event)
            expect(device.modem.cellular_location.id).not_to eq location.id
          end

          it 'updates the latitude and longitude of the location' do
            subject.run(device, event)
            expect(device.modem.cellular_location.lat).to eq 28.337973
            expect(device.modem.cellular_location.lon).to eq -81.555043
          end

          it 'creates an event indicating the location was updated' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('Created device cellular location with coordinates')
          end
        end
      end

      context 'when given cellular location data' do
        let(:message) { 'type=modem~idx=0~mcc=311~mnc=480~lac=65534~cid=52025601' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :location) }

        it 'creates an event for the modem location status' do
          VCR.use_cassette('geolocation from google') do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
          end
          expect(device.events.last.info).to include('Location status')
        end

        context 'when the device does not have a modem' do
          it 'creates a modem' do
            VCR.use_cassette('geolocation from google') do
              expect do
                subject.run(device, event)
              end.to change(DeviceModem, :count).by(1)
            end
          end
        end

        context 'when the device has a modem' do
          before do
            device.modems.first_or_create
          end

          it 'updates the modem' do
            VCR.use_cassette('geolocation from google') do
              subject.run(device, event)
            end
            expect(device.modem.mcc).to eq '311'
            expect(device.modem.mnc).to eq '480'
            expect(device.modem.lac).to eq '65534'
            expect(device.modem.cid).to eq '52025601'
          end
        end
      end
    end
  end
end
