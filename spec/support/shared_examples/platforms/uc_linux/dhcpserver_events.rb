# frozen_string_literal: true

shared_examples_for 'uclinux dhcpserver events' do
  describe Platforms::UcLinux::DhcpserverEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#dhcpserver' do
      context 'when the client device exists in the database' do
        let(:client_device) { create(:gateway_device, organization: device.organization) }

        context 'when a new lease is added' do
          let(:message) { "intf=eth0~passthrough=wwan0~mac=#{client_device.mac}~ip=10.63.218.160~action=add" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client device' do
            expect do
              subject.run(device, event)
            end.to associate_devices(device, client_device)
          end
        end

        context 'when a lease is listed' do
          let(:message) { "mac=#{client_device.mac}~ip=10.63.218.160~action=list~type=dynamic" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client device' do
            expect do
              subject.run(device, event)
            end.to associate_devices(device, client_device)
          end

          it 'marks the associated device with the appropriate lease type' do
            subject.run(device, event)
            expect(device.device_associations.first.lease_type).to eq 'dynamic'
          end
        end

        context 'when a client MAC is within the range of a known VPN Gateway device' do
          let(:message) { "mac=00#{(client_device.mac.hex + 2).to_s(16)}~ip=10.63.218.160~action=list" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client device' do
            expect do
              subject.run(device, event)
            end.to associate_devices(device, client_device)
          end
        end

        context 'when a lease is removed' do
          let(:message) { "intf=eth0~passthrough=wwan0~mac=#{client_device.mac}~ip=10.63.218.160~action=del" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          before { device.associate_with_device(client_device) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('DHCP lease expired for MAC')
          end

          it 'dissociates the device from the client device' do
            expect do
              subject.run(device, event)
            end.to dissociate_devices(device, client_device)
          end
        end

        context 'when an unknown action is specified in the message' do
          let(:message) { "intf=eth0~passthrough=wwan0~mac=#{client_device.mac}~ip=10.63.218.160~action=unknown" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('DHCP Server status')
          end

          it 'does not associate the device to the client device' do
            expect do
              subject.run(device, event)
            end.not_to associate_devices(device, client_device)
          end
        end
      end

      context 'when the client mac does not exist in the database' do
        let(:client_mac) { generate(:mac_address) }

        context 'when a new lease is added' do
          let(:message) { "intf=eth0~passthrough=wwan0~mac=#{client_mac}~ip=10.63.218.160~action=add" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client mac' do
            expect do
              subject.run(device, event)
            end.to associate_device_to_mac(device, client_mac)
          end
        end

        context 'when a lease is listed' do
          let(:message) { "mac=#{client_mac}~ip=10.63.218.160~action=list" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('Device handed IP')
          end

          it 'associates the device to the client mac' do
            expect do
              subject.run(device, event)
            end.to associate_device_to_mac(device, client_mac)
          end
        end

        context 'when a lease is removed' do
          let(:message) { "intf=eth0~passthrough=wwan0~mac=#{client_mac}~ip=10.63.218.160~action=del" }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }
          let(:second_client_mac) { generate(:mac_address) }

          before { device.associate_with_mac(client_mac) }

          it 'creates an event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.dhcps.count }.by(1)
            expect(device.events.last.info).to include('DHCP lease expired for MAC')
          end

          it 'dissociates the device from the client mac' do
            expect do
              subject.run(device, event)
            end.to dissociate_device_from_mac(device, client_mac)
          end

          it 'does not dissociate the device from other client macs' do
            device.associate_with_mac(second_client_mac)
            expect do
              subject.run(device, event)
            end.not_to dissociate_device_from_mac(device, second_client_mac)
          end
        end
      end

      context 'when the MAC address is not specified' do
        let(:message) { 'intf=eth0~passthrough=wwan0~ip=10.63.218.160~action=add' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcpserver) }

        it 'creates an event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.dhcps.count }.by(1)
          expect(device.events.last.info).to include('DHCP Server status')
        end

        it 'does not create a device association' do
          expect do
            subject.run(device, event)
          end.not_to change { device.device_associations.count }
        end
      end
    end
  end
end
