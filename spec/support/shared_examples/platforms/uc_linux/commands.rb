# frozen_string_literal: true

shared_examples_for 'uclinux commands' do
  describe '#send_command' do
    context 'with json commands enabled' do
      let(:command_stub) do
        Platforms::UcLinux::Command.new(
          device:   Device.new,
          command:  :a_command,
          user:     'user',
          password: 'password'
        )
      end

      before do
        allow(command_stub).to receive(:send!)
        allow(subject).to receive(:remote_control_disabled?).and_return(true)
      end

      it 'instantiates a Platforms::UcLinux::Command' do
        expect(Platforms::UcLinux::Command).to receive(:new).and_return(command_stub)
        subject.send_command({})
      end

      it 'sends the command' do
        allow(Platforms::UcLinux::Command).to receive(:new).and_return(command_stub)
        expect(command_stub).to receive(:send!)
        subject.send_command({})
      end
    end

    context 'with json commands disabled' do
      let(:command_stub) { AclCommand.new(Device.new, {}) }

      before do
        allow(command_stub).to receive(:send!)
        allow(subject).to receive(:remote_control_disabled?).and_return(false)
      end

      it 'instantiates a GatewayCommand' do
        expect(AclCommand).to receive(:new).and_return(command_stub)
        subject.send_command({})
      end

      it 'sends the command' do
        allow(AclCommand).to receive(:new).and_return(command_stub)
        expect(command_stub).to receive(:send!)
        subject.send_command({})
      end
    end
  end
end
