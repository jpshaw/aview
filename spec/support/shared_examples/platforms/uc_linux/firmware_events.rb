# frozen_string_literal: true

shared_examples_for 'uclinux firmware events' do
  describe Platforms::UcLinux::FirmwareEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#firmware' do
      context 'when given firmware data' do
        let(:message) { "version=15.3.55~serial=#{test_serial}" }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        context 'when the firmware changes' do
          it 'updates the device firmware' do
            expect do
              subject.run(device, event)
            end.to change(device, :firmware).to('15.3.55')
          end

          it 'generates an event indicating the firmware change' do
            expect do
              subject.run(device, event)
            end.to change { device.events.firmwares.count }.by(1)
            expect(device.events.last.info).to include('15.3.55')
          end
        end

        context 'when the firmware does not change' do
          before do
            device.update_attribute(:firmware, '15.3.55')
          end

          it 'does not update the device firmware' do
            expect do
              subject.run(device, event)
            end.not_to change(device, :firmware)
          end

          it 'generates a basic firmware event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.firmwares.count }.by(1)
            expect(device.events.last.info).to include('using firmware version')
          end
        end

        it 'sets the device serial' do
          expect do
            subject.run(device, event)
          end.to change(device, :serial).to(test_serial)
        end
      end

      context 'when a firmware download fails' do
        let(:message) { 'Failed to download firmware configuration.accns.com/device_firmware/6300-CX/15.5.1: 404' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates an event indicating the firmware download failed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Firmware download failed')
        end
      end

      context 'when the firmware is up to date' do
        let(:message) { 'Firmare is up to date (15.3.55).' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates an event indicating the firmware is up to date' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to eq 'Firmare is up to date'
        end
      end

      context 'when a flash update fails' do
        let(:message) { 'Flash update fail: 1: Insufficient memory for image!' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'generates an event with the message' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to include('Insufficient memory for image')
        end
      end

      context 'when given an unrecognized firmware message' do
        let(:message) { 'unrecognized event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :firmware) }

        it 'creates a generic firmware event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.firmwares.count }.by(1)
          expect(device.events.last.info).to eq 'Firmware status'
        end
      end
    end
  end
end
