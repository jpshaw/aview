# frozen_string_literal: true

shared_examples_for 'uclinux status events' do
  describe Platforms::UcLinux::StatusEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#hearbeat' do
      context 'when given a heartbeat' do
        let(:message) { '' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :heartbeat) }

        it 'generates a heartbeat event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Heartbeat')
        end
      end
    end

    describe '#status' do
      context 'when given an ethernet status' do
        let(:message) { 'type=ethernet~intf=eth0~rx=1046~tx=4298' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        context 'when the device has no networks' do
          before do
            expect(device.networks.count).to eq 0
          end

          it 'creates and associates with an ethernet network' do
            expect do
              subject.run(device, event)
            end.to change { device.networks.count }.by(1)
            expect(device.networks.last.net_type).to eq 'ethernet'
          end
        end

        context 'when the device has a network for the interface' do
          before do
            device.networks.create(net_type: 'ethernet', network_iface: 'eth0')
          end

          it 'does not create another network' do
            expect do
              subject.run(device, event)
            end.not_to change { device.networks.count }
          end

          it 'updates the existing network' do
            expect do
              subject.run(device, event)
            end.to change { device.networks.first.rx }.from(nil).to(1046)
          end
        end
      end

      context 'when given an unrecognized status' do
        let(:message) { 'unrecognized status event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        it 'creates a basic status event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.modems.count }.by(1)
          expect(device.events.last.info).to include('Device status')
        end
      end
    end
  end
end
