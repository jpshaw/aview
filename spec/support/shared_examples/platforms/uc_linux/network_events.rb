# frozen_string_literal: true

shared_examples_for 'uclinux network events' do
  describe Platforms::UcLinux::NetworkEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#dhcp' do
      context 'when given a dhcp message' do
        let(:message) { 'pppl=100.64.161.76~dns1=198.224.181.135~dns2=198.224.178.135' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :dhcp) }

        it 'creates a basic network event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Network status')
        end
      end
    end

    describe '#network' do
      context 'when given a primary ip with `ip0` and priority with `default`' do
        let(:host)    { '172.16.0.183' }
        let(:message) { "type=modem~idx=0~intf=wwan0~mtu=1430~mtu_ipv6=1430~default=pri~ip0=#{host}/24~gw0=100.72.211.89~metric0=3~default_ipv6=pri~ip1=~gw1=fe80::b857:cdc:f063:9d69~metric1=512~ip2=2600:1006:b01b:2183:b89b:33ff:fed0:a0b6/64" }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        context 'when the host changes' do
          before do
            expect(device.host).to be nil
          end

          it 'creates an event indicating the host has changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Cellular WAN IP changed')
          end

          it 'sets the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(nil).to(host)
          end

          it 'updates the dns record for the device' do
            subject.run(device, event)
            expect(PowerDnsRecordManager::Record.last.content).to eq host
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, host)
          end

          it 'creates a basic dhcp event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('Network status')
          end
        end
      end

      context 'when given a primary ip with `ip0` and priority with `priority`' do
        let(:host)    { '172.16.0.183' }
        let(:message) { "idx=0~type=modem~=~gw0=10.79.216.58~intf=wwan0~ip0=#{host}~link=~metric0=5~mtu=1430~mtu_ipv6=1430~priority=pri~priority_ipv6=~speed=" }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        before do
          expect(device.host).to be nil
        end

        context 'when the host changes' do
          it 'creates an event indicating the host has changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Cellular WAN IP changed')
          end

          it 'sets the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(nil).to(host)
          end

          it 'updates the dns record for the device' do
            subject.run(device, event)
            expect(PowerDnsRecordManager::Record.last.content).to eq host
          end
        end
      end

      context 'when given a primary IPv4 address with `passthrough`' do
        let(:message) { 'default=pri~passthrough=10.61.222.81' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        context 'when the host changes' do
          before do
            expect(device.host).to be nil
          end

          it 'creates an event indicating the host has changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Cellular WAN IP changed')
          end

          it 'sets the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(nil).to('10.61.222.81')
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, '10.61.222.81')
          end

          it 'creates a basic dhcp event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('Network status')
          end
        end
      end

      context 'when given a primary mgmt IP address' do
        let(:message) { 'cnt=3~type=mgmt~default=pri~ip0=192.168.64.99~name0=ipsec-tunnel-aview' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'sets the device mgmt_host but not the host' do
          expect do
            subject.run(device, event)
          end.to change(device, :mgmt_host).from(nil).to('192.168.64.99')
          expect(device.host).to be nil
        end
      end

      context 'when given a tunnel IP address' do
        let(:message) { 'cnt=0~aview=purge~type=ipsec~tunnel=aview~ip_local=10.251.3.3~ip_remote=128.136.167.111~ipsec=up~ipsec0=up~net_local0=192.168.64.101/32~net_remote0=192.168.211.50/32' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'sets the device mgmt_host but not the host' do
          expect do
            subject.run(device, event)
          end.to change(device, :mgmt_host).from(nil).to('192.168.64.101')
          expect(device.host).to be nil
        end
      end

      context 'when given a primary IPv6 address with `passthrough`' do
        let(:message) { 'default_ipv6=pri~passthrough_ipv6=2600:1006:b017:c8b2:b440:8aff:feb7:ba69' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        context 'when the host changes' do
          before do
            expect(device.host).to be nil
          end

          it 'creates an event indicating the host has changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.first.info).to include('Cellular WAN IP changed')
          end

          it 'sets the device host' do
            expect do
              subject.run(device, event)
            end.to change(device, :host).from(nil).to('2600:1006:B017:C8B2:B440:8AFF:FEB7:BA69')
          end
        end

        context 'when the host does not change' do
          before do
            device.update_attribute(:host, '2600:1006:b017:c8b2:b440:8aff:feb7:ba69')
          end

          it 'creates a basic dhcp event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('Network status')
          end
        end
      end

      context 'when given mtu values' do
        let(:message) { 'mtu=1400~mtu_ipv6=1500' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'saves the mtu values as part of the network record' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.networks.last.mtu).to eq '1400'
          expect(device.networks.last.mtu_ipv6).to eq '1500'
        end
      end

      context 'when the device has no networks' do
        let(:message) { 'type=ethernet~intf=eth0~mtu=1500~link=ok~speed=100baseTx-FD,~rx=48899004~tx=603808~metric0=5' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        before do
          expect(device.networks.count).to eq 0
        end

        it 'creates a network' do
          expect do
            subject.run(device, event)
          end.to change { device.networks.count }.by(1)
        end
      end

      context 'when given a new primary wan interface' do
        let(:message) { 'type=ethernet~intf=eth0~default=pri' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        before do
          expect(device.primary_wan_iface).not_to eq 'eth0'
        end

        it 'updates the device wan interface' do
          expect do
            subject.run(device, event)
          end.to change(device, :primary_wan_iface).to 'eth0'
        end

        it 'creates an event indicating the primary interface has changed' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.first.info).to include('Primary WAN interface changed')
        end
      end

      context 'when the primary interface does not change' do
        let(:message) { 'type=ethernet~intf=eth0~default=pri' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        before do
          device.update_attribute(:primary_wan_iface, 'eth0')
        end

        it 'creates a network event for the interface' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.last.info).to include('Network status for eth0')
        end
      end

      context 'when the aview tunnel was connected, and is reconnecting' do
        let(:message) { 'aview=purge~type=ipsec~tunnel=aview~ipsec=connecting~ipsec0=connecting' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        before do
          device.networks.where(net_type: 'ipsec', network_iface: 'aview').first_or_create
        end

        it 'creates an event stating the tunnel is connecting' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.last.info).to include('aview tunnel connecting')
        end

        it 'saves the tunnel name as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.network_iface).to eq 'aview'
        end

        it 'saves the tunnel status as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.status).to eq 'connecting'
        end
      end

      context 'when the aview tunnel is brought up' do
        let(:message) { 'aview=purge~type=ipsec~tunnel=aview~if_stats=/proc/net/dev/wwan0~ipsec=up~isakmp-sa=unchanged~ipsec-sa=up' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'creates an event stating the tunnel is established' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.last.info).to include('aview tunnel established')
        end

        it 'saves the tunnel name as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.network_iface).to eq 'aview'
        end

        it 'saves the tunnel status as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.status).to eq 'Connected'
        end
      end

      context 'context when another tunnel already exists for the device' do
        let(:old_tunnel) { 'type=ipsec~tunnel=old~if_stats=/proc/net/dev/wwan0~ipsec=up~isakmp-sa=unchanged~ipsec-sa=up' }
        let(:new_tunnel) { 'aview=purge~type=ipsec~tunnel=aview~if_stats=/proc/net/dev/wwan0~ipsec=up~isakmp-sa=unchanged~ipsec-sa=up' }
        let(:event1)   { Platforms::Event.new(mac: device.mac, raw: old_tunnel, type: :network) }
        let(:event2)   { Platforms::Event.new(mac: device.mac, raw: new_tunnel, type: :network) }

        it 'destroys old tunnel and creates new tunnel' do
          expect(device.networks.count).to eq 0
          subject.run(device, event1)
          expect(device.networks.count).to eq 1
          expect(device.networks.last.network_iface).to eq 'old'
          subject.run(device, event2)
          expect(device.networks.count).to eq 1
          expect(device.networks.last.network_iface).to eq 'aview'
        end
      end

      context 'when the aview tunnel is brought down' do
        let(:message) { 'aview=purge~type=ipsec~tunnel=aview~if_stats=/proc/net/dev/wwan0~ipsec=down~isakmp-sa=down~ipsec-sa=up' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'creates an event stating the tunnel is disconnected' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.last.info).to include('aview tunnel disconnected')
        end

        it 'saves the tunnel name as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.network_iface).to eq 'aview'
        end

        it 'saves the tunnel status as part of the network record' do
          subject.run(device, event)
          expect(device.networks.last.status).to eq 'Not Connected'
        end
      end

      context 'when IP addresses are provided for a tunnel' do
        let(:message) { 'aview=purge~network: type=ipsec~tunnel=aview~ip_local=172.27.183.58~ip_remote=184.106.213.137~net_local=172.27.183.58/32~net_remote=192.168.211.50/32~if_stats=/proc/net/dev/lo~ipsec=up~isakmp-sa=up~ipsec-sa=up~nfmark-me/him=0x0/0xffff0001' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'saves the tunnel IP addresses as part of the DeviceNetwork record' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.networks.last.ip_addrs[:ip_local]).to eq '172.27.183.58'
          expect(device.networks.last.ip_addrs[:ip_remote]).to eq '184.106.213.137'
          expect(device.networks.last.ip_addrs[:net_local]).to eq '172.27.183.58/32'
          expect(device.networks.last.ip_addrs[:net_remote]).to eq '192.168.211.50/32'
        end
      end

      context 'when no interface is given' do
        let(:message) { 'type=ethernet~' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'creates a single generic network event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.last.info).to include('Network status')
        end
      end

      context 'when the given DNS details' do
        let(:message) { 'type=dns~dns0=198.224.154.135~dns1=198.224.152.119' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        context 'when the DNS values change' do
          before do
            device.update(dns1: '8.8.8.8', dns2: '4.2.2.2')
          end

          it 'creates an event stating the DNS values changed' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('DNS updated to 198.224.154.135, 198.224.152.119')
          end
        end

        context 'when the DNS remain the same' do
          before do
            device.update(dns1: '198.224.154.135', dns2: '198.224.152.119')
          end

          it 'creates a generic DNS status event' do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('DNS status')
          end
        end

        it 'does not create a network record' do
          expect do
            subject.run(device, event)
          end.to change { device.networks.count }.by(0)
        end
      end

      context 'when the tx and rx values are large' do
        let(:message) { 'type=ethernet~intf=eth0~rx=9766833153~tx=7766833153' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

        it 'saves the tx and rx values as part of the network record' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.networks.first.rx).to eq 9_766_833_153
          expect(device.networks.first.tx).to eq 7_766_833_153
        end
      end

      context 'for ethernet ports' do
        context 'when a new ethernet network is created' do
          let(:message) { 'type=ethernet~intf=eth0~mtu=1500~link=ok~rx=161154~tx=2821~default=~ip0=192.168.210.1/24~ip1=192.168.1.1/24' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

          before do
            expect(device.networks.count).to eq 0
          end

          it 'does not generate a `connected` event' do
            subject.run(device, event)
            device.events.each do |event|
              expect(event.info).not_to include('connected')
            end
          end
        end

        context 'when an existing ethernet port goes from `down` to `up`' do
          let(:message) { 'type=ethernet~intf=eth0~mtu=1500~link=ok~rx=161154~tx=2821~default=~ip0=192.168.210.1/24~ip1=192.168.1.1/24' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

          before do
            device.networks.create(network_iface: 'eth0', status: Platforms::UcLinux::Network::NOT_CONNECTED)
          end

          it 'creates an event indicating the ethernet port is connected' do
            subject.run(device, event)
            expect(device.events.last.info).to include("'eth0' connected")
          end
        end

        context 'when an existing ethernet port goes from `up` to `down`' do
          let(:message) { 'type=ethernet~intf=eth0~mtu=1500~link=no~rx=161154~tx=2821~default=~ip0=192.168.210.1/24~ip1=192.168.1.1/24' }
          let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :network) }

          before do
            device.networks.create(network_iface: 'eth0', status: Platforms::UcLinux::Network::CONNECTED)
          end

          it 'creates an event indicating the ethernet port is disconnected' do
            subject.run(device, event)
            expect(device.events.last.info).to include("'eth0' disconnected")
          end
        end
      end
    end
  end
end
