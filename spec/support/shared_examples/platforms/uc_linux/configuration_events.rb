# frozen_string_literal: true

shared_examples_for 'uclinux configuration events' do
  describe Platforms::UcLinux::ConfigurationEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#configuration' do
      context 'a configuration value is updated' do
        let(:message) { '(vpn.ipsec.aview.remote.hostname) changing from (remotecontrol.accns.com) to (remote.accns.com)' }
        let(:event)   do
          Platforms::Event.new(mac: device.mac, raw: message,
                               type: :config, severity: EventLevels::NOTICE)
        end

        it 'creates an event indicating the configuration value was updated' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.last.info).to eq 'vpn.ipsec.aview.remote.hostname updated to remote.accns.com'
        end
      end

      context 'a configuration count is updated' do
        let(:message) { '(firewall.filter) count changing from (1) to (2)' }
        let(:event)   do
          Platforms::Event.new(mac: device.mac, raw: message,
                               type: :config, severity: EventLevels::NOTICE)
        end

        it 'creates an event indicating the configuration count was updated' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.last.info).to eq 'firewall.filter count updated to 2'
        end
      end

      context 'a device experienced an error when getting/loading its configuration' do
        let(:message) { 'unable to load config' }
        let(:event)   do
          Platforms::Event.new(mac: device.mac, raw: message,
                               type: :config, severity: EventLevels::WARNING)
        end

        it 'generates an event with and elevated alert level' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.last.info).to eq message
          expect(device.events.configs.last.level).to eq EventLevels::ERROR
        end
      end

      context 'a device finishes loading its configuration' do
        let(:message) { 'Config download/processing complete.' }
        let(:event)   do
          Platforms::Event.new(mac: device.mac, raw: message,
                               type: :config, severity: EventLevels::NOTICE)
        end

        it 'creates an event indicating the configuration is loaded' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.last.info).to eq message
        end
      end

      context 'when given an unrecognized config message' do
        let(:message) { 'unrecognized event' }
        let(:event)   do
          Platforms::Event.new(mac: device.mac, raw: message,
                               type: :config, severity: EventLevels::INFO)
        end

        it 'creates a generic config event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.configs.count }.by(1)
          expect(device.events.configs.last.info).to eq message
        end
      end
    end
  end
end
