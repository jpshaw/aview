# frozen_string_literal: true

shared_examples_for 'uclinux reboot events' do
  describe Platforms::UcLinux::RebootEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#reboot' do
      let(:message) { 'msg=Remote restart~cmd=reboot' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :reboot) }

      it 'creates an event indicating the device is attempting to reboot' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }
        expect(device.events.first.info).to include('attempting to reboot')
      end
    end

    describe '#restart' do
      it 'updates the last restart timestamp' do
        event = Platforms::Event.new(mac: device.mac,
                                     raw: 'msg=Remote restart',
                                     type: :restart)
        Timecop.freeze do
          time = Time.now.utc
          expect do
            subject.run(device, event)
          end.to change(device, :last_restarted_at).to(time)
        end
      end

      context 'when given just a message' do
        let(:message) { 'msg=Remote restart' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :restart) }

        it 'creates a reboot event indicating the reason the device rebooted' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.first.info).to include('Remote restart')
        end
      end

      context 'when given a message and params' do
        let(:message) { 'msg=Modem firmware update~carrier=ATT~cmd=hardreboot' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :restart) }

        it 'creates a reboot event indicating the reason the device rebooted' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }
          expect(device.events.first.info).to include('Modem firmware update')
        end
      end
    end
  end
end
