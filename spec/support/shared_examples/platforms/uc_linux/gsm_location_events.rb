# frozen_string_literal: true

shared_examples_for 'uclinux gsm location events' do
  describe Platforms::UcLinux::LocationEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#location' do
      context 'when given cellular location data' do
        let(:message) { 'type=modem~idx=0~mcc=311~mnc=480~lac=65534~cid=52025601' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :location) }

        it 'creates an event for the modem location status' do
          VCR.use_cassette('geolocation from google') do
            expect do
              subject.run(device, event)
            end.to change { device.events.count }
            expect(device.events.last.info).to include('Location status')
          end
        end

        context 'when the device does not have a modem' do
          it 'creates a modem' do
            VCR.use_cassette('geolocation from google') do
              expect do
                subject.run(device, event)
              end.to change(DeviceModem, :count).by(1)
            end
          end
        end

        context 'when the device has a modem' do
          before do
            device.modems.first_or_create
          end

          it 'updates the modem' do
            VCR.use_cassette('geolocation from google') do
              subject.run(device, event)
            end
            expect(device.modem.mcc).to eq '311'
            expect(device.modem.mnc).to eq '480'
            expect(device.modem.lac).to eq '65534'
            expect(device.modem.cid).to eq '52025601'
          end
        end
      end
    end
  end
end
