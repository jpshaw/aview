# frozen_string_literal: true

shared_examples_for 'uclinux test events' do
  describe Platforms::UcLinux::TestEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#speed' do
      let(:message) { 'tx_avg=0.9590Mbps~tx_latency=543.46ms~rx_avg=1.4607Mbps~rx_latency=542.59ms' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :speed) }

      it 'creates an event indicating speed results were given' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }
        expect(device.events.first.info).to include('Speed Test Results')
      end

      context 'if the active WAN interface is not a cellular modem' do
        before do
          device.modems.first_or_create(active: true)
          device.update_attribute(:primary_wan_iface, 'eth0')
        end

        it 'does not create a speed test record for the device' do
          subject.run(device, event)
          expect(device.modem.speed_tests).to be_blank
        end
      end

      context 'if the active WAN interface is a cellular modem' do
        before do
          device.modems.first_or_create(active: true)
          device.update_attribute(:primary_wan_iface, 'wwan0')
        end

        it 'creates a speed test record for the device modem' do
          expect(device.modem.speed_tests).to be_blank
          subject.run(device, event)
          expect(device.modem.speed_tests).to be_present
        end

        it 'keeps the speed test record limited to five per modem, including the latest speed test' do
          expect(device.modem.speed_tests).to be_blank
          6.times do
            device.modem.speed_tests.create
          end
          subject.run(device, event)
          expect(device.modem.speed_tests.count).to eq 5
          expect(device.modem.speed_tests.last.latency_units).to eq 'ms'
          expect(device.modem.speed_tests.last.rx_avg).to eq 1.4607
        end
      end
    end
  end
end
