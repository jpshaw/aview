# frozen_string_literal: true

shared_examples_for 'uclinux modem events' do
  describe Platforms::UcLinux::ModemEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#modem' do
      context 'when given modem data' do
        let(:message) { 'idx=0~apn=vzwinternet~imei=359225050557818~imsi=311480017903663~phone=8136290627~iccid=89148000000176270115~provider=Verizon~carrier=Verizon~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.03 r22385 carmd-fwbuild1 2014/06/04 15:01:26~usb=480~sid=unknown~nid=unknown~sku=1102016' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :modem) }

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        it 'sets the modem name' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:name) }.to('SierraWirelessIncorporated MC7354')
        end

        it 'sets the correct carrier' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:carrier) }.to('Verizon')
        end

        it 'sets the SKU' do
          expect do
            subject.run(device, event)
          end.to change { device.modem.try(:sku) }.to('1102016')
        end
      end

      context 'when given an unrecognized modem event' do
        let(:message) { 'unrecognized modem event' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :modem) }

        it 'creates a basic modem event' do
          expect do
            subject.run(device, event)
          end.to change { device.events.modems.count }.by(1)
          expect(device.events.last.info).to include('Modem status')
        end
      end
    end
  end
end
