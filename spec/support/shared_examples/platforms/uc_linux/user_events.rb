# frozen_string_literal: true

shared_examples_for 'uclinux user events' do
  describe Platforms::UcLinux::UserEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#user' do
      let(:message) { 'name=root~service=sshd~state=opened~remote=172.16.3.149~tty=ssh' }
      let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :user) }

      it 'creates an event' do
        expect do
          subject.run(device, event)
        end.to change { device.events.count }
      end
    end
  end
end
