# frozen_string_literal: true

shared_examples_for 'uclinux port signal events' do
  describe Platforms::UcLinux::PortSignalEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    describe '#port_signal' do
      context 'when given a serial port status' do
        let(:message) { 'type=signal~device=port1~cts=1~dcd=1' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :serial) }

        it 'creates an event indicating the serial port status' do
          expect do
            subject.run(device, event)
          end.to change { device.events.count }.by(1)
          expect(device.events.first.info).to include('Serial Port 1')
        end
      end

      context 'when a pin is unplugged' do
        # NOTE: cts is set to '0', indicating the pin is unplugged
        let(:message) { 'type=signal~device=port1~cts=0~dcd=1' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :serial) }

        context 'when the pin is monitored' do
          let(:configuration) do
            create(:device_configuration_with_monitored_serial,
                              organization: device.organization)
          end

          before do
            expect(configuration.to_h[:serial][:port1][:monitor][:cts]).to be true
            device.update_attribute(:configuration_id, configuration.id)
          end

          it 'attempts to set the uuid of the event' do
            expect(device).to receive(:uuid_for).with(:port_down)
            subject.run(device, event)
          end
        end

        context 'when the pin is not monitored' do
          let(:configuration) do
            create(:device_configuration_with_unmonitored_serial,
                              organization: device.organization)
          end

          before do
            expect(configuration.to_h[:serial][:port1][:monitor]).to be nil
            device.update_attribute(:configuration_id, configuration.id)
          end

          it 'does not attempt to set the uuid of the event' do
            expect(device).not_to receive(:uuid_for).with(:port_down)
            subject.run(device, event)
          end
        end
      end
    end
  end
end
