# frozen_string_literal: true

shared_examples_for 'uclinux status events for modem' do
  describe Platforms::UcLinux::StatusEvents do
    subject { Platforms::UcLinux::SyslogProcessor }

    before do
      allow(Measurement::CellularUtilization).to receive(:save_sample)
    end

    describe '#status' do
      context 'when given a modem status' do
        let(:message) { 'type=modem~idx=0~intf=wwan0~dbm=-65~per=78~cnti=lte~rx=55393875~tx=8402216~evdo_rssi=-125.00~evdo_ecio=-2.50~evdo_sinr=9.00~evdo_io=-106.00~lte_rssi=-65.00~rssi=-65.00~lte_rsrq=-11.00~rsrq=-11.00~lte_rsrp=-94.00~rsrp=-94.00~lte_snr=6.40~snr=6.40' }
        let(:event)   { Platforms::Event.new(mac: device.mac, raw: message, type: :status) }

        context 'when the device has no networks' do
          before do
            expect(device.networks.count).to eq 0
          end

          it 'creates and associates with an modem network' do
            expect do
              subject.run(device, event)
            end.to change { device.networks.count }.by(1)
            expect(device.networks.last.net_type).to eq 'modem'
          end
        end

        context 'when the device has a network for the interface' do
          before do
            device.networks.create(net_type: 'modem', network_iface: 'wwan0')
          end

          it 'does not create another network' do
            expect do
              subject.run(device, event)
            end.not_to change { device.networks.count }
          end

          it 'updates the existing network' do
            expect do
              subject.run(device, event)
            end.to change { device.networks.first.rx }.from(nil).to(55_393_875)
          end
        end

        it 'creates a modem' do
          expect(device.modem).to be_blank
          subject.run(device, event)
          expect(device.modem).to be_present
        end

        context 'when the cnti does not change' do
          before do
            device.modems.where(active: true, cnti: 'lte').first_or_create
          end

          it 'creates a basic modem status event' do
            expect do
              subject.run(device, event)
            end.not_to change { device.modem.cnti }
            expect(device.events.first.info).to include('Modem status')
          end
        end

        context 'when the signal strength changes and falls below the set threshold' do
          before do
            allow_any_instance_of(DeviceModem).to receive(:signal_strength_threshold).and_return(90)
            device.modems.first_or_create
          end

          it 'generates an event indicating the signal change' do
            subject.run(device, event)
            expect(device.events.last.info).to include('Signal below threshold')
          end
        end
      end
    end
  end
end
