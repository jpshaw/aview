# frozen_string_literal: true

shared_examples_for 'a uclinux device with modem' do
  include_examples 'acts as platform'
  include_examples 'uclinux gsm location events'
  include_examples 'uclinux modem events'
  include_examples 'uclinux modem status events'
  include_examples 'uclinux status events for modem'
end
