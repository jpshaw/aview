# frozen_string_literal: true

shared_examples_for 'a device configuration modifier' do
  it 'grants access if user has ability to modify the device configuration' do
    allow(user).to receive(:has_ability_on?).with(device_configuration.organization, to: 'Modify Device Configurations').and_return(true)
    expect(subject).to permit(user, device_configuration)
  end

  it 'denies access if user does not have ability to modify the device configuration' do
    allow(user).to receive(:has_ability_on?).with(device_configuration.organization, to: 'Modify Device Configurations').and_return(false)
    expect(subject).not_to permit(user, device_configuration)
  end
end
