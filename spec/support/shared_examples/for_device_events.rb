# frozen_string_literal: true

shared_examples_for 'a management host updater' do
  it 'updates the management host' do
    expect do
      @device.process(@inform)
    end.to change(@device, :mgmt_host).from(nil).to(@management_host)
  end

  it 'adds a dns entry' do
    expect do
      @device.process(@inform)
    end.to change(PowerDnsRecordManager::Record, :count).by(1)
  end
end
