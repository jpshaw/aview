# frozen_string_literal: true

# IMPORTANT: Any subclass of device that includes the ConfigurationMonitorable concern should remove
#           these tests, since the same methods exist in ConfigurationMonitorable and that would make
#           these tests obsolete for the device subclass and they would also fail.

shared_examples_for 'a monitorable device' do
  describe '#heartbeat_frequency' do
    it 'retrieves the default status frequency value' do
      expect(subject).to receive(:default_status_freq).and_return('1m')
      subject.heartbeat_frequency
    end

    it 'uses the DurationCalculator to convert a value to seconds' do
      expect(subject).to receive(:default_status_freq).and_return('1m')
      expect(DurationCalculator).to receive(:calculate)
      subject.heartbeat_frequency
    end

    it 'returns a duration value converted in seconds' do
      %w[1m 2m].each do |duration|
        allow(subject).to receive(:default_status_freq).and_return(duration)
        expect(subject.heartbeat_frequency).to eq(DurationCalculator.calculate(duration))
      end
    end
  end

  describe '#grace_period' do
    it 'retrieves the default grace period duration value' do
      expect(subject).to receive(:default_grace_period_duration).and_return('1m')
      subject.grace_period
    end

    it 'uses the DurationCalculator to convert a value to seconds' do
      expect(subject).to receive(:default_grace_period_duration).and_return('1m')
      expect(DurationCalculator).to receive(:calculate)
      subject.grace_period
    end

    it 'returns a duration value converted in seconds' do
      %w[1m 2m].each do |duration|
        allow(subject).to receive(:default_grace_period_duration).and_return(duration)
        expect(subject.grace_period).to eq(DurationCalculator.calculate(duration))
      end
    end
  end
end
