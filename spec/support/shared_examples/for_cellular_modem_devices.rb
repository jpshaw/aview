# frozen_string_literal: true

shared_examples_for 'it has a cellular modem' do
  let(:model) { described_class }

  describe 'scopes' do
    describe '.sort_by_modem_carrier' do
      before do
        @device_1 = create("#{model.to_s.underscore}_device".to_sym, :att_modem_carrier)
        @device_2 = create("#{model.to_s.underscore}_device".to_sym, :verizon_modem_carrier, series: @device_1.series)
        @device_3 = create("#{model.to_s.underscore}_device".to_sym, series: @device_1.series)
      end

      context 'when sorted ascending' do
        let(:result) { model.sort_by_modem_carrier 'asc' }

        it 'sorts null modem carrier names first and higher modem carrier names last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_1
          expect(result.last).to eq @device_2
        end
      end

      context 'when sorted descending' do
        let(:result) { model.sort_by_modem_carrier 'desc' }

        it 'sorts higher modem carrier names first and null modem carrier names last' do
          expect(result.first).to eq @device_2
          expect(result.second).to eq @device_1
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_modem_signal' do
      before do
        @device_1 = create("#{model.to_s.underscore}_device".to_sym, :strong_modem_signal)
        @device_2 = create("#{model.to_s.underscore}_device".to_sym, :weak_modem_signal, series: @device_1.series)
        @device_3 = create("#{model.to_s.underscore}_device".to_sym, series: @device_1.series)
      end

      context 'when sorted ascending' do
        let(:result) { model.sort_by_modem_signal 'asc' }

        it 'sorts null modem signals first and higher modem signals last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { model.sort_by_modem_signal 'desc' }

        it 'sorts higher modem signals first and null modem signals last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_modem_cnti' do
      before do
        @device_1 = create("#{model.to_s.underscore}_device".to_sym, :lte_modem_cnti)
        @device_2 = create("#{model.to_s.underscore}_device".to_sym, :umts_modem_cnti, series: @device_1.series)
        @device_3 = create("#{model.to_s.underscore}_device".to_sym, series: @device_1.series)
      end

      context 'when sorted ascending' do
        let(:result) { model.sort_by_modem_cnti 'asc' }

        it 'sorts null modem cnti first and higher modem cnti last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_1
          expect(result.last).to eq @device_2
        end
      end

      context 'when sorted descending' do
        let(:result) { model.sort_by_modem_cnti 'desc' }

        it 'sorts higher modem cnti first and null modem cnti last' do
          expect(result.first).to eq @device_2
          expect(result.second).to eq @device_1
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_modem_number' do
      before do
        @device_1 = create("#{model.to_s.underscore}_device".to_sym, :high_modem_number)
        @device_2 = create("#{model.to_s.underscore}_device".to_sym, :low_modem_number, series: @device_1.series)
        @device_3 = create("#{model.to_s.underscore}_device".to_sym, series: @device_1.series)
      end

      context 'when sorted ascending' do
        let(:result) { model.sort_by_modem_number 'asc' }

        it 'sorts null modem numbers first and higher modem numbers last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { model.sort_by_modem_number 'desc' }

        it 'sorts higher modem numbers first and null modem numbers last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end
    end

    describe '.sort_by_modem_iccid' do
      before do
        @device_1 = create("#{model.to_s.underscore}_device".to_sym, :high_modem_iccid)
        @device_2 = create("#{model.to_s.underscore}_device".to_sym, :low_modem_iccid, series: @device_1.series)
        @device_3 = create("#{model.to_s.underscore}_device".to_sym, series: @device_1.series)
      end

      context 'when sorted ascending' do
        let(:result) { model.sort_by_modem_iccid 'asc' }

        it 'sorts null modem iccid first and higher modem iccid last' do
          expect(result.first).to eq @device_3
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_1
        end
      end

      context 'when sorted descending' do
        let(:result) { model.sort_by_modem_iccid 'desc' }

        it 'sorts higher modem iccid first and null modem iccid last' do
          expect(result.first).to eq @device_1
          expect(result.second).to eq @device_2
          expect(result.last).to eq @device_3
        end
      end
    end
  end
end
