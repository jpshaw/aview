# frozen_string_literal: true

shared_examples_for 'an action restricted by `Modify Organizations`' do |verb, action|
  before do
    @test_organization = create(:random_regular_organization)
    @test_role         = create(:role, organization: @test_organization)
    Permission.create(manager: @user.organization, manageable: @test_organization, role: @test_role)
    Permission.create(manager: @user, manageable: @test_organization, role: @test_role)
  end

  context 'when the user does not have the ability to `Modify Organizations`' do
    it 'redirects to the root path' do
      params = { id: @test_role.id, organization_id: @test_organization.slug }
      send verb, action, params
      expect(response).to redirect_to root_path
    end
  end
end
