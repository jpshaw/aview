# frozen_string_literal: true

shared_examples_for 'an unauthorized action' do |verb, action, path|
  context 'when the current user is not authorized for the action' do
    it 'denies access' do
      path = (path.present? ? send(path) : root_path).split('?').first
      send verb, action, id: 0
      expect(response).to redirect_to path
    end
  end
end
