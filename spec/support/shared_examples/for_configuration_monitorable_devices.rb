# frozen_string_literal: true

shared_examples_for 'a configuration monitorable device' do |configuration|
  let(:duration)  { %w[1w 1d 1h 1m 1s].sample               }
  let(:seconds)   { DurationCalculator.calculate(duration)  }

  before do
    # Configuration monitorable models delegate #heartbeat_frequency and #grace_period to their
    # superclass if there is no configuration or value in the configuration. The superclass uses
    # values from the Settings so we stub them here. There is no easy way to stub the use of keyword
    # 'super' so we test the value that the superclass returns instead.
    allow(Settings).to receive_message_chain(:devices, :default_status_freq).and_return(duration)
    allow(Settings).to receive_message_chain(:devices, :default_grace_period_duration).and_return(duration)
  end

  describe '#heartbeat_frequency' do
    context 'with no configuration' do
      before do
        allow(subject).to receive(:configuration).and_return(nil)
      end

      it 'delegates to superclass' do
        expect(subject.heartbeat_frequency).to eq(seconds)
      end
    end

    context 'with a configuration' do
      before do
        allow(subject).to receive(:configuration).and_return(configuration)
      end

      it 'delegates to configuration' do
        expect(configuration).to receive(:heartbeat_frequency)
        subject.heartbeat_frequency
      end
    end
  end

  describe '#grace_period' do
    context 'with no configuration' do
      before do
        allow(subject).to receive(:configuration).and_return(nil)
      end

      it 'delegates to superclass' do
        expect(subject.grace_period).to eq(seconds)
      end
    end

    context 'with a configuration' do
      before do
        allow(subject).to receive(:configuration).and_return(configuration)
      end

      it 'delegates to configuration' do
        expect(configuration).to receive(:heartbeat_grace_period)
        subject.grace_period
      end
    end
  end
end
