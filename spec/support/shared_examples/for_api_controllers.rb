# frozen_string_literal: true

shared_examples_for 'a SDN presence validator' do |verb, action|
  it 'returns a FORBIDDEN (403) status without SDN' do
    send verb, action
    expect(response.status).to be(403)
  end

  it 'does not return a FORBIDDEN (403) status with SDN' do
    request.env['HTTP_X_SSL_CERT_SDN'] = 'k1=v1/k2=v2'
    send verb, action
    expect(response.status).not_to be(403)
  end
end
