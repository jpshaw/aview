# frozen_string_literal: true

shared_examples 'a device subcategory presenter' do
  let(:presenter) { described_class.new(view) }

  before do
    allow(view).to receive(:current_user) { create(:user) }
  end

  describe '#facets' do
    it 'includes the specified keys' do
      expect(presenter.facets.keys).to include(:deployment, :model, :status)
    end
  end
end
