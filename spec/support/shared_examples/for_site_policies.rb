# frozen_string_literal: true

shared_examples_for 'a site modifier' do
  it 'grants access if user has ability to modify the site' do
    allow(user).to receive(:has_ability_on?).with(site.organization, to: 'Modify Sites').and_return(true)
    expect(subject).to permit(user, site)
  end

  it 'denies access if user does not have ability to modify the site' do
    allow(user).to receive(:has_ability_on?).with(site.organization, to: 'Modify Sites').and_return(false)
    expect(subject).not_to permit(user, site)
  end
end
