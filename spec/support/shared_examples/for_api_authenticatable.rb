# frozen_string_literal: true

shared_examples_for 'an api authenticatable resource' do
  let(:authentication_token) { 'Mmxm3sJrZBhkJXX7s2DN' }

  describe '.find_by_authentication_token' do
    before do
      resource.update(authentication_token: authentication_token)
    end

    context 'when given nil' do
      it 'returns nil' do
        expect(subject.class.find_by(authentication_token: nil)).to be nil
      end
    end

    context 'when given a token that does not match any resources' do
      it 'returns nil' do
        expect(subject.class.find_by(authentication_token: 'testing')).to be nil
      end
    end

    context 'when given a token that matches the characters and case' do
      it 'returns the resource' do
        expect(subject.class.find_by(authentication_token: authentication_token)).to eq resource
      end
    end
  end
end
