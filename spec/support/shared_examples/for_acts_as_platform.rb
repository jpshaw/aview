# frozen_string_literal: true

shared_examples_for 'acts as platform' do
  describe '#platform' do
    it 'returns the device platform' do
      expect(device.platform).to eq platform
    end
  end

  describe '#is_platform?' do
    context 'when given nil' do
      it 'returns false' do
        expect(device.is_platform?(nil)).to be false
      end
    end

    context 'when given another platform' do
      it 'returns false' do
        expect(device.is_platform?(:test_platform)).to be false
      end
    end

    context 'when given the device platform' do
      it 'returns true' do
        expect(device.is_platform?(platform)).to be true
      end
    end
  end

  describe '.platform' do
    it 'returns the device platform' do
      expect(device.class.platform).to eq platform
    end
  end

  describe '.is_platform?' do
    context 'when given nil' do
      it 'returns false' do
        expect(device.class.is_platform?(nil)).to be false
      end
    end

    context 'when given another platform' do
      it 'returns false' do
        expect(device.class.is_platform?(:test_platform)).to be false
      end
    end

    context 'when given the device platform' do
      it 'returns true' do
        expect(device.class.is_platform?(platform)).to be true
      end
    end
  end
end
