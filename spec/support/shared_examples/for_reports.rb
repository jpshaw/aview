# frozen_string_literal: true

shared_examples_for 'a report' do
  describe '#initialize' do
    let(:report_record) { build(:device_report) }

    it 'sets report record to nil if not passed' do
      report = report_class.new
      expect(report.instance_variable_get(:@report_record)).to be nil
    end

    it 'sets report record if passed' do
      expect(report_record).to be_present
      report = report_class.new(report_record)
      expect(report.instance_variable_get(:@report_record)).to eq(report_record)
    end
  end

  context 'when instantiated' do
    # We offset the length of 'Report' by 1 because back index starts at -1, not 0.
    def report_name
      report.class.to_s[0..('Report'.length + 1) * -1]
    end

    subject(:report)    { report_class.new(report_record) }

    let(:report_record) { build(:device_report) }

    it_behaves_like 'a parameters repo for UI form fields'
    it_behaves_like 'a generic report'

    describe '#title' do
      it 'returns a string representing the report title' do
        expect(report.title).to eq(report_name.underscore.titleize)
      end
    end

    describe '#report_type' do
      it 'returns a string representing the report type' do
        expect(report.report_type).to eq(report_name.underscore)
      end
    end

    describe '#generate!' do
      it 'generates data for the report' do
        expect(report).to receive(:generate_data)
        allow(report).to receive(:table)
        allow(report).to receive(:pie_chart)
        allow(report).to receive(:trend_chart)
        report.generate!
      end

      it 'produces table data' do
        expect(report).to receive(:table)
        allow(report).to receive(:generate_data)
        allow(report).to receive(:pie_chart)
        allow(report).to receive(:trend_chart)
        report.generate!
      end

      it 'produces pie chart data' do
        expect(report).to receive(:pie_chart)
        allow(report).to receive(:generate_data)
        allow(report).to receive(:table)
        allow(report).to receive(:trend_chart)
        report.generate!
      end

      it 'produces trend chart data' do
        expect(report).to receive(:trend_chart)
        allow(report).to receive(:generate_data)
        allow(report).to receive(:table)
        allow(report).to receive(:pie_chart)
        report.generate!
      end

      it 'updates the device report record with the generated data' do
        expect(report_record.table).to        be_empty
        expect(report_record.pie_chart).to    be_empty
        expect(report_record.trend_chart).to  be_empty

        table_content       = ['table content']
        pie_chart_content   = { pie_key: 'pie value' }
        trend_chart_content = { trend_key: 'trend value' }

        allow(report).to receive(:generate_data)
        allow(report).to receive(:table).and_return(table_content)
        allow(report).to receive(:pie_chart).and_return(pie_chart_content)
        allow(report).to receive(:trend_chart).and_return(trend_chart_content)
        stub_report_file
        report.generate!
        report_record.reload
        expect(report_record.table).to        eq(table_content)
        expect(report_record.pie_chart).to    eq(pie_chart_content)
        expect(report_record.trend_chart).to  eq(trend_chart_content)
      end

      it "updates the device report record's finished_at value" do
        expect(report_record.finished_at).to be(nil)

        allow(report).to receive(:generate_data)
        allow(report).to receive(:table)
        allow(report).to receive(:pie_chart)
        allow(report).to receive(:trend_chart)
        report.generate!

        report_record.reload
        expect(report_record.finished_at).not_to be nil
      end
    end
  end
end

shared_examples_for 'a parameters repo for UI form fields' do
  it { is_expected.to respond_to(:organization_id)    }
  it { is_expected.to respond_to(:include_hierarchy)  }
  it { is_expected.to respond_to(:deployment)         }
  it { is_expected.to respond_to(:sort)               }
  it { is_expected.to respond_to(:direction)          }
  it { is_expected.to respond_to(:range)              }
  it { is_expected.to respond_to(:report_type)        }
  it { is_expected.to respond_to(:models)             }
end

shared_examples_for 'a generic report' do
  it { is_expected.to respond_to(:title)              }
  it { is_expected.to respond_to(:report_type)        }
  it { is_expected.to respond_to(:device_models)      }
  it { is_expected.to respond_to(:generate!)          }
end
