# frozen_string_literal: true

shared_examples_for 'a manageable resource' do
  describe '#permission_managed_by manager' do
    let(:manager) { create(:random_regular_organization) }
    let(:permission) do
      Permission.new(
        manager:    manager,
        manageable: manageable,
        role:       manager.admin_role
      )
    end

    it 'returns nil when the manageable does not have a permission managed by the manager' do
      expect(manageable.permission_managed_by(manager)).to be nil
    end

    it 'returns a permission when the manageable has a permission managed by the manager' do
      allow(manageable).to receive(:manageable_permissions).and_return([permission])
      expect(manageable.permission_managed_by(manager)).to eq(permission)
    end
  end
end
