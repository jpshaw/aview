# frozen_string_literal: true

shared_examples_for 'a searchable model' do
  let(:klass) { subject.class }

  describe '#search!' do
    it 'returns an error when attempting to search all' do
      expect { klass.search!(all: nil) }.to raise_exception(Searchable::InvalidSearchException)
    end

    it 'returns an error when attempting to search with invalid scope' do
      expect { klass.search!(invalid: nil) }.to raise_exception(Searchable::InvalidSearchException)
    end

    it 'responds to all of its declared allowable filters' do
      klass.allowable_filters.each do |filter|
        expect(subject.class).to respond_to filter
      end
    end
  end
end
