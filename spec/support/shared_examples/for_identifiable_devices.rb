# frozen_string_literal: true

shared_examples_for 'an identifiable device' do
  let(:klass)          { subject.class }
  let(:test_klass)     { TestDevice }
  let(:model_name)     { 'Test Model' }

  let(:mac_range)      { 0x00ABCDEF000000..0x00ABCDEF999999 }
  let(:valid_mac)      { rand(mac_range).to_s(16).upcase }
  let(:invalid_mac)    { (mac_range.last + 1).to_s(16).upcase }

  let(:serial_range)   { 8_300_010_000_000_000..8_300_019_999_999_999 }
  let(:valid_serial)   { rand(serial_range) }
  let(:invalid_serial) { (serial_range.last + 1) }

  before do
    klass.mac_ranges    = {}
    klass.serial_ranges = {}
  end

  after do
    klass.mac_ranges    = {}
    klass.serial_ranges = {}
  end

  describe '.has_mac_range' do
    context 'when given a mac range' do
      it 'adds it to the device class' do
        expect do
          klass.has_mac_range mac_range
        end.to change { klass.mac_ranges.count }.by(1)
      end

      # This ensures the integrity of the class variables used.
      it 'does not add it to other device classes' do
        expect do
          klass.has_mac_range mac_range
        end.not_to change { test_klass.mac_ranges.count }
      end

      context 'when given a device model' do
        it 'sets the device model for the range' do
          klass.has_mac_range mac_range, model_name: model_name
          expect(klass.model_name_for_mac(valid_mac)).to eq model_name
        end
      end
    end
  end

  describe '.has_serial_range' do
    context 'when given a serial range' do
      it 'adds it to the device class' do
        expect do
          klass.has_serial_range serial_range
        end.to change { klass.serial_ranges.count }.by(1)
      end

      # This ensures the integrity of the class variables used.
      it 'does not add it to other device classes' do
        expect do
          klass.has_serial_range serial_range
        end.not_to change { test_klass.serial_ranges.count }
      end

      context 'when given a device model' do
        it 'sets the device model for the range' do
          klass.has_serial_range serial_range, model_name: model_name
          expect(klass.model_name_for_serial(valid_serial)).to eq model_name
        end
      end
    end
  end

  describe '.valid_mac?' do
    before do
      klass.has_mac_range mac_range
    end

    context 'when given nil' do
      it 'returns false' do
        expect(klass.valid_mac?(nil)).to eq false
      end
    end

    context 'when given an invalid mac' do
      it 'returns false' do
        expect(klass.valid_mac?(invalid_mac)).to eq false
      end
    end

    context 'when given a valid mac' do
      it 'returns true' do
        expect(klass.valid_mac?(valid_mac)).to eq true
      end
    end
  end

  describe '.valid_serial?' do
    before do
      klass.has_serial_range serial_range
    end

    context 'when given nil' do
      it 'returns false' do
        expect(klass.valid_serial?(nil)).to eq false
      end
    end

    context 'when given an invalid serial' do
      it 'returns false' do
        expect(klass.valid_serial?(invalid_serial)).to eq false
      end
    end

    context 'when given a valid serial' do
      it 'returns true' do
        expect(klass.valid_serial?(valid_serial)).to eq true
      end
    end
  end

  describe '.valid_identifiers?' do
    before do
      klass.has_mac_range mac_range
      klass.has_serial_range serial_range
    end

    context 'when given a valid mac' do
      it 'returns true' do
        expect(klass.valid_identifiers?(mac: valid_mac)).to eq true
      end
    end

    context 'when given a valid serial' do
      it 'returns true' do
        expect(klass.valid_identifiers?(serial: valid_serial)).to eq true
      end
    end

    context 'when given a valid mac and valid serial' do
      it 'returns true' do
        expect(klass.valid_identifiers?(mac: valid_mac, serial: valid_serial)).to eq true
      end
    end

    context 'when given a valid mac and invalid serial' do
      it 'returns false' do
        expect(klass.valid_identifiers?(mac: valid_mac, serial: invalid_serial)).to eq false
      end
    end

    context 'when given an invalid mac and invalid serial' do
      it 'returns false' do
        expect(klass.valid_identifiers?(mac: invalid_mac, serial: invalid_serial)).to eq false
      end
    end
  end

  describe '.valid_serial_check_digit?' do
    let(:serial) do
      serial_base = rand(10_000_000_000) * 100
      check_digit = (98 - (serial_base % 97)) % 97
      serial_base + check_digit
    end

    context 'when given a valid serial' do
      it 'returns true' do
        expect(klass.valid_serial_check_digit?(serial)).to be(true)
      end
    end

    context 'when given an invalid serial' do
      it 'returns false' do
        expect(klass.valid_serial_check_digit?(serial + 1)).to be(false)
      end
    end
  end
end
