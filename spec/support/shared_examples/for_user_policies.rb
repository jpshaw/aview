# frozen_string_literal: true

shared_examples_for 'a user modifier' do
  context 'when the managed user does not have an organization' do
    before do
      managed_user.organization = nil
    end

    it 'denies access' do
      expect(subject).not_to permit(user, managed_user)
    end
  end

  context 'when the managed user has an organization' do
    before do
      managed_user.organization = organization
      expect(managed_user.organization).to be_present
    end

    context 'when the user has the ability to modify users for the user`s organization' do
      before do
        allow(user).to receive(:has_ability_on?).and_return(true)
      end

      it 'grants access' do
        expect(subject).to permit(user, managed_user)
      end
    end

    context 'when the user does not have the ability to modify users for the user`s organization' do
      before do
        allow(user).to receive(:has_ability_on?).and_return(false)
      end

      it 'denies access' do
        expect(subject).not_to permit(user, managed_user)
      end
    end
  end
end
