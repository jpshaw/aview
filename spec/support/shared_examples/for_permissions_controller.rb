# frozen_string_literal: true

shared_examples_for 'permission :index' do
  it 'renders a json datatable with the user permission in it' do
    get :index, @params.merge(format: :json)
    result = JSON.parse(response.body)
    expect(result['recordsTotal']).to eq 1
    expect(result['data'].first).to include @organization_permission.manageable.name
  end
end

shared_examples_for 'permission :new' do
  it 'assigns a new Permission to @permission' do
    xhr :get, :new, @params
    permission = assigns(:permission)
    expect(permission).to be_present
    expect(permission).to be_new_record
    expect(permission).to be_a Permission
  end

  it 'renders the :new template' do
    xhr :get, :new, @params
    expect(response).to render_template :new
  end
end

shared_examples_for 'permission :create' do
  context 'with valid attributes' do
    it 'creates a new permission' do
      expect do
        xhr :post, :create, @params
      end.to change(Permission, :count).by(1)
    end

    it 'renders the create template' do
      xhr :post, :create, @params
      expect(response).to render_template :create
    end
  end

  context 'with invalid attributes' do
    before do
      @params[:permission] = {
        manageable_type: nil,
        manageable_id:   nil,
        role_id:         nil
      }
    end

    it 'does not save the new permission' do
      expect do
        xhr :post, :create, @params
      end.not_to change(Permission, :count)
    end

    it 'renders the :create view' do
      xhr :post, :create, @params
      expect(response).to render_template :create
    end
  end
end

shared_examples_for 'permission :edit' do
  before do
    @permission = @manager.manager_permissions.first
  end

  it 'assigns the requested permission to @permission' do
    xhr :get, :edit, @params.merge(id: @permission)
    expect(assigns(:permission)).to eq @permission
  end

  it 'renders the :edit view' do
    xhr :get, :edit, @params.merge(id: @permission)
    expect(response).to render_template :edit
  end
end

shared_examples_for 'permission :update' do
  before do
    @permission_params = {}
    @params.merge!(id: @permission, permission: @permission_params)
  end

  context 'with valid attributes' do
    before do
      @permission_params[:role_id] = @valid_role.id
    end

    it 'locates the requested @permission' do
      xhr :patch, :update, @params
      expect(assigns(:permission)).to eq @permission
    end

    it 'changes @permission`s attributes' do
      xhr :patch, :update, @params
      @permission.reload
      expect(@permission.role).to eq @valid_role
    end

    it 'redirects to the manager permissions path' do
      xhr :patch, :update, @params
      expect(response).to redirect_to [@manager, :permissions]
    end
  end

  context 'with invalid attributes' do
    before do
      @permission_params[:role_id] = @invalid_role.id
    end

    it 'locates the requested @permission' do
      xhr :patch, :update, @params
      expect(assigns(:permission)).to eq @permission
    end

    it 'does not change the @permission`s attributes' do
      xhr :patch, :update, @params
      @permission.reload
      expect(@permission.role).not_to eq @invalid_role
    end

    it 're-renders the :edit view' do
      xhr :patch, :update, @params
      expect(response).to render_template :edit
    end
  end
end

shared_examples_for 'permission :destroy' do
  before do
    @params.merge!(id: @permission)
  end

  it 'deletes the permission' do
    expect do
      xhr :delete, :destroy, @params
    end.to change(Permission, :count).by(-1)
  end

  it 'renders the :destroy template' do
    xhr :delete, :destroy, @params
    expect(response).to render_template :destroy
  end
end
