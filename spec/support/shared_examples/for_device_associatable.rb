# frozen_string_literal: true

shared_examples_for 'an associatable device' do
  describe '#device_associations' do
    let!(:association) do
      create(:device_association, source_id: device.id, source_mac: device.mac)
    end
    let!(:inverse_association) do
      create(:device_association, target_id: device.id, target_mac: device.mac)
    end
    let!(:other_association) { create(:device_association) }

    it 'returns associations for the device' do
      expect(device.device_associations).to include(association, inverse_association)
    end

    it 'does not return associations for other devices' do
      expect(device.device_associations).not_to include(other_association)
    end
  end

  describe '#device_wan_associations' do
    let!(:wan_association) do
      create(:device_association,
             source_id: device.id, source_mac: device.mac, source_type: 'wan')
    end
    let!(:inverse_wan_association) do
      create(:device_association,
             target_id: device.id, target_mac: device.mac, target_type: 'wan')
    end
    let!(:other_wan_association) do
      create(:device_association, source_type: 'wan')
    end

    it 'returns wan associations for the device' do
      expect(device.device_wan_associations).to include(wan_association, inverse_wan_association)
    end

    it 'does not return wan associations for other devices' do
      expect(device.device_wan_associations).not_to include(other_wan_association)
    end
  end

  describe '#device_lan_associations' do
    let!(:lan_association) do
      create(:device_association,
             source_id: device.id, source_mac: device.mac, source_type: 'lan')
    end
    let!(:inverse_lan_association) do
      create(:device_association,
             target_id: device.id, target_mac: device.mac, target_type: 'lan')
    end
    let!(:other_lan_association) do
      create(:device_association, source_type: 'lan')
    end

    it 'returns lan associations for the device' do
      expect(device.device_lan_associations).to include(lan_association, inverse_lan_association)
    end

    it 'does not return lan associations for other devices' do
      expect(device.device_lan_associations).not_to include(other_lan_association)
    end
  end

  describe '#device_associations_for_type' do
    let!(:association) do
      create(:device_association,
             source_id: device.id, source_mac: device.mac, source_type: 'test')
    end
    let!(:inverse_association) do
      create(:device_association,
             target_id: device.id, target_mac: device.mac, target_type: 'test')
    end
    let!(:other_association) do
      create(:device_association, source_type: 'test')
    end

    it 'returns associations matching the type for the device' do
      expect(device.device_associations_for_type(:test)).to include(association, inverse_association)
    end

    it 'does not return associations matching the type for other devices' do
      expect(device.device_associations_for_type(:test)).not_to include(other_association)
    end
  end

  describe '#device_associations_for_interface' do
    let!(:association) do
      create(:device_association,
             source_id: device.id, source_mac: device.mac, source_interface: 'eth1')
    end
    let!(:inverse_association) do
      create(:device_association,
             target_id: device.id, target_mac: device.mac, target_interface: 'eth1')
    end
    let!(:other_association) do
      create(:device_association, source_interface: 'eth1')
    end

    it 'returns associations matching the interface for the device' do
      expect(device.device_associations_for_interface(:eth1)).to include(association, inverse_association)
    end

    it 'does not return associations matching the interface for other devices' do
      expect(device.device_associations_for_interface(:eth1)).not_to include(other_association)
    end
  end

  describe '#associated_with_device?' do
    let(:other_device) { create(:test_device) }

    context 'when given a device it is associated with' do
      before do
        create(:device_association,
               source_id: device.id,
               source_mac: device.mac,
               target_id: other_device.id,
               target_mac: other_device.mac)
      end

      it 'returns true' do
        expect(device.associated_with_device?(other_device)).to be true
      end
    end

    context 'when given a device it is inversely associated with' do
      before do
        create(:device_association,
               source_id: other_device.id,
               source_mac: other_device.mac,
               target_id: device.id,
               target_mac: device.mac)
      end

      it 'returns true' do
        expect(device.associated_with_device?(other_device)).to be true
      end
    end

    context 'when given a device it is not associated with' do
      it 'returns false' do
        expect(device.associated_with_device?(other_device)).to be false
      end
    end
  end

  describe '#associated_with_mac?' do
    let(:other_mac) { generate(:mac_address) }

    context 'when given a mac it is associated with' do
      before do
        create(:device_association,
               source_id: device.id,
               source_mac: device.mac,
               target_id: nil,
               target_mac: other_mac)
      end

      it 'returns true' do
        expect(device.associated_with_mac?(other_mac)).to be true
      end
    end

    context 'when given a mac it is inversely associated with' do
      before do
        create(:device_association,
               source_id: nil,
               source_mac: other_mac,
               target_id: device.id,
               target_mac: device.mac)
      end

      it 'returns true' do
        expect(device.associated_with_mac?(other_mac)).to be true
      end
    end

    context 'when given a mac it is not associated with' do
      it 'returns false' do
        expect(device.associated_with_mac?(other_mac)).to be false
      end
    end
  end

  describe '#associate_with_device' do
    let(:other_device) { create(:test_device) }

    context 'when the devices are not associated' do
      it 'creates an association' do
        expect do
          device.associate_with_device(other_device)
        end.to change(DeviceAssociation, :count).by(1)
      end

      it 'associates the devices' do
        expect do
          device.associate_with_device(other_device)
        end.to associate_devices(device, other_device)
      end
    end

    context 'when the devices are associated' do
      before { device.associate_with_device(other_device) }

      it 'does not create a new association' do
        expect do
          device.associate_with_device(other_device)
        end.not_to change(DeviceAssociation, :count)
      end
    end

    context 'when the devices are inversely associated' do
      before { other_device.associate_with_device(device) }

      it 'does not create a new association' do
        expect do
          device.associate_with_device(other_device)
        end.not_to change(DeviceAssociation, :count)
      end
    end

    context 'when given details about the association' do
      let(:details) { build(:device_association_with_details).details }

      before do
        device.associate_with_device(other_device, details)
      end

      it 'stores them' do
        association = device.device_associations.first
        expect(association.ip_address).to eq details['ip_address']
        expect(association.source_interface).to eq details['source_interface']
        expect(association.target_interface).to eq details['target_interface']
        expect(association.source_type).to eq details['source_type']
        expect(association.target_type).to eq details['target_type']
      end
    end
  end

  describe '#associate_with_mac' do
    let(:other_mac) { generate(:mac_address) }

    context 'when the mac is not associated' do
      it 'creates an association' do
        expect do
          device.associate_with_mac(other_mac)
        end.to change(DeviceAssociation, :count).by(1)
      end

      it 'associates to the mac' do
        expect do
          device.associate_with_mac(other_mac)
        end.to associate_device_to_mac(device, other_mac)
      end
    end

    context 'when the mac is associated' do
      before { device.associate_with_mac(other_mac) }

      it 'does not create a new association' do
        expect do
          device.associate_with_mac(other_mac)
        end.not_to change(DeviceAssociation, :count)
      end
    end

    context 'when the mac is inversely associated' do
      before do
        create(:device_association,
               source_id: nil,
               source_mac: other_mac,
               target_id: device.id,
               target_mac: device.mac)
      end

      it 'does not create a new association' do
        expect do
          device.associate_with_mac(other_mac)
        end.not_to change(DeviceAssociation, :count)
      end
    end

    context 'when given details about the association' do
      let(:details) { build(:device_association_with_details).details }

      before do
        device.associate_with_mac(other_mac, details)
      end

      it 'stores them' do
        association = device.device_associations.first
        expect(association.ip_address).to eq details['ip_address']
        expect(association.source_interface).to eq details['source_interface']
        expect(association.target_interface).to eq details['target_interface']
        expect(association.source_type).to eq details['source_type']
        expect(association.target_type).to eq details['target_type']
      end
    end
  end

  describe '#dissociate_from_device' do
    let(:other_device) { create(:test_device) }

    context 'when the devices are associated' do
      before { device.associate_with_device(other_device) }

      it 'removes the association' do
        expect do
          device.dissociate_from_device(other_device)
        end.to dissociate_devices(device, other_device)
      end
    end

    context 'when the devices are not associated' do
      it 'does not remove any associations' do
        expect do
          device.dissociate_from_device(other_device)
        end.not_to change(DeviceAssociation, :count)
      end
    end
  end

  describe '#dissociate_from_mac' do
    let(:other_mac) { generate(:mac_address) }

    context 'when the mac is associated' do
      before { device.associate_with_mac(other_mac) }

      it 'removes the association' do
        expect do
          device.dissociate_from_mac(other_mac)
        end.to dissociate_device_from_mac(device, other_mac)
      end
    end

    context 'when the mac is not associated' do
      it 'does not remove any associations' do
        expect do
          device.dissociate_from_mac(other_mac)
        end.not_to change(DeviceAssociation, :count)
      end
    end
  end
end
