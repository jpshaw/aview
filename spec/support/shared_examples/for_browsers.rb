# frozen_string_literal: true

shared_examples_for 'an ie browser' do |version|
  it 'adds the ie class to the html element' do
    view.request.env['HTTP_USER_AGENT'] = ie_user_agent(version)
    render
    expect(view.browser.send("ie#{version}?")).to be true
    expect(rendered).to have_selector("html[class='ie ie#{version}'][lang='en']")
  end

  context 'when the browser is not modern (IE6-8)' do
    it 'includes the HTML5 shim' do
      if (6..8).cover?(version)
        view.request.env['HTTP_USER_AGENT'] = ie_user_agent(version)
        render
        expect(view.browser.modern?).to be false
        expect(rendered).to match('/assets/html5.js')
      end
    end
  end
end
