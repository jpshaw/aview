# frozen_string_literal: true

require 'set'

shared_examples_for 'a base device class' do
end

shared_examples_for 'device_show_page_check' do
  it { current_path.should match "/devices/#{@device.mac}" }
end

shared_examples_for 'device_events_page_check' do
  it { current_path.should match "/devices/#{@device.mac}/events" }
end

shared_examples_for 'device_show_generic_blocks' do
  it { is_expected.to have_css('#content-header') }
  context('with the content-header') do
    subject { find('#content-header') }

    it { is_expected.to have_content(@device.category_name) }
    it { is_expected.to have_content(@device.series_name) }
    it { is_expected.to have_content(@device.mac) }
    it { is_expected.to have_css('#device-subscription-button') }
    it { is_expected.to have_css('#device-route-links') }
  end

  it { is_expected.to have_css('#device-details') }
  it { is_expected.to have_css('#location') }
  it { is_expected.to have_css('#contacts') }
end

shared_examples_for 'device_events_generic_blocks' do
  it { is_expected.to have_css('#content-header') }
  context('with the content-header') do
    subject { find('#content-header') }

    it { is_expected.to have_content(@device.category_name) }
    it { is_expected.to have_content(@device.series_name) }
    it { is_expected.to have_content(@device.mac) }
    it { is_expected.to have_css('#device-subscription-button') }
    it { is_expected.to have_css('#device-route-links') }
  end
end

shared_examples_for 'device_events_generic_blocks_for_admins' do
end

shared_examples_for 'device_show_generic_blocks_for_viewers' do
  it { is_expected.not_to have_css('#admin-panel') }
  it { find('#location').should_not have_css('#location_submit_button') }
  skip { find('#location').should_not have_css('#location_clear_button') }
  it { find('#contacts').should_not have_link('Add Contact') }
end

shared_examples_for 'device_events_generic_blocks_for_viewers' do
end

shared_examples_for 'device_show_gateway_specific_blocks_for_admins' do
  it { find('#content-header').should have_css('.commands') }
end

shared_examples_for 'device_events_gateway_specific_blocks_for_admins' do
  it { find('#content-header').should have_css('.commands') }
end

shared_examples_for 'device_show_gateway_specific_blocks_for_viewers' do
  it { find('#content-header').should_not have_css('.commands') }
end

shared_examples_for 'device_events_gateway_specific_blocks_for_viewers' do
  it { find('#content-header').should_not have_css('.commands') }
end

shared_examples_for 'device_show_netbridge_specific_blocks_for_admins' do
  it { find('#content-header').should have_css('.commands') }
end

shared_examples_for 'device_events_netbridge_specific_blocks_for_admins' do
  it { find('#content-header').should have_css('.commands') }
end

shared_examples_for 'device_show_netbridge_specific_blocks_for_viewers' do
  it { find('#content-header').should_not have_css('.commands') }
end

shared_examples_for 'device_events_netbridge_specific_blocks_for_viewers' do
  it { find('#content-header').should_not have_css('.commands') }
end

# Test Partials for Deploying and Undeploying
shared_examples_for 'deploy_and_undeploy_device' do
  describe 'with a deploy button' do
    skip 'should be able to deploy an undeployed device' do
      @device.toggle_deployment
      visit device_path(@device.mac)
      device_show_page_check
      click_link 'Deploy'
      device_show_page_check
      expect(find('#admin-panel')).to have_link('Undeploy')
    end

    skip 'should be able to undeploy a deployed device' do
      click_link 'Undeploy'
      device_show_page_check
      expect(find('#admin-panel')).to have_link('Deploy')
    end
  end
end

# Test Partials for Switching Organizations
shared_examples_for 'switch_organization_for_device' do
  describe 'with a organization switching capabilites' do
    before do
      create_child_boundary_organization
      visit device_path(@device.mac)
    end

    it 'is able to change the organization' do
      device_show_page_check
      select @new_boundary_organization.name, from: 'device[organization_id]'
      click_button 'device_settings_update'
      device_show_page_check
      expect(find_field('device_organization_id').value).to eq @new_boundary_organization[:id].to_s
    end
  end
end

# Test Partials for Site ID stuff
shared_examples_for 'set_and_view_site_id_for_device' do
  describe 'with site_id settings' do
    it 'allows me to change the site_id' do
      fill_in 'device_site_id', with: 'APPLESAUCE123'
      click_button 'device_settings_update'
      device_show_page_check
      expect(find_field('device_site_id').value).to eq 'APPLESAUCE123'
    end

    it 'sets the site_id back to default if it set to blank' do
      fill_in 'device_site_id', with: ''
      click_button 'device_settings_update'
      device_show_page_check
      expect(find_field('device_site_id').value).to eq 'Default'
    end

    it 'allows me to get to the current site' do
      click_link 'View devices at site'
      expect(current_path).to match "/sites/#{@device.site[:id]}"
    end
  end
end

# Test Partials for Location Setting Stuff
shared_examples_for 'set_location_information_for_device' do
  describe 'with location settings' do
    before do
      create_location
      visit device_path(@device.mac)
    end

    it 'allows the settings of a valid location' do
      VCR.use_cassette('geocoder location lookup') do
        create_location_from_form('1208 E Kennedy BLVD', 'Suite 226', 'Tampa', 'FL', '33602')
      end
      device_show_page_check
      alert_success("The device's location has been successfully updated")
      expect(find_field('location_address_1').value).to eq '1208 E John F Kennedy Blvd'
      expect(find_field('location_address_2').value).to eq 'Suite 226'
      expect(find_field('location_city').value).to eq 'Tampa'
      expect(find_field('location_state_abbr').value).to eq 'FL'
      expect(find_field('location_postal_code').value).to eq '33602'
      expect(find_field('location_country_id').value).to eq @country[:id].to_s
    end

    # Can't do this one at the moment due to the fact that hitting the clear button brings up a javascript dialog box we can't interact with
    skip 'should clear the location when the clear button is pressed' do
      create_location_from_form('1208 E Kennedy BLVD', 'Suite 226', 'Tampa', 'FL', '33602')
      device_show_page_check
      click_button 'location_clear_button'
      device_show_page_check
      alert_notice('Location information for the device has been cleared')
      expect(find_field('location_address_1').value).to eq ''
      expect(find_field('location_address_2').value).to eq ''
      expect(find_field('location_city').value).to eq ''
      expect(find_field('location_state_abbr').value).to eq ''
      expect(find_field('location_postal_code').value).to eq ''
    end

    it 'does not allow you to create a blank location' do
      VCR.use_cassette('geocoder location lookup') do
        create_location_from_form
      end
      alert_error('Please enter valid location information')
    end

    it 'does not allow you to update with a blank location' do
      VCR.use_cassette('geocoder location lookup') do
        create_location_from_form('1208 E Kennedy BLVD', 'Suite 226', 'Tampa', 'FL', '33602')
      end
      device_show_page_check
      VCR.use_cassette('geocoder location lookup') do
        create_location_from_form
      end
      alert_error('Please enter valid location information')
    end
  end
end

# Test Partials for Contact Setting stuff
shared_examples_for 'managing_contacts_for_device' do
  describe 'with contact settings' do
    it 'allows you to add a valid contact' do
      expect(find('#device_contact_list')).to have_content('This device currently has no contacts')
      create_contact_from_form('Test User', '8131234567', 'test.user@accelerated.com')
      device_show_page_check
      expect(find('#device_contact_list')).to have_content('Test User')
      alert_success('Contact Successfully Added')
    end

    it 'does not allow you to add a blank contact' do
      create_contact_from_form
      device_show_page_check
      expect(subject).to have_content('Invalid Contact')
    end

    it 'allows you to select an existing contact that has been added to the organization' do
      created_linked_contact
      visit device_path(@device.mac)
      device_show_page_check
      click_button 'contact_add_button'
      device_show_page_check
      expect(find('#device_contact_list')).to have_content('Test User')
    end

    it 'does not allow you to add the same contact twice' do
      create_contact_from_form('Test User', '8131234567', 'test.user@accelerated.com')
      device_show_page_check
      click_button 'contact_add_button'
      device_show_page_check
      alert_notice('Contact Already Added on the Current Device')
    end

    it 'allows you to delete a contact' do
      create_contact_from_form('Test User', '8131234567', 'test.user@accelerated.com')
      device_show_page_check
      click_link 'contact_delete_button'
      device_show_page_check
      alert_success('Removed Contact from Device')
      expect(find('#device_contact_list')).to have_content('This device currently has no contacts')
    end
  end
end

shared_examples_for 'device_show_for_admin' do
  # Checks to make sure it is on the right path
  include_examples 'device_show_page_check'

  # Checks the basic device page blocks: details, maps, contacts
  include_examples 'device_show_generic_blocks'

  # Checks that we can deploy and undeploy a device
  include_examples 'deploy_and_undeploy_device'

  # Checks site_id setting and viewing
  include_examples 'set_and_view_site_id_for_device'

  # Checks setting of location information
  include_examples 'set_location_information_for_device'

  # Checks that managing contacts works
  include_examples 'managing_contacts_for_device'
end

shared_examples_for 'device_show_for_viewer' do
  # Checks to make sure it is on the right path
  include_examples 'device_show_page_check'

  # Checks the basic device page blocks: details, maps, contacts
  include_examples 'device_show_generic_blocks'
  include_examples 'device_show_generic_blocks_for_viewers'
end

shared_examples_for 'device_events_for_admin' do
  # Checks to make sure it is on the right path
  include_examples 'device_events_page_check'

  # Checks the basic device page blocks
  include_examples 'device_events_generic_blocks'
  include_examples 'device_events_generic_blocks_for_admins'
end

shared_examples_for 'device_events_for_viewer' do
  # Checks to make sure it is on the right path
  include_examples 'device_events_page_check'

  # Checks the basic device page blocks
  include_examples 'device_events_generic_blocks'
  include_examples 'device_events_generic_blocks_for_viewers'
end
