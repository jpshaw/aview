# frozen_string_literal: true

shared_examples 'an action that redirects if an import is already running' do |verb, action|
  context 'when an import is running' do
    before do
      create(:qa_import)
    end

    it 'redirects to the imports index path and flashes a message' do
      send(verb, action)
      expect(response).to redirect_to admin_imports_path
      expect(flash).not_to be nil
      expect(flash[:notice]).to match /The importer is currently running/i
    end
  end
end

shared_examples 'an action that redirects if the import is not found' do |verb, action|
  context 'when an invalid import id is given' do
    it 'redirects to the imports index path and flashes a message' do
      send(verb, action, id: 'abc123')
      expect(response).to redirect_to admin_imports_path
      expect(flash).not_to be nil
      expect(flash[:error]).to match /Import #abc123 not found/i
    end
  end
end

shared_examples 'an action that authorizes import access' do |verb, action|
  context 'when the import setting is disabled' do
    before do
      Settings.imports = Config::Options.new(enabled: false)
    end

    it 'redirects to the root path and flashes a message' do
      params = { id: 1 }
      send(verb, action, params)
      expect(response).to redirect_to root_path
      expect(flash).not_to be nil
      expect(flash[:error]).to match /You are not authorized to perform this action/i
    end
  end
end

shared_examples 'an action that checks if the requested import is finished' do |verb, action|
  context 'when the import is not finished running' do
    before do
      @import = create(:qa_import)
    end

    it 'redirects to the imports index path and flashes a message' do
      send(verb, action, id: @import)
      expect(response).to redirect_to admin_imports_path
      expect(flash).not_to be nil
      expect(flash[:info]).to match /Import ##{@import.id} is in progress/i
    end
  end

  context 'when the import is finished running' do
    before do
      @import = create(:finished_qa_import)
    end

    it 'renders the page' do
      send(verb, action, id: @import)
      expect(response).to render_template action
    end
  end
end
