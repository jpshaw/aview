# frozen_string_literal: true

shared_examples_for 'a manager of resources' do
  describe 'Access to resources' do
    # Setup DB contents so tests are meaningful. Both organizations need user, account, device and site.
    let(:parent_user)                 { create(:user,                 organization: parent_organization) }
    let(:parent_device)               { create(:test_device,          organization: parent_organization) }
    let(:parent_site)                 { create(:site,                 organization: parent_organization) }
    # TODO: Restore when paperclip is replaced with carrierwave.
    # let(:parent_device_configuration) { create(:device_configuration, organization: parent_organization) }

    let(:user)                        { create(:user,                 organization: organization) }
    let(:device)                      { create(:test_device,          organization: organization) }
    let(:site)                        { create(:site,                 organization: organization) }
    # TODO: Restore when paperclip is replaced with carrierwave.
    # let(:device_configuration)        { create(:device_configuration, organization: organization) }

    # Setup extra records so we can run negative tests.
    let(:extra_organization)          { create(:random_regular_organization) }
    let(:extra_user)                  { create(:user,                 organization: extra_organization) }
    let(:extra_device)                { create(:test_device,          organization: extra_organization) }
    let(:extra_site)                  { create(:site,                 organization: extra_organization) }
    # TODO: Restore when paperclip is replaced with carrierwave.
    # let(:extra_device_configuration)  { create(:device_configuration, organization: extra_organization) }

    # Ability to be used in tests.
    let(:user_ability) { create(:user_ability) }

    def user_manager?
      manager.class == User
    end

    before do
      # Persist data.
      parent_organization   # To be used as parent of the below organization.
      organization          # This is the same as the below manager when manager is an organization,
      #   otherwise it is the user's organization when the manager is a user.
      manager # This can be an organization (the above organization) or a user.
      parent_user
      parent_device
      parent_site
      # TODO: Restore when paperclip is replaced with carrierwave.
      # parent_device_configuration
      user
      device
      site
      # TODO: Restore when paperclip is replaced with carrierwave.
      # device_configuration
      extra_organization
      extra_user
      extra_device
      # TODO: Restore when paperclip is replaced with carrierwave.
      # extra_device_configuration
      extra_site

      # Make organization a child of parent_organization.
      parent_organization.children << organization

      # Remove all permissions so we can set them up as needed for the tests.
      Permission.destroy_all
      expect(Permission.count).to be 0
    end


    context 'Without a permission for user manager' do
      # Note: The returned values are converted to arrays because grouping in the SQL causes trouble
      # with array-type methods.

      it 'does not return any resources' do
        if user_manager?
          expect(manager.organizations).to be_empty
          expect(manager.users).to be_empty
          expect(manager.devices).to be_empty
          expect(manager.sites).to be_empty
          expect(manager.device_configurations).to be_empty
        end
      end
    end


    context 'With permission to an organization without descendants' do
      before do
        # The organization is either the manager or the user's organization. If the manager is a user
        # its organization needs a permission so either way we have to create one for the organization.
        expect do
          Permission.create!(
            manager:    organization,
            manageable: organization,
            role:       organization.admin_role
          )
        end.to change(Permission, :count).by(1)

        # If the manager is a user, it needs a permission. We check here because if the manager is
        # an organization the above created permission might be the same thing we are trying to
        # create here.
        user_manager?
        if user_manager?
          expect do
            Permission.create!(
              manager:    manager,
              manageable: organization,
              role:       organization.admin_role
            )
          end.to change(Permission, :count).by(1)
        end
      end

      # Note: The returned values are converted to arrays because grouping in the SQL causes trouble
      # with array-type methods.

      it 'onlies manage the permitted organization' do
        organizations = manager.organizations
        expect(organizations.count).to be 1
        expect(organizations.first).to eq(organization)
        expect(Organization.count).to be > 1
      end

      it "onlies manage the permitted organization's devices" do
        devices = manager.devices
        expect(devices.count).to be 1
        expect(devices.first).to eq(device)
        expect(Device.count).to be > 1
      end

      it "onlies manage the permitted organization's sites" do
        # The newly created site + the default site.
        site_count = 2
        sites      = manager.sites

        expect(sites.count).to be site_count
        expect(sites.last).to eq(site)
        expect(Site.count).to be > site_count
      end

      context "When role has ability to 'Modify Users'" do
        before do
          organization.admin_role.abilities << create(:user_ability)
        end

        it "onlies manage the permitted organization's users" do
          expect(manager.users.count).to eq(organization.users.count)
        end
      end
    end

    context 'With permission to an organization with descendants' do
      before do
        # The organization needs a permission for the managed parent organization.
        expect do
          Permission.create!(
            manager:    organization,
            manageable: parent_organization,
            role:       organization.admin_role
          )
        end.to change(Permission, :count).by(1)

        # If the manager is a user, it needs a permission. We check here because if the manager is
        # an organization the above created permission might be the same thing we are trying to
        # create here.
        if user_manager?
          expect do
            Permission.create!(
              manager:    manager,
              manageable: parent_organization,
              role:       organization.admin_role
            )
          end.to change(Permission, :count).by(1)
        end
      end

      # Note: The returned values are converted to arrays because grouping in the SQL causes trouble
      # with array-type methods.

      it "manages all organization's in hierarchy" do
        organizations = manager.organizations
        expect(organizations.count).to eq(Organization.count - 1)
        expect(organizations).not_to include(extra_organization)
      end

      it "manages all devices in organization's hierarchy" do
        devices = manager.devices
        expected = user_manager? ? 2 : 1
        expect(devices.count).to eq(expected)
        expect(devices).not_to include(extra_device)
      end

      it "manages all sites in organization's hierarchy" do
        # The extra site + the extra site organization's default site account
        # for the 2 sites this user should not be able to manage.
        # extra_site_count = 2
        sites            = manager.sites

        expected = user_manager? ? 4 : 2

        expect(sites.count).to eq(expected)
        expect(sites).not_to include(extra_site)
      end

      context "When role has ability to 'Modify Users'" do
        before do
          organization.admin_role.abilities << create(:user_ability)
        end

        it "manages all users in organization's hierarchy" do
          users = manager.users
          expected = user_manager? ? 3 : 1
          expect(users.count).to eq(expected)
          expect(users).not_to include(extra_user)
        end
      end
    end

    describe '#has_ability?' do
      it 'returns false when manager does not have the ability' do
        expect(user.has_ability?(user_ability.name)).to be false
      end

      it 'returns true when manager has the ability' do
        # Add ability to role we'll use in the permission records.
        organization.admin_role.abilities << user_ability

        # We need 2 permissions, one for the organization and another one for the manager.
        Permission.create!(
          manager:    organization,
          manageable: organization,
          role:       organization.admin_role
        )

        # We only need one permission when the manager is the organization.
        unless manager == organization
          Permission.create!(
            manager:    manager,
            manageable: organization,
            role:       organization.admin_role
          )
        end

        # The manager should have the ability on the resource.
        expect(manager.has_ability?(user_ability.name)).to be true
      end
    end

    describe '#has_ability_on?' do
      it 'returns false when manager does not have the ability on the resource' do
        expect(manager.has_ability_on?(parent_user, to: user_ability.name)).to be false
      end

      it 'returns true when manager has the ability on the resource' do
        # Add ability to role we'll use in the permission records.
        organization.admin_role.abilities << user_ability

        # We need 2 permissions, one for the organization and another one for the manager.
        Permission.create!(
          manager:    organization,
          manageable: parent_organization,
          role:       organization.admin_role
        )

        # We only need one permission when the manager is the organization.
        unless manager == organization
          Permission.create!(
            manager:    manager,
            manageable: parent_organization,
            role:       organization.admin_role
          )
        end

        # The manager should have the ability on the resource.
        expect(manager.has_ability_on?(parent_organization, to: user_ability.name)).to be true
      end

      # Check permissions for manageables not in the same hierarchy
      context 'when there are manageables not in the same hierarchy' do
        before do
          # Add permission for organization on itself
          Permission.create!(
            manager:    organization,
            manageable: organization,
            role:       organization.admin_role
          )

          # Add permission for owned objects
          unless manager == organization
            Permission.create!(
              manager:    manager,
              manageable: organization,
              role:       organization.admin_role
            )
          end

          @user_ability   = create(:user_ability)
          @device_ability = create(:device_ability)

          @sibling_org = create(:random_regular_organization)
          parent_organization.add_child(@sibling_org)
          @sibling_role = @sibling_org.admin_role
          @sibling_role.abilities << @user_ability

          expect(@sibling_org.siblings).to include(organization)
        end

        context "when accessing a sibling's abilities" do
          context 'when it does not have a permission on the sibling' do
            before do
              expect(Permission.for(manager: organization, manageable: @sibling_org)).to be_empty
            end

            it 'returns false' do
              expect(manager.has_ability_on?(@sibling_org, to: @device_ability.name)).to be false
            end
          end

          context 'when it has a permission on the sibling' do
            before do
              Permission.create!(manager: organization, manageable: @sibling_org, role: @sibling_role)

              unless manager == organization
                Permission.create!(
                  manager:    manager,
                  manageable: @sibling_org,
                  role:       organization.admin_role
                )
              end
            end

            context 'when checking an ability the sibling grants to the organization' do
              before do
                expect(@sibling_role.abilities).to include(@user_ability)
              end

              context "when the manager's permission on its organization has the ability" do
                before do
                  organization.admin_role.abilities << @user_ability unless organization.admin_role.abilities.include?(@user_ability)
                end

                it 'returns true' do
                  expect(manager.has_ability_on?(@sibling_org, to: @user_ability.name)).to be true
                end
              end

              context "when the manager's permission on the manageable does not have the ability" do
                before do
                  if manager == organization
                    # The organization's permission on the sibling is dicated by the @sibling_role
                    @sibling_role.abilities.delete(@user_ability) if @sibling_role.abilities.include?(@user_ability)
                  else
                    # The managers permission on the sibling is dictated by it's organization's admin role
                    organization.admin_role.abilities.delete(@user_ability) if organization.admin_role.abilities.include?(@user_ability)
                  end
                end

                it 'returns false' do
                  expect(manager.has_ability_on?(@sibling_org, to: @user_ability.name)).to be false
                end
              end
            end

            if Role.scoped_to_organization?
              context 'when checking an ability the sibling did not grant to the organization' do
                before do
                  organization.admin_role.abilities << @device_ability
                  expect(manager.has_ability_on?(organization, to: @device_ability.name)).to be true
                  # expect(@sibling_role.abilities).not_to include(@device_ability)
                end

                it 'returns false' do
                  expect(manager.has_ability_on?(@sibling_org, to: @device_ability.name)).to be false
                end
              end
            end
          end
        end
      end
    end

    describe '#manageables' do
      it 'returns an empty collection when manager does not have manageables of the specified type for the ability' do
        expect(manager.manageables(of_type: :user, for_ability: user_ability.name)).to be_empty
      end

      it 'returns a populated collection when manager has manageables of the specified type for the ability' do
        # Add ability to role we'll use in the permission records.
        organization.admin_role.abilities << user_ability

        # We need 2 permissions, one for the organization and another one for the manager.
        Permission.create!(
          manager:    organization,
          manageable: parent_organization,
          role:       organization.admin_role
        )

        # We only need one permission when the manager is the organization.
        unless manager == organization
          Permission.create!(
            manager:    manager,
            manageable: parent_organization,
            role:       organization.admin_role
          )
        end

        # The manager should have manageables of the specified type for the specified ability.
        manageables   = manager.manageables(of_type: :user, for_ability: user_ability.name)
        user_count    = 0
        parent_organization.self_and_descendants.each do |organization|
          user_count += organization.users.count
        end
        expect(manageables).to be_present
        expect(manageables.count).to eq(user_count)
      end
    end

    describe '#modifiable_organizations' do
      it 'delegates to #manageables method' do
        expect(manager).to receive(:manageables)
          .with(
            for_ability:  'Modify Organizations',
            of_type:      Permission::MANAGER_TYPE_ORGANIZATION
          )
        manager.modifiable_organizations
      end
    end

    describe '#has_ability_for_any_organization_to?' do
      before do
        @role = organization.roles.create! name: 'Test'
        Permission.create(manager: organization, manageable: organization, role: @role)
        Permission.create(manager: manager, manageable: organization, role: @role)
      end

      context 'when the manager does not have the ability for any managed organizations' do
        it 'returns false' do
          retval = manager.has_ability_for_any_organization_to?('Test Things')
          expect(retval).to be false
        end
      end

      context 'when the manager has the ability for a managed organization' do
        before do
          @role.abilities << Ability.create!(name: 'Test Things')
        end

        it 'returns true' do
          retval = manager.has_ability_for_any_organization_to?('Test Things')
          expect(retval).to be true
        end
      end
    end

    describe '#permission_for' do
      before do
        @parent_organization = create(:random_regular_organization)
        @organization        = create(:random_regular_organization)
        @child_organization  = create(:random_regular_organization)
        @role                = create(:role, organization: @organization)

        @parent_organization.add_child(@organization)
        @organization.add_child(@child_organization)
      end

      context 'when the manager has no permissions for the resource' do
        it 'returns nil' do
          expect(manager.permission_for(@organization)).to be nil
        end
      end

      context 'when the manager has a permission for the resource' do
        before do
          @permission = Permission.create!(
            manager:    organization,
            manageable: @organization,
            role:       @role
          )

          unless manager == organization
            @permission = Permission.create!(
              manager:    manager,
              manageable: @organization,
              role:       @role
            )
          end
        end

        it 'returns the permission for the resource' do
          expect(manager.permission_for(@organization)).to eq @permission
        end

        context 'when the manager also has a permission on the parent organization' do
          before do
            @parent_permission = Permission.create(manager: manager, manageable: @parent_organization, role: @role)
          end

          it 'returns the permission for the resource' do
            expect(manager.permission_for(@organization)).to eq @permission
          end

          context 'when getting the permission for the child organization' do
            it 'returns the nearest permission' do
              expect(manager.permission_for(@child_organization)).to eq @permission
            end
          end
        end

        context 'when the manager does not have a permission on the parent organization' do
          context 'when getting a permission for the parent organization' do
            it 'returns nil' do
              expect(manager.permission_for(@parent_organization)).to be nil
            end
          end
        end
      end
    end

    describe '#abilities_for' do
      before do
        @parent_organization = create(:random_regular_organization)
        @organization        = create(:random_regular_organization)
        @child_organization  = create(:random_regular_organization)
        @role                = create(:role, organization: @organization)

        @organization_ability   = create(:organization_ability)
        @user_ability           = create(:user_ability)
        @device_ability         = create(:device_ability)
        @configuration_ability  = create(:configuration_ability)
        @import_devices_ability = create(:import_devices_ability)

        # Ensure parent permission has abilities available
        @role.abilities << @organization_ability
        @role.abilities << @user_ability
        @role.abilities << @device_ability
        @role.abilities << @configuration_ability
        @role.abilities << @import_devices_ability

        @parent_organization.add_child(@organization)
        @organization.add_child(@child_organization)
      end

      context 'when given nil' do
        it 'returns an empty array' do
          expect(manager.abilities_for(nil)).to eq []
        end
      end

      context 'when the manager has no permissions' do
        it 'returns an empty array' do
          expect(manager.abilities_for(@organization)).to eq []
        end
      end

      context 'when the manager has a permission on the resource' do
        before do
          @org_abilities = []
          @org_abilities << @organization_ability
          @org_abilities << @user_ability
          @org_abilities << @device_ability
          @org_abilities << @configuration_ability

          @org_role = create(:role, organization: @organization)

          @org_abilities.each do |ability|
            @org_role.abilities << ability
          end

          Permission.create!(
            manager:    organization,
            manageable: @organization,
            role:       @role
          )

          unless manager == organization
            Permission.create!(
              manager:    manager,
              manageable: @organization,
              role:       @role
            )
          end
        end

        it 'can access the abilities for the permission' do
          expect(manager.abilities_for(@organization).sort).to eq @role.abilities.sort
        end

        context 'when the manager has a permission on the child organization' do
          before do
            @child_org_abilities = []
            @child_org_abilities << @import_devices_ability

            @child_org_role = create(:role, organization: @child_organization)

            @child_org_abilities.each do |ability|
              @child_org_role.abilities << ability
            end

            Permission.create(manager: manager, manageable: @child_organization, role: @child_org_role)
          end

          context 'when getting abilities for the child organization' do
            it 'can only access the abilities it has for the permission' do
              expect(manager.abilities_for(@child_organization).sort).to eq @child_org_abilities.sort
            end

            it 'does not have access to the abilities in the organization above it' do
              @org_abilities.each do |ability|
                expect(manager.abilities_for(@child_organization)).not_to include(ability)
              end
            end
          end
        end
      end
    end

    describe '#manageable_organizations' do
      before do
        if user_manager?
          Permission.create!(
            manager:    manager.organization,
            manageable: organization,
            role:       organization.admin_role
          )
        end
        Permission.create!(
          manager:    manager,
          manageable: organization,
          role:       organization.admin_role
        )
        expect(manager.manager_permissions.count).to be 1
      end

      it 'returns organizations for which permissions exist for the manager' do
        expect(manager.manageable_organizations).to eq([organization])
      end
    end

    describe '#head_manageable_organizations' do
      it 'retrieves a list of manageable organizations' do
        expect(manager).to receive(:manageable_organizations).and_return([])
        manager.head_manageable_organizations
      end

      it 'delegates to the Organization class to do the processing' do
        organizations = [Organization.new]
        allow(manager).to receive(:manageable_organizations).and_return(organizations)
        expect(Organization).to receive(:head_organizations_in).with(organizations)
        manager.head_manageable_organizations
      end
    end
  end
end
