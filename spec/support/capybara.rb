# frozen_string_literal: true

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.javascript_driver = :selenium_chrome_headless

Capybara.raise_server_errors = false
