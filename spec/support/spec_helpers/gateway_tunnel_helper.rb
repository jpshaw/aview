# frozen_string_literal: true

module GatewayTunnelHelper
  # Public: Creates a hash of probe results for the given number of
  # Gateway tunnels.
  #
  # n - The Integer number of tunnels to create results for.
  #
  # Returns a Hash of probe results for the given tunnels.
  def snmp_results_for_tunnel_count(n, mac = nil, tunnel_type = 6)
    results = {}

    results['1.3.6.1.2.1.1.5.0'] = mac

    (1..n).each do |i|
      results["1.3.6.1.4.1.74.1.30.6.1.1.2.#{i}"]  = "ipsec#{i - 1}" # ipsec iface
      results["1.3.6.1.4.1.74.1.30.6.1.1.5.#{i}"]  = 123_456 # duration
      results["1.3.6.1.4.1.74.1.30.6.1.1.6.#{i}"]  = "135.120.40.#{50 + i}" # endpoint
      results["1.3.6.1.4.1.74.1.30.6.1.1.19.#{i}"] = 'fe80::1293:e9ff:fe09:cd48' # endpoint ipv6
      results["1.3.6.1.4.1.74.1.30.6.1.1.7.#{i}"]  = tunnel_type # endpoint type
      results["1.3.6.1.4.1.74.1.30.6.1.1.13.#{i}"] = 1 # auth type
      results["1.3.6.1.4.1.74.1.30.6.1.1.16.#{i}"] = 1 # server mgmt
      results["1.3.6.1.4.1.74.1.30.6.1.1.17.#{i}"] = 2 # initiator
      results["1.3.6.1.4.1.74.1.30.6.1.1.18.#{i}"] = 4 # mode
    end

    results
  end
end
