# frozen_string_literal: true

module LogInHelper
  def log_in_user(user)
    visit root_url
    expect(current_path).to eq '/users/sign_in'
    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: '123456'
    click_on 'Login'
  end

  def log_in_for_request_spec(user)
    post user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
  end
end
