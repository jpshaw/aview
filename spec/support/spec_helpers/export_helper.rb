# frozen_string_literal: true

module ExportHelper
  def intercept_send_data_results(data_table_klass)
    allow_any_instance_of(DataTablesController).to receive(:send_data) do |controller|
      @results = CSV.parse(data_table_klass.new(controller.view_context).to_csv)
      controller.render nothing: true, status: :ok
    end
  end
end
