# frozen_string_literal: true

module LayoutHelper
  def ie_user_agent(version = 8)
    "Mozilla/4.0 (Windows; MSIE #{version}.0; Windows NT 5.2)"
  end
end
