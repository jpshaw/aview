# frozen_string_literal: true

module ReportHelper
  def stub_report_file
    file = StringIO.new
    allow(File).to receive(:atomic_write).and_yield(file)
    allow(File).to receive(:exists?).and_return(true)
    allow(File).to receive(:read).and_return(file.string)
  end
end
