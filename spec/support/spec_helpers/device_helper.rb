# frozen_string_literal: true

module DeviceHelper
  def device_show_page_check
    current_path.should match "/devices/#{@device.mac}"
  end
end
