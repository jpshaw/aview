# frozen_string_literal: true

module NetbridgeConfigurationHelper
  def random_config_pairs(keys = nil, count = 2)
    keys ||= random_config_keys(count)
    keys.reduce({}) do |h, k|
      h.merge(k => Random.rand(10_000))
    end
  end

  def random_config_keys(count = 2)
    NetbridgeConfiguration::ATTRIBUTES_KEYS.sample(count)
  end

  def random_attributes(count = 5)
    attributes = {}

    # Needed for checking the column type
    config = NetbridgeConfiguration.new

    random_config_keys(count).each do |key|
      case config.column_for_attribute(key).type
      when :string, :text
        attributes[key] = 'test'
      when :integer, :float, :decimal
        attributes[key] = 1
      when :boolean
        attributes[key] = true
      else
        attributes[key] = nil
      end
    end

    attributes
  end
end
