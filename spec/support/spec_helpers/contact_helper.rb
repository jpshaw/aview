# frozen_string_literal: true

module ContactHelper
  def create_contact_from_form(name = '', phone = '', email = '')
    fill_in 'contact_name', with: name
    fill_in 'contact_phone', with: phone
    fill_in 'contact_email', with: email
    click_button 'contact_create_button'
  end
end
