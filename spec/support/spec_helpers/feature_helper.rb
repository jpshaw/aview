# frozen_string_literal: true

module FeatureHelper
  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def wait_for_animation
    sleep 1.0 unless Capybara.current_driver == 'poltergeist'
  end

  def wait_for_page_to_load
    sleep 0.8 # there must be a better way but I failed to find it. This is not needed with the selenium js driver.
  end

  def wait_for_jquery_to_load
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until evaluate_script('(typeof $ != "undefined")')
    end
  end

  def finished_all_ajax_requests?
    page.evaluate_script('jQuery.active').zero?
  end

  def click_animated_element(element)
    if Capybara.current_driver == 'poltergeist'
      element.trigger('click')
    else
      sleep 1.0
      element.click
    end
  end
end
