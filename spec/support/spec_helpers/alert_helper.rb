# frozen_string_literal: true

module AlertHelper
  def alert_success(msg = '')
    find('.alert.alert-success').should have_content(msg)
  end

  def no_alert_success
    expect(subject).not_to have_css('.alert.alert-success')
  end

  def alert_notice(msg = '')
    find('.alert.alert-notice').should have_content(msg)
  end

  def no_alert_notice
    expect(subject).not_to have_css('.alert.alert-notice')
  end

  def alert_error(msg = '')
    find('.alert.alert-error').should have_content(msg)
  end

  def no_alert_error
    expect(subject).not_to have_css('.alert.alert-error')
  end
end
