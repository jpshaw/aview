# frozen_string_literal: true

module CreateResourceHelper
  def create_account
    @account = create(:account)
  end

  def create_country
    @country = create(:country)
  end

  def create_location
    create_country if @country.nil?
    @location = create(:location, country: @country)
  end

  def create_abilities
    # Default Abilities
    @site_ability = create(:site_ability)
    @organization_ability = create(:organization_ability)
    @user_ability = create(:user_ability)
    @device_ability = create(:device_ability)
    @configuration_ability = create(:configuration_ability)
    @device_configuration_ability = create(:device_configuration_ability)
    @deploy_devices_ability = create(:deploy_devices_ability)
    @send_commands_ability = create(:send_device_commands_ability)
    @assign_non_production_firmware_ability = create(:assign_non_production_firmwares_ability)
    @change_location_ability = create(:change_device_location_ability)
    # Admin Abilities
    @import_devices_ability = create(:import_devices_ability)
    @replace_devices_ability = create(:replace_devices_ability)
    @view_admin_panel_ability = create(:view_admin_panel_ability)
  end

  def create_gateway_category_and_models
    @gateway_category = create(:gateway_category)
    @gateway_8100 = create(:gateway_8100_model, category: @gateway_category)
    @gateway_8200 = create(:gateway_8200_model, category: @gateway_category)
    @gateway_8300 = create(:gateway_8300_model, category: @gateway_category)
  end

  def create_netreach_category_and_models
    @netreach_category = create(:netreach_category)
    @netreach_4200_NX = create(:netreach_4200_NX_model, category: @netreach_category)
  end

  def create_netbridge_category_and_models
    @netbridge_category = create(:netbridge_category)
    @netbridge_FX = create(:netbridge_FX_model, category: @netbridge_category)
    @netbridge_SX = create(:netbridge_SX_model, category: @netbridge_category)
    @netbridge_6300_DX = create(:netbridge_6300_DX_model, category: @netbridge_category)
  end

  def create_dial_to_ip_category_and_models
    @dial_to_ip_category = create(:dial_to_ip_category)
    @dial_to_ip_5300_DC = create(:dial_to_ip_5300_DC_model, category: @dial_to_ip_category)
  end

  def create_cellular_category_and_models
    @cellular_category  = create(:cellular_category)
    @cellular_6300_CX   = create(:cellular_6300_CX_model, category: @cellular_category)
    @cellular_6300_LX   = create(:cellular_6300_LX_model, category: @cellular_category)
  end

  def create_remote_manager_category_and_models
    @remote_manager_category  = create(:remote_manager_category)
    @remote_manager_5400_RM   = create(:remote_manager_5400_RM_model, category: @remote_manager_category)
  end

  def create_ucpe_category_and_models
    @ucpe_category = create(:ucpe_category)
    @ucpe_9400_UA  = create(:ucpe_9400_UA_model, category: @ucpe_category)
  end

  def create_all_categories_and_models
    create_gateway_category_and_models
    create_netreach_category_and_models
    create_netbridge_category_and_models
    create_cellular_category_and_models
    create_dial_to_ip_category_and_models
    create_remote_manager_category_and_models
    create_ucpe_category_and_models
  end

  def create_all_categories
    @gateway_category = create(:gateway_category)
    @netreach_category = create(:netreach_category)
    @netbridge_category = create(:netbridge_category)
    @dial_to_ip_category = create(:dial_to_ip_category)
    @remote_manager_category = create(:remote_manager_category)
    @ucpe_category = create(:ucpe_category)
  end

  def create_roles
    @controller_role  = create(:admin_role_with_sequenced_name, organization: @organization)
    @viewer_role      = create(:viewer_role, organization: @organization)

    @controller_role.abilities << @site_ability
    @controller_role.abilities << @organization_ability
    @controller_role.abilities << @user_ability
    @controller_role.abilities << @device_ability
    @controller_role.abilities << @configuration_ability
  end

  def create_contact
    @contact = create(:contact)
  end

  def created_linked_contact
    create_organization if @organization.nil?
    @contact = create(:contact)
    @organization.contacts << @contact
  end

  def create_netbridge_configuration
    @netbridge_config = create(:netbridge_configuration)
  end

  def create_netreach_configuration
    @netreach_configuration = create(:netreach_configuration)
  end

  def create_organization
    create_abilities
    @organization = create(:organization)
    create_roles
  end

  def create_organization_with_all_abilities
    create_abilities
    @organization = create(:organization_with_all_abilities)
    create_roles
  end

  # Only for creating a second organization, must run create_organization first
  def create_child_boundary_organization
    create_organization if @organization.nil?
    @new_boundary_organization = create(:random_boundary_organization)
    @organization.children << @new_boundary_organization
  end

  def create_child_regular_organization
    create_organization if @organization.nil?
    @new_regular_organization = create(:random_regular_organization)
    @organization.children << @new_regular_organization
  end

  def create_user
    create_organization if @organization.nil?
    @user = create(:user_with_org_permission, organization: @organization)
    @timezone = @user.timezone
    @user
  end

  def create_user_with_all_abilities
    create_organization_with_all_abilities if @organization.nil?
    @user = create(:user_with_all_abilities, organization: @organization, name: Faker::Name.name)
    @timezone = @user.timezone
    @user
  end

  def create_armt_controller_user
    create_organization_with_all_abilities if @organization.nil?
    @user = create(:user_with_armt_controller_abilities, organization: @organization)
    @timezone = @user.timezone
  end

  def create_viewer_user
    create_organization if @organization.nil?
    @user = create(:user_with_org_permission, organization: @organization)
    @timezone = @user.timezone
  end

  def create_device
    create_organization if @organization.nil?
    @device = create(:test_device, :up, organization: @organization)
  end

  def create_down_device
    create_organization if @organization.nil?
    @device = create(:test_device, :down, organization: @organization)
  end

  def create_deployed_device
    create_organization if @organization.nil?
    @device = create(:test_device, :deployed, organization: @organization)
    allow_any_instance_of(Device).to receive(:category).and_return(build(:test_category))
    allow_any_instance_of(Device).to receive(:series).and_return(build(:test_model))
  end

  def create_netreach_device
    create_organization if @organization.nil?
    @device = create(:netreach_device, :up, organization: @organization)
  end

  def create_netbridge_device
    create_organization if @organization.nil?
    @device = create(:netbridge_device, :up, organization: @organization)
  end

  def create_dial_to_ip_device
    create_organization if @organization.nil?
    @device = create(:dial_to_ip_device, :up, organization: @organization)
  end

  def create_remote_manager_device
    create_organization if @organization.nil?
    @device = create(:remote_manager_device, :up, organization: @organization)
  end

  def create_ucpe_device
    create_organization if @organization.nil?
    @device = create(:ucpe_device, :up, organization: @organization)
  end

  def create_undeployed_devices
    create_organization if @organization.nil?
    create_list(:test_device, 5, :undeployed, organization: @organization)
  end

  def create_gateway_category
    @gateway_category = create(:gateway_category)
  end

  def create_gateway_event_uuids
    create_gateway_category if @gateway_category.nil?
    @uuids = []
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_UP, name: 'Device up')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_REBOOTED, name: 'Device rebooted')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_IP_CHANGED, name: 'Device primary ip changed')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_UNREACHABLE, name: 'Device unreachable')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_DBU_FAILED,  name: 'Backup WAN autotest failed')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_DBU_THRESHOLD, name: 'Backup WAN Threshold reached')
    @uuids << create(:uuid, category: @gateway_category, code: EventUUIDS::GATEWAY_CELL_LOCATION_CHANGED, name: 'Device cellular location changed')
  end

  def create_profile_for_gateway_notifications_with_profile_and_frequency(profile, frequency)
    create_user if @user.nil?
    create_gateway_event_uuids if @uuids.nil?
    @profile = create(profile, user: @user)
    @uuids.each do |uuid|
      @profile.subscribed_events.create(uuid_id: uuid.id, frequency: frequency)
    end
    @profile
  end

  def create_profile_for_hourly_gateway_notifications
    create_profile_for_gateway_notifications_with_profile_and_frequency(:hourly_notification_profile, NotificationProfile::Frequencies::EVERY_HOUR)
  end

  def create_profile_for_immediate_gateway_notifications
    create_profile_for_gateway_notifications_with_profile_and_frequency(:immediate_notification_profile, NotificationProfile::Frequencies::IMMEDIATELY)
  end

  def create_profile_for_no_gateway_notifications
    create_profile_for_gateway_notifications_with_profile_and_frequency(:no_notification_profile, NotificationProfile::Frequencies::NEVER)
  end

  def create_default_profile_for_immediate_gateway_notifications
    create_profile_for_immediate_gateway_notifications
    @profile.update(name: 'Default')
    @profile
  end

  def create_profile_with_no_subscribed_events
    create_user if @user.nil?
    create_gateway_event_uuids if @uuids.nil?
    @profile = create(:immediate_notification_profile, user: @user)
  end

  def create_subscriptions_for_gateway_device
    create_user if @user.nil?
    create_device if @device.nil?
    create_profile_for_immediate_gateway_notifications if @profile.nil?
    @device_subscription = create(:device_subscription, user: @user, publisher_id: @device.id, profile: @profile)
    @org_subscription = create(:organization_subscription, user: @user, publisher_id: @organization.id, profile: @profile)
    @subscriptions = [@device_subscription, @org_subscription]
  end

  def create_netbridge_event_uuids
    @netbridge_category ||= create(:netbridge_category)
    @uuids = []
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_UP, name: 'Device up')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_REBOOTED, name: 'Device rebooted')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_IP_CHANGED, name: 'Device primary ip changed')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_CNTI_CHANGED, name: 'Device network type changed')
    @uuids << create(:uuid, category: @netbridge_category, code: EventUUIDS::NETBRIDGE_UNREACHABLE, name: 'Device unreachable')
  end

  def create_netreach_event_uuids
    @netreach_category ||= create(:netreach_category)
    @uuids = []
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_UP, name: 'Device up')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_REBOOTED, name: 'Device rebooted')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_IP_CHANGED, name: 'Device ip changed')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_UNREACHABLE, name: 'Device unreachable')
    @uuids << create(:uuid, category: @netreach_category, code: EventUUIDS::NETREACH_ROGUE_ACCESS_POINT, name: 'Device detected rogue Access Point')
  end

  def create_remote_manager_event_uuids
    @remote_manager_category ||= create(:remote_manager_category)
    @uuids = []
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_UP, name: 'Device up')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_IP_CHANGED, name: 'Device primary ip changed')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_CNTI_CHANGED, name: 'Device network type changed')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_UNREACHABLE, name: 'Device unreachable')
    @uuids << create(:uuid, category: @remote_manager_category, code: EventUUIDS::REMOTEMANAGER_WAN_CHANGED, name: 'Device WAN interface changed')
  end

  def create_dial_to_ip_event_uuids
    @dial_to_ip_category ||= create(:dial_to_ip_category)
    @uuids = []
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_UP, name: 'Device up')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_IP_CHANGED, name: 'Device primary ip changed')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_CNTI_CHANGED, name: 'Device network type changed')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_UNREACHABLE, name: 'Device unreachable')
    @uuids << create(:uuid, category: @dial_to_ip_category, code: EventUUIDS::DIALTOIP_WAN_CHANGED, name: 'Device WAN interface changed')
  end

  def create_ucpe_event_uuids
    @ucpe_category ||= create(:ucpe_category)
    @uuids = []
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_UP, name: 'Device up')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_DOWN, name: 'Device down')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_FW_CHANGED, name: 'Device firmware changed')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_IP_CHANGED, name: 'Device primary ip changed')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_REBOOTED, name: 'Device rebooted')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_UNREACHABLE, name: 'Device unreachable')
    @uuids << create(:uuid, category: @ucpe_category, code: EventUUIDS::UCPE_WAN_CHANGED, name: 'Device WAN interface changed')
  end

  def create_device_event(data = {})
    create_device if @device.nil?
    data ||= {}
    data[:device] = @device
    @event = create(:event, data)
  end

  def create_immediate_notification_for_device_subscription
    create_subscriptions_for_gateway_device if @subscriptions.nil?
    create_device_event if @event.nil?
    @notification = @device_subscription.notifications.create(user_id: @user.id, event_id: @event.id)
  end

  def create_dial_to_ip_device_configurations
    create_dial_to_ip_category_and_models
    firmware_1 = create(:device_firmware, device_model: @dial_to_ip_5300_DC)
    firmware_2 = create(:device_firmware, device_model: @dial_to_ip_5300_DC)
    @device_config_1 = create(:device_configuration, device_firmware: firmware_1)
    @device_config_2 = create(:device_configuration, device_firmware: firmware_2)
  end
end
