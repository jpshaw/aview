# frozen_string_literal: true

module LocationHelper
  def create_location_from_form(address_1 = '', address_2 = '', city = '', state = '', postal_code = '')
    fill_in 'location_address_1', with: address_1
    fill_in 'location_address_2', with: address_2
    fill_in 'location_city', with: city
    fill_in 'location_state_abbr', with: state
    fill_in 'location_postal_code', with: postal_code
    select 'United States', from: 'location[country_id]'
    click_button 'location_submit_button'
  end
end
