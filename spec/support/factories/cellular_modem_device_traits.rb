# frozen_string_literal: true

FactoryBot.define do
  trait :att_modem_carrier do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, carrier: 'AT&T'
    end
  end

  trait :verizon_modem_carrier do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, carrier: 'Verizon'
    end
  end

  trait :strong_modem_signal do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, signal: -40
    end
  end

  trait :weak_modem_signal do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, signal: -80
    end
  end

  trait :lte_modem_cnti do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, cnti: 'LTE'
    end
  end

  trait :umts_modem_cnti do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, cnti: 'UMTS'
    end
  end

  trait :high_modem_number do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, number: '9876543210'
    end
  end

  trait :low_modem_number do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, number: '1234567890'
    end
  end

  trait :high_modem_iccid do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, iccid: '89014102276124176941'
    end
  end

  trait :low_modem_iccid do
    after(:create) do |cellular_device|
      create :device_modem, device: cellular_device, iccid: '89014102276124176940'
    end
  end
end
