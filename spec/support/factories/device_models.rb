# frozen_string_literal: true

FactoryBot.define do
  factory :device_model do
    category_id { 1 }
  end
end
