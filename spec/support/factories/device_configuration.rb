# frozen_string_literal: true

FactoryBot.define do
  factory :device_configuration do
    organization    factory: :random_regular_organization
    sequence(:name) { |n| format('Device Configuration_%03d', n) }
    device_firmware
    description { '{}' }
    compiled_settings { '{}' }

    trait :with_sequenced_device_model do
      association :device_firmware, factory: %i[device_firmware with_sequenced_device_model]
    end

    trait :with_sub_organization do
      organization factory: :sub_organization
    end

    trait :with_sequenced_device_model_and_alternate_firmware do
      association :device_firmware, factory: %i[device_firmware with_sequenced_device_model with_alternate_schema]
    end

    trait :with_sequenced_device_model_and_test_firmware do
      association :device_firmware, factory: %i[device_firmware with_sequenced_device_model with_test_schema]
    end

    trait :with_basic_settings do
      settings do
        '{
          "auth" : {
            "user" : {
              "root" : {
                "password" : "$2a$05$gndmVv1gMcbhSV8OMWBE1eGjnViBO2Y3yxrRyyHAszPmzXkgefjgC"
              }
            }
          },
          "firmware" : {
            "version" : "16.1.100"
          },
          "schema" : {
            "version" : "164"
          }
        }'
      end
      compiled_settings { settings }
    end

    factory :device_configuration_with_data do
      compiled_settings do
        {
          schema: { version: '58' },
          firmware: { version: '15.5.9' }
        }.to_json
      end
    end

    factory :device_configuration_with_monitored_serial do
      compiled_settings do
        {
          schema: { version: '58' },
          firmware: { version: '15.5.9' },
          serial: {
            port1: { label: 'Test Port 1', monitor: { cts: true, dcd: true } }
          }
        }.to_json
      end
    end

    factory :device_configuration_with_unmonitored_serial do
      compiled_settings do
        {
          schema: { version: '58' },
          firmware: { version: '15.5.9' },
          serial: {
            port1: { label: 'Test Port 1' }
          }
        }.to_json
      end
    end
  end
end
