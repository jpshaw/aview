# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    association :event, factory: :device_event
    association :user, factory: :user
    association :notification_subscription, factory: :organization_subscription
  end
end
