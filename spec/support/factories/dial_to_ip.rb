FactoryBot.define do
  factory :dial_to_ip_device, :parent => :device, :class => DialToIp do

    association :organization, :factory => :random_boundary_organization

    sequence(:serial) do |i|
      serial_base = "530001#{i + 10000000}".to_i * 100
      check_digit = (98 - ((serial_base) % 97)) % 97
      serial = serial_base + check_digit
      serial
    end
    sequence(:mac) { |i| "002705#{i + 100000}" }

    object_type "DialToIp"
    hw_version "5300-DC"
    firmware "1.0.0"

  end
end
