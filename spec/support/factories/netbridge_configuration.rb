# frozen_string_literal: true

FactoryBot.define do
  factory :netbridge_configuration do
    association :organization
    sequence (:name) { |i| format('config %06d', i) }

    trait :with_sub_organization do
      association :organization, factory: :sub_organization
    end

    factory :invalid_netbridge_configuration do
      name { nil }
    end
  end
end
