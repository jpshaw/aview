# frozen_string_literal: true

FactoryBot.define do
  factory :model, class: DeviceModel do
    association :category

    factory :test_model do
      name        { 'Test' }
      description { 'Test Default Model' }
      class_name  { 'TestDevice' }
      model_version { 'Test' }
      category factory: :test_category
    end

    factory :test_model_with_sequenced_name do
      sequence(:name) { |i| format('Test Model %03d', i) }
      description { 'Test Default Model' }
      sequence(:class_name) { |i| format('Test Device_%03d', i) }
      model_version { 'Test' }
      category    factory: :test_category_with_sequenced_name
    end

    factory :gateway_8100_model do
      name        { '8100' }
      description { 'Gateway 8100 Model' }
      class_name  { 'Gateway' }
      model_version { '8100' }
      category    factory: :gateway_category
    end

    factory :gateway_8200_model do
      name        { '8200' }
      description { 'Gateway 8200 Model' }
      class_name  { 'Gateway' }
      model_version { '8200' }
      category    factory: :gateway_category
    end

    factory :gateway_8300_model do
      name        { '8300' }
      description { 'Gateway 8300 Model' }
      class_name  { 'Gateway' }
      model_version { '8300' }
      category    factory: :gateway_category
    end

    factory :netreach_4200_NX_model do
      name        { '4200-NX' }
      description { 'NetReach 4200-NX Model' }
      class_name  { 'Netreach' }
      model_version { '4200-NX' }
      category    factory: :netreach_category
    end

    factory :netbridge_FX_model do
      name        { '6200-FX' }
      description { 'NetBridge 6200-FX Model' }
      class_name  { 'Netbridge' }
      model_version { '6200-FX' }
      category    factory: :netbridge_category
    end

    factory :netbridge_SX_model do
      name        { 'SX' }
      description { 'NetBridge SX Model' }
      class_name  { 'Netbridge' }
      model_version { 'SX' }
      category    factory: :netbridge_category
    end

    factory :netbridge_6300_DX_model do
      name        { '6300 DX' }
      description { 'NetBridge 6300 DX Model' }
      class_name  { 'Netbridge' }
      model_version { '6300 DX' }
      category    factory: :netbridge_category
    end

    factory :dial_to_ip_5300_DC_model do
      name        { '5300-DC' }
      description { 'Dial-to-IP 5300-DC Model' }
      class_name  { 'DialToIp' }
      model_version { '5300-DC' }
      category    factory: :dial_to_ip_category
    end

    factory :dial_to_ip_5301_DC_model do
      name        { '5301-DC' }
      description { 'Dial-to-IP 5301-DC Model' }
      class_name  { 'DialToIp' }
      model_version { '5301-DC' }
      category    factory: :dial_to_ip_category
    end

    factory :cellular_6300_CX_model do
      name        { '6300-CX' }
      description { 'Cellular 6300-CX Model' }
      class_name  { 'Cellular' }
      model_version { '6300-CX' }
      category    factory: :cellular_category
    end

    factory :cellular_6300_LX_model do
      name        { '6300-LX' }
      description { 'Cellular 6300-LX Model' }
      class_name  { 'Cellular' }
      model_version { '6300-LX' }
      category    factory: :cellular_category
    end

    factory :remote_manager_5400_RM_model do
      name        { '5400-RM' }
      description { 'Remote Manager 5400-RM Model' }
      class_name  { 'RemoteManager' }
      model_version { '5400-RM' }
      category    factory: :remote_manager_category
    end

    factory :ucpe_9400_UA_model do
      name        { '9400-UA' }
      description { 'uCPE 9400-UA Model' }
      class_name  { 'Ucpe' }
      model_version { '9400-UA' }
      category factory: :ucpe_category
    end
  end
end
