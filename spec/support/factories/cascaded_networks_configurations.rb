# frozen_string_literal: true

FactoryBot.define do
  factory :cascaded_networks_configuration do
    data do
      begin
      {
        cascaded_networks: [
          {
            id: 1,
            ip_address: '192.168.2.1',
            subnet_mask: '255.255.255.1',
            router_ip_address: '192.168.2.1',
            description: 'ORIGINAL',
            local_lan_alias: true,
            internet_only_route: false,
            routing_metric: '',
            restrict_vpn_advertisement: nil,
            history: {
              created_by: 'Big Bird',
              created_on: '2/2/1920',
              updated_by: 'Oscar the Grouch',
              updated_on: '3/3/2020'
            }
          }
        ]
      }
    end
    end
  end
end
