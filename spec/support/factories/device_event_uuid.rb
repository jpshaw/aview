# frozen_string_literal: true

FactoryBot.define do
  factory :device_event_uuid, class: DeviceEventUUID do
    sequence(:name) { |n| "Event UUID #{n}" }
    category_id { 1 }
    code { 1 }
  end
end
