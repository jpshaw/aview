# frozen_string_literal: true

FactoryBot.define do
  factory :contact do
    sequence(:name) { |i| "Test User #{i}" }
    sequence(:token) { |_i| Contact.tokenize(name: name, phone: phone, email: email) }
    sequence(:phone) { |i| (i + 1_000_000_000).to_s }
    sequence(:email) { |i| "test.user#{i + 100_000}@accelerated.com" }
    label { 0 }
  end
end
