# frozen_string_literal: true

FactoryBot.define do
  factory :netreach_device, parent: :device, class: Netreach do
    association :organization, factory: :random_boundary_organization
    sequence(:mac) { |i| "00C0CA#{i + 100_000}" }

    object_type { 'Netreach' }
    hw_version { '4200-NX' }
    firmware { '1.672.91' }
    is_deployed { true }

    factory :netreach_device_with_random_regular_organization do
      organization factory: :random_regular_organization
    end
  end
end
