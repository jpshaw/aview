# frozen_string_literal: true

FactoryBot.define do
  factory :internet_routes_configuration do
    data do
      begin
      {
        'routes' => [
          {
            'ip_address' => '192.168.1.1',
            'subnet' => '255.255.255.1',
            'description' => 'Number 1'
          },
          {
            'ip_address' => '192.168.1.2',
            'subnet' => '255.255.255.2',
            'description' => 'Number 2'
          }
        ]
      }
    end
    end
  end
end
