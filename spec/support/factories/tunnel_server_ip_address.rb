# frozen_string_literal: true

FactoryBot.define do
  factory :tunnel_server_ip_address do
    sequence(:ip_address) { |i| "127.0.0.#{i}" }
    tunnel_server factory: :tunnel_server
  end
end
