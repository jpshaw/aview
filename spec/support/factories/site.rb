# frozen_string_literal: true

FactoryBot.define do
  factory :site do
    association :organization
    sequence (:name) { |i| format('Site %03d', i) }

    factory :site_with_sub_organization do
      association :organization, factory: :sub_organization
    end

    factory :site_with_random_regular_organization do
      organization factory: :random_regular_organization
    end
  end
end
