# frozen_string_literal: true

FactoryBot.define do
  factory :category, class: DeviceCategory, aliases: [:device_category] do
    factory :test_category do
      sequence(:name) { |n| format('Test_%03d', n) }
      description { 'Test Device Category' }
      class_name { 'TestDevice' }

      factory :test_category_with_sequenced_name do
        sequence(:class_name) { |n| format('TestDevice_%03d', n) }
      end
    end

    factory :gateway_category do
      name { DeviceCategory::GATEWAY_CATEGORY }
      description { 'Gateway Device Category' }
      class_name { 'Gateway' }
    end

    factory :netreach_category do
      name { DeviceCategory::WIFI_CATEGORY }
      description { 'NetReach Device Category' }
      class_name { 'Netreach' }
    end

    # TODO: Deprecate and remove the Netbridge Category.
    factory :netbridge_category do
      name { 'NetBridge' }
      description { 'NetBridge Device Category' }
      class_name { 'Netbridge' }
    end

    factory :legacy_cellular_category do
      name { DeviceCategory::LEGACY_CELLULAR_CATEGORY }
      description { 'Legacy Cellular Device Category' }
      class_name { 'Legacy Cellular' }
    end

    factory :cellular_category do
      name { DeviceCategory::CELLULAR_CATEGORY }
      description { 'Cellular Device Category' }
      class_name { 'Cellular' }
    end

    factory :dial_to_ip_category do
      name { DeviceCategory::DIAL_TO_IP_CATEGORY }
      description { 'Dial-to-IP Device Category' }
      class_name { 'DialToIp' }
    end

    factory :remote_manager_category do
      name { DeviceCategory::REMOTE_MANAGER_CATEGORY }
      description { 'Remote Manager Device Category' }
      class_name { 'RemoteManager' }
    end

    factory :ucpe_category do
      name { DeviceCategory::UCPE_CATEGORY }
      description { 'uCPE Device Category' }
      class_name { 'Ucpe' }
    end
  end
end
