# frozen_string_literal: true

FactoryBot.define do
  factory :rogue_access_point do
    device_id { 1 }
    sequence(:ssid) { |i| "SSID #{i}" }
    sequence(:mac)  { |i| "00C0CA#{i + 100_000}" }
    sequence(:ip)   { |i| "10.100.10.#{i}" }
  end
end
