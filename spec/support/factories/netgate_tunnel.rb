# frozen_string_literal: true

FactoryBot.define do
  factory :netgate_tunnel do
    association :netgate
  end
end
