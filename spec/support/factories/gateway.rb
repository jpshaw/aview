FactoryBot.define do
  factory :gateway_device, :parent => :device, :class => Gateway do
    association :organization, :factory => :random_boundary_organization
    sequence(:mac) { |i| "00D0CF#{i + 100000}" }

    object_type "Gateway"
    hw_version "8200"
    firmware "5.3.105"
    is_deployed true

    trait :support_tunnel_status do
      firmware Gateway::TUNNEL_STATS_FIRMWARE_MIN
    end

    trait :firmware_6_2 do
      firmware '6.2.0'
    end

    trait :with_details do
      after(:create) do |gateway|
        create :gateway_details, device: gateway
      end
    end

    trait :static_connection_type do
      after(:create) do |gateway|
        create :gateway_details, device: gateway, active_conn_type: 1
      end
    end

    trait :dhcp_connection_type do
      after(:create) do |gateway|
        create :gateway_details, device: gateway, active_conn_type: 2
      end
    end

    trait :commandable do
      firmware Gateway::COMMANDS_FIRMWARE_MIN
      mgmt_host "https://testing.accns.com"
    end

    trait :json_commandable do
      firmware Gateway::JSON_COMMANDS_FIRMWARE_MIN
    end

    trait :vlan_commandable do
      up
      json_commandable
      is_deployed true
    end

    trait :with_tunnel do
      after(:create) do |gateway|
        create :gateway_tunnel_outbound, device: gateway
      end
    end

    factory :blank_gateway_device do
      hw_version nil
      firmware nil
      site_id nil

      factory :blank_gateway_device_with_details do
        after(:create) do |gateway|
          create :gateway_details, device: gateway
        end
      end
    end

    factory :gateway_device_with_configuration do
      association :configuration, :factory => :gateway_configuration
      after(:create) do |gateway|
        create :gateway_details, device: gateway
      end
    end
  end
end
