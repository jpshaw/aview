# frozen_string_literal: true

FactoryBot.define do
  factory :device_association do
    sequence(:source_id, 10_000)
    sequence(:target_id, 100_000)
    sequence(:source_mac) { generate :mac_address }
    sequence(:target_mac) { generate :mac_address }
    details {}
  end

  factory :device_association_with_details, parent: :device_association do
    ip_address { Faker::Internet.ip_v4_address }
    source_interface { "eth#{Faker::Number.digit}" }
    target_interface { "eth#{Faker::Number.digit}" }
    source_type { %w[wan lan].sample }
    target_type { %w[wan lan].sample }
  end
end
