# frozen_string_literal: true

FactoryBot.define do
  factory :customer_service do
    sequence(:name) { |n| "Service_Name_#{n}" }
  end
end
