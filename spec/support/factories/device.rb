FactoryBot.define do

  class TestDevice < Device
    has_mac_range 0xAABBCC000000..0xAABBCCFFFFFF
  end

  class TestDeviceWithModem < Device
    has_mac_range 0xDDEEFF000000..0xDDEEFFFFFFFF
    include CellularModem
  end

  factory :device do
    association :organization

    object_type "Device"

    trait :deployed do
      is_deployed true
    end

    trait :undeployed do
      is_deployed false
    end

    trait :up do
      deployed
      after(:create) { |device| device.heartbeat! }
    end

    trait :down do
      deployed
      after(:create) { |device| device.recovery_failed! }
    end

    trait :unreachable do
      deployed
      after(:create) { |device| device.unreachable! }
    end

    factory :test_device, :parent => :device, :class => TestDevice do
      association :organization
      sequence(:mac) { |i| "AABBCC#{i + 100000}" }
      object_type "TestDevice"
      hw_version "Test"

      trait :with_random_regular_organization_site do
        association :site, factory: :site_with_random_regular_organization
        before(:create) do |device|
          device.organization = device.site.organization
        end
      end

      trait :with_sub_organization_site do
        association :site, factory: :site_with_sub_organization
        before(:create) do |device|
          device.organization = device.site.organization
        end
      end

      trait :with_sub_organization do
        association :organization, :factory => :sub_organization
      end

      trait :with_sequenced_category do
        association :category, factory: :test_category_with_sequenced_name
      end

      trait :with_random_location do
        association :location, factory: :random_location
      end

      trait :model_8100 do
        association :series, factory: :gateway_8100_model
      end

      trait :model_5300_dc do
        association :series, factory: :dial_to_ip_5300_DC_model
      end

      trait :model_9400_ua do
        association :series, factory: :ucpe_9400_UA_model
      end

      trait :model_5400_rm do
        association :series, factory: :remote_manager_5400_RM_model
      end

      trait :with_sequenced_model do
        association :series, factory: :test_model_with_sequenced_name
      end

      factory :test_device_with_category do
        category  factory: :test_category_with_sequenced_name
      end
    end

    factory :test_device_with_modem_association, :parent => :device, :class => TestDeviceWithModem do
      association :organization, :factory => :random_boundary_organization
      sequence(:mac) { |i| "DDEEFF#{i + 100000}" }
      object_type "TestDeviceWithModem"
      hw_version "TestModem"

      trait :with_location do
        association :location
      end

      trait :with_valid_cell_geolocation do
        after(:build) do |device|
          device.modems.first_or_create
          device.modem.mcc = 310
          device.modem.mnc = 410
          device.modem.cid = "141631000"
          device.modem.lac = "65534"
        end
      end

      trait :with_invalid_cell_geolocation do
        after(:build) do |device|
          device.modems.first_or_create
          device.modem.mcc = 310
          device.modem.mnc = 410
          device.modem.cid = '31ED901'
          device.modem.lac = 'CC3C'
        end
      end
    end

    factory :device_with_random_regular_organization do
      organization factory: :random_regular_organization
      sequence(:mac) { |i| "AABBCC#{i + 100000}" }
    end

  end
end
