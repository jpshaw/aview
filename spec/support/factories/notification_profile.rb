# frozen_string_literal: true

FactoryBot.define do
  factory :notification_profile, class: NotificationProfile do
    sequence(:name) { |n| "Profile #{n}" }
    association :user

    factory :immediate_notification_profile do
      name { 'Immediate Profile' }
    end

    factory :no_notification_profile do
      name { "Don't bother me Profile" }
    end

    factory :hourly_notification_profile do
      name { 'Hourly profile' }
    end

    factory :invalid_notification_profile do
      name { nil }
    end
  end
end
