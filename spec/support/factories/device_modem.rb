# frozen_string_literal: true

FactoryBot.define do
  factory :device_modem do
    active { true }
    name { 'Pantech 290' }
    carrier { 'verizon' }
    signal { -65 }
    number { '2678005406' }
    imei { '990000479019087' }
    imsi { '311480023533244' }
    esn { '12806198302' }
    iccid { '89148000000231587388' }
    revision { nil }
    apn { 'vzwinternet' }
    ecio { nil }
    cnti { 'LTE' }
    cid { '66126862' }
    lac { '21991' }
    created_at { '2012-11-08 21:50:00 UTC' }
    updated_at { '2012-11-14 17:12:48 UTC' }
    rx { 348_717 }
    tx { 477_273 }
    rx_30 { 9224 }
    tx_30 { 13_496 }
    rsrp { -111.000 }
    rsrq { -15.000 }
    sinr { 9.000 }
    snr { -3.400 }
    usb_speed { 12 }

    trait :with_device do
      association :device, factory: :test_device
    end
  end
end
