# frozen_string_literal: true

FactoryBot.define do
  factory :cellular_location do
    sequence(:radio) { |i| "Radio #{i}" }
    sequence(:mcc) { |i| i + 100 }
    sequence(:mnc) { |i| i + 100 }
    sequence(:lac) { |i| i + 10_000 }
    sequence(:cid) { |i| i + 10_000_000 }
    lat { 27.9553 }
    lon { -82.4563 }
    accuracy { 2152 }
  end
end
