# frozen_string_literal: true

FactoryBot.define do
  # Creates a simple, sequenced, left-adjusted mac address
  #
  # Examples
  #
  #   generate(:mac_address)
  #   #=> "100000000000"
  #
  #   generate(:mac_address)
  #   #=> "200000000000"
  #
  #   100_000.times.map { generate(:mac_address) }.last
  #   #=> "1869F0000000"
  #
  sequence :mac_address do |n|
    n.to_s(16).ljust(12, '0').upcase
  end

  # Creates an Accelerated-prefixed, sequenced, left-adjusted mac address
  #
  # Examples
  #
  #   generate(:accelerated_mac)
  #   #=> "002704100000"
  #
  #   generate(:accelerated_mac)
  #   #=> "002704200000"
  #
  #   100_000.times.map { generate(:accelerated_mac) }.last
  #   #=> "0027041869F0"
  #
  sequence :accelerated_mac do |n|
    "002704#{n.to_s(16).ljust(6, '0').upcase}"
  end

  # Creates a Netbridge FX mac address
  #
  # Range: 002704010001..00270401FFFF
  sequence :netbridge_fx_mac do |n|
    "00270401#{(n + 1).to_s(16).rjust(4, '0').upcase}"
  end

  # Creates a Netbridge SX mac address
  #
  # Range: 002704000000..002704010000
  sequence :netbridge_sx_mac do |n|
    # Make sure we don't go above 002704010000, which is in FX range.
    # Note: 10000 hex is 65536 decimal
    "00270400#{(n % 65_536).to_s(16).rjust(4, '0').upcase}"
  end
end
