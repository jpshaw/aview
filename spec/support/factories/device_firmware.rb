# frozen_string_literal: true

FactoryBot.define do
  factory :device_firmware do
    device_model        factory: :test_model
    sequence(:version)  { |n| format('14.12.%03d', n) }
    schema_file { Rack::Test::UploadedFile.new(fixture('json_schema_valid.json')) }

    trait :with_sequenced_device_model do
      device_model factory: :test_model_with_sequenced_name
    end

    trait :with_alternate_schema do
      schema_file { Rack::Test::UploadedFile.new(fixture('json_schema_valid_alternate.json')) }
    end

    trait :with_test_schema do
      schema_file { Rack::Test::UploadedFile.new(fixture('json_schema_test.json')) }
    end

    trait :with_real_schema do
      schema_file { Rack::Test::UploadedFile.new(fixture('5301-DC-16.1.100-20160128-accns.schema')) }
    end
  end
end
