# frozen_string_literal: true

FactoryBot.define do
  factory :ability do
    group_id { AbilityGroups::DEFAULT }
    sequence(:name)         { |n| "#{Faker::Lorem.word}_#{n}" }
    sequence(:description)  { |n| "#{n}. #{Faker::Lorem.sentence}" }

    factory :site_ability do
      name { 'Modify Sites' }
      description { 'create, delete, modify details of sites' }
    end

    factory :organization_ability do
      name { 'Modify Organizations' }
      description { 'create, delete, modify details of organizations' }
    end

    factory :user_ability do
      name { 'Modify Users' }
      description { 'create, delete, modify users' }
    end

    factory :device_ability do
      name { 'Modify Devices' }
      description { 'create, delete, modify details of, change parent organization of, remote-control, (un)deploy, replace devices' }
    end

    factory :configuration_ability do
      name { 'Modify Configurations' }
      description { 'create, delete, modify details of, device configurations' }
    end

    factory :device_configuration_ability do
      name        { 'Modify Device Configurations' }
      description { 'create, delete, modify details of, device configurations' }
    end

    factory :replace_devices_ability do
      name { 'Replace Devices' }
      description { 'replace device with another device manually' }
      group_id { AbilityGroups::ADMIN }
    end

    factory :import_devices_ability do
      name { 'Import Devices' }
      description { 'import devices manually' }
      group_id { AbilityGroups::ADMIN }
    end

    factory :manage_accounts_ability do
      name { 'Manage Accounts' }
      description { 'can manage accounts' }
      group_id { AbilityGroups::DEVICE }
    end

    factory :view_admin_panel_ability do
      name { 'View Admin Panel' }
      description { 'view the admin panel' }
    end

    factory :deploy_devices_ability do
      name { 'Deploy Devices' }
      description { 'deploy devices' }
    end

    factory :send_device_commands_ability do
      name { 'Send Device Commands' }
      description { 'send commands to devices' }
    end

    factory :assign_non_production_firmwares_ability do
      name { 'Assign Non-Production Firmwares' }
      description { 'assign non-production firmwares to a device' }
    end

    factory :change_device_location_ability do
      name { 'Modify Device Location' }
      description { 'modify a device location' }
    end
  end
end
