# frozen_string_literal: true

FactoryBot.define do
  factory :uuid, class: DeviceEventUUID do
    association :category
  end
end
