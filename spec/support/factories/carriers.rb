# frozen_string_literal: true

FactoryBot.define do
  factory :carrier do
    name { 'AT&T' }

    trait :with_carrier_detail do
      carrier_detail
    end
  end
end
