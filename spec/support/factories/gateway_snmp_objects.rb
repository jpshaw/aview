# frozen_string_literal: true

FactoryBot.define do
  # Creates a hash of gateway snmp result objects, where each attribute is
  # mapped to the oid constants defined in Platforms::Gateway::Snmp::Objects.
  #
  # Example
  #
  #   build(:gateway_snmp_results)
  #   #=> {".1.3.6.1.4.1.74.1.30.1.20.0"=>"002704600000"}
  #
  #   build(:gateway_snmp_results, hardware_mac: '00D0CF123456')
  #   #=> {".1.3.6.1.4.1.74.1.30.1.20.0"=>"00D0CF123456"}
  #
  factory :gateway_snmp_results, class: Hash do
    sequence(:hardware_mac) { generate :accelerated_mac }

    # Convert hash keys to snmp objects with a prefixed dot
    initialize_with do
      attributes.map do |key, value|
        [
          ".#{Platforms::Gateway::Snmp::Objects.const_get(key.to_s.upcase)}",
          value
        ]
      end.to_h
    end
  end

  factory :gateway_connected_devices_snmp_results, parent: :gateway_snmp_results do
    sequence(:wan1_device_mac) { generate :accelerated_mac }
    sequence(:wan2_device_mac) { generate :accelerated_mac }
  end
end
