# frozen_string_literal: true

include ActionDispatch::TestProcess

FactoryBot.define do
  factory :configuration_schema do
    model               factory: :test_model
    sequence(:name)     { |n| "#{Faker::Lorem.words.join('_')}_#{n}" }
    schema_file         { Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/support/fixtures/json_schema_valid.json'))) }
  end
end
