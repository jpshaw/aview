# frozen_string_literal: true

FactoryBot.define do
  factory :netreach_configuration do
    association :organization
    sequence (:name) { |i| "config #{i + 100_000}" }
  end
end
