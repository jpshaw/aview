FactoryBot.define do
  factory :ucpe_device, :parent => :device, :class => Ucpe do

    association :organization, :factory => :random_boundary_organization

    sequence(:serial) do |i|
      serial_base = "940001#{i + 10000000}".to_i * 100
      check_digit = (98 - ((serial_base) % 97)) % 97
      serial = serial_base + check_digit
      serial
    end
    sequence(:mac) { |i| "002705#{i + 100000}" }

    object_type "Ucpe"
    hw_version "9400-UA"
    firmware "15.11.7"

  end
end
