# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    sequence(:name)        { |i| format('Organization %03d', i) }
    sequence(:slug)        { |i| "organization-#{i}" }
    sequence(:import_name) { |i| "organization-#{i}" }

    after(:create) do |organization|
      organization.create_admin_role(name: 'Admin', description: 'has all abilities')
      organization.manageable_permissions.create(manager: organization, role: organization.admin_role)
    end

    factory(:organization_with_all_abilities) do
      after(:create) do |organization|
        organization.admin_role.abilities = Ability.all
      end
    end

    # TODO: Deprecate this factory. Regular/boundary organizations were
    # replaced with permissions and abilities.
    factory :random_boundary_organization do
      sequence(:name) { |i| "Customer Organization#{i}" }
      sequence(:slug) { |i| "customer-organization#{i}" }
      sequence(:import_name) { |i| "customer-organization-#{i}" }
    end

    # TODO: Deprecate this factory. Regular/boundary organizations were
    # replaced with permissions and abilities.
    factory :random_regular_organization do
      sequence(:name) { |i| format('Node Organization %03d', i) }
      sequence(:slug) { |i| "node-organization#{i}" }
      sequence(:import_name) { |i| "node-organization-#{i}" }
    end

    factory :sub_organization do
      before(:create) do
        create(:organization) if Organization.count.zero?
      end

      sequence(:name) { |i| format('Sub Organization %03d', i) }
      sequence(:slug) { |i| "sub-organization-#{i}" }
      sequence(:import_name) { |i| "sub-organization-#{i}" }

      after(:create) do |org|
        Organization.first.children << org
      end
    end
  end
end
