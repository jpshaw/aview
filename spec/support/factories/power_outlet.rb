# frozen_string_literal: true

FactoryBot.define do
  factory :power_outlet do
    device_id { 1 }
    port { 1 }
    the_state { :on }
  end
end
