# frozen_string_literal: true

FactoryBot.define do
  factory :data_plan do
    name { 'Test Data Plan' }
    cycle_type { :monthly }
    cycle_start_day { 1 }
    data_limit { 10 }
  end
end
