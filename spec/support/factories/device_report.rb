# frozen_string_literal: true

FactoryBot.define do
  factory :device_report, class: DeviceReport do
    user
    params { { Faker::Lorem.word.to_sym => Faker::Lorem.word } }
  end
end
