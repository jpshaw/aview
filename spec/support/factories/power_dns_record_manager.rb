# frozen_string_literal: true

FactoryBot.define do
  factory :dns_domain, class: PowerDnsRecordManager::Domain do
    type { 'NATIVE' }
    sequence(:name) { |i| "domain-#{i}.accns.net" }
  end

  factory :dns_soa, class: PowerDnsRecordManager::Record do
    type { PowerDnsRecordManager::Record::SOA_TYPE }
    sequence(:domain_id)
    sequence(:name) { |i| "soa-#{i}.accns.net" }
  end

  factory :dns_nameserver, class: PowerDnsRecordManager::Record do
    type { PowerDnsRecordManager::Record::NS_TYPE }
    sequence(:domain_id)
    sequence(:name) { |i| "ns-#{i}.accns.net" }
  end
end
