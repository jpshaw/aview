# frozen_string_literal: true

FactoryBot.define do
  factory :event, class: DeviceEvent do
    association :device, factory: :test_device

    factory :event_with_device_with_random_regular_organization do
      device factory: :device_with_random_regular_organization
    end

    trait :with_sequenced_category do
      association :device, factory: %i[test_device with_sequenced_category]
    end

    trait :for_model_8100 do
      association :device, factory: %i[test_device model_8100 with_sequenced_category]
    end

    trait :for_model_5300_dc do
      association :device, factory: %i[test_device model_5300_dc with_sequenced_category]
    end

    trait :for_model_5400_rm do
      association :device, factory: %i[test_device model_5400_rm]
    end

    trait :with_sub_organization do
      association :device, factory: %i[test_device with_sub_organization]
    end
  end
end
