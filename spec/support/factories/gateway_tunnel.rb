# frozen_string_literal: true

FactoryBot.define do
  factory :gateway_tunnel do
    association :device, factory: :gateway_device

    factory :gateway_tunnel_outbound do
      endpoint_type { 6 }
      tunnel_mode { nil }
      initiator { 3 }
      connection_type { 0 }
      auth_type { 1 }
      auth_protocol { 8 }
      wan_conn_method { 1 }
      ipsec_iface { 'IPSEC0' }
      endpoint { '128.136.162.23' }
      endpoint_ipv6 { nil }
      connection_id { '7042163801146828611' }
      agns_managed { true }
      started_at { '2018-07-09 11:58:31' }
      tunnel_name { 'AVIEW-DEV-TUNNEL' }
    end
  end
end
