# frozen_string_literal: true

FactoryBot.define do
  factory :netbridge_device, parent: :device, class: Netbridge do
    sequence(:mac) { |i| "0027040#{i + 10_000}" }
    object_type { 'Netbridge' }
    hw_version { Netbridge::FX_6200_MODEL }
    firmware { '2.211.71' }

    factory :netbridge_device_with_random_regular_organization do
      organization factory: :random_regular_organization
    end

    trait :configurable do
      configuration factory: :netbridge_configuration
    end

    trait :sx do
      sequence(:mac) { generate(:netbridge_sx_mac) }
      hw_version { Netbridge::SX_MODEL }
    end

    trait :fx do
      sequence(:mac) { generate(:netbridge_fx_mac) }
      hw_version { Netbridge::FX_6200_MODEL }
    end
  end
end
