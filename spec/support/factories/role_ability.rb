# frozen_string_literal: true

FactoryBot.define do
  factory :role_ability do
    role factory: :admin_role_with_sequenced_name
    ability
  end
end
