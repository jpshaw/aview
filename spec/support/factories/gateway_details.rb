# frozen_string_literal: true

FactoryBot.define do
  factory :gateway_details do
    association :device, factory: :gateway_device
    sequence(:active_wan_iface) { |i| "eth#{i}" }

    factory :gateway_details_with_extenders do
      wan_1_cell_extender { '1' }
      wan_2_cell_extender { 'N' }
    end
  end
end
