# frozen_string_literal: true

FactoryBot.define do
  factory :timezone, class: Timezone do
    name { 'Eastern Time (US & Canada)' }
    official_name { '(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima' }
    off { '-300' }

    factory :not_default_timezone do
      name { 'Midway Island' }
      official_name { '(GMT -11:00) Midway Island, American Samoa' }
      off { '-660' }
    end

    factory :alaska_timezone do
      name { 'Alaska' }
      official_name { '(GMT -9:00) Alaska' }
      off { '-540' }
    end
  end
end
