# frozen_string_literal: true

FactoryBot.define do
  factory :tunnel_server do
    sequence(:name) { |i| "VARBA21#{i}" }
    sequence(:location) { |i| "LOCATION #{i}" }
  end
end
