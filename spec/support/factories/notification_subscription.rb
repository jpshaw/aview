# frozen_string_literal: true

FactoryBot.define do
  factory :notification_subscription, class: NotificationSubscription do
    association :user
    association :profile, factory: :notification_profile

    factory :device_subscription do
      publisher_type { NotificationSubscription::Types::DEVICE }
    end

    factory :organization_subscription do
      publisher_type { NotificationSubscription::Types::ORGANIZATION }
      association :publisher, factory: :organization
    end

    factory :invalid_subscription do
      profile_id { nil }
      publisher_type { nil }
      publisher_id { nil }
    end
  end
end
