# frozen_string_literal: true

FactoryBot.define do
  factory :usb_cellular_device, parent: :device, class: UsbCellular do
    organization factory: :random_boundary_organization

    sequence(:serial) do |i|
      serial_base = "630002#{i + 10_000_000}".to_i * 100
      check_digit = (98 - (serial_base % 97)) % 97
      serial = serial_base + check_digit
      serial
    end
    sequence(:mac) { |i| "002704#{i + 100_000}" }

    object_type { 'UsbCellular' }
    hw_version  { '6300-LX' }
    firmware    { '1.0.0' }
  end
end
