# frozen_string_literal: true

FactoryBot.define do
  factory :country do
    code { 'US' }
    name { 'United States' }

    factory :random_country do
      sequence(:code) { |i| format('%02d', i % 100) }
      sequence(:name) { |i| format('Country %03d', i) }
    end
  end
end
