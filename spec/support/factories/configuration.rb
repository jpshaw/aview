# frozen_string_literal: true

FactoryBot.define do
  factory :configuration do
    sequence(:name) { |n| "Test Configuration #{n}" }
  end
end
