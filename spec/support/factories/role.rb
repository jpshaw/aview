# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    organization factory: :random_regular_organization
    sequence(:name) { |i| "Admin #{i}" }
    description { 'can do everything with in their scope' }
    after(:build) { |role| role.admin = true }

    factory :viewer_role do
      name { 'Viewer' }
      description { 'can view everything with in their scope' }
      after(:build) { |role| role.admin = false }
    end

    factory :admin_role do
      # Default role is admin

      factory :admin_role_with_sequenced_name do
        sequence(:name) { |n| "Admin_#{n}" }

        factory :admin_role_with_abilities do
          after(:create) do |role|
            %i[device_ability replace_devices_ability import_devices_ability manage_accounts_ability].each do |ability_type|
              ability = create(ability_type)
              create(:role_ability, role: role, ability: ability)
            end
          end
        end
      end
    end
  end
end
