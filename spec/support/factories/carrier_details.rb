# frozen_string_literal: true

FactoryBot.define do
  factory :carrier_detail do
    color { '#FFFFFF' }
    display_name { 'AT&T' }
    regexp { '' }
    tail { 'txt.att.net' }
  end
end
