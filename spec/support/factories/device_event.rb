# frozen_string_literal: true

FactoryBot.define do
  factory :device_event, class: DeviceEvent do
    association :device, factory: :test_device

    trait :with_uuid do
      association :uuid, factory: :device_event_uuid
    end
  end
end
