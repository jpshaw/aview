# frozen_string_literal: true

FactoryBot.define do
  factory :snmp_profile do
    sequence(:community) { |i| "armt-snmp #{i}" }
  end
end
