# frozen_string_literal: true

FactoryBot.define do
  factory :subscribed_event, class: SubscribedEvent do
    frequency { NotificationProfile::Frequencies::NEVER }

    sequence(:profile_id)
    sequence(:uuid_id)
  end
end
