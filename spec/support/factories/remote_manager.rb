# frozen_string_literal: true

FactoryBot.define do
  factory :remote_manager_device, parent: :device, class: RemoteManager do
    organization factory: :random_boundary_organization
    series factory: :remote_manager_5400_RM_model

    sequence(:serial) do |i|
      serial_base = "530002#{i + 10_000_000}".to_i * 100
      check_digit = (98 - (serial_base % 97)) % 97
      serial = serial_base + check_digit
      serial
    end
    sequence(:mac) { |i| "002707#{i + 100_000}" }

    object_type { 'RemoteManager' }
    hw_version  { '5400-RM' }
    firmware    { '1.0.0' }
  end
end
