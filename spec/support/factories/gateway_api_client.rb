# frozen_string_literal: true

FactoryBot.define do
  factory :gateway_api_client, class: Gateway::API::Client do
    host { '2620:000e:4000:8890:0000:0000:0101:57d4' }
    user { 'authuser' }
    password { 'authpass' }
  end
end
