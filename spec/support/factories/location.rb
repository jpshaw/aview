# frozen_string_literal: true

FactoryBot.define do
  factory :location do
    association :country, factory: :random_country

    address_1 { '1208 E Kennedy BLVD' }
    address_2 { 'Suite 226' }
    city { 'Tampa' }
    state { 'FL' }
    postal_code { '33602' }
    latitude { 27.9511 }
    longitude { -82.4479 }
    is_verified { 1 }
    after(:build) do |location|
      formated_address = [
        location[:address_1],
        location[:address_2],
        location[:city],
        location[:state],
        location[:postal_code],
        location.country[:name]
      ].join(' ').squeeze(' ').upcase

      location.input_token = Digest::MD5.hexdigest(formated_address.gsub(/[^0-9a-z]/i, '').upcase)
    end

    factory :random_location do
      sequence(:address_1) { |i| "#{i + 1000} W. Martin Luther King Jr. Blvd" }
      association :country, factory: :random_country
    end
  end
end
