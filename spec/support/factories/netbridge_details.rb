# frozen_string_literal: true

FactoryBot.define do
  factory :netbridge_details do
    association :device, class_name: 'Netbridge'

    usb_hub { 'Connected' }
    usb_list { '0424:2412,0f3d:68aa' }
  end
end
