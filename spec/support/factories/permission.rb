# frozen_string_literal: true

FactoryBot.define do
  factory :permission do
    role        factory: :admin_role_with_sequenced_name
    manageable  factory: :random_regular_organization
    manager     factory: :random_regular_organization
  end
end
