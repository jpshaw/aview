# frozen_string_literal: true

FactoryBot.define do
  factory :network_configuration, parent: :configuration, class: NetworkConfiguration do
  end
end
