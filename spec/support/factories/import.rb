FactoryBot.define do
  factory :import, :class => Import do
    factory :blank_import do
    end
    factory :qa_import do
      import_file File.new(Rails.root.join("spec", "support", "fixtures", "att_qa_smx_netgate_list.tab").to_s, "r")
      factory :finished_qa_import do
        the_state :success
        finished_at Time.now

        factory :finished_qa_import_with_objects do
          after(:create) do |import|
            import.device_ids = FactoryBot.create_list(:gateway_device, 5).map(&:id)
            import.customer_ids = FactoryBot.create_list(:sub_organization, 3).map(&:id)
            import.account_ids = FactoryBot.create_list(:account_with_sub_organization, 5).map(&:id)
            import.site_ids = FactoryBot.create_list(:site_with_sub_organization, 5).map(&:id)
            import.location_ids = FactoryBot.create_list(:random_location, 5).map(&:id)
            import.configuration_ids = FactoryBot.create_list(:gateway_configuration, 1).map(&:id)
            import.skipped_device_ids = FactoryBot.build_list(:gateway_device, 5).map(&:mac)
            import.save
          end
        end
      end
    end

    factory :invalid_import do
      import_file File.new(Rails.root.join("spec", "support", "fixtures", "invalid_import_file.tab").to_s, "r")
    end
  end
end
