# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    association :organization
    association :timezone

    sequence (:email) { |i| format('admin_%04d@accns.com', i) }
    password { '123456' }
    password_confirmation { '123456' }
    first_name { 'Test' }
    last_name { 'User' }
    notify_enabled { true }

    trait :with_non_default_timezone do
      association :timezone, factory: :not_default_timezone
    end

    trait :with_alaska_timezone do
      association :timezone, factory: :alaska_timezone
    end

    factory :invalid_user do |u|
      u.name nil
      u.email nil
    end

    factory :api_user do
      authentication_token { 'C8a5a374479c19f6E903e5763545d15A081d319d' }
    end

    factory :non_root_user do
      association :organization, factory: :sub_organization
    end

    factory :non_root_user_without_roles do
      association :organization, factory: :sub_organization
    end

    factory :user_with_random_regular_organization do
      organization factory: :random_regular_organization
    end

    factory :user_with_org_permission, aliases: %i[root_user root_user_with_admin_role] do
      after(:create) do |user|
        Permission.create(
          manager:    user,
          manageable: user.organization,
          role:       Role.create(name: 'Test', organization_id: user.organization.id)
        )
      end
    end

    factory :user_with_all_abilities do
      after(:create) do |user|
        role = Role.create(name: 'Test', organization_id: user.organization.id)
        role.abilities = Ability.all
        Permission.create(
          manager:    user,
          manageable: user.organization,
          role:       role
        )
      end
    end

    factory :user_with_armt_controller_abilities do
      after(:create) do |user|
        role = Role.create(name: 'Test', organization_id: user.organization.id)
        role.abilities = Ability.where(name: ['Modify Configurations',
                                              'Modify Device Configurations',
                                              'Modify Organizations',
                                              'View Admin Panel',
                                              'Deploy Devices',
                                              'Send Device Commands',
                                              'Assign Non-Production Firmwares',
                                              'Modify Device Location'])
        Permission.create(
          manager:    user,
          manageable: user.organization,
          role:       role
        )
      end
    end
  end
end
