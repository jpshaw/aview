# frozen_string_literal: true

require 'rails_helper'

describe OrganizationImporter do
  subject { described_class }

  context 'with reduced data' do
    before do
      @data_filename = Rails.root.join('spec',
                                       'support',
                                       'fixtures',
                                       'organization_template.csv')
    end

    describe '.import_file' do
      context 'when the organizations do not exist' do
        it 'creates them' do
          expect do
            subject.import_file(@data_filename)
          end.to change(Organization, :count)
        end
      end

      context 'after the import is run' do
        before do
          subject.import_file(@data_filename)
          @root = Organization.find_by name: 'root'
        end

        it 'adds the root node' do
          expect(Organization.roots.first).to eq @root
        end

        context 'when parent org names are given' do
          before do
            @grandparent = Organization.find_by name: 'grandparent'
            @parent      = Organization.find_by name: 'parent'
            @child       = Organization.find_by name: 'child'
          end

          it 'creates a hierarchy' do
            expect(@root.children).to include(@grandparent)
            expect(@grandparent.children).to include(@parent)
            expect(@parent.children).to include(@child)
            expect(@child.children).to be_empty
          end
        end

        context 'when no parent org name is given' do
          before do
            @parentless_child = Organization.find_by name: 'parentless_child'
          end

          it 'adds the org as a child of root' do
            expect(@root.children).to include(@parentless_child)
          end
        end
      end
    end
  end
end
