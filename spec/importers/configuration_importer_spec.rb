# frozen_string_literal: true

require 'rails_helper'

describe ConfigurationImporter do
  subject { described_class }

  context 'with reduced data' do
    before do
      @data_filename = Rails.root.join('spec',
                                       'support',
                                       'fixtures',
                                       'config_template.csv')
    end

    describe '.import_file' do
      context 'when the config organization exists' do
        before do
          @organization = create(
            :organization,
            name:        'companyname',
            import_name: 'companyname'
          )
        end

        it 'creates configurations for rows that have data and defaults' do
          expect do
            subject.import_file(@data_filename)
          end.to change(NetbridgeConfiguration, :count).by(7)
        end

        it 'assigns the config values' do
          subject.import_file(@data_filename)
          config_org = Organization.where(name: 'nonexistentorg').first
          config = NetbridgeConfiguration.where(name: 'default', organization_id: config_org.id).first
          full_config = config.config_values_with_inherited
          expect(full_config['cfg']).to eq 'config.accns.com'
          expect(full_config['var']).to eq 'A'
          expect(full_config['dlm']).to eq '5'
          expect(full_config['img']).to eq '1.542.01'
          expect(full_config['img2']).to eq '2.291.51'
        end

        context 'when a parent config name is given' do
          it 'assigns the configuration as a child of the parent' do
            subject.import_file(@data_filename)
            child  = NetbridgeConfiguration.find_by name: '0027040107E2'
            parent = NetbridgeConfiguration.where(organization_id: child.organization_id).find_by name: 'default'
            expect(parent.children).to include(child)
          end
        end

        context 'when device macs match the config name' do
          before do
            create(
              :netbridge_device,
              mac:          '0027040107E2',
              organization: @organization
            )

            create(
              :netbridge_device,
              mac:          '00270400019D',
              organization: @organization
            )
          end

          context 'when the config has data' do
            it 'assigns the configuration to the device' do
              subject.import_file(@data_filename)
              config = NetbridgeConfiguration.find_by name: '0027040107E2'
              device = Device.find_by mac: '0027040107E2'
              expect(device.configuration).to eq config
            end
          end

          context 'when the config does not have data' do
            it 'assigns the parent config to the device' do
              subject.import_file(@data_filename)
              device        = Device.find_by mac: '00270400019D'
              parent_config = NetbridgeConfiguration.where(organization_id: device.organization_id)
                                                    .find_by name: 'default'
              expect(device.configuration).to eq parent_config
            end
          end
        end

        context 'when the config does not exist' do
          it 'creates it and tags it as `added`' do
            results = subject.import_file(@data_filename)
            expect(results[:added]).to include('default')
          end
        end

        context 'when the config exists with the name but no import name' do
          before do
            @config = NetbridgeConfiguration.create!(
              organization_id: @organization.id,
              name:            'default',
              import_name:     nil
            )
          end

          it 'finds the config and updates the import name to match' do
            results = subject.import_file(@data_filename)
            @config.reload
            expect(@config.import_name).to eq @config.name
          end
        end

        context 'when the config exists by import name' do
          before do
            @config = NetbridgeConfiguration.create!(
              organization_id: @organization.id,
              name:            'Default Configuration',
              import_name:     'default'
            )
            expect(@config.cfg).to be nil
          end

          it 'finds the config and updates its attributes' do
            results = subject.import_file(@data_filename)
            @config.reload
            expect(results[:added]).not_to include(@config.name)
            expect(@config.cfg).to eq 'config.accns.com'
          end
        end
      end
    end
  end
end
