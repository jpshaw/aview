# frozen_string_literal: true

require 'rubygems'
require 'rails_helper'
require 'smx_importer'

describe SmxImporter do
  context 'with reduced data' do
    before do
      @data_filename = Rails.root.join('spec',
                                       'support',
                                       'fixtures',
                                       'armt_device_list_reduced.tab')
    end

    describe '.whitelist?' do
      it 'whitelists records with a community of "armt-snmp"' do
        expect(described_class.send('whitelist?', snmp_config: { community: 'armt-snmp' })).to eq true
      end

      it 'whitelists records with a community of "aview-snmp"' do
        expect(described_class.send('whitelist?', snmp_config: { community: 'aview-snmp' })).to eq true
      end

      it 'whitelists records with a account starting with "DTREE"' do
        expect(described_class.send('whitelist?', account: "DTREE#{Random.rand(100)}")).to eq true
      end
    end

    describe '.import_file' do
      context 'after importing data_file with sync method where all records in file are valid' do
        before do
          create_organization

          Gateway.first_or_create!(mac: '00D0CFFFFFFF', configuration_id: 1,
                                   organization_id: @organization.id)
          @import = described_class.sync_from_file(@data_filename)
        end

        it 'creates a device from the data file' do
          expect(Device
                 .where(mac: '002704239308')
                 .count).to eq 1
        end

        it 'does not skip any devices' do
          expect(@import[:devices][:skipped].count).to eq(0)
        end

        it 'adds attributes to device' do
          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .firmware).to eq('6.1.64')

          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .hw_version).to eq('8300')

          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .host).to eq('128.136.162.21')

          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .serial).to eq('8300000270291437')
        end

        context 'when given a u110 serial range' do
          it 'creates a gateway device object' do
            expect(Device.find_by(mac: '0027042B07AA')).to be_present
          end
        end

        it 'adds details to device' do
          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .details
                 .wan_1_cell_extender).to eq('Y')
        end

        it 'sets the configuration name for the device' do
          expect(Device
                 .where(mac: '002704239308')
                 .first
                 .details
                 .configuration_name).to eq('VPN-DEVICE1-MDL')
        end

        it 'adds the contact to the device' do
          contact = Device.where(mac: '002704258159').first.contacts.first
          expect(contact).to be
          expect(contact.email).to eq 'james@test.com'
          expect(contact.name).to eq 'JAMES M. SAMPSON III'
          expect(contact.phone).to eq '8008675309'
        end

        context 'when only a contact name is given' do
          it 'saves the contact and associates it with the device' do
            contact = Device.where(mac: '00D0CF1AFE82').first.contacts.first
            expect(contact).to be
            expect(contact.name).to eq 'TEST CONTACT NAME'
            expect(contact.email).to be_blank
            expect(contact.phone).to be_blank
          end
        end

        it 'creates a customer from the data file' do
          expect(Organization.count).to be > 1
        end

        it 'links customers to the root organization' do
          expect(Organization
                 .first
                 .children
                 .where(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')
                 .first).to be
        end

        it 'does not create duplicate customers' do
          expect(Organization
                 .where(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')
                 .count).to eq 1
        end

        it 'links a device to its organization' do
          expect(Organization
                 .where(import_name: 'USARMT')
                 .first.devices.where(mac: '002704239308')
                 .count).to eq 1
        end

        it 'adds a location to the device' do
          device = Device.find_by(mac: '002704258159')
          expect(device.location).to be_present
        end

        it 'adds a configuration to device' do
          expect(Device
                 .where(mac: '002704258159')
                 .first.configuration.snmp.version).to eq '2c'
        end

        it 'removes devices not in data dump' do
          expect(Device
                 .where(mac: '00D0CFFFFFFF')
                 .count).to eq 0
        end

        it 'does not recreate a renamed organization' do
          example_org = Organization.where(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT').first_or_create!
          example_org.update(name: 'ADVANCED MGMT')
          described_class.sync_from_file(@data_filename)
          expect(Organization.where(name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT').count).to eq 0
        end

        it 'adds a site to device' do
          expect(Device
                 .where(mac: '002704239308')
                 .first.site.name).to eq 'ACCELERATED TAMPA PEAK10'
        end

        it 'adds sites to organizations' do
          expect(Organization
                 .where(import_name: 'USARMT')
                 .first
                 .sites
                 .where(name: 'ACCELERATED TAMPA PEAK10')
                 .first).to be
        end

        it 'downcases the community string' do
          # The AT&T VPN Gateway forces the community string to lower-case, so we must do the same
          expect(Device
                 .where(mac: '002704239308')
                 .first.configuration.snmp.community).to eq 'armt-snmp'
        end
      end

      context 'after importing data_file with append method' do
        before do
          create_organization
          Gateway
            .where(mac: '00D0CFFFFFFF')
            .first_or_create!(configuration_id: 1, organization_id: @organization.id)
          described_class.append_from_file(@data_filename)
        end

        it 'does not remove devices not in data dump' do
          expect(Device
                 .where(mac: '00D0CFFFFFFF')
                 .count).to eq 1
        end
      end

      context "when a device's organization is a descendant but not child of root" do
        before do
          create_organization
          customer_name = 'AT&T INTERNAL - ARMT DEMO ACCOUNT'
          device_customer = Organization.where(import_name: customer_name)
                                        .first_or_create(name: customer_name)
          customer_name = 'Management Co'
          management_customer = Organization.where(import_name: customer_name)
                                            .first_or_create(name: customer_name)
          root_org = Organization.first
          root_org.children << management_customer
          root_org.save
          management_customer.save
          management_customer.children << device_customer
          management_customer.save
          device_customer.save

          described_class.sync_from_file(@data_filename)
        end

        it 'expects children of descendant to be created' do
          expect(Organization
                 .where(name: 'USARMT')
                 .first.devices.where(mac: '002704239308')
                 .count).to eq 1
        end
      end

      context 'after pre-populating a device without a serial, then importing data_file with update method' do
        before do
          create_organization
          Gateway
            .where(mac: '002704258159')
            .first_or_create!(configuration_id: 1, organization_id: @organization.id)
          described_class.update_from_file(@data_filename)
        end

        it 'does not add new devices' do
          expect(Device.pluck(:mac)).to eq ['002704258159']
        end

        it 'updates device data from data file' do
          expect(Device
                 .where(mac: '002704258159')
                 .first.site.name).to eq 'SPARE'
        end

        it 'updates serial number of the device' do
          expect(Device
                 .where(mac: '002704258159')
                 .first.serial).to eq '8300000878061524'
        end
      end
    end

    describe '.process_data' do
      before do
        @smx_data = {
          customer_name: 'TEST COMPANY',
          device: {
            mac: '002704258159',
            host: '124.178.250.53',
            firmware: '4.10.34',
            hw_version: '8100'
          },
          site: { name: 'DARWIN' },
          account: 'AAKDSL',
          details: {
            active_conn_type: '3',
            wan_1_conn_type: '3',
            wan_1_circuit_type: '',
            wan_1_cell_extender: '1',
            wan_2_cell_extender: 'N',
            dial_mode: 'B',
            dial_backup_enabled: false,
            dial_backup_type: 'T',
            dial_modem_type: 'I'
          },
          portal_config: {
            password: nil,
            ssl_password: 'abc123',
            ssl_port: 443,
            auth_user: 'authuser',
            auth_password: 'authpassword'
          },
          snmp_config: {
            version: '2c',
            community: 'armt-snmp'
          },
          contact: { name: 'TEST USER' },
          location: {
            address_1: '3/5 BISHOP STREET STUART PARK',
            address_2: '',
            address_3: 'DARWIN NT_0820',
            line_1: '',
            line_2: '',
            line_3: '',
            line_4: '',
            city: '',
            state: '',
            province: '',
            postal: '',
            country: ''
          },
          wan_devices: {
            wan1: '00d0cf123456',
            wan2: '002704010203'
          }
        }
      end

      before do
        create_organization
      end

      context 'when the device does not exist' do
        it 'creates the device and assigns it to the customer' do
          expect do
            described_class.send(:process_data, @smx_data)
          end.to change(Device, :count).from(0).to(1)
          device = Device.all.first
          expect(device.mac).to eq '002704258159'
          expect(device.organization.name).to eq 'AAKDSL'
        end
      end

      context 'when the device exists' do
        before do
          described_class.send(:process_data, @smx_data)
          @device = Device.find_by(mac: '002704258159')
        end

        context 'when the account for the device changes' do
          it "updates the device's customer" do
            expect(@device.mac).to eq '002704258159'
            expect(@device.organization.name).to eq 'AAKDSL'
            @smx_data[:account] = 'BBKDSL'
            described_class.send(:process_data, @smx_data)
            @device.reload
            expect(@device.organization.name).to eq 'BBKDSL'
          end
        end

        context 'when the device host is set' do
          it 'does not change the host' do
            @device.update host: '1.2.3.4'
            described_class.send(:process_data, @smx_data)
            @device.reload
            expect(@device.host).to eq '1.2.3.4'
          end
        end

        context 'when the device host is nil' do
          it 'sets the host' do
            @device.update host: nil
            described_class.send(:process_data, @smx_data)
            @device.reload
            expect(@device.host).to eq '124.178.250.53'
          end
        end

        context 'when the device firmware is set' do
          it 'does not change the firmware' do
            @device.update firmware: '1.2.3'
            described_class.send(:process_data, @smx_data)
            @device.reload
            expect(@device.firmware).to eq '1.2.3'
          end
        end

        context 'when the device firmware is nil' do
          it 'sets the firmware' do
            @device.update firmware: nil
            described_class.send(:process_data, @smx_data)
            @device.reload
            expect(@device.firmware).to eq '4.10.34'
          end
        end

        context 'when the device has details' do
          it 'updates the details' do
            expect(@device.details.id).not_to be nil
            details_id = @device.details.id
            @device.details.update active_wan_iface: 'eth5'

            described_class.send(:process_data, @smx_data)

            @device.reload
            expect(@device.details.id).to eq details_id
            expect(@device.details.active_wan_iface).to eq 'eth5'
          end
        end
      end

      # We can expect any blank values to set auth_user to nil, thanks to the
      # `value_or_nil` method in SmxParser.
      context 'when the auth user id is nil' do
        before do
          @smx_data[:portal_config][:auth_user] = nil
        end

        it 'sets the user id to the default "support"' do
          data = described_class.send(:process_data, @smx_data)
          expect(data).not_to be nil
          expect(data.configuration.portal.auth_user).to eq 'support'
        end
      end

      context 'when the auth user is set' do
        before do
          @smx_data[:portal_config][:auth_user] = 'user1234'
        end

        it 'sets the user id to the given value' do
          data = described_class.send(:process_data, @smx_data)
          expect(data).not_to be nil
          expect(data.configuration.portal.auth_user).to eq 'user1234'
        end
      end
    end
  end
end
