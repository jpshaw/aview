# frozen_string_literal: true

require 'rails_helper'
require 'device_importer'
require 'devices/netbridge'
require 'devices/netreach'
require 'devices/gateway'
require 'location'

describe DeviceImporter do
  subject { described_class }

  context 'with reduced data' do
    before do
      @data_filename = Rails.root.join('spec',
                                       'support',
                                       'fixtures',
                                       'accelerated_template.csv')

      @location_data_filename = Rails.root.join('spec',
                                                'support',
                                                'fixtures',
                                                'accelerated_template_with_location.csv')
    end

    describe '.import_file' do
      context 'after importing accelerated data_file' do
        before do
          expect(Organization.count).to eq 0
          create_organization # Creates a root organization
          create_all_categories_and_models
          expect(Device.where(mac: '0027040103A2').count).to eq 0
          expect(Device.where(mac: '00C0CA6E8FCC').count).to eq 0
          expect(Device.where(mac: '00D0CF076FC8').count).to eq 0
          expect(Device.where(mac: '00270427AF53').count).to eq 0
          expect(Organization.count).to eq 1
          expect(Organization.first.descendants.count).to eq 0
          described_class.import_file(@data_filename)
        end

        it 'creates a NetBridge from the data file' do
          expect(Device
                 .where(mac: '0027040103A2')
                 .first
                 .category
                 .name).to eq('NetBridge')
        end

        it 'creates a NetReach from the data file' do
          expect(Device
                 .where(mac: '00C0CA6E8FCC')
                 .first
                 .category
                 .name).to eq('WiFi')
        end

        it 'creates a VPN Gateway from the data file' do
          expect(Device
                 .where(mac: '00D0CF076FC8')
                 .first
                 .category
                 .name).to eq(DeviceCategory::GATEWAY_CATEGORY)
        end

        it 'creates a 6300-CX from the data file' do
          expect(Device
                 .where(mac: '00270427AF53')
                 .first
                 .category
                 .name).to eq('Cellular')
        end

        it 'adds hardware model to device' do
          expect(Device
                 .where(mac: '0027040103A2')
                 .first
                 .hw_version).to eq('6200-FX')
        end

        it 'creates a 5400-RM from the data file' do
          expect(Device.find_by(mac: '0027042956A8').hw_version).to eq('5400-RM')
        end

        it 'creates a 5301-DC from the data file' do
          expect(Device.find_by(mac: '00270427C04C').hw_version).to eq('5301-DC')
        end

        context 'with a IX14 mac and serial' do
          it 'creates a IX14 from the data file' do
            device = Device.find_by(mac: '0004F3123456')
            expect(device.serial).to eq('IX14-987654')
            expect(device.category.name).to eq('Cellular')
            expect(device.hw_version).to eq('IX14')
          end
        end

        context 'with a IX14 mac and hw_version' do
          it 'creates a IX14 from the data file' do
            device = Device.find_by(mac: '0004F3987654')
            expect(device.category.name).to eq('Cellular')
            expect(device.hw_version).to eq('IX14')
          end
        end

        # Check that the created descendant matches the org name from the import file
        it 'creates a descendant org from the data file' do
          descendant_org = Organization.find_by name: 'Sony'
          expect(Organization.first.descendants).to include(descendant_org)
        end

        it 'links a device to its organization' do
          expect(Organization
                 .first
                 .descendants
                 .where(name: 'Sony')
                 .first.devices.where(mac: '0027040103A2')
                 .count).to eq 1
        end

        # Check that the site ID matches what is specified in the import file
        it 'adds site to device' do
          expect(Device
                 .where(mac: '0027040103A2')
                 .first
                 .site.name).to eq('office')
        end

        context 'when a deployment descriptor is given' do
          context 'when the device data indicates it is deployed' do
            it 'is deployed' do
              expect(Device.find_by(mac: '0027040103A2').deployed?).to be true
            end
          end

          context 'when the device data indicates it is undeployed' do
            it 'is undeployed' do
              expect(Device.find_by(mac: '00C0CA6E8FCC').deployed?).to be false
            end
          end
        end

        context 'when a deployment descriptor is not given' do
          it 'is undeployed' do
            expect(Device.find_by(mac: '00D0CF076FC8').deployed?).to be false
          end
        end

        context 'when a host is given' do
          context 'when the host is not set' do
            it 'sets the host' do
              expect(Device.find_by(mac: '0027040103A2').host).to eq '192.168.16.3'
            end
          end

          context 'when the host is already set' do
            before do
              device = Device.find_by(mac: '0027040103A2')
              device.update host: '1.2.3.4'
            end

            it 'keeps the current host' do
              described_class.import_file(@data_filename)
              expect(Device.find_by(mac: '0027040103A2').host).to eq '1.2.3.4'
            end
          end
        end

        context 'when a host is not given' do
          it 'does nothing to the host' do
            expect(Device.find_by(mac: '00D0CF076FC8').host).to be nil
          end
        end

        context 'when a parent organization is given' do
          it 'sets the devices org to be a child of the parent organization' do
            parent = Organization.find_by name: 'Root'
            device = Device.find_by(mac: '0027040103A2')
            expect(device.organization.parent).to eq parent
          end

          context 'when the parent is created from an organization given in the file' do
            it 'sets the devices org to be a child of the parent organization' do
              parent = Organization.find_by name: 'Sony'
              device = Device.find_by(mac: '00D0CF076FC8')
              expect(device.organization.name).to eq 'Testing'
              expect(device.organization.parent).to eq parent
            end
          end
        end

        context 'when a parent organization is not given' do
          it 'sets the devices org to be a child of the root organization' do
            root_org = Organization.first
            device   = Device.find_by(mac: '00C0CA6E8FCC')
            expect(device.organization.parent).to eq root_org
          end
        end

        context 'when an invalid mac is passed' do
          it 'does not save any of the row data' do
            expect(Device.find_by(mac: '00D0CF076FC')).to be nil
            expect(Organization.find_by(name: 'Hello World')).to be nil
          end
        end

        context 'activated_at' do
          let(:device_1)  { Device.find_by(mac: '0027040103A2') }
          let(:device_2)  { Device.find_by(mac: '00C0CA6E8FCC') }
          let(:timestamp) { Time.parse('2011-01-05 11:27:03 EDT') }

          context 'when a activated_at timestamp is given' do
            it 'sets the devices `activated_at` to the timestamp' do
              expect(device_1.activated_at).to eq timestamp
            end

            context 'and a device already has a activated_at timestamp' do
              before { device_1.update(activated_at: Time.now) }

              it 'overrides activated_at with given timestamp' do
                described_class.import_file(@data_filename)
                expect(device_1.reload.activated_at).to eq timestamp
              end
            end
          end

          context 'when activated_at is invalid' do
            it 'leaves activated_at timestamp blank' do
              expect(device_2.activated_at).to be nil
            end

            context 'and a device already has a activated_at timestamp' do
              before { device_2.update(activated_at: timestamp) }

              it 'preserves previous activated_at timestamp' do
                described_class.import_file(@data_filename)
                expect(device_2.reload.activated_at).to eq timestamp
              end
            end
          end
        end

        context 'when a heartbeat timestamp is given' do
          context 'when the device does not have a `last heartbeat`' do
            it 'sets the devices `last heartbeat` to the timestamp' do
              device = Device.find_by(mac: '0027040103A2')
              expect(device.last_heartbeat_at).to eq Time.parse('2011-01-05 11:27:03')
            end
          end

          context 'when the device has a `last heartbeat`' do
            before do
              @device    = Device.find_by(mac: '0027040103A2')
              @timestamp = Time.parse('2014-01-01 00:00:01')
              @device.update last_heartbeat_at: @timestamp
            end

            it 'keeps the current `last heartbeat`' do
              described_class.import_file(@data_filename)
              expect(@device.last_heartbeat_at).to eq @timestamp
            end
          end
        end

        context 'when no heartbeat timestamp is given' do
          it 'does nothing to the devices `last heartbeat`' do
            expect(Device.find_by(mac: '00D0CF076FC8').last_heartbeat_at).to be nil
          end
        end

        it 'does not recreate a renamed organization' do
          example_org = Organization.where(name: 'Sony').first
          example_org.update(name: 'Sony Corp')

          expect do
            described_class.import_file(@data_filename)
          end.not_to change(Organization, :count)
        end

        # Check that the site ID from the import file was added
        it 'adds sites to organizations' do
          expect(Organization
                 .where(name: 'Sony')
                 .first
                 .sites
                 .where(name: 'office')
                 .first).to be
        end
      end

      context 'after importing accelerated data_file' do
        before do
          create_organization

          expect(Device.where(mac: '002704010111').count).to eq 0
          DeviceFactory
            .create(mac: '002704010111', organization_id: @organization.id)

          described_class.import_file(@data_filename)
        end

        it 'does not remove devices not in data dump' do
          expect(Device
                 .where(mac: '002704010111')
                 .count).to eq 1
        end
      end

      context 'when a hw_version is given' do
        before do
          create_organization
          create_all_categories_and_models
        end

        context 'when the device is new' do
          before do
            expect(Device.where(mac: '00C0CA6E8FCE').count).to eq 0
            expect(Device.where(mac: '00D0CF076FC8').count).to eq 0
            described_class.import_file(@data_filename)
          end

          context 'when a valid hw_version is specified' do
            it 'sets the hw_version' do
              # Note: this hw_version should be set to 4200-NX based on the MAC address,
              #       but since we specify the hw_version in the import, the importer
              #       already sets the hw_version before our concern gets a chance to.
              expect(Device.find_by(mac: '00C0CA6E8FCE').hw_version).to eq '6300-CX'
            end
          end

          context 'when an invalid hw_version is specified' do
            it 'sets the hw_version based on the serial or MAC address' do
              expect(Device.find_by(mac: '00D0CF076FC8').hw_version).to eq '8200'
            end
          end
        end

        context 'when the device already exists' do
          before do
            DeviceFactory.create(mac: '00C0CA6E8FCE', organization_id: Organization.first.id)
            DeviceFactory.create(mac: '00D0CF076FC8', organization_id: Organization.first.id)

            expect(Device.where(mac: '00C0CA6E8FCE').count).to eq 1
            expect(Device.where(mac: '00D0CF076FC8').count).to eq 1

            described_class.import_file(@data_filename)
          end

          context 'when a valid hw_version is specified' do
            it 'sets the hw_version' do
              # Note: this hw_version should be set to 4200-NX based on the MAC address,
              #       but since we specify the hw_version in the import, the importer
              #       already sets the hw_version before our concern gets a chance to.
              expect(Device.find_by(mac: '00C0CA6E8FCE').hw_version).to eq '6300-CX'
            end
          end

          context 'when an invalid hw_version is specified' do
            it 'sets the hw_version based on the serial or MAC address' do
              expect(Device.find_by(mac: '00D0CF076FC8').hw_version).to eq '8200'
            end
          end
        end
      end

      context 'when a config name is given' do
        before do
          create_organization # Creates a root organization
          @organization = Organization.create!(name: 'Sony')
        end

        context 'for netbridge configurations' do
          context 'when the config exists for the device organization' do
            before do
              @config = @organization.netbridge_configurations.create!(name: 'Default')

              described_class.import_file(@data_filename)
            end

            it 'assigns the config to a device' do
              expect(Device.find_by(mac: '0027040103A2').configuration).to eq @config
            end
          end

          context 'when the config does not exist for the device organization' do
            before do
              @config = @organization.netbridge_configurations.create!(name: 'Testing')

              described_class.import_file(@data_filename)
            end

            it 'does not assign a configuration to the device' do
              expect(Device.find_by(mac: '0027040103A2').configuration).to be nil
            end
          end
        end

        context 'for device configurations' do
          context 'when the config exists for the device organization' do
            before do
              @config = create(:device_configuration, name: 'default', organization: @organization)

              described_class.import_file(@data_filename)
            end

            it 'assigns the config to a device' do
              expect(Device.find_by(mac: '00270427AF53').configuration).to eq @config
            end
          end

          context 'when the config does not exist for the device organization' do
            before do
              @config = create(:device_configuration, name: 'Testing', organization: @organization)

              described_class.import_file(@data_filename)
            end

            it 'does not assign a configuration to the device' do
              expect(Device.find_by(mac: '00270427AF53').configuration).to be nil
            end
          end
        end
      end

      # Setup the all the organizations first.  That way, when we run the
      # DeviceImporter, we can check to see if the devices were added to
      # the correct sub-organization.  Should look like the following
      #   Accelerated (root org)
      #   \
      #    -> Management Co (org)
      #       \
      #        -> Sony (org)
      #           \
      #            -> 0027040103A2 (device)
      context "when a device's organization is a descendant but not child of root" do
        before do
          create_organization
          expect(Device.where(mac: '0027040103A2').count).to eq 0
          expect(Organization.first.descendants.where(name: 'Sony').count).to eq 0
          root_org = Organization.first
          customer_org_name = 'Sony'
          management_org_name = 'Management Co'
          management_customer = Organization.where(import_name: management_org_name)
                                            .first_or_initialize(name: management_org_name)
          management_customer.parent = root_org
          device_customer = Organization.where(import_name: customer_org_name)
                                        .first_or_initialize(name: customer_org_name)
          device_customer.parent = management_customer
          management_customer.save
          device_customer.save

          described_class.import_file(@data_filename)
        end

        it 'expects device to be added to device_customer org' do
          expect(Organization
                 .first
                 .descendants
                 .where(name: 'Sony')
                 .first.devices.where(mac: '0027040103A2')
                 .count).to eq 1
        end
      end
    end
  end

  describe 'Methods' do
    before do
      @data_filename = Rails.root.join('spec',
                                       'support',
                                       'fixtures',
                                       'accelerated_template.csv')
    end

    describe '#get_organization_for_name' do
      before do
        @importer = subject.new(@data_filename)
        @name     = 'Test'
      end

      context 'when an organiation for the `name` and `import name` does not exist' do
        it 'creates a new one' do
          expect do
            @importer.send(:get_organization_for_name, @name)
          end.to change(Organization, :count).by(1)
        end
      end

      context 'when an organization has the name both `name` and `import name`' do
        before do
          @organization = create(:random_regular_organization, name: @name, import_name: @name)
        end

        it 'returns the organization' do
          expect(@importer.send(:get_organization_for_name, @name)).to eq @organization
        end
      end

      context 'when an organization has the `name` and `import name` is nil' do
        before do
          @organization = create(:random_regular_organization, name: @name, import_name: nil)
        end

        it 'returns the organization and sets the `import name` to `name`' do
          organization = @importer.send(:get_organization_for_name, @name)
          expect(organization).to eq @organization
          expect(organization.import_name).to eq @name
        end
      end

      context 'when an organization has the `import name` that does not match the `name`' do
        before do
          @org_by_import_name = create(:random_regular_organization, name: 'Test 123', import_name: @name)
          @org_by_name        = create(:random_regular_organization, name: @name, import_name: 'Test 123')
        end

        it 'returns the organization that matches the `import name`' do
          organization = @importer.send(:get_organization_for_name, @name)
          expect(organization).to eq @org_by_import_name
        end
      end
    end
  end
end
