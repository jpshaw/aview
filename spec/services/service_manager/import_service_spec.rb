# frozen_string_literal: true

require 'rails_helper'

describe ServiceManager::ImportService do
  let(:tabfile) { fixture_path + '/' + 'armt_device_list_reduced.tab' }
  let!(:root_node) { create(:organization) }

  before do
    create_gateway_category_and_models
  end

  describe '.import_file' do
    let(:device_1)    { Device.find_by(mac: '002704239308') }
    let(:device_2)    { Device.find_by(mac: '002704258159') }
    let(:device_3)    { Device.find_by(mac: '00D0CF1AFE82') }
    let(:u110_device) { Device.find_by(mac: '0027042B07AA') }

    before do
      allow(ServiceManager).to receive(:gateway_whitelist).and_return(['aview-snmp', 'armt-snmp'])
    end

    context 'for whitelist' do
      def snmp_community_device_count(community)
        Gateway.joins(configuration: :snmp).where(snmp_profiles: { community: community }).count
      end

      context 'for aview-snmp' do
        before do
          allow(ServiceManager).to receive(:gateway_whitelist).and_return(['aview-snmp'])
          described_class.import_file(tabfile)
        end

        it 'imports aview-snmp devices' do
          expect(Gateway.count).to eq 1
        end

        it 'does not import armt-snmp devices' do
          expect(snmp_community_device_count('armt-snmp')).to eq 0
        end
      end

      context 'for armt-snmp' do
        before do
          allow(ServiceManager).to receive(:gateway_whitelist).and_return(['armt-snmp'])
          described_class.import_file(tabfile)
        end

        it 'imports armt-snmp devices' do
          expect(Gateway.count).to eq 3
        end

        it 'does not import aview-snmp devices' do
          expect(snmp_community_device_count('aview-snmp')).to eq 0
        end
      end

      context 'for both' do
        before do
          allow(ServiceManager).to receive(:gateway_whitelist).and_return(['aview-snmp', 'armt-snmp'])
          described_class.import_file(tabfile)
        end

        it 'imports all devices' do
          expect(Gateway.count).to eq 4
        end
      end

      context 'for none' do
        before do
          allow(ServiceManager).to receive(:gateway_whitelist).and_return([])
          described_class.import_file(tabfile)
        end

        it 'imports no devices' do
          expect(Gateway.count).to eq 0
        end
      end
    end

    context 'for organizations' do
      before { described_class.import_file(tabfile) }

      it 'creates organizations' do
        expect(Organization.count).to be > 1
      end

      it 'links customers to the root organization' do
        customer = Organization.find_by(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')
        expect(root_node.children).to include(customer)
      end

      it 'does not link customers to the root organization if they already have a parent' do
        customer = Organization.find_by(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')
        new_parent = root_node.children.where.not(id: customer.id).first
        customer.parent = new_parent
        customer.save
        described_class.import_file(tabfile)
        expect(customer.reload.parent).to eq new_parent
        expect(root_node.reload.children).not_to include(customer)
      end

      it 'links organizations to their customer name if it changes' do
        account = Organization.find_by(import_name: 'USARMT')
        customer = account.parent
        new_parent = root_node.children.where.not(id: customer.id).first
        account.parent = new_parent
        account.save
        described_class.import_file(tabfile)
        expect(account.reload.parent).to eq customer
        expect(customer.reload.children).to include(account)
      end

      it 'does not create duplicate customers' do
        expect(Organization.where(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT').count).to eq 1
      end

      it 'links devices to organizations' do
        expect(Organization.find_by(import_name: 'USARMT').devices).to include(device_1)
      end

      context 'when an organization is renamed' do
        before do
          organization = Organization.find_by(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')
          organization.update(name: 'ADVANCED MGMT')
        end

        it 'does not recreate it' do
          described_class.import_file(tabfile)
          expect(Organization.find_by(import_name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')).to be_present
          expect(Organization.find_by(name: 'AT&T INTERNAL - ARMT DEMO ACCOUNT')).not_to be_present
        end
      end
    end

    context 'for device data' do
      let!(:results) { described_class.import_file(tabfile) }

      it 'creates devices with valid data' do
        expect(Device.count).to eq 4
      end

      it 'does not skip any devices' do
        expect(results[:devices][:skipped].count).to eq(0)
      end

      it 'sets device firmware' do
        expect(device_1.firmware).to eq '6.1.64'
      end

      it 'sets device hardware version' do
        expect(device_1.hw_version).to eq '8300'
      end

      it 'sets device host' do
        expect(device_1.host).to eq '128.136.162.21'
      end

      it 'sets device serial' do
        expect(device_1.serial).to eq '8300000270291437'
      end

      context 'when given a u110 serial' do
        it 'creates a gateway device' do
          expect(u110_device).to be_present
        end
      end

      it 'creates details for the device' do
        expect(device_1.details).to be_present
        expect(device_1.details.active_conn_type).to eq '1'
        expect(device_1.details.wan_1_conn_type).to eq 1
        expect(device_1.details.dial_mode).to eq 'B'
        expect(device_1.details.dial_backup_enabled).to be false
        expect(device_1.details.dial_backup_type).to eq 'T'
        expect(device_1.details.dial_modem_type).to eq 'I'
        expect(device_1.details.wan_1_cell_extender).to eq 'Y'
        expect(device_1.details.wan_2_cell_extender).to eq 'N'
        expect(device_1.details.configuration_name).to eq 'VPN-DEVICE1-MDL'
      end
    end

    context 'for sites' do
      let(:site)         { Site.find_by(name: 'ACCELERATED TAMPA PEAK10') }
      let(:organization) { Organization.find_by(import_name: 'USARMT') }

      before { described_class.import_file(tabfile) }

      it 'creates and associates sites for devices' do
        expect(device_1.site).to eq site
      end

      it 'associates sites to organizations' do
        expect(organization.sites).to include(site)
      end
    end

    context 'for contacts' do
      before { described_class.import_file(tabfile) }

      it 'creates and associates contacts to devices' do
        contact = device_2.contacts.first
        expect(contact).to be_present
        expect(contact.email).to eq 'james@test.com'
        expect(contact.name).to eq 'JAMES M. SAMPSON III'
        expect(contact.phone).to eq '8008675309'
      end

      it 'creates and associates contacts to device organizations' do
        contact = device_2.organization.contacts.first
        expect(contact).to be_present
        expect(contact.email).to eq 'james@test.com'
        expect(contact.name).to eq 'JAMES M. SAMPSON III'
        expect(contact.phone).to eq '8008675309'
      end

      context 'when only a contact name is given' do
        it 'saves the contact and associates it with a device' do
          contact = device_3.contacts.first
          expect(contact).to be_present
          expect(contact.name).to eq 'TEST CONTACT NAME'
          expect(contact.email).to be_blank
          expect(contact.phone).to be_blank
        end
      end
    end

    context 'for locations' do
      before { described_class.import_file(tabfile) }

      it 'creates a location for the device' do
        expect(device_2.location.address.to_s).to eq '3/5 BISHOP STREET STUART PARK DARWIN 0820'
      end

      it 'stores the address details in the record' do
        expect(device_2.location.address_1).to eq '3/5 BISHOP STREET STUART PARK'
        expect(device_2.location.city).to eq 'DARWIN'
      end

      it 'stores latitude and longitude' do
        expect(device_2.location.latitude).to eq 23.292481
        expect(device_2.location.longitude).to eq -159.851003
      end
    end

    context 'for snmp configurations' do
      before { described_class.import_file(tabfile) }

      it 'creates an snmp configuration for the device' do
        expect(device_1.configuration.snmp).to be_present
      end

      # Gateways force the community string to lower-case
      it 'downcases the community string' do
        expect(device_1.configuration.snmp.community).to eq 'armt-snmp'
        expect(device_2.configuration.snmp.community).to eq 'aview-snmp'
      end
    end

    context 'for portal profiles' do
      before { described_class.import_file(tabfile) }

      it 'creates a portal configuration for the device' do
        expect(device_1.configuration.portal).to be_present
      end

      it 'sets the portal user and password' do
        expect(device_1.configuration.portal.auth_user).to eq 'support'
        expect(device_1.configuration.portal.ssl_password).to eq 'oatu1j8zbp3wv42'
      end
    end

    context 'for device updates on devices not fully setup' do
      before do
        create(:blank_gateway_device, mac: '002704239308')
        described_class.import_file(tabfile)
      end

      it 'sets device firmware' do
        expect(device_1.firmware).to eq '6.1.64'
      end

      it 'sets device host' do
        expect(device_1.host).to eq '128.136.162.21'
      end

      it 'sets device serial' do
        expect(device_1.serial).to eq '8300000270291437'
      end

      it 'sets the device snmp community name' do
        expect(device_1.configuration.snmp.community).to eq 'armt-snmp'
      end
    end

    context 'for device updates on devices already initialized' do
      before do
        create(:blank_gateway_device, mac: '002704239308')
        device_1.update(firmware: '6.2.524', hw_version: 'U110', host: '1.2.3.4', serial: '8300000493291430')
        described_class.import_file(tabfile)
        device_1.reload
      end

      it 'does not override device firmware' do
        expect(device_1.firmware).to eq '6.2.524'
      end

      it 'does not override hardware version' do
        expect(device_1.hw_version).to eq 'U110'
      end

      it 'does not override host' do
        expect(device_1.host).to eq '1.2.3.4'
      end

      it 'does not override serial' do
        expect(device_1.serial).to eq '8300000493291430'
      end
    end
  end
end
