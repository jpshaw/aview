# frozen_string_literal: true

require 'rails_helper'

describe GatewayDualWANService do
  subject { described_class.new(wan_states) }

  describe '#blank?' do
    let(:wan_states) { [] }

    it 'returns true if wan states is blank' do
      expect(subject.blank?).to be true
    end
  end

  describe '#both_up?' do
    let(:wan_states) { [described_class::BOTH_UP.sample] }

    it 'returns true when both wans are up' do
      expect(subject.both_up?).to be true
    end
  end

  describe '#both_down?' do
    let(:wan_states) { [described_class::BOTH_DOWN.sample] }

    it 'returns true when both wans are up' do
      expect(subject.both_down?).to be true
    end
  end

  describe '#wan1_up_and_wan2_down?' do
    let(:wan_states) { [described_class::WAN1_UP_WAN2_DOWN.sample] }

    it 'returns true when both wans are up' do
      expect(subject.wan1_up_and_wan2_down?).to be true
    end
  end

  describe '#wan1_down_and_wan2_up?' do
    let(:wan_states) { [described_class::WAN1_DOWN_WAN2_UP.sample] }

    it 'returns true when both wans are up' do
      expect(subject.wan1_down_and_wan2_up?).to be true
    end
  end
end
