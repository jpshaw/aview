# frozen_string_literal: true

require 'rails_helper'

describe CarrierInformationService do
  context '#drop' do
    before do
      create(:carrier, :with_carrier_detail)
    end

    it 'drops all Carrier and CarrierDetail records' do
      subject.drop
      expect(Carrier.all.size).to eq 0
      expect(CarrierDetail.all.size).to eq 0
    end
  end

  context '#load_records_from_database' do
    subject(:result) { described_class.new.load_records_from_database }

    before do
      create(:carrier, :with_carrier_detail)
    end

    it 'returns records with carrier_details and carriers' do
      expect(result[:carriers].size).to eq 1
      expect(result[:carrier_details].size).to eq 1
    end
  end

  context '#load_records_from_file' do
    subject(:result) { described_class.new.load_records_from_file }

    it 'returns records with carrier_details and carriers' do
      expect(result[:carriers]).to be
      expect(result[:carrier_details]).to be
    end
  end

  context '#seed' do
    before do
      carrier = build(:carrier)
      carrier_detail = build(:carrier_detail)
      @carrier_service = described_class.new
      @carrier_service.records[:carriers] = [carrier]
      @carrier_service.records[:carrier_details] = [carrier_detail]
    end

    it 'creates Carrier and CarrierDetail records' do
      @carrier_service.seed
      expect(Carrier.all.size).to eq 1
      expect(CarrierDetail.all.size).to eq 1
    end
  end

  context '#serialized_records' do
    subject(:result) { described_class.new.serialized_records }

    it 'returns formatted YAML string' do
      expect(result).to be_a String
    end
  end
end
