# frozen_string_literal: true

require 'rails_helper'

describe DeviceHeartbeatMonitorService do
  let(:cache) { double('cache') }

  before do
    # Always stub the cache object so TorqueBox object doesn't wreak havoc.
    allow_any_instance_of(described_class).to receive(:cache).and_return(cache)
  end

  context 'During initialization' do
    describe '#initialize' do
      it 'logs the cache properties' do
        expect_any_instance_of(described_class).to receive(:log_cache_properties)
        described_class.new
      end
    end

    describe '#log_cache_properties' do
      it "retrieves the cache's clustering mode, eviction strategy and max entries" do
        expect(cache).to receive(:clustering_mode)
        expect(cache).to receive(:eviction_strategy)
        expect(cache).to receive(:max_entries)

        # It is implied that #log_cache_properties is called during initialization based on above spec.
        described_class.new
      end
    end
  end

  context 'After initialization' do
    let(:device_state)  { DeviceState.new(state: DeviceState::States::UP) }
    let(:device)        { create(:test_device, device_state: device_state) }

    before do
      # We don't need the cache to be used, it errors out during testing.
      allow_any_instance_of(described_class).to receive(:log_cache_properties)
    end

    describe '#start' do
      it 'creates a thread to run the job' do
        expect(Thread).to receive(:new)
        subject.start
      end

      it 'runs the job in the thread' do
        expect(subject).to receive(:run)
        subject.start
        subject.thread.join
      end
    end

    describe '#run' do
      it 'syncs cache with monitorable devices' do
        subject.instance_variable_set(:@done, true)
        expect(subject).to receive(:sync_cache_with_monitorable_devices)
        subject.run
      end

      it 'processes the cache' do
        expect(subject).to receive(:done).and_return(false, true)
        expect(subject).to receive(:process_cache)
        subject.run
      end
    end

    describe '#sync_cache_with_monitorable_devices' do
      it 'processes monitorable devices' do
        expect(Device).to receive(:monitorable).and_return(Device.limit(0))
        subject.__send__(:sync_cache_with_monitorable_devices)
      end

      context 'when a device does not have a last_heartbeat_at value' do
        before do
          expect(device.last_heartbeat_at).to be nil
        end

        it 'does not update the cache' do
          expect(cache).not_to receive(:put)
          subject.__send__(:sync_cache_with_monitorable_devices)
        end
      end

      context 'when a device has a last_heartbeat_at value' do
        before do
          device.update_attribute(:last_heartbeat_at, Time.now)
        end

        it "updates the cache with the device's mac and info" do
          letters     = ('a'..'z').to_a
          device_info = { a: letters.sample, b: letters.sample }
          allow(subject).to receive(:device_info).and_return(device_info)
          expect(cache).to receive(:put).with(device.mac, device_info)
          subject.__send__(:sync_cache_with_monitorable_devices)
        end
      end
    end

    describe '#device_info' do
      it 'returns a hash with device info' do
        now = Time.now
        expect(device).to receive(:last_heartbeat_at).and_return(now)
        expect(device).to receive(:heartbeat_frequency).and_return(1)
        expect(device).to receive(:grace_period).and_return(2)

        device_info = subject.__send__(:device_info, device)

        expect(device_info.class).to eq(Hash)
        expect(device_info.keys.sort).to eq(%i[grace_period heartbeat_frequency last_heartbeat_at])
        expect(device_info[:last_heartbeat_at]).to eq(now)
        expect(device_info[:heartbeat_frequency]).to eq(1)
        expect(device_info[:grace_period]).to eq(2)
      end
    end

    describe '#process_cache' do
      it 'processes keys from cache' do
        expect(cache).to receive(:keys).and_return([])
        subject.__send__(:process_cache)
      end

      it 'retrieves cached info for a given key' do
        # We don't need to expire the device.
        allow(subject).to receive(:valid_cached_info?).and_return(false)

        allow(cache).to receive(:keys).and_return(%w[abc])
        expect(subject).to receive(:cached_info_for).with('abc')
        subject.__send__(:process_cache)
      end

      it 'does not expire the device if the cached info is invalid' do
        expect(cache).to receive(:keys).and_return(%w[abc])
        expect(subject).to receive(:cached_info_for)
        expect(subject).to receive(:valid_cached_info?).and_return(false)
        expect(subject).not_to receive(:device_expired?)
        expect(subject).not_to receive(:expire_device)
        subject.__send__(:process_cache)
      end

      it 'does not expire the device if the device has not expired' do
        expect(cache).to receive(:keys).and_return(%w[abc])
        expect(subject).to receive(:cached_info_for)
        expect(subject).to receive(:valid_cached_info?).and_return(true)
        expect(subject).to receive(:device_expired?).and_return(false)
        expect(subject).not_to receive(:expire_device)
        subject.__send__(:process_cache)
      end

      it 'expires the device if the cached info is valid and the device has expired' do
        expect(cache).to receive(:keys).and_return(%w[abc])
        expect(subject).to receive(:cached_info_for)
        expect(subject).to receive(:valid_cached_info?).and_return(true)
        expect(subject).to receive(:device_expired?).and_return(true)
        expect(subject).to receive(:expire_device)
        subject.__send__(:process_cache)
      end
    end

    describe '#cached_info_for' do
      it 'retrieves the values from the cache for the given key' do
        expect(cache).to receive(:get).with('abc')
        subject.__send__(:cached_info_for, 'abc')
      end
    end

    describe '#valid_cached_info?' do
      before do
        allow(subject).to receive(:log_error)
      end

      context 'with invalid cached info object type' do
        before do
          expect(subject).to receive(:cached_info).at_least(:once).and_return([])
          expect(subject).not_to receive(:last_heartbeat_at)
          expect(subject).not_to receive(:heartbeat_frequency)
          expect(subject).not_to receive(:grace_period)
        end

        it 'removes the entry from the cache' do
          expect(cache).to receive(:remove)
          subject.__send__(:valid_cached_info?)
        end

        it 'returns false' do
          allow(cache).to receive(:remove)
          expect(subject.__send__(:valid_cached_info?)).to be(false)
        end
      end

      context 'with invalid cached info values' do
        before do
          allow(subject).to receive(:cached_info).and_return({})
        end

        it 'removes the entry from the cache' do
          expect(cache).to receive(:remove)
          subject.__send__(:valid_cached_info?)
        end

        it 'returns false' do
          allow(cache).to receive(:remove)

          # Invalid last_heartbeat_at
          allow(subject).to receive(:last_heartbeat_at).and_return(nil)
          allow(subject).to receive(:heartbeat_frequency).and_return(1)
          allow(subject).to receive(:grace_period).and_return(2)
          expect(subject.__send__(:valid_cached_info?)).to be(false)

          # Invalid heartbeat_frequency
          allow(subject).to receive(:last_heartbeat_at).and_return(Time.now)
          allow(subject).to receive(:heartbeat_frequency).and_return(nil)
          allow(subject).to receive(:grace_period).and_return(2)
          expect(subject.__send__(:valid_cached_info?)).to be(false)

          # Invalid grace_period
          allow(subject).to receive(:last_heartbeat_at).and_return(Time.now)
          allow(subject).to receive(:heartbeat_frequency).and_return(1)
          expect(subject).to receive(:grace_period).and_return(nil)
          expect(subject.__send__(:valid_cached_info?)).to be(false)
        end
      end

      context 'with valid cached info object type and values' do
        before do
          allow(subject).to receive(:cached_info).and_return(
            last_heartbeat_at:    Time.now,
            heartbeat_frequency:  1,
            grace_period:         2
          )
        end

        it 'does not remove the entry from the cache' do
          expect(cache).not_to receive(:remove)
          subject.__send__(:valid_cached_info?)
        end

        it 'returns true' do
          expect(subject.__send__(:valid_cached_info?)).to be(true)
        end
      end
    end

    describe '#device_expired?' do
      context 'when the expiration time is in the past' do
        before do
          expect(subject).to receive(:expiration_time).and_return(Time.now - 1.day)
        end

        it 'returns true' do
          expect(subject.__send__(:device_expired?)).to be(true)
        end
      end

      context 'when the expiration time is in the future' do
        before do
          expect(subject).to receive(:expiration_time).and_return(Time.now + 1.day)
        end

        it 'returns false' do
          expect(subject.__send__(:device_expired?)).to be(false)
        end
      end

      context 'when the expiration time matches the current time' do
        before do
          current_time = Time.now
          allow(Time).to receive(:now).and_return(current_time)
          expect(subject).to receive(:expiration_time).and_return(current_time)
        end

        it 'returns false' do
          expect(subject.__send__(:device_expired?)).to be(false)
        end
      end
    end

    describe '#expire_device' do
      it 'removes the entry from the cache' do
        expect(cache).to receive(:remove)
        subject.__send__(:expire_device)
      end

      context 'when a device is not found' do
        before do
          allow(cache).to receive(:remove)
          allow(Device).to receive(:find_by).and_return(nil)
        end

        it "does not send a 'down!' command to the device" do
          expect_any_instance_of(Device).not_to receive(:down!)
          subject.__send__(:expire_device)
        end
      end

      context 'when a device is found' do
        let(:device) { Device.new }

        before do
          allow(cache).to receive(:remove)
          allow(Device).to receive(:find_by).and_return(device)
        end

        it "sends a 'down!' command to the device" do
          expect(device).to receive(:down!)
          subject.__send__(:expire_device)
        end
      end
    end
  end
end
