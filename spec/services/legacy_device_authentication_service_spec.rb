# frozen_string_literal: true

require 'rails_helper'

describe LegacyDeviceAuthenticationService, services: true do
  let(:service) { described_class.new(device_id, headers) }
  let(:headers) { { 'HTTP_X_FORWARDED_PROTO' => 'https' } }

  describe '#execute' do
    subject { service.execute }

    context 'with a nil device_id' do
      let(:device_id) { nil }

      it 'raises Aview::Device::NotAuthorizedError exception' do
        expect { subject }.to raise_error(Aview::Device::NotAuthorizedError)
      end
    end

    context 'with a Netbridge SX device_id' do
      let(:device_id) { generate(:netbridge_sx_mac) }

      it { is_expected.to be true }
    end

    context 'with a Netbridge FX device_id' do
      let(:device_id) { generate(:netbridge_fx_mac) }

      context 'with a valid SDN header' do
        before { headers.merge!('HTTP_X_SSL_CERT_SDN' => "CN=#{device_id}") }

        it { is_expected.to be true }
      end

      context 'with an invalid SDN header' do
        before { headers.merge!('HTTP_X_SSL_CERT_SDN' => 'CN=INVALID') }

        it 'raises Aview::Device::NotAuthorizedError exception' do
          expect { subject }.to raise_error(Aview::Device::NotAuthorizedError)
        end
      end
    end
  end
end
