# frozen_string_literal: true

require 'rails_helper'

describe DeviceAssociationService do
  subject { described_class }

  describe '#associate_devices' do
    context 'when given valid data' do
      let(:data)    { build(:device_association).attributes }
      let(:details) { build(:device_association_with_details).details }
      let(:service) { subject.new(data.merge(details)) }

      context 'when the assocation does not exist' do
        it 'creates an association' do
          expect do
            service.associate_devices
          end.to change(DeviceAssociation, :count).by(1)
        end
      end

      context 'when the association exists' do
        before { create(:device_association, data) }

        it 'does not create an association' do
          expect do
            service.associate_devices
          end.not_to change(DeviceAssociation, :count)
        end
      end

      it 'assigns details' do
        service.associate_devices
        association = DeviceAssociation.first
        expect(association.ip_address).to eq details['ip_address']
        expect(association.source_type).to eq details['source_type']
        expect(association.target_type).to eq details['target_type']
        expect(association.source_interface).to eq details['source_interface']
        expect(association.target_interface).to eq details['target_interface']
      end
    end

    context 'when given invalid data' do
      let(:data)    { {} }
      let(:service) { subject.new(data) }

      it 'does not create an association' do
        expect do
          service.associate_devices
        end.not_to change(DeviceAssociation, :count)
      end
    end
  end

  describe '#dissociate_devices' do
    context 'when given valid data' do
      let(:data)    { build(:device_association).attributes }
      let(:service) { subject.new(data) }

      context 'when the association exists' do
        before { create(:device_association, data) }

        it 'removes the association' do
          expect do
            service.dissociate_devices
          end.to change(DeviceAssociation, :count).by(-1)
        end
      end

      context 'when the assocation does not exist' do
        it 'does not remove any associations' do
          expect do
            service.dissociate_devices
          end.not_to change(DeviceAssociation, :count)
        end
      end
    end

    context 'when given invalid data' do
      let(:data)    { {} }
      let(:service) { subject.new(data) }

      it 'does not remove any associations' do
        expect do
          service.dissociate_devices
        end.not_to change(DeviceAssociation, :count)
      end
    end
  end
end
