# frozen_string_literal: true

require 'rails_helper'

describe MacAddressCorrelationService do
  subject { described_class }

  describe '#execute' do
    context 'when given nil' do
      it 'returns nil' do
        expect(subject.new(nil).execute).to be nil
      end
    end

    context 'when given a valid mac address' do
      let(:client_device) { create(:gateway_device) }

      context 'for a mac that is correlated to a device' do
        let(:mac)     { "00#{(client_device.mac.hex + 2).to_s(16)}" }
        let(:service) { subject.new(mac) }

        before { expect(mac).not_to eq client_device.mac }

        it 'returns the client mac' do
          expect(service.execute).to eq client_device.mac
        end
      end

      context 'for a device mac' do
        let(:mac)     { client_device.mac }
        let(:service) { subject.new(mac) }

        it 'returns the device mac' do
          expect(service.execute).to eq mac
        end
      end

      context 'for a mac that is not correlated to a device' do
        let(:mac)     { generate(:mac_address) }
        let(:service) { subject.new(mac) }

        it 'returns the mac' do
          expect(service.execute).to eq mac
        end
      end

      context 'for a mac with colons' do
        let(:mac)           { '2B:AD:C9:01:D5:14' }
        let(:formatted_mac) { mac.delete(':') }
        let(:service)       { subject.new(mac) }

        it 'strips the colons' do
          expect(service.execute).to eq formatted_mac
        end
      end

      it 'caches the correlated mac result' do
        service = subject.new(generate(:mac_address))
        expect(service).to receive(:correlated_mac).once
        service.execute
        service.execute
      end
    end

    context 'when given an invalid mac address' do
      let(:mac) { 'abc' }

      it 'returns nil' do
        expect(subject.new(mac).execute).to be nil
      end

      it 'does not attempt to correlate the mac' do
        service = subject.new(mac)
        expect(service).not_to receive(:correlated_mac)
        service.execute
      end
    end
  end
end
