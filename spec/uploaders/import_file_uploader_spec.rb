# frozen_string_literal: true

require 'rails_helper'

describe ImportFileUploader do
  it 'knows its size limit' do
    expect(described_class.constants).to include(:FILE_SIZE_LIMIT)
  end

  it "knows its file's checksum" do
    expect(subject).to respond_to(:checksum)
    expect(Digest::MD5).to receive(:hexdigest)
    subject.checksum
  end
end
