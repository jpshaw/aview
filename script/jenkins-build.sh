#!/bin/bash --login
#
# Build the webapp with Jenkins
#
# script/jenkins-build.sh [aview|armt|avwob]
#

set -e

# Terminal colors
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

print_info()  { print_msg "${GREEN}$1${NC}"            ; }
print_error() { print_msg "${RED}$1${NC}"              ; }
print_msg()   { printf "==> [$(date "+%H:%M:%S")] $1\n"; }

cd "$(dirname "$0")/.."
[ -z "$DEBUG" ] || set -x

export ENGINE="$1"

# GC customizations
export RUBY_GC_MALLOC_LIMIT=79000000
export RUBY_GC_HEAP_INIT_SLOTS=800000
export RUBY_HEAP_FREE_MIN=100000
export RUBY_HEAP_SLOTS_INCREMENT=400000
export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1

# Setup environment
RAILS_ROOT="$(cd "$(dirname "$0")"/.. && pwd)"
export RAILS_ROOT
export RAILS_ENV="test"
export RACK_ROOT="$RAILS_ROOT"
export RACK_ENV="$RAILS_ENV"

clear_bundler() {
  if [ -d '.bundle' ]; then
    rm -rf '.bundle'
  fi
}

clear_knobs() {
  if test -n "$(find . -maxdepth 1 -name '*.knob' -print -quit)"; then
    rm -f *.knob
  fi
}

cleanup() {
  bin/engine unload
  clear_bundler
}

print_info "Building $ENGINE webapp ..."
clear_bundler
clear_knobs

print_info "Copying configurations"
cp config/database.yml.example config/database.yml

print_info "Loading $ENGINE engine"
bin/engine load "$ENGINE"

print_info "Creating revision"
export REVISION="$(git rev-parse HEAD | cut -c1-7)"
echo "$REVISION" > REVISION

print_info "Bundle for assets"
rvm use 2.3.6
bundle check || script/bundle_install

print_info "Precompiling assets"
yarn install
NODE_ENV=production RAILS_ENV=production DB_ADAPTER=nulldb \
  bundle exec rake assets:precompile

print_info "Vendoring jruby gems"
export JAVA_OPTS="$JAVA_OPTS -Xms1024m -Xmx1024m"
export JRUBY_OPTS="-Xnative.verbose=true --dev"
rvm use 9.1.17.0
bundle check || script/bundle_install
bundle install --clean --deployment --without development test assets

print_info "Patching ..."
git apply patch/socksify-jruby-fix.patch 2> patch.error.log || true
sed -i.bak '/# Load engine/,$d' ./Gemfile
echo "gem \"$ENGINE\", path: \"engines/$ENGINE\"" >> Gemfile

print_info "Creating torquebox archive"
bundle exec torquebox archive > torquebox.log
mv *.knob "$ENGINE-$REVISION.knob"

# Copy to local nix dependencies folder if it exists
if [ -d "/var/www/html/aview-dep.digi.com/" ]; then
  cp "$ENGINE-$REVISION.knob" /var/www/html/aview-dep.digi.com/
fi

# TODO: Copy to local jenkins cache

print_info "Cleaning up ..."
cleanup

print_info "Build finished"
