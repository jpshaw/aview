require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AviewWebapp
  class Application < Rails::Application

    config.assets.initialize_on_precompile = false
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components')

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths += %W(#{config.root}/app)
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{config.root}/app/models/configurations)
    config.autoload_paths += %W(#{config.root}/app/controllers/configuration)

    # Force HTTP requests to use UTF-8
    config.middleware.insert 0, Rack::UTF8Sanitizer

    ### Caching

    # Use TorqueBox::Infinispan::Cache for the Rails cache store
    if defined? TorqueBox::Infinispan::Cache
      config.cache_store = :torque_box_store
    end

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'UTC'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s].sort
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Setup custom 404 and 500 pages
    config.exceptions_app = self.routes

    config.to_prepare do
      Devise::Mailer.layout 'inspinia_mail'
    end

    # List of whitelisted strong parameters that are permitted by default
    config.action_controller.always_permitted_parameters = %w( controller action format )
  end
end
