const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')

environment.loaders.append('vue', vue)
environment.config.externals = {
	'highcharts': 'Highcharts'
}
module.exports = environment
