AviewWebapp::Application.routes.draw do

  get 'errors/not_found'

  get 'errors/internal_server_error'

  apipie

  namespace :api, defaults: { format: :json } do
    api_version(module: 'V1', path: { value: 'v1' }, default: false) do
      namespace :auth do
        resources :att_bc, only: [:create, :index]
      end
    end

    api_version(module: 'V2', path: { value: 'v2' }, default: true) do
      resources :devices, only: [:index, :show] do
        member do
          patch :update_settings
        end
      end
      resources :accounts, only: [:update] do
        member do
          patch :update_password
          patch :update_filter
          patch :clear_filter
          patch :generate_api_token
        end

        resources :devices, only: [:index, :show, :update] do
        end

        resources :notification_subscriptions do
          collection do
            get :search
            get :reset
          end
        end

        resources :notification_profiles do
          member do
            get :subscriptions
          end
        end
      end
      resources :users
      resources :roles
      resources :sites
      resources :passwords, only: [:update]
      resources :organizations do
        member do
          get :abilities
        end
      end
    end

    api_version(module: 'V3', path: { value: 'v3' }, default: false) do
      resources :devices, only: [:update] do
        member do
          patch :update_location
          patch :clear_location
        end

        resources :contacts, only: [:new, :show, :update, :create, :index, :destroy]
        resources :dial_tests, only: [:index]
      end
    end

    api_version(module: 'V4', path: { value: 'v4' }, default: false) do
      resources :devices, only: [:index] do
        collection do
          get :usage
        end
      end
    end
  end

  scope module: :api, defaults: {format: :json} do
    namespace :device_configurations do
      resources :v2, only: [:show]
    end

    namespace :certificates do
      resources :v2, only: [:create]
      post '/', controller: 'v2', action: :create
    end
  end

  post "/pdns/lookup", to: "power_dns_backend#lookup"
  post "/pdns/getDomainMetadata", to: proc { [404, {}, ['']] }
  post "/pdns/calculateSOASerial", to: proc { [404, {}, ['']] }

  resource :dashboard, only: :show do
    collection do
      get 'device_table'
    end
  end

  resource :highcharts, only: [] do
    collection do
      get 'cellular_utilization'
      get 'connectivity_status_chart'
      get 'deployed_status_chart'
      get 'ids_alerts_histogram'
      get 'network_type_chart'
      get 'signal_strength_chart'
      get 'intelliflow_app_usage'
      get 'intelliflow_app_usage_over_time'
      get 'wan_utilization'
    end
  end

  root :to => 'dashboards#show'

  devise_for :users
  resources :users do
    resources :permissions

    collection do
      resource :activation, controller: :activations, only: [:new, :create]
    end
  end

  resources :account, :except => :show do
    patch :update, :on => :collection
    patch :change, :on => :collection
    get   :change, :on => :collection
    get   :change_password, :on => :collection
    patch :update_password, :on => :collection
    get   :api, :on => :collection
    patch :generate_api_token, :on => :collection
  end

  resources :searches, only: [:index, :new]

  resources :notifications do
    get :since, :on => :collection
  end

  post 'data_tables/devices/(:data_table_class)/(:reference_id)', to: 'data_tables#devices', as: :devices_data_tables
  post 'data_tables/events/(:mac)', to: 'data_tables#events', as: :events_data_tables
  # TODO: Get rid of index route and action, used by reports
  resources :data_tables, only: [] do
    collection do
      post :searches
      post :sites
      post :users
      post :notifications
      post :intelliflow_client_usage
      post :groups
      post :group_devices
    end
  end

  resources :devices, except: :create do
    collection do
      get :vpn_gateway
      get :wifi
      get :legacy_cellular
      get :cellular
      get :dial_to_ip
      get :remote_manager
      get :ucpe
      get :to_csv

      post :with_selected
      post :vpn_gateway
      post :wifi
      post :legacy_cellular
      post :cellular
      post :dial_to_ip
      post :remote_manager
      post :ucpe
    end

    member do
      get :terminal
      get :toggle_deployment
      get :replace
      get :events
      get :remove_contact
      get :commands_list
      get :remote
      get :organization_configurations
      get :move_to_organization

      post :replace_with
      post :add_contact

      patch :remote
      patch :save_configuration
    end

    resources :modems, only: :destroy

    resources :charts do
      collection do
        get :signal
        get :network
      end
    end

    resources :device_associations do
      collection do
        get :table
      end
    end

    get "/events/since" => "devices#events_since"

    namespace :configuration do
      resource :network, only: [:show, :update]
      resource :internet_routes, only: [:show, :create, :update, :destroy] do
        delete :destroy_all
      end
      resource :cascaded_networks, only: [:show, :create, :update, :destroy] do
        delete :destroy_all
      end
    end

    resources :ids_alerts, only: :index
  end

  resources :tunnel_servers, only: [:index, :show] do
    member do
      get :ip_addresses
    end
  end

  resources :configurations

  get '/configurations/:id/device_config', to: 'legacy_configurations#device_config'

  constraints subdomain: "#{Settings.subdomains.configurations}" do
    get '/:id', to: 'legacy_configurations#device_config'
  end

  resources :legacy_configurations do
    collection do
      get :inherited_values
      get :available_parents
      get :organization_configurations
      get :parent_configuration
      get :exists_for_organization
      get :cellular
    end

    member do
      post  :copy
      get   :device_config
    end
  end

  resources :reports do
    member do
      post :export
    end

    collection do
      get :availability
      get :cellular_network_type
      get :cellular_utilization
      get :cellular_signal_strength
      get :vig
      get :finished_generating
    end
  end

  resources :chart_data do
    get :device, :on => :collection
  end

  resources :sites do
    get :to_csv, :on => :collection

    collection do
      get :search
    end
  end

  resources :organizations do
    member do
      get   :edit_logo
      patch :update_logo
      get   :edit_footer
      patch :update_footer
      patch :update_device_settings
      get :actions_menu
    end

    collection do
      post  :move_devices_to
      get :search
    end

    resources :roles do
      get :abilities
    end

    resources :permissions
    resources :data_plans do
      collection do
        post :move_devices_to
        get :exists_for_organization
      end
    end


  end

  resources :maps do
    get :pins, :on => :collection
    get :alert_pins, :on => :collection
  end

  resources :events do
    get :new_since, :on => :collection
    get :since, :on => :collection
  end

  resources :device_configurations, except: [:index] do
    collection do
      get '/v2/:mac', to: 'device_configurations#device'
      get :dial_to_ip
      get :remote_manager
      get :cellular
      get :ucpe
      get :exists_for_organization
    end
    member do
      get   :edit_linked_devices
      patch :update_linked_devices
      post  :copy
    end
  end

  resources :device_firmwares, only: [] do
    member do
      get :schema_file
    end
  end

  get '/admin', :to => 'admin#index'

  get '/vvig_pnc', :to => 'vvig_pnc#index'

  get '/vvig_pnc/data', :to => 'vvig_pnc#get_data'

  namespace :admin do
    get :server_log

    get :device_statistics

    resources :dns

    resources :imports do
      collection do
        get     :template
        delete  :destroy_all
      end

      member do
        get :objects
      end
    end

    resources :certificates, only: [:index, :destroy] do
      member do
        patch :approve
      end
    end

    resources :customer_services, except: [:show]

    resources :settings, only: [] do
      collection do
        patch :update
      end
    end
  end

  resources :device_models, only: [:index] do
    resources :device_firmwares, only: [:index]

    member do
      get :firmware_select
    end
  end

  resources :groups do
    collection do
      post :destroy_multiple
    end

    member do
      get :device_search
      post :add_devices
      post :remove_devices
      get :edit_firmware
      post :update_firmware
      get :group_widgets
      get :configuration
    end
  end

  namespace :services, defaults: { format: :json } do
    scope :configurations do
      get   'device/:device_id', to: 'configurations#device'
      patch 'device/:device_id', to: 'configurations#update_device'
      get   'schemas/:model/ui', to: 'configurations/schemas#ui'
    end
  end

  post "/feedback" => "feedback#create"

  get "/cellular_locations" => "cellular_locations#show"

  get "/401" => "errors#unauthorized"
  get "/404" => "errors#not_found"
  get "/500" => "errors#internal_server_error"

  get '*unmatched_route', via: :all, to: 'errors#not_found'
end
