#TODO: Rails 4 - Remove these monkeypatches. They are fixed in Rails 4 with this
# commit: https://github.com/rails/rails/commit/a548792

if RUBY_PLATFORM.downcase == 'java'

  class Java::JavaSql::SQLRecoverableException
    def blame_file!(file)
      (@blamed_files ||= []).unshift file
    end
  end

  class Java::JavaLang::NoClassDefFoundError
    def blame_file!(file)
      (@blamed_files ||= []).unshift file
    end
  end

  class Java::JavaLang::NullPointerException
    def blame_file!(file)
      (@blamed_files ||= []).unshift file
    end
  end

  class Java::JavaSql::SQLException
    def blame_file!(file)
      (@blamed_files ||= []).unshift file
    end
  end

end
