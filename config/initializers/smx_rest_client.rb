require 'smx-rest-client'

ServiceManagerRest.configure do |config|
  config.scheme = ENV['SMX_REST_SCHEME'] || Settings.services.smx_rest.scheme
  config.host   = ENV['SMX_REST_HOST']   || Settings.services.smx_rest.host
  # config.debugging = Rails.env.development?
end
