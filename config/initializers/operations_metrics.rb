require 'operations-metrics'

if Settings.operations_metrics.enabled? and not Rails.env.test?
  Operations::Metrics.configure(
    enabled: true,
    host: Settings.operations_metrics.host,
    port: Settings.operations_metrics.port
  )
else
  Operations::Metrics.configure(enabled: false)
end

# TODO: Move this to the operations metrics gem railtie
if defined?(TorqueBox::Messaging::MessageProcessor)
  TorqueBox::Messaging::MessageProcessor.send(:include, Operations::Metrics::Helpers)
end