require 'kraken/geolocation'

Rails.application.config.after_initialize do
  unless Rails.env.test?
    Kraken::Location.on_geocode do |payload|
      Kraken::MessageBus.publish(payload, topic: 'aview.locations.geocoder')
    end

    Kraken::Location.on_reverse_geocode do |payload|
      Kraken::MessageBus.publish(payload, topic: 'aview.locations.reverse-geocoder')
    end
  end
end
