# Be sure to restart your server when you modify this file.

if ENV['TORQUEBOX_APP_NAME']
  Rails.application.config.session_store :torquebox_store,
    httponly: true, secure: Rails.env.production?
else
  Rails.application.config.session_store :cookie_store,
    key: '_aview_session', httponly: true, secure: Rails.env.production?
end
