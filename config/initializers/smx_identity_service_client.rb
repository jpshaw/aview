require 'smx_identity_client'

SmxIdentityClient.configure do |config|
  config.host = "#{Settings.services.smx_identity.host}:#{Settings.services.smx_identity.port}"
  config.scheme = "http"
  config.base_path = "/"
end
