require 'kraken-message-bus'

# TODO: Replce Kraken::MessageBus with ApplicationBus
Kraken::MessageBus.configure do |config|
  config.brokers = Settings.kafka.brokers
  config.producer_threshold = 1000
  config.producer_interval = 5
end
