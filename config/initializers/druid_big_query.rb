if Settings.druid_big_query_service.enabled
  DruidDB::Node::Broker.class_eval do
    def connection
      DruidDB::Connection.new(host: Settings.druid_big_query_service.host,
                              port: Settings.druid_big_query_service.port)
    end
  end
end
