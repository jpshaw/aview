require 'voltage_client'

VoltageClient.configure do |config|
  config.host = "#{Settings.services.voltage.host}:#{Settings.services.voltage.port}"
  config.scheme = Settings.services.voltage.scheme
  config.timeout = Settings.services.voltage.timeout
end
