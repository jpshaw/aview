# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = 'v2.0.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all nonJS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(
  framework.css
  base.css
  plugins.css
  aview.css
  update_events.js
  maps.js
  charts.js
  config_compiler.js
  device_commands.js
  mac_address_filter.js
  contact_cards.js
  reports_form.js
  update_notifications.js
  html5.js
  form_change_guard.js
  configuration_fields_disable.js
  update_raw_events.js
  device_configurations.js
  admin_settings.js
  organizations.js
  device_location.js
  visualsearch_datatable_integration.js
  visualsearch_map_integration.js
  x-editable/loading.gif
  XMODEM.js
  ion.rangeslider/sprite-skin-flat.png
  individual_device_configuration.js
  markerclusterer.js
  oms.min.js
)
