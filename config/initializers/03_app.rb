module Aview
  VERSION ||= File.read(Rails.root.join('VERSION')).strip.freeze

  def self.version
    VERSION
  end

  def self.config
    Settings
  end
end

# Load Aview as soon as possible
Dir[Rails.root.join('lib/aview/*.rb')].each do |file|
  require file
end

# Load Kraken as soon as possible
Dir[Rails.root.join('lib/kraken/*.rb')].each do |file|
  require file
end
