# Support for converting an array to a hash was added in Ruby 2.1:
#   http://blog.pivotal.io/pivotal-labs/labs/ruby-2-1-0-changes-of-note
#
# TODO: Remove this when JRuby is updated to support Ruby 2.1+ (current 1.9.3)
#
# Based on the ruby implementation found here:
#  https://github.com/ruby/ruby/blob/77a952d1fb140b748debe142b1a408b89abdf409/array.c#L2156
#
unless Array.method_defined?(:to_h)
  class Array
    def to_h
      hash = Hash.new

      self.each_with_index do |array, i|
        raise TypeError.new("wrong element type #{array.class} at #{i} (expected array)") unless array.is_a?(Array)
        raise ArgumentError.new("wrong array length at #{i} (expected 2, was #{array.length})") if array.length != 2

        hash[array[0]] = array[1]
      end

      hash
    end
  end
end