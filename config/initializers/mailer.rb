if Settings.email.present? and not Rails.env.test?
  Rails.application.configure do
    protocol = Settings.https_enabled ? 'https' : 'http'

    config.action_mailer.default_url_options   = { protocol: 'https', host: Settings.host }
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.perform_deliveries    = true
    config.action_mailer.delivery_method       = Settings.email.delivery_method
    config.action_mailer.asset_host = Rails.env.development? ? Rails.public_path.to_s : "https://#{Settings.host}/"
  end
else
  Rails.application.configure do
    protocol = Settings.https_enabled ? 'https' : 'http'

    config.action_mailer.default_url_options   = { protocol: 'http', host: Settings.host }
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.perform_deliveries    = false
    config.action_mailer.delivery_method       = Settings.email.delivery_method
  end
end
