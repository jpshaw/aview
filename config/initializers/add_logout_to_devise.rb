module Devise
  # Enable logging out by default.
  mattr_accessor :logout_enabled
  @@logout_enabled = true
end
