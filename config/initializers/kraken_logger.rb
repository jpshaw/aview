require 'kraken/logger'

Kraken::Logger.configure do |config|
  config.base_dir = Rails.root.join('log')
  config.verbose = !Rails.env.production?
end
