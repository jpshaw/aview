
# Ensure measurement subclasses are loaded correctly.
measurements_dir = Rails.root.join('app', 'models', 'measurement', '*.rb')
Dir[measurements_dir].each { |file| require file }
