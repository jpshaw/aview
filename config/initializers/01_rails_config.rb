# The first initializer to run.

# Config is used by other initializers, ensure it's setup first
Config.setup do |config|
  config.const_name = "Settings"
  config.merge_nil_values = false
end
