if Rails.env.production? || Rails.env.staging? || Rails.env.beta?
  Rails.application.configure do
    # Exception Notifier Middleware for Sending emails to us if there is a 404/500 error
    config.middleware.use ExceptionNotification::Rack,
      email: {
        email_prefix: "[#{Settings.host} Exception] ",
        sender_address: %{"Aview Exception Notifier" <aview-alerts@accelerated.com>},
        exception_recipients: %w{aview@accelerated.com}
      }
  end
end
