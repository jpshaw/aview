require 'silencer/logger'

Rails.application.configure do
  unless Rails.env.development?
    # Disable device event poller log spam
    config.middleware.swap Rails::Rack::Logger, Silencer::Logger, :silence => [%r{^/data_tables\/events\/[a-zA-Z0-9]{12}\.json}]
  end
end
