SeedMigration.config do |config|
  config.ignore_ids = true
end

SeedMigration.register Country
SeedMigration.register Timezone
SeedMigration.register Organization
SeedMigration.register DeviceCategory
SeedMigration.register DeviceEventUUID
SeedMigration.register Ability
SeedMigration.register Role
SeedMigration.register Permission
SeedMigration.register DeviceModel
SeedMigration.register NotificationProfile
SeedMigration.register RoleAbility
SeedMigration.register PowerDnsRecordManager::Domain
SeedMigration.register PowerDnsRecordManager::Record
SeedMigration.register Carrier
SeedMigration.register CarrierDetail
SeedMigration.register Region
