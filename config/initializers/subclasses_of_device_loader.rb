#NOTE:  This file needs to be towards the end of the list in initializers or it might break Rails.
#       For instance, if it goes before devise.rb it won't work because Devise has not been initialized
#       yet and Devise is used in the Device model.

# Load subclasses of Device if in development.
if Rails.env.development?
  Dir["#{Rails.root}/app/models/devices/*.rb"].each {|file| require file }
end
