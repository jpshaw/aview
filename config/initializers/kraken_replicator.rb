if Settings.service_manager_sync.enabled
  require 'kraken/replicator'

  Kraken::Replicator.on_replication do |payload|
    Kraken::MessageBus.publish(payload, topic: 'aview.replicator')
  end

  Rails.application.config.after_initialize do
    Kraken::Replicator::SMX.load!
  end
end
