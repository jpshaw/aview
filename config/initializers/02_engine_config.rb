# Load engine settings
require 'engine'

if Engine.loaded?
  %w{settings.yml settings.staging.yml settings.secret.yml}.each do |settings_file|
    settings_file_path = "#{Engine.engine_path}/config/#{settings_file}"
    next unless File.exists?(settings_file_path)
    Settings.add_source!(settings_file_path)
  end
end

# Load server-specific settings
server_specific_config = Rails.root.join('config', 'server.settings.yml').to_s

if File.exists?(server_specific_config)
  Settings.add_source!(server_specific_config)
end

Settings.reload!
