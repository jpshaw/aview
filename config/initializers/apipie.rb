Apipie.configure do |config|
  config.app_name                = 'Accelerated View API'
  config.copyright               = "&copy; #{Time.now.year} Accelerated Concepts"
  config.default_version         = '2.0'
  config.reload_controllers      = Rails.env.development?
  config.api_base_url            = '/api'
  config.doc_base_url            = '/apidocs'
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/api/**/*.rb"
  config.authenticate            = Proc.new do
    authenticate_user! unless signed_in?
  end
  config.translate               = false
  config.default_locale          = nil
  config.app_info['2.0']         = <<-EOS
    Documentation for interacting with the Accelerated View API.
  EOS
end
