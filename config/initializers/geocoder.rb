require 'geocoder'
require 'addressable/uri'

proxy_setting =
  if Settings.proxies
    Settings.proxies.geocoder || Settings.proxies.default
  else
    ''
  end

proxy_uri    = Addressable::URI.parse(proxy_setting)
proxy_scheme = proxy_uri.scheme if proxy_uri.present?

# Geocoder only wants the authority section (user information, host, port)
http_proxy   = proxy_scheme == 'http'  ? proxy_uri.authority : nil
https_proxy  = proxy_scheme == 'https' ? proxy_uri.authority : nil

Geocoder.configure(
  lookup: :google,
  api_key: Settings.api_keys.google,
  http_proxy: http_proxy,
  https_proxy: https_proxy,
  timeout: 15,
  units: :km,
  always_raise: :all
)
