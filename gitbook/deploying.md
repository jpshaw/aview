## Deploying Devices

### Individual deployment

To deploy an individual device, go to the device details page.  This can be
done by searching for the device's MAC address in the search bar, then clicking
on the MAC address of the correct device in the results.  Once on the details
page, click on the `Actions` drop-down on the right side of the page and select
`Deploy`.

![Deploy Individual
Device](https://aview-docs.accns.com/screenshots/deploy_individual_device.png)

Immediately after a device is deployed, Accelerated View will automatically
send a remote command to the device to get its current status.  This ensures
that the status of the device is correctly reflected in Accelerated View.  For
details on device statuses, see the [Monitoring Devices](https://aview-docs.accns.com/statuses.html)
section of this guide.
