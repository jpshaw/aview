# Monitoring Devices

Device monitoring in Accelerated View is controlled by the state of the device.
 The two states a device can be in are deployed and undeployed.  Deployed
indicates that the device should be monitored based on the events that are sent
in by the device, and will be listed with a particular status (see below).  A
device that is deployed will also generate notification alerts based on the
events received and Accelerated View's access to that device.

Undeployed indicates that the device ***should not*** be monitored.  Undeployed
devices will be listed with a `||` pause icon as their status and with a dark
gray status color.  Accelerated View will still list details and events for
undeployed devices, but these events will not generate any notification alerts.

_____

Accelerated View marks ***deployed*** devices with a color indicator to provide
a visual way to note what its status is.  Below is a list of color statuses and
their meaning.

  * **Green:** The device is online and is reachable through the Accelerated
    View system.  For details on what triggers a device to go up, refer to the
    [Notification Profiles](https://aview-docs.accns.com/notifications/profiles.html)
    section of this guide.
  * **Red:** The device is down. This status is also referred to as the `Alert`
    status.  For details on what triggers a device to go down, refer to the
    [Notification Profiles](https://aview-docs.accns.com/notifications/profiles.html)
    section of this guide.
  * **Blue:** The device is running on a backup WAN connection.  This indicates
    that the device's current WAN connection is one of its backup connections
    (i.e. not WAN1).
      * **Note:** The blue status only applies to Gateway devices.
  * **Orange:** The device is in dual-WAN modem, but only its WAN1 connection
    is online.  The secondary WAN interface is down.
      * **Note:** The orange status only applies to Gateway devices.
  * **Gray:** The device is inactive, meaning that it has never checked in to
    Accelerated View.  This is the state of the device if it is deployed, but no
    event has ever been received by Accelerated View.
