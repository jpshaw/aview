## Changing your password

The `Change Password` section of the user preferences allows a user to update
their password, which is used to login to the Accelerated View site.  Note that
the user must enter their current password and a new password.  All passwords
must be at least 6 characters in length.

The password can be reset by clicking on the `Forgot your password` button of
the Login Page.  See additional information in the
[Login Page] (https://aview-docs.accns.com/login.html) section.
