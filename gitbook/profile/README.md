# Managing User Preferences

This section is where a user can modify account information and subscribe to
various device notifications. To manage the account, click on the `Settings`
drop-down at the top of left navigation panel, then select the `Account` link.
