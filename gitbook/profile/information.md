## User Details and settings

This will display the information panel shown below.

![User Profile](https://aview-docs.accns.com/screenshots/user_profile_information.png)

The panel allows a user to modify the name associated with the account, along
with the e-mail address for the account. There is also a selection area for
their time zone so that all data is displayed in local time.

**NOTE:** Any subscriptions in Accelerated View will send notification alerts
to this email address.

This page also lets a user select whether to enable Notifications for the
account and whether geographic Map displays should be made available.

Once all the desired information has been entered or changed, click on Save
Changes to save the information.
