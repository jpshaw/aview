## Adjusting when a Legacy Cellular Device is considered Down by Accelerated View

Configuring when a Legacy Cellular device is considered down (red status) in
Accelerated View is done by adjusting the *Status Frequency* and/or *Grace Period*
options in a cellular device's configuration profile.

### Status Frequency

The *Status Frequency* option controls how often a device checks in to Accelerated View.
By default, cellular devices will send a status check-in to Accelerated View once every
30 minutes.

![Legacy Cellular Status Frequency](https://aview-docs.accns.com/screenshots/cellular_status_frequency.png)

### Grace Period

The *Grace Period* option controls how long in addition to the check-in time
Accelerated View should wait before considering a device as Down.  The default grace
period for cellular devices is 109 minutes.  When this is combined with the
default status frequency of 30 minutes, this means that by default Accelerated View will
consider a cellular device as down if it has not heard from the device within
139 minutes, or 2 hours and 19 minutes.

![Legacy Cellular Grace Period](https://aview-docs.accns.com/screenshots/cellular_grace_period.png)

### Examples

If a user would like to show a cellular device as down after 1 week of inactivity,
they can do so by adjusting the *Status Frequency* and *Grace Period* accordingly.
Below are several example settings that can be used to achieve this.

**Option 1:**
* Status Frequency: 1d
* Grace Period: 6d

**Option 2:**
* Status Frequency: 1h
* Grace Period: 6d23h

**Option 3:**
* Status Frequency: 12h
* Grace Period: 12h6d
