## Adjusting when a uCPE Device is considered Down by Accelerated View

Configuring when a uCPE device is considered down (red status) in
Accelerated View is done by adjusting the *Status Frequency* and/or *Grace Period*
options in a uCPE device's configuration profile.

![uCPE Timeout Options](https://aview-docs.accns.com/screenshots/ucpe_timeout_options.png)

### Status Frequency

The *Status Frequency* option controls how often a device checks in to Accelerated
View.  By default, uCPE devices will send a status check-in to Accelerated View once every
30 minutes.

### Grace Period

The *Grace Period* option controls how long in addition to the check-in time
Accelerated View should wait before considering a device as Down.  The default grace
period for uCPE devices is 109 minutes.  When this is combined with the
default status frequency of 30 minutes, this means that by default Accelerated View will
consider a uCPE device as down if it has not heard from the device within
139 minutes, or 2 hours and 19 minutes.

### Examples

If a user would like to show a uCPE device as down after 1 day of inactivity,
they can do so by adjusting the *Status Frequency* and *Grace Period* accordingly.
Below are several example settings that can be used to achieve this.

**Option 1:**
* Status Frequency: 10m
* Grace Period: 23h50m

**Option 2:**
* Status Frequency: 1h
* Grace Period: 23h

**Option 3:**
* Status Frequency: 6h
* Grace Period: 18h
