# Adjusting when a Device is considered Down by Accelerated View

The following details how to configure when a device is considered down (red status)
in Accelerated View.

When a cellular device goes into the down status, a user can elect to receive
a notification of the event.  For details on configuring a user's notification,
please refer to the
[Notification Profiles](https://aview-docs.accns.com/notifications/profiles.html)
section of this guide.

***Only the following device categories have adjustable options.  Any
other devices cannot be changed in this aspect.***

* Cellular
* Dial-to-IP
* Remote Manager
* Legacy Cellular
