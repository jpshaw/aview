# 1.3.10

## Release date:  2015-01-09

### Features

* Add new configuration options to coincide with 6200-FX firmware version 2.361.91
  released on 2014-01-06

* Add validation and help popover details for configuration settings of Remote Manager
  and Dial-to-IP devices.


### Improvements

* Device firmware versions are sorted in order from newest firmware to oldest.

* Add support for tracking events sent by Dial-to-IP devices when a user logs in
  or out of the box.

* Updated text in notification sent when a device comes back online from being down.


### Bug Fixes

* None


### Deprecations

* Removed management of device configuration schemas used by Remote Manager and
  Dial-to-IP devices.  The schemas are now handled by the external Firmware 
  Management Service application.
