# 3.6.12

## Release date:  2017-12-21

* Support for the 6310-DX model
* Fixed bug validating new contacts for a device
* Allow location details for a Gateway device to be cleared from Service Manager
