# Product Release Notes

The following sections contain release notes for the Accelerated View tool. 
Versioning is based on the [semver](http://semver.org/) specification.  The
format is MAJOR.MINOR.PATCH.
