# 1.4.0

## Release date:  2015-02-16

### Features

* Ability to configure when a device is considered down (red status) in Accelerated View.
  Done by changing the *Status Frequency* and/or *Grace Period* options in a
  device's configuration.  The first option controls how often a device checks
  in to Accelerated View, the second controls how long in addition to the check-in time
  Accelerated View should wait before considering a device as down.

* Add dependencies, default settings, and detailed help information for 5400-RM
  and 5301-DC configuration profiles.

* Enhanced remote commands for 8200/8300 VPN Gateway device.  In addition to
  profile queries, rebooting, and checking the device's status, the new remote
  command interface for the VPN Gateway can now perform the following:

    * arping, firmware upgrade, ping test, traceroute, tracepath, and tunnel
      control (bringing tunnel up/down, bouncing a tunnel, or checking a
      tunnel's status).

    * **VPN Gateway must be running on firmware version _6.1.8_ or higher to
      support the new remote commands.**


### Improvements

* Display the cellular signal strength dBm along with its percentage on the
  device details page.

* Improve index page for 5400-RM devices.  List the phone number and ICCID for
  the RM's cellular modem.

* Support for full high-availability (HA) setup.


### Bug Fixes

* Cellular details were not stored/displayed properly for certain embedded modems
  used by the 5400-RM.

* The *ippassthrough* configuration option for 6200-FX devices was not saved
  properly.

* In 6200-FX configurations, blank, unlocked values included in parent configs
  were ignored.

* The list of SMS commands on the device's details page were chopped off the
  edge of the page, making it hard for a user to select one of the options.


### Deprecations

* _None_
