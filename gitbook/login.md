# Login Page

To login, visit the Accelerated View site by going to https://aview.accns.com. 
Enter in the email that is associated with your login and your password.  Login
credentials are assigned and managed by your account administrator.  If you do
not have a login setup, please contact your administrator.

![Login](https://aview-docs.accns.com/screenshots/login.png)

The footer, which appears throughout the Accelerated View site, includes a
copyright along with the version of Accelerated View that is currently running.
A link to the Accelerated View Terms of Service is also provided.

## Password Reset

If you have a login setup in Accelerated View, but do not know the password,
you can reset your password by clicking on the `Forgot your password` link.
This will take you to a page where you will confirm your email address.  Once
you click `Send reset instructions`, Accelerated View will send an email to you
with instructions on how to reset your password.

**Note:** The user must reset their password within 72 hours (3 days) after
the email has been sent.  After this time, the user will have to re-do the
instructions listed above to perform a password reset.
