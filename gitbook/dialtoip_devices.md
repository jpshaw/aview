# Dial-to-IP Devices

Dial-to-IP devices refer to the Accelerated Dial-to-IP Converter line of
products.  Supported models are listed below.  A user can find a list of all
the Dial-to-IP devices they have access to by clicking on the `Devices` tab in
the navigation bar, and then select the `Dial-to-IP` link from the drop-down.

  * 5300-DC
  * [5301-DC](https://accelerated.com/products/5301_dc_dial_to_ip_converter/)

![Dial-to-IP Devices](https://aview-docs.accns.com/screenshots/dialtoip_index.png)

## Dial-to-IP Device Details

![Dial-to-IP Device](https://aview-docs.accns.com/screenshots/dialtoip_device.png)

The majority of details provided on the Dial-to-IP device details page are
applicable to all categories of devices.  For details on those table entries,
refer to the [Devices](https://aview-docs.accns.com/site/devices.html) section
of this guide.  The device details specific to Dial-to-IP devices are listed below.

  * **Management IP:** The IP address of the Dial-to-IP device's aView IPSec tunnel.
    If this IP does not match the aView tunnel IP address listed in the local
    web UI of the Dial-to-IP device, a user with the ability to modify devices
    will be given an option to edit the Management IP address.
  * **Uplink IP Address:** The IP address of the current WAN network connection
    of the Dial-to-IP device.
  * **Primary Interface:** The currently used WAN connection interface.

### Configuration Tab

Dial-to-IP devices are configured through the Accelerated View site.  For
details, see the [Dial-to-IP
Configuration](/configurations/dialtoip_configuration.md) section of this guide.

### Associated Devices Tab

This tab lists any devices that the Dial-to-IP device has associated with, indicating
that they are connected to each other.  The tab will include the following
details about the associated device.

  * **Device ID:** The identifying MAC address for the associated device.
  * **Model:** If the associated device is a type of device supported by ARMT,
    this entry will display the type of device ARMT knows it as. If the device
    is not supported by ARMT, the category will be listed with a dash ( - ).
  * **Interface:** The physical interface of the Dial-to-IP device the associated
     device is connected to.
  * **Type:** How the associated device is connected to the Dial-to-IP device.  For
  	example, a type listed as `WAN` indicates that the associated device is
  	connected to one of the Dial-to-IP device's WAN Ethernet ports.
  * **IP Address:** The IP address of the associated device.
  * **Connected:** Indicates when the device association was made and how long
    the associated device has been connected to the Dial-to-IP device.

### Tunnels Tab

The Tunnel tab lists status for any IPSec tunnels established by the Dial-to-IP
device.  Each row of the tab indicates a unique IPSec tunnel of the device.

  * **Name:** The name of the IPSec tunnel, as specified in the configuration
    of the Dial-to-IP device.
  * **Status:** Indicates whether or not the Dial-to-IP device has established
    the IPSec tunnel.
  * **Last Updated:** The last time Accelerated View received an update concerning this tunnel.
  * **IpLocal:** The tunnel IP address assigned to the Dial-to-IP device.
  * **IpRemote:** The IP address of the server that the Dial-to-IP device built a tunnel to.
  * **NetLocal:** The network settings on the Dial-to-IP device for the tunnel.
  * **NetRemote:** The network settings on the server for the tunnel.

### Network Tab

The Network tab lists details for network interfaces that belong to the
Dial-to-IP device.  If a Dial-to-IP device has multiple networks, there will be
multiple network tabs, each one corresponding to one of the device's networks.

  * **Connection Type:** the type of network connection for the interface.
  * **Priority:** Indicates if the network connection is the current primary or
    alternate connection.
  * **IP Ranges:** The IP addressing for the network.
  * **MTU:** The MTU size of the network interface.
  * **Gateway IP:** The default route (gateway IP) of the network interface.
  * **Bytes Received:** The total amount of data received, measured in bytes,
    on the network interface since the Dial-to-IP device has been online (resets
    on reboot).
  * **Bytes Transmitted:** The total amount of data transmitted, measured in
    bytes, on the network interface since the Dial-to-IP device has been online
    (resets on reboot).
  * **Ethernet Speed:** If the network type is an Ethernet interface, then this
    indicates the speed of the Ethernet port.
  * **Ethernet Status:** If the network type is an Ethernet interface, then
    this indicates the status of the physical connection on the Ethernet port.

### Remote Commands

Remote commands are sent to a Dial-to-IP device through an IPSec tunnel that the
Dial-to-IP device establishes to the Accelerated remote control server, or through
the upstream IP address of the device if no tunnel IP is available. All Dial-to-IP
devices by default establish an IPSec tunnel to the Accelerated remote.accns.com
server.  Additional IPSec tunnel endpoints are detailed here:

https://kb.accelerated.com/m/67493/l/949015-aview-tunnel-alternatives

The tunnel IP address for the Dial-to-IP device is stored in Accelerated View as
the `Management IP` on the Device Details tab.  If the Management IP is not known,
then Accelerated View will use the IP address listed in the `Uplink IP Address`
field on the Device Details tab for sending remote commands.

When a user selects to send a remote command to a device, Accelerated View
utilizes a DNS service running on the Accelerated remote control server
to verify what tunnel IP address is associated to the device.  If the DNS service
responds with a valid IP address, Accelerated View will send the remote command
to that IP address, otherwise it will use the known `Management IP` or the
`Uplink IP Address` if the Management IP is unknown.

Before sending the remote command to the Dial-to-IP device, Accelerated View
will first perform a HTTP request to the device to validate that the correct
device is at the given IP address.

The available remote commands that can be sent to Dial-to-IP devices are listed below.

  * **Check status:** The Dial-to-IP device will immediately send its current
    status.
  * **Check signal strength:** The Dial-to-IP device will send its signal strength
    and network info once every 10 seconds for the next 15 minutes.
  * **Perform Speed Test:** The Dial-to-IP device will perform a speed test and
    send the Upload/Download results.
  * **ARPing Attached Device:** The Dial-to-IP device will attempt to ARPing the
    client device attached to its Ethernet port.
  * **Send Wake-on-LAN to Attached Device:** The Dial-to-IP device will send a
    wake-on-LAN packet to the client device attached to its Ethernet port.
  * **Check Configuration:** The Dial-to-IP device will immediately pull its
    configuration and reboot to apply any new settings.
  * **Reboot:** Reboot the Dial-to-IP device.

### Emergency SMS Commands

As an alternative to the Remote commands described above, users can also send
commands via SMS messaging to a Dial-to-IP device. The SIM card used by the
Dial-to-IP device must have SMS enabled, or the user will not be able to send
SMS commands.  The available SMS commands that can be sent are listed below.

  * *Set Configuration:* A pop-up window appears where a user can enter in a
    configuration option or set of options to apply to the Dial-to-IP device. 
    Multiple config options must be separated by a comma.  For example, if a
    user wants to set the APN of a Dial-to-IP device to managedvpn, they would
    enter "modem_apn=managedvpn".
  * *Configuration Reset:* Restore default configuration on the Dial-to-IP
    device and reboot.
  * *Firmware Reset:* Restore to the backup firmware image on the Dial-to-IP
    device, and reboot.
  * *Factory Reset:* Restore default configuration on the Dial-to-IP device,
    restore to the backup firmware image, and reboot.
  * *Remote Control:* Tell the Dial-to-IP device to bring up its remote
    control tunnel (useful if on low data plan, so the remote control tunnel
    is not always up using data).
  * *Reboot:* Reboot the Dial-to-IP device.


## Dial-to-IP configuration requirements for Accelerated View

In order for the Accelerated View site to monitor a Dial-to-IP device, it must
send its syslogs to the Accelerated View syslog server IP address, the domain
of which is `logs.accns.com`.

In order for a Dial-to-IP device to get its configuration from the Accelerated
View site, it must be set to pull its configuration from the Accelerated View
configuration server `configuration.accns.com`.  The device must also be able to
sync its date and time with an NTP server; the default is `time.accns.com`.
