# VIG Reports

The VIG Report creates an ad hoc report of a list of VPN Gateway units and what
VIGs they are associated with (if any). VIG reports are based on the events that
are sent in by the VPN Gateway devices into ARMT.

## Creating an VIG report

To create a new VIG report, click on the `VIG Report` link in the `Reports`
drop-down menu of the navigation bar.  From the report form, select the desired
criteria for the report. The form items are described below.

![VIG Report Form](https://aview-docs.accns.com/aview/report_form_vig.png)

  * **Organization:** The organization that the devices are associated with. 
    The user can also specify whether or not to include devices that belong to
    sub-organizations of the selected organization by clicking the `hierarchy`
    icon button next to the `Organization` drop-down.
  * **Deployment Status:** Select to run the report on all devices,
    deployed-only, or undeployed-only.  For a description on deployed versus
    undeployed, refer to the [Monitoring Devices]
    (https://aview-docs.accns.com/statuses.html) section of this guide.
  * **Order by:** Select the parameter that is used to order and list the
    results of the report.

  Once all the desired options are specified, click `Create` to generate the
VIG report.  Note that depending on the number of devices
and the time span of the report, it may take several minutes for the report to
complete.  The report generation process is done in the background.  The user
can navigate away from this page without interrupting or stopping the process. 
The user can check on the report process by going to the report `History` page
(see below for details), and then clicking on the appropriate report ID.

![VIG Report](https://aview-docs.accns.com/aview/vig_report.png)

  The resulting VIG report can be exported to a CSV file
by clicking the `CSV` button in the report.
