# Availability Reports

The Availability Report creates an ad hoc report on device availability,
including detailed uptime data and graphs.  Availability reports are based on
the events that are sent in by the devices into Accelerated View.  The downtime
percentage is calculated by adding up the time when the device was reported as
down (red status) or unreachable (gray status) and subtracting the result
from the total time.  The uptime percentage is calculated by subtracting the
downtime percentage from 100%.

## Creating an availability report

To create a new availability report, click on the `Availability` link in the
`Reports` drop-down menu of the navigation bar.  From the report form, the user
selects the desired criteria for the report. The form items are described below.

![Availability Report Form](https://aview-docs.accns.com/screenshots/availability_report_form.png)

  * **Organization:** The organization that the devices are associated with. 
    The user can also specify whether or not to include devices that belong to
    sub-organizations of the selected organization by clicking the `hierarchy`
    icon button next to the `Organization` drop-down.
  * **Device Categories:** The categories of devices to run the report on.
    Multiple device categories and/or models can be selected together.
  * **Device Models:** The models of devices to run the report on.
    Multiple device categories and/or models can be selected together.
  * **Deployment:** Select to run the report on all devices,
    deployed-only, or undeployed-only.  For a description on deployed versus
    undeployed, refer to the [Monitoring Devices]
    (https://aview-docs.accns.com/statuses.html) section of this guide.
  * **Order by:** Select the parameter that is used to order and list the
    results of the report.
  * **Time span:** The time frame for what events are used to generate the
    report.

  Once all the desired options are specified, click `Create` to generate the
availability report.  Note that depending on the number of devices and the time
span of the report, it may take several minutes for the report to complete. 
The report generation process is done in the background.  The user can navigate
away from this page without interrupting or stopping the process.  The user can
check on the report process by going to the report `History` page (see below
for details), and then clicking on the appropriate report ID.

![Availability Report](https://aview-docs.accns.com/screenshots/availability_report.png)

  The resulting availability report can be exported to a CSV file by clicking
the `CSV` button in the report.
