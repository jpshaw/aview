# Cellular Network Type Reports

The Cellular Network Type Report creates an ad hoc report on cellular network
type and the time duration devices spent on a given network.  Cellular network
type reports are based on the events that are sent in by the devices into
Accelerated View.

## Creating an Cellular Network Type report

To create a new cellular network type report, click on the `Cellular Network
Type` link in the `Reports` drop-down menu of the navigation bar.  From the
report form, the user selects the desired criteria for the report. The form
items are described below.

![Cellular Network Type Report Form](https://aview-docs.accns.com/screenshots/report_form_cellular_network.png)

  * **Organization:** The organization that the devices are associated with. 
    The user can also specify whether or not to include devices that belong to
    sub-organizations of the selected organization by clicking the `hierarchy`
    icon button next to the `Organization` drop-down.
  * **Device Categories:** The categories of devices to run the report on.
    Multiple device categories and/or models can be selected together.
  * **Device Models:** The models of devices to run the report on.
    Multiple device categories and/or models can be selected together.
  * **Deployment:** Select to run the report on all devices,
    deployed-only, or undeployed-only.  For a description on deployed versus
    undeployed, refer to the [Monitoring Devices]
    (https://aview-docs.accns.com/statuses.html) section of this guide.
  * **Order by:** Select the parameter that is used to order and list the
    results of the report.
  * **Time span:** The time frame for what events are used to generate the
    report.

  Once all the desired options are specified, click `Create` to generate the
cellular network type report.  Note that depending on the number of devices and
the time span of the report, it may take several minutes for the report to
complete.  The report generation process is done in the background.  The user
can navigate away from this page without interrupting or stopping the process. 
The user can check on the report process by going to the report `History` page
(see below for details), and then clicking on the appropriate report ID.

![Cellular Network Type Report](https://aview-docs.accns.com/screenshots/cellular_network_report.png)

  The resulting cellular network type report can be exported to a CSV file by
clicking the `CSV` button in the report.
