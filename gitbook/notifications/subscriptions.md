## Notification Subscriptions

To subscribe to an individual device, go to the device details page.  This can
be done by searching for the device's MAC address in the search bar, then
clicking on the MAC address of the correct device in the results.  Once on the
details page, click on the `Subscribe` button listed in the `Actions`
drop-down at the top right of the page.

![Subscribe button](https://aview-docs.accns.com/screenshots/subscribe_button.png)

To subscribe to an organization, click on the `Organizations` tab in the
navigation panel.  From there, click the `Actions` drop-down for the appropriate
organization, and then click `Subscribe`.

A user can manage their notification subscriptions by clicking the `Account`
button at the top right of any page on the Accelerated View site.  From there,
click the `Notification Subscriptions` tab.  This lists all the organizations
 that the user is subscribed to.  To view the devices the user is subscribed to,
select the `Devices` tab inside the notification subscriptions table.

![Subscriptions tab](https://aview-docs.accns.com/screenshots/notification_subscriptions.png)

To delete a subscription (aka "unsubscribe"), click the `Delete` trash icon for
the appropriate subscription. Users can also unsubscribe to a device or
organization in the same manner that they subscribed to it.
