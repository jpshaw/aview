## Notification Profiles

This tab in the user preferences displays the existing profiles for the user. A
user can create a new profile by clicking on the `Create` plus icon at the top-
right of the list. This will bring up the new notification profile modal.
The modal gives you the ability to name the profile and specify the type of
alerts that should generate a notification.  Note that multiple device types
can be subscribed to the same notification profile and alert on various
conditions.

![Notification Profile](https://aview-docs.accns.com/screenshots/notification_profile.png)

For each alert type you can define when a notification should be issued. The
notification can be disabled or sent at various time intervals up to 24 hours.
Interval notifications are referred to as "Bulk" notifications in the email
that is sent by the notification system.

![Sample Bulk Notification](https://aview-docs.accns.com/screenshots/bulk_notification_email.png)

![Sample Individual Notification](https://aview-docs.accns.com/screenshots/individual_notification_email.png)

Below are details for each notification and what it is triggered by.

### Gateway notifications
  * **Device up:** This notification will trigger the device to show as
    up (green status) in Accelerated View. This notification is generated whenever
    the gateway device is considered unreachable (gray status) or down (red
    status) and the device checks back in to Accelerated View by either of the
    following:
    * The gateway device sends an SNMP inform to Accelerated View.
    * Accelerated View is able to probe the gateway device through its
      management tunnel.
  * **Device down:** The VPN Gateway device is probed once every 15 minutes.
    If Accelerated View misses two consecutive probes and also has not received
    any SNMP informs within the past 30 minutes, the device will be marked as
    down (red status) and this notification will be sent.  The time specifying
    when a gateway device is considered down cannot be adjusted.
  * **Device firmware changed:** The gateway device is running on a different
    firmware version from the last time the device was probed.
  * **Device primary IP changed:** The gateway device's current WAN IP address
    has changed. To enable this SNMP inform, enable the `Active WAN IP Change`
    option in the Service Manager profile for the VPN Gateway.
  * **Device rebooted:** The gateway device sent an SNMP inform that it was
    rebooted. To enable this SNMP inform, enable the `Device Reboot` option in
    the Service Manager profile for the VPN Gateway.
  * **Backup WAN autotest failed** - the gateway device sent an SNMP inform
    that its scheduled autotest has failed on either the WAN2 or Cellular
    interface. To enable this SNMP inform, enable the `W2 Autotest Failure` and
    `Dial Autotest Failure` options in the Service Manager profile for the VPN
    Gateway.
  * **Backup WAN threshold reached** - the gateway device sent an SNMP inform
    that it has been connected with either the WAN2 or Cellular interface longer
    than the threshold time.  The gateway device will continue to use the backup
    connection if this inform is sent; the inform is meant to be a warning that
    the gateway device has been in backup mode for an extended period of time. To
    enable this SNMP inform, enable the `Dial Backup Threshold` option in the
    Service Manager profile for the VPN Gateway.
  * **Device WAN interface changed:** The gateway device sent an SNMP inform
    that it has switched to a new WAN interface. To enable this SNMP inform,
    enable the `Active WAN IP Change` option in the Service Manager profile for
    the VPN Gateway.
  * **Device cellular location changed:** The cellular location details reported
    by the Gateway device changed.  For more details about determining the
    cellular location of a Gateway device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section.
  * **Device signal strength fell below a nominal threshold:** The cellular signal
    strength reported by the VPN Gateway device has dropped below a specified
    threshold percentage. This threshold is defined by the Accelerated View admin
    in the Admin panel of the site.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.
  * **Ping test succeeded:**  The VPN Gateway device sent an SNMP inform that
    it successfully pinged an IP address specified in its SMx profile.
    The VPN Gateway must be running version 6.2 or higher to have ping tests enabled.
  * **Ping test failed:**  The VPN Gateway device sent an SNMP inform that
    it failed to ping an IP address specified in its SMx profile.
    The VPN Gateway must be running version 6.2 or higher to have ping tests enabled.
  * **Device network type changed:** The SNMP probe response from the VPN Gateway
    device indicated that the cellular network (4G LTE, 3G HSPA+, etc.) type
    that it is connected on has changed.
  * **VRRP state changed:** The VPN Gateway device sent an SNMP inform that
    its VRRP state changed.  Note that VRRP must be enabled in the VPN Gateway's
    Service Manager profile to receive this inform.

### Legacy Cellular notifications
  * **Device up:** This notification is generated whenever the Legacy Cellular
    device is considered down (red status) and the device checks back in to the
    Accelerated View monitor by sending a syslog or if the device is reachable
    through remote control commands.  This notification will trigger the device
    to show as up (green status) in Accelerated View.
  * **Device down:** The Legacy Cellular device sends a heartbeat syslog once every 30
    minutes.  If Accelerated View has not received any other syslogs within the
    past 139 minutes (2 hours 19 minutes), the device will be marked as down
    (red status) and a notification will be sent.  The time specifying when a
    Legacy Cellular device is considered down can be adjusted by changing the
    configuration of the device.  See the
    [Legacy Cellular Down Status](https://aview-docs.accns.com/monitoring_device_status/legacy_cellular_down_status.html)
    section of this guide.
  * **Device signal strength fell below a nominal threshold:** The cellular signal
    strength reported by the Legacy Cellular device has dropped below a specified
    threshold percentage. This threshold is defined by the Accelerated View admin
    in the Admin panel of the site.
  * **Device firmware changed:** The Legacy Cellular device is running on a different
    firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The Legacy Cellular device sent a syslog stating
    that the IP address it received from the Legacy Cellular network has changed.
  * **Device network type changed:** The Legacy Cellular device sent a syslog stating
    that the Legacy Cellular network (4G LTE, 3G HSPA+, etc.) type that it is connected
    on has changed.
  * **Device rebooted:** The Legacy Cellular device sent a syslog that it rebooted.
    The notification and log will include the reason for the reboot.
  * **Device cellular location changed:** The cellular location details reported
    by the Legacy Cellular device changed.  For more details about determining the
    cellular location of a Legacy Cellular device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.

### WiFi notifications
  * **Device up:** This notification is generated whenever the WiFi
    device is considered down (red status) and the device checks back in to the
    Accelerated View monitor by sending a syslog.  This notification will trigger
    the device to show as up (green status) in Accelerated View.
  * **Device down:** The WiFi device sends a heartbeat syslog once every 15
    minutes.  If Accelerated View misses two consecutive heartbeats and has not
    received any other syslogs within the past 30 minutes, the device will be
    marked as down (red status) and this notification will be sent.  The time
    specifying when a WiFi device is considered down cannot be adjusted.
  * **Device firmware changed:** The WiFi device is running on a different
    firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The WiFi device sent a syslog stating that
    its WAN IP address has changed.
  * **Device detected rogue Access Point:** The WiFi device sent a syslog
    stating that it found a rogue Access Point on its network.  The notification
    will include the SSID, MAC address, and IP address of the rogue Access Point.
  * **Device rebooted:** The WiFi device sent a syslog that it rebooted.  The
    notification and log will include the reason for the reboot.

### Dial-to-IP notifications
  * **Device up:** This notification is generated whenever the Dial-to-IP
    device is considered down (red status) and the device checks back in to the
    Accelerated View monitor by sending a syslog or if the device is reachable
    through remote control commands.  This notification will trigger the device
    to show as up (green status) in Accelerated View.
  * **Device down:** The Dial-to-IP device sends a heartbeat syslog once every 30
    minutes.  If Accelerated View has not received any syslogs within the
    past 139 minutes (2 hours 19 minutes), the device will be marked as down
    (red status) and a notification will be sent.  The time specifying when a
    Dial-to-IP device is considered down can be adjusted by changing the
    configuration of the device.  See the
    [Dial-to-IP Down Status](https://aview-docs.accns.com/monitoring_device_status/dialtoip_down_status.html)
    section of this guide.
  * **Device rebooted:** The Dial-to-IP device sent a syslog that it rebooted.
    The notification and log will include the reason for the reboot.
  * **Device firmware changed:** The Dial-to-IP device is running on a different
    firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The Dial-to-IP device sent a syslog stating
    that the IP address it received from the cellular network has changed.
  * **Device network type changed:** The Dial-to-IP device sent a syslog stating
    that the cellular network (4G LTE, 3G HSPA+, etc.) type that it is connected
    on has changed.
  * **Device WAN interface changed:** The Dial-to-IP device sent a syslog stating
    that it has switched to a new WAN interface.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.  Note this alert only applies to the 5300-DC model.
  * **Device cellular location changed:** The cellular location details reported
    by the Dial-to-IP device changed.  For more details about determining the
    cellular location of a Dial-to-IP device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section. Note this
    alert only applies to the 5300-DC model.

### RemoteManager notifications
  * **Device up:** This notification is generated whenever the
    RemoteManager device is considered down (red status) and the device checks
    back in to the Accelerated View monitor by sending a syslog or if the device
    is reachable through remote control commands.  This notification will trigger
    the device to show as up (green status) in Accelerated View.
  * **Device down:** The RemoteManager device sends a heartbeat syslog once
    every 30 minutes.  If Accelerated View has not received any syslogs within the
    past 139 minutes (2 hours 19 minutes), the device will be marked as down
    (red status) and a notification will be sent.  The time specifying when a
    RemoteManager device is considered down can be adjusted by changing the
    configuration of the device.  See the
    [Remote Manager Down Status](https://aview-docs.accns.com/monitoring_device_status/remote_manager_down_status.html)
    section of this guide.
  * **Device rebooted:** The RemoteManager device sent a syslog that it rebooted.
    The notification and log will include the reason for the reboot.
  * **Device firmware changed:** The RemoteManager device is running on a
    different firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The RemoteManager device sent a syslog
    stating that the IP address it received from the cellular network has changed.
  * **Device network type changed:** The RemoteManager device sent a syslog
    stating that the cellular network (4G LTE, 3G HSPA+, etc.) type that it is
    connected on has changed.
  * **Device WAN interface changed:** The RemoteManager device sent a syslog
    stating that it has switched to a new WAN interface.
  * **Device port down:** The Remote Manager sent a syslog stating that either
    one of its serial ports has been disconnected.  To receive this notification,
    the CTS or DCD pin must be selected as monitorable for each desired serial port
    in the configuration profile of the Remote Manager device.
  * **Device signal strength fell below a nominal threshold:** The cellular signal
    strength reported by the Remote Manager device has dropped below a specified
    threshold percentage. This threshold is defined by the Accelerated View admin
    in the Admin panel of the site.
  * **Device ethernet port connected:**  The Remote Manager sent a syslog stating
    that a client device was connected to its Ethernet port and it has physical
    Ethernet connectivity to that device.
  * **Device ethernet port disconnected:** The Remote Manager sent a syslog
    stating that it lost physical connectivity to a client device that was
    previously connected via Ethernet to the Remote Manager.
  * **Device cellular location changed:** The cellular location details reported
    by the Remote Manager device changed.  For more details about determining the
    cellular location of a Remote Manager device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.

### Cellular notifications
  * **Device recovered:** This notification is generated whenever the
    Cellular device is considered down (red status) and the device checks
    back in to the Accelerated View monitor by sending a syslog or if the device
    is reachable through remote control commands.  This notification will trigger
    the device to show as up (green status) in Accelerated View.
  * **Device down:** The Cellular device sends a heartbeat syslog once
    every 30 minutes.  If Accelerated View has not received any syslogs within the
    past 139 minutes (2 hours 19 minutes), the device will be marked as down
    (red status) and a notification will be sent.  The time specifying when a
    Cellular device is considered down can be adjusted by changing the
    configuration of the device.  See the
    [Cellular Down Status](https://aview-docs.accns.com/monitoring_device_status/embedded_cellular_down_status.html)
    section of this guide.
  * **Device rebooted:** The Cellular device sent a syslog that it rebooted.
    The notification and log will include the reason for the reboot.
  * **Device firmware changed:** The Cellular device is running on a
    different firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The Cellular device sent a syslog
    stating that the IP address it received from the cellular network has changed.
  * **Device network type changed:** The Cellular device sent a syslog
    stating that the cellular network (4G LTE, 3G HSPA+, etc.) type that it is
    connected on has changed.
  * **Device WAN interface changed:** The Cellular device sent a syslog
    stating that it has switched to a new WAN interface.
  * **Device signal strength fell below a nominal threshold:** The cellular signal
    strength reported by the Cellular device has dropped below a specified
    threshold percentage. This threshold is defined by the Accelerated View admin
    in the Admin panel of the site.
  * **Device ethernet port connected:**  The Cellular sent a syslog stating
    that a client device was connected to its Ethernet port and it has physical
    Ethernet connectivity to that device.
  * **Device ethernet port disconnected:** The Cellular sent a syslog
    stating that it lost physical connectivity to a client device that was
    previously connected via Ethernet to the device.
  * **Device cellular location changed:** The cellular location details reported
    by the Cellular device changed.  For more details about determining the
    cellular location of a Cellular device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.

### uCPE notifications
  * **Device recovered:** This notification is generated whenever the
    uCPE device is considered down (red status) and the device checks
    back in to the Accelerated View monitor by sending a syslog or if the device
    is reachable through remote control commands.  This notification will trigger
    the device to show as up (green status) in Accelerated View.
  * **Device down:** The uCPE device sends a heartbeat syslog once
    every 30 minutes.  If Accelerated View has not received any syslogs within the
    past 139 minutes (2 hours 19 minutes), the device will be marked as down
    (red status) and a notification will be sent.  The time specifying when a
    uCPE device is considered down can be adjusted by changing the
    configuration of the device.  See the
    [uCPE Down Status](https://aview-docs.accns.com/monitoring_device_status/ucpe_down_status.html)
    section of this guide.
  * **Device rebooted:** The uCPE device sent a syslog that it rebooted.
    The notification and log will include the reason for the reboot.
  * **Device firmware changed:** The uCPE device is running on a
    different firmware version from the last firmware version the device logged.
  * **Device primary IP changed:** The uCPE device sent a syslog
    stating that the IP address it received from the cellular network has changed.
  * **Device WAN interface changed:** The uCPE device sent a syslog
    stating that it has switched to a new WAN interface.
  * **Device cellular location changed:** The cellular location details reported
    by the uCPE device changed.  For more details about determining the
    cellular location of a uCPE device, refer to the
    [Devices](https://aview-docs.accns.com/site/devices.html) section.
  * **Device data usage over limit:** The device exceeded the specified Individual
    Data Limit value listed in the Data Plan associated to the device.  For more
    information on Data Plans, refer to the [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section of this guide.
