# Managing Notifications

Notifications provide a convenient way of being alerted when a device or group
of devices changes state. Notifications are subscribed to on an individual
per-device basis or by subscribing to an organization.  When a user subscribes
to a device or organization, a notification profile must be applied to the
subscription.  The notification profile is where a user configures exactly what
type of notifications they would like to receive from the device or
organization.

To view the devices and/or organizations the user is currently subscribed to,
go the user preferences by clicking on the `Account` button at the top of the
Accelerated View site, then click on the `Notification Subscriptions` sub-menu.

To view or create a new notification profile, go to the user preferences by
clicking on the `Settings` drop-down at the top of the left navigation panel,
select the `Account` link, then click on the `Notification Profiles` sub-menu.

**Note:** If a user subscribes to an individual device, and also subscribes to
the organization that the device belongs to, they could possibly receive two of
the same notifications from the device, depending on what notification profile
is applied to each subscription.
