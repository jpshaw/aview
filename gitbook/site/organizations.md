# Organizations

The Organizations page displays all organizations the user has access to. An
organization is a logical entity used to associate devices together.  It does
not necessarily represent a client.

![Organizations](https://aview-docs.accns.com/screenshots/organizations.png)

Clicking on an organization name will direct the user to a page that displays
all the devices associated with that organization and any sub-organization under
it.

Each line also has an Actions drop-down that allows the user to edit the
organization settings, add new users, add new sub-organizations, delete the
organization, and subscribe to receive notifications from any device in the
organization.

**Note:** Deleting an organization will also delete any devices, users, and
sub-organizations associated with that organization.

## Creating a New Organization

To create a new organization, navigate to the Organization page, click on the
drop-down action box on the right hand side of an organization’s row, and
select `New Sub Organization`

![New Organization](https://aview-docs.accns.com/screenshots/new_organization.png)

### Organization Abilities

Part of the purpose of an organization is to control the access users within
that organization have to Accelerated View.  When creating an organization,
there are a list of roles that must be selected to specify the access that
organization will have to its devices.

  * **Modify Sites:** Controls the ability for users within the organization to
    edit the Site IDs associated with that organization.
  * **Modify Devices:** Controls the ability for users within the organization
    to edit the options listed in the `Admin Panel` section on a device's
    details page, as well as deploying/undeploying devices.
  * **Modify Organizations:** Controls the ability for users within the
    organization to edit any sub-organizations that the organization has.
  * **Modify Users:** Controls the ability for users within the organization to
    edit the settings and permissions of other users.
  * **Modify Configurations:** Controls the ability for users within the
    organization to edit the Legacy Cellular configuration profiles of a device.
  * **Replace Devices:** Controls the ability for users within that
    organization to move devices from one organization to another.
  * **Import Devices:** Controls the ability for users to add new devices into
    Accelerated View with the import tool.
  * **Modify Device Configurations:** Controls the ability for users within the
    organization to edit the Cellular, Dial-to-IP and RemoteManager configuration
    profiles of a device.
  * **View Configuration History:** Controls the ability for users within the
    organization to view the history of changes made to a device's configuration
    profile.
  * **View Admin Panel:** Controls access to the Admin panel of the site.  If
    enabled, the Admin panel will be accessible by a link in the right section
    of the top navigation bar (between the Account and Logout buttons).
  * **Deploy Devices:** Controls the ability for users within the organization
    to deploy or undeploy devices.  For more information on what device deployment,
    please refer to the [Monitoring Devices](https://aview-docs.accns.com/statuses.html)
    section of this guide.
  * **Send Device Commands:** Controls the ability for users within the
    organization to send remote commands to devices.
  * **Assign Non-Production Firmwares:** Controls the ability for users within
    the organization to select non-production firmware versions in a device's
    configuration profile.

### Optional Service Name

Users can associate a Service Name to an organization, as a way of indicating what
level of service a support company provides to the organization.

To add a Service Name to an organization, navigate to the `Organization` page,
click on the drop-down action box on the right hand side of the organization's
row, and select the `Edit Settings` option.  Select the appropriate Service Name
from the drop-down list provided.  If the desired Service Name is not available,
please contact your Accelerated View admin to have the Service Name included.

![Service Name Setup](https://aview-docs.accns.com/screenshots/service_name_setup.png)

The Service Name will then be displayed on any device in that organization or
any of its sub-organizations. Also, any notifications that get sent from Accelerated View
for the organization's devices will include the Service Name in the notification
details.

![Service Name Device Example](https://aview-docs.accns.com/screenshots/service_name_device_display.png)

### Optional Footer

The footer text displayed at the bottom of the Accelerated View site can be changed
at an organization level.  Any footer that is set for the organization will be
applied to all sub-organizations.  To set or update the footer text for an
organization, navigate to the `Organization` page, click on the drop-down action
box on the right hand side of the organization's row, and select the `Edit`
option.  Enter in the desired footer text in the `Footer(optional)` field and
click `Update`.  Note that the footer supports HTML syntax.

### Device Settings

The `Device Settings` tab contains all selections to enable or disable optional
details or metrics for devices in that organization or its sub-organizations.
Below is are detail for each option listed in the Device Settings tab.

* **Gateway - Enable Cellular Modem Data Collection:** when enabled, the
  Accelerated View application will collect cellular statistics from all VPN
  Gateway devices that belong to that organization or any of its sub-organization.
  These cellular utilization statistics will be displayed on the device's details
  page as the `Cellular Utilization` chart and the `Modem` tab. Refer to the
  [Gateway Devices](https://aview-docs.accns.com/gateway_devices.html) section
  for more details. The cellular statistics can also be used to generate
  cellular-based reports for VPN Gateway devices.
* **Gateway - Enable VLAN Data Collection :** when enabled, the Accelerated View
  application will collect details of the LAN ports of the VPN Gateway device,
  including the Vlan IDs associated with the LAN ports.

![Device Settings Example](https://aview-docs.accns.com/screenshots/org_device_settings.png)

### Data Plans

The `Data Plans` tab allows authorized users to create and manage cellular data
plans for the selected organization. Once a data plan is created, the user can
apply that data plan to one or more devices by selecting the data plan under the 
`Settings` tab on the details page for the device.

* **Name:**  The name of the data plan.  Clicking on the data plan name will display
  a list of the devices associated to that data plan, including how much data each
  device has used under the data plan. Users can export these details by clicking
  the `Export` button on the index page for that Data Plan
* **Number of devices:** The count of devices assigned to this data plan.
* **Data Limit:** The amount of data usage at which point a notification should be
  generated for the data plan.  Note that for this notification to be generated, the
  `Overage Notification` option must be selected.
* **Data Used:** The amount of data used by the devices that belong to the data plan.
* **Individual Data Limit:** The amount of data usage for each device under the
  data plan at which point a notification should be generated.  To receive this
  notification, a user must subscribe to the device and select the
  `Device data usage over limit` option in the notification profile. For additional
  details on setting up a notification profile, refer to the
   [Notification Profiles](https://aview-docs.accns.com/notifications/profiles.html)
   section.
* **Start Day:** The day of the month to reset the `Data Used` value of the data plan.
* **Overage Notification:**   Selecting this option will send a notification when
  the `Data Used` value of the data plan exceeds the `Data Limit` option of the
  data plan.  When triggered, this notification will be sent to all users that
  belong to the same organization that the data plan belongs to. The notification
  will include a breakdown of the data plan usage per device within the data plan.
* **Created:** The time when the data plan was created.

![Data Plan Example](https://aview-docs.accns.com/screenshots/org_data_plan.png)
