# Configurations

Displays the device configuration profiles available to the user.

**Notice:** Configurations can only be managed by certified users.  Please
ensure that the appropriate user has the correct permissions setup.  Refer to
the [Managing User Permissions](https://aview-docs.accns.com/users/permissions.html)
section of this guide for details on how to setup the correct permissions.

![Configurations](https://aview-docs.accns.com/screenshots/configurations.png)

Configuration profiles can be viewed by clicking on the appropriate
configuration type from the navigation bar and then click on the desired
configuration name.  This will direct a user to a page that displays the
configuration settings for the specified profile.  For details of each
configuration option, refer to the individual device configuration sections of
this guide.  Note that required fields are marked with asterisks (*) and any
values inherited from the parent configuration are listed in red.  Users can
edit the configuration profile by clicking the `Edit` pencil icon in the top
right of the profile.  You also have the option of copying the existing
configuration by clicking the `Copy` icon in the top right corner of the profile.

![Individual
Configuration](https://aview-docs.accns.com/screenshots/individual_configuration.png)

All devices that the configuration profile is linked to are listed at the
bottom of the profile.  These are referred to as "linked devices".
