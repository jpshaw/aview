# Users

The Users page displays all user accounts under the current user can manage.
Authorized users can modify any existing user or create a new one.

![Users](https://aview-docs.accns.com/screenshots/users.png)

Users can be modified by clicking on the `Edit` pencil icon in the row of
the desired user. Users can be deleted by clicking on the `Delete` trash
icon in the row of the desired user or from within the User Edit page.

![Individual User](https://aview-docs.accns.com/screenshots/individual_user.png)

## Managing User Permissions

User's are given specific access and abilities by their organization
administrator or the administrator of an organization above them.  The
abilities and access to certain areas of the Accelerated View site are called
user "permissions".  A permission could include features such as being able to
send remote commands, move devices to other organizations, deploy/undeploy
devices, and having access to view certain categories of devices.

![User Permissions](https://aview-docs.accns.com/screenshots/user_permissions.png)

To manage the permissions a user has, navigate to the Users page, click the
`Edit` pencil icon in the row of the desired user, and then click the
`Permissions` tab.  New permissions can be added by clicking the `Create`
plus icon, selecting the new permission from the drop-down list, and then click
`Create Permission`.  Note that the list of available permissions are based on
the permissions of the user who is managing the user and the permissions available
in the user's organization.  To remove a permission from a user, click the `Delete`
link. 

## Creating a New User

New users can be created by clicking the `Create` plus icon located at the top
right of the Users page.  The user will first be asked to select which organization
the new user will belong to, and select a role to assign to the user.  When a
new user is created, Accelerated View will send a welcome email to the user's
email address with instructions on setting up their user's password and  account.

**Note:** When creating a new user, the user must setup their account and password
within 72 hours (3 days) after the email has been sent.  After this time, the
user will need to be recreated.

![New User](https://aview-docs.accns.com/screenshots/new_user.png)
