# Reports

Accelerated View allows a user to create a variety of reports based on various
selection criteria. The Reports feature can be selected by clicking on
`Reports` in the navigation panel, and then selecting the desired report type.

![Reports Drop-down](https://aview-docs.accns.com/screenshots/reports_dropdown.png)

For details on report types, refer to the
[Generating Reports](https://aview-docs.accns.com/reports/index.html) section
of this guide.

## Report History

Accelerated View will retain up to ten generated reports per user.  These
reports can be viewed by clicking on the `History` link in the `Reports`
drop-down menu of the navigation bar.

![Report History](https://aview-docs.accns.com/screenshots/report_history.png)

Clicking on a report ID will direct the user to a page that displays the
results of the report.  The user can export any of the reports listed in the
report history to a CSV file by clicking the `CSV` link.
