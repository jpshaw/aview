# Devices

Displays the status of all devices accessible by a user. Clicking on the
`Devices` button in the navigation bar displays a drop-down that lets a user
specify which type of device to be displayed, or they can display all devices
together.

![Devices](https://aview-docs.accns.com/screenshots/all_devices.png)

The Devices page allows a user to display the current status of all devices
managed by their organization. The filter bar also allows for the selection of
only a certain category of devices (VPN, WiFi, Cellular, etc).

Once displayed, the user will see a tabular tab of the devices and their
current statuses. The filter bar allows a user to filter the results so that
only the relevant devices are shown. The user can select the organization that
should be displayed and then further narrow the results down by filtering on
the device type or status.

All devices are listed in the table with the following details.  A user can
display additional details or hide details by selecting the `wrench` icon at
the top-right of the table.

  * **Status:** The status displayed is a color coded indicator. Refer to
    [Monitoring Devices](https://aview-docs.accns.com/statuses.html) section of
    this guide for details on each status color.
  * **MAC:** The MAC address of the device generating the event.
    The MAC address field is selectable. Clicking this link will direct the user
    to the device's details page. For additional information about the device's
    details page, refer to the section of this guide that details the specific
    device category that is being viewed.
  * **Comment:** Any user-entered comment on the device, up to 255 characters
  * **Host:** The WAN IP address of the device.  Clicking the link provided in
    the field will open a new URL with the host as the URL.  For example if the
    host is 172.16.3.2, the URL will be https://172.16.3.2
  * **Category:** The category of the device.
  * **Model:** The model of the device.
  * **Organization:** The organization that the device is associated with. A
    device is assigned to an organization when it is entered into Accelerated
    View. In most cases the organization will be the company or one of its
    sub-organizations. This field is selectable.  Clicking the link in this field
    will display all the devices associated with that organization and any
    sub-organizations under it.
  * **Site:** The site ID that the device is associated with.  Clicking the
    link in this field will display all the devices associated with that site ID
    in the same organization.
  * **Firmware:** The firmware version the device is currently running on.
  * **City:** The city where the device is located.
  * **Country:** The country where the device is located.
  * **Activation Date:** The timestamp of when the device first reported in to
    the Accelerated View site.
  * **Last Heartbeat:** The last time the device has checked in with
    Accelerated View.

## Device Details Page

Clicking on the Device ID (MAC address) of a device will direct the user to the
device details page.  This includes detailed information about the specific
device.  Below is a list of details found on the device details page that
applies to all categories of devices.

![Individual Device](https://aview-docs.accns.com/screenshots/individual_device.png)

### Settings Tab

The Settings tab contains attributes that can only be updated if the user has
permissions to do so.  If a user does not have permissions to edit these
settings, this tab will not be listed.  If a user does have the ability to
edit these settings of the device, then the options will be listed in an edit
mode, with drop-downs and input text boxes listed.  The screenshot above shows a
device that the user has permissions to edit.  Multiple options can be edited
at the same time.  Once any desired updates have been made, click the green
`Update Settings` button at the bottom of the Settings tab.

  * **Organization:** The organization that the device is currently associated
    with.  If the user has the ability to change the device's organization, a
    button labelled `Change` will be displayed that, when clicked, will allow a
    user to move the device to a new organization.  When moving the device to
    a new organization, the user can also optionally copy the device's site
    and/or data plan (if any).
  * **Site ID:** The site ID that the device is associated with.  Clicking the
    link in this field will display all the devices associated with that site ID
    in the same organization.

### Details Tab

The Details tab contains specific information that identifies the device. 
Below is a list of details that apply to all devices.

  * **Status:** The current status of the device when this page was loaded.
  * **MAC Address:** The MAC address of the device.
  * **Serial:** The serial number of the device.
  * **Model:** The model of the device.
  * **Firmware Version:** The firmware version the device is currently running
    on.
  * **Activated At:** The timestamp of when the device first reported in to the
    Accelerated View site.
  * **Last Heartbeat:** The last time the device has checked in with
    Accelerated View.
  * **Last Restart:** The last time the device rebooted.
  * **Comment:** Any user-entered comment on the device, up to 255 characters

### Location Tab

The Location tab displays the location of the device on a geographical map
based on the device's address details.  Depending on the user's permissions,
the location details may be listed in a view-only mode, or there will be
editable text boxes and drop-downs for the user to update the location if
desired.

To add or update a device's location, enter in a valid address into the location
details.  Once all desired address details have been typed in, click the green
`Update` button at the bottom of the location tab. The Accelerated View application will
send the address details to GoogleMaps to validate the address and the map will
be updated.

If Google does not return a valid address, Accelerated View will save the address details as
entered.  However, the Accelerated View application will not display a map of the address
details since Google did not return valid latitude/longitude coordinates.

Clicking the `Clear` button at the bottom of the location tab will remove the
location details for the device and remove the map from the page.

### Cellular Location

If Accelerated View has valid GSM location details for a device with cellular
capabilities, a second pin, colored orange, will appear on the map to indicate
the location determined using these GSM details.

![Example GSM Location](https://aview-docs.accns.com/screenshots/gsm_location_example.png)

### Contacts Tab

The Contacts tab displays any contacts that are associated with the device.  A
contact must contain a name and email address, along with an optional phone
number.  When creating a new contact, the contact can be designated as a
general, technical, sales, or site contact.  The same contact info can be used
for multiple devices.  Once a contact is made, the user can apply the same
contact to another device by going to the new device's details page, click the
green `Add` button, and then select the appropriate contact from the
drop-down list of existing contacts.

![Device Contact](https://aview-docs.accns.com/screenshots/device_contact.png)

### Commands

The `Commands` button provides a drop-down list of available remote commands
that can be sent to the device.  Clicking on a command will execute that
command.  For a list of available device commands and details of what they do,
refer to the `Remote Commands` section of the particular device category in
this guide.

### Reports

The `Reports` button provides a drop-down list of available reports that can
be generated for the individual device.  For more details on each report, refer
to the [Reports](https://aview-docs.accns.com/reports/index.html) section.

### Actions

The `Actions` button provides a drop-down list of available actions that can be
performed concerning the device in Accelerated View.

  * **Subscribe/Unsubscribe:** Allows a user to subscribe to or unsubscribe
    from receiving notification alerts from the device.  For details about
    managing notifications and profiles, refer to the
    [Managing your User Preferences]
    (https://aview-docs.accns.com/profile/index.html) section of this guide.
  * **Deploy/Undeploy:** Controls the deployment of a device.  For details
    about the deployment status and deploying devices, refer to the
    [Statuses](https://aview-docs.accns.com/statuses.html) and [Deploying Devices]
    (https://aview-docs.accns.com/deploying.html) sections of this guide.

### Pages

The `Pages` button provides a drop-down list of pages related to the individual
device.

  * **Device Details:**  Directs the user to the device details page.  If the
    user is already on the device details page, the page will simply refresh.
  * **Device Events:**  Directs the user to the events page for the specific
    device.

### Device Events

The Device Events page displays a real-time chronologically sorted table (newest on
top) of device events for the individual device. Event management provides
a set of alert conditions that are significant to the user. As the Event
conditions are detected they are categorized and color-coded for easy
recognition. Critical errors and alerts are coded Red while warnings and
notices are Orange or Yellow. All other device events are informational only
and not coded. The Events page refreshes automatically every few seconds to
keep up to date information on the page.

Events can be filtered in a variety of different ways using the Filter Menu at
the top of list. A user can filter the list of events in the following ways:

  * **Level:** Severity level of the alert.
    selected at once.
  * **Range:** The time range of events to display.  The default is to
    display the events for the past 24 hours.
  * **Type:** The type of events displayed can be filtered.

A user can also export the the current page or all events using the `Export` button.

## Event Details

All Events are listed with the following information:

  * **Created:** The date that an event occurred and time that the event
    occurred. The time is displayed in the time zone of the user.
  * **Type:** The state of the device that is generating the event. There
    are currently seven different states that a device can be in.
    1. *Status:* Device is reporting its current operational status.
    2. *Configuration:* Device or remote operation has initiated a
      configuration change or information request.
    3. *Firmware:* A firmware action has been initiated or the device
      determined there was a conflict.
    4. *Reboot:* The device has restarted.
    5. *DHCP:* The device is reporting a change in its DHCP configuration.
    6. *Tunnel:* VPN tunnel status.
    7. *Remote:* A command has been received from the portal and an action
      initiated.
  * **Level:** The severity level of the event being displayed. Accelerated
    View supports a modified version of the severity levels defined by
    [RFC 5424](https://tools.ietf.org/html/rfc5424). The supported six levels
    of events are listed from lowest to highest alert level below.
    1. *Debug:* Info useful to developers for debugging the application, not
      useful during operations.
    2. *Info:* Normal operational messages – used for reporting, measuring
       throughput, etc. - no action required.
    3. *Notice:* Events that are unusual but not error conditions. No immediate
      action required.
    4. *Warning:* Warning messages. Not an error, but indication that an error
      will occur if action is not taken. This message indicates an item that must be
      resolved within a given time.
    5. *Error:* Non-urgent failures, these should be relayed to developers or
      admins; each item must be resolved within a given time.
    6. *Alert:* Should be corrected immediately, therefore the appropriate
      action or support notification should be initiated to fix the problem.
  * **Information:** The data that the device transmitted to Accelerated View.
    These messages can be simple text messages or complete hardware status
    displays. The information field is selectable. Clicking this link will direct
    the user to a page that displays all the information of the individual event,
    including a detailed description of what has occurred.

![Individual Event](https://aview-docs.accns.com/screenshots/individual_event.png)
