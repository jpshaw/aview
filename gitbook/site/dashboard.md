# Dashboard

The `Dashboard` page displays several charts to provide at-a-glance details
for the devices a user has access to.  The segments inside each chart can
be selected to display a table of devices included in that segment.
Each chart can be exported by opening the menu at the top-right of the chart.

The dashboard also shows a real time list of devices that are in an alert
status (down). The display includes a map of sites that a user has access to.
There is also a tabular list at the bottom of the page that lists the devices
in alert status.

![Dashboard](https://aview-docs.accns.com/screenshots/dashboard.png)

The dashboard displays all critical information about the devices under a
user’s scope of control. The top of the dashboard displays the number of
devices managed and the number of devices in each state.

Alert devices are displayed visually on a map of the organization’s home
country. The maps can be zoomed in and out to display finer levels of
granularity all the way to a single site. Detailed information on a particular
site can be obtained by clicking on the status indicator display. This will
display an information bubble with the device type, site ID, and device
information. Clicking on the device ID will direct the user to the individual
device's details page. Clicking on the site ID will direct the user to a page
that lists all the devices associated with that site ID in that particular organization.

**Note:** The dashboard page does not automatically refresh, so the devices
listed in on the dashboard page were in alert status at the time of when the
page loaded.  If the page is left alone for an extended period of time, the
listed devices in alert state may become outdated.

## Device Count Charts

At the top of the Dashboard page are charts that list the number of devices in
a specific state or status.  Clicking one of segments will display the
devices that are in the selected state or status that the user has access to. 
For details on each device status and state, please refer to the [Monitoring
Devices](https://aview-docs.accns.com/statuses.html) section of this guide.

  * **Deployed:** Devices that are deployed, regardless of what status the
    device has.  This includes all devices but those with the pause icon.
  * **Undeployed:** Devices that are undeployed (pause icon).
  * **Up:** Devices that are deployed and up.  This includes green, blue, and
    orange color statuses.
  * **Down:** Devices that are deployed and down (red color status).
  * **Unreachable:** Devices that are deployed and unreachable (gray color status).
