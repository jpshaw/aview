# Tunnel Servers

The `Tunnel Servers` page displays all the known VIG and SIG tunnel endpoints
that VPN Gateway devices in Accelerated View are connected to. Each tunnel server
is listed in the table with the following details:

  * **Name:** The name of the tunnel server.  Clicking on the name will direct
    the user to a page that lists details for each VPN Gateway device connected
    to that tunnel server.
  * **Devices:** The number of devices connected to the tunnel server.
  * **IP Addresses:** The number of IP addresses associated with the tunnel
    server.  Depending on the network setup, a single tunnel server could have
    multiple IP addresses and physical servers pooled together that makes up
    the tunnel server network.
  * **Type:** The type of tunnel server; typically either a VIG or SIG.
  * **Location:** The listed location for the tunnel serer.
