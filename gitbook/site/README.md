# Site Navigation

## Navigation Panel

The navigation panel, which is found on the left side of every page, provides
access to the main sections of the Accelerated View site. Several of the items
have sub-menus that allow the user to further narrow the selection.

The navigation panel also has the ability to set an organization filter so that
all displays only apply to that organization and any sub-organizations. The
organization field, which is above the menu, allows a user to limit their
displays to a certain organization. Accelerated View makes extensive use of the
structure called the Organization. An organization is simply a term that is
used to associate devices, sites, users, and configurations together.  A
sub-organization is a term that describes an organization that is accessible
under a higher level organization.

![Navigation Panel](https://aview-docs.accns.com/screenshots/navigation_panel.png)

## Search bar

In the toolbar at the top of each page of the Accelerated View site is a Search
box that can be used to locate specific devices or sites.  Enter the desired
value, press Enter, and the selected entries will be displayed.  The search
tool will match on the following fields:

  * MAC address
  * Serial Number
  * Modem ICCID
  * Site ID

The `Advanced Search` link can be used to search for a device or site by more
specific details; including location, IP address, phone number, site ID, and
cellular modem details.

## Filter bar

On a number of pages in the Accelerated View site there will be a variety of
filters that allow a user to narrow their display.

The type of Filters changes depending on the type of detail being displayed.
These allow a user to filter only the entries they wish to see. The items that
can be filtered are listed when a user clicks on the filter bar. Multiple
filters can be applied at once.  Once a filter is selected, the results on the
page will automatically be filtered and refreshed.

A user can remove the applied filters by selecting the `x` icon to the right
of the filter bar. The applied filters only apply to the current page, if the
user goes to a different page and comes back, the filters will be reset.

Note that some filters require the user to type in a value to be filtered on,
such as the `MAC` address filter for devices.

![Filter toolbar](https://aview-docs.accns.com/screenshots/filter_toolbar.png)

  * **Organization:** The organization filter to apply to the current page. 
    The icon next to the organization drop-down allows a user to filter and show
    just the single organization selected, or to include the organization's
    sub-organizations.
  * **Device-only filters**
      * *Categories:* The category of devices (such as gateway, cellular, or
        WiFi).
      * *Deployment:* What state the device is in (deployed or undeployed).
      * *Firmware:* The firmware version of the device.
      * *Hostname:* The network hostname of the device.
      * *MAC:* The MAC address of the device.
      * *Model:* The device model type (such as 8200 or 6200-FX)
      * *Site:* The site name associated with the device.
      * *Status:* What status the device is in (such as up or down).  Note, the
        status only applies to deployed devices.
  * **Event-only filters**
      * *Types:* All events are tagged with a certain type, making it easy to
        associate similar events together (such as DHCP, tunnel, or reboot).
      * *Levels:* The severity level of the event.  Each event is tagged with a
        certain severity level to indicate the importance of the event (such as
        notice or alert).  Severity levels are listed in the filter from lowest
        severity to highest severity.
      * *Range:* The time range of events to display.  The default is to
        display the events for the past 30 days.
  * **User-only filters**
      * *Name:* The name of the user.
      * *Email:* The email address of the user.
      * *Timezone:* The timezone selected by the user for displaying timestamps
        in Accelerated View.
      * *Last Login:* The timestamp of the user's last successful login.
      * *Status:* The status of the user's account in Accelerated View.  Active
        indicates that the user has logged in within the past 30 days.  If a user
        has not logged in within the past 30 days, they account is set to inactive.
        A Pending account indicates the user has never logged in to Accelerated View.

## Pagination

Several pages throughout the Accelerated View site have pagination, where only
a certain number of entries are listed on each page.  The number of entries can
be controlled by the pagination drop-down menu, which is always listed at the
top-right of the table on the page.

## Account

The `Settings` drop-down at the top of the Navigation panel includes an `Account`
button.  This is where a user can access their profile settings, update their
password, and manage their notifications.  For more details, please refer to the
[Managing User Preferences](https://aview-docs.accns.com/profile/index.html) and the
[Managing Notifications](https://aview-docs.accns.com/notifications/index.html) sections.

## Logout

The `Settings` drop-down at the top of the Navigation panel includes a `Logout`
button.  Clicking on this will log the user out of Accelerated View.
