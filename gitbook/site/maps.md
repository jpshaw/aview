# Maps

Displays the graphical view of monitored devices on a world map. The maps allows
a user to drill down on specific units and their current status. When global
devices are being managed, a specific country or region can be selected.

![Maps](https://aview-docs.accns.com/screenshots/maps.png)

The `Maps` page displays the status of the various devices visually on a map of
the organization’s home country. The map can be zoomed in and out to display
finer levels of granularity all the way to a single site. The map can be
filtered by organization, device category, device type, status, and
country/region. Changes can be made to any filter criteria before filtering the
map. A user can also toggle the map between a street view and geographical view.

Once the desired devices are displayed a user can obtain more detailed
information on a particular site by clicking on the status indicator display.
This will display an information bubble with the device type, site ID, and
device information.  Clicking on the Organization will direct the user to a
page that lists all the devices associated with that organization.  Clicking on
the site ID will direct the user to a page that lists all the devices
associated with that site ID in that particular organization.

![map pop-up](https://aview-docs.accns.com/screenshots/map_popup.png)
