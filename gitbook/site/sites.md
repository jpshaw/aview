# Sites

The Sites page displays all the current sites that the user has access to. The
panel displays all unique sites that have been entered for the organization
along with the organization name and number of devices.  Note that sites are
tied to the organization they are associated with, so there may be multiple
sites with the same name, but they are associated to different organizations.

![Sites](https://aview-docs.accns.com/screenshots/sites.png)

The Site and Organization fields are both selectable items. Clicking on Site
name will bring up the device display panel and list all devices at that
location.

![Individual Site](https://aview-docs.accns.com/screenshots/individual_site.png)

Selecting the Organization field will display all devices that have been
defined for that organization. Any device in the list can then be selected and
managed.
