# WiFi Devices

WiFi devices refer to the Accelerated WiFi Extender line of products. 
Supported models are listed below.  A user can find a list of all the WiFi
devices they have access to by clicking on the `Devices` tab in the navigation
bar, and then select the `WiFi` link from the drop-down.

  * 4200-NX

![WiFi Devices](https://aview-docs.accns.com/screenshots/wifi_index.png)

## WiFi Device Details

![WiFi Device](https://aview-docs.accns.com/screenshots/wifi_device.png)

The majority of details provided on the WiFi device details page are applicable
for all categories of devices.  For details on those table entries, refer to
the [Devices](https://aview-docs.accns.com/site/devices.html) section of this
guide.  The device details specific to WiFi devices are listed below.

  * **IP Address:** The WAN IP address of the WiFi device.

### Associated Devices Tab

This tab lists any devices that the WiFi device has associated with, indicating
that they are connected to each other.  The tab will include the following
details about the associated device.

  * **Device ID:** The identifying MAC address for the associated device.
  * **Model:** If the associated device is a type of device supported by ARMT,
    this entry will display the type of device ARMT knows it as. If the device
    is not supported by ARMT, the category will be listed with a dash ( - ).
  * **Interface:** The physical interface of the WiFi device the associated
     device is connected to.
  * **Type:** How the associated device is connected to the WiFi device.  For
  	example, a type listed as `WAN` indicates that the associated device is
  	connected to one of the WiFi device's WAN Ethernet ports.
  * **IP Address:** The IP address of the associated device.
  * **Connected:** Indicates when the device association was made and how long
    the associated device has been connected to the WiFi device.

### Rogue Access Points Tab

The WiFi device can be configured to search for other wireless Access Points (APs).
Any of these nearby APs that also happen to be on the same LAN as the WiFi device
will be flagged as a rogue AP.  This tab lists any rogue APs discovered by the
WiFi device.  The following details are included about each rogue AP.

  * **MAC:** The identifying MAC address of the rogue AP.
  * **SSID:** The wireless SSID broadcasted by the rogue AP.
  * **IP:** The IP address of the rogue AP.

### Configuration

WiFi devices are configured through the AT&T Service Manager.


## WiFi configuration requirements for Accelerated View

In order for the Accelerated View site to monitor a WiFi device, it must send
its syslogs to the Accelerated View syslog server IP address, the domain of
which is `logs.accns.com`
