# Legacy Cellular Devices

Legacy Cellular devices refer to the Accelerated Cellular Extender line of products. 
Supported models are listed below.  A user can find a list of all the cellular
devices they have access to by clicking on the `Devices` tab in the navigation
bar, and then select the `Cellular` link from the drop-down.

  * [6200-FX](https://accelerated.com/products/cellular_extender_model_6200_fx/)
  * NetBridge-SX

![Legacy Cellular Devices](https://aview-docs.accns.com/screenshots/cellular_index.png)

Legacy Cellular devices are listed in tabular form.  The majority of the details are
applicable to all device categories.  For details on those table entries, refer
to the [Devices](https://aview-docs.accns.com/site/devices.html) section of this
guide.  The table entries specific to cellular devices are listed below.

  * **Carrier:** The cellular provider of the current connection.
  * **dBm:** The RSSI signal strength of the cellular connection measure in dBm.
  * **Signal:** The RSSI signal strength percentage of the cellular connection.
  * **Network:** The technology type of the cellular connection.
  * **Phone Number:** The phone number of the current cellular connection.

## Legacy Cellular Device Details

![Legacy Cellular Device](https://aview-docs.accns.com/screenshots/cellular_device.png)

The majority of details provided on the cellular device details page are
applicable to all categories of devices.  For details on those table entries,
refer to the [Devices](https://aview-docs.accns.com/site/devices.html) section
of this guide.  The device details specific to cellular devices are listed below.

  * **Cellular IP Address:** The IP address received when connecting to the
    cellular network.
  * **Client MAC Address:** The MAC address of the client device (the device
    connected to the Cellular Extender's Ethernet port).
  * **Client IP Address:** The IP address the Cellular Extender device assigned
    to the client device (the device connected to its Ethernet port).
  * **Ethernet Status:** The status of the physical link on the Ethernet port
    of the Cellular Extender.
  * **USB Conditioner:** Indicates if a USB Conditioner hub device is connected
    between the Cellular Extender and the USB modem.
  * **USB List:** Lists the vendor and product IDs of any USB devices connected
    to the Cellular Extender.
  * **Management Tunnel Status:** Indicates whether the remote command IPSec tunnel
    is up and active, or disabled.  If disabled or down, users will not be able to
    send remote commands to the Legacy Cellular Device.  Note that SMS commands
    will still work if the SIM card used by the Legacy Cellular device has SMS enabled.

### Settings Tab

The majority of details provided on the Legacy Cellular `Settings` tab are
applicable to all categories of devices.  For details on those table entries,
refer to the [Devices](https://aview-docs.accns.com/site/devices.html) section
of this guide.  The device details specific to Legacy Cellular devices are listed below.

  * **Data Plan:** The data plan assigned to the device.  If the user has the
    ability to change the data plan, a drop-down will be displayed and lists
    the available data plans for the organization the device belongs to.  Once
    a data plan is associated with the device, any cellular data used by the
    device will be tracked in the data plan, which can be viewed and managed
    by clicking the `Edit` button for an Organization (see the
    [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section).  The user also has the option of creating a new data plan if desired.

### Configuration

Legacy Cellular devices are configured through the Accelerated View site.  For
details, see the
[Legacy Cellular Configuration](https://aview-docs.accns.com/configurations/legacy_cellular_configuration.html)
section of this guide.

### Associated Devices Tab

This tab lists any devices that the Legacy Cellular device has associated with, indicating
that they are connected to each other.  The tab will include the following
details about the associated device.

  * **Device ID:** The identifying MAC address for the associated device.
  * **Model:** If the associated device is a type of device supported by ARMT,
    this entry will display the type of device ARMT knows it as. If the device
    is not supported by ARMT, the category will be listed with a dash ( - ).
  * **Interface:** The physical interface of the Legacy Cellular device the associated
     device is connected to.
  * **Type:** How the associated device is connected to the Legacy Cellular device.  For
  	example, a type listed as `LAN` indicates that the associated device is
  	connected to one of the Legacy Cellular device's LAN Ethernet ports.
  * **IP Address:** The IP address of the associated device.
  * **Connected:** Indicates when the device association was made and how long
    the associated device has been connected to the Legacy Cellular device.

### Cellular Details Tab

The Cellular Details tab lists details for the cellular modem.  The name of the cellular
modem is listed in the title bar of the Cellular Details tab.  In the example above, the
cellular modem name is `Sierrawireless340u`.  If a cellular device has multiple
modems, there will be multiple tables, each one corresponding to one of
the cellular device's modems.  A modem entry can be removed by clicking the red
trash icon at the top-right of the specific tab.

  * **Carrier:** The cellular provider of the modem.
  * **Signal Strength:** The current RSSI signal strength value, measured in dBm,
    and percentage of the modem.
  * **Signal Bars:** The current signal bars, based on the signal strength.
  * **Speed Test:** The five most recent (if any) speed tests performed on the
device. Speed tests are generated by sending a **Speed Test** command to the device.
    * ***Note:***  In order to minimize the speed test's impact on cellular data
consumption, the results are an estimation of the available throughput of the
device, and may not represent the full network speed available.
  * **Phone Number:** The phone number of the modem.
  * **RSSI:** The Received Signal Strength Indicator (RSSI) of the modem. RSSI
    provides information about total received wide-band power, including signal
    interference and thermal noise.
  * **RSRP:** An LTE-specific value, representing the Reference Signal Received
    Power of the modem.  RSRP is the average power of resource elements that carry
    cellular specific reference signals over the entire bandwidth.
  * **RSRQ:** An LTE-specific value, representing the Reference Signal Receive
    Quality (RSRQ) of the modem.  RSRQ is a combination of RSSI and RSRP and
    indicates the quality of the received reference signal.
  * **SiNR:** The signal-to-interference-plus-noise ratio of the modem.
  * **SNR:** The signal-to-noise ratio of the modem.
  * **IMEI:** The IMEI value of the modem.
  * **ESN:** The ESN value of the modem.  Note that the ESN value only applies
to CDMA cellular networks.
  * **ICCID:** The ICCID value of the modem.
  * **Revision:** The hardware version of the modem.
  * **APN:** The APN that the modem is currently connected with.  Note that not
all modems require an APN.
  * **ECIO:** The signal-to-noise ratio of the modem.  The lower the Ec/Io
value, the greater the noise.
  * **CNTI:** The technology type that the modem is currently connected to.
  * **Band:** The frequency or type of cellular band the modem is currently
    connected to.
  * **CID:** The CID identifier of the modem's current connection.
  * **LAC:** The LAC identifier of the modem's current connection.
  * **MCC:** The mobile country code of the modem's current connection.
  * **MNC:** The mobile network code of the modem's current connection.
  * **PLMN:** The PLMN ID of the modem's current connection, which is a
    combination of the MCC and MNC values.
  * **USB Speed:** The speed of the USB port(s) on the Legacy Cellular device,
    measured in Mbps. 480 indicates USB 2.0 High Speed and 12 indicates USB 2.0
    Full Speed.

### Cellular Utilization Chart

If the device supports a cellular modem, a cellular utilization chart will be
displayed.  The chart shows the amount of cellular data used by the device.
The initial timespan will display the data usage for the past 24 hours.
A user can select different timespans from the `Range` drop-down.

![Cellular
Utilization](https://aview-docs.accns.com/screenshots/cellular_utilization.png)

### Tunnels Tab

The Tunnel tab lists status for any IPSec tunnels established by the Legacy Cellular
device.  Each row of the tab indicates a unique IPSec tunnel of the device.

  * **Name:** The name of the IPSec tunnel, as specified in the configuration
    of the Legacy Cellular device.
  * **Status:** Indicates whether or not the Legacy Cellular device has established
    the IPSec tunnel.

### Remote Commands

Remote commands are sent to a cellular device through an IPSEC tunnel the
cellular device establishes to the Accelerated remote control server. If the
remote control IPSEC tunnel is disconnected, then a user will not be able to
send remote commands to the cellular device. The available remote commands that
can be sent to cellular devices are listed below.

  * **Check status:** The cellular device will immediately send its current
    status.
  * **Check signal strength:** The cellular device will send its signal strength
    and network info once every 10 seconds for the next 15 minutes.
  * **Perform Speed Test:** The cellular device will perform a speed test and
    send the Upload/Download results.
    * ***Note:***  In order to minimize the speed test's impact on cellular data
consumption, the results are an estimation of the available throughput of the
device, and may not represent the full network speed available.
  * **ARPing Attached Device:** The cellular device will attempt to ARPing the
    client device attached to its Ethernet port.
  * **Send Wake-on-LAN to Attached Device:** The cellular device will send a
    wake-on-LAN packet to the client device attached to its Ethernet port.
  * **Check Configuration:** The cellular device will immediately pull its
    configuration and reboot to apply any new settings.
  * **Reboot:** Reboot the cellular device.
  * **Emergency SMS Commands**
      * *Set Configuration:* A pop-up window appears where a user can enter in a
        configuration option or set of options to apply to the cellular device. 
        Multiple config options must be separated by a comma.  For example, if a
        user wants to set the APN of a cellular device to managedvpn, they would
        enter "modem_apn=managedvpn".
      * *Configuration Reset:* Restore default configuration on the cellular
        device and reboot.
      * *Firmware Reset:* Restore to the backup firmware image on the cellular
        device, and reboot.
      * *Factory Reset:* Restore default configuration on the cellular device,
        restore to the backup firmware image, and reboot.
      * *Remote Control:* Tell the cellular device to bring up its remote
        control tunnel (useful if on low data plan, so the remote control tunnel
        is not always up using data).
      * *Reboot:* Reboot the cellular device.
  * **Remote Power Commands:**  If a Remote Power device is connected via
    serial-to-USB cable to the Cellular Extender, these remote commands can be
    used to toggle the power on or off on either of the Remote Power device's
    outlets.

## Legacy Cellular configuration requirements for Accelerated View

In order for the Accelerated View site to monitor a cellular device, it must
send its syslogs to the Accelerated View syslog server IP address, the domain
of which is `logs.accns.com`

In order for a cellular device to get its configuration from the Accelerated
View site, it must be set to pull its configuration from the Accelerated View
configuration server `aview.accns.com`.  The device must also be able to sync
its date and time with an NTP server; the default is `time.accns.com`.
