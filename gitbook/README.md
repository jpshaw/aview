Accelerated View
====

Accelerated View (Accelerated View) is a sophisticated, but intuitive
cloud-based portal that monitors remote data network devices across Tier 1
carriers and service providers. In today’s environment the configuration,
installation, and central management of complex distributed networks is a
challenge for businesses. The challenge increases for businesses with multiple
locations, which now may span multiple time zones and countries. To complicate
matters further many locations may not have any support staff on-site.

Accelerated View provides a company with one system to monitor and remotely
control VPN Gateways, Cellular Extenders, WiFi Extenders, and Dial-to-IP
devices. The system has a powerful user interface that allows sorting and
searching in order to improve productivity and identify problem patterns. In
addition to standard reports, online views can be exported to create ad hoc
reports. Data is provided on over 50 items regarding the availability,
performance, configuration and location of your devices. Remote devices can be
configured in groups or individually, and the firmware easily updated from a
central operations center. Based on user criteria the portal can also send
real-time alerts to defined e-mail addresses.

The system's mapping functions provide a visual picture of a customer’s
network status at any given time. The maps can be zoomed in and out to display
finer levels of granularity all the way to a single site. The device can then
be selected from the map for further analysis and control. Mapping is also used
for site location information, along with the physical address and contact
information.

Managing large enterprises, distributed enterprises, and mid-market networks
alike is simple with Accelerated View. As long as a site has a connection to
the Internet, you are ready to remotely manage its devices. Accelerated View
lives in the cloud and utilizes up to date secure technology. This is
especially important in large distributed enterprises, where there could be
thousands of locations. Your infrastructure can be monitored and controlled by
multiple administrators who are geographically dispersed, each with appropriate
permissions.
