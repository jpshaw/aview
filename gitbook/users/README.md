# Managing User Access

The following section details how to control the level of access a user has to
devices and organizations within Accelerated View.  This is done by
[Roles](https://aview-docs.accns.com/users/roles.html) and
[Permissions](https://aview-docs.accns.com/users/permissions.html).

To view the roles and permissions associated with an organization, navigate to
the `Organizations` page, click on the drop-down action box on the right hand
side of the desired organization's row, and select `Edit Settings`. The roles
and permissions will be listed as tabs on the organization's edit page.

![Demo Org](https://aview-docs.accns.com/screenshots/demo_org.png)

To view the permissions associated with a user, navigate to the `Users` page
and select either the desired user's email address or `Edit` icon.  The user's
permissions will be listed as a tab on the edit page.

![Demo User Permissions](https://aview-docs.accns.com/screenshots/demo_user.png)

If your user has the ability to manage other users, you will also have the ability
to edit the user's notification subscriptions and profiles.  For more details
on notifications, see the
[Managing Notifications](https://aview-docs.accns.com/notifications/index.html)
section of this guide.
