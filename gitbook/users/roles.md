# Roles

Roles are used to specify a group of abilities for an organization.  These
roles can then be applied to one or more users within that organization as a
permission. For details on permissioning users, please refer to the
[Permissions](https://aview-docs.accns.com/users/permissions.html) section of
this guide.

To view the roles an organization has, navigate to the `Organizations` page,
click on the drop-down action box on the right hand side of the desired
organization's row, and select `Roles`.

![Demo Org Roles](https://aview-docs.accns.com/screenshots/demo_org_roles.png)

To create a new role, select the `Create` plus icon. Specify the name of the
role and select the desired abilities.  A description of each ability can be
found by hovering over the help boxes next to each ability check-box.  Once all
the desired abilities have be selected, click `Create Role`.

![New Role](https://aview-docs.accns.com/screenshots/new_role.png)
