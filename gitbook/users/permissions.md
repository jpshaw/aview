# Permissions

Permissions are used to specify the type of access a user has to one or more
organizations. Users can be added to an organization and be given specific
access to certain other organizations. A permission a user has for an
organization is based on the role that is assigned to the user for that
organization. For details on creating a role, please refer to the
[Roles](https://aview-docs.accns.com/users/roles.html) section of this guide.
Below is a screenshot detailing an example set of organizations and users.

![Demo Organizations](https://aview-docs.accns.com/screenshots/demo_organizations.png)

![Demo Users](https://aview-docs.accns.com/screenshots/demo_users.png)

Assigning permissions to a user can be done when creating the user, or by
selecting the `Create` plus icon on the `Permissions` tab of the user's
profile. Note that a user cannot change their own permissions.  They can only
change permissions for other users if they have the permission themselves to do
so.

Below is an example permission setup for the given organization list. The
screen-shot accompanying the setup details how the permissions page of the
user's profile should look.

![Demo Org Permissions](https://aview-docs.accns.com/screenshots/demo_org_permissions.png)

  * A user that belongs to the `Accelerated Demo` organization with the
    following permissions:
    * Admin permissions on `Accelerated Demo`.
    * View-only permissions on `USARMT`.

![Demo User Permissions](https://aview-docs.accns.com/screenshots/demo_user_permissioning.png)
