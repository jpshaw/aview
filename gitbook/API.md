# API

Accelerated View provides a RESTful API that can be used to obtain a list of
details for one or more devices a user has access to.  Full documentation on
using the API can be found at the following link:

https://aview.accns.com/apidocs

## Managing your token

Authenticated use of the API is accomplished by the use of a per-user unique
token ID.  You can view your user's token ID by clicking the `Settings` drop-down
at the top of the left navigation panel, select the `Account` link, then click
the `API Access` tab.  This token never expires as long as the user has access
to the Accelerated View application.  You can generate a new token at any time
for your user by clicking the `Generate New Token` button.

## Limitations

The API limits each user to 100 requests every 15 minutes.

![API Setup](https://aview-docs.accns.com/screenshots/api_setup.png)
