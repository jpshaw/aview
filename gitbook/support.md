# Support

## Supported Browsers

* Internet Explorer 9, 10, or 11 
* Latest Chrome
* Latest Firefox
* Latest Opera
* Latest Safari
* Microsoft Edge
