## Creating a New Legacy Cellular Configuration Profile

Before creating a new cellular configuration profile, first decide if the
profile should inherit any settings from another configuration profile
(referred to as the 'parent configuration').  This adds the benefit of having a
structured and/or tiered configuration environment, where a new configuration
inherits all the defaults for an organization, and the user adds the necessary
changes to those base settings.  To view the current list of configuration
profiles a user has access to, click on the `Configurations` button in the
navigation panel and then select `Legacy Cellular` in the drop-down.

![Legacy Cellular Configuration Index](https://aview-docs.accns.com/screenshots/configurations.png)

To create a new configuration profile, first navigate to the `Configurations`
section of Accelerated View by clicking on the `Configurations` button in the
navigation panel, and then select `Legacy Cellular` in the drop-down.  Next click the
plus icon located at the top right of the window to create a new cellular
configuration.  Specify a name for the configuration profile and select the
desired parent configuration.  If a user does not select a parent
configuration, the profile will inherit the system default values for a
cellular device.

Next, enter in any other desired configuration options.  Refer to the help boxes
for each configuration option for details on what the option is used for as well
as what the default value is for the option.  Required fields are marked
with asterisks (*) and any values inherited from the parent configuration are
listed in red.  Once all the desired configuration options have been specified,
click `Create`.

![New Legacy Cellular Configuration](https://aview-docs.accns.com/screenshots/new_cellular_config.png)

### Applying a cellular configuration profile

To apply a configuration profile to a cellular device, go to the device details
page for the specific device.  This can be done by searching for the device's
MAC address in the search bar, then clicking on the MAC address of the correct
device in the results.  Once on the details page, select the new configuration
profile from the `Configuration` drop-down menu below the Admin panel.

![Apply a Legacy Cellular Configuration](https://aview-docs.accns.com/screenshots/applying_cellular_config.png)


## Deleting a configuration profile

Configuration profiles can be deleted by selecting the desired configuration
name on the `Legacy Cellular Configurations` page, then select the `Delete
Configuration` button at the top right of the profile.  Note that if there are
any devices linked to the configuration profile that is being deleted, the
profile will not be deleted.  The user must first unlink the configuration
profile from the device and then they can remove the profile.
