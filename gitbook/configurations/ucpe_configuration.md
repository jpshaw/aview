## Creating a New uCPE Configuration Profile

### View a List of All uCPE Configuration Profiles

To view the current list of group configuration profiles a user has access to:

  * Click on the `Configurations` button in the navigation panel.
  * Select `uCPE` in the drop-down.

![uCPE Configuration
Index](https://aview-docs.accns.com/screenshots/configurations_ucpe_index.png)

### Creating a New Configuration Profile

To create a new group configuration profile:

  * Click on the `Configurations` button in the navigation panel.
  * Select `uCPE` in the drop-down.
  * Click the plus icon located at the top right of the window.
  * Select the firmware version.  The appropriate configuration schema will
    automatically load.
  * Set any desired settings in the presented configuration schema.  Hovering
    your mouse over the name for a configuration option will display a pop-up
    providing help details about that option, including any default values.
    Once all the desired configuration options have been specified, click `Create`.
  * **NOTE:**  If the desired firmware is not listed, it must be uploaded onto
    the firmware management service, which is hosted at
    [distro.accns.com](https://distro.accns.com).

![New uCPE
Configuration](https://aview-docs.accns.com/screenshots/configurations_ucpe_new.png)

### Applying a uCPE configuration profile

To apply a group configuration profile to a uCPE device, go to the device
details page for the specific device.  This can be done by searching for the
device's MAC address in the search bar, then clicking on the MAC address of the
correct device in the results.  Once on the details page, click on the `Configuration`
tab, select the new configuration profile from the `Group Configuration` drop-down,
and click the `Save` button.

![Applying a uCPE
Configuration](https://aview-docs.accns.com/screenshots/configurations_ucpe_apply.png)

### Individual Device Configuration Settings

In addition to applying a group configuration profile to a device, authorized
users will also have the option of setting configuration options to apply only
to an individual device.  These individual device configuration options are
listed in the `Configuration` tab, below the `Group Configuration` option.

When a new group configuration is selected for a device, a list of inherited
configuration settings from that group configuration profile will be displayed.
Authorized users can then click on any setting and select the `Override` option
to change the selected configuration option.  It is important to note that any
configuration option overridden in this way will only apply to that individual
device.  Any other devices that are linked to the same group configuration
will only receive configuration settings from the group configuration profile,
not any of the overriden settings from another individual device's configuration.

* **Inherit All:**  This button can be used to reset an individual device's
  configuration back to the group configuration profile's settings.  Any
  configuration options previously overridden in the individual device configuration
  will be undone and reset back to the settings inside the group configuration.

## Deleting a configuration profile

Configuration profiles can be deleted by clicking on the `X` delete icon for
the appropriate profile on the `uCPE Configurations` page.  A
confirmation pop-up will ask the user to confirm that they wish to delete the
configuration profile.  If there are any devices linked to the configuration
profile that is being deleted, the profile will not be deleted.  The user must
first unlink the configuration profile from the device and then they can remove
the profile.
