# Gateway Devices

Gateway devices refer to the AT&T VPN Gateway line of products.  Supported
models are listed below.  A user can find a list of all the Gateway devices
they have access to by clicking on the `Devices` tab in the navigation bar, and
then select the `Gateway` link from the drop-down.

  * U110
  * VPN Gateway 8300
  * VPN Gateway 8200
  * VPN Gateway 8100

![Gateway Devices](https://aview-docs.accns.com/screenshots/gateway_index.png)

Gateway devices are listed in tabular form.  The majority of the details are
applicable to all device categories.  For details on those table entries, refer
to the [Devices](https://aview-docs.accns.com/site/devices.html) section of this
guide.  The table entries specific to Gateway devices are listed below.

  * **Tunnels:** The current number of VPN tunnels the Gateway device has up as
    of when this page was loaded.
  * **Connection:** The connection currently being used by the Gateway device
    as of when this page was loaded.

## Gateway Device Details

![Gateway Device](https://aview-docs.accns.com/screenshots/gateway_device.png)

The majority of details provided on the Gateway device details page are
applicable to all categories of devices.  For details on those table entries,
refer to the [Devices](https://aview-docs.accns.com/site/devices.html) section
of this guide.  The device details specific to Gateway devices are listed below.

  * **Primary IP:** The IP address of the current WAN network connection of the
    Gateway device.
  * **Firmware Rollback:** The rollback firmware version.  Issuing the **Rollback**
    remote command to the Gateway device will cause it to revert to the rollback
    firmawre version specified here.
  * **Management IP:** The IP address of the Gateway device's management tunnel.
    If this IP does not match the maintenance tunnel IP address listed for the
    Gateway device in the AT&T Service Manager, a user with the ability to
    modify devices will be given an option to edit the management IP address.
  * **Configuration Model:** The configuration model associated in the Service
    Manager profile for the VPN Gateway.
  * **Primary Interface:** The currently used WAN connection interface.  For
    example, `eth1` corresponds to the WAN1 port of the 8200 VPN Gateway.
  * **Primary Connection Type:** The type of network configuration of the
    current WAN connection.
  * **WAN 1 Cell Extender:** Indicates if the `WAN via Cell Extender` option is
    enabled in the Gateway device's AT&T Service Manager profile.
      * *Note:* This field will only appear if there is a valid value entered
        in the SMx profile (not blank).
  * **WAN 2 Cell Extender:** Indicates if the `WAN2 via Cell Extender` option
    is enabled in the Gateway device's AT&T Service Manager profile.
      * *Note:* This field will only appear if there is a valid value entered
          in the SMx profile (not blank).
  * **Time in Backup:** If the current status of the VPN Gateway is in backup mode,
    this will indicate the duration that the VPN Gateway has bee running in backup
    mode.  This field will only be displayed when the VPN Gateway is in backup mode.
  * **LAN Status:** The status of the eight LAN ports of the VPN Gateway.  A
    black color status indicates that nothing is physically connected to
    the LAN port.  A green status indicates that some network device is
    physically connected to the LAN port.  A user can hover over each port to
    display a popup text with network details about the port.

### WAN Utilization

If a VPN Gateway's SMx profile has COS or non-COS bandwidth monitoring enabled,
and the VPN Gateway's profile has the WAN Utilization SNMP inform enabled, a
WAN utilization chart will be displayed detailing the amount of bandwidth used by
the device.  Note, the VPN Gateway must be on firmware version 6.2 or higher in
order to enable non-COS bandwidth monitoring. For COS bandwidth monitoring, the
VPN Gateway must be on firmware version 6.4.546 or higher.

### Settings Tab

The majority of details provided on the Gateway `Settings` tab are
applicable to all categories of devices.  For details on those table entries,
refer to the [Devices](https://aview-docs.accns.com/site/devices.html) section
of this guide.  The device details specific to Gateway devices are listed below.

  * **Data Plan:** The data plan assigned to the device.  If the user has the
    ability to change the data plan, a drop-down will be displayed and lists
    the available data plans for the organization the device belongs to.  Once
    a data plan is associated with the device, any cellular data used by the
    device will be tracked in the data plan, which can be viewed and managed
    by clicking the `Edit` button for an Organization (see the
    [Organizations](https://aview-docs.accns.com/site/organizations.html)
    section).  The user also has the option of creating a new data plan if desired.

### Associated Devices Tab

This tab lists any devices that the Gateway device has associated with, indicating
that they are connected to each other.  The tab will include the following
details about the associated device.

  * **Device ID:** The identifying MAC address for the associated device.
  * **Model:** If the associated device is a type of device supported by ARMT,
    this entry will display the type of device ARMT knows it as. If the device
    is not supported by ARMT, the category will be listed with a dash ( - ).
  * **Interface:** The physical interface of the Gateway device the associated
     device is connected to.
  * **Type:** How the associated device is connected to the Gateway device.  For
  	example, a type listed as `WAN` indicates that the associated device is
  	connected to one of the Gateway device's WAN Ethernet ports.
  * **IP Address:** The IP address of the associated device.
  * **Connected:** Indicates when the device association was made and how long
    the associated device has been connected to the Gateway device.

### Cellular Details Tab

The Cellular Details tab lists details for the Gateway's cellular modem, if it is using one.
The name of the cellular modem is listed in the title bar of the Modem table. If a
Gateway device has multiple modems, there will be multiple modem panels, each
one corresponding to one of the cellular device's modems.  A modem entry can be
removed by clicking the red trash icon at the top-right of the specific tab.

  * **Carrier:** The cellular provider of the modem.
  * **Signal Strength:** The current RSSI signal strength value, measured in dBm,
    and percentage of the modem.
  * **Phone Number:** The phone number of the modem.
  * **RSSI:** The Received Signal Strength Indicator (RSSI) of the modem. RSSI
    provides information about total received wide-band power, including signal
    interference and thermal noise.
  * **RSRP:** An LTE-specific value, representing the Reference Signal Received
    Power of the modem.  RSRP is the average power of resource elements that carry
    cellular specific reference signals over the entire bandwidth.
  * **RSRQ:** An LTE-specific value, representing the Reference Signal Receive
    Quality (RSRQ) of the modem.  RSRQ is a combination of RSSI and RSRP and
    indicates the quality of the received reference signal.
  * **SiNR:** The signal-to-interference-plus-noise ratio of the modem.
  * **SNR:** The signal-to-noise ratio of the modem.
  * **IMEI:** The IMEI value of the modem.
  * **ESN:** The ESN value of the modem.  Note that the ESN value only applies
to CDMA cellular networks.
  * **ICCID:** The ICCID value of the modem.
  * **IMSI:** The IMSI value of the modem.
  * **Revision:** The hardware version of the modem.
  * **APN:** The APN that the modem is currently connected with.  Note that not
all modems require an APN.
  * **ECIO:** The signal-to-noise ratio of the modem.  The lower the Ec/Io
value, the greater the noise.
  * **CNTI:** The technology type that the modem is currently connected to.
  * **Band:** The frequency or type of cellular band the modem is currently
    connected to.
  * **CID:** The CID identifier of the modem's current connection.
  * **LAC:** The LAC identifier of the modem's current connection.
  * **MCC:** The mobile country code of the modem's current connection.
  * **MNC:** The mobile network code of the modem's current connection.
  * **PLMN:** The PLMN ID of the modem's current connection, which is a
    combination of the MCC and MNC values.

### Cellular Utilization Chart

If a VPN Gateway's primary WAN connection is a cellular connection, and the device
belongs to an organization with the `Enable Cellular Modem Data Collection` option
enabled (see the *Device Settings* details in the
[Organizations](https://aview-docs.accns.com/site/organizations.html) section), a
cellular utilization chart will be displayed on the Cellular Details tab detailing
the amount of cellular data used by the device.

![Cellular
Utilization](https://aview-docs.accns.com/screenshots/cellular_utilization.png)


### Tunnels Tab

The Tunnel tab lists details for the VPN tunnel established by the gateway
device.  If a Gateway device has multiple tunnels active, there will be
multiple tunnel tabs, each one corresponding to one of the Gateway device's
tunnels.

  * **Name:** The name of the tunnel, as specified in the Service Manager profile.
  * **Connection ID:** The unique identifier for the current tunnel connection.
  * **Connection Type:** The type of network connection the tunnel is
    established over.
  * **Account:** The account that the tunnel settings are registered under.
  * **User ID:** The ATTUID that the tunnel settings are registered under
  * **Duration:** The total time the current tunnel has been established as of
    when this page was loaded.
  * **Mode:** Indicates if the user can control the tunnel.
  * **Endpoint Type:** The authentication server that will be used by the
    tunnel endpoint.
  * **Endpoint IP:** The IP address of the tunnel endpoint.
  * **WAN Connection:** The type of network configuration of the current WAN
    connection (WAN1, WAN2, etc.)
  * **AGNS Managed:** The service the tunnel is authenticated for.
  * **Auth Server:** The server where the tunnel details are configured.
  * **Auth Protocol:** The authentication protocol for the tunnel.
  * **Initiator:** Indicates how the tunnel was initiated.
  * **Last Updated:** The last time Accelerated View received an update concerning this tunnel.

### Location Tab

The location details for a VPN Gateway device are provided through the AT&T
Service Manager profile of the device.  As such, location details are displayed
as read-only for VPN Gateway devices.  If users want to set the location of a
VPN Gateway device, they can do so through the Service Manager profile for the
VPN Gateway.  In the profile, users can specify address details
for the location, which Accelerated View will then use to lookup location
coordinates for the device, or users can specify the latitude and longitude
coordinates manually in the Service Manager profile.

### Connectivity Tests Tab

This tab lists the 10 most recent backup connectivity tests reported by the
VPN Gateway device, including:

- Result: whether the test passed or failed
- Reason: the error code (if applicable)
- Category: the category of the failed test (if applicable)
- Source: whether the test was manually triggered or automated
- Date: when the test was executed

### IDS Tab

This tab lists a count of IDS (intrusion detection) alerts detected by the VPN
Gateway.  The alerts are broken down by IDS type and displayed on a table,
showing details for each type of alert and how many of each alert type was received.

The alerts are sent from the VPN Gateway to a Kafka REST API on ARMT.  The VPN
Gateway must be running firmware version 6.5.0 or higher to send these alerts.
The chart on the tab lists a total number of alerts received per day (if any)
over the past 30 days.

### Remote Commands

Remote commands are sent to a Gateway device through its management tunnel.  If
the Gateway device's management tunnel to the Accelerated View site is
disconnected, then a user will not be able to send remote commands to the
Gateway device.  The available remote commands that can be sent to gateway
devices are listed below.  Any responses from sending one of these remote
commands to a Gateway device will be listed as an event in Accelerated View.

***Firmware version 6.1.8 or newer***

  * **Arping:** Send an network ARPing to a specified IP address.
  * **Check Status:** Perform an immediate SNMP probe of the gateway.
    device.
  * **Firmware:** either check if a firmware update is available, or tell the
    Gateway device to upgrade its firmware.  The specified firmware version
    is based on the firmware URL defined in the gateway's AT&T Service Manager
    profile.  There is also an option to revert the device to the rollback
    firmware version.
  * **Ping:** ping a specified IP address.
  * **Profile Query:** Send the command to perform a partial or full
    configuration update from the device's AT&T Service Manager profile.
  * **Reboot:** Send the command to reboot the Gateway device.
  * **Tracepath:**  trace the path to a specified network IP address,
    discovering MTU along this path
  * **Traceroute:**  trace the network path to a specified IP address.
  * **Wan Toggle:** enable or disable a WAN interface on the Gateway device
    for a specified amount of time.  The Gateway device must be on firmware
    version 6.5.0 or higher to accept this command.
  * **Backup WAN Test:** manually run a backup connectivity test on the
    secondary WAN connection.  The Gateway device must be on firmware
    version 6.5.0 or higher to accept this command.
  * **Tunnels:** provides the ability to check the status of all configured
    outbound tunnels of a Gateway device, including bring a tunnel up/down,
    bounce a tunnel, or checking a tunnel's status.  The name and index of the
    desired outbound tunnel must be specified to perform these commands.
  * **CX Access:** provides the ability to setup port forwarding rules for
    SSH access to the router connected to WAN2 of the Gateway device, using
    the WAN1/primary IP address of the Gateway device.

***Firmware versions older than 6.1.8***

  * **Check Device Status:** Perform an immediate SNMP probe of the gateway
    device.
  * **Check Configuration:** Send the command to perform a partial or full
    update depending on the configuration option specified in the gateway
    device's AT&T Service Manager profile.
  * **Reboot Device:** Send the command to reboot the Gateway device.

## Gateway configuration requirements for Accelerated View

Accelerated View utilizes a management tunnel to communicate with the gateway
device.  The management tunnel must be configured correctly in order for
Accelerated View to be able to probe and send commands to the Gateway device. 
Contact your AT&T Service Manager specialist if assistance is needed
configuring the management tunnel of the Gateway device.

In addition to the management tunnel, the Gateway device must be configured to
send its SNMP Informs to the Accelerated View server.  Several of the
notifications sent by Accelerated View are based on the SNMP informs sent by
the Gateway device.  Contact your AT&T Service Manager specialist if assistance
is needed configuring the SNMP Inform options of the Gateway device.
