# Adding Devices to Accelerated View

## Adding Gateway Devices

Gateway devices are added, configured, and deleted from the AT&T Service
Manager (SMx).  The Accelerated View tool receives a daily update from SMx with
a list of gateway devices.  This list is then synced with the list of gateway
devices currently in Accelerated View.  If any new devices are found in the SMx
file, they are added in Accelerated View, and their SMx configuration settings
are imported.  If a device is not found in the SMx file, but it exists in
Accelerated View, then that device is removed from Accelerated View.

## Adding Devices

Any non-Gateway device can be added by Accelerated View site administrators, as
long as it is a recognized device type (cellular, WiFi, etc.).  The user
should contact their Accelerated View site administrator if they do not have
permission to add devices.  To add devices to the Accelerated View site, use
the admin device importer, found by clicking the `Admin` button in the top
navigation bar.

![Admin Button](https://aview-docs.accns.com/screenshots/admin_button.png)

Next click on the green `New Import` button.  If a device import is already
running, this button will not be available and the user must wait for the
current import to finish.  Refresh the `Admin` page to check on the status of
the running import.

In the new import options, ensure that the following options are selected.

![New Import](https://aview-docs.accns.com/screenshots/new_import.png)

The file that is uploaded must contain details for the new devices in a
specific format.  Download the file template by clicking the `Template CSV`
link.

> **Important Note:** Keep the first line of the CSV file exactly as is.  These
column headers are required for the device importer to work properly.  If the
column headers are deleted, then the devices will not be imported.

Enter in the device details that are listed in the template CSV file.  Note
that a MAC address, a serial number, and an organization must be specified
in order to add a new device.  The other fields are optional.

If you are specifying a value in the `config_name` column of the CSV file for a
device, that value must correspond to an existing configuration profile for that
category of devices in Accelerated View.  See the `Configuration` sections for each type of
device in this guide for more details about setting up configuration profiles.

> **Note:** Configurations cannot be specified in the CSV file for Gateway or
WiFi devices.  Those devices' configuration profiles are setup in the AT&T
Service Manager.

Once the CSV file is created and selected, click the green `Import` button to
add the devices.  Once the import is complete (refresh the Admin page to check
its status), the user can review the import results by clicking on the import
ID.

![Import Result](https://aview-docs.accns.com/screenshots/device_import_result.png)
