# Changelog

The format is based on [Keep a Changelog] and this project adheres to
[Semantic Versioning].

## [3.12.0] - 2018-10-25
### Changed
- Improve device event processing throughput
- Add support for IPSec DNS lookup for device commands

## [3.11.1] - 2018-10-25
### Added
- A clock shown near the logo in the top left (disabled by default). Users
  can enable this on their account page (AVIEW-453).
- Latest VPN Gateway MIB support (AVIEW-410).
- Add VIG location information to VIG Utilization Charts

### Removed
- Extra period in VVIG help text (AVIEW-444).

### Fixed
- Null pointer exception that prevented some Data Plans from being shown.
- Table offset error that prevented reports from being shown (AVIEW-455).

### Changed
- Device heartbeats are now pre-calculated, instead of each time an event comes
  in. This greatly improves event throughput.
- VVIG is now just VIG (AVIEW-462).
- WAN utilization chart changes based on AT&T specifications (AVIEW-445,
  AVIEW-446, AVIEW-447)
- WAN utilization chart help text was updated to reflect changes.
- Tunnel status color was changed based on feedback.

## [3.11.0] - 2018-10-12
### Added
- VIG/PNC Utilization Charts
- Update device event processor to extract the reason for reboots and translate
  them into a human readable messages.

### Fixed
- Allow blank time for Admin Settings > Maintenance Message.
- Bug fix for footer/content overlap with long custom footer.
- Fix for typographical erros in WAN Toggle description.
- Fix for Dataplan limit decimal places display disparity between fields.
- Reset Dataplan usage when cycle start date is changed.
- Resolve issues with dropped device logs.

## [3.10.2] - 2018-09-11
### Changed
- Custom text for aView and ARMT feedback modals (AVIEW-336)
- Increase footer length from 255 to 1023 (AVIEW-337)
- Update `last restart` timestamp when Gateway evBoot inform in received
- Update labels, colors, and fix selector logic for Gateway WAN Utilization
  chart (AVIEW-378)
- Updated WAN Utilization graph help documentation

### Fixed
- Listen for contact updates on the correct model to replicate to SMX
- Include user information in feedback email for aView (AVIEW-359)
- Fix bug where Gateway VLANs were assigned to disabled ports (AVIEW-368)
- Resolve inconsistencies with dataplan display (AVIEW-375)
- Ensure that the dataplan date is properly rounded to days in UTC regardless
  of the local time
- Clear support contact list if any emails are invalid
- Preserve history fields when creating/updating Gateway Internet Route or
  Cascaded Network configurations (AVIEW-371)

## [3.10.1] - 2018-08-10
### Removed
- net-ping gem (and annoying `busybox` cli error)

### Changed
- Hide feedback link if no contact list is set (AVIEW-348)
- Make hidden tags input wider to avoid wrapping unless address is fairly
  long (AVIEW-347)
- Do not close modal when clicking background (AVIEW-339)
- Add proper failure messages for tunnel up/down commands (AVIEW-345)
- Rename "CX Access" to "Router Access" (AVIEW-323)

### Fixed
- Fix bug with wrench options not working for column hide/show (AVIEW-343)
- Fix labeling on status column (AVIEW-341)
- Fix data plans bulk emails (AVIEW352)
- Resolve issue where the feedback widget throws an error from login since
  there is no user from which to get user account. Per Kim’s suggestion, simply hiding the link when there is no user (AVIEW-353)
- Merge create history with updates so it isn't clobbered (AVIEW-342)
- Replicate contact edits to SMX (AVIEW-340)

## [3.10.0] - 2018-08-10
### Added
- Added maintenance message page
- Added new admin-controlled login message box
- Support for new CX Access command options
- Support for new Gateway SNMP Informs
- Ability for users to export data plans
- Attach a breakdown of individual device usage to data plan alerts

### Changed
- Only run data plan job if it is enabled and has devices
- Optimize cellular utilization reports for BigQuery

### Fixed
- Whitelist history params for updates to Gateway Internet Routes / Cascaded
  Networks (AVIEW-322)

## [3.9.4] - 2018-07-30
### Changed
- Always display cellular utilization chart (AVIEW-297)
- Optimize availability report when using BigQuery
- Update acc-config.js to f03fee2742a
- Disable device event poller spam in the deployed rails logs
- VRRP state of a VPN Gateway device is now based on the VRRP status of any/all
  VLAN interfaces instead of just the default VLAN interface
- Remove microseconds from smx replication history timestamps (AVIEW-275)

### Fixed
- Remove stale VRRP state for devices not longer enabled with VRRP
- Use correct structs to parse SMX delete replications for Internet Routes
  and Cascaded Networks (AVIEW-277)

## [3.9.3] - 2018-07-25
### Changes
- Fetch metrics for data plan devices in a single query instead of querying per
  device (AVIEW-257)

### Fixed
- Replace deprecated HTTP gem `with` with `headers`

## [3.9.2] - 2018-07-23
### Fixed
- Replace deprecated `status_code` call with `code` for http gem
- Fix device commands that require user credentials

## [3.9.1] - 2018-07-20
### Changed
- Use tunnel name in tunnel-related events instead of connection id for the
  VPN Gateway (AVIEW-240)
- Replaced WR14 cellular model with IX14 model (AVIEW-279)

### Fixed
- Fixed the IDS alert URL when the URI="url", and the code=URI (AVIEW-260)
- Target the entire select2 organization filter widget to resolve the
  jumping problem (AVIEW-262)
- Update faceting to use device_state_status and keep exclusion of
  undeployed devices for down devices (AVIEW-263)
- Fixed manual tunnel command delimiter (AVIEW-265)
- Remove ES2017 function declaration shorthand to avoid IE11 issues (AVIEW-266)
- Ensure Gateway firmware is >= 7.0 for tunnels status feature (AVIEW-267)

## [3.9.0] - 2018-07-13
### Added
- Added new SIM switch remote commands for uClinux devices
- Add ability to migrate arrays in acc-config.js (PR #2567)
- Added link to Site ID on device show page
- Enhanced tunnels column for Gateway devices to show active versus expected
  tunnels count and status
- Added ability to search API for devices by serial
- Added new `Comment` field to device index and show pages
- vuejs-rails 2.5.13
- Added logging info to the bulk notifier
- Use kraken logger in BulkNotifier job
- Added new `CX Access` remote command for VPN Gateway devices
- Added user/account sync between ARMT and SMx when a VPN Gateway
  profile/config is updated

### Removed
- Removed requirements for serial numbers to be exactly 16-digits
- Removed requirement for users to click `OK` button of Privacy/Cookie policies
  before they could login to the site.
- Removed `VRRP Master` from VPN Gateway device statuses
- Removed `Category` column from device-specific index pages. Category is
  still included on `All` index page

### Changed
- Categorized and grouped remote commands listed under `Commands` dropdown
- Allow individual uclinux device configurations to span the whole row
- Updated footer links to add Privacy and Cookie policies
- Replaced Druid backend for reports/charts with BigQuery (including partitiontime support)
- Status column on Gateway index page now only reflects the WAN status of the device
- Filter index page to always show undeployed devices at bottom of the list of
  devices, regardless of the up/down status of the undeployed device.
- Use puma instead of thin for MRI
- Increated site-wide org typeahead search limit from 10 to 100 to account for
  search terms with common phrasing
- Changed manual tunnel up/down command to prepopulate interface fields with
  with blank instead of zero
- Adjusted width of modal field headers
- Limit location postal code to 20 characters
- Upgrade dry-validation from 0.12.0 to 0.12.1
- Updated device status filtering to not show undeployed devices when filtering
  by up/down status

### Fixed
- Replace deprecated `with` with `headers` for http gem
- Fixed bug where modem with missing mcc/mnc/cid/lac values would show an
  incorrect cellular location
- Fixed bug where `Update Modem Firmware` remote command would fail when sent
  to a uClinux device using the json_command
- Prevent duplicate modem records when device switches SIM slots
- Prevent "aview=purge" ipsec events from deleting the tunnel that the event updates.
- Fixed bug where bulk notification frequency was not honored
- Prevent Error on Page Load for Device Show Page for Legacy Cellular Devices
- Fixed bug where clicking in text box of site-wide org filter would make the
  dropdown unusable

## [3.8.0] - 2018-06-14
### Added
- Add support for U115 model (AVIEW-125)
- Add Site ID to device show page header
- Add ability to sort device status by color (AVIEW-138)
- Add alerts for Gateway OOB cable inform (AVIEW-121)

### Removed
- Remove support for deprecated TorqueBox `node_name`
- Removed the following gems: coffee-rails, sshkit, fpm, childprocess, browser,
  option, eventmachine, ruby-ip, zookeeper, ffaker, sysinfo, rb-readline

### Changed
- Let individual and group ucLinux device configuration span the whole row
- Replace FactoryGirl with FactoryBot
- VRRP now takes precedence over Dual WAN for Gateway status

### Upgraded
- rails 4.2.10 (was 4.2.7.1)
- http 3.3.0 (was 0.9.6)
- uglifier 4.1.10 (was 2.7.2)
- carrierwave 1.2.2 (was 0.10.0)
- mini_magick 4.8.0 (was 3.8.1)
- newrelic 5.1.0.344 (was 3.18.0.329)
- ipaddress 0.8.3 (was 0.8.0)
- hashie 3.5.7 (was 3.4.2)
- sass-rails 5.0.7 (was 5.0.6)
- non-stupid-digest-assets 1.0.9 (was 1.0.5)
- apipie-rails 0.5.8 (was 0.3.5)
- versionist 1.7.0 (was 1.4.1)
- yard 0.9.12 (was 0.8.7.6)
- enumerize 2.2.2 (was 1.0.0)
- paper_trail 9.0.2 (was 8.0.0)
- bcrypt 3.1.12 (was 3.1.10)
- pundit 1.1.0 (was 1.0.1)
- bootstrap-sass 3.3.7 (was 3.3.5)
- exception_notification 4.2.2 (was 4.1.1)
- axlsx 3.0.0.pre (was 2.1.0.pre)
- roo 2.7.1 (was 2.5.1)
- selenium-webdriver 3.12.0 (was 2.53.0)
- remotipart 1.4.2 (was 1.2.1)
- font-awesome-rails 4.7.0.4 (was 4.5.0.0)
- simple_form 3.5.1 (was 3.2.0)
- geocoder 1.4.8 (was 1.4.4)
- interactor-rails 2.2.0 (was 2.0.1)
- seed_migration 1.2.3 (was 1.0.5)
- protected_attributes 1.1.4 (was 1.1.3)
- rack-utf8_sanitizer 1.5.0 (was 1.3.2)
- savon 2.12.0 (was 2.11.1)
- micromachine 3.0.0 (was 1.2.0)
- httparty 0.16.2 (was 0.13.5)
- friendly_id 5.2.4 (was 5.1.0)
- versionomy 0.5.0 (was 0.4.4)
- net-ping 2.0.4 (was 1.7.7)
- will_paginate 3.1.6 (was 3.0.7)
- breadcrumbs_on_rails 3.0.1 (was 2.3.1)
- highcharts-rails 6.0.3 (was 4.1.8)
- jbuilder 2.7.0 (was 2.3.1)
- awesome_print 1.8.0 (was 1.6.1)
- better_errors 2.4.0 (was 1.1.0)
- binding_of_caller 0.8.0 (was 0.7.3.pre1)
- fix-db-schema-conflicts 3.0.2 (was 2.0.0)
- letter_opener 1.6.0 (was 1.4.1)
- rspec-html-matchers 0.9.1 (was 0.7.0)
- rspec-rails 3.7.2 (was 3.5.2)
- timecop 0.9.1 (was 0.8.0)
- faker 1.8.7 (was 1.5.0)
- shoulda-matchers 3.1.2 (was 2.8.0)
- shoulda-callback-matchers 1.1.4 (was 1.1.3)
- capybara 3.1.0 (was 2.5.0) 3.1.0
- poltergeist 1.18.0 (was 1.8.1)
- database_cleaner 1.7.0 (was 1.5.1)
- webmock 3.4.1 (was 1.21.0)
- test_after_commit 1.1.0 (was 0.4.1)
- simplecov 0.16.1 (was 0.10.0)
- activerecord-jdbcmysql-adapter 1.3.24 (was 1.3.23)
- thin 1.7.2 (was 1.6.4)
- rb-readline 0.5.5 (was 0.5.4)
- web-console 3.3.0 (was 2.2.1)

### Fixed
- Disable strict integer validation on DeviceModem signal that prevents some
  device events from being processed (AVIEW-208)

## [3.7.8] - 2018-05-25
### Added
- Add ability to migrate arrays to uClinux configuration compiler
- Add support for WR14 device model
- Add support for non-standard device serial formats
- Add Privacy and Cookie policy links to the footer (aView only)
- Add cookie messaging to login for GDPR compliance

## [3.7.7] - 2018-05-18
### Added
- Add UTC labels to IDS chart timestamps

### Removed
- Remove no-longer-relevant IDS parenthetical

### Changed
- Rename `Washington DC` to `District of Columbia`

### Fixed
- Ensure root organization is always set as the parent of organizations created
  via SMX auth (AVIEW-171)
- Fix form capitalization on page shown when a report is generating
- Fix 'Authoritative Server' inheritance check-box
- Ensure location address is html safe when loaded via ajax

### Removed
- Remove references to yellow color status

## [3.7.6] - 2018-05-09
### Added
- Add general init.d script (AVIEW-22)
- Collect and store Gateway primary VRRP status with SNMP (AVIEW-4)
- Save tunnel name from SNMP inform (AVIEW-80)
- Save device attributes if updated in SMx tabfile (AVIEW-93)
- Rake task to synchronize VIG associations
- Rake task to generate a report on VIG association health
- Added status option to WAN toggle command (AVIEW-88)
- Add a state dropdown to the device location page for when the country is
  USA. Any other country will display the province text field. (AVIEW-115)
- Associate Gateway Tunnels with Tunnel Servers
- Authorize Gateway configuration changes (AVIEW-146)

### Changed
- Add support for Gateway Dual WAN statuses (AVIEW-1, AVIEW-77, AVIEW-79)
- Add latest Gateway tunnel down reason codes (AVIEW-63)
- Dynamic unit labeling for the WAN Utilization charts (AVIEW-59)
- Javascript (acc-config.js) updates in support of the latest firmware
- Swap orange + blue dual wan statuses for consistency (AVIEW-96)
- Add 5402-RM device support (AVIEW-106)
- Hide WAN Toggle feature for 8200 and older gateways (AVIEW-111)
- Update of the text for VRRP Master Mode in the Device Details page (AVIEW-79)
- Improved propagation of updates from the SM to ARMT of existing
  Internet Routes/Cascaded Networks (AVIEW-161)
- Only show device settings tab for users with ability to modify device
  configurations (Level 1/2 in ARMT) (AVIEW-164)

### Removed
- Device event cleanup job (AVIEW-29)
- Disable 'Last Configuration Check'
- Remove stray fields from the network configuration tab (AVIEW-76)

### Fixed
- Set default map parameter with ES5 instead of ES6 (AVIEW-69)
- Fix date and time parsing in logstash device event processor (AVIEW-68)
- Fix the WAN Utilization to use Inform time intervals (AVIEW-33)
- Change location event processing to save lat/lon coordinates in a cellular
  location record instead of the user-entered location (AVIEW-142)
- Add support for handling new IPsec config path in firmware versions
  18.4.54 or higher (AVIEW-133)
- Fix the Authoritative Server inheritance syncing between SM and ARMT (AVIEW-87)
- Fix the unending spinner when clicking on Inherit in Internet Routes (AVIEW-154)
- Prevent stale VIG associations by using Gateway Tunnel associations to
  Tunnel Servers (AVIEW-107)
- Ensure location address fields are html safe (AVIEW-157)
- Fix device data plan name for view-only users (AVIEW-164)
- "VIG" not capitalized in some locations (AVIEW-73)

## [3.7.4] - 2018-03-16
### Added
- Device map javascript for maps page and dashboard
- Add Kraken::Logger gem
- Rspec configuration for services
- Geocoder / Reverse Geocoder service integration tests
- Support for HSDPA 3G Network
- Replicate device location coordinates back to SMX if they already exist
- Ability to delete device modems

### Changed
- Use Kraken::Logger for webapp and services
- Capitalize WAN toggle command
- Clarify error message when sending remote command to device

### Removed
- Old maps javascript that extended jQuery

### Fixed
- Show cellular pins for a device if it doesn't have a location
- Hide device location tab if user disabled maps
- Copy device site only if present

### Upgraded
- nokogiri 1.6.1 -> 1.8.2
- mysql2 0.3.11 -> 0.4.10
- vcr 2.9.3 -> 4.0.0
- dogstatsd-ruby 2.2.0 -> 3.3.0

## [3.7.3] - 2018-01-18
### Fixed
- Fix navigation menu hover messages
- Fix admin panel authorization check
- Fix manual tunnel command by name
- Fix sort advanced search results by country when searched by country
- Fix gateway and netbridge command dependencies

## [3.7.2] - 2018-01-15
### Fixed
- Fix configuration validations

## [3.7.1] - 2018-01-12
### Added
- Add wan_toggle command for VPN Gateways
- Add backup_test command for VPN Gateways
- Add new Connectivity Test tab for tracking backup test results

### Changed
- Refactor scoped queries to use the Finder pattern
- Add activated_at column to accelerated device importer
- Optimized device details page for faster loading and rendering. Tabs
  now load on click instead of at page-load time
- Auto-undeploy VPN Gateway devices if Accelerated View hasn't heard
  from them within 30 days
- Auto-deploy VPN Gateway devices once they sync with Accelerated View
- Tracking the cellular band reported by a VPN Gateway's cellular modem
- Track tx/rx usage of VPN Gateway's LAN ports
- Add encrypted SMS string to SMS error events
- Allow users to set blank site IDs for a device

### Removed
- "Default" site IDs

### Fixed
- Fixed bug when saving the history of who edited a device configuration
- Fix orphaned individual config from stopping new device config save
- Removed duplicate device_network records

## [3.7.0] - 2017-12-04
### Added
- Add support for Intelliflow
- Add support for Intrusion Detection System (IDS)

### Changed
- Remove deprecated RPM packaging
- Remove deprecated notification profile/subscription routes
- Update and clarify the WAN Utilization Chart Help modal
- Add last configuration checked at time to device details
- Add modem band support for Gateways on at least 6.5 firmware

### Fixed
- Do not allow duplicate network interfaces for a given device

## [3.6.11] - 2017-11-13
### Fixed
- Fix issue preventing contact form errors from being shown in the contact modal
- Allow locations to be cleared for devices via SMX inbound

## [3.6.10] - 2017-10-31
### Fixed
- Remove size limit when saving device configuration history

## [3.6.9] - 2017-10-26
### Changed
- Update and clarify the WAN Utilization Chart Help modal
- Set Cascaded Network boolean defaults to false to match SMX
- Limit Internet Routes / Cascaded Networks description field to 30
  characters and capitalize input
- Limit Site ID and Contact inputs to match SMX
- Display "Assign Existing Contact" list in alphabetical order

### Fixed
- Show current (incomplete) day interval on the 7 day WAN Utilization chart
- Remove Gateway configuration DHCP Type to prevent Address Translation errors
- Ensure Gateway contacts are capitalized to prevent duplication from SMX
- Only display one world map on Location tab

## [3.6.8] - 2017-10-20
### Fixed
- Make WAN Utilization legend consistent when CoS is enabled/disabled
- Fix mismatched headers for WAN and Cellular utilization charts
- Fix exceptions thrown by parsing invalid dates from SMX
- Let Gateway contacts be shown in the "Assign Existing Contacts" list
- Fix device associations for existing contacts
- Don't allow blank Site IDs for devices

## [3.6.7] - 2017-10-11
### Changed
- Remove "Default" notification profiles

### Fixed
- Fix map filtering (broken in 3.5.0)
- Fix Cascaded Networks / Internet Routes for IE9-11
- Only track Gateway cellular utilization when cellular is primary
- Show device organization name for device reports on reports history page
- Fix duplicate Cascaded Networks / Internet Routes issue when created in ARMT
- Clear device location when blank location is given

## [3.6.6] - 2017-10-02
### Changed
- Move create entry button for Cascaded Networks / Internet Routes
- Add explicit filter text for Cascaded Networks / Internet Routes
- Remove latitude & longitude from device location view-only tab

### Fixed
- Fix log file naming for instances where an explicit file name isn't given
- Fix Gateway network configuration sync with SMX
- Catch JBoss illegal state exceptions

## [3.6.5] - 2017-09-25
### Changed
- Upgrade geocoder gem from 1.3.1 to 1.4.4

### Fixed
- Perform lookups for locations that already exist but aren't verified
- Retry location lookups if network errors occur
- Catch Kafka::BufferOverflow exceptions and retry for kafka producers
- Ensure role exists before rendering its abilities
- Default new permission abilities to empty array if none are found through
  manageable organizations
- Ensure notification profile exists before editing

## [3.6.4] - 2017-09-22
### Fixed
- Fix smx import organization inheritance
- Fix network type dashboard chart

## [3.6.3] - 2017-09-21
### Changed
- Use Twilio to send SMS commands to cellular devices
- Increase worker counts for AVIEW & ARMT
- Add Gateway import filter for AVIEW & ARMT

### Fixed
- Fix uclinux imports (broken in 3.6.1)

## [3.6.2] - 2017-09-19
### Changed
- Add ARMT support for changing device sites
- Add ARMT support for changing device organizations (except Gateways)
- Auto-approve new device certificates. Deprecate the revoked certificates
  admin page.
- Sync newly imported Gateway device configurations with Service Manager
- Move Ruby on Rails production logging to it's own file
- Set production log level to info instead of debug
- Track IMEI/IMSI for Gateway cellular modems
- Remove "CoS" label from WAN Utilization Chart

### Fixed
- Catch exceptions in probe consumer thread and retry connecting to HornetQ
- Fix Gateway probes bug introduced by multi-modem support
- Fix how metrics are written to Druid (introduced in 3.6.1)

## [3.6.1] - 2017-09-01
- Update gateway configuration tab to show model/system inherited properties
- Use ExecJS + Node as the javascript runtime
- Fix notation of bytes transmitted/received columns from kilo-bytes
  to megabytes
- Display updated_at timestamp for gateway tunnels
- Fixed error when saving long APNs or device DNS servers processed
  from device events.
- Fixed Device.with_modem scope so it doesn't return duplicate device records
- Remove discontinued gateway devices once a day at 11:30 PM instead
  of with each import
- Add support for concurrent imports

## [3.5.1] - 2017-08-08
- Fix contact permissions for ARMT and allow for controller users
- Update uClinux configuration javascript library to the latest
- Add feature flag for smx synchronization
- Added support for 633x-MX models

## [3.5.0] - 2017-07-27
- Persist site id, location, and contact changes to smx for Gateway devices
- Allow only a single contact for Gateway devices
- Store and display Class of Service for Gateway devices
- Store and display modem speed tests reported by uClinux devices
- Display expanded configuration by default for Gateway devices
- Display `updated_at` timestamps for networks and tunnels
- Add CLI tool for managing VIG imports
- Add rake task for batch updating Gateway management hosts
- Move smx consumer services into the webapp
- Move report logging to it's own file at `log/reports.log`
- Move smx logging to it's own file at `log/service_manager.log`
- Fix map cluster image loading
- Fix field formatting for Gateway network configurations
- Fix availability report issue for monthly time range

## [3.4.0] - 2017-06-30
- Increase gateway command http ping timeout from 5 to 30 seconds to
  allow low-latency responses

## [3.3.1] - 2017-04-24
- Fix exceptions for cellular devices that don't have any utilization metrics
- Fix configuration compilation
- Add support for 6355-SR and 5401-RM models

## [3.3.0] - 2017-04-06
- Fix torquebox bug where exceptions are thrown if torquebox_init.rb isn't found
- Force HTTP request uri and headers to use UTF-8 encoding
- Fix versiononomy exception for invalid firmware versions
- Fix Netflow/Elasticsearch query error
- Fix scoping issues with certain dashboard charts
- Display number of devices affected when deleting a configuration or data plan
- Add production 6350-SR serial range
- Fix device configuration field order on new and edit to match show
- Fix ARMT login race condition that can throw
  ActiveRecord::RecordNotUnique exceptions
- Refactor device associations to fix duplication and stale issues
- Fix 500 error for a device's show page when it has invalid network ip entries
- Scope configuration linked devices to the configuration's organization
- Let users with the ability to modify a configuration assign devices to it
- Only show outbound tunnels for Gateway commands
- Refactor Distinguished Name parsing for device certificate authentication

## [3.2.0] - 2017-03-07
- Group configuration "Create" button is disabled when no firmware is selected
- Fix Cross Account logic to use correct method when logging in to ARMT
- Fix Device Events export exception when no search params are given
- Fix device show exception when it's configuration points to an
  invalid firmware
- Use queues for imports instead of topics
- Fix issue where multiple copies of the same object are shown in data tables
- Fix ambiguous queries when sorting by created_at, updated_at
- Rename configurations controller to legacy configurations
- Change X-UA-Compatible to IE=11 for ARMT
- Change operations metrics to use DataDog for AVIEW
- Add device model and firmwares support to the admin panel

## [3.1.1] - 2017-02-09
- Fix device certificate strategy namespace for cdn parsing (AS-470)
- Fix error caused by malformed json being sent to save device configurations
- Add validator function to support acc-config.js
- Install imagemagick to fix logo uploads

## [3.1.0] - 2017-02-07
- Add usage api for a single device (AS-376)
- Add usage api for list of devices (AS-419)
- Add devices api filter by organization (AS-364)
- Add devices api field limiter (AS-365)
- Add devices api support for only returning devices that have changed
  in a given timeframe (AS-363)
- Add React on Rails for frontend development (AS-379)
- Allow serial numbers to be changed (AS-330)
- Upgrade rails to 4.2.7.1 (fixes CVE-2016-6316 and CVE-2016-6317)
- Parse Service Manager configuration updates (AS-45)
- Limit IPSec tunnel options to outbound values only (AS-401)
- Add Single Sign-On support to ARMT for AT&T Business Center users (AS-433)
- Update naming for rollback command hash keys (AS-466)
- Update error message for rollback commands (AS-645)
- Update acc-config.js to the latest version
- Fix long running import email when an error occurs
- Fix reverse proxy issue with trailing dots in the hostname
- Disable metrics seed task due to high resource utilization
- Upgrade newrelic gem to 3.18
- Retry getting import and report files if not found
- Add import log and redirect all import results to it

## [3.0] - 2016-11-29
- Support for U110 Gateway device model.
- Support for the 6350-SR Cellular device model.
- Display rollback firmware version for a VPN Gateway on its device details page
- Add **Rollback** and **Rollback Check** remote commands to revert
  the firmware on a VPN Gateway.
- Track and display client hostnames and domain names in netflow details.
- Renamed labels on charts and graphs in the **Netflow** tab on the
  device's details page.
- Removed redundant downtime line in the bar graph of an Availability
  report. Bar graph now shows a simpler uptime percentage graph over time.
- Allow serial numbers to be updated.

## [2.15.0] - 2016-07-22
### Added
- creating a group configuration sets compiled settings
- saving device configurations compiles child configurations
- show inheritance links when an individual configuration is saved
- handle inherited and overridden configuration values
- add override and inherit to firmware and grace period
- change device group config link from view to edit
- update compiled settings on group configuration
- only show group configurations in drop downs
- add ability to set initial value on updated select2 dropdown
- create device configuration save template
- changing a device group configuration updates form
- populate device configuration tab
- create device configuration tab
- save device configuration route and action
- create feature switch for individual configurations
- save changes made to a device's configuration
- add is configurable helper method to device
- center device configuration form warning and settings

### Fixed
- engine translations
- individual config settings show and save
- Only include devices connected to a VIG for reports
- save device configuration tests for JRuby
- do not set individual configuration to group configuration
- tests in JRuby
- Save available location details from SMx importer even if we do not
  perform location lookup (AV-2540).
- scope device lookup to current user
- 'alpha' devices are not configurable
- configuration settings merge using js compiler
- prevent attempting to show zombie notifications in the database
- device configuration changes parent config as a child
- Adjusted uClinux network event process to handle priority key
  included in firmware 16.6.66 and above (AV-2538)
- update a gateway with a configuration
- prevent javascript error when updating a device configuration
- total client netflow usage only uses client field
- netflow package to install new plugin on upgrade
- ARMT translations
- device events logstash upgrade removes edn if installed
- update logstash-device-events package

### Removed
- Unnecessary and error causing admin_panel render

### Refactor
- do not remove override settings from child
- missed a space
- load config_compiler.js via Rails assets
- rename device presenter individual configuration helper method
- create standalone device configuration compiler js

### Chore
- Cleaned up method for creating formatted address from SMx importer details

### Test
- fix failing notifications index export feature spec

### Docs
- Fixed typo in 2.14 release notes
- Updated 2.14 release notes based on feedback from Cyndy
- Moved note concerning config model to match location on VPN Gateawy
  details page
- Removed ARMT-only release notes from 2.14 notes.
- Added release notes for version 2.14

[Keep a Changelog]: http://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: http://semver.org/spec/v2.0.0.html
[Unreleased]: https://bitbucket.org/accelecon/aview/branches/compare/HEAD%0Dv3.7.6
[3.7.6]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.6%0Dv3.7.4
[3.7.4]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.4%0Dv3.7.3
[3.7.3]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.3%0Dv3.7.2
[3.7.2]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.2%0Dv3.7.1
[3.7.1]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.1%0Dv3.7.0
[3.7.0]: https://bitbucket.org/accelecon/aview/branches/compare/v3.7.0%0Dv3.6.11
[3.6.11]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.11%0Dv3.6.10
[3.6.10]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.10%0Dv3.6.9
[3.6.9]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.9%0Dv3.6.8
[3.6.8]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.8%0Dv3.6.7
[3.6.7]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.7%0Dv3.6.6
[3.6.6]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.6%0Dv3.6.5
[3.6.5]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.5%0Dv3.6.4
[3.6.4]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.4%0Dv3.6.3
[3.6.3]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.3%0Dv3.6.2
[3.6.2]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.2%0Dv3.6.1
[3.6.1]: https://bitbucket.org/accelecon/aview/branches/compare/v3.6.1%0Dv3.5.1
[3.5.1]: https://bitbucket.org/accelecon/aview/branches/compare/v3.5.1%0Dv3.5.0
[3.5.0]: https://bitbucket.org/accelecon/aview/branches/compare/v3.5.0%0Dv3.4.0
[3.4.0]: https://bitbucket.org/accelecon/aview/branches/compare/v3.4.0%0Dv3.3.1
[3.3.1]: https://bitbucket.org/accelecon/aview/branches/compare/v3.3.1%0Dv3.3.0
