# Accelerated View Operations

Nifty information for interacting with the application

[TOC]

## Application CLI Tool

The application user has a bin utility for running useful operational commands. To view the options type `aview` or `aview --help`:


```
$ aview --help

  Acclerated View Manager

  Usage: aview [OPTIONS]

  Application options:
    --console                 Start a rails console session
    --tail-log <lines>        Tail the JBoss log (<lines> is optional)
    --deploy                  Deploys the latest release
    --prune-releases <keep>   Prunes the releases dir (<keep> is optional)
    --symlink-shared-dirs     Symlinks shared dirs (public, log, etc)
    --create-dirs             Create required directories
    --load-settings           Load the custom settings.yml

  VIG options:
    --import-vigs             Runs a VIG import
    --load-vig-file           Loads the custom vig file
    --install-vig-file <file> Install the given vig file

  Database options:
    --migrate                 Run all migrations (db, seed, etc)
    --db-migrate              Only run database migrations
    --seed-migrate            Only run seed migrations
    --tasks-migrate           Only run task migrations
    --load-db-config          Load the database configuration

  TorqueBox options:
    --torquebox-deploy        Deploy torquebox descriptor
    --torquebox-undeploy      Undeploy torquebox descriptor
    --torquebox-console       Run the JBoss AS7 CLI
    --sync-gems               Adds the app gems to the torquebox jruby

  Common options:
    --version                 Print the current aview version
    --verbose                 Increase verbosity
    -h, --help                Print this message and exit
```

## Process Manager

Supervisord manages the aview processes. To check if it's running:

```
$ ps aux | grep supervisord
aview     2218  0.0  0.0 206684 12492 ?        Ss   00:06   0:55 /usr/bin/python /opt/aview/.local/bin/supervisord --configuration /opt/aview/etc/supervisord.conf
```

To start:

```
$ aviewd
```

To shutdown:

```
$ aviewctl shutdown
```

### Services

`aviewctl` is the control interface to the aview process manager. Run `aviewctl help` to see the list of commands:

```
$ aviewctl help

default commands (type help <topic>):
=====================================
add    clear  fg        open  quit    remove  restart   start   stop  update
avail  exit   maintail  pid   reload  reread  shutdown  status  tail  version

```

To view the status of the aview services run `aviewctl status`:

```
$ aviewctl status
influxdb                         RUNNING   pid 22161, uptime 1 day, 2:27:48
jboss                            RUNNING   pid 3581, uptime 5:01:49
logstash_device_events           RUNNING   pid 10786, uptime 22:18:14
logstash_netflow                 RUNNING   pid 10735, uptime 22:18:49
pdns                             RUNNING   pid 22159, uptime 1 day, 2:27:48
```


## Common Uses

### Manage the application service

Use these to start/stop/restart the jboss application server that runs the application.

##### Starting

```
$ aviewctl start jboss
jboss: started
```

##### Stopping
```
$ aviewctl stop jboss
jboss: stopped
```

##### Restarting
```
$ aviewctl restart jboss
jboss: stopped
jboss: started
```

### Update Settings

Each deployment has a local file for server-dependent settings, located at `$AVIEW_DIR/etc/settings.yml`. After you make changes to the file you will need to load it into the currently deployed application with:

```
$ aview --load-settings
2016-06-28T16:09:00-0400   INFO: loading app configuration
2016-06-28T16:09:00-0400   INFO: copying app config from /opt/aview/etc/settings.yml
```

Restart the application to pick up the changes:

```
$ aviewctl restart jboss
```

### Update SMX Import File Path

The setting for the **SMX Import File** path is in `$AVIEW_DIR/etc/settings.yml`, under the `### Data files` section:

```
### Data files
data_files:
  -
    name: smx_netgate_list
    type: smx
    location: ~/armt_device_list.tab
    action: sync
```

Load the updated settings:

```
$ aview --load-settings
```

Restart the application to pick up the file path changes:

```
$ aviewctl restart jboss
```

### Update VIG Import File

Place the VIG spreadsheet anywhere on the server with read permissions for the application user. Install the spreadsheet with:

```
$ aview --install-vig-file vig_file.xlsx
```

Sync the updated VIG spreadsheet with the database:

```
$ aview --import-vigs
```

### Application Console

Open a [rails console][rails-console], useful for inspecting the state of the application and database.

```
$ aview --console
irb(main):001:0> Settings.reverse_proxy.enabled?
=> true
irb(main):002:0> Device.count
   (2.0ms)  SELECT COUNT(*) FROM `devices`
D, [2016-06-28T15:19:08.994000 #1442407170] DEBUG -- AVIEW:    (2.0ms)  SELECT COUNT(*) FROM `devices`
=> 54
```

### Database Migration

Changes to the SQL database are handled by migrations provided by the application. If there are pending migrations or hotfix changes that need to be applied, you can run them with:

```
$ aview --db-migrate
```


## Logs

All logs are located in `$AVIEW_DIR/log`

### JBoss Application

```
$ tail -f $AVIEW_DIR/log/jboss.log
```

### Device Events

```
$ tail -f $AVIEW_DIR/log/device_events.log
```

## Deployments

Deployments are stored in the `$RELEASES_DIR` folder:

```
$ ls $RELEASES_DIR
20160622140740  20160623095606  20160628133516
```

The currently deployed application is symlinked to `$AVIEW_DIR/current`:

```
$ ls -la $AVIEW_DIR | grep current
lrwxrwxrwx   1 aview aview        34 Jun 28 13:43 current -> /opt/aview/releases/20160628133516
```


## Support

If you run into any issues or have any questions about aview operations please feel free to reach out to the Accelerated View Development Team at `aview@accelerated.com` - we are all copied on this list.

[rails-console]: http://guides.rubyonrails.org/command_line.html
