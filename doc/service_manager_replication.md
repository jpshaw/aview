Service Manager Replication
===========================

AT&T’s Service Manager maintains configuration of the gateway line of
devices (8200, 8300, U110). ARMT now interacts with Service Manager so
that certain pieces of the configuration can be handled indirectly
through ARMT. This is a 2-way replication effort that strives to keep
the ARMT database in sync with Service Manager.

Process
-------

### Inbound

Service Manager sends updates to `smx_inbound` via a raw socket. The
inbound service then converts the received binary data into json and
puts it onto one of three topics - inbound, internet-routes, or
cascaded-networks. Network configuration and device info (site id,
contact, and location) go on the inbound topic. Internet Routes and
Cascaded Networks go on their own respective topics.

There are 4 services in the aview repository that pick up these Service
Manager changes from their respective Kafka topics. These services find
or create the appropriate records from the ARMT database, then updates
them as needed.

*Note: The services used to be run via Torquebox but are now run as
independent services via Supervisord ([branch] yet to be merged).*

*Note: It is important to ensure that these ARMT database changes do not
trigger replication back to Service Manager since the changes were
triggered by Service Manager in the first place.*

### Outbound

#### Synchronous - (CN, IR, and device network)

Changes made in ARMT for cascaded networks, internet routes, and device
networks go straight to `smx_outbound` synchronously via HTTP using the
swagger client generated from the `smx_outbound` repository. The
outbound service then sends the data to Service Manager, returning any
errors incurred.

#### Asynchronous - (site id, contact, location)

Changes in device info (site id, contact, location) from ARMT are saved
to the database then mapped to Service Manager format (using the
`device_datamapper` gem) and put onto the outbound Kafka topic. The
updates are then picked up by the `smx_outbound_device_service` which
forwards them to `smx_outbound` via HTTP. When the outbound service gets
the update request, it passes it on to Service Manager using the
`service-manager-jruby` gem.

### Location Notes

Changes to location are complicated because of our lookup service. When
a change occurs to a location, we check to see if we have a validated
instance of that address in the database. If we do, we use that id. If
we do not, we put the location on a Kafka topic for an asynchronous
lookup from Google. When the lookup returns, we have a new latitude and
longitude to save to the ARMT database and replicate back to Service
Manager.

Repositories
------------

-   [aview] - frontend of ARMT, also contains the code for the [armt
    services]
-   [smx-inbound-mri] - first stop coming in from Service Manager
-   [smx-outbound-device-consumer] - changes to the individual device
    (site id, contact, location) are picked up from the outbound Kafka
    topic then shipped off to `smx_outbound`.
-   [smx-outbound] - last stop on the way out to Service Manager
-   [service-manager-jruby] - (gem) library that `smx_outbound` uses to
    talk to Service Manager - encapsulates the smx.jar
-   [device-datamapper] - (gem) library that converts Service Manager
    fields into ARMT fields and back.

ARMT Services
-------------

`smx_inbound_device_service` - inbound topic

`smx_inbound_cn_config_service` - cascaded-networks topic

`smx_inbound_ir_config_service` - internet-routes topic

`smx_inbound_network_config_service` - inbound topic

`smx_outbound_device_service` - outbound topic

`lookup_service` - for locations only

Misc.
-----

**Device** - *Individual* device data - site id, location, mgmt tunnel,
etc.

**Network** - device network data (ie. DHCP/LAN) for system, model,
device

**IR** - Internet Routes for system, model, device

**CN** - Cascaded Networks for system, model, device

  [branch]: https://bitbucket.org/accelecon/aview/branch/smx-microservices
  [aview]: https://bitbucket.org/accelecon/aview
  [armt services]: #armt-services
  [smx-inbound-mri]: https://bitbucket.org/accelecon/smx-inbound-mri
  [smx-outbound-device-consumer]: https://bitbucket.org/accelecon/smx-outbound-device-consumer
  [smx-outbound]: https://bitbucket.org/accelecon/smx-outbound
  [service-manager-jruby]: https://bitbucket.org/accelecon/service-manager-jruby
  [device-datamapper]: https://bitbucket.org/accelecon/device-datamapper
