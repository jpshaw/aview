#Bower 

## Setup
1. Install [Node](https://nodejs.org/en/)
2. Install [Bower](http://bower.io/)
3. Install [Bower Installer](https://github.com/blittle/bower-installer)

## Install a Package
1. Add Package to `bower.json`. 
Bower uses the `bower.json` file to track dependencies and specify configuration options (similar to the Gemfile for Bundler). The preferred way to do this is to run the below command, the `-S` option tells Bower to add the package to the `bower.json` dependencies.  
`bower install <package>#<version> -S`

2. Run `bower prune` to ensure your `bower_components` directory only contains packages that are being used in the application.

3. Run `bower-installer`.
This will copy only the main files from the installed packages at `bower_components` into our asset path at `vendor/assets/components`. The reason we do this step is to keep only the necessary files in source control from each package instead of the entire repository.
There are many configuration options which can be specified, including the ability to [overwrite the main files](https://github.com/blittle/bower-installer#overriding-main-files) from the package. 

4. Add assets to `application.js` and `plugins.css`.
The easiest way to do this is to copy the paths from running `bower-installer`. In fact, it might be useful to grep for the package you are installing, something like this `bower-installer | grep jquery-ui` which will yield results like:  
```
jquery-ui : /Users/andreleblanc/sandbox/aview_mri/vendor/assets/components/jquery-ui/jquery-ui.css
jquery-ui : /Users/andreleblanc/sandbox/aview_mri/vendor/assets/components/jquery-ui/jquery-ui.js  
```  
From these results you copy the path after `vendor/assets/components` excluding the file extension (`jquery-ui/jquery-ui`).


### Gotchas
* If the main asset is minified (i.e. `.min.css`) you will need to reference the package with `.min` in `application.js` and `plugins.css`.
* If there are images referenced in the css, you will need to add an overwrite to `plugins/overwrites.scss` and reference the image with the `asset-url` helper.

## Remove a Package
1. Remove dependency from `bower.json`
2. Remove package from `vendor/assets/components`.
