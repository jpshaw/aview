# Deploying

An engine must be loaded and a build must be available in the rails
directory before deploying. Settings for deployment are located in the
engine settings file, located at
`engines/engine_name/config/settings.yml`. You **must** have SSH access
to the deployment server for it to work. Please see the System
Administrator for access.

To deploy a build, run:

``` bash
$ bundle exec rake deploy:staging
$ bundle exec rake deploy:production
```

Please be mindful when deploying to production.

Deployment does several things:

-   It will create a `releases` folder in the target's `$HOME`
    directory, if one does not already exist
-   The build is uploaded to the target server and placed in a new
    release folder, the name of which is the timestamp of the deployment
-   The build archive is exploded, and symlinks are created to folders
    in the target's `$HOME` directory
-   The database is migrated and seeded
-   Cleanup is performed on the server, and a certain amount of releases
    are kept around - the remaining are removed
-   If everything is successful, the `$HOME/current` symlink is updated
    to point to the latest release