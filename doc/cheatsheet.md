# Cheatsheet

Useful commands to use when interacting with aView, ARMT and AV-WOB.

## Deployment

Before deploying, please ensure the Jenkins jobs for the target are all green:

* http://aview-jenkins.digi.com/job/aview/job/staging/
* http://aview-jenkins.digi.com/job/armt/job/staging/

### Setup

#### Nix

Ensure the nix channels point to the new build server. Check the nix channels:

```bash
$ nix-channel --list
aview-nixpkgs http://nix.accns.com/channels/aview-nixpkgs-unstable
nixpkgs http://nix.accns.com/channels/nixpkgs-unstable
```

If the host is `nix.accns.com`, change the channels to use `aview-nix.digi.com`:

```bash
nix-channel --remove nixpkgs
nix-channel --remove aview-nixpkgs
nix-channel --add http://aview-nix.digi.com/channels/nixpkgs-unstable nixpkgs
nix-channel --add http://aview-nix.digi.com/channels/aview-nixpkgs-unstable aview-nixpkgs
```

#### Proxy

You'll need a proxy server with access to digi's network in order to install packages on aview or armt.

##### AVIEW

Since peak10 has direct access to bebop-001 (10.250.0.85), we only need to set the proxy env variable. First ssh to the target server:

```bash
ssh $host
```

Then set the http proxy:

```bash
export http_proxy="http://127.0.0.1:9888/"
```

Add https to grab packages from cache.nixos.org:

```bash
export https_proxy="http://127.0.0.1:9888/"
```

##### ARMT

To install on armt, you must first ensure that the server you are ssh from has a proxy running. For this example we'll use bebop-001, which is running `tinyproxy`.

First, determine the local and remote ports you'll use for tinyproxy, as well as the local host, and set the remote port forward string:

```bash
local_proxy_host=localhost
local_proxy_port=8888
remote_proxy_port=9888
remote_port_forward="$remote_proxy_port:$local_proxy_host:$local_proxy_port"
```

Now create the ssh tunnel with reverse port forwarding, where `$host` is the ssh config name of the target you want to deploy to:

```bash
ssh -NfR $remote_port_forward $host
```

Next, ssh to the target server:

```bash
ssh $host
```

Then set the http proxy:

```bash
export http_proxy="http://127.0.0.1:9888/"
```

Add https to grab packages from cache.nixos.org:

```bash
export https_proxy="http://127.0.0.1:9888/"
```

Note: The port used above is the `$remote_proxy_port`.

I wrapped this logic in a script on bebop-001 as my user, named `armt-nix-tunnel`. For example, here's how to connect to tampa labs and update the nix channels:

```bash
[bebop-001:~] > armt-nix-tunnel tampa-labs
Last login: Thu Mar 29 13:20:47 2018 from 10.20.30.20
[t2smx1c2@APP-DB anms-apps]$ export http_proxy="http://127.0.0.1:9888/"
[t2smx1c2@APP-DB anms-apps]$ export https_proxy="http://127.0.0.1:9888/"
[t2smx1c2@APP-DB anms-apps]$ nix-channel --update
downloading Nix expressions from ‘http://aview-nix.digi.com/channels/nixpkgs-unstable//nixexprs.tar.bz2’...
downloading ‘http://aview-nix.digi.com/channels/nixpkgs-unstable//nixexprs.tar.bz2’... [6558/9370 KiB, 3256.2 KiB/s]
downloading Nix expressions from ‘http://aview-nix.digi.com/channels/aview-nixpkgs-unstable//nixexprs.tar.bz2’...
downloading ‘http://aview-nix.digi.com/channels/aview-nixpkgs-unstable//nixexprs.tar.bz2’... [1591/2145 KiB, 1585.3 KiB/s]
downloading Nix expressions from ‘http://aview-nix.digi.com/channels/ted-lilley//nixexprs.tar.bz2’...
downloading ‘http://aview-nix.digi.com/channels/ted-lilley//nixexprs.tar.bz2’... [1813/2145 KiB, 1800.4 KiB/s]
unpacking channels...
created 7 symlinks in user environment
```

How to connect to each environment:

```bash
Usage: armt-nix-tunnel [southfield|birmingham|hayward|tampa-labs]
```


### AVIEW Staging (Peak 10)

When deploying the webapp, run this command on the target app server:

```bash
upgrade_webapp
```

It is just a wrapper around these commands:

```bash
avpkg channels update
avpkg update aview-webapp
avapp deploy
```

For any other packages, install with:

```bash
avpkg install <prefix>.<name>
```

Where `prefix` is either `<nixpkgs>` or `<aview-nixpkgs>`, depending on which channel the package belongs to.

### ARMT Staging (Tampa Labs)

When deploying the webapp, run this command on the target app server:

```bash
upgrade_webapp
```

For any other packages, install with:

```bash
avpkg install <prefix>.<name>
```

Where `prefix` is either `<nixpkgs>` or `<aview-nixpkgs>`, depending on which channel the package belongs to.

## Operations

### JBoss CLI

Note: You must be on a server with JBoss running.

```bash
$HOME/.nix-profile/jboss/bin/jboss-cli.sh
```

Then connect:

```bash
The controller is not available at localhost:9999: java.net.ConnectException: JBAS012144: Could not connect to remote://localhost:9999. The connection timed out: JBAS012144: Could not connect to remote://localhost:9999. The connection timed out
[disconnected /] connect
```

Print the status of all queues:

```bash
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default:read-children-resources(child-type="runtime-queue", recursive=true, include-runtime=true)
```

JBoss CLI Documentation: https://developer.jboss.org/wiki/CommandLineInterface

### Rails console

```bash
avapp console
```

### List processes

```bash
avctl status
```

### Stop a specific process

```bash
avctl stop jboss
```

### List installed packages

```bash
nix-env -q
```

### Check for available packages

```bash
nix-env -qa | grep "name"
```
