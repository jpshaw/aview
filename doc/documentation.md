# Documentation

All documentation is stored in the `gitbook` directory in the project.
These files are compiled together with the gitbook tool to create a
documentation booklet, which is hosted at
<https://aview-docs.accns.com/>.

We also maintain a list of servers used for the AView application, including
development, staging, and production, on the IT Operations sharepoint site:

## Beta/Staging

https://accelecon.sharepoint.com/itops/schema/_layouts/15/start.aspx#/Lists/ip_schema_vlan4/AllItems.aspx

## Production

https://accelecon.sharepoint.com/itops/schema/_layouts/15/start.aspx#/Lists/ip_schema_vlan_10/AllItems.aspx
