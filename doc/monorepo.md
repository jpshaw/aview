# Monorepo

## Motivation

* Changes to features are tracked in single code repository, making them much
  easier to follow.
* Common libraries can be tested and deployed without having to worry about git
  or ssh credential management.
