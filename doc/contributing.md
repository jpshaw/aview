# How to contribute to Accelerated View

We'd love for you to contribute to our source code and to make Accelerated View even better than it is today! This document contains the guidelines we'd like you to follow:

## Questions

If you have questions about Accelerated View, please direct these to the [Accelerated View Mailing List][aview-mailing-list] or [JIRA][jira-issues]. We are also available on the Accelerated HipChat.

## Issues and Bugs

* **Ensure the issue was not already reported** by searching the [JIRA Open Bugs][jira-open-bugs].

* If you're unable to find an open issue addressing the problem, please open a new one. Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring.

## Feature Requests

You can request a new feature by submitting an issue to [JIRA][jira-issues]. If you would like to implement a new feature then consider what kind of change it is:

* **Major Changes** that you wish to see in the project should be discussed first on our
[aview dev mailing list][aview-mailing-list] so that we can better coordinate our efforts, prevent duplication of work, and help you to craft the change so that it is successfully accepted into the project.
* **Small Changes** can be crafted and submitted to the [Bitbucket Repository] as a Pull Request.

## Issue Template

* **Story**

    A description of the feature, enhancement or bug.

* **Reproduction** (If applicable.)

    Include how to reproduce the issue.

* **Resolution**

    A description of how the problem should be implemented or resolved.

* **Resources** (If applicable.)

    Include resources to increase understanding of task. e.g. screen shots, blog posts, previous issues. Each attachment, link, etc. should have a small description as well as a matching reference in its respective section.

* **Testing**

    A description of how to test the implementation or fix for the issue. This can include the unit/integration tests needed as well as steps for the QA tester to manually verify the fix.

* **Applicable sections of the issue template should be filled out as soon, and completely as possible.**

* **Story Points**

    An estimate of the effort required to complete the task.

    *  1  - 30 min
    *  2  - 1 hour
    *  4  - 2 hours
    *  8  - 4 hours
    *  16 - 1 day
    *  32 - 2 days
    *  48 - 3 days

## Git Commit Guidelines

Good commit messages serve at least three important purposes:

* To speed up the review process.

* To help us write good release notes.

* To help the future maintainers of aview, say five years into the future, find out why a particular change was made to the code or why a specific feature was added.

Structure your commit message like this (from: [http://git-scm.com/book/ch5-2.html][git-commit]):

> Short (50 chars or less) summary of changes

> More detailed explanatory text, if necessary. Wrap it to about 72 characters or so. In some contexts, the first line is treated as the subject of an email and the rest of the text as the body. The blank line separating the summary from the body is critical (unless you omit the body entirely); tools like rebase can get confused if you run the two together.

> Further paragraphs come after blank lines.

> Bullet points are okay, too

> Typically a hyphen or asterisk is used for the bullet, preceded by a single space, with blank lines in between, but conventions vary here

### DO

* Write the summary line and description of what you have done in the imperative mode, that is as if you were commanding someone. Start the line with "Fix", "Add", "Change" instead of "Fixed", "Added", "Changed".

* Always leave the second line blank.

* Line break the commit message (to make the commit message readable without having to scroll horizontally in `gitk`).

### DON'T

* Don't end the summary line with a period - it's a title and titles don't end with a period.

### TIPS

* If it seems difficult to summarize what your commit does, it may be because it includes several logical changes or bug fixes, and are better split up into several commits using `git add -p`.

## Workflow

**1) [Fork the project]**

**2) Create an issue branch**

```
$ git checkout -b AV-####
```

**3) Commit your changes**

**4) Test your branch**

```
$ git checkout -b develop
$ git push -f origin develop
```

This will [test](https://builds.accns.com/job/aview-development-tests/), [build](https://builds.accns.com/job/aview-development-build/) and [release](https://builds.accns.com/job/aview-development-release/) your changes to the developer environment located at [https://dev.accns.com](https://dev.accns.com).

**5) Create a new pull request**

Before submitting a pull request, please do these things:

-   Rebase your branch against staging (in order to create a
    fast-forward merge). Possibly wait for another PR if it is about to
    merge in.
-   Rebase your branch to ensure logically separate, cohesive commits
    and eliminate any typo/mistake revisions
-   Ensure all tests pass
-   In the pull request description, prefix with "AV-\#\#\#\#" to link
    to the JIRA issue
-   Analyze your code changes with [rubocop](http://batsov.com/rubocop/)

We use the de-facto standard [Ruby a coding style guideline]. It is also
encouraged to use [Rubocop], which will point out style issues, as well
as other possible problems.

When your code and commits are ready for review, create a [pull request](https://bitbucket.org/accelecon/aview/pull-requests/new) on Bitbucket.

**6) Staging / QA**

When merged, your code changes will be [tested](https://builds.accns.com/view/Staging/job/aview-staging-tests/), [built](https://builds.accns.com/view/Staging/job/aview-staging-build/) and [released](https://builds.accns.com/view/Staging/job/aview-staging-release/) to the staging environment located at [https://staging.accns.com](https://staging.accns.com).

  [aview-mailing-list]: mailto:aview@accelerated.com
  [jira-issues]: https://accelecon.atlassian.net/projects/AV/issues
  [jira-open-bugs]: https://accelecon.atlassian.net/issues/?filter=10800
  [Bitbucket Repository]: https://bitbucket.org/accelecon/aview
  [git-commit]: http://git-scm.com/book/ch5-2.html
  [Fork the project]: https://bitbucket.org/accelecon/aview/fork
  [Ruby a coding style guideline]: https://github.com/bbatsov/ruby-style-guide
  [Rubocop]: https://github.com/bbatsov/rubocop
