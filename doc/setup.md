# Setup

You will need to:

-   Install [git](http://git-scm.com/), a distributed version control system. Read the GitHub
    ["Set Up Git"](https://help.github.com/articles/set-up-git) article to learn how to use git.

*Some additional resources for familiarizing yourself with git:* \* [Pro Git ebook] \* [Try Git] \* [Git Immersion] \* [Think Like a Git]

-   Install [Ruby] and [JRuby]. We currently use MRI Ruby 2.0+ for
    development and JRuby 9.1.5.0 for production. Your operating system
    may already have them installed or offer them as a
    pre-built packages. You can check by typing `ruby -v` and `jruby -v`
    in your shell or console. Additionally, you may use a ruby version
    manager such as [RVM] or [chruby].

-   Update rubygems, the ruby system which manages Ruby libraries.
    Run `gem update --system --no-document`.  (if you want cli
    documentation, drop the `--no-document`)

-   Install/update [Bundler], a Ruby dependency management tool, and
    [Rake].  Run
    
    `gem install --no-document -f bundler`
    
    `gem install rake -v '12.0.0'`
    
-   Install [MySQL], a database engine. Your operating system should
    have pre-built packages available for this. We currently use MySQL
    5.6 in production. You may want to create a development user.

-   For Ubuntu, install the development libraries for MySQL, XSLT and
    sqlite3.  You'll also need nodejs for asset compilation: `sudo
    apt-get install -y libxslt1-dev libsqlite3-dev libmysqlclient-dev
    nodejs`.  Some of these aren't absolutely necessary for Aview
    development (such as sqlite), but are good to have for Rails
    development in general.

-   Clone the source code. From your command line, run
    `git clone git@bitbucket.org:accelecon/aview.git`, which will create
    an `aview` directory with the source code. Change into this
    directory (run `cd aview`) and run the remaining commands from
    there.

-   Change to your desired version of ruby with rvm or chruby.  You can
    make this sticky by specifying your ruby version in a
    [.ruby-version] file in the project directory.  You may want to add
    `.ruby-version` to a [global gitignore].

-   We use the poltergeist javascript driver for our feature specs. This
    requires PhantomJS to be installed on your system prior to running `bundle install`.
    PhantomJS is a single binary executable that needs to be in your PATH
    when running feature specs. The download can be found at:
    https://github.com/teampoltergeist/poltergeist/tree/v1.8.1

-   Install Bundler-managed gems, (the actual libraries that this
    application uses, like Ruby on Rails) by running `bundle install`.
    This may take a long time to complete.

-   Enter your database config at `config/database.yml`.  You can copy the
    sample file from `config/database.yml.example`.  Instead of "production",
    create entries for "development" and "test" with the same settings except
    database name.  Customize the database names for each environment (e.g. end
    with "_dev" and "_test"). You'll need to match the user and password
    created when you installed MySQL.

-   If you haven't already created the databases in MySQL, run
    `rake db:create` (actually, run this prefixed by `bundle exec`.  For
    brevity, we'll drop the `bundle exec` from all rake commands.)

-   Run `rake db:migrate` and `rake seed:migrate` to initialize the database.

-   Seed an initial user by running `rake
    'app:create_root_user[test.user@accelerated.com,password]'`

*If you intend to run your code with torquebox:*

-   Follow the additional instructions in **[TorqueBox](http://torquebox.org/)**

[Pro Git ebook]: http://git-scm.com/book
[Try Git]: https://try.github.io/levels/1/challenges/1
[Git Immersion]: http://gitimmersion.com/
[Think Like a Git]: http://think-like-a-git.net/
[Ruby]: http://www.ruby-lang.org/
[JRuby]: http://jruby.org/
[RVM]: https://rvm.io/
[chruby]: https://github.com/postmodern/chruby
[Bundler]: http://bundler.io/
[Rake]: http://docs.seattlerb.org/rake/
[MySQL]: http://www.mysql.com/
[.ruby-version]: https://gist.github.com/fnichol/1912050
[global gitignore]: https://help.github.com/articles/ignoring-files/#create-a-global-gitignore
