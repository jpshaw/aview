# Building

Build can only be done with JRuby. Each build has two possible
environments: `staging` and `production`. The staging environment loads
like production, the difference being that it will display the git
revision and load any staging settings.

To build for an environment, run:

``` bash
$ bundle exec rake build:staging
$ bundle exec rake build:production
```

Building does several things:

-   Assets are precompiled
-   Gems are installed for deployment (without `development` and `test`)
-   A torquebox archive is created for the current environment and
    version

After a successful build you will see an archive file in the rails app
directory, such as `aview-staging-1.4.0.knob`. This file is required
when attempting to `deploy`.

If there is a problem with javascript precompilation, it may help to run
the `precompile_js` script.

```bash
$ ./scripts/precompile_js.rb
```
