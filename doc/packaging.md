# Nix Deployment Notes

## Introduction

Nix is a package manager which provides a repository of software
installable in a user directory.  Since the Aview application depends on
many packaged components and must be installable in a user directory
without special privileges, nix provides the opportunity to take
advantage of these repositories without having to recreate them
ourselves.

Up until this point, we have been creating rpm packages for the
components we need to install in our application directory.  Examples of
such packages include:

- Torquebox
- the Aview app itself
- the Java runtime environment
- the influxdb database
- the logstash log processor
- the pdns dns server
- the shellinabox terminal web ui
- the supervisord process manager

In addition, we use rpms to package file-only payloads such as our SNMP
MIB files and application certificates.

There are several issues with rpms, including:

- reproducibility - rpms cannot be used on our development systems to
provide the same environment as is seen in staging and production

- lack of ability to relocate - the rpms provided by the Red Hat
repositories almost uniformly are not relocatable, which prevents their
direct use for installation in a user directory.  This requires us to
extract and repackage them, which can be challenging in some cases (e.g.
java).  While this is still better than building from source, it's
nowhere near as good as being able to use those rpms off-the-shelf.

- pull deployment rather than push - since the yum tool cannot be used
in user-space, we are restricted to using the rpm command, which
requires the file to be local.  This requires a multi-step process of
downloading the file to a user workstation, copying it to the server,
logging into the server, changing user to the service user and finally
installing the software.

- inefficient - sometimes packages still have to be copied to
the target system, even if they are unchanged

- target environment-specific - rpms want to install as a particular
user and have different expectations of prerequisites being installed.
This requires us to build separate bundles for each deployment
environment.

While these are significant drawbacks, there are also advantages to rpms
which is why we use them, namely:

- targeted to the deployment platform - these packages are vetted for
our deployment environment and originate from Red Hat, a known quality
source

- strong capabilities of dependency specification, install, upgrade and
uninstall automation and post-install configuration

## Nix

Nix addresses most of these issues.

### Reproducibility

Nix packages, when implemented correctly, encapsulate all of the
necessary environmental dependencies of the software.  This means that a
Nix package is independent of the particular Linux distribution on which
it is installed.  So the same package will work for developer
workstations running Ubuntu as well as staging and production systems
running Red Hat.  All that is necessary is that the CPU architecture and
kernel capabilities match.

However, not all of our development environments run Linux.  Nix is able
to run on MacOS as well, though.  Because Nix packages are actually
build instructions rather than a particular binary output, the same
package can be designed with the instructions for both Linux and MacOS.
This means that the two environments can each run Nix and install the
same package, and receive the correct versions of the package meant for
that platform from the same package file.

### Ability to Relocate

Nix treats all package installations as user-oriented and does not rely
on root access to install software.  Ability to relocate is therefore
built-in.

### Push deployment

While the application software needs to be installed as the application
user, Nix does allow for push deployment of software via SSH.  This
allows a user to push our packages to the deployment server from their
own workstation so long as they are running Nix locally.  Because Nix
can be set up to allow multi-user management, with the application user
being the software store owner, this means you can push directly from
your workstation and still have the software be installed as the
application user's account, without needing to enable ssh access to the
application account (which is not allowed on most customer systems).

### Efficiency

When configured for push deployment, nix is able to query the remote
system prior to deployment and only deploy those packages which have
changed, making it highly efficient.

### Target independent

Nix does not allow system- or user-specific data in its packages, hence
it does not expect to be installed under a specific user.

Because composing packages from subpackages is trivial in nix, different
builds for different environments (ones that don't require specific
subpackages, for example) can be assembled easily out of parts which are
reusable.

## Nix installation

In order to work as described in a multiuser environment, there are some
special requirements above and beyond the basic nix installation.

First, the basic installation needs to be performed on two systems, a
user workstation and a target deployment system.

The user workstation can be any Linux or MacOS, but preferably Linux.
Windows is supported with cygwin 1.7.x or later but is not recommended.

See the [nix manual] for full details, but the basic method is as
follows.

### Install nix on the user workstation

Nix requires a path owned by its user, mounted at `/nix`.  For the
purpose of the application, we want its files all located under its home
directory to ensure that the disk space used is on the proper partition.
We'll follow this model for the user workstation installation as well.

To satisfy both of these requirements, first create a nix directory
(called the "nix store") under the user's home directory.  On the
workstation, it is suggested that you use the directory
`~/.local/opt/nix`.  Create the directory with the command `mkdir -p
~/.local/opt/nix`.

The installation on the user workstation will be a single-user
installation, the nix store directory does not need special permissions.
The multi-user installation on the deployment server will need other
permissions as well.

You'll also need a mount point at `/nix`.  Do `sudo mkdir /nix`.

Next, bind mount the user directory as `/nix` by editing `/etc/fstab`
with the command `sudoedit /etc/fstab` (or `sudo vim…`).  Add the
following lines and save:

```
# nix
/home/<user>/.local/opt/nix                  /nix            none    bind
```

Then mount the directory with the command `sudo mount -a`.  Verify that
it exists and has the proper permissions with `getfacl /nix`.  The
output should look as follows:

```
getfacl: Removing leading '/' from absolute path names
# file: nix
# owner: ted
# group: ted
user::rwx
group::rwx
other::r-x
```

Now we can install nix.  On the user workstation, run:

```
curl https://nixos.org/nix/install > install_nix && sh install_nix
```

See that the nix shell initializer has been added to your `.bashrc`,
then log off and log on for it to load.  You should be able to run
`nix-env` now.

### Install nox, the package management searcher, and nix-repl

On the user workstation, run:

```
nix-env -i nox
```

This will make the `nox` command available, which makes it possible to search for
nix packages by substring.

Also install `nix-repl`:

```
nox nix-repl
```

That's the first part.

### Install nix on the target system

On the target deployment system, you'll need to first install nix under
the application user.  Since the application user doesn't have sudo
privileges, you'll need to prep first from your own account.

We still need the `/nix` path, but this time we'll need it mounted from
a directory under the application user.  It is suggested to create a
`nix` subdirectory under the user home directory for this purpose.  If
the application user is `aview` and its directory is `/opt/aview`, then
from your account run:

```
sudo -u aview mkdir /opt/aview/nix`
```

You'll also need the mount point at `/nix`.  Do `sudo mkdir /nix`.

Next, bind mount the user directory as `/nix` by editing `/etc/fstab`
with the command `sudoedit /etc/fstab` (or `sudo vim…`).  Add the
following lines and save:

```
# nix
/opt/aview/nix                  /nix            none    bind
```

Then mount the directory with the command `sudo mount -a`.  Verify that
it exists and has the proper permissions with `getfacl /nix`.  The
output should look as follows:

```
getfacl: Removing leading '/' from absolute path names
# file: nix
# owner: aview
# group: aview
user::rwx
group::rwx
other::r-x
```

Now we can install nix.  Sudo to the application user and run:

```
curl https://nixos.org/nix/install > install_nix && sh install_nix
```

See that the nix shell initializer has been added to the user's
`.bashrc`, then exit and sudo again to reload.  You should be able to
run `nix-env` now.

### Install nox, the package management searcher

Still running as the application user, run:

```
nix-env -i nox
```

This will make the `nox` command available, which makes it possible to
search for nix packages by substring.

We'll skip installing `nix-repl` on the deployment system as it likely
will not be needed here.

### Set up supervisord and the nix-daemon

Since nix is set up owned by the application user, others will need to
talk to a daemon running under the application user in order to manage
nix.

The nix-daemon is already installed by the nix installer.  We only have
to configure and enable it to get it working.

The nix-daemon will run under supervisord, so we'll need to install that
first:

```
nox supervisor
```

Install the package named "python2.7-supervisor-3.1.1", or the latest
version that runs on python2.7, if available.

**NB**: if you have an existing set of Python environment variables such
as PYTHON_PATH, they may interfere with the operation of the new
supervisor.  It is recommended that you comment them out of your
shell initialization files and relogin/sudo.

Before we can use supervisord, it needs a patch to enable configuration
of the supervisord.conf file via an environment variable.  There will
eventually be a nix package (or "derivation") with this patch made
available by our repository.  Until then, you can save this patch on the
server:

```
diff --git a/nix/store/65sxjgxdf58nqj1rbvrcn1wcijclacyz-python2.7-supervisor-3.1.1/lib/python2.7/site-packages/supervisor/options.py b/nix/store/65sxjgxdf58nqj1rbvrcn1wcijclacyz-python2.7-supervisor-3.1.1/lib/python2.7/site-packages/supervisor/options.py
index 94bbf67..929b1a4 100644
--- a/nix/store/65sxjgxdf58nqj1rbvrcn1wcijclacyz-python2.7-supervisor-3.1.1/lib/python2.7/site-packages/supervisor/options.py
+++ b/nix/store/65sxjgxdf58nqj1rbvrcn1wcijclacyz-python2.7-supervisor-3.1.1/lib/python2.7/site-packages/supervisor/options.py
@@ -98,12 +98,15 @@ class Options:
         self.add(None, None, "h", "help", self.help)
         self.add("configfile", None, "c:", "configuration=")

-        here = os.path.dirname(os.path.dirname(sys.argv[0]))
-        searchpaths = [os.path.join(here, 'etc', 'supervisord.conf'),
-                       os.path.join(here, 'supervisord.conf'),
-                       'supervisord.conf',
-                       'etc/supervisord.conf',
-                       '/etc/supervisord.conf']
+        searchpaths = [
+          os.environ.get('SUPERVISORD_CONF', '/NOCONF'),
+          os.path.join(os.environ.get('XDG_CONFIG_DIR', '/NOCONF'), 'supervisord.conf'),
+          os.path.join(os.environ.get('HOME', '/NOCONF'), '.config', 'supervisord.conf'),
+          os.path.join(os.environ.get('XDG_CONFIG_DIR', '/NOCONF'), 'supervisord', 'supervisord.conf'),
+          os.path.join(os.environ.get('HOME', '/NOCONF'), '.config', 'supervisord', 'supervisord.conf'),
+          '/etc/supervisord.conf',
+          '/etc/supervisord/supervisord.conf'
+          ]
         self.searchpaths = searchpaths

     def default_configfile(self):
```

Save it as "supervisord.patch" under your account, then apply it with
the command:

```
cd /
sudo patch -p1 < ~/supervisord.patch
```

Now configure the SUPERVISORD_CONF environment variable as the
application user.  This should go in
`~aview/.config/aview/aview.d/supervisord.sh`, but if that directory
doesn't exist, then just set it in `~aview/.bashrc`:

```
export SUPERVISORD_CONF="/opt/aview/etc/supervisord.conf"
```

Run the same command in your shell so the variable is set in your
environment (again, this is as the application user).

You should also set this variable in your own account's `~/.bashrc` so
you can manage supervisord from there as well.

Now modify the directory which will contain the supervisord socket to
allow access for the admin group (at peak10, that's "prodadm").

As the application user:

```
cd ~
chmod -R g=u tmp
find tmp -type d -exec chmod g+s '{}' \;
setfacl -Rm g:prodadm:rwX,d:g:prodadm:rwx tmp
```

Now check the directory permissions with `getfacl tmp`.  They should
look like the following:

```
# file: tmp
# owner: aview
# group: aview
# flags: -s-
user::rwx
group::rwx
group:prodadm:rwx
mask::rwx
other::r-x
default:user::rwx
default:group::rwx
default:group:prodadm:rwx
default:mask::rwx
default:other::r-x
```

If there is an existing supervisord.conf, there may be a running
existing version of supervisord.  Check and if so, shut it down with the
command `aviewctl shutdown`.

If there was an existing supervisord.conf file, modify it to match the
following instructions.

If there isn't, however, create the `~aview/etc` directory if necessary
and run the following commands:

```
cd ~
mkdir etc
cd etc
echo_supervisord_conf > supervisord.conf
```

Edit the supervisord.conf file and modify it to match the lines as shown
here:

```
[unix_http_server]
file=/opt/aview/tmp/supervisord.sock
chmod=770

[...]

[supervisorctl]
serverurl=unix:///opt/aview/tmp/supervisord.sock

[include]
files=supervisor.d/*.conf
```

Create a `etc/supervisor.d` directory with `mkdir supervisor.d` and add
the following file as `nix-daemon.conf`:

```
[program:nix-daemon]
command=nix-daemon
environment=NIX_CONF_DIR=/nix/etc
stdout_logfile=%(here)s/../log/nix-daemon.log
stderr_logfile=%(here)s/../log/nix-daemon.log
```

Also create the `~aview/log` directory if it doesn't exist yet.

Before starting supervisord, we'll also need to configure nix-daemon.

```
cd /nix
mkdir etc
chmod g=u,g+s etc
setfacl -m g:prodadm:rwx,d:g:prodadm:rwx etc
chmod -R g=u var
find var -type d -exec chmod g+s '{}' \;
setfacl -Rm g:prodadm:rwX,d:g:prodadm:rwx var
```

While we're at it, we'll also configure permissions on the nox database:

```
cd /tmp
setfacl -m g:prodadm:rw nox.dbm
setfacl -m g:prodadm:rwx nox.dbm.*
```

Now paste the following into `/nix/etc/nix.conf`:

```
trusted-users = @prodadm
```

Now we're ready to start the nix-daemon.

Start supervisord with the command:

```
supervisord
```

Check the status of services with the command:

```
supervisorctl status
```

You should see something like the following:

```
nix-daemon                       RUNNING   pid 12752, uptime 0:00:23
```

### Configure your account for nix

From your account, run the following:

```
cd ~
ln -s /nix/var/nix/profiles/default .nix-profile
mkdir .nix-defexpr
ln -s /nix/var/nix/profiles/per-user/aview/channels .nix-defexpr
cp ~aview/.nix-channels .
```

Next, add the nix initialization script to your `.bash_profile`:

```
is_file() { [[ -f "$1" ]]; }

setup_nix() {
  local nixrc

  nixrc="/opt/aview/.nix-profile/etc/profile.d/nix.sh"
  is_file "${nixrc}" || return 0
  source "${nixrc}"
  export NIX_REMOTE="daemon"
}

setup_nix

umask 002 # to play nice with group directories
```

Log off and back on to get the changes from your `.bash_profile`.

Now test with `nix-env`.  You should get an error about not having
specified a command, but the fact that nix-env ran at all is what we
care about.

### Usage

Nix is now installed for use both by the application user as well as any
user who configures their account as above.

Any application installed from your user account will be immediately
available to the application user and all other users who share the nix
default profile, linked in their home directory as `.nix-profile`.

While the application user owns the nix store, your account will be able
to install software by communicating with the nix-daemon, which runs as
the application user and therefore can install software to the store.
Nix will not allow a group-owned store, so the daemon is the only way
for multiple users to be able to all install software.

Note that the application user does not need to be configured to use the
daemon since it can work directly on the store, as it would in a
single-user configuration.

While software can be installed as either your or the application's
user, the goal is for your account to be the primary means of
installation, for two reasons:

1. It's more convenient to install software without the extra step of
switching to the application user first.

2. Nix has a push model of preparing software for installation which
relies on being able to do nix operations over ssh.  The account has to
be the one on the receiving end of the ssh session, which is always a
user account.

The push model is not only convenient but also very efficient, since nix
can query the target system for already-installed components.  It can
then push only the necessary components to the remote server, saving
bandwidth and processing.  This means nix packages don't need to be
pre-built with only a subset of components in order to trim down upgrade
packages.  While there may still need to be some specifics for builds
targeting different systems (e.g. ARMT vs. AVWOB vs. Aview), the
differences can be limited to system-specific configuration files and
installation procedures rather than entirely separate artifact packages.

Pushing consists of two parts, the push and the install.

The push puts all of the package files on the target server, but does
not do the post-installation steps or put the package outputs on the
user's PATH.

The install, which must be done on the target system, checks for the
presence of the installation files, put there by the push so no need for
a download, and does the post-install steps, making the application
available to the users.

To push, the application must already be available on the source
machine, our user's workstation.  On that machine, run:

```
nix-copy-closure --to=<[user@]host> "$(which <program name>)"
```

A "closure" is a package along with all of its dependencies.  The
`$(which <program name>)` simply specifies the path to the package in
the local nix store, which is more easily written that way since nix
paths are extremely long.

Once the closure has been pushed, log onto the target server and install
the package with:

```
nix-env -i <package_name>
```

Any binaries provided by the package will then be available from the nix
bin dir on all nix users' PATHs.

[nix manual]: https://nixos.org/nix/manual/
