# TorqueBox

[TorqueBox](http://torquebox.org/) is used to run the application in production. It sits
upon the latest [JBoss Application Server](http://jbossas.jboss.org/) with JRuby, and is an
all-in-one solution that provides messaging, scheduling, services, load
balancing, and more.

### Install

There are two ways to get the torquebox server:

1.  Install the `torquebox-server` gem. It can be installed by running
    `gem install torquebox-server`.

2.  [Grab the latest binary] and follow the instructions for installing
    it by reading the [TorqueBox Getting Started Guide].

### Configuration

The torquebox configuration file is located at
`#{Rails.root}/config/torquebox.yml`. TorqueBox uses this file when
bringing up the application server with the correct runtime(s) and
environment. The `RAILS_ENV` used by rails is set with the application
key:

``` yaml
application:
  env: production
```

If you want to run in development, simply update the env:

``` yaml
application:
  env: development
```

All jobs, services, processors and messaging pipes loaded by the JBoss
server must be declared in `torquebox.yml`.

**Note:** it is recommended to comment out the jobs and services section
if you are developing with TorqueBox to reduce CPU/Memory usage.

For more information see the [Official TorqueBox Configuration].

### Running

Before starting the server you may want to set the following environment
variables:

``` bash
export JAVA_OPTS='-Xms1024m -Xmx2048m -XX:MaxPermSize=2048m -Dfile.encoding=UTF-8 -Djava.net.preferIPv6Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true'
```

``` bash
export JRUBY_OPTS='--dev'
```

``` bash
export REPORTS_DIR='/path-to-directory'
```

To run the application with TorqueBox, first ensure that it has been
deployed by running (you only need to do this once):

``` bash
$ torquebox deploy
```

To start the TorqueBox server:

``` bash
$ torquebox run
```

You will see the JBoss Application Server log output to the command
line. Once loaded, point your browser to `http://localhost:8080` to log
into the app.

More information about TorqueBox can be found in the [online manual].

## API

JBoss has a management CLI and HTTP API where you can interact with a running instance and perform useful operations. The ARMT JBoss package should have the management module enabled – if not it’ll need to be configured, and JBoss will need a restart to pick the changes up.
 
### Console
 
https://docs.jboss.org/author/display/AS72/Management+Clients#ManagementClients-RunningtheCLI
https://docs.jboss.org/author/display/AS72/CLI+Recipes
 
 
To open a console, run:
 
``` 
$HOME/.nix-profile/jboss/bin/jboss-cli.sh
```
 
Enter "connect" when prompted:
 
``` 
You are disconnected at the moment. Type 'connect' to connect to the server or 'help' for the list of supported commands.
[disconnected /] connect
[standalone@localhost:9999 /]
```
 
From here you can query the messaging subsystem. To list the queue names currently registered with HornetQ:
 
``` 
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default:read-children-names(child-type="runtime-queue")
{
    "outcome" => "success",
    "result" => [
        "jms.queue./local_queues/device_events",
        "jms.queue./queues/bulk_notifications",
        "jms.queue./queues/cellular_location",
        "jms.queue./queues/device_commands",
        "jms.queue./queues/firmwares_sync",
        "jms.queue./queues/immediate_notifications",
        "jms.queue./queues/imports",
        "jms.queue./queues/probe_results",
        "jms.queue./queues/probes",
        "jms.queue./queues/reports",
        "jms.queue./queues/scheduled_device_commands",
        "jms.queue./queues/torquebox/20170424220643/tasks/torquebox_backgroundable",
        "jms.topic./topics/imports",
        "sf.my-cluster.8566588f-edd6-11e6-8155-e70a2b01b870"
    ]
}
```
 
To get the message count for all queues:
 
```
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default/runtime-queue="*":read-attribute(name="message-count")
{
    "outcome" => "success",
    "result" => [
        {
           "address" => [
                ("subsystem" => "messaging"),
                ("hornetq-server" => "default"),
                ("runtime-queue" => "jms.queue./local_queues/device_events")
            ],
            "outcome" => "success",
            "result" => 0L
        },
        {
          # Other queues ...
        }
    ]
}
```
 
Note: The count is displayed as a “long”, hence the “L” after the number for each queue result.
 
To get the message count for a single queue:
 
``` 
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default/runtime-queue="jms.queue./local_queues/device_events":read-attribute(name="message-count")
{
    "outcome" => "success",
    "result" => 4L
}
```

To view metadata for all queues:
 
```
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default:read-children-resources(child-type="runtime-queue", recursive=true, include-runtime=true)
{
    "outcome" => "success",
    "result" => {
      # omitted
    }
}
```

To view metadata for a single queue:

```
[standalone@localhost:9999 /] /subsystem=messaging/hornetq-server=default/runtime-queue="jms.queue./local_queues/device_events":read-resource(recursive=true, include-runtime=true)
{
    "outcome" => "success",
    "result" => {
        "consumer-count" => 8,
        "dead-letter-address" => "jms.queue.DLQ",
        "delivering-count" => 3,
        "durable" => false,
        "expiry-address" => "jms.queue.ExpiryQueue",
        "filter" => undefined,
        "id" => 8453545978L,
        "message-count" => 3L,
        "messages-added" => 17055667L,
        "paused" => false,
        "queue-address" => "jms.queue./local_queues/device_events",
        "scheduled-count" => 0L,
        "temporary" => false
    }
}
```
 
You can run these commands in non-interactive mode by passing them to the cli script in single-quotes, like so:
 
```
$HOME/.nix-profile/jboss/bin/jboss-cli.sh --connect --commands='/subsystem=messaging/hornetq-server=default/runtime-queue="*":read-attribute(name="message-count")'
```
 
 
### HTTP API
 
https://docs.jboss.org/author/display/AS72/The+HTTP+management+API
 
The CLI output can be a bit tricky to parse if you’re trying to automate checking JBoss. Luckily the HTTP API supports JSON.
 
To get the message count for all queues:
 
```
curl -s "http://localhost:9990/management/subsystem/messaging/hornetq-server/default/runtime-queue/*/?operation=attribute&name=message-count&recursive&include-runtime&json.pretty" | jq '[ .[] | {(.address[2]."runtime-queue"): (.result) } ] | add'
```

[Grab the latest binary]: http://torquebox.org/download/
[TorqueBox Getting Started Guide]: http://torquebox.org/getting-started/3.1.1/
[Official TorqueBox Configuration]: http://torquebox.org/documentation/3.1.1/web.html#web-specific-config-in-descriptor
[online manual]: http://torquebox.org/documentation/3.1.1/
