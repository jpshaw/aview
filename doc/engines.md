# Engines

[Rails Engines] are used to load specific flavors of Accelerated View
(such as `aview` and `armt`) by hooking into the core rails application.
Engines are managed by running our custom `engine` rake command:

``` bash
$ rake engine
```

### Info

If no engine is loaded you should see an empty hash:

``` bash
$ rake engine:info
# => {}
```

If an engine is loaded you should see details about it. For example,
`aview`'s engine will look like:

``` bash
$ rake engine:info
{:engine=>"aview",
 :engine_path=>"/path/to/aview/engines/aview",
 :gem_path=>"engines/aview",
 :torquebox=>"/path/to/aview/config/torquebox.yml",
 :login=>
  "/path/to/aview/app/views/devise/sessions/_log_in_box.html.erb",
 :devise=>"/path/to/aview/config/initializers/devise.rb",
 :seed_migrations=>
  ["20140828095605_add_admin_panel_to_root_admin.rb",
   "20140909174710_add_view_admin_panel_ability_to_root_organization_admin_role.rb",
   "20141203210957_add_modify_logo_ability.rb"]}
```

### Loading

To load an engine, first check that it exists in the `engines` folder
and then run `rake engine:load[engine_name]`, for example:

``` bash
$ rake engine:load[aview]
Copying files ...
--> config/torquebox.yml
--> app/views/devise/sessions/_log_in_box.html.erb
--> config/initializers/devise.rb

Copying seed migrations ...
--> db/data/20140828095605_add_admin_panel_to_root_admin.rb
--> db/data/20140909174710_add_view_admin_panel_ability_to_root_organization_admin_role.rb
--> db/data/20141203210957_add_modify_logo_ability.rb
```

**Note:** the `torquebox.yml` file copied over is targeted for
`production`. See the **[TorqueBox Configuration]** section for more
information.

Engines can have their own seed migration files, which will copy over
when an engine is loaded. You must run `rake seed:migrate` after loading
an engine to ensure the database is seeded correctly for it.

If an engine is already loaded, running `rake engine:load[...]` will
complain with:

``` bash
Engine already loaded: aview

Please unload the current engine by running:
  $ rake engine:unload
```

You can automatically unload an existing engine by running the `reload`
task. For example, if `aview` is already loaded and you wish to load
`armt`, you can run:

``` bash
$ rake engine:reload[armt]
Removing /path/to/aview/config/torquebox.yml
Removing /path/to/aview/app/views/devise/sessions/_log_in_box.html.erb
Removing /path/to/aview/config/initializers/devise.rb
Removing /path/to/aview/db/data/20140828095605_add_admin_panel_to_root_admin.rb
Removing /path/to/aview/db/data/20140909174710_add_view_admin_panel_ability_to_root_organization_admin_role.rb
Removing /path/to/aview/db/data/20141203210957_add_modify_logo_ability.rb
Removing config /path/to/aview/config/engine.yml
Copying files ...
--> config/torquebox.yml
--> app/views/devise/sessions/_log_in_box.html.erb
--> config/initializers/devise.rb

Copying seed migrations ...
--> db/data/20140731140206_remove_old_abilities.rb
--> db/data/20140731142507_add_correct_roles_for_armt.rb
--> db/data/20140805190708_add_device_categories.rb
--> db/data/20140805203843_add_netbridge_firmwares.rb
--> db/data/20141010152538_ensure_root_permission_is_set.rb
--> db/data/20141014114410_add_device_configuration_abilities.rb
--> db/data/20141027185437_remove_rm_and_dc_devices.rb
```

### Unloading

To unload an engine you can simply run `rake engine:unload`, and it will
unload whatver engine is loaded:

``` bash
$ rake engine:unload
Removing /path/to/aview/config/torquebox.yml
Removing /path/to/aview/app/views/devise/sessions/_log_in_box.html.erb
Removing /path/to/aview/config/initializers/devise.rb
Removing /path/to/aview/db/data/20140731140206_remove_old_abilities.rb
Removing /path/to/aview/db/data/20140731142507_add_correct_roles_for_armt.rb
Removing /path/to/aview/db/data/20140805190708_add_device_categories.rb
Removing /path/to/aview/db/data/20140805203843_add_netbridge_firmwares.rb
Removing /path/to/aview/db/data/20141010152538_ensure_root_permission_is_set.rb
Removing /path/to/aview/db/data/20141014114410_add_device_configuration_abilities.rb
Removing /path/to/aview/db/data/20141027185437_remove_rm_and_dc_devices.rb
Removing config /path/to/aview/config/engine.yml
```
[Rails Engines]: http://guides.rubyonrails.org/engines.html
[TorqueBox Configuration]: torquebox.md#markdown-header-configuration
