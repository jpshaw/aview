# ARMT 17.6 Installation Steps

## Stop services
Stop jboss first

```bash
  avctl stop jboss
```

Check if jboss is still running. If so, `kill -9` it

```bash
  avapp jboss ps
```

Stop druid services, and `kill -9` if needed

```bash
  avctl stop druid-broker
  avctl stop druid-coordinator
  avctl stop druid-historical
  avctl stop druid-middleManager
  avctl stop druid-overlord
```

Stop the rest of the services

```bash
  avctl stop all
```


## Shutdown supervisord
```bash
  avctl shutdown
```

## Install 17.6 packages
```bash
  avpkg channels update
  avpkg install smx-inbound
  avpkg install smx-outbound
  avpkg install smx-outbound-device-consumer
  avpkg update aview-snmp-mibs
  avpkg update aview-nixpkgs.av
  avpkg update armt-webapp
```

### Need feedback
- How do we update supervisord? Hayward / Tampa are at 3.3.1, Southfield is at 3.2.2.
- Hayward / Tampa use the zk-server-3.4.9-1 package, but Southfield uses zookeeper-3.4.6. Should we change zookeeper on Southfield to match? How do we do that?

## Deploy ARMT web app
```bash
  AVIEW_ENGINE=armt avapp deploy
```

## Temporarily disable services from starting via supervisord
```bash
  mkdir -p $AVIEW_DIR/etc/supervisord-tmp
  mv $AVIEW_DIR/etc/supervisord/*.conf $AVIEW_DIR/etc/supervisord-tmp/
```

## Start supervisord
```bash
  avd
```

## Copy sync services back to supervisord
The `smx-inbound-*` services exist in the webapp codebase, and their supervisord scripts are installed via rake task.

```bash
  services=( zookeeper kafka smx-inbound smx-outbound smx-outbound-device-consumer )

  pushd $AVIEW_DIR/etc/
    for file in "${service_files[@]}"; do
      mv "supervisord-tmp/$file.conf" "supervisord/$file.conf"
    done
  popd
```

## Generate smx microservice configs
These services exist in the webapp codebase:

* smx-inbound-device-consumer
* smx-inbound-network-consumer
* smx-inbound-ir-consumer
* smx-inbound-cn-consumer

This command generates their supervisor configs and stubs a local config file for server-specific configurations.

```bash
  avapp exec rake smx:services:install
```

## Start sync services
```bash
  avctl update
```

## Generate sync file
This contains the list of devices and models to fetch

```bash
  RAILS_ENV=production avapp exec rake smx:create_sync_file
```

## Start synchronizing
```bash
  RAILS_ENV=production avapp exec rake smx:run_sync
```

## Go eat dinner
I think I'll have Chinese food

## Verify sync completed
There isn't a task to do this, we'll just read the logs and metrics in Datadog

## Start the rest of the services services

```bash
  pushd $AVIEW_DIR/etc/
    cp supervisord-tmp/*.conf supervisord/
  popd
  avctl update
```

## ????

## Profit!
