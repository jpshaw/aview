# Testing

The project uses the [parallel_tests](https://github.com/grosser/parallel_tests) gem to speed up spec testing. In order to use the gem you must add the following to the `test` section in your `database.yml` file:

```yaml
test:
  database: yourproject_test<%= ENV['TEST_ENV_NUMBER'] %>
```

Then export the number of processors you'd like to use:

```bash
export PARALLEL_TEST_PROCESSORS=4
```

## Create database(s)

```bash
bundle exec rake parallel:create
```

## Copy development schema (repeat after migrations)

```bash
bundle exec rake parallel:prepare
```

## Run tests for a specific file

No need to parallelize this, so we just use `rspec`

```bash
bundle exec rspec spec/models/device_spec.rb
```

## Run tests in parallel for a specific directory

```bash
bundle exec parallel_rspec spec/controllers/**/*_spec.rb
```
## Run tests in parallel for a set of directories

```bash
bundle exec parallel_rspec spec/controllers/**/*_spec.rb spec/models/**/*_spec.rb
```

## Run all tests in parallel

Note: The specs in `features` and `requests` may fail. See `TODO`.

```bash
bundle exec parallel_rspec spec/**/*_spec.rb
```

## Run CI tests

When a pull request is merged, CI will run unit tests:

``` bash
$ bundle exec rake test:ci
```

## TODO

* Add support for integration tests
