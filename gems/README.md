# Custom Gems

These modules represent separate domains of the application that should have
minimal interaction and knowledge outside their own scope. Making these modules
into gems helps enforce the scope limitation and allows for specs to be run
independently from the rest of the application.
