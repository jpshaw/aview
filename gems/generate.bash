#!/usr/bin/env bash
#
# Generate a new kraken gem
#
# See bundle options: https://bundler.io/v1.14/man/bundle-gem.1.html
#
# Example
#
#   ./gems/generate.bash foo
#   Creating gem 'kraken-foo'...
#   ...
#   Gem 'kraken-foo' created at /path/to/gems/kraken/foo
#
set -e
# set -x # Debug
shopt -s extglob
shopt -s globstar

# Configuration ################################################################
name="$1"
gem_name="kraken-$name"
gems_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
kraken_gems_dir="$gems_dir/kraken"
gem_dir="$kraken_gems_dir/$name"
bitbucket_pattern="bitbucket\.org\/accelecon\/aview"
bitbucket_url_pattern="\"https:\\/\\/$bitbucket_pattern\""

# Why perl? Because sed'ing in place on OSX can be painful, and perl's
# multiline string option is much easier to use than sed's.
#
# -0777 tell perl to slurp files whole
# -w    enable many useful warnings
# -i    edit file in place (without backup)
# -pe   run the given pattern
find_and_replace() {
  local pattern="$1"
  local files=("${@:2}")
  perl -0777 -w -i -pe "$pattern" "${files[@]}"
}

# Gem ##########################################################################
pushd $kraken_gems_dir >/dev/null
  bundle gem --no-mit $gem_name
popd >/dev/null

echo "Renaming gem directory to $gem_dir ..."
mv -f $kraken_gems_dir/$gem_name $gem_dir

echo "Removing gem git repository ..."
[ -d $gem_dir/.git ] && rm -rf $gem_dir/.git

echo "Removing 3rd party ci configs ..."
[ -f $gem_dir/.travis.yml ] && rm -rf $gem_dir/.travis.yml


# README.md ####################################################################
echo "Updating gem readme ..."
gem_readme="$gem_dir/README.md"

## Add bitbucket project and gem repository information
github_pattern="github\.com\/\[USERNAME\]\/$gem_name"
find_and_replace "s/$github_pattern/$bitbucket_pattern/" $gem_readme
find_and_replace "s/GitHub/Bitbucket/g" $gem_readme
find_and_replace "s/rubygems\.org/rubygems\.accns\.com/g" $gem_readme


# Gemspec ######################################################################
echo "Updating gemspec ..."
gemspec="$gem_dir/$gem_name.gemspec"

## Remove blank line at the top
first_line=$(grep -m1 '' $gemspec | xargs) # use xargs to strip blanks
[ -z "$first_line" ] &&
  tail -n +2 $gemspec > $gemspec.tmp &&
  mv -f $gemspec.tmp $gemspec

## Change gem push host to rubygems.accns.com
public_push_pattern="# Prevent pushing.*pushes\.\"\n  end"
private_push_pattern=`cat <<EOF
# Prevent pushing this gem to rubygems\.org\.
  spec\.metadata\[\"allowed_push_host\"\] = \"http:\/\/rubygems\.accns\.com\/\"
EOF
`
find_and_replace "s/$public_push_pattern/$private_push_pattern/is" $gemspec

## Set summary & description to empty strings
find_and_replace "s/%q\{.*\}/\"\"/g" $gemspec

## Set homepage to bitbucket url
find_and_replace "s/\"TODO:.*repo URL.*\"/$bitbucket_url_pattern/g" $gemspec

## Change double quotes to single
find_and_replace "s/\"/\'/g" $gemspec

## Fix "\x0" split
find_and_replace "s/'\\\x0'/\"\\\x0\"/" $gemspec


# Gemfile ######################################################################
echo "Updating gemfile ..."
gemfile="$gem_dir/Gemfile"
find_and_replace "s/git_source.*/gemspec\n/s" $gemfile


# Ruby files ###################################################################
echo "Updating ruby files ..."
find_and_replace "s/\"/\'/g" $gem_dir/**/*.rb
find_and_replace "s/\"/\'/g" $gem_dir/Rakefile
find_and_replace "s/\"/\'/g" $gem_dir/bin/console


# Done #########################################################################
echo "Gem '$gem_name' created at $gem_dir"
