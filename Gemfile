source 'https://rubygems.org'

source 'https://gemstash.accns.com/private' do
  gem 'kraken-logger', '~> 0.3'
  gem 'kraken-message-bus', '~> 0.3'
  gem 'kraken-geolocation', '~> 0.2'
  gem 'kraken-replicator', '~> 0.4'
  gem 'smx-rest-client', '~> 0.8'
  gem 'device-datamapper', '~> 0.25'
  gem 'voltage_client', '~> 1.1'
  gem 'smx_identity_client', '~> 1.3'
  gem 'operations-metrics', '~> 0.2'
  gem 'metrics_service', '~> 1.1'
  gem 'syslog_ruby', '~> 0.0.4', require: false
  gem 'snmp4jruby', '~> 0.0.21', platform: :jruby
end

# Rails
gem 'rails', '~> 4.2.10'
gem 'puma', '~> 3.11', '>= 3.11.4', platform: :mri
gem 'activerecord-jdbcmysql-adapter', '~> 1.3', '>= 1.3.24', platform: :jruby
gem 'activerecord-nulldb-adapter', '~> 0.3.9'
gem 'activerecord-import'
gem 'mysql2', '~> 0.4.10', platform: :mri
gem 'request_store', '~> 1.4', '>= 1.4.1'
gem 'silencer', '~> 1.0', '>= 1.0.1'

# Assets
gem 'sass-rails', '~> 5.0', '>= 5.0.7'
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.7'
gem 'uglifier', '~> 4.1', '>= 4.1.10'
gem 'non-stupid-digest-assets', '~> 1.0', '>= 1.0.9'
gem 'execjs', '~> 2.7.0' # Used to compile configurations using javascript
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.4'

# Attributes
gem 'virtus', '~> 1.0', '>= 1.0.5'
gem 'enumerize', '~> 2.2', '>= 2.2.2'

# API
gem 'apipie-rails', '~> 0.5.8'
gem 'versionist', '~> 1.7'

# Auditing
gem 'paper_trail', '~> 9.0', '>= 9.0.2'

# Authentication
gem 'bcrypt', '~> 3.1', '>= 3.1.12'
gem 'devise', '~> 3.5'
# See here for which version to use with devise: http://bit.ly/1c07nRS
gem 'devise-token_authenticatable', '~> 0.4.0'
gem 'devise-multi-radius-authenticatable', '~> 0.1.2', require: false

# Authorization
gem 'pundit', '~> 1.1'

# Date & Time
gem 'chronic_duration', '~> 0.10.6'

# Error handling
gem 'exception_notification', '~> 4.2', '>= 4.2.2'

# Excel
gem 'axlsx', '~> 3.0.0.pre'
gem 'roo', '~> 2.7', '>= 2.7.1'
gem 'nokogiri', '~> 1.8', '>= 1.8.2'

# File uploading
gem 'carrierwave', '~> 1.2', '>= 1.2.2'
gem 'mini_magick', '~> 4.8'
gem 'remotipart', '~> 1.4', '>= 1.4.2'

# Frontend
gem 'webpacker', '~> 3.5'
gem 'react-rails', '= 2.2.1'
gem 'lodash-rails', '~> 4.17', '>= 4.17.4'
gem 'will_paginate', '~> 3.1', '>= 3.1.6'
gem 'breadcrumbs_on_rails', '~> 3.0', '>= 3.0.1'
gem 'highcharts-rails', '~> 6.0', '>= 6.0.3'
gem 'jbuilder', '~> 2.7'
gem 'simple_form', '~> 3.5', '>= 3.5.1'
gem 'bootstrap-tagsinput-rails'
gem 'summernote-rails'
gem 'jquery-turbolinks'
gem 'momentjs-rails', '>= 2.9.0'
gem 'moment_timezone-rails'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'

# Geolocation
gem 'geocoder', '~> 1.4', '>= 1.4.8'

# Hierarchies
gem 'closure_tree', '~> 6.6'

# Message bus processing
gem 'ruby-kafka', '0.5.0' # 0.5.1+ requires kafka 10.1.x (we have 10.0.1)

# Monitoring
gem 'newrelic_rpm', '~> 5.1', '>= 5.1.0.344'
gem 'dogstatsd-ruby', '~> 3.3'

# Network
gem 'ipaddress', '~> 0.8.3'
gem 'addressable', '~> 2.5', '>= 2.5.2'
gem 'httparty', '~> 0.16.2'
gem 'friendly_id', '~> 5.2', '>= 5.2.4'
gem 'http', '~> 3.3'
gem 'ipaddr_extensions', '~> 1.0.0', require: false
gem 'savon', '~> 2.12'

# Parsing
gem 'mac_address', '~> 0.0.4'

# Rails enhancements
gem 'hashie', '~> 3.5', '>= 3.5.7'
gem 'retryable_record', '~> 0.3.0'
gem 'interactor-rails', '~> 2.2'
gem 'seed_migration', '~> 1.2', '>= 1.2.3'
gem 'protected_attributes', '~> 1.1', '>= 1.1.4'
gem 'rake-task-migration', '~> 1.0.2'
gem 'rack-utf8_sanitizer', '~> 1.5'

# Ruby enhancements
gem 'memoist', '~> 0.16.0'

# Settings
gem 'config', '~> 1.6', '>= 1.6.1'
gem 'rails-settings-cached', '~> 0.4.0'

# State machine
gem 'micromachine', '~> 3.0'

# Versioning
gem 'versionomy', '~> 0.5.0'

gem 'googleauth'

group :development do
  gem 'awesome_print', '~> 1.8'
  gem 'better_errors', '~> 2.4'
  gem 'binding_of_caller', '~> 0.8.0', platform: :mri
  gem 'fix-db-schema-conflicts', '~> 3.0', '>= 3.0.2'
  gem 'letter_opener', '~> 1.6'
  gem 'web-console', '~> 3.3', platform: :mri
end

group :development, :test do
  gem 'rspec-rails', '~> 3.7', '>= 3.7.2'
  gem 'rspec-html-matchers', '~> 0.9.1'
  gem 'timecop', '~> 0.9.1'
  gem 'faker', '~> 1.8', '>= 1.8.7'
  gem 'parallel_tests'
end

group :test do
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
  gem 'shoulda-callback-matchers', '~> 1.1', '>= 1.1.4'
  gem 'capybara', '~> 3.1'
  gem 'database_cleaner', '~> 1.7'
  gem 'selenium-webdriver', '~> 3.12'
  gem 'poltergeist', '~> 1.18'
  gem 'factory_bot_rails', '4.10.0'
  gem 'webmock', '~> 3.4', '>= 3.4.1'
  gem 'vcr', '~> 4.0'
  gem 'test_after_commit', '~> 1.1'
  gem 'simplecov', '~> 0.16.1'
  gem 'codeclimate-test-reporter', '~> 1.0.0'
end

group :torquebox do
  gem 'torquebox', '~> 3.2', platform: :jruby, group: [:development, :production, :staging, :beta]
  gem 'torquebox-core', '~> 3.2', platform: :jruby, group: :test
  gem 'torquebox-server', '~> 3.2', platform: :jruby, group: :development
  gem 'torquebox-rake-support', '~> 3.2', platform: :jruby
  gem 'torquebox-no-op', '~> 3.2', platform: :mri
end

# Load engine
config_file = 'config/engine.yml'
if File.exists?(config_file)
  require 'yaml'
  config = YAML.load_file(config_file)

  if config && config[:engine] && config[:gem_path]
    gem config[:engine], path: config[:gem_path]
  end
end
