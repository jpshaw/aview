class CellularReport < Report

  include CellularHelper


  def initialize(report_record=nil)
    super(report_record)

    categories
  end


  private

  def categories
    # @categories is used internally in Report#device_models, by device_models, in report.rb.
    @categories ||= DeviceCategory.where(name: [DeviceCategory::LEGACY_CELLULAR_CATEGORY,
                                                DeviceCategory::CELLULAR_CATEGORY,
                                                DeviceCategory::DIAL_TO_IP_CATEGORY,
                                                DeviceCategory::REMOTE_MANAGER_CATEGORY,
                                                DeviceCategory::GATEWAY_CATEGORY])
  end


  def generate_data
    report_record.devices.find_each(batch_size: DEVICE_BATCH_SIZE) do |device|
      initialize_device_results(device)
      process_device_events(device)
    end

    update_report_record_stats_headers
  end


  def initialize_device_results(device)
    results[device] = intervals_template
  end


  def intervals_template
    template  = {}
    interval  = start_time

    begin
      # Initialize the interval hash with Hash.new(0). This will enable us to use += without the
      # need of worrying to initialize the key/value pair first later on.
      template[interval] = Hash.new(0)
    end while (interval += interval_seconds) < (end_time - interval_seconds)

    template
  end


  # Get the initial event right outside of start time bounds for a given event type. This allows
  # devices to have statistics, even if they don't have any events of the given type within the
  # defined bounds.
  def initial_event_of_type(device, type_flag)
    initial_event = initial_device_event_for_flag(device, type_flag)
    down_event    = initial_down_event(device)

    if initial_event && down_event && down_event.created_at > initial_event.created_at
      initial_event = nil
    end

    initial_event
  end


  # Get last device event for the given flag before the time range.
  def initial_device_event_for_flag(device, flag)
    initial_events(device)
      .where('device_events.raw_data like ?', "%#{flag}%")
      .first
  end


  # Get last down event for the device before the time range.
  def initial_down_event(device)
    initial_events(device)
      .where('device_events.uuid_id in (?)', device_down_uuid_ids)
      .first
  end

  def device_up_uuid_ids
    @device_up_uuid_ids ||= get_device_event_uuids_of_type("UP")
  end


  def device_down_uuid_ids
    @device_down_uuid_ids ||= get_device_event_uuids_of_type("DOWN")
  end

  def get_device_event_uuids_of_type(type)
    uuid_ids = []
    categories.each do |category|
      uuid_ids << DeviceEventUUID
                    .find_by_code("EventUUIDS::#{category.class_name.upcase}_#{type}".constantize)
                    .try(:id)
    end
    return uuid_ids
  end


  # Get events for the device before the time range sorted descending.
  def initial_events(device)
    device
      .events
      .where('device_events.created_at < ?', start_time)
      .order('device_events.created_at desc')
  end


  def process_device_events(device)
    # Get the initial event value (before time bounds) for the device, the events, set the current
    # start and end times to the beginning of the time range we need to operate on and extract the
    # device's intervals.
    current_value       = initial_value(device)
    events              = events_for_device(device)
    current_start_time  = start_time
    current_end_time    = start_time
    intervals           = results[device].keys

    events.find_each(batch_size: EVENT_BATCH_SIZE) do |event|
      # Update the current end time with the latest event time.
      current_end_time = event.created_at

      # UP event.
      if device_up_uuid_ids.include? event.uuid_id

        # On an UP event the current value will either be a valid value or the default value (from a
        # DOWN event). Either case we log the current seconds before the UP event, we clear the
        # current value for the next event to update it and set the new start time slot.
        if current_value.present?
          update_intervals(device, current_value, intervals, current_start_time, current_end_time)

          current_value       = nil
          current_start_time  = event.created_at
        else
          # Do nothing. This would be a consecutive UP event and we only need to keep track of the
          # start time at the first of the consecutive UP events.
        end

      # DOWN event.
      elsif device_down_uuid_ids.include? event.uuid_id

        # If there is a current value we log its seconds until the DOWN event and we set the current
        # value to the default and its start time with this event.
        if current_value.present?
          update_intervals(device, current_value, intervals, current_start_time, current_end_time)

          current_value       = default_value
          current_start_time  = event.created_at

        # If there is no current value the prior event was an UP event. We need to set the current
        # value with the default until a new regular event occurs.
        else
          current_value = default_value
        end

      # Regular event.
      else

        # If there is no current value the prior event was an UP event. Update current value.
        if current_value.blank?
          current_value = event_value(event)

        # If there is a current value and the new value is not the same we need to log the seconds
        # for the current value and set up the new one.
        else
          new_value = event_value(event)

          # Process change only if there is really a change.
          if new_value != current_value
            update_intervals(device, current_value, intervals, current_start_time, current_end_time)

            current_value       = new_value
            current_start_time  = event.created_at
          else
            # Do nothing. Same value so updating the end time before the case statement is all we
            # need.
          end
        end
      end
    end

    # Process remaining time since last event. For beginning time use either first interval time if
    # no events or the last calculated start time otherwise.
    begin_time    = events.empty? ? intervals.first : current_start_time
    current_value = default_value if current_value.blank?  # Last event was an UP event.

    update_intervals(device, current_value, intervals, begin_time, intervals.last + interval_seconds)

  end


  def events_for_device(device)
    device_events_query_for_device(device)
      .where(
        "device_events.raw_data like ? or device_events.uuid_id in (?)",
        "%#{report_type_flag}%",
        device_up_uuid_ids + device_down_uuid_ids
      )
  end


  def update_intervals(device, value, intervals, begin_time, finish_time)
    running_interval  = find_interval_before(intervals, begin_time)
    running_interval  = intervals.first unless running_interval.present?
    running_time      = begin_time < running_interval ? running_interval : begin_time

    while running_interval <= intervals.last
      next_interval = running_interval + interval_seconds

      if finish_time < next_interval
        seconds = finish_time - running_time
      else
        seconds = next_interval - running_time
      end

      results[device][running_interval][value] += seconds

      running_interval  = next_interval
      running_time      = next_interval

      return if running_time > finish_time
    end
  end


  # TODO: This version of JRuby does not implement yet the `bsearch` method in Array or Range.
  #       When `bsearch` becomes available replace the existing code with a call to a new method
  #       (i.e.: find_interval_after(intervlas, time)) containing the following line and adjust code
  #       accordingly:
  #         interval = intervals.bsearch { |interval| interval > time }
  # NOTE: `bsearch` is good at finding =>, not <, so plan accordingly for changes.
  def find_interval_before(intervals, time)
    # Already sorted, don't need to sort before reversing.
    intervals.reverse.each do |interval|
      return interval if interval <= time
    end

    # Default to not found.
    nil
  end


  def summarize_results_by_interval
    intervals_hash = intervals_template

    results.each do |_, intervals|
      intervals.each do |interval, value_seconds|
        value_seconds.each do |value, seconds|
          intervals_hash[interval][value] += seconds
        end
      end
    end

    intervals_hash
  end

end
