
class DeviceProbeResult

  attr_accessor :status, :oids, :device_id

  def initialize(results = {})
    @device_id = 0
    @status = :error
    @oids = {}
  end

end
