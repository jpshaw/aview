# TODO: Refactor this to use Rails 4 cache apis
class Api::RateLimiter
  extend ActiveModel::Callbacks

  attr_accessor :user

  delegate :api_request_limit, :api_time_window, to: :user

  define_model_callbacks :increment, only: :before

  before_increment :fetch_cache

  def initialize(opts = {})
    @user = opts[:user]
  end

  def count
    cache.fetch(rate_key, expires_in: api_time_window) { 0 }.to_i
  end

  def increment
    run_callbacks :increment do
      cache.increment(rate_key, 1, expires_in: expires_in) unless max_requests_reached?
    end
  end

  # @return [Boolean] Whether or not the max request limit has been reached.
  def max_requests_reached?
    count >= api_request_limit
  end

  # The number of requests the user can make in the current interval before
  # reaching the rate limit.
  # @return [Integer] The number of api calls the user can make in the current interval.
  def remaining_calls
    max_requests_reached? ? 0 : api_request_limit - count
  end

  # @return [Time] The time until the rate limit resets.
  def expires_at
    Time.at(count_timeout + api_time_window).utc
  end

  private

  def cache
    Rails.cache
  end

  def rate_key
    "api:rate:#{user.id}"
  end

  def time_key
    "api:time:#{user.id}"
  end

  def expires_in
    current = Time.now.utc.to_i
    expires = expires_at.to_i
    expires > current ? (expires - current) : 0
  end

  def fetch_cache
    count &&
      count_entry &&
      count_timeout
  end

  def count_entry
    cache.send(:read_entry, rate_key, {})
  end

  def count_timeout
    cache.fetch(time_key, expires_in: api_time_window) { count_entry.instance_variable_get(:@created_at) }.to_i
  end
end
