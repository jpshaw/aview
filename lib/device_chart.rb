
class DeviceChart

  def initialize(device, params)
    @device = device
    @type   = params[:chart_type] || 'availability'
    @range  = params[:range] || 'last_day'
    @subtitle = ""

    @results = {}
  end

  def generate
    return if @device.nil?

    case @type
    when "availability"
      get_availability
    when "signal_strength"
      get_signal_strength
    when "network"
      get_network
    end

    self
  end

  def to_json
    @results.to_json
  end

  private

  def get_availability

    up          = []
    down        = []
    series      = base_series_for_range
    range_start = range_start_date
    ticks       = ticks_for_range

    checks = @device.events.where(:event_type => EventTypes::STATUS)
      .where("created_at >= ?", range_start).order("created_at asc").count(:group => group_for_range)


    # Add data counts
    checks.each do |date, count|

      up_pct = (((count*1.0)/(ticks*1.0))*100).round(2)

      if up_pct > 100
        up_pct = 100
      end

      down_pct = (100 - up_pct)

      # Find series for date
      series[date] = { :up => up_pct, :down => down_pct }

    end

    # Extract data
    unless series.blank?
      series.each do |date, point|
        up << point[:up]
        down << point[:down]
      end
    end

    @results = {
      :chart => { :type => 'area' },
      :credits => { :enabled => false },
      :exporting => { :enabled => Settings.charts.exporting },
      :colors => ['#910000', '#8bbc21'],
      :title => { :text => "Availability Report - #{range_title}" },
      :subtitle => { :text => "#{@device.series_name} #{@device.mac}" },
      :xAxis => {
        :type => 'datetime',
        :title => { :text => range_units }
      },
      :yAxis => {
        :title => { :text => "Availability" }
      },
      :tooltip => {
        :pointFormat => '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b><br/>',
        :shared => true
      },
      :plotOptions => {
        :area => {
          :stacking => 'percent',
          :lineColor => '#ffffff',
          :lineWidth => 1,
          :marker => { :enabled => false }
        }
      },
      :series => [
        {
          :name => 'Down',
          :pointInterval => interval_for_range,
          :pointStart => range_start.to_time.utc.to_i * 1000,
          :data => down
        },
        {
          :name => 'Up',
          :pointInterval => interval_for_range,
          :pointStart => range_start.to_time.utc.to_i * 1000,
          :data => up
        }
      ]
    }
  end

  def get_signal_strength

    range_start = range_start_date

    checks = @device.events.where(:event_type => EventTypes::STATUS)
      .where("raw_data like ?", '%dbm%').where("created_at >= ?", range_start).order("created_at asc")

    series = base_signal_series_for_range

    checks.each do |check|

      if _obj = check.raw_data
        if _obj.has_key?("modem")
          if _obj["modem"].has_key?("dbm")

            dbm = _obj["modem"]["dbm"].to_i

            dbm = -50 if dbm > -50
            dbm = -112 if dbm < -112

            percent = (((dbm + 112.0) * 100.0) / 62.0).round(2)

            if series[check.created_at.utc.to_date][check.created_at.utc.hour].nil?
              series[check.created_at.utc.to_date][check.created_at.utc.hour] = {}
            end

            series[check.created_at.utc.to_date][check.created_at.utc.hour][check.created_at.utc.min] = percent

          end
        end
      end

    end


    # Build series
    _series = []

    # each day
    # use date object + hour + minute to construct new time
    # to_i with value
    # value is (nil || {minute => value})
    series.each do |day, hours|

      hours.each do |hour, minutes|

        if minutes.nil?
          time = Time.utc(day.year, day.month, day.day, hour)
          _series << [time.to_i*1000 , nil]
        else

          minutes.each do |minute, percent|

            time = Time.utc(day.year, day.month, day.day, hour, minute)
            _series << [time.to_i * 1000, percent]

          end

        end

      end
    end


    @results = {
      :chart => { :type => 'area', :zoomType => 'x', :spacingRight => 20 },
      :credits => { :enabled => false },
      :exporting => { :enabled => Settings.charts.exporting },
      :colors => ['#8bbc21'],
      :title => { :text => "Signal Strength Report - #{range_title}" },
      :subtitle => { :text => "#{@device.series_name} #{@device.mac}" },
      :xAxis => {
        :type => 'datetime',
        :maxZoom => 3 * 3600000, # 1 day
        :title => { :text => range_units }
      },
      :yAxis => {
        :min => 0,
        :max => 100,
        :title => { :text => "Signal Strength (%)" }
      },
      :tooltip => {
        :pointFormat => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.1f}%</b><br/>',
        :shared => true
      },
      :plotOptions => {
        :area => {
          :fillColor => '#8bbc21',
          :lineColor => '#ffffff',
          :lineWidth => 1,
          :marker => { :enabled => false },
          :shadow => false,
          :states => {
            :hover => {
              :lineWidth => 1
            }
          },
          :threshold => nil
        }
      },
      :series => [
        :type => 'area',
        :name => 'Signal',
        :data => _series
      ]
    }
  end

  def get_network

    range_start = range_start_date

    checks = @device.events.where(:event_type => EventTypes::STATUS)
      .where("raw_data like ?", '%cnti%').where("created_at >= ?", range_start).order("created_at asc")

    networks = {}

    series = []

    checks.each do |check|

      if _obj = check.raw_data
        if _obj.has_key?("modem")
          if _obj["modem"].has_key?("cnti")

            cnti = "#{_obj["modem"]["cnti"]}".upcase

            unless networks.has_key?(cnti)
              networks[cnti] = 0
            end

            networks[cnti] += 1

          end
        end
      end

    end

    # Compile network statistics

    unless networks.blank?

      sum = networks.values.inject(:+) * 1.0

      networks.each do |network, count|

        type = case network
        when "LTE"
          "4G"
        when "HSDPA/HSUPA"
          "4G"
        else
          "3G"
        end

        name = "#{network} (#{type})"

        percent = (((count * 1.0)/(sum))*100.0).round(2)

        series << [name, percent]

      end

    end

    @results = {
      :chart => {
        :plotBackgroundColor => nil,
        :plotBorderWidth => nil,
        :plotShadow => false
      },
      :credits => { :enabled => false },
      :exporting => { :enabled => Settings.charts.exporting },
      :title => { :text => "Cellular Network Report - #{range_title}" },
      :subtitle => { :text => "#{@device.series_name} #{@device.mac}" },

      :tooltip => {
        :pointFormat => '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      :plotOptions => {
        :pie => {
          :allowPointSelect => true,
          :cursor => 'pointer',
          :dataLabels => {
            :enabled => true,
            :color => '#000000',
            :connectorColor => '#000000',
            :format => '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      :series => [
        :type => 'pie',
        :name => 'Network type',
        :data => series
      ]
    }
  end

  def base_series_for_range
    retval = {}

    seed = { :up => 0, :down => 100 }

    case @range
    when "last_day"

      i = 24.hours.ago.utc.hour

      # Seed
      retval[i] = seed

      22.times do
        if (i > 23)
          i = 0
        end
        retval[i] = seed
        i += 1
      end

    when "last_week"

      today = Date.today.to_date
      start = 7.days.ago.utc.to_date

      (start..today).each do |day|
        retval[day] =  seed
      end

    when "last_month"
      today = Date.today.to_date
      start = 30.days.ago.utc.to_date

      (start..today).each do |day|
        retval[day] =  seed
      end

    end

    retval
  end

  def base_signal_series_for_range

    retval = {}

    case @range
    when "last_day"

      start_time = 24.hours.ago.utc

    when "last_week"

      start_time = 7.days.ago.utc

    when "last_month"

      start_time = 30.days.ago.utc

    end


    _end_time = Time.now.utc
    _end_date = _end_time.to_date
    _start_time = start_time
    _start_date = _start_time.to_date

    # Get start hours

    (_start_date.._end_date).each do |day|

      i = 0
      limit = 23

      retval[day] = {}

      # start
      if day == _start_date
        i = _start_time.hour
      end

      if day == _end_date
        limit = (_end_time.hour - 1)

        if limit == -1
          limit = 1
        end

      end

      (i..limit).each do |n|
        retval[day][n] = nil
      end


    end

    retval
  end


  def group_for_range
    case @range
    when "last_day"
      "hour(created_at)"
    else
      "Date(created_at)"
    end
  end

  def ticks_for_range
    case @range
    when "last_day"
      2
    else
      48
    end
  end

  def interval_for_range
    case @range
    when "last_day"
      3600000
    when "last_week"
      86400000
    else
      86400000
    end
  end

  def range_start_date
    case @range
    when "last_day"
      24.hours.ago.utc
    when "last_week"
      7.days.ago.utc
    when "last_month"
      30.days.ago.utc
    when "last_3_months"
      3.months.ago.utc
    end
  end

  def range_title
    case @range
    when "last_day"
      "Last Day"
    when "last_week"
      "Last Week"
    when "last_month"
      "Last 30 Days"
    when "last_3_months"
      "Last 3 Months"
    end
  end

  def range_units
    case @range
    when "last_day"
      "Hours"
    when "last_week"
      "Days"
    when "last_month"
      "Days"
    when "last_3_months"
      "Days"
    end
  end

end
