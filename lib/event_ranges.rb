module EventRanges
  LAST_DAY   = 1
  LAST_WEEK  = 2
  LAST_MONTH = 3

  def self.select_list
    [
      ["Last Day",   LAST_DAY  ],
      ["Last Week",  LAST_WEEK ],
      ["Last Month", LAST_MONTH]
    ]
  end

end
