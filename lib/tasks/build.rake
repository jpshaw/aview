namespace :build do
  def build(target)
    Rails.env        = 'production'
    ENV['RAILS_ENV'] = 'production'
    ENV['NAME']      = "#{Engine.name}-#{target}-#{app_version}.knob"

    Engine.build
  end

  task :development do
    ENV['STAGING'] = 'true'
    build :development
  end

  task :staging do
    ENV['STAGING'] = 'true'
    build :staging
  end

  task :production do
    build :production
  end

  task :prepare do
    FileUtils.rm_rf "#{Engine.root_path}/.bundle"
    FileUtils.rm_rf "#{Engine.root_path}/public/assets"
  end
end
