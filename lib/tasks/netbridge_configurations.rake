namespace :netbridge_configurations do
  desc 'Synchronize heartbeat json data with heartbeat sql columns'
  task sync_heartbeat_data: :environment do
    NetbridgeConfiguration.find_each do |configuration|
      configuration.send(:set_heartbeat_fields)
      configuration.save
    end
  end
end

namespace :accelerated do

  namespace :netbridge_configurations do

    namespace :ippassthrough_conversion do

      desc "Converts values of ippassthrough attribute from string to boolean"
      task convert_to_boolean: :environment do
        puts "Converting ippassthrough to boolean values in netbridge configurations"
        NetbridgeConfiguration.find_each(batch_size: 500) do |config|
          next unless config.ippassthrough_old.present?
          value = case config.ippassthrough_old
          when '1', 'true'
            true
          when '0', 'false'
            false
          else
            nil
          end
          config.update_attributes(ippassthrough: value)
          puts "Configuration with id #{config.id} processed"
        end
        puts "Configurations processed"
      end

      desc "Converts values of ippassthrough attribute from boolean to string"
      task convert_to_string: :environment do
        puts "Converting ippassthrough values in netbridge configurations to strings"
        NetbridgeConfiguration.find_each(batch_size: 500) do |config|
          next unless config.ippassthrough.present?
          value = case config.ippassthrough
          when true, 'true'
            '1'
          when false, 'false'
            '0'
          else
            nil
          end
          config.update_attributes(ippassthrough: value)
          puts "Configuration with id #{config.id} processed"
        end
        puts "Configurations processed"
      end

    end
  end
end
