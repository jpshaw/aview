namespace :device_configurations do
  desc 'Synchronize heartbeat json data with heartbeat sql columns'
  task sync_heartbeat_data: :environment do
    DeviceConfiguration.find_each do |configuration|
      configuration.send(:set_heartbeat_fields)
      configuration.save
    end
  end
end

namespace :accelerated do

  namespace :device_configurations do

    desc "Associates device configurations to firmwares"
    task associate_firmwares: :environment do
      configs = []

      puts "Retrieving configurations"

      [DialToIp, RemoteManager].each do |device_type|
        device_type.all.each do |device|
          next unless device.configuration.present?

          unless configs.include?(device.configuration)
            puts "Configuration #{device.configuration.name} retrieved"
            configs << device.configuration
          end
        end
      end

      puts "Configurations retrieved"

      puts "Processing configurations"

      configs.each do |config|
        puts "Processing configuration with id #{config.id}"

        if schema = ConfigurationSchema.find_by(id: config.configuration_schema_id)

          description = JSON.parse(config.description)

          next unless description.has_key?('firmware') && description['firmware'].has_key?('version')

          firmware_version = description['firmware']['version']

          if firmware = DeviceFirmware.where(device_model_id: schema.model.id, version: firmware_version).first
            config.update_column(:device_firmware_id, firmware.id)
            puts "Device configuration with id '#{config.id}' associated to firmware with id #{firmware.id}"
          else
            puts "Firmware for device configuration with id '#{config.id}', model '#{schema.model.id}' and firmware version '#{firmware_version}' could not be found"
          end
        else
          puts "Schema for device configuration with id #{config.id} could not be found"
        end

        puts "Configuration with id #{config.id} processed"
      end

      puts "Configurations processed"
    end

    desc "Copies device configuration description field to the compiled_settings field"
    task populate_compiled_settings: :environment do
      completed_configurations = 0
      DeviceConfiguration.find_each do |configuration|
        configuration.compiled_settings = configuration.settings
        completed_configurations += 1 if configuration.save
        configuration.errors.full_messages.each { |error| puts "Error saving #{configuration.id}: #{error}" }
      end
      puts "Completed #{completed_configurations} out of #{DeviceConfiguration.count} device configurations"
    end
  end

end
