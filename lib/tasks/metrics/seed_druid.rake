require 'tasks/helpers/seed_druid'

namespace :metrics do
  desc 'Seed Druid with data from Influx'
  task seed_druid: :environment do
    Tasks::Helpers::SeedDruid.run
  end
end
