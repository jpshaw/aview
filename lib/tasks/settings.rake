namespace :settings do

  task :print => :environment do
    require 'json'
    puts JSON.pretty_generate Settings
  end

end
