namespace :events do

  task :destroy_events_with_message, [:message] => :environment do |t, args|
    if args[:message].blank?
      puts "Missing message, exiting without deleting anything"
      exit
    end
    puts "Removing events containing message (#{args[:message]})"
    events_removed = 0
    begin
      DeviceEvent.where("information like ?", "%#{args[:message]}%").find_each(batch_size: 1000) do |event|
        event.destroy
        events_removed += 1
      end
    rescue => e
      puts "Event cleanup task failed with error: #{e.message}"
    end
    puts "Finished. Removed #{events_removed} events."
  end

end
