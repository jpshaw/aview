namespace :migrations do

  def invoke(command)
    Rake::Task[command].invoke
  end

  task :user_name_migration do
    invoke 'users:migrate_names'
  end

  task :set_node_name do
    invoke 'devices:set_node_name'
  end

  task :migrate_notification_timestamps do
    invoke 'notifications:migrate_timestamps'
  end

  task :migrate_location_coordinates_to_decimal do
    invoke 'locations:migrate_coordinates_to_decimal'
  end

  task :acquire_cellular_locations do
    invoke 'cellular_locations:acquire'
  end

  task :user_fix_notification_enable do
    invoke 'users:disable_notifications_if_email_blank'
  end

  task :upcase_device_modem_cnti do
    invoke 'device_modems:upcase_cnti'
  end

  task :remove_gateway_lans do
    invoke 'devices:remove_gateway_lans'
  end

  task :populate_tunnel_server_associations do
    invoke 'tunnel_servers:create_associations'
  end

  task :populate_device_configuration_compiled_settings do
    invoke 'accelerated:device_configurations:populate_compiled_settings'
  end

  # TODO: Figure out a way to do this faster
  # task :migrate_influx_metrics_to_druid do
  #   invoke 'metrics:seed_druid'
  # end
end
