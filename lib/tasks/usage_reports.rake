namespace :reports do

  task :create_stats_report => :environment do
    require 'reports/application_stats_report'

    report = ApplicationStatsReport.new
    report.run
    report.save

    puts "Saved report to #{`pwd`.strip}/#{report.name}"
  end

end
