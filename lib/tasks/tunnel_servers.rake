namespace :tunnel_servers do
  desc 'Remove all tunnel server associations'
  task :clear_associations => :environment do
    GatewayTunnel.connected_to_vig.find_each do |tunnel|
      tunnel.send(:disassociate_from_tunnel_server)
    end
  end

  desc 'Create associations between active tunnels and tunnel servers'
  task :create_associations => :environment do
    GatewayTunnel.connected_to_vig.find_each do |tunnel|
      tunnel.send(:associate_with_tunnel_server)
    end
  end

  desc 'Synchronize associations between active tunnels and tunnel servers'
  task sync_associations: :environment do
    GatewayTunnel.connected_to_vig.where(tunnel_server_id: nil).find_each do |tunnel|
      tunnel.send(:associate_with_tunnel_server)
    end
  end

  desc 'Generate a report of stale associations between device tunnels and tunnel servers'
  task stale_associations_report: :environment do
    require 'csv'

    date = Time.now.strftime("%Y%m%d%H%M%S")
    file = "/tmp/stale_vig_associations_#{date}.csv"

    puts "Generating stale vig associations report: #{file}"

    CSV.open(file, 'wb') do |csv|
      csv << ['device_id', 'account', 'firmware', 'deployed', 'current', 'expected', 'stale']

      Gateway.preload(:organization).select('devices.id, devices.mac, devices.organization_id, devices.firmware, devices.is_deployed').find_each do |device|
        current  = device.tunnel_servers.to_a.sort
        expected = device.tunnels.connected_to_vig.map(&:tunnel_server).compact.uniq.sort

        next if current == expected
        stale = current - expected

        csv << [
          device.mac,
          device.organization.name,
          device.firmware,
          device.is_deployed,
          current.count,
          expected.count,
          stale.count
        ]
      end
    end

    puts "Done."
  end

  desc 'Run VIG Import job'
  task :import_vigs => :environment do
    VigImporter.new.run
  end
end
