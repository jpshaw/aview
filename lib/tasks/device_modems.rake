namespace :device_modems do
  desc 'Upcase all DeviceModem cnti values'
  task upcase_cnti: :environment do
    DeviceModem.find_each(batch_size: 100) do |modem|
      modem.save! # Upcase is in a before_save callback.
    end
  end
end
