namespace :reports do

  def reports_dir
    ENV['REPORTS_DIR'].presence || '/tmp'
  end

  def report_date
    Time.now.strftime("%Y%m%d%H%M%S")
  end

  def generate_csv_report(name, csv_map, &block)
    path = "#{reports_dir}/#{name}-#{report_date}.csv"
    puts "Generating report: #{path}"

    require 'csv'
    CSV.open(path, 'wb') do |csv|
      csv << csv_map.keys

      row = lambda do |object|
        csv << csv_map.values.map { |value| value.call(object) }
      end

      yield(row)
    end

    puts "Done."
  end

  desc 'Generate a basic device report for the given ORGANIZATION_NAME'
  task organization_devices: :environment do
    organization_name = ENV['ORGANIZATION_NAME'].presence
    raise 'Please set ORGANIZATION_NAME' unless organization_name

    organization = Organization.find_by(name: organization_name)
    raise "Organization not found: #{organization_name}" unless organization

    report_name = "#{organization.slug}-devices-report"

    csv_map = {
               mac: ->(device) { device.mac },
            serial: ->(device) { device.serial },
       config_name: ->(device) { device.try(:configuration).try(:name) },
      organization: ->(device) { device.organization_name },
           site_id: ->(device) { device.site_name },
         address_1: ->(device) { device.location.try(:address_1) },
         address_2: ->(device) { device.location.try(:address_2) },
              city: ->(device) { device.location.try(:city) },
            region: ->(device) { device.location.try(:region) },
       postal_code: ->(device) { device.location.try(:postal_code) },
           country: ->(device) { device.location.try(:country_name) },
    }

    generate_csv_report(report_name, csv_map) do |row|
      organization.devices.find_each do |device|
        row.call(device)
      end
    end
  end

  desc 'Generate a report of devices with a modem'
  task devices_with_modem: :environment do
    report_name = 'devices-with-modem-report'

    csv_map = {
               mac: ->(device) { device.mac },
            serial: ->(device) { device.serial },
        hw_version: ->(device) { device.hw_version },
      organization: ->(device) { device.organization_name },
            parent: ->(device) { device.organization.parent.try(:name) },
           site_id: ->(device) { device.site_name },
              imei: ->(device) { device.modem.imei },
             iccid: ->(device) { device.modem.iccid },
             phone: ->(device) { device.modem.number },
           carrier: ->(device) { device.modem.carrier },
      activated_at: ->(device) { device.activated_at },
      heartbeat_at: ->(device) { device.last_heartbeat_at },
    }

    generate_csv_report(report_name, csv_map) do |row|
      Device.with_modem.find_each do |device|
        row.call(device)
      end
    end
  end

  desc 'Generate a report of deployed devices with their hostname'
  task devices_with_hostname: :environment do
    report_name = 'devices-with-hostname-report'

    csv_map = {
               mac: ->(device) { device.mac },
            serial: ->(device) { device.serial },
      organization: ->(device) { device.organization_name },
           site_id: ->(device) { device.site_name },
          hostname: ->(device) { device.hostname },
        primary_ip: ->(device) { device.host },
      activated_at: ->(device) { device.activated_at },
      heartbeat_at: ->(device) { device.last_heartbeat_at },
    }

    generate_csv_report(report_name, csv_map) do |row|
      Device.where(is_deployed: true).find_each do |device|
        row.call(device)
      end
    end
  end

  desc 'Generate a report of VPN Gateway WAN change instances for the given ORGANIZATION_NAME'
  task organization_devices_wan_change: :environment do
    organization_name = ENV['ORGANIZATION_NAME'].presence
    raise 'Please set ORGANIZATION_NAME' unless organization_name

    organization = Organization.find_by(name: organization_name)
    raise "Organization not found: #{organization_name}" unless organization

    report_name = "#{organization.slug}-gateway-device-wan-changed"

    csv_map = {
                     mac: ->(event) { event.device.mac },
                  serial: ->(event) { event.device.serial },
            organization: ->(event) { event.device.organization_name },
                 site_id: ->(event) { event.device.site_name },
         wan_change_time: ->(event) { event.created_at },
      wan_change_message: ->(event) { event.information }
    }

    wan_change_event_uuid = EventUUIDS.const_get('gateway_wan_change'.upcase)
    wan_change_uuid_id    = DeviceEventUUID.find_by_code(wan_change_event_uuid).id

    generate_csv_report(report_name, csv_map) do |row|
      organization.devices.find_each do |device|
        device.events.where(uuid_id: wan_change_uuid_id).each do |event|
          row.call(event)
        end
      end
    end
  end
end
