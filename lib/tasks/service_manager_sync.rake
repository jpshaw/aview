namespace :service_manager_sync do
  namespace :base do
    desc 'Sync device configurations with service manager'
    task device: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.device_consumer.topic)
      Tasks::Helpers::ServiceManager::DeviceBaseSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync model configurations with service manager'
    task model: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.device_consumer.topic)
      Tasks::Helpers::ServiceManager::ModelBaseSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync the system configuration with service manager'
    task system: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.device_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemBaseSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync system, model, and device configurations with service manager'
    task all: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.device_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemBaseSync.new(producer).execute
      Tasks::Helpers::ServiceManager::ModelBaseSync.new(producer).execute
      Tasks::Helpers::ServiceManager::DeviceBaseSync.new(producer).execute
      producer.shutdown
    end
  end

  namespace :internet_routes do
    desc 'Sync device internet routes with service manager'
    task device: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.internet_routes_consumer.topic)
      Tasks::Helpers::ServiceManager::DeviceInternetRoutesSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync model internet routes with service manager'
    task model: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.internet_routes_consumer.topic)
      Tasks::Helpers::ServiceManager::ModelInternetRoutesSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync system internet routes with service manager'
    task system: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.internet_routes_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemInternetRoutesSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync all internet routes with service manager'
    task all: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.internet_routes_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemInternetRoutesSync.new(producer).execute
      Tasks::Helpers::ServiceManager::ModelInternetRoutesSync.new(producer).execute
      Tasks::Helpers::ServiceManager::DeviceInternetRoutesSync.new(producer).execute
      producer.shutdown
    end
  end

  namespace :cascaded_networks do
    desc 'Sync device cascaded networks with service manager'
    task device: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.cascaded_networks_consumer.topic)
      Tasks::Helpers::ServiceManager::DeviceCascadedNetworksSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync model cascaded networks with service manager'
    task model: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.cascaded_networks_consumer.topic)
      Tasks::Helpers::ServiceManager::ModelCascadedNetworksSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync system cascaded networks with service manager'
    task system: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.cascaded_networks_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemCascadedNetworksSync.new(producer).execute
      producer.shutdown
    end

    desc 'Sync all cascaded networks with service manager'
    task all: :environment do
      producer = Tasks::Helpers::KafkaMessageBus.new(Settings.kafka.cascaded_networks_consumer.topic)
      Tasks::Helpers::ServiceManager::SystemCascadedNetworksSync.new(producer).execute
      Tasks::Helpers::ServiceManager::ModelCascadedNetworksSync.new(producer).execute
      Tasks::Helpers::ServiceManager::DeviceCascadedNetworksSync.new(producer).execute
      producer.shutdown
    end
  end

end

