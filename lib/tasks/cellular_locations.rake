namespace :cellular_locations do
  QUOTA = 2000

  desc 'Acquire cellular locations from geolocation service for all modems'
  task acquire: :environment do
    modems = DeviceModem.where.not(mcc: nil)
    valid_modems = modems.select { |modem| Geolocator.can_get_cell_location?(modem) && modem.cellular_location.nil? }
    puts "There are #{valid_modems.count} valid modems"
    create_locations(valid_modems)
    puts "There were #{@requests_made} requests made out of the allowable #{QUOTA}"
  end

  def create_locations(modems)
    @requests_made = 0
    modems.each do |modem|
      break if @requests_made >= QUOTA
      puts "Creating location for modem: #{modem.id}"
      set_location_for(modem)
      sleep(0.250) # stay well under Google quota - 10 requests/second/user
    end
  end

  def set_location_for(modem)
    begin
      location = CellularLocation.find_by_modem(modem).first
      unless location
        puts 'Requesting location from Google'
        geolocator = Geolocator.get_location(modem)
        @requests_made += 1
        puts 'Could not get location from Google' unless geolocator
        return unless geolocator
        location = CellularLocation.create(lac: modem.lac, cid: modem.cid, mcc: modem.mcc, mnc: modem.mnc, lat: geolocator.latitude, lon: geolocator.longitude, accuracy: geolocator.accuracy)
      end
      modem.cellular_location = location
      modem.save
    rescue => e
      logger.error "Error setting cell location for device modem #{modem}\n" \
                   "#{e.message}\n #{e.backtrace[0,3].join("\n")}\n"
      nil
    end
  end
end
