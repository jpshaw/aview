require 'open3'

namespace :test do

  desc 'Setup test environment'
  task :setup do
    ENV['RAILS_ENV'] = 'test'

    processor_count =
      if ENV['PARALLEL_TEST_PROCESSORS'].present?
        ENV['PARALLEL_TEST_PROCESSORS']
      else
        Concurrent.processor_count.to_s
      end

    puts "Parallel test processors: #{processor_count}"
    ENV['PARALLEL_TEST_PROCESSORS'] = processor_count

    puts "Dropping test database(s) ..."
    Rake::Task['parallel:drop'].invoke

    puts "Creating test database(s) ..."
    Rake::Task['parallel:create'].invoke

    puts "Loading schema(s) ..."
    Rake::Task['parallel:load_schema'].invoke
  end

  desc 'Run CI tests'
  task ci: ['test:setup', 'environment'] do
    units = %w(
      controllers
      data_tables
      finders
      helpers
      importers
      interactors
      jobs
      lib
      mailers
      models
      policies
      presenters
      processors
      serializers
      services
      uploaders
      views
    )

    files = units.map { |unit| Dir.glob("spec/#{unit}/**/*_spec.rb") }.flatten.join(' ')
    command = "bundle exec parallel_rspec #{files}"

    Open3.popen2e(command) do |stdin, stdout_stderr, wait_thread|
      stdin.close_write
      stdout_stderr.each { |line| puts line }
      wait_thread.value
    end
  end

end
