namespace :locations do

  task :migrate_coordinates_to_decimal => :environment do
    locations_with_float_coordinates.find_each do |location|
      location.update_attributes(latitude: location.f_latitude,
                                longitude: location.f_longitude)
    end
  end

  def locations_with_float_coordinates
    Location
      .where(latitude: nil, longitude: nil)
      .where.not(f_latitude: nil, f_longitude: nil)
  end

end