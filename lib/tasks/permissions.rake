namespace :accelerated do

  namespace :permissions do


    # MANAGEABLE ORGANIZATIONS

    desc "Deletes all permissions that have an organization as manageable"
    task delete_manageable_organizations: :environment do
      puts "Deleting all permissions with an organization as the manageable... "
      count = Permission.where(
        manageable_type: Permission::MANAGEABLE_TYPE_ORGANIZATION
      ).delete_all
      puts "  -> #{count} deleted"
    end

    desc "Creates permissions for organizations that manage themselves"
    task create_manageable_organizations: :environment do
      Rake::Task['accelerated:permissions:delete_manageable_organizations'].invoke

      admin_role  = Role.admin
      count       = 0

      # Create one record with organization as manageable and manager and admin role for all
      # organizations.
      puts "Creating permissions with an organization as the manageable... "
      Organization.all.each do |organization|
        organization.manageable_permissions.create(
          manager:  organization,
          role:     admin_role
        )
        count += 1
      end

      puts "  -> #{count} created"
    end


    # MANAGER USERS WITH MANAGEABLE ORGANIZATIONS

    desc "Deletes all permissions that have a user as manager and an organization as manageable"
    task delete_users_with_manageable_organizations: :environment do
      puts "Deleting all permissions with a user as the manager and an organization as the manageable... "
      count = Permission.where(
        manager_type:     Permission::MANAGER_TYPE_USER,
        manageable_type:  Permission::MANAGEABLE_TYPE_ORGANIZATION
      ).delete_all
      puts "  -> #{count} deleted"
    end

    desc "Creates permissions for admin users to manage their organizations"
    task create_manager_admin_users_with_manageable_organizations: :environment do
      Rake::Task['accelerated:permissions:delete_users_with_manageable_organizations'].invoke

      class RolesUser < ActiveRecord::Base
        belongs_to :role
        belongs_to :user
      end

      admin_role  = Role.admin
      count       = 0

      # Process all user records.
      puts "Creating permissions with admin users as managers and their organizations as manageables... "
      RolesUser.all.each do |record|
        if record.role.admin?
          record.user.manager_permissions.create(
            manageable: record.user.organization,
            role:       admin_role,
            parent:     record.user.organization.self_managing_permission
          )
          count += 1
        end
      end

      puts "  -> #{count} created"
    end


    # ALL OF THE ABOVE

    desc "Creates permissions for organizations, accounts, and users"
    task create_all: :environment do
      Rake::Task['accelerated:permissions:create_manageable_organizations'].invoke
      Rake::Task['accelerated:permissions:create_manager_admin_users_with_manageable_organizations'].invoke
    end

  end

end
