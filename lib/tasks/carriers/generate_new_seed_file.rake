namespace :carriers do
  desc 'Overwrites carrier_info.yml with records generated from the database. '
  task generate_new_seed_file: :environment do
    carrier_service = CarrierInformationService.new
    file = File.new('db/seeds/carrier_info.yml', 'w')
    file.write carrier_service.serialized_records
  end
end
