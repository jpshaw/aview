namespace :carriers do
  desc 'Drops Carrier and CarrierDetail records then seeds them from carrier_info.yml.'
  task reseed: :environment do
    carrier_service = CarrierInformationService.new
    carrier_service.load_records_from_file
    carrier_service.drop
    carrier_service.seed
  end
end
