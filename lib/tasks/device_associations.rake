namespace :device_associations do

  def assign_platform_data_from_meta(platform, data, meta, direction)
    ip    = "ip_address"
    iface = "#{direction}_interface"
    type  = "#{direction}_type"

    case platform
    when :alpha
      data[ip]    = meta[:lan_ip] if meta[:lan_ip] && data[ip].blank?
      data[iface] = 'eth0'
      data[type]  = 'wan'
    when :buildroot
      data[ip]    = meta[:wan_ip] if meta[:wan_ip] && data[ip].blank?
      data[iface] = 'eth0'
      data[type]  = 'lan'
    when :uc_linux
      data[ip]    = meta[:wan_ip] if meta[:wan_ip] && data[ip].blank?
      data[iface] = meta[:interface].to_s.downcase if meta[:interface]
      data[type]  = 'lan'
    when :gateway
      data[iface] = meta[:interface].to_s.downcase if meta[:interface]
      data[type]  = 'wan'
    end

    # Normalize interfaces
    data[iface] = 'eth1' if data[iface] == 'wan1'
    data[iface] = 'eth2' if data[iface] == 'wan2'
  end

  desc 'Migrate old device association columns to the new ones'
  task migrate: :environment do
    DeviceAssociation.find_each(batch_size: 1) do |association|
      # Association already has new data, don't override
      next if association.source_id || association.target_id

      source_device = Device.find_by(id: association.device_id)
      next unless source_device

      data = {
        source_id: source_device.id,
        source_mac: source_device.mac,
        target_mac: association.associated_device_mac
      }

      # Assign source device data
      assign_platform_data_from_meta(
        source_device.platform,
        data,
        (association.meta || {}),
        :source
      )

      target_device = Device.find_by(mac: association.associated_device_mac)
      associations_to_destroy = []

      if target_device.present?
        data[:target_id] = target_device.id

        # Check for inverse relationship
        inverse_association = DeviceAssociation.where(device_id: target_device.id,
                                          associated_device_mac: source_device.mac).first

        if inverse_association.present?
          # Assign target device data
          assign_platform_data_from_meta(
            target_device.platform,
            data,
            (inverse_association.meta || {}),
            :target
          )

          # Destroy inverse association to remove duplicates
          associations_to_destroy << inverse_association
        end
      end

      # Assign data to new fields
      data.each do |key, value|
        association.send("#{key}=", value)
      end

      if association.save
        associations_to_destroy.each(&:destroy)
      else
        puts "Failed to save association #{association.inspect}"
      end
    end
  end

  desc 'Deduplicate device interface associations'
  task deduplicate_interfaces: :environment do
    associations_to_destroy = Set.new

    compare_association_interfaces = lambda do |association, direction|
      device = association.send("#{direction}_device")
      interface = association.send("#{direction}_interface")

      return if device.blank? || interface.blank?

      device.device_associations_for_interface(interface).each do |other_association|
        next if other_association == association

        # Keep the most recent association for the device interface
        if association.created_at > other_association.created_at
          associations_to_destroy << other_association
        else
          associations_to_destroy << association
        end
      end
    end

    DeviceAssociation.find_each do |association|
      next if associations_to_destroy.include?(association)
      compare_association_interfaces.call(association, :source)
      compare_association_interfaces.call(association, :target)
    end

    puts "Removing #{associations_to_destroy.count} duplicate associations"
    associations_to_destroy.each(&:destroy)
  end
end
