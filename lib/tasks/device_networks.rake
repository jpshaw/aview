namespace :device_networks do
  desc 'Remove duplicate network_iface entries for a given device'
  task clean_dups: :environment do
    networks_destroyed = 0
    puts 'Gathering and destroying duplicate device networks for every device'
    DeviceNetwork.select('id, device_id, network_iface, count(*)').group('device_id, network_iface').having('count(*) > 1').each do |network|
      network.destroy
      networks_destroyed += 1
    end
    puts "There were #{networks_destroyed} device networks destroyed."
  end
end
