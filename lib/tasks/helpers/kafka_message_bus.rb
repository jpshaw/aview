module Tasks
  module Helpers
    class KafkaMessageBus
      CLIENT_ID = 'sync-task'.freeze
      MESSAGE_DELIVERY_THRESHOLD = 100
      DELIVERY_INTERVAL_SECONDS = 5

      def initialize(topic)
        @logger = Rails.logger = Logger.new(STDOUT)
        client = Kafka.new(
          seed_brokers: Settings.kafka.brokers,
          client_id: CLIENT_ID
        )
        @producer = client.async_producer(
          delivery_threshold: MESSAGE_DELIVERY_THRESHOLD,
          delivery_interval: DELIVERY_INTERVAL_SECONDS
        )
        @topic = topic
      end

      def publish(message)
        logger.debug "Putting configuration on the #{topic} topic..."
        @producer.produce(message.to_json, topic: topic)
      rescue Kafka::BufferOverflow
        sleep 1
        retry
      end

      def shutdown
        @producer.shutdown if @producer
      end

      private

      attr_reader :topic, :logger
    end
  end
end
