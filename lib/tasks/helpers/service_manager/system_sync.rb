module Tasks
  module Helpers
    module ServiceManager
      module SystemSync
        def execute
          perform_sync(::Configuration::SYSTEM_NAME, '!SYSTEM')
        end
      end
    end
  end
end
