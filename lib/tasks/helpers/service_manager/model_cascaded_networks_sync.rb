module Tasks
  module Helpers
    module ServiceManager
      class ModelCascadedNetworksSync < CascadedNetworksSync
        include Tasks::Helpers::ServiceManager::ModelSync
      end
    end
  end
end
