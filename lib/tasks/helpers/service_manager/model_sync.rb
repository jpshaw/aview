module Tasks
  module Helpers
    module ServiceManager
      module ModelSync
        def execute
          scoped_gateways.find_each do |device|
            model_name = device.config_name
            next if completed_models.include?(model_name)
            perform_sync(model_name, device.acct_name)
            completed_models << model_name
          end
        end

        protected

        def completed_models
          @completed_models ||= []
        end
      end
    end
  end
end
