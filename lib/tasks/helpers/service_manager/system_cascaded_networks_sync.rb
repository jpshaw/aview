module Tasks
  module Helpers
    module ServiceManager
      class SystemCascadedNetworksSync < CascadedNetworksSync
        include Tasks::Helpers::ServiceManager::SystemSync
      end
    end
  end
end
