module Tasks
  module Helpers
    module ServiceManager
      class DeviceInternetRoutesSync < InternetRoutesSync
        include Tasks::Helpers::ServiceManager::DeviceSync
      end
    end
  end
end
