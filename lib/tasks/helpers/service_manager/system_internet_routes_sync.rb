module Tasks
  module Helpers
    module ServiceManager
      class SystemInternetRoutesSync < InternetRoutesSync
        include Tasks::Helpers::ServiceManager::SystemSync
      end
    end
  end
end
