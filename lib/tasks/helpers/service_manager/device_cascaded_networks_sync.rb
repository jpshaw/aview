module Tasks
  module Helpers
    module ServiceManager
      class DeviceCascadedNetworksSync < CascadedNetworksSync
        include Tasks::Helpers::ServiceManager::DeviceSync
      end
    end
  end
end
