module Tasks
  module Helpers
    module ServiceManager
      class InternetRoutesSync

        def initialize(kafka_producer)
          @logger = Rails.logger = Logger.new(STDOUT)
          @smx_client = ServiceManagerRest::InternetRoutesApi.new
          @producer = kafka_producer
        end

        def perform_sync(device_id, account)
          logger.info "Getting internet routes for #{device_id} from Service Manager ..."

          configuration = smx_client.get_internet_routes(account, device_id)

          configuration.data.each do |internet_route|
            producer.publish(within_envelope(internet_route))
          end
        rescue ServiceManagerRest::ApiError => error
          case error.code
          when 500
            raise error # Reraise exception if the service has an internal error
          when 404
            logger.info "Internet routes for #{device_id} not found"
          else
            logger.error "#{error.class}: #{error.message}"
          end
        end

        protected

        attr_reader :logger, :smx_client, :producer

        def within_envelope(config_data)
          { meta: { action: 'UPSERT' }, data: config_data }
        end

        def scoped_gateways
          select_fields = {
            devices: [:id, :mac, :object_type, :organization_id],
            organizations: [:id, 'name as acct_name'],
            gateway_details: [:id, 'configuration_name as config_name']
          }

          scope = Gateway.joins(:organization).joins(:details)

          # Allow optionally loading models for a single organization
          scope = scope.where(organizations: { name: ENV['SMX_ACCOUNT'] }) if ENV['SMX_ACCOUNT']

          # Select just the fields needed in order to reduce the overhead of loading
          # over 25k device records.
          scope = scope.select(select_fields.map { |table, columns| columns.map { |column| "#{table}.#{column}" } }.join(','))

          scope
        end

      end
    end
  end
end
