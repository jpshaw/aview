module Tasks
  module Helpers
    module ServiceManager
      module DeviceSync
        def execute
          scoped_gateways.find_each { |device| perform_sync(device.mac, device.acct_name) }
        end
      end
    end
  end
end

