module Tasks
  module Helpers
    class SeedDruid
      class << self
        LIMIT = 1000

        def influx
          @influx ||= MetricsService::Client.new(Measurement::Base.client_opts('influxdb')).adapter.client
        end

        def query(opts)
          query_arg = "SELECT * FROM #{opts[:name]} WHERE time >= '#{opts[:start]}' and time < '#{opts[:end]}' LIMIT #{LIMIT} OFFSET #{opts[:offset]}"
          puts "SeedDruid - query     : #{query_arg}"
          influx.query(query_arg).first.try(:[], 'values')
        end

        def run
          migrate_cellular_utilization
          migrate_wan_utilization
        end

        def migrate_cellular_utilization
          iterate_over('cellular_utilization') do |opts|
            results = query(opts)

            while results.present?
              puts "SeedDruid - offset    : #{opts[:offset]}"
              results.each do |point|
                Measurement::CellularUtilization.create(
                  mac: point['mac'],
                  carrier: point['carrier'],
                  data_plan: point['data_plan'],
                  interface: point['interface'],
                  rx: point['received'],
                  tx: point['transmitted'],
                  time: Time.at(point['time'] / 1000)
                )
              end

              opts[:offset] += LIMIT
              results = query(opts)
            end
          end
        end

        def migrate_wan_utilization
          iterate_over('wan_utilization') do |opts|
            results = query(opts)

            while results.present?
              puts "SeedDruid - offset    : #{opts[:offset]}"
              results.each do |point|
                Measurement::WanUtilization.create(
                  mac: point['mac'],
                  interface: point['interface'],
                  time: Time.at(point['time'] / 1000),
                  upload_hwm: point['upload_hwm'],
                  upload: point['upload'],
                  download_hwm: point['download_hwm'],
                  download: point['download']
                )
              end

              opts[:offset] += LIMIT
              results = query(opts)
            end
          end
        end

        def iterate_over(name)
          opts = {}
          opts[:name] = name

          35.times do |i|
            day_offset = -35 + i
            puts "SeedDruid - day_offset: #{day_offset}"

            opts[:start] = time.advance(days: day_offset).iso8601
            opts[:end] = time.advance(days: day_offset + 1).iso8601
            opts[:offset] = 0

            yield(opts)
          end
        end

        def time
          @time ||= Time.now.utc
        end
      end
    end
  end
end
