namespace :app do

  # Examples:
  #   $ rake 'app:create_root_user[test.user@accelerated.com,abc123]'
  desc 'Creates an admin user for the root organization - \'email\' and \'password\' are required.'
  task :create_root_user, [:email, :password] => :environment do |task, args|
    root_organization = Organization.roots.first

    raise ArgumentError.new('Please create an organization first') unless root_organization

    user = User.create!(
      email:                 args[:email],
      password:              args[:password],
      password_confirmation: args[:password],
      organization_id:       root_organization.id
    )

    permission = Permission.create!(
      manager:    user,
      manageable: root_organization,
      role:       root_organization.admin_role
    )

    puts 'Successfully created root user'
  end

  desc 'Print the current aview version'
  task :version do
    puts Aview.version
  end

  # TODO: Remove when all build scripts are updated
  desc 'Create a file with the current aview version'
  task :create_version_file do
    puts "[DEPRECATION] `app:create_version_file` is deprecated."
  end
end
