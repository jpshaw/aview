namespace :notifications do

  NOTIFIABLE_USERS_FILE = "#{ENV['HOME']}/user_notify_on.csv"
  BATCH_SIZE            = 100

  desc 'Turn off notifications for all users and save the user ids of those who had it enabled'
  task :turn_off => :environment do
    CSV.open(NOTIFIABLE_USERS_FILE, 'wb') do |csv|
      csv << ['user_id']

      User.where(notify_enabled: true).pluck(:id).each do |user_id|
        csv << [user_id]
      end
    end

    # Turn off notifications for all users
    User.update_all(notify_enabled: false)
  end

  # Turn on notifications for users that previously had it enabled
  desc 'Turn on notifications for users that previously had it enabled'
  task :turn_on => :environment do

    unless File.exists?(NOTIFIABLE_USERS_FILE)
      raise "Unable to find notifiable users file: #{NOTIFIABLE_USERS_FILE}"
    end

    # Turn on notifications for users that previously had it enabled
    enabled_users = []

    CSV.foreach(NOTIFIABLE_USERS_FILE, headers: true) do |row|
      enabled_users << row['user_id']
    end

    # Enable user notifications in batches
    enabled_users.each_slice(BATCH_SIZE).to_a.each do |user_ids|
      User.where(id: user_ids).update_all(notify_enabled: true)
    end

    File.delete NOTIFIABLE_USERS_FILE
  end

  # TODO: Remove this once all instances have been migrated
  desc 'Populate `created_at` column for notifications'
  task :migrate_timestamps => :environment do
    Notification.where(created_at: nil).where.not(delivered_at: nil).find_each(batch_size: 500) do |notification|
      notification.update_columns(created_at: notification.delivered_at)
    end
  end

end
