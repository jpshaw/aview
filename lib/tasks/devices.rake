namespace :devices do

  VPN_GATEWAY_MANAGMENT_IP_FILE = "#{ENV['HOME']}/vpn_gateway_mgmt_IP_update.csv"
  SMX_TAB_FILE = "#{ENV['HOME']}/smx_netgate_list.tab"

  task :set_node_name, [:name] => :environment do |t, args|
    args.with_defaults(:name => Socket.gethostname)
    Device.update_all(node_name: args[:name])
  end

  task :remove_gateway_lans => :environment do
    Gateway.joins(:networks).find_each do |device|
      device.networks.lan.destroy_all
    end
  end

  desc 'Sync device status with device states table'
  task sync_device_status: :environment do
    Device.joins(:device_state).find_each do |device|
      device.save!
    end
  end

  # Turn on notifications for users that previously had it enabled
  desc 'Update the management IP of target VPN Gateway devices'
  task :update_management_ips => :environment do

    unless File.exists?(VPN_GATEWAY_MANAGMENT_IP_FILE)
      raise "Unable to find file with list of VPN Gateways and Managment IP: #{VPN_GATEWAY_MANAGMENT_IP_FILE}"
    end

    puts "START: parsing through CSV file for new management IPs"

    # Turn on notifications for users that previously had it enabled
    device_update_count = 0

    CSV.foreach(VPN_GATEWAY_MANAGMENT_IP_FILE, headers: true) do |row|
      begin
        vpn_gateway_mac = row['mac']
        management_ip = IPAddr.new(row['ip_addr']) #this will throw an error if ip_addr is not a valid IPv4/IPv6 address
        if device = Device.find_by(mac: vpn_gateway_mac)
          device.mgmt_host = management_ip.to_s
          if device.changed?
            device_update_count += 1
            device.save!
            puts "Updated managment IP for #{vpn_gateway_mac}"
          else
            puts "Skipping update of device #{vpn_gateway_mac}.  Management IP didn't change"
          end
        else
          puts "Unable to find device: #{vpn_gateway_mac}"
        end
      rescue => e
        puts "Error updating device #{row}" \
                     "      #{e.message}"
      end
    end
    puts "DONE:  Updated the management IPs for #{device_update_count} VPN Gateways"
  end

  desc 'Pull list of 8200s from database, then search the SMX tabfile to see if their model needs updating'
  task :fix_gateways_labelled_as_8200s => :environment do
    unless File.exists?(SMX_TAB_FILE)
      raise "Unable to find SMX tabfile file #{SMX_TAB_FILE}"
    end
    puts "Start:  Searching for VPN Gateways labelled as 8200 but are a different model"
    header = File.readlines(SMX_TAB_FILE).first
    Gateway.where(hw_version: '8200').find_each do |device|
      begin
        if entry = open(SMX_TAB_FILE) { |f| f.each_line.find { |line| line.include?(device.mac) } }
          csv_string = header + entry
          csv = CSV.new(csv_string, :col_sep => "\t", :headers => true, :quote_char => "\x07")
          csv.each do |row|
            model_name = row['HARDWARE_MODEL'] || '8200'
            model = DeviceModel.find_by_name(model_name)
            if model.name != '8200'
              puts "Updating #{device.mac} in #{device.organization.name} from 8200 to #{model.name}"
              device.hw_version = model.name
              device.model_id = model.id
              device.category_id = model.category_id
              device.save!
            end
          end
        end
      rescue => e
        puts "Error parsing #{SMX_TAB_FILE} for device #{device.mac}"
      end
    end
    puts "DONE"
  end

end
