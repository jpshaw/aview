require 'pp'
require 'json'


namespace :tests do

  namespace :dial_to_ip do

    def dc_mac
      "00270403023D"
    end

    def dc_syslog(data)

      syslog = {
        "@source" => "udp://192.168.33.1:52902/",
        "@tags" => [],
        "facility" => "13",
        "syslog_timestamp" => "Feb 11 15:18:18",
        "syslog_host" => "108.166.111.162",
        "mac" => dc_mac,
        "device_host" => "208.54.36.243",
        "type" => data[:type],
        "message" => data[:message],
        "@timestamp" => "2013-07-12T03:47:52.895Z",
        "@source_host" => "192.168.33.1",
        "@source_path" => "/",
        "@message" => "<13>Feb 11 15:18:18 108.166.111.162 #{dc_mac}/208.54.36.243 #{data[:type]}: #{data[:message]}\n",
        "@type" => "dial_to_ip"
      }

      pp syslog
    end

    task :all do
      Rake::Task["tests:dial_to_ip:firmware"].invoke
      Rake::Task["tests:dial_to_ip:modem"].invoke
      Rake::Task["tests:dial_to_ip:signal_status"].invoke
      Rake::Task["tests:dial_to_ip:location"].invoke
      Rake::Task["tests:dial_to_ip:dhcp"].invoke
      Rake::Task["tests:dial_to_ip:network_details"].invoke
      Rake::Task["tests:dial_to_ip:network_dns"].invoke
      Rake::Task["tests:dial_to_ip:network_status"].invoke
    end

    task :dhcp do
      data = {
        type: 'dhcp',
        message: 'pppl=10.161.237.168~dns1=172.16.0.4~dns2=10.5.133.45~dns3=10.5.136.242'
      }
      dc_syslog data
    end

    task :firmware do
      data = {
        type: 'firmware',
        message: 'version=1.0.12-dc~serial=5300010001010114'
      }
      dc_syslog data
    end

    task :location do
      data = {
        :type => 'location',
        :message => 'type=modem~idx=0~mcc=505~mnc=1~lac=14433~cid=3602~lat=-27.538140~lon=153.003677~alt=27.300000'
       }
       dc_syslog data
    end

    task :modem do
      data = {
        :type => "modem",
        :message => "idx=0~apn=telstra.wap~imei=357784044010427~imsi=~phone=0459644080~iccid=89610107098629000074~provider=Telstra~manufacturer=HuaweiTechnologiesCo.Ltd.~model=MU609~revision=12.107.07.05.457~usb=480"
      }
      dc_syslog data
    end

    task :network_details do
      data = {
        :type => "network",
        :message => "type=modem~idx=0~intf=eth1~mtu=1500~rx=11314121~tx=19441250~default=pri~ip0=10.161.237.168/28~gw0=10.161.237.161~metric0=3"
      }
      dc_syslog data
    end

    task :network_dns do
      data = {
        :type => 'network',
        :message => 'type=dns~dns0=172.16.0.4~dns1=10.5.133.45~dns2=10.5.136.242'
      }
      dc_syslog data
    end

    task :network_status do
      data = {
        :type => 'network',
        :message => 'type=ethernet~intf=eth0~mtu=1500~link=ok~speed=100baseTx-FD,~rx=92764924~tx=344496~default=alt~ip0=172.16.0.183/24~gw0=172.16.0.4~metric0=5~ip1=192.168.210.1/24'
      }
      dc_syslog data
    end

    task :signal_status do
      data = {
        :type => "status",
        :message => "type=modem~idx=0~intf=eth1~dbm=-51~per=100~cnti=edge~rx=11314121~tx=19440913"
      }
      dc_syslog data
    end

  end

end
