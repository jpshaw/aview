
require 'pp'
require 'json'


namespace :tests do

  namespace :netreach do

    def netreach_mac
      "00C0CAABC123"
    end

    def nr_syslog(data)

      syslog = {
        "@source" => "udp://192.168.33.1:52902/",
        "@tags" => [],
        "facility" => "13",
        "syslog_timestamp" => "Feb 11 15:18:18",
        "syslog_host" => "108.166.111.162",
        "mac" => netreach_mac,
        "device_host" => "208.54.36.243",
        "type" => data[:type],
        "message" => data[:message],
        "@timestamp" => "2013-07-12T03:47:52.895Z",
        "@source_host" => "192.168.33.1",
        "@source_path" => "/",
        "@message" => "<13>Feb 11 15:18:18 108.166.111.162 #{netreach_mac}/208.54.36.243 #{data[:type]}: #{data[:message]}\n",
        "@type" => "wifi"
      }

      pp syslog
    end

    task :all do
      # Typical startup and status logs
      Rake::Task["tests:netreach:dhcp_message"].invoke
      Rake::Task["tests:netreach:restart"].invoke
      Rake::Task["tests:netreach:firmware_download_success"].invoke
      Rake::Task["tests:netreach:firmware_up_to_date"].invoke
      Rake::Task["tests:netreach:config_success"].invoke
      Rake::Task["tests:netreach:config_up_to_date"].invoke
      Rake::Task["tests:netreach:status"].invoke
      Rake::Task["tests:netreach:client_connect"].invoke
      Rake::Task["tests:netreach:client_disconnect"].invoke
      Rake::Task["tests:netreach:client_summary"].invoke
      Rake::Task["tests:netreach:staquery_details"].invoke
      Rake::Task["tests:netreach:staquery_list"].invoke
      Rake::Task["tests:netreach:status_restart"].invoke
      # Generic message to ensure that random syslogs are accepted
      Rake::Task["tests:netreach:channel_list"].invoke
      # Restore default config
      Rake::Task["tests:netreach:default_config_reset"].invoke
      Rake::Task["tests:netreach:default_config_success"].invoke
      Rake::Task["tests:netreach:default_config_restart"].invoke
      # Firmware upgrade
      Rake::Task["tests:netreach:firmware_upgrade_inprogress"].invoke
      Rake::Task["tests:netreach:firmware_upgrade_success"].invoke
      # Remote controls
      Rake::Task["tests:netreach:remote_config_flag"].invoke
      Rake::Task["tests:netreach:remote_default_flag"].invoke
      Rake::Task["tests:netreach:remote_reboot_flag"].invoke
      # Vlan auto-discovery
      Rake::Task["tests:netreach:vlan_discover"].invoke
      # Errors
      Rake::Task["tests:netreach:config_fail"].invoke
      Rake::Task["tests:netreach:default_config_error"].invoke
      Rake::Task["tests:netreach:firmware_download_fail_bin"].invoke
      Rake::Task["tests:netreach:firmware_download_fail_md5"].invoke
      Rake::Task["tests:netreach:firmware_upgrade_error"].invoke
      Rake::Task["tests:netreach:firmware_upgrade_failure"].invoke
      Rake::Task["tests:netreach:time_sync_error"].invoke
    end

    task :channel_list do
      data = {
        :type => "info",
        :message => "SSIDchannels=9;9"
      }
      nr_syslog data
    end

    task :client_connect do
      data = {
        :type => "network",
        :message => "Client connected: a4670645f035"
      }
      nr_syslog data
    end

    task :client_disconnect do
      data = {
        :type => "network",
        :message => "Client disconnected: a4670645f035"
      }
      nr_syslog data
    end

    task :client_summary do
      data = {
        :type => "network",
        :message => "Associated client summary: a4670645f035,0015707ea967,a4670645f035,0015707ea967"
      }
      nr_syslog data
    end

    task :config_success do
      data = {
        :type => "info",
        :message => "date=Wed Oct 16 01:05:50 EDT 2013~firmware=1.552.41~status=ok"
      }
      nr_syslog data
    end

    task :config_fail do
      data = {
        :type => "info",
        :message => "date=Wed Oct 16 01:10:50 EDT 2013~firmware=1.552.41~status=error"
      }
      nr_syslog data
    end

    task :config_up_to_date do
      data = {
        :type => "info",
        :message => "Configuration up to date"
      }
      nr_syslog data
    end

    task :default_config_error do
      data = {
        :type => "Update-Reset",
        :message => "critical error!"
      }
      nr_syslog data
    end

    task :default_config_reset do
      data = {
        :type => "Update-Reset",
        :message => "Reset button pressed. Starting reset back to default config!"
      }
      nr_syslog data
    end

    task :default_config_restart do
      data = {
        :type => "Update-Reset",
        :message => "Rebooting..."
      }
      nr_syslog data
    end

    task :default_config_success do
      data = {
        :type => "Update-Reset",
        :message => "Flash update completed successfully. Default config loaded"
      }
      nr_syslog data
    end

    task :dhcp_message do
      data = {
        :type => "dhcp",
        :message => "GatewayLanIP=10.23.199.225~GatewayMAC=00:D0:CF:17:1B:3C~GatewayWanIP=184.255.16.178~WebIP=10.23.199.229~DNS=10.1.1.23 10.101.1.45"
      }
      nr_syslog data
    end

    task :firmware_download_fail_bin do
      data = {
        :type => "firmware",
        :message => "wget of (1.554.42.bin) failed"
      }
      nr_syslog data
    end

    task :firmware_download_fail_md5 do
      data = {
        :type => "firmware",
        :message => "wget of (1.554.42.md5) failed"
      }
      nr_syslog data
    end

    task :firmware_download_success do
      data = {
        :type => "Update-Check",
        :message => "new version is ready to apply"
      }
      nr_syslog data
    end

    task :firmware_upgrade_error do
      data = {
        :type => "Update-Apply",
        :message => "critical error!"
      }
      nr_syslog data
    end

    task :firmware_upgrade_failure do
      data = {
        :type => "Update-Apply",
        :message => "Error: firmware update stopped. Aborted without affecting device"
      }
      nr_syslog data
    end

    task :firmware_upgrade_inprogress do
      data = {
        :type => "Update-Apply",
        :message => "Updating firmware. DO NOT POWER OFF THE UNIT"
      }
      nr_syslog data
    end

    task :firmware_upgrade_success do
      data = {
        :type => "Update-Apply",
        :message => "Firmware update successful"
      }
      nr_syslog data
    end

    task :firmware_up_to_date do
      data = {
        :type => "firmware",
        :message => "img '1.552.41' is current"
      }
      nr_syslog data
    end

    task :remote_config_flag do
      data = {
        :type => "remote",
        :message => "command received [config].  Checking config now"
      }
      nr_syslog data
    end

    task :remote_default_flag do
      data = {
        :type => "remote",
        :message => "command received [default].  Restoring default configurations."
      }
      nr_syslog data
    end

    task :remote_reboot_flag do
      data = {
        :type => "remote",
        :message => "command received [reboot]"
      }
      nr_syslog data
    end

    task :restart do
      data = {
        :type => "info",
        :message => "Cause of last restart: (Configuration update)"
      }
      nr_syslog data
    end

    task :status do
      data = {
        :type => "status",
        :message => "cnt=3643~rx=3027795~tx=1949101~rx10=852~tx10=523~uptime= 12:20:24 up 25 days, 11:14,  load average: 0.00, 0.00, 0.00"
      }
      nr_syslog data
    end

    task :status_restart do
      data = {
        :type => "info",
        :message => "rebooting (Configuration update)"
      }
      nr_syslog data
    end

    task :staquery_details do
      data = {
        :type => "staquery",
        :message => "RawData(Ch SSID BSSID Security SiganlPct W-Mode)=1 MR12_AP 06:18:0a:21:3c:d8 WPA1PSKWPA2PSK/TKIPAES 60 11b/g/n~2 Papi 2c:b0:5d:9a:d1:84 WPA2PSK/AES 0 11b/g/n~6 07B405906210 00:12:0e:7a:76:89 WEP 24 11b/g~6 Cisco77659 68:7f:74:51:00:6f WPA1PSKWPA2PSK/TKIPAES 10 11b/g/n~9 ASHLEY AND MATT c0:3f:0e:28:92:3c WEP~10 kellysvcs 00:c0:ca:49:d2:7e WPA/AES 60 11b/g/n~10 KellyGuest 00:c0:ca:49:d2:7f WPA2PSK/AES 65 11b/g/n~11 kellysvcs1 00:c0:ca:6e:66:a6 WPA/AES 70 11b/g/n~11 KellyGuest1 00:c0:ca:6e:66:a7 WPA2PSK/AES 70 11b/g/n~11 kellysvcs2 00:c0:ca:6e:66:f2 WPA/AES 50 11b/g/n~11 KellyGuest2 00:c0:ca:6e:66:f3 WPA2PSK/AES 55 11b/g/n~11 belkin.dbc 08:86:3b:7b:fd:bc WPA1PSKWPA2PSK/AES 0 11b/g/n"
      }
      nr_syslog data
    end

    task :staquery_list do
      data = {
        :type => "staquery",
        :message => "SSIDS=07B405906210 ASHLEY Cisco77659 KellyGuest KellyGuest1 KellyGuest2 MR12_AP Papi belkin.dbc kellysvcs kellysvcs1 kellysvcs2"
      }
      nr_syslog data
    end

    task :time_sync_error do
      data = {
        :type => "smxtime",
        :message => "Failed to establish date/time!"
      }
      nr_syslog data
    end

    task :vlan_discover do
      data = {
        :type => "auto-vlan",
        :message => "Vlan=4~webip=10.23.199.229"
      }
      nr_syslog data
    end

  end

end
