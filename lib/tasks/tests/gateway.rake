
require 'pp'
require 'json'

namespace :tests do

  namespace :gateway do

    def mac
      "00:D0:CF:00:0C:30"
    end

    def send_trap(data)

      trap = {
        "@source" => "udp://192.168.33.10:60817/",
        "@tags" => [],
        "facility" => "30",
        "syslog_timestamp" => "Jul 10 00:07:31",
        "syslog_host" => "webapp",
        "snmp_proc_name" => "snmptrapd",
        "snmp_proc_id" => "11788",
        "snmp_timestamp" => "2013-07-10 00:07:26",
        "snmp_ip_protocol" => "UNKNOWN",
        "tunnel_ip" => ["1.2.3.10"],
        "tunnel_port" => ["64157"],
        "interface_ip" => ["192.168.33.10"],
        "message" => data[:parsed_message],
        "mac" => mac,
        "@timestamp" => "2013-07-10T00:07:31.531Z",
        "@source_host" => "192.168.33.10",
        "@source_path" => "/",
        "@message" => data[:raw_message],
        "@type" => "vpn"
      }

      pp trap
    end

    def send_empty_trap(data)

      trap = {
        "@source" => "udp://192.168.33.10:60817/",
        "@tags" => [],
        "facility" => "30",
        "syslog_timestamp" => "Jul 10 00:07:31",
        "syslog_host" => "webapp",
        "snmp_proc_name" => "snmptrapd",
        "snmp_proc_id" => "11788",
        "snmp_timestamp" => "2013-07-10 00:07:26",
        "snmp_ip_protocol" => "UNKNOWN",
        "message" => data[:parsed_message],
        "mac" => mac,
        "@timestamp" => "2013-07-10T00:07:31.531Z",
        "@source_host" => "192.168.33.10",
        "@source_path" => "/",
        "@message" => data[:raw_message],
        "@type" => "vpn"
      }

      pp trap
    end

    task :all do
      Rake::Task["tests:gateway:tunnel_up_failed"].invoke
      Rake::Task["tests:gateway:tunnel_up"].invoke
      Rake::Task["tests:gateway:tunnel_alive"].invoke
      Rake::Task["tests:gateway:tunnel_down"].invoke
      Rake::Task["tests:gateway:tunnel_primary_up"].invoke
      Rake::Task["tests:gateway:tunnel_primary_down"].invoke
      Rake::Task["tests:gateway:firewall_threshold_exceeded"].invoke
      Rake::Task["tests:gateway:firewall_full"].invoke
      Rake::Task["tests:gateway:excessive_dial"].invoke
      Rake::Task["tests:gateway:ip_changed"].invoke
      Rake::Task["tests:gateway:rogue_mac"].invoke
      Rake::Task["tests:gateway:rogue_mac_file_not_found"].invoke
      Rake::Task["tests:gateway:profile_not_found"].invoke
      Rake::Task["tests:gateway:disco"].invoke
      Rake::Task["tests:gateway:dial_test_failed"].invoke
      Rake::Task["tests:gateway:reboot"].invoke
      Rake::Task["tests:gateway:shutdown"].invoke
      Rake::Task["tests:gateway:ssh_login"].invoke
      Rake::Task["tests:gateway:dhcp_exhausted"].invoke
      Rake::Task["tests:gateway:secondary_login"].invoke
      Rake::Task["tests:gateway:ssh_user_switch"].invoke
      Rake::Task["tests:gateway:cell_modem_required"].invoke
      Rake::Task["tests:gateway:wan2_test_failed"].invoke
      Rake::Task["tests:gateway:unknown_event"].invoke
      Rake::Task["tests:gateway:empty_event"].invoke
    end

    # ngevIpConntrackTable - Sent if the Firewall Connection table reaches the specified threshold.
    #
    # OBJECTS {
    #  ngevNetGateMac,
    #  ngevIpConntrackCurrentPercentage - Current ip table percentage of max (int)
    #  ngevIpConntrackMaxPercentage - Max percentage before alert is cut (int)
    # }
    #
    task :firewall_threshold_exceeded do

      # Params
      current_percentage = 89
      max_percentage = 75

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevIpConntrackTable#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevIpConntrackCurrentPercentage.0 = INTEGER: #{current_percentage}#011ATT-NETGATE-MIB::ngevIpConntrackMaxPercentage.0 = INTEGER: #{max_percentage}",
        :raw_message => "<30>Jul 11 21:21:00 webapp snmptrapd[13542]: 2013-07-11 21:21:00 <UNKNOWN> [UDP: [192.168.33.1]:60869->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevIpConntrackTable#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevIpConntrackCurrentPercentage.0 = INTEGER: #{current_percentage}#011ATT-NETGATE-MIB::ngevIpConntrackMaxPercentage.0 = INTEGER: #{max_percentage}"
      }

      send_trap data
    end

    # ngevIpConntrackTableFull - Indicates the firewall Connection table has reached capacity.
    #
    # OBJECTS { ngevNetGateMac }
    #
    task :firewall_full do

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevIpConntrackTableFull#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}",
        :raw_message => "<30>Jul 11 21:24:23 webapp snmptrapd[13542]: 2013-07-11 21:24:23 <UNKNOWN> [UDP: [192.168.33.1]:60869->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevIpConntrackTableFull#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}"
      }

      send_trap data
    end

    # ngevExcessiveDial - The device has been in dial backup longer than the allowed threshold.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevTimeInDial - Number of minutes the device has been connected (int)
    #   ngifTimeInDial - How long the current dial connection has been up (timeticks)
    #   ngevMaxTimeInDialUntilAlert - Number of minutes connected via dial before an alert is sent (timeticks)
    #   ngifActiveWanIface - Primary connection interface (string)
    # }
    #
    task :excessive_dial do

      time_in_dial = 100
      iface_time_in_dial = 364123
      dial_alert_threshold = 370000
      active_wan_iface = "ppp1"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevExcessiveDial#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTimeInDial.0 = INTEGER: #{time_in_dial}#011ATT-NETGATE-MIB::ngifTimeInDial.0 = Timeticks: (#{iface_time_in_dial}) 0:00:56.78#011ATT-NETGATE-MIB::ngevMaxTimeInDialUntilAlert.0 = Timeticks: (#{dial_alert_threshold}) 0:00:50.00#011ATT-NETGATE-MIB::ngifActiveWanIface.0 = STRING: #{active_wan_iface}",
        :raw_message => "<30>Jul 11 21:39:28 webapp snmptrapd[13542]: 2013-07-11 21:39:28 <UNKNOWN> [UDP: [192.168.33.1]:60869->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (16000) 0:02:40.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevExcessiveDial#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTimeInDial.0 = INTEGER: #{time_in_dial}#011ATT-NETGATE-MIB::ngifTimeInDial.0 = Timeticks: (#{iface_time_in_dial}) 0:00:56.78#011ATT-NETGATE-MIB::ngevMaxTimeInDialUntilAlert.0 = Timeticks: (#{dial_alert_threshold}) 0:00:50.00#011ATT-NETGATE-MIB::ngifActiveWanIface.0 = STRING: #{active_wan_iface}"
      }

      send_trap data
    end


    # ngevActiveWanIpChanged - The IP address associated with the active WAN interface has changed.
    # For example, dial backup was initiated because WAN connectivity was lost.  Another example
    # is that a PPPoE connection changed its IP address.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngifActiveWanIface - Primary connection interface (string)
    #   ngevActiveWanIp - Address of the active wan interface
    #   ngevIPv6ActiveWanIp - Address of the active ipv6 interface
    # }
    #
    task :ip_changed_for_iface, :iface do |t, args|
      wan_iface = args[:iface]
      wan_ipv4 = "172.16.14.6"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevActiveWanIpChanged#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngifActiveWanIface.0 = STRING: #{wan_iface}#011ATT-NETGATE-MIB::ngevActiveWanIp.0 = IpAddress: #{wan_ipv4}",
        :raw_message => "<30>Jul 12 00:52:55 webapp snmptrapd[13542]: 2013-07-12 00:52:45 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevActiveWanIpChanged#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngifActiveWanIface.0 = STRING: #{wan_iface}#011ATT-NETGATE-MIB::ngevActiveWanIp.0 = IpAddress: #{wan_ipv4}"
      }

      send_trap data
    end

    task :ip_changed do
      Rake::Task["tests:gateway:ip_changed_for_iface"].invoke("eth1")
    end

    # ngevRogueMacDetected - The unauthorized MAC detection feature is enabled and an unauthorized MAC address was detected.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevRogueMac - MAC address of the unauthorized machine (str)
    #   ngevRogueIp - IP Address of the unauthorized machine (ip)
    #   ngevRogueInterface - Interface the unauthorized access was detected on (str)
    #   ngevTrapDescription - SMX description field (str)
    #   ngevIPv6RogueIp
    # }
    #
    task :rogue_mac do

      rogue_mac = "10:93:e9:09:cd:48"
      rogue_ip = "10.2.3.4"
      rogue_iface = "eth0"
      trap_desc = "Info goes here"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevRogueMacDetected#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRogueMac.0 = STRING: #{rogue_mac}#011ATT-NETGATE-MIB::ngevRogueIp.0 = IpAddress: #{rogue_ip}#011ATT-NETGATE-MIB::ngevRogueInterface.0 = STRING: #{rogue_iface}#011ATT-NETGATE-MIB::ngevTrapDescription.0 = STRING: #{trap_desc}",
        :raw_message => "<30>Jul 12 01:09:58 webapp snmptrapd[13542]: 2013-07-12 01:09:48 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevRogueMacDetected#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRogueMac.0 = STRING: #{rogue_mac}#011ATT-NETGATE-MIB::ngevRogueIp.0 = IpAddress: #{rogue_ip}#011ATT-NETGATE-MIB::ngevRogueInterface.0 = STRING: #{rogue_iface}#011ATT-NETGATE-MIB::ngevTrapDescription.0 = STRING: #{trap_desc}"
      }

      send_trap data
    end

    # ngevRogueMacNoFile - The unauthorized MAC detection feature is enabled and the authorized MAC file doesn't exist.
    #
    # OBJECTS { ngevNetGateMac }
    #
    task :rogue_mac_file_not_found do

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevRogueMacNoFile#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}",
        :raw_message => "<30>Jul 12 01:23:25 webapp snmptrapd[13542]: 2013-07-12 01:23:15 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevRogueMacNoFile#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}"
      }

      send_trap data
    end

    # ngevDeviceDiscoOrNotFound - Sent if the AT&T NetGate profile is not found. If the AT&T NetGate goes too long without
    # finding its device profile then the device will switch its status to discontinued, which will also trigger a trap/inform.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevQueryProblem - Specifies what type of problem there was with the query
    #     INTEGER {
    #       profileNotFound(0),
    #       discontinuingTheDevice(1)
    #     }
    # }
    #
    task :profile_not_found do

      query_problem = "profileNotFound(0)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceDiscoOrNotFound#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevQueryProblem.0 = INTEGER: #{query_problem}",
        :raw_message => "<30>Jul 12 01:36:27 webapp snmptrapd[13542]: 2013-07-12 01:36:17 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceDiscoOrNotFound#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevQueryProblem.0 = INTEGER: #{query_problem}"
      }

      send_trap data
    end

    # Uses ngevDeviceDiscoOrNotFound - Discontinuing (using different integer)
    task :disco do

      query_problem = "discontinuingTheDevice(1)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceDiscoOrNotFound#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevQueryProblem.0 = INTEGER: #{query_problem}",
        :raw_message => "<30>Jul 12 01:36:27 webapp snmptrapd[13542]: 2013-07-12 01:36:17 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceDiscoOrNotFound#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevQueryProblem.0 = INTEGER: #{query_problem}"
      }

      send_trap data
    end

    # ngevTunnelUp - Information about the results of a tunnel connection attempt.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevTunConnectionId - ID used to tie the disconnect together
    #   ngevTunnelDateAndTime - Timestamp from when the tunnel came up
    #   ngtuTunMode - Tunnel control mode
    #     INTEGER {
    #       tunnelUnknown(0),
    #       tunnelUserControl(1),
    #       tunnelUserViewOnly(2),
    #       tunnelHidden(3),
    #       tunnelMaintenance(4)
    #     }
    #   ngevConnectionType - Connection type: Broadband, dial, etc.
    #     INTEGER {
    #       connTypeBroadband(0),
    #       connTypeAgnsDial(1),
    #       connTypeOtherIspDial(2)
    #     }
    #   ngevAccount - Customer account used to start the tunnel
    #   ngevUserid - Customer userid used to start the tunnel
    #   ngtuEndpoint - IP address of the endpoint the tunnel is connected to
    #   ngtuAuthType - Authentication server used by tunnel
    #     INTEGER {
    #       internetOnly(0),
    #       serviceManager(1),
    #       customerDirect(2),
    #       branchOffice(3),
    #       customerDirectSm(4),
    #       noTunnelYet(999)
    #     }
    #   ngtuAuthProt - Authentication protocol
    #     INTEGER {
    #       none(0),
    #       custRadius(1),
    #       custSecureID(2),
    #       custAxent(3),
    #       custCert(4),
    #       custLDAP(5),
    #       custInternal(6),
    #       branchSharedSecret(7),
    #       agnsManaged(8)
    #     }
    #   ngtuEndpointType - Type of endpoint the tunnel is connected to
    #     INTEGER {
    #       none(0),
    #       nortel(3),
    #       linuxFrameSig(4),
    #       gig(5),
    #       linuxSig(6),
    #       cisco(9),
    #       vig(20)
    #     }
    #   ngevTunConReason - Results of the tunnel connection attempt
    #   nghwWanConnectionMethod - Current WAN connection setting - Not from SMX
    #     INTEGER {
    #       wanConfigUnknown(0),
    #       wanConfigStatic (1),
    #       wanConfigDHCP (2),
    #       wanConfigPPPoE (3),
    #       wanConfigDialPri(4),
    #       wanConfigISDNPri(5),
    #       wanConfigPPPoA (6),
    #       wanConfig3GPer (7),
    #       wanConfig3GPri (8)
    #     }
    #   ngtuTunInitiator - What initiated the tunnel
    #     INTEGER {
    #       attUnknown(0),
    #       attFromUser(1),
    #       attAutoLogon(2),
    #       attPersistentLogon(3),
    #       attVpnctl(4)
    #     }
    #   ngtuIpsecIface - IPSec interface for the tunnel
    #   ngtuIPv6Endpoint - IPv6 address of the endpoint the tunnel is connected to
    #   ngevVPNServiceIPv4Address - VPN service IPv4 address assigned to the tunnel connection
    #   ngevVPNServiceIPv6Address - VPN service IPv6 address assigned to the tunnel connection
    #   ngevTunConReasonNUM - Numeric value that indicates the result of the tunnel connection attempt
    # }
    #
    task :tunnel_up do

      connection_id = "10005123445252452422"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      tunnel_mode = "tunnelMaintenance(4)"
      conn_type = "connTypeAgnsDial(1)"
      account = "ATAP"
      user_id = "ACCEL6"
      endpoint = "184.106.13.10"
      auth_type = "serviceManager(1)"
      auth_prot = "agnsManaged(8)"
      endpoint_type = "linuxSig(6)"
      conn_reason = "Successful Logon"
      wan_conn_method = "wanConfigStatic(1)"
      tun_initiator = "attPersistentLogon(3)"
      ipsec_iface = "eth5"
      vpn_ipv4 = "50.13.14.15"
      conn_reason_num = "1"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}",
        :raw_message => "<30>Jul 12 02:15:21 webapp snmptrapd[13542]: 2013-07-12 02:15:11 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}"
      }

      send_trap data
    end

    task :tunnel_primary_up do

      connection_id = "10005123445252452421"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      tunnel_mode = "tunnelUserControl(1)"
      conn_type = "connTypeAgnsDial(1)"
      account = "ATAP"
      user_id = "ACCEL6"
      endpoint = "184.106.13.10"
      auth_type = "serviceManager(1)"
      auth_prot = "agnsManaged(8)"
      endpoint_type = "linuxSig(6)"
      conn_reason = "Successful Logon"
      wan_conn_method = "wanConfigStatic(1)"
      tun_initiator = "attPersistentLogon(3)"
      ipsec_iface = "eth5"
      vpn_ipv4 = "50.13.14.15"
      conn_reason_num = "1"
      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}",
        :raw_message => "<30>Jul 12 02:15:21 webapp snmptrapd[13542]: 2013-07-12 02:15:11 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}"
      }
      send_trap data
    end

    task :tunnel_up_failed do

      connection_id = "10005123445252452422"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      tunnel_mode = "tunnelMaintenance(4)"
      conn_type = "connTypeAgnsDial(1)"
      account = "ATAP"
      user_id = "ACCEL6"
      endpoint = "184.106.13.10"
      auth_type = "serviceManager(1)"
      auth_prot = "agnsManaged(8)"
      endpoint_type = "linuxSig(6)"
      conn_reason = "Failed to find endpoint"
      wan_conn_method = "wanConfigStatic(1)"
      tun_initiator = "attPersistentLogon(3)"
      ipsec_iface = "eth0"
      vpn_ipv4 = "50.13.14.15"
      conn_reason_num = "1"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}",
        :raw_message => "<30>Jul 12 02:15:21 webapp snmptrapd[13542]: 2013-07-12 02:15:11 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelUp#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuTunMode = INTEGER: #{tunnel_mode}#011ATT-NETGATE-MIB::ngevConnectionType.0 = INTEGER: #{conn_type}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngtuEndpoint = IpAddress: #{endpoint}#011ATT-NETGATE-MIB::ngtuAuthType = INTEGER: #{auth_type}#011ATT-NETGATE-MIB::ngtuAuthProt = INTEGER: #{auth_prot}#011ATT-NETGATE-MIB::ngtuEndpointType = INTEGER: #{endpoint_type}#011ATT-NETGATE-MIB::ngevTunConReason.0 = STRING: #{conn_reason}#011ATT-NETGATE-MIB::nghwWanConnectionMethod.0 = INTEGER: #{wan_conn_method}#011ATT-NETGATE-MIB::ngtuTunInitiator = INTEGER: #{tun_initiator}#011ATT-NETGATE-MIB::ngtuIpsecIface = STRING: #{ipsec_iface}#011ATT-NETGATE-MIB::ngevVPNServiceIPv4Address.0 = IpAddress: #{vpn_ipv4}#011ATT-NETGATE-MIB::ngevTunConReasonNUM.0 = INTEGER: #{conn_reason_num}"
      }

      send_trap data
    end

    # ngevTunnelDown - Information about a tunnel disconnect. The one exception is a tunnel 'alive' message, which is sent for a
    # tunnel that is still up. Tunnel 'alive' messages have a ngtuReasonCode of tnlAlive.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevTunConnectionId,
    #   ngevTunnelDateAndTime,
    #   ngtuReasonCode
    #     INTEGER {
    #       discoButton(0),
    #       tnlInitiatedDisco(226),
    #       missedHeartBts(227),
    #       eth1Lost(258),
    #       eth1Restored(259),
    #       powerLost(503),
    #       tnlAlive(504),
    #       deviceStatusInactiveWasActive(506),
    #       noTunnInInetOnlyMode(507),
    #       tnlIdleTimeout(508),
    #       dialIdleTimeout(509),
    #       maxSessionTimeout(510),
    #       noReasonGiven(599)
    #     }
    # }
    #
    task :tunnel_down do

      connection_id = "10005123445252452422"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      reason_code = "tnlIdleTimeout(508)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}",
        :raw_message => "<30>Jul 12 02:41:59 webapp snmptrapd[13542]: 2013-07-12 02:41:49 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}"
      }

      send_trap data
    end

    task :tunnel_primary_down do

      connection_id = "10005123445252452421"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      reason_code = "tnlIdleTimeout(508)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}",
        :raw_message => "<30>Jul 12 02:41:59 webapp snmptrapd[13542]: 2013-07-12 02:41:49 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}"
      }

      send_trap data
    end

    # Uses tunnel down, with different reason code (from MIB)
    task :tunnel_alive do

      connection_id = "10005123445252452422"
      date_and_time = "2013-7-9,20:7:25.0,-4:0"
      reason_code = "tnlAlive(504)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}",
        :raw_message => "<30>Jul 12 02:41:59 webapp snmptrapd[13542]: 2013-07-12 02:41:49 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevTunnelDown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevTunConnectionId.0 = STRING: #{connection_id}#011ATT-NETGATE-MIB::ngevTunnelDateAndTime.0 = STRING: #{date_and_time}#011ATT-NETGATE-MIB::ngtuReasonCode = INTEGER: #{reason_code}"
      }

      send_trap data
    end

    # ngevDialAutotestFailure - The Dial Autotest failed.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevDialTestFailReason - Reason the test failed (str)
    #   ngevDialTestFailCategory
    #     INTEGER {
    #       dialProblemError(0),
    #       dialProblemWarning(1),
    #       dialProblemMessage(2),
    #       dialProblemNoCategory(3)
    #     }
    # }
    #
    task :dial_test_failed do

      fail_reason = "Problem with cellular network"
      fail_category = "dialProblemError(2)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDialAutotestFailure#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevDialTestFailReason.0 = STRING: #{fail_reason}#011ATT-NETGATE-MIB::ngevDialTestFailCategory.0 = INTEGER: #{fail_category}",
        :raw_message => "<30>Jul 12 02:53:26 webapp snmptrapd[13542]: 2013-07-12 02:53:16 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDialAutotestFailure#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevDialTestFailReason.0 = STRING: #{fail_reason}#011ATT-NETGATE-MIB::ngevDialTestFailCategory.0 = INTEGER: #{fail_category}"
      }

      send_trap data
    end

    # ngevDialAutotestFailure - The WAN2 Autotest failed.
    # Note that this is the same event object that is used to note a
    # Dial Autotest failure.  The difference can be found in the details
    # that are sent with the inform
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevDialTestFailReason - Reason the test failed (str)
    #   ngevDialTestFailCategory
    #     INTEGER {
    #       dialProblemError(0),
    #       dialProblemWarning(1),
    #       dialProblemMessage(2),
    #       dialProblemNoCategory(3)
    #     }
    # }
    #
    task :wan2_test_failed do

      fail_reason = "Error bringing up Secondary WAN connection."
      fail_category = "dialProblemWarning(1)"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDialAutotestFailure#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevDialTestFailReason.0 = STRING: #{fail_reason}#011ATT-NETGATE-MIB::ngevDialTestFailCategory.0 = INTEGER: #{fail_category}",
        :raw_message => "<30>Jul 12 02:53:26 webapp snmptrapd[13542]: 2013-07-12 02:53:16 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDialAutotestFailure#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevDialTestFailReason.0 = STRING: #{fail_reason}#011ATT-NETGATE-MIB::ngevDialTestFailCategory.0 = INTEGER: #{fail_category}"
      }

      send_trap data
    end

    # ngevDeviceReboot - Sent when the device reboots. Requestor IP will be set to loopback if an automated process caused the reboot.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevRequestorIp - IPv4 of the reboot requestor
    #   ngevIPv6RequestorIp - IPv6 of the requestor
    # }
    #
    task :reboot do

      requestor_ip = "108.13.51.14"
      requestor_ipv6 = ""

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceReboot#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}",
        :raw_message => "<30>Jul 12 02:59:26 webapp snmptrapd[13542]: 2013-07-12 02:59:16 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceReboot#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}"
      }

      send_trap data
    end

    # ngevDeviceShutdown - Sent when the device shuts down.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevRequestorIp,
    #   ngevIPv6RequestorIp
    # }
    #
    task :shutdown do

      requestor_ip = "108.13.51.14"
      requestor_ipv6 = ""

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceShutdown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}",
        :raw_message => "<30>Jul 12 03:03:51 webapp snmptrapd[13542]: 2013-07-12 03:03:41 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDeviceShutdown#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}"
      }

      send_trap data
    end

    # ngevSshLogin - Sent when a user accesses the device via SSH.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevSshSourceIp,
    #   ngevIPv6RequestorIp
    # }
    #
    task :ssh_login do

      ssh_source_ip = "108.15.94.14"
      ssh_source_ipv6 = ""

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSshLogin#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevSshSourceIp.0 = IpAddress: #{ssh_source_ip}",
        :raw_message => "<30>Jul 12 03:09:01 webapp snmptrapd[13542]: 2013-07-12 03:08:51 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSshLogin#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevSshSourceIp.0 = IpAddress: #{ssh_source_ip}"
      }

      send_trap data
    end

    # ngevDhcpPoolExhausted - Sent when a VLAN's DHCP pool is full.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngvlId - VLAN number (int)
    # }
    #
    task :dhcp_exhausted do

      vlan_id = 4

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDhcpPoolExhausted#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngvlId = INTEGER: #{vlan_id}",
        :raw_message => "<30>Jul 12 03:14:38 webapp snmptrapd[13542]: 2013-07-12 03:14:28 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevDhcpPoolExhausted#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngvlId = INTEGER: #{vlan_id}"
      }

      send_trap data
    end

    # ngevSecondaryAuthAttempt - Sent when a secondary login attempt occurs. Secondary logins are performed
    # when someone tries to ssh into the AT&T NetGate.
    #
    # OBJECTS {
    #   ngevNetGateMac
    #   ngevRequestorIp - IPv4
    #   ngevAccount - Account used for ssh login attempt (str)
    #   ngevUserid - User ID used for ssh login attempt (str)
    #   ngevLoginResult - Result of the login attempt
    #     INTEGER {
    #       failure(0),
    #       success(1)
    #     }
    #   ngevNewLocalID - Userid the user became after successful login attempt
    #   ngevIPv6RequestorIp - IPv6
    # }
    #
    task :secondary_login do

      requestor_ip = "108.15.6.15"
      requestor_ipv6 = ""
      account = "ATAP"
      user_id = "ACCEL5"
      login_result = "success(1)"
      new_local_id = "root"


      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSecondaryAuthAttempt#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngevLoginResult.0 = INTEGER: #{login_result}#011ATT-NETGATE-MIB::ngevNewLocalID.0 = STRING: #{new_local_id}",
        :raw_message => "<30>Jul 12 03:23:27 webapp snmptrapd[13542]: 2013-07-12 03:23:17 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSecondaryAuthAttempt#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngevLoginResult.0 = INTEGER: #{login_result}#011ATT-NETGATE-MIB::ngevNewLocalID.0 = STRING: #{new_local_id}"
      }

      send_trap data
    end

    # ngevSwitchUseridAttempt - Sent when a user who is ssh'ed into the AT&T NetGate tries to switch to another userid.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ngevRequestorIp - IPv4
    #   ngevAccount - Account used for ssh login attempt (str)
    #   ngevUserid - User ID used for ssh login attempt (str)
    #   ngevLoginResult - Result of the login attempt
    #     INTEGER {
    #       failure(0),
    #       success(1)
    #     }
    #   ngevNewLocalID - Userid the user became after successful login attempt
    #   ngevOldLocalID - Userid in use before use made switch
    #   ngevIPv6RequestorIp - IPv6
    # }
    #
    task :ssh_user_switch do

      requestor_ip = "108.15.6.15"
      requestor_ipv6 = ""
      account = "ATAP"
      user_id = "ACCEL5"
      login_result = "success(1)"
      new_local_id = "root"
      old_local_id = "admin"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSwitchUseridAttempt#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngevLoginResult.0 = INTEGER: #{login_result}#011ATT-NETGATE-MIB::ngevNewLocalID.0 = STRING: #{new_local_id}#011ATT-NETGATE-MIB::ngevOldLocalID.0 = STRING: #{old_local_id}",
        :raw_message => "<30>Jul 12 03:33:10 webapp snmptrapd[13542]: 2013-07-12 03:33:00 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevSwitchUseridAttempt#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ngevRequestorIp.0 = IpAddress: #{requestor_ip}#011ATT-NETGATE-MIB::ngevAccount.0 = STRING: #{account}#011ATT-NETGATE-MIB::ngevUserid.0 = STRING: #{user_id}#011ATT-NETGATE-MIB::ngevLoginResult.0 = INTEGER: #{login_result}#011ATT-NETGATE-MIB::ngevNewLocalID.0 = STRING: #{new_local_id}#011ATT-NETGATE-MIB::ngevOldLocalID.0 = STRING: #{old_local_id}"
      }

      send_trap data
    end

    # ngevCellModemRequired - Sent when either the AT&T NetGate boots without a cell modem present or when the
    # AT&T NetGate detects that the cell modem has been removed. The cellular modem must be a supported unit
    # that allows the AT&T NetGate to query its current status.
    #
    # OBJECTS {
    #   ngevNetGateMac,
    #   ng3gModel - 3G card model
    #   ng3gManufacturer - 3G card manufacturer
    #   ng3gID - IMEI or ESN of card
    #   ng3gRevision - 3G card revision
    #   ng3gSIMCardNumber - Number of the card
    #   ng3gPhoneNum - Phone number
    #   ngevCardNotFoundTime - When the card went missing
    # }
    #
    task :cell_modem_required do

      model = "Momentum"
      manufacturer = "Sierra Wireless"
      id = "123135125325151514"
      revision = "1.151.66.11551"
      sim_card_number = "1515151"
      phone = "8137151133"
      not_found_at = "2013-7-9,20:7:25.0,-4:0"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevCellModemRequired#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ng3gModel = STRING: #{model}#011ATT-NETGATE-MIB::ng3gManufacturer = STRING: #{manufacturer}#011ATT-NETGATE-MIB::ng3gID = STRING: #{id}#011ATT-NETGATE-MIB::ng3gRevision = STRING: #{revision}#011ATT-NETGATE-MIB::ng3gSIMCardNumber = STRING: #{sim_card_number}#011ATT-NETGATE-MIB::ng3gPhoneNum = STRING: #{phone}#011ATT-NETGATE-MIB::ngevCardNotFoundTime.0 = STRING: #{not_found_at}",
        :raw_message => "<30>Jul 12 03:42:30 webapp snmptrapd[13542]: 2013-07-12 03:42:20 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevCellModemRequired#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ng3gModel = STRING: #{model}#011ATT-NETGATE-MIB::ng3gManufacturer = STRING: #{manufacturer}#011ATT-NETGATE-MIB::ng3gID = STRING: #{id}#011ATT-NETGATE-MIB::ng3gRevision = STRING: #{revision}#011ATT-NETGATE-MIB::ng3gSIMCardNumber = STRING: #{sim_card_number}#011ATT-NETGATE-MIB::ng3gPhoneNum = STRING: #{phone}#011ATT-NETGATE-MIB::ngevCardNotFoundTime.0 = STRING: #{not_found_at}"
      }

      send_trap data
    end

    # ngevCellModemPluggedIn - Fake event
    task :unknown_event do

      model = "Momentum"
      manufacturer = "Sierra Wireless"
      id = "123135125325151514"
      revision = "1.151.66.11551"
      sim_card_number = "1515151"
      phone = "8137151133"
      not_found_at = "2013-7-9,20:7:25.0,-4:0"

      data = {
        :parsed_message => "#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevCellModemPluggedIn#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ng3gModel = STRING: #{model}#011ATT-NETGATE-MIB::ng3gManufacturer = STRING: #{manufacturer}#011ATT-NETGATE-MIB::ng3gID = STRING: #{id}#011ATT-NETGATE-MIB::ng3gRevision = STRING: #{revision}#011ATT-NETGATE-MIB::ng3gSIMCardNumber = STRING: #{sim_card_number}#011ATT-NETGATE-MIB::ng3gPhoneNum = STRING: #{phone}#011ATT-NETGATE-MIB::ngevCardNotFoundTime.0 = STRING: #{not_found_at}",
        :raw_message => "<30>Jul 12 03:42:30 webapp snmptrapd[13542]: 2013-07-12 03:42:20 <UNKNOWN> [UDP: [192.168.33.1]:55040->[192.168.33.10]]:#012DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (0) 0:00:00.00#011SNMPv2-MIB::snmpTrapOID.0 = OID: ATT-NETGATE-MIB::ngevCellModemPluggedIn#011ATT-NETGATE-MIB::ngevNetGateMac.0 = STRING: #{mac}#011ATT-NETGATE-MIB::ng3gModel = STRING: #{model}#011ATT-NETGATE-MIB::ng3gManufacturer = STRING: #{manufacturer}#011ATT-NETGATE-MIB::ng3gID = STRING: #{id}#011ATT-NETGATE-MIB::ng3gRevision = STRING: #{revision}#011ATT-NETGATE-MIB::ng3gSIMCardNumber = STRING: #{sim_card_number}#011ATT-NETGATE-MIB::ng3gPhoneNum = STRING: #{phone}#011ATT-NETGATE-MIB::ngevCardNotFoundTime.0 = STRING: #{not_found_at}"
      }

      send_trap data
    end

    # ngevCellModemPluggedIn - Fake event
    task :empty_event do

      data = {}

      send_empty_trap data
    end

    task :probe do
      # @results["1.3.6.1.4.1.74.1.30.1.1.0"] = "8200"
      # @results["1.3.6.1.4.1.74.1.30.1.2.0"] = "5.1.104"
      # @results["1.3.6.1.4.1.74.1.30.2.1.0"] = "eth3"
      # @results["1.3.6.1.4.1.74.1.30.1.7.0"] = 4
      # @results["1.3.6.1.4.1.74.1.30.1.3.0"] = "DTREE"
      # @results["1.3.6.1.4.1.74.1.30.2.4.0"] = 0
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.2.1"] = "eth0"
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.5.1"] = ""
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.6.1"] = "1.2.3.4"
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.7.1"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.13.1"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.16.1"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.17.1"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.18.1"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.2.2"] = "eth5"
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.5.2"] = ""
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.6.2"] = "10.6.23.114"
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.7.2"] = 2
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.13.2"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.16.2"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.17.2"] = 1
      # @results["1.3.6.1.4.1.74.1.30.6.1.1.18.2"] = 1
    end

  end

end
