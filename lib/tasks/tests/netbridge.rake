
require 'pp'
require 'json'


namespace :tests do

  namespace :netbridge do

    def netbridge_mac
      "00270401042C"
    end

    def nb_syslog(data)

      syslog = {
        "@source" => "udp://192.168.33.1:52902/",
        "@tags" => [],
        "facility" => "13",
        "syslog_timestamp" => "Feb 11 15:18:18",
        "syslog_host" => "108.166.111.162",
        "mac" => netbridge_mac,
        "device_host" => "208.54.36.243",
        "type" => data[:type],
        "message" => data[:message],
        "@timestamp" => "2013-07-12T03:47:52.895Z",
        "@source_host" => "192.168.33.1",
        "@source_path" => "/",
        "@message" => "<13>Feb 11 15:18:18 108.166.111.162 #{netbridge_mac}/208.54.36.243 #{data[:type]}: #{data[:message]}\n",
        "@type" => "cellular"
      }

      pp syslog
    end

    # All simulates a typical NetBridge startup event sequence and then sends
    # all the error messages afterwards.
    task :all do
      # Events send during normal NetBridge startup
      Rake::Task["tests:netbridge:restart"].invoke
      Rake::Task["tests:netbridge:firmware"].invoke
      Rake::Task["tests:netbridge:modem_new_format"].invoke
      Rake::Task["tests:netbridge:ip_address_update"].invoke
      Rake::Task["tests:netbridge:usb"].invoke
      Rake::Task["tests:netbridge:status"].invoke
      Rake::Task["tests:netbridge:ethernet_down"].invoke
      Rake::Task["tests:netbridge:ethernet_up"].invoke
      Rake::Task["tests:netbridge:dhcp_message"].invoke
      Rake::Task["tests:netbridge:client_connection"].invoke
      Rake::Task["tests:netbridge:config"].invoke
      Rake::Task["tests:netbridge:status_restart"].invoke
      # Remote control events
      Rake::Task["tests:netbridge:remote_config"].invoke
      Rake::Task["tests:netbridge:signal_test"].invoke
      Rake::Task["tests:netbridge:speed_test"].invoke
      # Error events
      Rake::Task["tests:netbridge:apn_mismatch"].invoke
      Rake::Task["tests:netbridge:config_errors"].invoke
      Rake::Task["tests:netbridge:firmware_errors"].invoke
      Rake::Task["tests:netbridge:ipsec_events"].invoke
      # Extras
      Rake::Task["tests:netbridge:modem_old_format"].invoke
    end

    task :config_errors do
      Rake::Task["tests:netbridge:passphrase_missing"].invoke
      Rake::Task["tests:netbridge:passphrase_decrypt_error"].invoke
      Rake::Task["tests:netbridge:config_certificate_missing"].invoke
      Rake::Task["tests:netbridge:config_certificate_untrusted"].invoke
      Rake::Task["tests:netbridge:config_key_mismatch"].invoke
      Rake::Task["tests:netbridge:config_decrypt_error"].invoke
      Rake::Task["tests:netbridge:config_certificate_sign_error"].invoke
    end

    task :firmware_errors do
      Rake::Task["tests:netbridge:getfile_error"].invoke
      Rake::Task["tests:netbridge:firmware_invalid"].invoke
      Rake::Task["tests:netbridge:firmware_write_error"].invoke
      Rake::Task["tests:netbridge:firmware_missing_certificate"].invoke
    end

    task :ipsec_events do
      Rake::Task["tests:netbridge:tunnel_died"].invoke
      Rake::Task["tests:netbridge:tunnel_down"].invoke
      Rake::Task["tests:netbridge:tunnel_established"].invoke
    end

    task :apn_mismatch do
      data = {
        :type => "status",
        :message => "STICKY APN (Current APN does not match configured central APN)"
      }
      nb_syslog data
    end

    task :client_connection do
      data = {
        :type => "mac",
        :message => "eth0=00:27:04:01:04:2C~eth1=00:C0:CA:12:34:56~ip=192.168.210.2"
      }
      nb_syslog data
    end

    task :config do
      data = {
        :type => "config",
        :message => "cfg=config.accns.com~config_change_reboot=1~dhcp_lease_max=5~dlm=5~dre=~drn=~drp=~drs=~eth=auto~gip=192.168.210.1~gip_secondary=1~img=1.442.91~img2=2.151.92~ips_cust_dh_grp=5~ips_keepalive=~iui=0~link_check_freq=4m~link_timeout=480~log=184.106.215.57~mtu=auto~ovr=0~png=8.8.8.8 184.106.215.57 50.28.67.96~rat=auto~remote_control=1~sip=1~sms_check_freq=1m~stf=0~sticky_apn=1~tue=secret~tup=60180~tus=184.106.213.137~upload_server=upload.accns.com~var=A~wdog=1~web_enable=1~xma=managedvpn~xmc=at&t~xmp=notused~xmu=notused"
      }
      nb_syslog data
    end

    task :config_certificate_missing do
      data = {
        :type => "config",
        :message => "Missing certificate; stopping"
      }
      nb_syslog data
    end

    task :config_certificate_sign_error do
      data = {
        :type => "config",
        :message => "Certificate signing failed"
      }

      nb_syslog data
    end

    task :config_certificate_untrusted do
      data = {
        :type => "config",
        :message => "Untrusted certificate; stopping."
      }
      nb_syslog data
    end

    task :config_key_mismatch do
      data = {
        :type => "config",
        :message => "Mismatching public key; stopping."
      }
      nb_syslog data
    end

    task :config_decrypt_error do
      data = {
        :type => "config",
        :message => "Could not decrypt 2.120.42.tar"
      }
      nb_syslog data
    end

    task :dhcp_message do
      data = {
        :type => "dhcp",
        :message => "waiting for dhcp handshake to begin"
      }
      nb_syslog data
    end

    task :ethernet_down do
      data = {
        :type => "eth0",
        :message => "state=unplugged"
      }
      nb_syslog data
    end

    task :ethernet_up do
      data = {
        :type => "eth0",
        :message => "state=active"
      }
      nb_syslog data
    end

    task :firmware do
      data = {
        :type => "firmware",
        :message => "version=2.282.81~serial=6200010792461349~vector=463.0.399.0.0.0"
      }
      nb_syslog data
    end

    task :firmware_invalid do
      data = {
        :type => "firmware",
        :message => "Invalid firmware image"
      }
      nb_syslog data
    end

    task :firmware_write_error do
      data = {
        :type => "firmware",
        :message => "error=write-kernel"
      }
      nb_syslog data
    end

    task :firmware_missing_certificate do
      data = {
        :type => "firmware",
        :message => "No certificate"
      }
      nb_syslog data
    end

    task :getfile_error do
      data = {
        :type => "firmware",
        :message => "Getfile error! Config download failed"
      }
      nb_syslog data
    end

    task :ip_address_update do
      data = {
        :type => "dhcp",
        :message => "pppl=10.164.96.179~dns1=8.8.8.8~dns2=4.2.2.4"
      }
      nb_syslog data
    end

    task :modem_old_format do
      data = {
        :type => "modem",
        :message => "desc=Novatel 760~revision=Q6085BDRAGONFLY_V179~provider=verizon~esn=25016143496~phone=9738971371"
      }
      nb_syslog data
    end

    task :modem_new_format do
      data = {
        :type => "modem",
        :message => "manufacturer=Pantech~model=UML295~provider=verizon~imei=990000695379794~imsi=311480055536447~iccid=89148000000545404817~phone=8133990985~apn=vzwinternet~meid=A1000037393ADF~mcc=311~mnc=480"
      }
      nb_syslog data
    end

    task :passphrase_missing do
      data = {
        :type => "config",
        :message => "No passphrase present in 2.120.42.tar"
      }
      nb_syslog data
    end

    task :passphrase_decrypt_error do
      data = {
        :type => "config",
        :message => "Cannot decrypt passphrase in 2.120.42.tar"
      }
      nb_syslog data
    end

    task :remote_config do
      data = {
        :type => "remote",
        :message => "Heard [config]. Checking config now."
      }
      nb_syslog data
    end

    task :restart do
    # this event sent during startup
      data = {
        :type => "restart",
        :message => "msg=Could not register on network"
      }
      nb_syslog data
    end

    task :signal_test do
      data = {
        :type => "signal",
        :message => "Marginal (-112 dBm) (0%)"
      }
      nb_syslog data
    end

    task :speed_test do
      data = {
        :type => "speed",
        :message => "TX1:0 0.2621 mbps~TX2:0 retrans mbps~TX3:0 0.2621 mbps~TX4:0 retrans mbps~TX5:0 0.2621 mbps~TX6:0 retrans mbps~TX7:0 0.2622 mbps~TX8:0 retrans mbps~TX9:0 0.2621 mbps~TX10:0 retrans mbps~RX1:0 0.5243 mbps~RX2:0 retrans mbps~RX3:0 0.2621 mbps~RX4:0 retrans mbps~RX5:0 0.5244 mbps~RX6:0 retrans mbps~RX7:0 0.2621 mbps~RX8:0 retrans mbps~RX9:0 0.5244 mbps~RX10:0 retrans mbps"
      }
      nb_syslog data
    end

    task :status do
      data = {
        :type => "status",
        :message => "cnt=32~rx=234350~tx=297480~dbm=-83~ecio=-31.500~cnti=LTE~rssi=-77~rsrp=-111.000~rsrq=-15.000~snr=-3.400~sinr=9.000~mtu=1422~prl=VER:12135~css=300,D,19~qcmipep=1~roam=All~usb=12"
      }
      nb_syslog data
    end

    task :status_restart do
    # this event sent before device actually reboots
      data = {
        :type => "status",
        :message => "restart (configuration update)"
      }
      nb_syslog data
    end

    task :tunnel_died do
      data = {
        :type => "ipsec",
        :message => "phase1 with 192.168.211.50 died; attempting to rescue"
      }
      nb_syslog data
    end

    task :tunnel_down do
      data = {
        :type => "ipsec",
        :message => "phase1 with 192.168.211.50 down"
      }
      nb_syslog data
    end

    task :tunnel_established do
      data = {
        :type => "ipsec",
        :message => "phase1 with 192.168.211.50 established"
      }
      nb_syslog data
    end

    task :usb do
      data = {
        :type => "usb",
        :message => "bo=y~usb1_2=0424:2412~usb1_4=0f3d:68aa"
      }
      nb_syslog data
    end

  end

end
