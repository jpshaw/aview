require 'pp'
require 'json'


namespace :tests do

  namespace :remote_manager do

    def rm_mac
      "002704030276"
    end

    def rm_syslog(data)

      syslog = {
        "@source" => "udp://192.168.33.1:52902/",
        "@tags" => [],
        "facility" => "13",
        "syslog_timestamp" => "Feb 11 15:18:18",
        "syslog_host" => "108.166.111.162",
        "mac" => rm_mac,
        "device_host" => "208.54.36.243",
        "type" => data[:type],
        "message" => data[:message],
        "@timestamp" => "2013-07-12T03:47:52.895Z",
        "@source_host" => "192.168.33.1",
        "@source_path" => "/",
        "@message" => "<13>Feb 11 15:18:18 108.166.111.162 #{rm_mac}/208.54.36.243 #{data[:type]}: #{data[:message]}\n",
        "@type" => "remote_manager"
      }

      pp syslog
    end

    task :all do
      Rake::Task["tests:remote_manager:firmware"].invoke
      Rake::Task["tests:remote_manager:modem"].invoke
      Rake::Task["tests:remote_manager:signal_status"].invoke
      Rake::Task["tests:remote_manager:location"].invoke
      Rake::Task["tests:remote_manager:dhcp"].invoke
      Rake::Task["tests:remote_manager:network_details"].invoke
      Rake::Task["tests:remote_manager:network_dns"].invoke
      Rake::Task["tests:remote_manager:network_status"].invoke
      Rake::Task["tests:remote_manager:config"].invoke
    end


    task :config do
      data = {
        type: 'config',
        message: 'status=Failed to load config from all servers'
      }
      rm_syslog data
    end

    task :dhcp do
      data = {
        type: 'dhcp',
        message: 'pppl=10.210.203.83~dns1=172.16.0.1~dns2=10.0.2.254~dns3=10.2.2.36'
      }
      rm_syslog data
    end

    task :firmware do
      data = {
        type: 'firmware',
        message: 'version=1.0.7-rm~serial=5300020001091448'
      }
      rm_syslog data
    end

    task :location do
      data = {
        :type => 'location',
        :message => 'type=modem~idx=0~mcc=505~mnc=1~lac=14433~cid=3602~lat=-27.538268~lon=153.003823~alt=27.600000'
      }
      rm_syslog data
    end

    task :modem do
      data = {
        :type => 'modem',
        :message => 'idx=0~apn=m2m.com.attz~imei=357784044140786~imsi=~phone=unknown~iccid=89011704258018972863~provider=unknown~manufacturer=HuaweiTechnologiesCo.Ltd.~model=MU609~revision=12.107.07.05.457~usb=480'
      }
      rm_syslog data
    end

    task :network_details do
      data = {
        :type => 'network',
        :message => 'type=modem~idx=0~intf=eth1~mtu=1500~rx=907328~tx=1336075~default=pri~ip0=10.210.203.83/29~gw0=10.210.203.81~metric0=3'
      }
      rm_syslog data
    end

    task :network_dns do
      data = {
        :type => 'network',
        :message => 'type=dns~dns0=172.16.0.1~dns1=10.0.2.254~dns2=10.2.2.36'
      }
      rm_syslog data
    end

    task :network_status do
      data = {
        :type => 'network',
        :message => 'type=ethernet~intf=eth0~mtu=1500~link=ok~speed=1000baseT-FD~rx=88224~tx=81242~default=alt~ip0=192.168.0.86/24~gw0=192.168.0.2~metric0=5~ip1=192.168.210.1/24'
      }
      rm_syslog data
    end

    task :signal_status do
      data = {
        :type => 'status',
        :message => 'type=modem~idx=0~intf=eth1~dbm=-64~per=80~cnti=umts~rx=941692~tx=1383585'
      }
      rm_syslog data
    end

  end

end
