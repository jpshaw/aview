namespace :accelerated do

  namespace :role_abilities do

    desc "Sync role abilities"
    task sync: :environment do
      class AbilitiesRole < ActiveRecord::Base
        belongs_to :ability
        belongs_to :role
      end

      count = 0

      puts "Creating role abilities..."
      AbilitiesRole.all.each do |ar|
        unless RoleAbility.exists?(role_id: ar.role, ability_id: ar.ability)
          RoleAbility.create(role: ar.role, ability: ar.ability)
          count += 1
        end
      end

      puts "  -> #{count} created"
    end

  end

end
