require 'engine'

namespace :engine do |args|
  desc 'Displays information about the currently loaded engine'
  task :info do
    Engine.display_info
  end

  desc 'Loads the given engine'
  task :load, [:engine] do |t, args|
    Engine.load(args[:engine])
  end

  desc 'Unloads the currently loaded engine'
  task :unload do
    Engine.unload
  end

  desc 'Unloads the currently loaded engine and then loads the given engine'
  task :reload, [:engine] do |t, args|
    Engine.reload(args[:engine])
  end
end

desc 'Alias for engine:info'
task engine: 'engine:info'
