namespace :users do

  desc 'Migrate user `first_name` + `last_name` to `name`'
  task :migrate_names => :environment do
    User.find_each(batch_size: 100) do |user|
      # Only update if the user's name isn't already set
      next if user.name.present?

      name = "#{user.first_name} #{user.last_name}".split.map(&:capitalize).join(' ')
      next if name.blank?

      user.update_attributes(name: name)
    end
  end

  desc 'Set notify_enabled to false if email is blank'
  task :disable_notifications_if_email_blank => :environment do
    User.find_each(batch_size: 100) do |user|
      user.update_attributes(notify_enabled: false) if user.email.blank?
    end
  end

end