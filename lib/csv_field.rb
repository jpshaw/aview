class CSVField

  def initialize(string = nil, opts = {})
    self.string           = string
    self.keep_duplicates  = opts[:keep_duplicates]  || false
    self.keep_nils        = opts[:keep_nils]        || false
    self.array            = to_array
  end


  def <<(value)
    array << value
  end


  def to_s
    array.uniq!     unless keep_duplicates
    array.compact!  unless keep_nils
    array.join(',')
  end


  alias_method :to_csv, :to_s

  private

  attr_accessor :string, :keep_duplicates, :keep_nils, :array

  def to_array
    string.to_s.split(',')
  end

end
