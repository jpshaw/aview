require 'report'

class VigReport < Report

  def initialize(report_record = nil)
    super(report_record)

    categories
  end

  def title
    'VIG Report'
  end

  def device_categories
    categories
  end

  def generate!
    generate_data
    report_record.table        = table
    report_record.finished_at  = Time.now.utc
    report_record.save!
  end

  def table
    table = []

    results.each do |device, data|
      table << [
        device.mac,
        device.host,
        device.category_name,
        device.series_name,
        (device.organization.name  if device.organization),
        device.account_name,
        (device.site.name          if device.site),
        (device.deployed? ? "Deployed" : "Undeployed"),
        device.last_heartbeat_at,
        {
          'vig_name'     => data[:vig_name],
          'vig_ip'       => data[:vig_ip],
          'vig_location' => data[:vig_location]
        }
      ]
    end

    table
  end

  private

  def categories
    # @categories is used internally in Report#device_models, in report.rb.
    @categories ||= DeviceCategory.where(name: DeviceCategory::GATEWAY_CATEGORY)
  end

  def generate_data
    report_record.devices.find_each do |device|
      next unless device.respond_to?(:connected_to_vig?) && device.connected_to_vig?

      # Default values
      results[device] = {
        vig_name:     nil,
        vig_ip:       nil,
        vig_location: nil
      }

      tunnel        = device.tunnels.connected_to_vig.first
      tunnel_server = tunnel.tunnel_server

      results[device][:vig_ip]       = tunnel.endpoint_ip_address
      results[device][:vig_name]     = tunnel_server.name     if tunnel_server
      results[device][:vig_location] = tunnel_server.location if tunnel_server
    end

    update_report_record_stats_headers
  end

  def update_report_record_stats_headers
    report_record.params.merge!(
      stats_headers: {
        'vig_name'     => 'VIG Name',
        'vig_ip'       => 'VIG IP',
        'vig_location' => 'VIG Location'
      }
    )
    report_record.save!
  end

end
