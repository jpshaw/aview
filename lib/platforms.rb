# Base module for all device platforms.
#
# A device platform exposes a set of methods for handling common actions like
# processing events, sending commands, configuration, etc. It is closely tied
# to the operating system each device runs. For example, the Netbridge device
# runs linux using `Buildroot`, whereas the 6300-CX, Remote Manager and Dial
# Capture devices run linux using `uClinux`. It is assumed that each device on
# a platform understands the same set of communication protocols and messages.
module Platforms
  NONE = '(none)'.freeze
  class EventError < StandardError; end
end
