module Presenters
  class Base
    attr_reader :view_context
    alias_method :view, :view_context

    delegate :current_user, :params, :check_box_tag, :link_to, to: :view

    def initialize(view_context)
      @view_context = view_context
    end

    protected

    def organizations_map
      @organization_map ||= current_user.organizations.select(:id, :name).map { |o| { o.name => o.id } }.reduce(:merge)
    end
  end
end
