class DurationCalculator

  DURATIONS = {
    'w' => :weeks,
    'd' => :days,
    'h' => :hours,
    'm' => :minutes,
    's' => :seconds
  }.freeze


    # Converts a string with 1 or more NB duration values, where N is a numeric value and B is a
    # duration base (w, d, h, m, or s for weeks, days, hours, minutes, or seconds), to a number of
    # seconds. All values are added and repeated bases are allowed (i.e.: 1m1m = 2m).
    # Examples:
    #   Duration.calculate('1s')        # =>       1
    #   Duration.calculate('2m')        # =>     120
    #   Duration.calculate('3h')        # =>   10800
    #   Duration.calculate('4d')        # =>  345600
    #   Duration.calculate('5w')        # => 3024000
    #   Duration.calculate('1d1h1m')    # =>   90060
    #   Duration.calculate('1d 1h 1m')  # =>   90060
    #   Duration.calculate('1d 1h 1m')  # =>   90060
    #   Duration.calculate('1m1m')      # =>     120

  def self.calculate(duration_string)
    duration_keys = duration_string.split(/\d+/).drop(1)  # First elem is an empty string. Drop it.

    raise if duration_keys.empty?

    duration_values = duration_string.split(/[#{DURATIONS.keys.join}]/)
    seconds         = 0

    duration_values.each_with_index do |value, index|
      seconds += value.to_i.__send__(DURATIONS[duration_keys[index]])
    end

    seconds
  rescue => e
    raise "Unknown durations found or malformed string in '#{duration_string}'. Valid durations: #{DURATIONS.keys}."
  end

end
