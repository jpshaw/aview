class DeviceFactory
  # Attempts to find a device class for the given attributes. If found, it will
  # initialize an instance of the device with the data.
  def self.new(attributes = {})
    find_class(attributes).try(:new, attributes)
  end

  # Attempts to find a device class for the given attributes. If found, it will
  # create an instance of the device with the data. If validation errors occur
  # the record will not be persisted.
  def self.create(attributes = {})
    find_class(attributes).try(:create, attributes)
  end

  # Attempts to find a device class for the given attributes. If found, it will
  # create an instance of the device with the data. If any errors are
  # encountered when validating they will be raised as exceptions.
  def self.create!(attributes = {})
    find_class(attributes).try(:create!, attributes)
  end

  def self.find_class(attributes = {})
    Device.subclasses.each do |klass|
      return klass if klass.valid_identifiers?(attributes)
    end
    nil
  end
  private_class_method :find_class
end
