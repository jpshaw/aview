module Forms

  class ContactForm
    include Virtus.model

    # ActiveModel plumbing to make `form_for` work
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    # Forms are never themselves persisted
    def persisted?
      false
    end

    # Stores the newly created/already existing contact object
    attr_reader :contact

    # Virtus Attributes
    attribute :name, String
    attribute :email, String
    attribute :phone, String
    attribute :label, Integer
    attribute :token, String

    #Validations
    validates :name, :presence => true, :length => { :minimum => 3, :maximum => 100 }
    validates :email, :presence => true, :length => { :maximum => 100 }
    validates_format_of :email, :with => /\A[^@]+@[^@]+\z/
    validates :phone, :length => { :maximum => 50 }
    validates :label, :presence => true
    validates :label, numericality: { only_integer: true }
    validates :token, :presence => true, :length => { :is => 32 }

    # Public: Cleans up the form data for the passed email and phone number
    # and set's the contact's token
    def process_data
      self.email = self.email.strip.downcase
      self.phone ||= ""
      set_token
    end

    # Public: Used to retrieve an existing contact if a token was set
    def retrieve_contact
      @contact = Contact.where(:token => self.token).first
    end

    # Public: Attempts to create a contact object based on whether the form
    # data is valid
    def save
      self.process_data
      if valid?
        persist!
      else
        false
      end
    end

    # Public: Creates a hash of the Form::Contact objects data without the
    # token field. Used for passing contact data in a redirect
    def to_hash_without_token
      contact_hash = self.to_hash
      contact_hash.delete(:token)
      contact_hash
    end

    private

    # Private: Creates a token for the new contact object
    def set_token
      self.token = Contact.token(self.email)
    end

    # Private: Creates and returns a contact object, either one that existed already
    # or a newly created one
    def persist!
      @contact = Contact.where(:token => self.token)
                        .first_or_create(:name => self.name, :email => self.email,
                                         :phone => self.phone, :label => self.label)
    end
  end

end
