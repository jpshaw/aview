module ContactLabels
  GENERAL   = 0
  TECHNICAL = 1
  SALES     = 2
  SITE      = 3

  # Public: Returns an array of label names and their value
  def self.select_list
    [["General Contact",GENERAL],["Technical Contact",TECHNICAL],["Sales Contact",SALES],["Site Contact",SITE]]
  end
end
