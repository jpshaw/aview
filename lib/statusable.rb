require 'devices'

module Statusable
  extend ActiveSupport::Concern
  include Devices

  # Determines the circle color on device lists
  def status_indicator
    case self.device_status
    when Status::UP
      'fa fa-circle status-up'
    when Status::NOTICE
      'fa fa-circle status-notice'
    when Status::DOWN
      'fa fa-circle status-down'
    when Status::BACKUP_MODE
      'fa fa-circle warning status-backup'
    when Status::TUNNEL_DOWN
      'fa fa-circle status-tunnel'
    when Status::INACTIVE
      'fa fa-circle status-inactive'
    when Status::UNDEPLOYED
      'fa fa-pause status-undeployed'
    when Status::VRRP_MASTER
      'fa fa-circle warning status-vrrp-master'
    when Status::DUAL_WAN_BOTH_UP
      'fa fa-circle fa-circle-green'
    when Status::DUAL_WAN_BOTH_DOWN
      'fa fa-circle fa-circle-red'
    when Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
      'fa fa-circle fa-circle-orange'
    when Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
      'fa fa-circle fa-circle-blue'
    else
      'fa fa-question-circle'
    end
  end

  # Color shown on the device show page
  def status_class
    case self.device_status
    when Status::UP
      'status-up'
    when Status::NOTICE
      'status-notice'
    when Status::DOWN
      'status-down'
    when Status::BACKUP_MODE
      'warning status-backup'
    when Status::TUNNEL_DOWN
      'status-tunnel'
    when Status::INACTIVE
      'status-inactive'
    when Status::UNDEPLOYED
      'status-undeployed'
    when Status::VRRP_MASTER
      'warning status-vrrp-master'
    when Status::DUAL_WAN_BOTH_UP
      'status-dual-wan-both-up'
    when Status::DUAL_WAN_BOTH_DOWN
      'status-dual-wan-both-down'
    when Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
      'status-dual-wan-wan1-up-wan2-down'
    when Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
      'status-dual-wan-wan1-down-wan2-up'
    end
  end

  def status_color
    case self.device_status
    when Status::UP
      'green'
    when Status::NOTICE
      'yellow'
    when Status::DOWN
      'red'
    when Status::BACKUP_MODE
      'blue'
    when Status::TUNNEL_DOWN
      'purple'
    when Status::INACTIVE
      'black'
    when Status::UNDEPLOYED
      'pause'
    when Status::VRRP_MASTER
      'pink'
    when Status::DUAL_WAN_BOTH_UP
      'green'
    when Status::DUAL_WAN_BOTH_DOWN
      'red'
    when Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
      'orange'
    when Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
      'blue'
    else
      'grey'
    end
  end

  def status_pin
    ActionController::Base.helpers.asset_path("markers/#{status_color}_marker.png")
  end

  # Shown when hovering over table icon
  def status_title
    case self.device_status
    when Status::UP
      'Up'
    when Status::NOTICE
      'Notice'
    when Status::DOWN
      'Down'
    when Status::BACKUP_MODE
      'Backup Mode'
    when Status::TUNNEL_DOWN
      'Tunnel Down'
    when Status::INACTIVE
      'Inactive'
    when Status::UNDEPLOYED
      'Undeployed'
    when Status::VRRP_MASTER
      'VRRP Master'
    when Status::DUAL_WAN_BOTH_UP
      'Dual WAN Up'
    when Status::DUAL_WAN_BOTH_DOWN
      'Dual WAN Down'
    when Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
      'WAN1 Up / WAN2 Down'
    when Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
      'WAN1 Down / WAN2 Up'
    else
      'Down'
    end
  end

  # Shown on the device show page
  def status_description
    description =
      case self.device_status
      when Status::UP
        'up'
      when Status::NOTICE
        'on notice'
      when Status::DOWN
        'down'
      when Status::BACKUP_MODE
        'up but in backup mode'
      when Status::TUNNEL_DOWN
        'up but user-controlled tunnel is down'
      when Status::INACTIVE
        'inactive'
      when Status::UNDEPLOYED
        'undeployed'
      when Status::VRRP_MASTER
        'up. Device is in VRRP Master mode'
      when Status::DUAL_WAN_BOTH_UP
        'up in Dual WAN mode - WAN1 Up / WAN2 Up'
      when Status::DUAL_WAN_BOTH_DOWN
        'down in Dual WAN mode - WAN1 Down / WAN2 Down'
      when Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
        'up in Dual WAN mode - WAN1 Up / WAN2 Down'
      when Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
        'up in Dual WAN mode - WAN1 Down / WAN2 Up'
      else
        'unknown'
      end

    "Device is #{description}"
  end

  def status_icon
    [status_color, status_description]
  end

end
