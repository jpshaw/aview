module Presenters
  module Devices
    autoload :Base,          'devices/base'
    autoload :Cellular,      'devices/cellular'
    autoload :DialToIp,      'devices/dial_to_ip'
    autoload :Gateway,       'devices/gateway'
    autoload :Netbridge,     'devices/netbridge'
    autoload :Netreach,      'devices/netreach'
    autoload :RemoteManager, 'devices/remote_manager'
    autoload :Ucpe,          'devices/ucpe'
  end
end

require 'presenters/base'
