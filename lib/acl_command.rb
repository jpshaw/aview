require 'active_support/core_ext/date/calculations'
require 'jasper/api/client'

class AclCommand < DeviceCommand

  # Public: Attempts to send a command to a device. Depending on the type of
  # command being sent, it will send an sms command or a regular tunnel
  # command.
  def send!
    if command.present?
      sms_command? ? send_sms_command : send_regular_command
    else
      command_failed!
      @message = "Invalid Command"
      @device.event info: @message, type: :remote, level: :error
    end
  end

  private

  # Internal: Checks to see if a command is present and if it is an sms command
  def sms_command?
    command.present? && command.match(/^sms_/ix) != nil
  end

  # Internal: Attempts to send a sms command to an ACL device
  def send_sms_command
    command, command_message = which_command
    if sms_validations_successful?
      unless @device.sent_sms_recently?
        @device.event info: "Attempting to #{command_message} via SMS ...",
                      type: :remote
        begin
          message = encrypted_message_for(command)
          if jasper_api_enabled?
            result = Jasper::API::Client.new().send_sms(@device.modem.iccid, message)
          else
            result = ::SMS::CommandService.new(@device.modem_number, message).execute
          end
          @device.modem.touch :sms_sent_at
          command_succeeded!
          @message = "SMS Command sent successfully."
          @device.event info: @message, type: :remote, data: result
        rescue => e
          command_failed!
          @message = "Failed to send SMS command."
          @device.event info: @message,
                        type: :remote, level: :error, data: { error: "#{e.message}", message: message }
        end
      else
        command_failed!
        @message = "An SMS command was sent to this device in the last 60 "\
                   "seconds, please wait #{@device.modem.time_left_til_sms_allowed} "\
                   "seconds and try again"
        @device.event info: @message,
                      type: :remote, level: :error
      end
    else
      command_failed!
      @message = "Unable to send SMS command. Please verify device has a modem "\
                 "and a valid phone number for an authorized carrier."
      @device.event info: @message, type: :remote, level: :error
    end
  end

  # Internal: Attempts to send a regular tunnel command to an ACL device.
  def send_regular_command

    command_type = case command
    when "restart"
      @device.event info: "Attempting to reboot device ...", type: :remote
      "restart"
    when "config"
      @device.event info: "Attempting to update device configuration ...",
            type: :remote
      "config"
    when "status"
      @device.event info: "Attempting to probe device ...", type: :remote
      "status"
    when "signal"
      @device.event info: "Attempting to check the device signal strength ...",
            type: :remote
      "signal"
    when "speed"
      @device.event info: "Attempting to perform speed test ...", type: :remote
      "speed"
    when "ping"
      @device.event info: "Attempting to ARPing attached device ...",
            type: :remote
      "ping"
    when "wake"
      @device.event info: "Attempting to send Wake-on-LAN to attached device ...",
            type: :remote
      "wake"
    when "modem_firmware"
      @device.event info: "Attempting to update firmware on the device's modem...",
            type: :remote
      "modem_firmware"
    when /^remote-power/
      @device.event info: "Attempting to send power toggle command to attached Remote Power device...",
            type: :remote
      command
    when /^sim/
      @device.event info: "Attempting to send SIM slot switch command to the device's modem...",
            type: :remote
      command
    end

    send_tunnel_command_to_device_ip(command_type)
  end

  # Internal: Used to make a tunnel connection and send a command through a
  # TCP socket connection to the ACL device
  def send_tunnel_command_to_device_ip(command_type = "")
    return command_failed! unless command_type.present?
    device_ip = @device.command_ip_addr || mac_to_ip(@device.mac)
    device_mac = get_mac_from_ip(device_ip)
    return unless device_mac.present?
    if device_mac.upcase == @device.mac.upcase
      begin
        Timeout.timeout(10) do
          TCPSocket.open(device_ip, 60181, local_ip) { |s| s.puts command_type }
          command_succeeded!
          @device.event info: "Successfully sent command to device at IP #{device_ip}", type: :remote
        end
      rescue => e
        command_failed!
        @device.event info: "Failed to send command #{command_type} to MAC #{@device.mac} on port 60180 at IP: #{device_ip} - device unreachable. Check status of the device's tunnel and that the Remote Control service is enabled in its config",
            type: :remote, level: :error, data: { error: "#{e.message}" }
      end
    else
      command_failed!
      @device.event info: "Command not sent - MAC address mismatch at IP #{device_ip}: expected #{@device.mac} but found #{device_mac}",
          type: :remote, level: :error
    end
  end

  # The local IPv4 address of the server the app is running on
  def local_ip
    @local_ip ||= Socket.ip_address_list.detect{|intf| intf.ipv4? && intf.ip_address != '127.0.0.1' }.ip_address
  end

  def mac_to_ip(mac)
   # Use the last four digits to convert mac to ip. This matches
   # what is on the ACL firmware for devices with static MAC-to-IPs
   return "172.%d.%d.%d" % [mac[0,2].to_i(16)+16,
                            "0x#{mac[2,2]}",
                            "0x#{mac[4,2]}"]
  end

  # validate MAC address of device at the IP address
  def get_mac_from_ip(ip)
    mac = nil
    begin
      Timeout.timeout(10) do
        html = Nokogiri::HTML(open "https://#{ip}", {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE})
        mac = html.at('body').inner_text.scan(/002704\h{6}/).first
      end
    rescue => e
      command_failed!
      @device.event info: "HTTPS Timeout reaching MAC #{@device.mac} at IP: #{ip} - device unreachable. Check the status of the device's aView tunnel and that the Web administration service is enabled in its config",
          type: :remote, level: :error, data: { error: "#{e.message}" }
    end
    return mac
  end

  # Internal: Gets the command and command message for the available command.
  #
  # Returns an array with the sms_command and it's particular command message.
  def which_command
    case command
    when "sms_config"
      ["config%#{command_data}", "set a configuration"]
    when "sms_reset_config"
      ["configurationreset%", "reset the configuration"]
    when "sms_factory_reset"
      ["factoryreset%", "reset device to factory default"]
    when "sms_firmware_reset"
      ["firmwarereset%", "reset the firmware to factory default"]
    when "sms_reboot"
      ["reboot%", "reboot the device"]
    when "sms_remote_control"
      ["remotecontrol%", "open the remote control tunnel"]
    when "sms_power_cycle"
      ["remote-power%#{command_data} repower", "power cycle outlet"]
    end
  end

  # Internal: Creates the encrypted message used for sms commands.
  #
  # message - A command String that needs to be encrypted.
  #
  # Returns a encrypted string of the passed command message.
  def encrypted_message_for(message = " ")
    # AES cipher
    cipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")

    # Initialize
    cipher.encrypt

    # Create 8 byte string of random characters
    salt = Array.new(8).map { |i| rand(255).chr }.join

    # Generate key, initialization vector using mac & salt
    cipher.pkcs5_keyivgen(@device.mac.downcase, salt, 1)

    # Encrypt
    e = cipher.update(message) + cipher.final
    e = "Salted__#{salt}#{e}" # OpenSSL compatible
    "accns%#{Base64.strict_encode64(e).chomp}%accns"
  end

  def jasper_api_enabled?
    @jasper_api_enabled ||= Settings.api_keys.jasper.enabled?
  end

  def sms_validations_successful?
    return false unless command.present?
    return false unless @device.can_receive_sms_commands?
    return true
  end

end
