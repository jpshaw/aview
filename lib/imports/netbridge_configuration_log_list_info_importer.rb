class NetbridgeConfigurationLogListInfoImporter

  def import(csv_row, configuration)
    if csv_row[:log_value].present?
      csv_field =   CSVField.new(configuration.log_list)
      csv_field <<  csv_row[:log_value]
      configuration.update_attribute(:log_list, csv_field.to_s)
    end
  end

end
