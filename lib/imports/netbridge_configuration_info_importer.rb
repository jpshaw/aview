class NetbridgeConfigurationInfoImporter

  TRUTHY                = /(y|yes|t|true|1)/i.freeze
  FALSY                 = /(n|no|f|false|0)/i.freeze
  ETHERNET_PORT_SPEEDS  = {
                            '1' => 'auto',
                            '2' => 'hdx10',
                            '3' => 'fdx10',
                            '4' => 'hdx100',
                            '5' => 'fdx100',
                            '6' => 'hdx1000',
                            '7' => 'fdx1000'
                          }.freeze
  CELL_NETWORK_TYPES    = {
                            '1' => 'auto',
                            '2' => '2g',
                            '3' => '3g',
                            '4' => '4g'
                          }.freeze

  def import(csv_row, organization)
    self.row                      = csv_row
    configuration                 = organization
                                      .netbridge_configurations
                                      .where(name: row[:name])
                                      .first_or_initialize
    configuration.dlm             = row[:dlm]
    configuration.xma             = row[:xma]
    configuration.xmu             = row[:xmu]
    configuration.xmp             = row[:xmp]
    configuration.rbt             = to_boolean(row[:rbt])
    configuration.remote_control  = to_boolean(row[:remote_control])
    configuration.sticky_apn      = to_boolean(row[:sticky_apn])
    configuration.ippassthrough   = to_boolean(row[:ippassthrough])
    configuration.usb_adjust      = row[:usb_adjust]
    configuration.eth             = ethernet_port_speed
    configuration.gip_secondary   = to_boolean(row[:gip_secondary])
    configuration.rat             = cell_network_type
    configuration.mtu             = row[:mtu]
    configuration.img2            = row[:img2]
    configuration.cfg             = row[:cfg]
    configuration.hostname        = row[:hostname]
    configuration.xmc             = row[:xmc]
    configuration.dnslist         = row[:dnslist]
    configuration.dhcp_lease_max  = row[:dhcp_lease_max].to_i if row[:dhcp_lease_max].present?
    configuration.drs             = row[:drs]
    configuration.dre             = row[:dre]
    configuration.drp             = row[:drp]
    configuration.drn             = row[:drn]
    configuration.gip             = row[:gip]

    configuration.save!

    configuration
  end


  private

  attr_accessor :row

  def ethernet_port_speed
    ETHERNET_PORT_SPEEDS[row[:eth]]
  end


  def cell_network_type
    CELL_NETWORK_TYPES[row[:rat]]
  end


  def to_boolean(value)
    return true   if truthy?(value)
    return false  if falsy?(value)
  end


  def truthy?(value)
    value == true || value.to_s =~ TRUTHY
  end


  def falsy?(value)
    value == false || value.to_s =~ FALSY
  end

end
