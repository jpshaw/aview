class SmxDeviceInfoImporter

  attr_reader :new_record

  def import(csv_row, organization, location, site)
    device   = Device.find_by(mac: csv_row[:mac])
    device ||= Device.find_by_serial(csv_row[:serial]) if csv_row[:serial].present?
    device ||= DeviceFactory.new(mac: csv_row[:mac], serial: csv_row[:serial], organization_id: organization.id)

    if device.present?
      self.new_record       = device.new_record?
      device.organization   = organization
      device.location       = location
      device.site           = site
      device.is_deployed    = csv_row[:is_deployed]

      device.save!
    end

    device
  end


  def new_record?
    new_record
  end


  private

  attr_writer :new_record

end
