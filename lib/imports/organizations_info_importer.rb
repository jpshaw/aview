class OrganizationsInfoImporter

  def import(csv_row)
    self.row = csv_row

    get_device_organization
    build_organization_hierarchy

    organization
  end


  private

  attr_accessor :row, :organization

  def get_device_organization
    self.organization = get_organization_for_name(row[:device_organization_name])
    raise_organization_error('device') unless organization
  end


  def build_organization_hierarchy
    parent_organization = \
      if row[:parent_organization_name].present?
        get_organization_for_name(row[:parent_organization_name])
      else
        root_org
      end

    raise_organization_error('parent') unless parent_organization

    add_org_child(parent: parent_organization,  child: organization)
    add_org_child(parent: root_org,             child: parent_organization)
  end


  def get_organization_for_name(name)
    organization = (Organization.find_by(import_name: name) || Organization.find_by(name: name))

    if organization
      # If `import name` is blank, set it to `name`.
      organization.update_attribute(:import_name, name) if organization.import_name.blank?
      organization
    else
      create_organization_for_name(name)
    end
  end


  def create_organization_for_name(name)
    params = { name: name, import_name: name }
    result = ::OrganizationCreator.call(params: params)
    result.organization if result.success?
  end


  def root_org
    @root_org ||= Organization.root
  end


  def add_org_child(opts)
    parent = opts[:parent]
    child  = opts[:child]

    # Do not build association if it already exists.
    unless parent.descendants.include?(child) || parent == child
      parent.add_child(child)
    end
  end


  def raise_organization_error(organization_qualifier)
    raise "Could not find or create #{organization_qualifier} organization from data: #{row.inspect}"
  end

end
