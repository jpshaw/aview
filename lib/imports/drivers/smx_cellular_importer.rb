require 'imports/smx_device_info_importer'
require 'imports/organizations_info_importer'
require 'imports/location_info_importer'
require 'imports/site_info_importer'
require 'imports/netbridge_configuration_info_importer'
require 'imports/netbridge_configuration_log_list_info_importer'
require 'imports/netbridge_configuration_ping_test_info_importer'
require 'csv_field'


class SmxCellularImporter
  include ::Kraken::Logger.file

  DEVICE_INFO_TYPE    = '1'.freeze
  SYSLOG_INFO_TYPE    = '2'.freeze
  PING_INFO_TYPE      = '3'.freeze
  YES                 = 'Y'.freeze
  ACTIVE              = 'A'.freeze
  DELETE_RETRY_LIMIT  = 5
  MACS_SLICE_SIZE     = 100
  SYSLOG_SERVER_INDEX = 4
  PING_ENTRY_INDEX    = 4

  private_class_method :new


  # Instantiates the processor and processes the import file.
  #
  # file_path - The String path to the import file.
  #
  # Returns a Hash of processed objects.
  def self.import_file(file_path)
    new(file_path).process
  end


  # Processes the import file.
  #
  # Returns a Hash of processed objects.
  def process
    CSV.foreach(file_path, headers: true, encoding: 'ISO-8859-1') do |csv_row|
      self.row  = csv_row
      self.mac  = row['DEVICE_ID'].upcase

      begin
        ActiveRecord::Base.transaction do
          if device_row?
            processed[:devices][:scanned] << mac
            process_device_row unless skippable?
          end

          unless skipped?
            process_syslog_row  if syslog_row?
            #TODO: Un-comment when ping_test column exists in netbridge_configurations table.
            # process_ping_row    if ping_row?
          end
        end
      rescue => e
        processed[:devices][:skipped] << mac unless processed[:devices][:skipped].include?(mac)
        logger.error e.message
      end
    end

    delete_unprocessed_devices
    logger.debug processed
    processed
  end


  private

  attr_accessor :file_path, :row, :mac, :device, :device_organization, :processed

  def initialize(file_path)
    self.file_path = file_path
  end


  def device_row?
    row_type == DEVICE_INFO_TYPE
  end


  def syslog_row?
    row_type == SYSLOG_INFO_TYPE
  end


  def ping_row?
    row_type == PING_INFO_TYPE
  end


  def row_type
    row['RECORD_TYPE']
  end


  def process_device_row
    smx_device_info_importer  = SmxDeviceInfoImporter.new
    self.device               = smx_device_info_importer.import(
                                  device_row,
                                  organization,
                                  location,
                                  site
                                )

    if device.present?
      device.configuration = configuration if device.platform == :buildroot
      processed[:devices][:added] << mac if smx_device_info_importer.new_record?
    else
      raise "Could not create device from data: #{row.inspect}"
    end
  end


  def device_row
    {
      mac:          mac,
      serial:       row['SERIAL#'],
      is_deployed:  row['DEVICE_STATUS'] == ACTIVE
    }
  end


  def organization
    self.device_organization = OrganizationsInfoImporter.new.import(organization_row)
  end


  def organization_row
    {
      device_organization_name: row['ACCOUNT_ID'],
      parent_organization_name: row['CUSTOMER_NAME']
    }
  end


  def location
    LocationInfoImporter.new.import(location_row)
  end


  def location_row
    {
      address_1:    row['ADDRESS_LINE1'],
      address_2:    row['ADDRESS_LINE2'],
      city:         row['CITY'],
      state:        row['STATE'] || row['PROVINCE'],
      postal_code:  row['ZIP_CODE'],
      country:      row['COUNTRY']
    }
  end


  def site
    SiteInfoImporter.new.import(site_row, device_organization)
  end


  def site_row
    {
      name: row['DESCRIPTION']
    }
  end


  def configuration
    NetbridgeConfigurationInfoImporter.new.import(configuration_row, device_organization)
  end


  def configuration_row
    {
      name:           mac,
      dlm:            row['DHCP_LEASE_TIME'],
      xma:            row['APN'],
      xmu:            row['APN_USERID'],
      xmp:            row['APN_PASSWORD'],
      rbt:            row['REBOOT_CONFIG_UPD'],
      remote_control: row['REMOTE_CONTROL_TNL'],
      sticky_apn:     row['KEEP_APN_STICKY'],
      ippassthrough:  row['ENABLE_PASSTHROUGH'],
      usb_adjust:     row['USB_MODE'],
      eth:            row['ETH_PORT_SPEED'],
      gip_secondary:  row['SECONDARY_GW_IP'],
      rat:            row['CELL_NETWORK_TYPE'],
      mtu:            row['MTU_SIZE'],
      img2:           row['TARGET_SOFTWARE'],
      cfg:            row['SOFTWARE_UP_PATH'],
      hostname:       row['DEVICE_HOSTNAME'],
      xmc:            row['TARGET_CARRIER'],
      dnslist:        configuration_dns_list,
      dhcp_lease_max: row['DHCP_LEASE_MAX'],
      drs:            row['DHCP_START_ADDR'],
      dre:            row['DHCP_END_ADDR'],
      drp:            row['DHCP_PREFIX'],
      drn:            row['DHCP_NW_ADDR'],
      gip:            row['DHCP_GW_ADDR']
    }
  end


  def configuration_dns_list
    dns_list =  CSVField.new(row['PRIMARY_DNS_ADDR'])
    dns_list << row['SECONDARY_DNS_ADDR']
    dns_list.to_s
  end


  def skippable?
    return false if (device_organization_name? && armt_enabled?)
    processed[:devices][:skipped] << mac
    true
  end


  def device_organization_name?
    row['ACCOUNT_ID'].present?
  end


  def armt_enabled?
    row['ARMT_ENABLED'] == YES
  end


  def skipped?
    processed[:devices][:skipped].include? mac
  end


  def process_syslog_row
    NetbridgeConfigurationLogListInfoImporter.new.import(syslog_row, device.configuration)
  end


  def syslog_row
    {
      log_value: row[SYSLOG_SERVER_INDEX]
    }
  end


  def process_ping_row
    NetbridgeConfigurationPingTestInfoImporter.new.import(ping_row, device.configuration)
  end


  def ping_row
    {
      ping_value: row[PING_ENTRY_INDEX]
    }
  end


  def delete_unprocessed_devices
    removable_macs.each_slice(MACS_SLICE_SIZE) do |macs_slice|
      Device.where(mac: macs_slice).each do |device|
        retries = 0

        begin
          device.destroy
          processed[:devices][:removed] << device.mac
        rescue ActiveRecord::StaleObjectError
          retries += 1

          if retries <= DELETE_RETRY_LIMIT
            # Remove the device state, the most frequent source of state object errors.
            device.device_state.delete if device.device_state.present?

            logger.warn "#{self.class.to_s} failed to remove device '#{device.mac}' - \
                        automatically retrying after StaleObjectError (attempt = #{retries})"
            retry   # This retries the 'begin' block not the 'do' block.
          else
            logger.error "#{self.class.to_s} failed to remove device '#{device.mac}' due to StaleObjectError"
          end
        end
      end
    end
  end


  def removable_macs
    return [] unless processed[:devices][:scanned].any?
    devices_macs - processed[:devices][:scanned]
  end


  def devices_macs
    Netbridge.pluck(:mac) + Cellular.pluck(:mac)
  end


  def processed
    @processed ||= {
      devices: {
        added:   [],
        scanned: [],
        removed: [],
        skipped: []
      },
      customers: {
        added: []
      },
      accounts: {
        added: []
      },
      sites: {
        added: []
      },
      locations: {
        added: []
      },
      configurations: {
        added: []
      }
    }
  end

end
