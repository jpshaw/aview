class NetbridgeConfigurationPingTestInfoImporter

  def import(csv_row, configuration)
    if csv_row[:ping_value].present?
      csv_field =   CSVField.new(configuration.log_list)
      csv_field <<  csv_row[:ping_value]
      configuration.update_attribute(:ping_test, csv_field.to_s)
    end
  end

end
