class SiteInfoImporter

  def import(csv_row, organization)
    organization.sites.for_name(csv_row[:name])
  end

end
