class LocationInfoImporter

  def import(csv_row)
    return unless csv_row[:address_1].present? && csv_row[:city].present?

    ::Locations::Upsert.execute(
      address_1:   csv_row[:address_1],
      address_2:   csv_row[:address_2],
      city:        csv_row[:city],
      state_code:  csv_row[:state],
      postal_code: csv_row[:postal_code],
      country:     csv_row[:country]
    )
  end

end
