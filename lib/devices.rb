# Public: Base for all device-related classes and modules.
module Devices

  class Deployment

    ALL        = 0
    DEPLOYED   = 1
    UNDEPLOYED = 2

    #TODO: Delete once the new filter toolbar is implemented across the site
    # ["All", 0], ["Deployed", 1], ["Undeployed", 2]
    def self.select_list
      [
        ["All",        ALL       ],
        ["Deployed",   DEPLOYED  ],
        ["Undeployed", UNDEPLOYED]
      ]
    end

    def self.facet_values
      {
        'Deployed' => DEPLOYED,
        'Undeployed' => UNDEPLOYED,
      }
    end

  end

  module Status
    # Green
    UP                         = 10
    DUAL_WAN_BOTH_UP           = 11

    # Pink
    VRRP_MASTER                = 20

    # Orange
    DUAL_WAN_WAN1_UP_WAN2_DOWN = 30

    # Blue
    DUAL_WAN_WAN1_DOWN_WAN2_UP = 40
    BACKUP_MODE                = 41

    # Purple
    TUNNEL_DOWN                = 46

    # Yellow
    NOTICE                     = 50

    # Red
    DUAL_WAN_BOTH_DOWN         = 60
    DOWN                       = 61

    # Grey
    UNREACHABLE                = 70
    INACTIVE                   = 71

    # Pause
    UNDEPLOYED                 = 80
  end

  # Reference to previous status codes
  module LegacyStatus
    UP                         = 1
    UNREACHABLE                = 2
    DOWN                       = 3
    UNDEPLOYED                 = 4
    INACTIVE                   = 5
    TUNNEL_DOWN                = 6
    BACKUP_MODE                = 7
    VRRP_MASTER                = 8
    DUAL_WAN_BOTH_UP           = 9
    DUAL_WAN_BOTH_DOWN         = 10
    DUAL_WAN_WAN1_UP_WAN2_DOWN = 11
    DUAL_WAN_WAN1_DOWN_WAN2_UP = 12
  end

end
