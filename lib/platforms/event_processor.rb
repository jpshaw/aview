require 'platforms/event_types'
require 'platforms/event_callbacks'

module Platforms
  # Base event processor class for device source types. The given event's type
  # is used to determine which method to invoke. If no method exists, a default
  # event is called and a generic message is generated.
  #
  # Examples
  #
  #   class SyslogProcessor < Platforms::EventProcessor
  #     event_type :status
  #
  #     def status
  #       # ... process event
  #     end
  #   end
  #
  #
  class EventProcessor
    include Platforms::EventTypes
    include Platforms::EventCallbacks

    attr_reader :device, :event

    delegate :type, :raw, to: :event, prefix: true, allow_nil: true

    def initialize(device, event)
      @device = device
      @event  = event
    end

    def run
      run_callbacks { run_event_method }
    end

    def event_method
      @event_method ||= self.class.event_types[event_type] || default_method
    end

    def self.run(device, event)
      new(device, event).run
    end

    private

    def run_callbacks(&block)
      before_callbacks.each { |callback| run_method(callback) }
      result = block.call
      after_callbacks.each { |callback| run_method(callback) }
      result
    end

    def run_event_method
      run_method(event_method)
    end

    def run_method(name)
      __send__(name)
    end

    def default_method
      :default_event
    end

    # If no methods are setup to handle the event, a default event is generated.
    def default_event
      device.event(
        info:  event_type.try(:humanize),
        type:  event_type,
        level: :info,
        data:  { event_type => event_raw }
      )
    end
  end
end
