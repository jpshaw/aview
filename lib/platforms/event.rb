module Platforms
  # The base event received from Logstash. The replaces the old way of handling
  # events by passing around a hash and can be composed into event sources
  # for further inspection, such as for specific syslog events.
  #
  # Examples
  #
  #   event = Platforms::Event.from_json('{"type": "status", "message": "cnt=1~rx=2~tx=3"}')
  #    => #<Platforms::Event:0x007f8efbd7cf80 @type="status", @message="cnt=1~rx=2~tx=3">
  #   event.type
  #    => "status"
  #
  class Event
    include ActiveModel::Validations

    SYSLOG  = 'syslog'
    SNMP    = 'snmp'
    UNKNOWN = 'unknown'

    SOURCES = [SYSLOG, SNMP]

    attr_reader :facility, :facility_label, :host, :logsource, :mac, :message,
      :node_name, :orig_host, :orig_hostip, :priority, :priority_raw, :raw,
      :severity, :severity_label, :tags, :timestamp, :version

    validate :mac_address

    def initialize(attrs = {})
      attrs.each do |key, value|
        value = value.strip if value.is_a? String
        instance_variable_set("@#{key}", value)
      end
    end

    def type
      "#{@type}".underscore
    end

    # TODO: Set the source in logstash
    def source
      @source ||= begin
        SOURCES.each { |s| return s if tags.include?(s) }
        UNKNOWN
      end
    end

    def syslog?
      source == SYSLOG
    end

    def snmp?
      source == SNMP
    end

    def self.from_json(json)
      event = JSON.parse(json)
      new(decorate(event))
    end

    # Fix any keys with a `@` prefix
    def self.decorate(event)
      event.inject({}) do |hash, (key, value)|
        key = key.gsub(/[^a-zA-Z_0-9]/, '_')
        hash[key] = value
        hash
      end
    end
    private_class_method :decorate

    private

    def mac_address
      unless mac.to_s.valid_mac?
        errors.add(:mac, 'is invalid')
      end
    end
  end
end
