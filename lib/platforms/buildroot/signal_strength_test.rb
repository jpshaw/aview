module Platforms
  module Buildroot

    # Excellent (-51 dBm) (100%) (LTE CNTI) (-51 RSSI) (-71 RSRP) (-8 RSRQ) (-99.000 SNR) (23.400 SINR) (0.000 Ec/Io)
    class SignalStrengthTest

      REGEX = %r{
        (?<message>\w{1,15})\s
        \((?<dbm>-\d{1,3})\sdBm\)\s
        \((?<percent>\d{1,3})%\)
        (\s\((?<cnti>.*)\sCNTI\))?
        (\s\((?<rssi>.*)\sRSSI\))?
        (\s\((?<rsrp>.*)\sRSRP\))?
        (\s\((?<rsrq>.*)\sRSRQ\))?
        (\s\((?<snr>.*)\sSNR\))?
        (\s\((?<sinr>.*)\sSINR\))?
        (\s\((?<ecio>.*)\sEC\/IO\))?
      }ix

      attr_reader :message, :dbm, :percent, :cnti, :rssi, :rsrp, :snr, :sinr, :ecio

      def initialize(string)
        if matches = string.match(REGEX)
          attrs = Hash[matches.names.zip(matches.captures)]
          attrs.each do |key, value|
            instance_variable_set("@#{key}", value)
          end
        end
      end

      def to_h
        @hash ||= instance_values.with_indifferent_access
      end
      alias_method :to_hash, :to_h

      def [](key)
        to_h[key]
      end
    end
  end
end
