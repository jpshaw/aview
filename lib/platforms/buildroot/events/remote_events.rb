require 'platforms/buildroot/signal_strength_test'

module Platforms::Buildroot::RemoteEvents
  extend ActiveSupport::Concern

  included do
    event_type :arping, :remote, :signal, :sms, :speed
  end

  def arping
    logger "#{syslog.raw}", type: :remote, data: { arping: syslog.data }
  end

  def remote
    logger 'Remote command received', type: :remote
  end

  # signal: Marginal (-112 dBm) (0%)
  # Excellent (-51 dBm) (100%) (LTE CNTI) (-51 RSSI) (-71 RSRP) (-8 RSRQ) (-99.000 SNR) (23.400 SINR) (0.000 Ec/Io)
  def signal
    results = Platforms::Buildroot::SignalStrengthTest.new(syslog.data)

    message = 'Signal Test Result'

    if results[:message] and results[:dbm] and results[:percent]
      message += ": #{results[:message]} #{results[:dbm]} dBm (#{results[:percent]}%)"
    end

    logger message, type: :remote, data: { signal: results.to_h }
  end

  # sms: SMS reboot~message received.~Sent 15/04/07,12:40
  # sms: Rebooting now
  def sms
    if syslog.raw =~ /~/
      # Creates a message that looks like:
      #   SMS reboot message received. Sent 15/04/07,12:40
      message = syslog.raw.split('~').join(' ')

    else
      message = "SMS Message: #{syslog.raw}"
    end

    logger message, data: { sms: syslog.data }
  end

  # TX1:0 0.2621 mbps~TX2:0 retrans mbps~TX3:0 0.2621 mbps~TX4:0 retrans mbps~TX5:0 0.2621 mbps~TX6:0 retrans mbps~TX7:0 0.2622 mbps~TX8:0 retrans mbps~TX9:0 0.2621 mbps~TX10:0 retrans mbps~RX1:0 0.5243 mbps~RX2:0 retrans mbps~RX3:0 0.2621 mbps~RX4:0 retrans mbps~RX5:0 0.5244 mbps~RX6:0 retrans mbps~RX7:0 0.2621 mbps~RX8:0 retrans mbps~RX9:0 0.5244 mbps~RX10:0 retrans mbps
  def speed
    # We can expect a hash for `speed test` messages
    results = syslog.to_h(pair_separator: ':')
    logger 'Speed Test Results', type: :remote, data: { speed: results }
  end
end
