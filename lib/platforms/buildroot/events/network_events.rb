require 'ds_flow_report'

module Platforms::Buildroot::NetworkEvents
  extend ActiveSupport::Concern

  included do
    event_type :dhcp, :dsflow, :eth0, :mac, :network_usage, :sshd
    alias_event :networkusage, :network_usage
  end

  # dhcp: waiting for dhcp handshake to begin
  # dhcp: pppl=100.64.161.76~dns1=198.224.181.135~dns2=198.224.178.135
  def dhcp
    case syslog.data
    when Hash
      syslog.data.each do |key, value|
        next unless value.present? and device.respond_to?("#{key}=")
        device.__send__("#{key}=", value)
      end

      if device.host_changed?
        from = device.host_was || Platforms::NONE
        to   = device.host     || Platforms::NONE
        logger "Cellular WAN IP changed from #{from} to #{to}", type: :dhcp,
          uuid: device.uuid_for(:ip_changed)
      else
        logger 'DHCP status', type: :dhcp
      end

      device.save! if device.changed?
    else
      logger syslog.data.to_s, type: :dhcp
    end
  end

  # dsflow: N1=60602~N2=487030~N3=246373~N4=5016470~N5=417605255~N6=6527987
  def dsflow
    # We can expect a hash for `dsflow` messages
    report = DSFlowReport.new(syslog.to_h)
    logger 'DS Flow Report', data: { results: report.to_h }
  end

  def eth0
    # We can expect a hash for `eth0` messages
    data = syslog.to_h

    if data.present? && data['state']
      device_details.update_attribute(:eth_state, data['state'])
      logger "Ethernet port state: #{data['state']}", type: :dhcp, level: :notice
    else
      logger 'Ethernet port status', type: :dhcp
    end
  end

  def mac
    # We can expect a hash for `mac` messages
    data = syslog.to_h

    if data.blank?
      logger 'Ethernet status', type: :dhcp
      return
    end

    # Check for ethernet devices
    if data.keys.map { |key| key.include?('eth') }.any?
      device_details.eth_macs = {}

      data.each do |key, value|
        next unless key.include?('eth')

        begin
          device_details.eth_macs[key.to_sym] = MacAddress.new(value).to_s.upcase
        rescue ArgumentError
          logger "Invalid mac reported for #{key}: #{value}", type: :dhcp, level: :warning
        end
      end
    end

    device_details.client_ip = data['ip']
    device_details.save! if device_details.changed?

    if device.client_mac && device.client_ip
      setup_association_with_client
      logger "Device handed IP #{device.client_ip} to MAC #{device.client_mac}", type: :dhcp
    else
      logger 'Ethernet status', type: :dhcp
    end
  end

  def setup_association_with_client
    device.associate_with_mac(device.client_mac, ip_address: device.client_ip,
                                                source_type: :lan,
                                           source_interface: :eth0)
  end

  # NetworkUsage: Unable to check data usage. SMS not supported
  # NetworkUsage: remaining=~used=~unit_measurement=
  # NetworkUsage: remaining=5058.61~used=61.3901~unit_measurement=MB~through_date=2015-04-20T17:42:45.000Z
  def network_usage
    case syslog.data
    when Hash

      data = syslog.data

      if data[:remaining] and data[:used] and data[:unit_measurement]
        units          = data[:unit_measurement]
        data_used      = "#{data[:used]} #{units}"
        data_remaining = "#{data[:remaining]} #{units}"

        logger "Network Usage: #{data_used} used and #{data_remaining} remaining."
      else
        logger 'Network Usage'
      end

    when /unable to check data usage/i
      logger syslog.data.to_s
    else
      logger 'Network Usage'
    end
  end
  alias_method :networkusage, :network_usage

  # sshd: SSH session started.  Session ends in 10 minutes
  # sshd: Server error. Session not started
  # sshd: Error copying key to server! Not starting session! Error:
  # sshd: SSH session time ended. Session stopped
  def sshd
    logger "SSHD: #{syslog.raw}", data: { sshd: syslog.data }
  end
end
