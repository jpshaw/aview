require 'platforms/buildroot/firmware_download'

module Platforms::Buildroot::FirmwareEvents
  extend ActiveSupport::Concern

  included do
    event_type :firmware
  end

  # firmware: version=2.391.31~serial=6200010070381321~vector=33.0.9.0.0.0~date=Sun Apr 12 02:26:30 UTC 2015
  # firmware: download=configuration.accns.com/configurations/00270401065F/device_config
  # firmware: download=aview.accns.com/device_firmware/AcceleratedConcepts/6200-FX/2.391.31.tar
  # firmware: Getfile error! Config download failed
  # firmware: No certificate.
  # firmware: write=uImage:/dev/mtd4
  # firmware: write=rootfs.squashfs:/dev/mtd6
  # firmware: Firmware update successful.
  def firmware
    case syslog.data
    when Hash
      data            = syslog.data
      device.firmware = data[:version].strip.upcase if data[:version].present?
      device.serial   = data[:serial].strip         if data[:serial].present?

      if device.firmware_changed?
        from = device.firmware_was || Platforms::NONE
        to   = device.firmware     || Platforms::NONE

        logger "Firmware changed from #{from} to #{to}", type: :firmware,
          uuid: device.uuid_for(:fw_changed)
      else
        logger "Device using firmware version #{device.firmware}", type: :firmware
      end

      device.save! if device.changed?

    when /download=/
      download_event = Platforms::Buildroot::FirmwareDownload.parse(syslog.data)
      logger download_event.message, type: download_event.type
    when /update successful/i
      logger 'Firmware update successful', type: :firmware
    when /error/i, /invalid/i
      logger 'Unable to check configuration', level: :error, type: :firmware
    when /no certificate/i
      logger 'Device missing SSL certificate files', level: :error, type: :firmware
    when /write=/
      logger 'Applying firmware. Please do not power off the device', level: :alert, type: :firmware
    else
      logger 'Firmware status', type: :firmware
    end
  end

end
