module Platforms::Buildroot::UsbEvents
  extend ActiveSupport::Concern

  USB        = 'usb'
  USB_BO     = 'bo'
  USB_BO_YES = 'y'

  included do
    event_type :usb
  end

  # usb: bo=y~usb1_2=0424:2412~usb1_4=0f3d:68aa
  def usb
    # We can expect a hash for `usb` messages
    data = syslog.to_h

    if data[USB_BO] && data[USB_BO] == USB_BO_YES
      device_details.usb_hub = 'Connected'
    else
      device_details.usb_hub = nil
    end

    if data.keys.map { |key| key.include?(USB) }.any?
      usb_list = []

      data.each do |key, value|
        next unless key.include?(USB)
        usb_list << value
      end

      device_details.usb_list = usb_list.join(',')
    else
      device_details.usb_list = nil
    end

    device_details.save! if device_details.changed?

    logger 'USB hub status'
  end
end
