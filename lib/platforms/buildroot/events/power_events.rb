module Platforms::Buildroot::PowerEvents
  extend ActiveSupport::Concern

  included do
    event_type :info, :power
  end

  # info: No Remote Power device detected! Unable to send command.
  # info: Unable to communicate with Remote Power device [$serial]. No interface available.
  # info: Unable to send cmd to Remote Power unit ($rc)
  def info
    logger syslog.data.to_s
  end

  # power: port1=OFF~port2=ON
  # power: Set outlet [1] to state [1] on Remote Power unit [2-2].
  def power
    case syslog.data
    when Hash
      ports    = syslog.data
      port_ids = []

      ports.each do |name, state|
        port_id = name[/\d+$/].to_i
        port_ids << port_id

        outlet = device.power_outlets.where(port: port_id).first_or_create

        # Ensure the outlet state is up to date
        state =~ /on/i ? outlet.turn_on! : outlet.turn_off!

        logger "Power outlet ##{port_id} is #{state.downcase}"
      end

      if port_ids.any?
        device.power_outlets.where('port not in (?)', port_ids).destroy_all
      else
        device.power_outlets.destroy_all
      end

    when /no remote power device detected/i
      logger 'No remote power devices detected'
      device.power_outlets.destroy_all

    when /set outlet/i
      logger syslog.data

    else
      logger 'Power outlet status'
    end
  end

end
