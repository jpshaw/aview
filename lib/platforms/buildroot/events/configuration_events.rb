module Platforms::Buildroot::ConfigurationEvents
  extend ActiveSupport::Concern

  included do
    event_type :certificates, :config
  end

  # certificates: Certificate URL changed. Generating new certificate.
  # certificates: Certificate doesn't match.
  # certificates: Certificate is invalid. Requesting new cert.
  def certificates
    logger syslog.data.to_s
  end

  def config
    case syslog.data
    when Hash
      logger 'Configuration successfully loaded', type: :config
    when /No passphrase present/i, /Cannot decrypt/i, /Corrupted file/i, /Missing/i, /Untrusted/i, /stopping/i, /Could not/i
      logger 'Failed to upgrade firmware', type: :config, level: :error
    when /certificate signing failed/i
      logger 'Device is missing SSL certificates', type: :firmware, level: :error
    else
      logger 'Configuration', type: :config
    end
  end
end
