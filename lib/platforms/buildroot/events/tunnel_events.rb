module Platforms::Buildroot::TunnelEvents
  extend ActiveSupport::Concern

  included do
    event_type :ipsec
  end

  def ipsec
    case syslog.data
    when /established/i
      create_tunnel_log "IPSec tunnel established with address #{syslog.ip_address}"
      update_tunnel_status 'Connected'
    when /down/i
      create_tunnel_log "IPSec tunnel with address #{syslog.ip_address} is down"
      update_tunnel_status 'Not Connected'
    when /attempting to rescue/i
      create_tunnel_log 'Attempting to reestablish IPSec tunnel'
      update_tunnel_status 'Not Connected'
    when /rescued/i
      create_tunnel_log "Rescued IPSec tunnel with address #{syslog.ip_address}"
      update_tunnel_status 'Connected'
    else
      create_tunnel_log 'IPSec tunnel status'
    end
  end

  private

  def create_tunnel_log(message)
    logger message, level: :notice, type: :tunnel
  end

  def update_tunnel_status(status)
    device_details.update_attribute(:tunnel_status, status)
  end
end
