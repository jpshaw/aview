require 'platforms/buildroot/modem_status'

module Platforms::Buildroot::ModemStatusEvents
  extend ActiveSupport::Concern

  included do
    alias_event :modem,  :modem_status
    alias_event :status, :modem_status
  end

  # status: cnt=1~rx=3315~tx=4381~dbm=-53~ecio=0.000~cnti=LTE~rssi=-53~rsrq=-8.000~snr=-99.000~sinr=0.000~mtu=1428~cid=~lac=9B00~usb=12
  # status: STICKY APN (Current APN does not match configured central APN)
  # status: restart forced restart
  # modem: manufacturer=Pantech~model=290~revision=L0290VWBB12F.248~provider=verizon~imei=990000479162713~imsi=311480023569585~iccid=89148000000232074634~phone=2678384151~apn=vzwinternet
  def modem_status
    event = Platforms::Buildroot::ModemStatus.new(device, syslog)

    case event.data
    when Hash
      if event.modem.cnti_changed?
        from = event.modem.cnti_was || Platforms::NONE
        to   = event.modem.cnti     || Platforms::NONE
        logger "Cellular network changed from #{from} to #{to}",
          type: :modem, level: :notice, uuid: device.uuid_for(:cnti_changed)
      else
        logger event.message, type: :modem
      end

      if event.modem.signal_changed? && event.modem.signal_below_threshold?
        percentage = AdminSettings.threshold_percentage
        logger "Signal below threshold of '#{percentage}%'",
          type: :modem, uuid: device.uuid_for(:signal_changed)
      end

      if event.modem.changed?
        event.modem.save! 
        device.deactivate_old_modems(event.modem)
      end

    when /sticky apn/i
      logger 'Device connected using incorrect APN', type: :modem, level: :warning
    when /restart/i
      logger 'Device attempting to reboot ...', type: :reboot
    else
      logger 'Modem status', type: :modem
    end
  end

end
