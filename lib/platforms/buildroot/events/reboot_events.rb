module Platforms::Buildroot::RebootEvents
  extend ActiveSupport::Concern

  included do
    event_type :restart
    alias_event :reboot, :restart
  end

  def restart
    device.update_attribute(:last_restarted_at, Time.now)

    reason = \
      case syslog.data
      when /msg.*\=/
        syslog.data.gsub(/msg.*\=/, '')
      when /restart\s*\w.*/
        syslog.data.gsub(/restart\s*/, '')
      else
        nil
      end

    message = 'Device rebooted'
    message += ": #{reason}" if reason

    logger message, type: :reboot, level: :notice, uuid: device.uuid_for(:rebooted)
  end
end
