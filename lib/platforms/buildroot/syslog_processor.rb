require 'platforms/event_processor'
require 'platforms/buildroot/syslog_event'
require 'platforms/buildroot/events/configuration_events'
require 'platforms/buildroot/events/firmware_events'
require 'platforms/buildroot/events/modem_status_events'
require 'platforms/buildroot/events/network_events'
require 'platforms/buildroot/events/power_events'
require 'platforms/buildroot/events/reboot_events'
require 'platforms/buildroot/events/remote_events'
require 'platforms/buildroot/events/tunnel_events'
require 'platforms/buildroot/events/usb_events'

module Platforms
  module Buildroot
    class SyslogProcessor < Platforms::EventProcessor

      include ConfigurationEvents
      include FirmwareEvents
      include ModemStatusEvents
      include NetworkEvents
      include PowerEvents
      include RebootEvents
      include RemoteEvents
      include TunnelEvents
      include UsbEvents

      def default_event
        case syslog.data
        when Hash
          logger event_type.try(:humanize)
        else
          logger syslog.data
        end
      end

      protected

      def device_details
        device.details ||= device.build_details
      end

      def syslog
        @syslog ||= SyslogEvent.new(event)
      end

      def logger(message, opts = {})
        details = { info: message, type: event_type, level: :info }.merge(opts)
        details[:data] ||= { details[:type] => syslog.data }
        device.event(details)
      end
    end
  end
end
