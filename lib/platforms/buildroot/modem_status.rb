# status: restart forced restart
# status: cnt=1~rx=3315~tx=4381~dbm=-53~ecio=0.000~cnti=LTE~rssi=-53~rsrq=-8.000~snr=-99.000~sinr=0.000~mtu=1428~cid=~lac=9B00~usb=12
module Platforms
  module Buildroot
    class ModemStatus

      MODEM_KEY_MAP = {
                 dbm: :signal,
                rx30: :rx_30,
                tx30: :tx_30,
                desc: :name,
        manufacturer: :maker,
               phone: :number,
            provider: :carrier,
                 usb: :usb_speed
      }.freeze

      attr_reader :device, :event
      delegate :data, to: :event, allow_nil: true

      def initialize(device, event)
        @device = device
        @event = event

        assign_modem_data if data.is_a?(Hash)
      end

      def modem
        return @modem unless @modem.blank?
        
        where_clause = {}

        if modem_data[:iccid].present? 
          where_clause[:iccid] = modem_data[:iccid]
        elsif modem_data[:esn].present? # some older USB 3G-only modems don't have an ICCID
          where_clause[:esn] = modem_data[:esn]
        elsif modem_data[:maker] && modem_data[:model]
          where_clause[:maker] = modem_data[:maker]
          where_clause[:model] = modem_data[:model]
        else
          where_clause[:active] = true
        end

        @modem = device.modems.where(where_clause).first_or_create
      end

      # Example: Modem status. Signal: 45%, CNTI: LTE
      def message
        message = 'Modem status.'

        parts = []
        parts << "Signal: #{modem.signal_pct}" if modem.signal.present?
        parts << "CNTI: #{modem.cnti}" if modem.cnti.present?

        if parts.any?
          message += ' '
          message += parts.join(', ')
        end

        message
      end

      def assign_modem_data

        modem_data.each do |key, value|
          next unless value.present? && modem.respond_to?("#{key}=")
          modem.__send__("#{key}=", value)
        end

        if modem_data[:maker] && modem_data[:model]
          modem.name = "#{modem_data[:maker]} #{modem_data[:model]}"
        end

        modem.active = true
      end

      def modem_data
        @modem_data ||= mapped_data.select { |k,v| v.present? }.with_indifferent_access
      end

      def mapped_data
        @mapped_data ||= rename_hash_keys(data.dup, MODEM_KEY_MAP)
      end

      def rename_hash_keys(hash, keys_pairs)
        keys_pairs.each do |old_key, new_key|
          hash[new_key] = hash.delete(old_key) if hash.has_key?(old_key)
        end

        hash
      end
    end
  end
end
