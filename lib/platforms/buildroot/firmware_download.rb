# # config
# download=184.106.201.130/002704/0108FE.mac,try=2
# download=aview.accns.com/configurations/002704010078/device_config
# download=config.accns.com/002704/010717.mac,try=3
# download=configuration.accns.com/002704/010501.mac
# download=configuration.accns.com/configurations/002704010158/device_config,try=3
# download=distro.accns.com/configurations/0027040100F0/device_config,try=3
#
# # firmware
# download=aview.accns.com/device_firmware/AcceleratedConcepts/6200-FX/2.391.31.tar
# download=aview.accns.com/device_firmware/AcceleratedConcepts/6200-FX/ .tar
# download=184.106.201.130/002704/2.391.31.tar,try=3
#
# download=.*\/[a-z0-9.]*.tar
#
# # old
# download=184.106.201.130/002704/0108FE.mac,try=2
# download=config.accns.com/002704/010717.mac,try=3
# download=configuration.accns.com/002704/010501.mac
#
module Platforms
  module Buildroot
    class FirmwareDownload
      DOWNLOAD_REGEX          = /download=/i
      DOWNLOAD_HOST_REGEX     = /download=(?<host>[a-z0-9\-.]+)\//i
      DOWNLOAD_FIRMWARE_REGEX = /download=.*\/(?<firmware>[0-9a-z.]+).tar/i #/download=.*\/*.tar/i
      DOWNLOAD_CONFIG_REGEX   = /download=.*\/*(.mac|device_config)/i

      attr_reader :string

      def initialize(string)
        raise ArgumentError.new('Invalid firmware download event') unless string =~ DOWNLOAD_REGEX
        @string = string
      end

      def message
        @message ||= begin
          if config?
            "Downloading firmware config from host: #{download_host}"
          elsif firmware?
            "Downloading firmware '#{firmware}' from host: #{download_host}"
          else
            "Downloading #{download_path}"
          end
        end
      end

      def type
        @type ||= begin
          if string =~ DOWNLOAD_CONFIG_REGEX
            :config
          elsif string =~ DOWNLOAD_FIRMWARE_REGEX
            :firmware
          else
            :status
          end
        end
      end

      def download_host
        @download_host ||= begin
          if match = string.match(DOWNLOAD_HOST_REGEX)
            match[:host]
          else
            download_path
          end
        end
      end

      def firmware
        return nil unless firmware?
        if match = string.match(DOWNLOAD_FIRMWARE_REGEX)
          return match[:firmware]
        end
      end

      def download_path
        @download_path ||= string.gsub(DOWNLOAD_REGEX, '')
      end

      def config?
        self.type == :config
      end

      def firmware?
        self.type == :firmware
      end

      def unknown?
        self.type == :unknown
      end

      def self.parse(string)
        FirmwareDownload.new(string)
      end
    end
  end
end
