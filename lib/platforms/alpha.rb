require 'platforms/alpha/syslog_processor'

module Platforms
  module Alpha
    def process_event(event)
      SyslogProcessor.run(self, event)
    end
  end
end
