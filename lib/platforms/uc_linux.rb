require 'platforms/uc_linux/syslog_processor'
require 'platforms/uc_linux/command'

module Platforms
  module UcLinux

    delegate :credentials,             to: :configuration, allow_nil: true
    delegate :remote_control_disabled?, to: :configuration, allow_nil: true

    # TODO: Look at how we can incorporate HTTP logs for UcLinux devices.
    def process_event(event)
      SyslogProcessor.run(self, event)
    end

    def send_command(data)
      if remote_control_disabled? && !sms_command?(data)
        ::Platforms::UcLinux::Command.new(data.merge(device: self)).send!
      else
        ::AclCommand.new(self, data).send!
      end
    end

    def sms_command?(data)
      data[:command].present? && data[:command].match(/^sms_/ix) != nil
    end

  end
end
