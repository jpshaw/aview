require 'platforms/buildroot/syslog_processor'
require 'acl_command'

module Platforms
  module Buildroot

    def process_event(event)
      SyslogProcessor.run(self, event)
    end

    def send_command(data)
      ::AclCommand.new(self, data).send!
    end

  end
end
