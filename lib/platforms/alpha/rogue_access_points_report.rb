module Platforms
  module Alpha
    class RogueAccessPointsReport
      SYSLOG_PREFIX = 'Rogue APs detected on network:'
      SEPARATOR     = '~'
      GROUPED_BY    = ','
      PAIRED_BY     = '='

      attr_reader :old, :access_points, :new, :device
      alias_method :old_access_points, :old
      alias_method :new_access_points, :new

      def initialize(device, message)
        @device            = device
        @old               = device.rogue_access_points.to_a
        @access_points     = parse(message)
        @new               = access_points - old
      end

      def had_access_points?
        old_access_points.any?
      end

      def access_point_ids
        access_points.map(&:id)
      end

      private

      def parse(message)
        if message.include?(SYSLOG_PREFIX)
          message = message.gsub(SYSLOG_PREFIX, '')
        end

        array_of_ap_hashes = message.strip.split(SEPARATOR).map do |group|

          if group.include?(GROUPED_BY)
            groups = group.split(GROUPED_BY)
          else
            # TODO: Remove this once all devices have been upgraded
            groups = group.split(' ')
          end

          groups.inject({}) do |hash, pair|
            key, value = pair.split(PAIRED_BY)
            hash[key.strip.downcase] = value
            hash
          end
        end

        access_points = []

        array_of_ap_hashes.map do |ap_hash|
          access_point = device.rogue_access_points.where(mac: ap_hash['mac']).first_or_create!(ap_hash)
          access_point.update_attributes(ap_hash)
          access_points << access_point
        end

        access_points
      end
    end
  end
end
