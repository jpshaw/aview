require 'platforms/event_processor'
require 'platforms/alpha/syslog_event'
require 'platforms/alpha/events/configuration_events'
require 'platforms/alpha/events/dhcp_events'
require 'platforms/alpha/events/firmware_events'
require 'platforms/alpha/events/network_events'
require 'platforms/alpha/events/reset_events'
require 'platforms/alpha/events/restart_events'
require 'platforms/alpha/events/station_query_events'
require 'platforms/alpha/events/status_events'
require 'platforms/alpha/events/time_events'
require 'platforms/alpha/events/vlan_events'

module Platforms
  module Alpha
    class SyslogProcessor < Platforms::EventProcessor

      include ConfigurationEvents
      include DhcpEvents
      include FirmwareEvents
      include NetworkEvents
      include ResetEvents
      include RestartEvents
      include StationQueryEvents
      include StatusEvents
      include TimeEvents
      include VlanEvents

      def default_event
        case syslog.data
        when Hash
          logger event_type.try(:humanize)
        else
          logger syslog.data
        end
      end

      protected

      def syslog
        @syslog ||= SyslogEvent.new(event)
      end

      def logger(message, opts = {})
        details = { info: message, type: event_type, level: :info }.merge(opts)
        details[:data] ||= { details[:type] => syslog.data }
        device.event(details)
      end
    end
  end
end
