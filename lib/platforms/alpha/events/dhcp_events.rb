module Platforms::Alpha::DhcpEvents
  extend ActiveSupport::Concern

  included do
    event_type :dhcp
  end

  # dhcp: GatewayLanIP=10.201.181.1~GatewayMAC=00:D0:CF:1D:12:BC~GatewayWanIP=99.16.224.185~WebIP=10.201.181.210~DNS=8.8.8.8 4.2.2.4
  def dhcp
    case syslog.data
    when Hash
      create_event_if_host_changed
      create_association_to_gateway
      logger 'Network settings updated', type: :dhcp
    else
      logger syslog.data.to_s
    end
  end

  private

  def create_event_if_host_changed
    return unless syslog.data.has_key?(:WebIP)
    device.host = syslog.data[:WebIP]

    if device.host_changed?
      from = device.host_was || Platforms::NONE
      to   = device.host     || Platforms::NONE

      logger "Device host changed from #{from} to #{to}", type: :dhcp,
        uuid: device.uuid_for(:ip_changed)

      device.save! if device.changed?
    end
  end

  def create_association_to_gateway
    return unless client_mac.present?
    device.associate_with_mac(client_mac, ip_address: syslog.data[:GatewayLanIP],
                                    source_interface: :eth0,
                                         source_type: :wan)
  end

  def client_mac
    @client_mac ||= ::MacAddressCorrelationService.new(syslog.data[:GatewayMAC]).execute
  end

end
