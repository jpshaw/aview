module Platforms::Alpha::ResetEvents
  extend ActiveSupport::Concern

  included do
    alias_event :update_reset, :reset
  end

  # Update-Reset: critical error!
  # Update-Reset: Reset button pressed. Starting reset back to default config!
  # Update-Reset: Rebooting...
  # Update-Reset: Flash update completed successfully. Default config loaded
  def reset
    case syslog.data
    when /reset back to default config/
      logger 'Device restoring system defaults', type: :config, level: :alert

    when /default config loaded/i
      logger 'Default settings restored on unit', type: :config, level: :notice

    when /rebooting/i
      logger 'Device attempting to reboot ...', type: :reboot, level: :notice

    when /critical error/i
      logger 'Unable to restore default config', type: :config, level: :alert

    else
      logger syslog.data.to_s
    end
  end
end
