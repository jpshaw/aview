module Platforms::Alpha::VlanEvents
  extend ActiveSupport::Concern

  included do
    event_type :auto_vlan
  end

  # auto-vlan: Vlan=4~webip=10.23.199.229
  def auto_vlan
    case syslog.data
    when Hash
      if syslog.data.has_key?(:webip)
        syslog.data[:host] = syslog.data.delete(:webip)
        device.host = syslog.data[:host]
      end

      if syslog.data.has_key?(:Vlan)
        syslog.data[:vlan_id] = syslog.data.delete(:Vlan)
      end

      logger 'Vlan auto-discovery executed', type: :dhcp, level: :notice

      device.save! if device.changed?
    else
      logger 'Vlan status'
    end
  end

end
