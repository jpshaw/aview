module Platforms::Alpha::RestartEvents
  extend ActiveSupport::Concern

  included do
    event_type :restart
  end

  # info: Cause of last restart: (Configuration update)
  # info: Cause of last restart: (link failure ping)
  def restart
    message = 'Device rebooted'

    if match = syslog.data.to_s.match(/restart:.*\((?<reason>.*)\)/)
      message += " - #{match[:reason]}"
    end

    logger message, type: :reboot, level: :notice, uuid: device.uuid_for(:rebooted)

    device.update_attribute :last_restarted_at, Time.now.utc
  end
end
