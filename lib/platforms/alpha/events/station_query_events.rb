require 'platforms/alpha/rogue_access_points_report'

module Platforms::Alpha::StationQueryEvents
  extend ActiveSupport::Concern

  included do
    event_type :station_query
    alias_event :staquery, :station_query
  end

  # staquery: RawData(Ch SSID BSSID Security SiganlPct W-Mode)=1 FDS210 b4:c7:99:03:08:82 WPA1PSKWPA2PSK/TKIPAES 24 11b/g/n
  # staquery: Rogue APs detected on network: SSID=tacocat,MAC=00c0ca49d277,IP=10.10.10.11~SSID=clearer,MAC=002704012345,IP=10.10.10.100
  def station_query
    case syslog.raw
    when /RawData/
      logger 'Station Query Details', level: :notice

    when /Rogue APs detected on network/

      report = Platforms::Alpha::RogueAccessPointsReport.new(device, syslog.raw)

      if report.access_points.any?

        report.new_access_points.each do |ap|
          logger "Rogue AP detected on network. " \
                 "SSID: #{ap.ssid} MAC: #{ap.mac}, IP: #{ap.ip}",
            level: :alert, uuid: device.uuid_for(:rogue_access_point)
        end

        device.rogue_access_points
              .where('id not in (?)', report.access_point_ids)
              .destroy_all
      else
        details = { level: :notice }
        details[:uuid] = device.uuid_for(:rogue_access_point) if report.had_access_points?

        device.rogue_access_points.destroy_all

        logger 'No Rogue APs detected on network.', details
      end

    else
      logger syslog.data.to_s
    end
  end

end
