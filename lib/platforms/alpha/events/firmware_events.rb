module Platforms::Alpha::FirmwareEvents
  extend ActiveSupport::Concern

  included do
    event_type :firmware
    alias_event :update_apply, :firmware
    alias_event :update_check, :firmware
  end

  # firmware: img '1.672.91' is current
  # firmware: img unknown!
  # firmware: Firmware update successful.
  # firmware: Getfile error! Config download failed
  # Update-Check: new version is ready to apply
  # Update-Check: wget from (firmware.accns.com) failed!
  # Update-Check: Unable to decrypt (/tmp/update.mac)!
  # Update-Check: wget of (1.481.81.bin) failed!
  # Update-Check: wget of (1.481.81.md5) failed!
  # Update-Check: Downloading firmware configuration
  # Update-Apply: Updating firmware. DO NOT POWER OFF THE UNIT
  #
  # NOTE: Need to find example logs for `/update stopped/` and `/critical error/`
  def firmware
    case syslog.data
    when /img.*is current/

      if match = syslog.data.to_s.match(/img.*\'(?<version>.*)\' is current/i)
        device.firmware = match[:version]

        if device.firmware_changed?
          from = device.firmware_was || Platforms::NONE
          to   = device.firmware     || Platforms::NONE

          logger "Firmware changed from #{from} to #{to}", type: :firmware,
            level: :notice, uuid: device.uuid_for(:fw_changed)

          device.save!
        end
      end

    when /ready to apply/
      logger 'Firmware downloaded', type: :firmware

    when /Updating firmware/
      logger 'Upgrading firmware now. DO NOT POWER OFF THE UNIT',
        type: :firmware, level: :warning

    when /update successful/
      logger 'Firmware upgrade successful', type: :firmware

    when /wget from.*failed/
      logger 'Failed to check for new firmware', type: :firmware, level: :error

    when /Unable.*decrypt/
      logger 'Failed to decrypt firmware', type: :firmware, level: :error

    when /img unknown/
      logger 'Unknown firmware image', type: :firmware, level: :error

    when /.*bin.*failed/, /.*md5.*failed/, /update stopped/, /critical error/
      logger 'Failed to upgrade firmware', type: :firmware, level: :error

    else
      logger syslog.data.to_s
    end
  end
end
