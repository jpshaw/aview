module Platforms::Alpha::ConfigurationEvents
  extend ActiveSupport::Concern

  included do
    alias_event :smxquery,    :configuration
    alias_event :anms_config, :configuration
    alias_event :info,        :configuration
  end

  # ANMS-Config: Downloading configuration
  # ANMS-Config: Error: unable to download configuration
  # info: date=Mon May 11 02:43:24 EDT 2015~firmware=1.621.81~status=ok
  # info: Configuration up to date
  # info: SSIDchannels=9
  # info: Setup complete
  # info: Networking restarted
  # info: Listening for HTTP requests at address 172.19.202.210
  # info: Cause of last restart: (Configuration update)
  # info: rebooting (Configuration update)
  # smxquery: date=Sun Aug 27 02:20:17 EDT 2000~firmware=1.471.91~status=error
  # smxquery: Downloading SMx profile
  # smxquery: SMx config download successful
  # smxquery: error! smxquery (210.88.1.199) failed with error (0)!
  def configuration
    case syslog.data
    when Hash

      if syslog.data.has_key?(:firmware)
        device.firmware = syslog.data[:firmware]

        if device.firmware_changed?
          from = device.firmware_was || Platforms::NONE
          to   = device.firmware     || Platforms::NONE

          logger "Firmware changed from #{from} to #{to}", type: :firmware,
            level: :notice, uuid: device.uuid_for(:fw_changed)

          device.save!
        end
      end

      if syslog.data.has_key?(:status)
        case syslog.data[:status]
        when 'ok'
          logger 'Configuration applied', type: :config
        when 'error'
          logger 'Failed to get configuration', type: :config, level: :error
        end
      end

    when /configuration up to date/i
      logger 'Configuration up to date', type: :config

    when /download successful/i
      logger 'Configuration downloaded', type: :config

    when /unable.*config/i
      logger 'Configuration download failed', type: :config, level: :error

    when /cause of last restart/i
      restart

    when /rebooting/i
      logger 'Device attempting to reboot ...', type: :reboot, level: :notice

    else
      logger syslog.data.to_s
    end
  end

end
