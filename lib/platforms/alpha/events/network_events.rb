module Platforms::Alpha::NetworkEvents
  extend ActiveSupport::Concern

  included do
    event_type :network
  end

  # network: Associated client summary: [28e34728188c#0123059b74e3303]
  # network: Client connected: 8863df784da7
  # network: Client disconnected: 8863df784da7
  def network
    case syslog.data
    when /client summary/i
      logger 'List of client devices updated'
    else
      logger syslog.data.to_s
    end
  end
end
