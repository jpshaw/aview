module Platforms::Alpha::StatusEvents
  extend ActiveSupport::Concern

  included do
    event_type :status
  end

  # status: cnt=1127~rx=1876023~tx=306449~rx10=1363~tx10=258~uptime= 21:10:13 up 7 days  19:41   load average: 0.00  0.00  0.00
  def status
    logger 'Device Status'
  end
end
