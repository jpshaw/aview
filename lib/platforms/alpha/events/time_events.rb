module Platforms::Alpha::TimeEvents
  extend ActiveSupport::Concern

  included do
    event_type :smxtime
  end

  # smxtime: date=Sat Apr 25 22:30:58 EDT 2015~server1=time.accns.com~server2=108.166.125.69
  def smxtime
    case syslog.data
    when /failed/i
      logger 'Device lost date/time', level: :warning
    else
      logger 'Time Server Status'
    end
  end
end
