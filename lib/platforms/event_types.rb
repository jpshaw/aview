module Platforms
  # Platform Event Types
  #
  # Event types are used to map incoming messages to methods that are called
  # to process them.
  #
  # Examples:
  #
  #   class MessageProcessor < Platforms::EventProcessor
  #
  #     # An event with key `status` will trigger the `status` method
  #     event_type :status
  #
  #     # An event with key `modem` will trigger the `modem_status` method
  #     event_type :modem, process_with: :modem_status
  #
  #     # Alternative to using `process_with`
  #     alias_event :modem, :modem_status
  #
  #     def status
  #       # process status
  #     end
  #
  #     def modem_status
  #       # process modem status
  #     end
  #   end
  #
  module EventTypes
    extend ActiveSupport::Concern

    included do
      class_attribute :_event_types, instance_writer: false
    end

    class_methods do
      def event_types
        self._event_types ||= HashWithIndifferentAccess.new
      end

      def event_type(*types)
        opts = types.extract_options!
        opts ||= {}

        types.compact.each do |type|
          event_types[type] = opts[:process_with] || type
        end
      end

      def alias_event(from, to)
        event_types[from] = to
      end
    end
  end
end