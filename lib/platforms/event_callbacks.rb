module Platforms
  # Platform Event Callbacks
  #
  # Callbacks can be defined to trigger logic before or after a specific event
  # method is called.
  #
  # Examples:
  #
  #   # Use callbacks to format a host and send an alert after it is processed.
  #   class MessageProcessor < Platforms::EventProcessor
  #     before_event :host_changed, :format_host
  #     after_event  :host_changed, :trigger_host_alert
  #
  #     def host_changed
  #       # process host
  #     end
  #
  #   private
  #
  #     def format_host
  #       self.host = HostFormatter.parse(event.host)
  #     end
  #
  #     def trigger_host_alert
  #       if self.host_changed?
  #         # Create alert
  #       end
  #     end
  #   end
  #
  module EventCallbacks
    extend ActiveSupport::Concern

    included do
      class_attribute :_event_callbacks, instance_writer: false

      private_class_method :blank_callback_hash, :add_event_callbacks,
        :fetch_event_callbacks
    end

    class_methods do
      def event_callbacks
        self._event_callbacks ||= HashWithIndifferentAccess.new
      end

      def before_event(event, *callbacks)
        add_event_callbacks event, :before, *callbacks
      end

      def after_event(event, *callbacks)
        add_event_callbacks event, :after, *callbacks
      end

      def before_callbacks(event)
        fetch_event_callbacks event, :before
      end

      def after_callbacks(event)
        fetch_event_callbacks event, :after
      end

      def blank_callback_hash
        { before: Set.new, after: Set.new }.with_indifferent_access
      end

      def add_event_callbacks(event, action, *callbacks)
        self._event_callbacks = event_callbacks.dup
        self._event_callbacks[event] ||= blank_callback_hash
        self._event_callbacks[event][action].merge(callbacks.compact)
      end

      def fetch_event_callbacks(event, action)
        (event_callbacks[event] || {}).fetch(action, Set.new).to_a
      end
    end

  private

    def before_callbacks
      self.class.before_callbacks(event_method)
    end

    def after_callbacks
      self.class.after_callbacks(event_method)
    end
  end
end