require 'platforms/uc_linux/commands/client'

module Platforms

  module UcLinux

    class Command

      class MissingCredentialsError < StandardError
        def to_s
          'User and/or Password are missing.'
        end
      end

      DEFAULT_SSL_PORT  = 443.freeze
      OK                = 200.freeze

      def initialize(opts)
        self.device   = opts[:device]
        self.command  = opts[:command]
        self.data     = opts[:data] || {}
        self.user     = opts[:user]
        self.password = opts[:password]

        validate!
      end


      def send!
        self.response = client.get(command, data)
        process_response
      rescue HTTP::TimeoutError
        log_event(
          info:   "The device did not respond to command '#{command}' within " +
                    "#{UcLinux::Commands::Client::TIMEOUT_SECONDS} seconds.",
          level:  :error
        )
      rescue Errno::ECONNREFUSED
        log_event(
          info:   "The device refused the connection on command '#{command}'.",
          level:  :error
        )
      rescue => e
        log_event(
          info:   "Unexpected error occurred while sending command '#{command}'. " +
                    "Error: #{e.message}. Please contact application support.",
          level:  :error
        )
      end


      private

      attr_accessor :device, :command, :data, :user, :password, :response

      def validate!
        has_device? and has_command? and has_credentials?
      end


      def has_device?
        return true if device.is_a? Device
        raise ArgumentError, 'A Device object is necessary to generate the appropriate command.'
      end


      def has_command?
        return true if [String, Symbol].any? {|klass| klass == command.class}
        raise ArgumentError, 'The command must be a string or a symbol.'
      end


      def has_credentials?
        return true if [user, password].all?(&:present?)
        raise MissingCredentialsError
      end


      def client
        UcLinux::Commands::Client.new(
          host: device.command_ip_addr,
          port: DEFAULT_SSL_PORT,
          user: user,
          pass: password
        )
      end


      def process_response
        if ok?
          log_event(info: "Successfully sent command '#{command}' to device")
          cache_credentials
        else
          log_event(
            info:   "Failed to send command '#{command}' to device",
            level:  :error
          )
        end
      end


      def ok?
        response.code == OK
      end


      def log_event(opts)
        device.event(opts.merge(type: :remote))
      end


      def cache_credentials
        Rails.cache.write(
          "#{Settings.device_configurations.credentials.cache_key_prefix}#{device.configuration_id}",
          {user: user, password: password},
          expires_in: Settings.device_configurations.credentials.expiration_minutes.minutes
        )
      end

    end

  end

end
