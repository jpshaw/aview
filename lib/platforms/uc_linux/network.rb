module Platforms
  module UcLinux

    # network: type=modem~idx=0~intf=eth1~mtu=1500~rx=4969635~tx=7186908~default=pri~ip0=10.61.230.228/29~gw0=10.61.230.225~metric0=3
    # network: type=dns~dns0=8.8.8.8~dns1=8.8.4.4~dns2=172.16.0.4
    # network: type=ethernet~intf=eth0~mtu=1500~link=ok~speed=100baseTx-FD,~rx=48899004~tx=603808~default=alt~ip0=172.16.0.183/24~gw0=172.16.0.4~metric0=5~ip1=192.168.210.1/24
    # network: type=modem~idx=1~intf=wwan0~mtu=1430~default=pri~ip0=~gw0=10.61.222.82~metric0=3~passthrough=10.61.222.81
    # network: type=modem~idx=0~intf=wwan0~mtu=1430~mtu_ipv6=1430~default=pri~ip0=100.72.211.90/30~gw0=100.72.211.89~metric0=3~default_ipv6=pri~ip1=~gw1=fe80::b857:cdc:f063:9d69~metric1=512~ip2=2600:1006:b01b:2183:b89b:33ff:fed0:a0b6/64
    # network: idx=0~type=modem~=~gw0=10.79.216.58~intf=wwan0~ip0=10.79.216.57/30~link=~metric0=5~mtu=1430~mtu_ipv6=1430~priority=pri~priority_ipv6=~speed=
    # network: aview=purge~cnt=1~type=ipsec~tunnel=aview~ip_local=172.29.93.231~ip_remote=128.136.167.120~ipsec=up~ipsec0=up~net_local0=172.29.93.231/32~net_remote0=192.168.211.50/32
    # network: type=ipsec~tunnel=aview~ip_local=172.27.183.58~ip_remote=184.106.213.137~net_local=172.27.183.58/32~net_remote=192.168.211.50/32~if_stats=/proc/net/dev/lo~ipsec=up~isakmp-sa=up~ipsec-sa=up~nfmark-me/him=0x0/0xffff0001
    # network: cnt=0~aview=purge~type=ipsec~tunnel=aview~ip_local=10.251.3.3~ip_remote=128.136.167.111~ipsec=up~ipsec0=up~net_local0=192.168.64.99/32~net_remote0=192.168.211.50/32
    # network: cnt=3~type=mgmt~default=pri~ip0=192.168.64.99~name0=ipsec-tunnel-aview

    class Network

      PRIMARY       = 'Primary'
      ALTERNATE     = 'Alternate'
      CONNECTED     = 'Connected'
      NOT_CONNECTED = 'Not Connected'
      ETHERNET      = 'ethernet'
      IPSEC         = 'ipsec'
      MGMT          = 'mgmt'

      NETWORK_KEY_MAP = {
        idx:         :index,
        intf:        :network_iface,
        tunnel:      :network_iface,
        type:        :net_type,
        net_local0:  :net_local,
        net_remote0: :net_remote
      }.freeze

      IPSEC_NETWORK_KEYS = [:ip_local, :ip_remote, :net_local, :net_remote].freeze

      attr_reader :device, :event
      delegate :data, to: :event, allow_nil: true

      def initialize(device, event)
        @device = device
        @event  = event

        assign_data if data.is_a?(Hash)
      end

      def record
        @record ||= DeviceNetwork.where(device_id: device.id, network_iface: interface).first_or_initialize
      end

      def interface
        network_data[:network_iface]
      end

      def host
        host = network_data[:ip0] || network_data[:passthrough] || network_data[:passthrough_ipv6]
        ::FormatIpAddressService.new(host).execute
      rescue ArgumentError # IP error
        nil
      end

      def tunnel_ip
        host = network_data[:net_local]
        ::FormatIpAddressService.new(host).execute
      rescue ArgumentError # IP error
        nil
      end

      def primary?
        network_data[:priority] == PRIMARY
      end

      def mgmt?
        type == MGMT
      end

      def tunnel?
        type == IPSEC
      end

      def ipsec_status
        network_data[:ipsec]
      end

      def type
        network_data[:net_type]
      end

      def aview_action
        network_data[:aview]
      end

      def dns_present?
        network_data[:dns0].present? || network_data[:dns1].present?
      end

      def ethernet?
        type == ETHERNET
      end

      private

      def assign_data
        return unless record.present?
        set_network_attributes
        set_network_ip_addrs
        set_primary_iface_details if primary?
        set_dns_details if dns_present?
        set_mgmt_host if tunnel?
      end

      def network_data
        @network_data ||= rename_hash_keys(data, NETWORK_KEY_MAP)
      end

      def set_network_attributes
        network_data.each do |key, value|
          next unless value.present? && record.respond_to?("#{key}=")
          record.send("#{key}=", value)
        end
      end

      def set_network_ip_addrs
        record.ip_addrs ||= {}
        ip_keys = network_data.keys.map(&:to_s).select { |k| k =~ /ip[0-9]/ }
        keys = (ip_keys + IPSEC_NETWORK_KEYS).map(&:to_sym)
        keys.each do |key|
          record.ip_addrs[key] = network_data[key] if network_data.has_key?(key)
        end
      end

      def set_primary_iface_details
        if host.present?
          mgmt? ? device.mgmt_host = host : device.host = host 
        end
        device.primary_wan_iface = interface if interface.present?
      end

      def set_dns_details
        device.dns1 = network_data[:dns0]
        device.dns2 = network_data[:dns1]
      end

      def set_mgmt_host
        device.mgmt_host = tunnel_ip if tunnel_ip.present?
      end

      def rename_hash_keys(hash, keys_pairs)
        renamed_hash = hash.dup.with_indifferent_access

        # Translate
        keys_pairs.each do |old_key, new_key|
          renamed_hash[new_key] = renamed_hash.delete(old_key) if renamed_hash.has_key?(old_key)
        end

        set_network_priority(renamed_hash, :default)
        set_network_priority(renamed_hash, :default_ipv6)
        set_network_priority(renamed_hash, :priority)
        set_network_priority(renamed_hash, :priority_ipv6)

        set_link_status(renamed_hash, :link)
        set_link_status(renamed_hash, :ipsec)

        renamed_hash
      end

      def set_network_priority(renamed_hash, key)
        if renamed_hash[key].present?
          renamed_hash[:priority] = \
            case renamed_hash[key]
            when 'pri'
              PRIMARY
            when 'alt'
              ALTERNATE
            else
              renamed_hash[key]
            end
        end
      end

      def set_link_status(renamed_hash, key)
        if renamed_hash[key].present?
          renamed_hash[:status] = \
            case renamed_hash[key]
            when 'ok', 'up'
              CONNECTED
            when 'no', 'down'
              NOT_CONNECTED
            else
              renamed_hash[key]
            end
        end
      end
    end
  end
end
