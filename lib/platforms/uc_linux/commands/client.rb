module Platforms

  module UcLinux

    module Commands

      class Client

        URI_PATH        = '/cgi-bin/remote_control.cgi'.freeze
        TIMEOUT_SECONDS = 5.freeze

        def initialize(opts = {})
          self.host     = opts[:host]
          self.port     = opts[:port]
          self.user     = opts[:user]
          self.pass     = opts[:pass]
          self.use_ssl  = opts[:use_ssl] == false ? false : true
        end


        def get(command, data = {})
          HTTP.headers(headers)
              .timeout(
                read:     TIMEOUT_SECONDS,
                connect:  TIMEOUT_SECONDS,
                write:    TIMEOUT_SECONDS
              )
              .basic_auth(
                user: user,
                pass: pass
              )
              .post(
                path,
                params: {cmd: command}.merge(data),
                ssl:    {verify_mode: OpenSSL::SSL::VERIFY_NONE}
              )
        end


        private

        attr_accessor :host, :port, :user, :pass, :use_ssl

        def headers
          { content_type: 'application/json; charset=utf-8' }
        end


        def path
          @path ||= "#{scheme}://#{domain}#{URI_PATH}"
        end


        def scheme
          use_ssl == true ? 'https' : 'http'
        end


        def domain
          domain = host.to_s.include?(':') ? "[#{host}]" : host
          port.present? ? "#{domain}:#{port}" : domain
        end

      end

    end

  end

end
