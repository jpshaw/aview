module Platforms
  module UcLinux

    # location: type=modem~idx=0~lat=28.337973~lon=-81.555043~alt=39.000000
    # location: type=modem~idx=0~mcc=311~mnc=480~lac=65534~cid=52025601
    class Location

      MODEM_DATA       = [:cid, :lac, :mcc, :mnc].freeze
      GEOLOCATION_DATA = [:lat, :lon, :alt].freeze
      DATA = (MODEM_DATA + GEOLOCATION_DATA).freeze

      attr_reader *DATA

      alias_method :latitude, :lat
      alias_method :longitude, :lon
      alias_method :altitude, :alt

      def initialize(attrs = {})
        attrs.each do |key, value|
          instance_variable_set("@#{key}", value)
        end
      end

      def to_h
        @hash ||= instance_values.with_indifferent_access
      end
      alias_method :to_hash, :to_h

      def modem_data
        @modem_data ||= to_h.select { |k,v| MODEM_DATA.include?(k.to_sym) }
      end

      def geolocation_data
        @geolocation_data ||= to_h.select { |k,v| GEOLOCATION_DATA.include?(k.to_sym) }
      end
    end
  end
end
