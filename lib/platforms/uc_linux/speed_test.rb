# speed: tx_avg=0.9590Mbps~tx_latency=543.46ms~rx_avg=1.4607Mbps~rx_latency=542.59ms
module Platforms
  module UcLinux
    class SpeedTest

      attr_reader :event
      delegate :data, to: :event, allow_nil: true

      def initialize(event)
        @event = event
      end

      def formatted_results
        return unless data.is_a?(Hash)
        {
          rx_avg: numbers_from(data[:rx_avg]),
          tx_avg: numbers_from(data[:tx_avg]),
          rx_latency: numbers_from(data[:rx_latency]),
          tx_latency: numbers_from(data[:tx_latency]),
          throughput_units: text_from(data[:rx_avg]),
          latency_units: text_from(data[:rx_latency])
        }
      end

      def numbers_from(string)
        string.tr('A-Za-z', '')
      end

      def text_from(string)
        string.tr('0-9.', '')
      end
    end
  end
end
