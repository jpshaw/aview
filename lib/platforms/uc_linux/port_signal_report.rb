module Platforms

  module UcLinux

    # serial: type=signal~device=port1~cts=1~dcd=1
    class PortSignalReport
      PORTS           = [:cts, :dcd].freeze
      PORT_REGEX      = /port(?<port>\d{1,5})/i
      DEFAULT_PORT_ID = 0
      PORT_ACTIVE     = 1
      PORT_UNPLUGGED  = 0
      ACTIVE          = 'active'
      UNPLUGGED       = 'unplugged'

      attr_reader :type, :device, :configuration, *PORTS

      def initialize(attrs = {})
        attrs.each do |key, value|
          instance_variable_set("@#{key}", value)
        end
      end

      def port_id
        return DEFAULT_PORT_ID if device.blank?
        if match = device.match(PORT_REGEX)
          return match[:port].to_i
        else
          return DEFAULT_PORT_ID
        end
      end

      def any_ports_down?
        given_ports.each do |port_name|
          return true if port_down?(port_name)
        end

        false
      end

      def message
        statuses = []

        given_ports.each do |port|
          statuses << "#{port.to_s.upcase} PIN: #{port_status(port)}"
        end

        message = statuses.join(', ')
        message += '.' unless message.blank?
        message
      end

      private

      def port(name)
        self.__send__(name) rescue nil
      end

      def port_given?(name)
        port(name).present?
      end

      def given_ports
        PORTS.select { |port| port_given?(port) }
      end

      def port_status(name)
        port_active?(name) ? ACTIVE : UNPLUGGED
      end

      def port_active?(name)
        port(name).to_i == PORT_ACTIVE
      end

      def port_unplugged?(name)
        port(name).to_i == PORT_UNPLUGGED
      end

      def port_down?(name)
        port_unplugged?(name) && port_monitored?(name)
      end

      def port_monitored?(name)
        configuration.try(:fetch_value, :serial, device, :monitor, name) == true
      end

    end

  end

end
