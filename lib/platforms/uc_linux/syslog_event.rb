module Platforms
  module UcLinux
    class SyslogEvent
      HASH_SEPARATOR = '~'
      PAIR_SEPARATOR = '='
      IP_PATTERN     = /\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b/

      attr_reader :event

      delegate :severity, :raw, to: :event

      def initialize(event)
        @event = event
      end

      def data
        @data ||= raw.include?(HASH_SEPARATOR) ? to_h : raw
      end

      def to_h(opts = {})
        opts[:pair_separator] ||= PAIR_SEPARATOR

        hash = raw.split(HASH_SEPARATOR).inject({}) do |hash, pair|
          key, value = pair.split(opts[:pair_separator])
          hash[key] = value
          hash
        end

        hash.with_indifferent_access
      end

      # TODO: Add support for parsing IPv6 addresses out
      def ip_address
        data.to_s[IP_PATTERN] if has_ip_address?
      end

      def has_ip_address?
        (data.to_s =~ IP_PATTERN) != nil
      end
    end
  end
end
