require 'platforms/uc_linux/syslog_event'
require 'platforms/uc_linux/events/configuration_events'
require 'platforms/uc_linux/events/dhcpserver_events'
require 'platforms/uc_linux/events/firmware_events'
require 'platforms/uc_linux/events/location_events'
require 'platforms/uc_linux/events/modem_events'
require 'platforms/uc_linux/events/network_events'
require 'platforms/uc_linux/events/port_signal_events'
require 'platforms/uc_linux/events/reboot_events'
require 'platforms/uc_linux/events/status_events'
require 'platforms/uc_linux/events/test_events'
require 'platforms/uc_linux/events/user_events'

module Platforms
  module UcLinux
    class SyslogProcessor < Platforms::EventProcessor

      include ConfigurationEvents
      include DhcpserverEvents
      include FirmwareEvents
      include LocationEvents
      include ModemEvents
      include NetworkEvents
      include PortSignalEvents
      include RebootEvents
      include StatusEvents
      include TestEvents
      include UserEvents

      def default_event
        case syslog.data
        when Hash
          logger event_type.try(:humanize)
        else
          logger syslog.data
        end
      end

      protected

      def syslog
        @syslog ||= SyslogEvent.new(event)
      end

      def logger(message, opts = {})
        details = { info: message, type: event_type, level: :info }.merge(opts)
        details[:data] ||= { details[:type] => syslog.data }
        device.event(details)
      end
    end
  end
end
