# modem: idx=0~apn=m2m.com.attz~imei=359225050282888~imsi=310170801878375~phone=unknown~iccid=89011704258018783757~provider=AT&T~carrier=unknown~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.02 r21040 carmd-fwbuild1 2014/03/17 23:49:48~usb=480~sid=unknown~nid=unknown
module Platforms
  module UcLinux
    class ModemStatus

      MODEM_KEY_MAP = {
                 dbm: :signal,
                rx30: :rx_30,
                tx30: :tx_30,
                desc: :name,
        manufacturer: :maker,
               phone: :number,
            provider: :carrier,
                 usb: :usb_speed
      }.freeze

      attr_reader :device, :event
      delegate :data, to: :event, allow_nil: true

      def initialize(device, event)
        @device = device
        @event = event

        assign_modem_data if data.is_a?(Hash)
      end

      def modem
        return @modem unless @modem.blank?
        
        where_clause = {}

        if modem_data[:iccid].present? 
          where_clause[:iccid] = modem_data[:iccid]
        elsif modem_data[:sim_slot].present? 
          where_clause[:sim_slot] = modem_data[:sim_slot]
        elsif modem_data[:maker] && modem_data[:model]
          where_clause[:maker] = modem_data[:maker]
          where_clause[:model] = modem_data[:model]
        else
          where_clause[:active] = true
        end

        @modem = device.modems.where(where_clause).first_or_create
      end

      # Example: Modem status. Signal: 45%, CNTI: LTE
      def message
        message = 'Modem status.'

        parts = []
        parts << "Signal: #{modem.signal_pct}" if modem.signal.present?
        parts << "Network: #{modem.cnti}" if modem.cnti.present?

        if parts.any?
          message += ' '
          message += parts.join(', ')
        end

        message
      end

      def assign_modem_data
        modem_data.each do |key, value|
          next unless value.present? && modem.respond_to?("#{key}=")
          modem.__send__("#{key}=", value)
        end

        if modem_data[:maker] && modem_data[:model]
          modem.name = "#{modem_data[:maker]} #{modem_data[:model]}"
        end

        modem.active = true
      end

      def modem_data
        @modem_data ||= rename_hash_keys(data, MODEM_KEY_MAP)
      end

      def rename_hash_keys(hash, keys_pairs)
        hash = hash.dup.with_indifferent_access

        # Translate
        keys_pairs.each do |old_key, new_key|
          hash[new_key] = hash.delete(old_key) if hash.has_key?(old_key)
        end

        # Check for cnti-prefixed data
        if cnti = hash[:cnti]
          prefixed_keys = hash.keys.map(&:to_s).select { |key| key.starts_with?(cnti) }

          prefixed_keys.each do |prefixed_key|
            unprefixed_key = prefixed_key.gsub("#{cnti}_", '')
            hash[unprefixed_key] = hash.delete(prefixed_key)
          end
        end

        hash[:cnti].upcase! if hash[:cnti].present?
        hash
      end
    end
  end
end
