require 'platforms/uc_linux/port_signal_report'

module Platforms::UcLinux::PortSignalEvents
  extend ActiveSupport::Concern

  included do
    event_type :port_signal
    alias_event :serial, :port_signal
  end

  # serial: type=signal~device=port1~cts=1~dcd=1
  def port_signal
    report_data = syslog.data.merge(configuration: device.configuration)
    report = Platforms::UcLinux::PortSignalReport.new(report_data)

    if report.any_ports_down?
      uuid = device.uuid_for(:port_down)
    end

    logger "Serial Port #{report.port_id}: #{report.message}", uuid: uuid
  end

end
