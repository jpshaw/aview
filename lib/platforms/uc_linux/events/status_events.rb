require 'platforms/uc_linux/modem_status'
require 'platforms/uc_linux/network'

module Platforms::UcLinux::StatusEvents
  extend ActiveSupport::Concern

  MODEM    = 'modem'
  ETHERNET = 'ethernet'

  included do
    event_type :heartbeat, :status
  end

  # uClinux devices send a "heartbeat" syslog as part of their periodic
  # check-in. If no datapoints change between heartbeats, it will be the only
  # syslog sent when checking-in. If datapoints do change between heartbeats,
  # they will be sent in separate syslogs, and the heartbeat syslog *will not*
  # be sent.
  #
  # heartbeat:
  def heartbeat
    logger 'Heartbeat'
  end

  # status: type=modem~idx=1~intf=wwan0~dbm=-88~per=41~cnti=umts~rx=2825~tx=6721~evdo_rssi=-125.00~evdo_ecio=-2.50~evdo_sinr=9.00~evdo_io=-106.00~gsm_rssi=-77.00
  # status: type=ethernet~intf=eth0~rx=1046~tx=4298
  def status
    case syslog.data
    when Hash
      case syslog.data[:type]
      when MODEM
        status = Platforms::UcLinux::ModemStatus.new(device, syslog)

        if status.modem.cnti_changed?
          from = status.modem.cnti_was || Platforms::NONE
          to   = status.modem.cnti     || Platforms::NONE
          logger "Cellular network changed from #{from} to #{to}",
            type: :modem, level: :notice, uuid: device.uuid_for(:cnti_changed)
        else
          logger status.message, type: :modem
        end

        if status.modem.signal_changed? && status.modem.signal_below_threshold?
          percentage = AdminSettings.threshold_percentage
          logger "Signal below threshold of '#{percentage}%'",
            type: :modem, uuid: device.uuid_for(:signal_changed)
        end

        if status.modem.changed?
          status.modem.save! 
        end

      when ETHERNET
        logger 'Ethernet status'
      end

      network = Platforms::UcLinux::Network.new(device, syslog)
      network.record.save! if network.record && network.record.changed?
      device.save! if device.changed?

    else
      logger 'Device status', type: :modem
    end
  end

end
