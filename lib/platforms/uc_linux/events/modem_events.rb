require 'platforms/uc_linux/modem_status'

module Platforms::UcLinux::ModemEvents
  extend ActiveSupport::Concern

  included do
    event_type :modem
  end

  # TODO: Add support for multiple modems
  # modem: idx=0~apn=m2m.com.attz~imei=359225050282888~imsi=310170801878375~phone=unknown~iccid=89011704258018783757~provider=AT&T~carrier=unknown~manufacturer=SierraWirelessIncorporated~model=MC7354~revision=SWI9X15C_05.05.16.02 r21040 carmd-fwbuild1 2014/03/17 23:49:48~usb=480~sid=unknown~nid=unknown
  def modem
    case syslog.data
    when Hash
      event = Platforms::UcLinux::ModemStatus.new(device, syslog)
      event.modem.save!
      device.deactivate_old_modems(event.modem)
    end
    logger 'Modem status', type: :modem
  end

end
