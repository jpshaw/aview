module Platforms::UcLinux::RebootEvents
  extend ActiveSupport::Concern

  included do
    event_type :reboot, :restart
  end

  # reboot: msg=Remote restart~cmd=reboot
  def reboot
    logger 'Device attempting to reboot ...', type: :reboot
  end

  # restart: msg=Remote restart
  # restart: msg=Remote restart~cmd=reboot
  # restart: msg=Modem firmware update~carrier=ATT~cmd=hardreboot
  def restart
    device.update_attribute(:last_restarted_at, Time.now)

    reason = \
      case syslog.data
      when /msg.*\=/
        syslog.data.gsub(/msg.*\=/, '')
      when /restart\s*\w.*/
        syslog.data.gsub(/restart\s*/, '')
      when Hash
        syslog.data[:msg]
      else
        nil
      end

    message = 'Device rebooted'
    message += ": #{reason}" if reason

    logger message, type: :reboot, level: :notice, uuid: device.uuid_for(:rebooted)
  end

end
