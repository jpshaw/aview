module Platforms::UcLinux::FirmwareEvents
  extend ActiveSupport::Concern

  included do
    event_type :firmware
  end

  # firmware: version=15.5.1~serial=6300010030051578
  # firmware: Failed to download firmware configuration.accns.com/device_firmware/6300-CX/15.5.1: 404
  # firmware: Firmare is up to date (15.3.55).
  # firmware: Flash update fail: 1: Insufficient memory for image!
  def firmware
    case syslog.data
    when Hash
      data            = syslog.data
      device.firmware = data[:version].strip.upcase if data[:version].present?
      device.serial   = data[:serial].strip         if data[:serial].present?

      if device.firmware_changed?
        from = device.firmware_was || Platforms::NONE
        to   = device.firmware     || Platforms::NONE

        logger "Firmware changed from #{from} to #{to}", type: :firmware,
          uuid: device.uuid_for(:fw_changed)
      else
        logger "Device using firmware version #{device.firmware}", type: :firmware
      end

      device.save! if device.changed?

    when /failed to download firmware/i
      logger 'Firmware download failed', level: :error, type: :firmware

    when /firmare is up to date/i
      logger 'Firmare is up to date', type: :firmware

    when /flash update fail/i
      logger syslog.data.to_s, level: :error, type: :firmware

    else
      logger 'Firmware status', type: :firmware
    end
  end

end
