module Platforms::UcLinux::DhcpserverEvents
  extend ActiveSupport::Concern

  included do
    event_type :dhcpserver
  end

  # dhcpserver: intf=eth0~passthrough=wwan0~mac=f8:b1:56:9a:78:9e~ip=10.63.218.160~action=add
  # dhcpserver: intf=eth0~passthrough=wwan0~mac=f8:b1:56:9a:78:9e~ip=10.63.218.160~action=del
  # dhcpserver: intf=eth0~mac=f8:b1:56:9a:78:9e~ip=192.168.210.123~action=add
  # dhcpserver: action=list
  # dhcpserver: mac=f8:b1:56:9a:78:9e~ip=192.168.210.123~action=list
  # dhcpserver: intf=eth0~mac=f8:b1:56:9a:78:9e~ip=192.168.210.123~action=del
  def dhcpserver
    if data.is_a?(Hash)
      create_event_from_action_type
    else
      create_default_dhcpserver_event
    end
  end

  private

  def data
    @data ||= syslog.data
  end

  def client_mac
    @client_mac ||= ::MacAddressCorrelationService.new(data[:mac]).execute
  end

  def create_default_dhcpserver_event
    logger 'DHCP Server status', type: :dhcp
  end

  def setup_association_with_mac
    return false unless client_mac.present?
    device.associate_with_mac(client_mac, ip_address: data[:ip],
                                    source_interface: data[:intf],
                                          lease_type: data[:type],
                                         source_type: :lan)
  end

  def remove_associated_devices_with_mac
    return false unless client_mac.present?
    device.dissociate_from_mac(client_mac)
  end

  def create_event_from_action_type
    case data[:action]
    when 'add', 'list'
      if setup_association_with_mac
        logger "Device handed IP #{data[:ip]} to MAC #{client_mac}", type: :dhcp
      else
        create_default_dhcpserver_event
      end
    when 'del'
      if remove_associated_devices_with_mac
        logger "DHCP lease expired for MAC #{client_mac}", type: :dhcp
      else
        create_default_dhcpserver_event
      end
    else
      create_default_dhcpserver_event
    end
  end

end
