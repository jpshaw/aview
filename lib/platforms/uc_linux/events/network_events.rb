require 'platforms/uc_linux/network'
require 'update_device_dns'

module Platforms::UcLinux::NetworkEvents
  extend ActiveSupport::Concern

  included do
    event_type :dhcp, :network
  end

  # NOTE: This method will be deprecated once all uClinux devices stop using
  # the `dhcp` message.
  #
  # dhcp: waiting for dhcp handshake to begin
  # dhcp: pppl=100.64.161.76~dns1=198.224.181.135~dns2=198.224.178.135
  def dhcp
    logger 'Network status', type: :dhcp
  end

  # network: type=ipsec~tunnel=aview~if_stats=/proc/net/dev/wwan0~ipsec=up~isakmp-sa=down~ipsec-sa=up
  # network: cnt=1~aview=purge~type=ipsec
  # network: type=ipsec~tunnel=aview~if_stats=/proc/net/dev/wwan0~ipsec=up~isakmp-sa=up~ipsec-sa=up
  # network: aview=purge~cnt=1~type=ipsec~tunnel=aview~ip_local=172.29.93.231~ip_remote=128.136.167.120~ipsec=up~ipsec0=up~net_local0=172.29.93.231/32~net_remote0=192.168.211.50/32
  # network: type=ipsec~tunnel=aview~ip_local=172.27.183.58~ip_remote=184.106.213.137~net_local=172.27.183.58/32~net_remote=192.168.211.50/32~if_stats=/proc/net/dev/lo~ipsec=up~isakmp-sa=up~ipsec-sa=up~nfmark-me/him=0x0/0xffff0001
  # network: cnt=0~aview=purge~type=ipsec~tunnel=aview~ip_local=10.251.3.3~ip_remote=128.136.167.111~ipsec=up~ipsec0=up~net_local0=192.168.64.99/32~net_remote0=192.168.211.50/32
  # network: type=dns~dns0=198.224.154.135~dns1=198.224.152.119
  # network: type=ethernet~intf=eth0~mtu=1500~link=ok~speed=100baseTx-FD~rx=436916404~tx=5375327663~default=~ip0=192.168.210.1/24~ip1=192.168.1.1/24
  # network: type=modem~idx=0~intf=wwan0~mtu=1428~rx=1010329980~tx=437169539~default=pri~ip0=~gw0=100.70.247.37~metric0=3
  # network: type=modem~idx=0~intf=wwan0~mtu=1430~mtu_ipv6=1430~default=pri~ip0=100.72.211.90/30~gw0=100.72.211.89~metric0=3~default_ipv6=pri~ip1=~gw1=fe80::b857:cdc:f063:9d69~metric1=512~ip2=2600:1006:b01b:2183:b89b:33ff:fed0:a0b6/64
  NET_ACTION_PURGE = 'purge'.freeze
  def network
    case syslog.data
    when Hash
      case network_data.type
      when /dns/
        create_dns_logs
      when /ipsec/
        device.ipsec_networks.where.not(id: network_data.record.id).destroy_all if network_data.aview_action == NET_ACTION_PURGE
        create_ipsec_logs
        save_network
      else
        update_host
        create_network_logs
        update_ethernet_status if network_data.ethernet?
        save_network
      end

      device.save! if device.changed?
    else
      logger 'Network status'
    end
  end

  private

  def network_data
    @network_data ||= Platforms::UcLinux::Network.new(device, syslog)
  end

  def update_host
    if device.host_changed?
      from = device.host_was || Platforms::NONE
      to   = device.host     || Platforms::NONE

      # Ensure we set the latest dns host
      UpdateDeviceDns.call(host: device.host, mac: device.mac)

      logger "Cellular WAN IP changed from #{from} to #{to}", type: :dhcp,
        uuid: device.uuid_for(:ip_changed)
    end
  end

  def create_dns_logs
    if device.dns1_changed? or device.dns2_changed?
      logger "DNS updated to #{device.dns1}, #{device.dns2}", level: :notice
    else
      logger 'DNS status'
    end

  end

  def create_ipsec_logs
    case network_data.ipsec_status
    when /up/
      logger "#{network_data.interface} tunnel established", type: :tunnel,
        level: :notice
    when /down/
      logger "#{network_data.interface} tunnel disconnected", type: :tunnel,
        level: :notice
    when /connecting/
      logger "#{network_data.interface} tunnel connecting", type: :tunnel,
        level: :notice
    else
      logger "Unknown status for #{network_data.interface} tunnel", type: :tunnel,
        level: :warning
    end
  end

  def create_network_logs
    if network_data.interface.present?
      if device.primary_wan_iface_changed?
        logger "Primary WAN interface changed to #{network_data.interface}",
          uuid: device.uuid_for(:wan_changed)
      end
      logger "Network status for #{network_data.interface} interface", type: :dhcp
    else
      logger 'Network status'
    end
  end

  # If the ethernet status changes and wasn't previously nil, generate an event.
  def update_ethernet_status
    if network_data.record.status_changed? && network_data.record.status_was != nil
      case network_data.record.status
      when Platforms::UcLinux::Network::CONNECTED
        logger "Ethernet port '#{network_data.interface}' connected", type: :dhcp,
          level: :notice, uuid: device.uuid_for(:ethernet_connected)

      when Platforms::UcLinux::Network::NOT_CONNECTED
        logger "Ethernet port '#{network_data.interface}' disconnected", type: :dhcp,
          level: :notice, uuid: device.uuid_for(:ethernet_disconnected)
      end
    end
  end

  def save_network
    network_data.record.save! if network_data.record and network_data.record.changed?
  end
end
