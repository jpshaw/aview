module Platforms::UcLinux::ConfigurationEvents
  extend ActiveSupport::Concern

  included do
    event_type :config
  end

  CONFIG_CHANGE       = /\((?<key>.*?)\) changing from \((?<from>.*?)\) to \((?<to>.*?)\)/i
  CONFIG_COUNT_CHANGE = /\((?<key>.*?)\) count changing from \((?<from>.*?)\) to \((?<to>.*?)\)/i

  # ### Sample NOTICE Syslogs ###
  # config: (vpn.ipsec.aview.remote.hostname) changing from (remotecontrol.accns.com) to (remote.accns.com)
  # config: (auth.user.test) created
  # config: (auth.user.test) deleted
  # config: (firewall.filter) count changing from (1) to (2)
  # config: Config download/processing complete.
  # config: Config is unmodified

  # ### Sample WARNING Syslogs ###
  # config: Date has yet to be correctly set.
  # config: Cert. url or mac misconfigured
  # config: genkeys not complete, no $key.
  # config: Bad private key.
  # config: Bad modulus, $mod1 != $mod2.
  # config: Allowing MAC in cert, $cmac != $mac.
  # config: Wrong MAC in cert, $cmac != $mac.
  # config: Required security file $i is not present.
  # config: Some or all required security files not present.
  # config: Certificate signing failed.
  # config: Certificate signing request submitted.
  # config: Certificate signing failed: $request_result
  # config: Required security file $i is not ready.
  # config: Some of all Certificates are not ready.
  # config: Failed to load config from all servers
  # config: Config provides an invalid cert (bad RSA): $1
  # config: Config provides an invalid cert (bad X509): $1
  # config: Date has yet to be correctly set.
  # config: Config provides an invalid key: $3
  # config: using firmware settings from new config
  # config: unable to parse schema file
  # config: failed to open %s for writing
  # config: unable to parse new config file
  # config: unable to migrate config
  # config: unable to load config

  def config
    case syslog.severity
    when EventLevels::NOTICE
      if match = syslog.data.to_s.match(CONFIG_CHANGE)
        logger "#{match[:key]} updated to #{match[:to]}", type: :config
      elsif match = syslog.data.to_s.match(CONFIG_COUNT_CHANGE)
        logger "#{match[:key]} count updated to #{match[:to]}", type: :config
      else
        logger syslog.data.to_s, type: :config
      end
    when EventLevels::WARNING
      logger syslog.data.to_s, level: :error, type: :config
    else
      logger syslog.data.to_s, type: :config
    end
  end
end
