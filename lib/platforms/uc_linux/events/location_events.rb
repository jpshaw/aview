require 'platforms/uc_linux/location'

module Platforms::UcLinux::LocationEvents
  extend ActiveSupport::Concern

  included do
    event_type :location
  end

  # location: cnt=396~idx=0~type=modem~lat=37.428333~lon=-93.840833
  def location
    case syslog.data
    when Hash
      location = Platforms::UcLinux::Location.new(syslog.data)
      modem = device.modem || device.modems.first_or_create

      if location.modem_data.any?
        location.modem_data.each do |key, value|
          next unless modem.respond_to?("#{key}=")
          modem.send("#{key}=", value)
        end

        modem.save! if modem.changed?
        logger 'Location status'
      end

      if location.geolocation_data.any?
        # The location information that the device reports should trump the
        # location details. However, since all the device reports is the lat/lon
        # coordinates, we cannot know for certain if the coordinates are for the
        # existing cellular location.  So instead of updating the shared cellular
        # location, create a new cellular location for the device.
        modem.cellular_location = CellularLocation.where(lat: location.latitude,
                                                         lon: location.longitude).first_or_create
        modem.save!
        logger "Created device cellular location with coordinates #{location.latitude}, #{location.longitude}"
      end
    else
      logger 'Location status'
    end
  end

end
