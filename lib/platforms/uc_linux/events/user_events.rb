module Platforms::UcLinux::UserEvents
  extend ActiveSupport::Concern

  included do
    event_type :user
  end

  # user: name=SIT~service=serial~state=closed~remote=127.0.0.1~tty=port2
  # user: name=root~service=sshd~state=opened~remote=172.16.3.149~tty=ssh
  # user: name=root~service=serial~state=opened~remote=172.16.3.28~tty=port1
  def user
    data = syslog.data
    logger "User #{data[:name]} at #{data[:remote]} #{data[:state]} #{data[:service]}"
  end

end
