module Platforms::UcLinux::TestEvents
  extend ActiveSupport::Concern

  included do
    event_type :speed
  end

  # speed: tx_avg=0.9590Mbps~tx_latency=543.46ms~rx_avg=1.4607Mbps~rx_latency=542.59ms
  def speed
    if device.using_cellular_as_primary?
      speed_test = Platforms::UcLinux::SpeedTest.new(syslog)
      device.modem.speed_tests.create!(speed_test.formatted_results)
    end
    logger 'Speed Test Results', type: :remote, data: { speed: syslog.data, disclaimer: I18n.t('devices.speed_test_disclaimer') }
  end

end
