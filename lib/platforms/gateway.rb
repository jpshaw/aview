require 'platforms/gateway/inform_processor'
require 'platforms/gateway/snmp/probeable'
require 'gateway_command'

module Platforms
  module Gateway
    include Snmp::Probeable

    def process_event(event)
      InformProcessor.run(self, event)
    end

    def send_command(data)
      ::GatewayCommand.new(self, data).send!
    end

  end
end
