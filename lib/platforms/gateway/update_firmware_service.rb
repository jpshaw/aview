module Platforms
  module Gateway
    class UpdateFirmwareService
      include ::Kraken::Logger.file

      attr_reader :device, :firmware

      def initialize(device, firmware)
        @device   = device
        @firmware = firmware
      end

      def execute
        return if firmware.blank?

        device.firmware = firmware

        if device.firmware_changed?
          from = device.firmware_was || Platforms::NONE

          if device.save
            message = "Firmware changed from #{from} to #{firmware}"

            device.event info: message, level: :notice, type: :firmware,
              uuid: device.uuid_for(:fw_changed)
          else
            logger.error "[#{device.mac}] Failed to save firmware #{firmware}. " \
                         "Errors: #{device.errors.full_messages.join(', ')}"
          end
        end
      end
    end
  end
end
