require 'platforms/gateway/snmp/vrrp_result'

# Process gateway snmp vrrp data
module Platforms::Gateway::Snmp::VRRPResultsProcessor
  extend self

  # We can reference these constants without the Platforms::Gateway prefix
  # without issue on MRI, but TorqueBox autoloading gets weird and requires
  # the full path
  LIMIT      = Platforms::Gateway::Snmp::Objects::VRRP_LIMIT
  INTERFACE  = Platforms::Gateway::Snmp::Objects::VRRP_INTERFACE
  STATUS     = Platforms::Gateway::Snmp::Objects::VRRP_STATUS
  VRRPResult = Platforms::Gateway::Snmp::VRRPResult

  def run(device, results)
    record  = device.details || device.build_details
    results = parse(results)

    master_result = results.find(&:master?)

    vrrp_state =
      if master_result
        master_result.status_name
      else
        results.first.status_name
      end

    record.update(vrrp_state: vrrp_state)
  end

  private

  def parse(results)
    (1..LIMIT).map do |index|
      VRRPResult.new(
        index: index,
        interface: results["#{INTERFACE}.#{index}"],
        status_code: results["#{STATUS}.#{index}"]
      )
    end
  end
end
