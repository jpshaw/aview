# Parses a timeticks string, given in the format "[days,]hh:mm:ss.hh".
#
# Examples
#
#   Platforms::Gateway::Snmp::TimeTicks.new('22 days, 1:45:00.00').to_i
#   # => 1907100
#
#   Platforms::Gateway::Snmp::TimeTicks.new('4:07:43.00').to_i
#   # => 14863
#
class Platforms::Gateway::Snmp::TimeTicks

  def initialize(string)
    @string  = string
    @days    = 0
    @hours   = 0
    @minutes = 0
    @seconds = 0

    parse
  end

  def to_i
    @to_i ||= [@days, @hours, @minutes, @seconds].inject(:+)
  end

  def to_s
    string
  end

  private

  attr_reader :string

  def parse
    return if string.blank?

    if string.include?(',')
      days, time = string.split(',').map(&:strip)
    else
      days = '0 days'
      time = string
    end

    hh, mm, ss = time.split(':').map(&:to_i)

    @days    = days.to_i.days.to_i
    @hours   = hh.hours.to_i
    @minutes = mm.minutes.to_i
    @seconds = ss.seconds.to_i
  end

end