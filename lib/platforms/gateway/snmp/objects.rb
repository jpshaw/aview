class Platforms::Gateway::Snmp::Objects

  GATEWAY_6_1_0 = Gem::Version.new('6.1')
  GATEWAY_6_2_0 = Gem::Version.new('6.2')
  GATEWAY_6_5_0 = Gem::Version.new('6.5')
  GATEWAY_7_0   = Gem::Version.new('7.0')

  # TODO: Convert oids to string names once support for custom MIB's is added
  SYSTEM_NAME          = '1.3.6.1.2.1.1.5.0'
  HARDWARE_MAC         = '1.3.6.1.4.1.74.1.30.1.20.0'
  HARDWARE_LEVEL       = '1.3.6.1.4.1.74.1.30.1.1.0'
  SOFTWARE_LEVEL       = '1.3.6.1.4.1.74.1.30.1.2.0'
  WAN_PREFIX           = '1.3.6.1.4.1.74.1.30.2'
  WAN_INTERFACE        = '1.3.6.1.4.1.74.1.30.2.1.0'
  WAN_BYTES_IN         = '1.3.6.1.4.1.74.1.30.2.2.0'
  WAN_BYTES_OUT        = '1.3.6.1.4.1.74.1.30.2.3.0'
  WAN_CONNECTION       = '1.3.6.1.4.1.74.1.30.1.7.0'
  SMX_ACCOUNT          = '1.3.6.1.4.1.74.1.30.1.3.0'
  TIME_IN_BACKUP       = '1.3.6.1.4.1.74.1.30.2.5.0'
  FIRMWARE_ROLLBACK    = '1.3.6.1.4.1.74.1.30.1.21.0'
  DUAL_WAN_STATE       = '1.3.6.1.4.1.74.1.30.1.22.0'

  # Interfaces
  WAN_IPV4_STATE       = '1.3.6.1.4.1.74.1.30.2.9.0'
  WAN_IPV6_STATE       = '1.3.6.1.4.1.74.1.30.2.10.0'

  # Connected devices
  WAN1_DEVICE_MAC      = '1.3.6.1.4.1.74.1.30.1.25.0'
  WAN2_DEVICE_MAC      = '1.3.6.1.4.1.74.1.30.1.27.0'

  # Overall device tunnel status
  TUNNELS_TOTAL_COUNT  = '1.3.6.1.4.1.74.1.30.6.2.0'
  TUNNELS_TOTAL_UP     = '1.3.6.1.4.1.74.1.30.6.3.0'
  TUNNELS_STATUS       = '1.3.6.1.4.1.74.1.30.6.4.0'

  # Individual tunnel data
  TUNNEL_IPSEC_IFACE   = '1.3.6.1.4.1.74.1.30.6.1.1.2'
  TUNNEL_DURATION      = '1.3.6.1.4.1.74.1.30.6.1.1.5'
  TUNNEL_ENDPOINT      = '1.3.6.1.4.1.74.1.30.6.1.1.6'
  TUNNEL_ENDPOINT_IPV6 = '1.3.6.1.4.1.74.1.30.6.1.1.19'
  TUNNEL_ENDPOINT_TYPE = '1.3.6.1.4.1.74.1.30.6.1.1.7'
  TUNNEL_AUTH_TYPE     = '1.3.6.1.4.1.74.1.30.6.1.1.13'
  TUNNEL_SERVER_MGMT   = '1.3.6.1.4.1.74.1.30.6.1.1.16'
  TUNNEL_INITIATOR     = '1.3.6.1.4.1.74.1.30.6.1.1.17'
  TUNNEL_MODE          = '1.3.6.1.4.1.74.1.30.6.1.1.18'
  TUNNEL_NAME          = '1.3.6.1.4.1.74.1.30.6.1.1.24'

  # Modems
  MODEM_PREFIX         = '1.3.6.1.4.1.74.1.30.5.1.1'
  MODEM_SLOT           = '1.3.6.1.4.1.74.1.30.5.1.1.2'
  MODEM_TYPE           = '1.3.6.1.4.1.74.1.30.5.1.1.3'
  MODEM_MANUFACTURER   = '1.3.6.1.4.1.74.1.30.5.1.1.4'
  MODEM_MODEL          = '1.3.6.1.4.1.74.1.30.5.1.1.5'
  MODEM_REVISION       = '1.3.6.1.4.1.74.1.30.5.1.1.6'
  MODEM_ID             = '1.3.6.1.4.1.74.1.30.5.1.1.7'
  MODEM_IMSI           = '1.3.6.1.4.1.74.1.30.5.1.1.10'
  MODEM_PHONE_NUMBER   = '1.3.6.1.4.1.74.1.30.5.1.1.12'
  MODEM_APN            = '1.3.6.1.4.1.74.1.30.5.1.1.13'
  MODEM_SUPPORT        = '1.3.6.1.4.1.74.1.30.5.1.1.16'
  MODEM_SIM_NUMBER     = '1.3.6.1.4.1.74.1.30.5.1.1.17'
  MODEM_UPDATED_AT     = '1.3.6.1.4.1.74.1.30.5.1.1.19'
  MODEM_CONN_STATUS    = '1.3.6.1.4.1.74.1.30.5.1.1.20'
  MODEM_NETWORK        = '1.3.6.1.4.1.74.1.30.5.1.1.21'
  MODEM_CARRIER        = '1.3.6.1.4.1.74.1.30.5.1.1.22'
  MODEM_MCC            = '1.3.6.1.4.1.74.1.30.5.1.1.23'
  MODEM_MNC            = '1.3.6.1.4.1.74.1.30.5.1.1.24'
  MODEM_LAC            = '1.3.6.1.4.1.74.1.30.5.1.1.25'
  MODEM_CID            = '1.3.6.1.4.1.74.1.30.5.1.1.26'
  MODEM_SIGNAL         = '1.3.6.1.4.1.74.1.30.5.1.1.27'
  MODEM_RSSI           = '1.3.6.1.4.1.74.1.30.5.1.1.28'
  MODEM_ECIO           = '1.3.6.1.4.1.74.1.30.5.1.1.29'
  MODEM_RSRP           = '1.3.6.1.4.1.74.1.30.5.1.1.30'
  MODEM_RSRQ           = '1.3.6.1.4.1.74.1.30.5.1.1.31'
  MODEM_SINR           = '1.3.6.1.4.1.74.1.30.5.1.1.32'
  MODEM_SNR            = '1.3.6.1.4.1.74.1.30.5.1.1.33'
  MODEM_BAND           = '1.3.6.1.4.1.74.1.30.5.1.1.35'

  # VRRP
  VRRP_INTERFACE       = '1.3.6.1.4.1.74.1.30.4.1.1.2'
  VRRP_STATUS          = '1.3.6.1.4.1.74.1.30.4.1.1.4'

  # TODO: Change this to HARDWARE_MAC when all Gateway devices are upgraded
  # to at least firmware 6.2.
  DEFAULT_MAC_OID      = SYSTEM_NAME

  BASE_OBJECTS = [
    HARDWARE_LEVEL,
    SOFTWARE_LEVEL,
    WAN_INTERFACE,
    WAN_CONNECTION,
    SMX_ACCOUNT,
    WAN_BYTES_IN,
    WAN_BYTES_OUT,
  ].freeze

  CONNECTED_DEVICE_MACS = [
    WAN1_DEVICE_MAC,
    WAN2_DEVICE_MAC
  ].freeze

  TUNNEL_OBJECTS = [
    TUNNEL_IPSEC_IFACE,
    TUNNEL_DURATION,
    TUNNEL_ENDPOINT,
    TUNNEL_ENDPOINT_IPV6,
    TUNNEL_ENDPOINT_TYPE,
    TUNNEL_AUTH_TYPE,
    TUNNEL_SERVER_MGMT,
    TUNNEL_INITIATOR,
    TUNNEL_MODE,
    TUNNEL_NAME,
  ].freeze

  TUNNEL_STATUS_OBJECTS = [
    TUNNELS_TOTAL_COUNT,
    TUNNELS_TOTAL_UP,
    TUNNELS_STATUS
  ].freeze

  CELL_MODEM_OBJECTS = [
    MODEM_SLOT,
    MODEM_TYPE,
    MODEM_MANUFACTURER,
    MODEM_MODEL,
    MODEM_REVISION,
    MODEM_ID,
    MODEM_IMSI,
    MODEM_PHONE_NUMBER,
    MODEM_APN,
    MODEM_SIM_NUMBER,
    MODEM_SUPPORT,
    MODEM_UPDATED_AT,
    MODEM_CONN_STATUS,
    MODEM_NETWORK,
    MODEM_CARRIER,
    MODEM_MCC,
    MODEM_MNC,
    MODEM_LAC,
    MODEM_CID,
    MODEM_SIGNAL,
    MODEM_RSSI,
    MODEM_ECIO,
    MODEM_RSRP,
    MODEM_RSRQ,
    MODEM_SINR,
    MODEM_SNR,
  ].freeze

  VRRP_OBJECTS = [
    VRRP_INTERFACE,
    VRRP_STATUS
  ].freeze

  # How many VRRP rows to request
  #
  # Since we're only interested in the primary interface VRRP at the moment,
  # we can assume its near the top in the VRRP table.
  #
  # TODO: Verify that this assumption is correct.
  VRRP_LIMIT = 2

  attr_reader :device, :options

  def initialize(device, options = {})
    @device  = device
    @options = options
    @objects = build_objects
  end

  def to_a
    @objects
  end

  private

  def build_objects
    objects = BASE_OBJECTS.dup

    # Add firmware-specific mac identifier
    objects.unshift(device_mac_oid)

    # Check for backup mode if using the correct firmware
    objects.push(TIME_IN_BACKUP) if firmware_version >= GATEWAY_6_1_0
    objects.push(FIRMWARE_ROLLBACK) if firmware_version >= GATEWAY_6_2_0

    # Check for connected devices
    objects += CONNECTED_DEVICE_MACS if firmware_version >= GATEWAY_6_2_0

    # VRRP
    (1..VRRP_LIMIT).each do |index|
      objects += vrrp_objects_for_index(index)
    end

    # Tunnels
    (1..device.tunnel_limit).each do |index|
      objects += tunnel_objects_for_index(index)
    end

    objects += TUNNEL_STATUS_OBJECTS if firmware_version >= GATEWAY_7_0

    # Modems
    if device.cell_modem_collection_enabled?
      (1..device.modem_slot_limit).each do |index|
        objects += cell_modem_objects_for_index(index)
        objects.push("#{MODEM_BAND}.#{index}") if firmware_version >= GATEWAY_6_5_0
      end
    end

    # Dual WAN
    if firmware_version >= GATEWAY_6_2_0
      objects.push(DUAL_WAN_STATE)
      objects.push(WAN_IPV4_STATE)
      objects.push(WAN_IPV6_STATE)
    end

    objects
  end

  def vrrp_objects_for_index(i)
    VRRP_OBJECTS.map { |oid| "#{oid}.#{i}" }
  end

  def tunnel_objects_for_index(i)
    TUNNEL_OBJECTS.map { |oid| "#{oid}.#{i}" }
  end

  def cell_modem_objects_for_index(i)
    CELL_MODEM_OBJECTS.map { |oid| "#{oid}.#{i}" }
  end

  def device_mac_oid
    self.class.mac_oid_for_firmware_version(firmware_version)
  end

  def firmware_version
    @firmware_version ||= Gem::Version.new(device.firmware.to_s)
  end

  # Starting with VPN Gateway firmware 6.2 the MAC address is stored at the
  # hwMac oid. For firmwares below 6.2 we use the sysName oid.
  #
  # Accepts a Gem::Version object or String with the firmware version.
  def self.mac_oid_for_firmware_version(version)
    version = Gem::Version.new(version.to_s) unless version.is_a? Gem::Version

    if version >= GATEWAY_6_2_0
      HARDWARE_MAC
    else
      SYSTEM_NAME
    end
  end

end
