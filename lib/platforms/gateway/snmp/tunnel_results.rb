class Platforms::Gateway::Snmp::TunnelResults

  AGNS_MANAGED = 1

  BLANK_ENDPOINTS = [
    '0.0.0.0',
    '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00'
  ].freeze

  FIELDS = [:ipsec_iface, :duration, :endpoint, :endpoint_ipv6, :endpoint_type,
            :mode, :name, :auth_type, :server_mgmt, :initiator].freeze

  def initialize(device, results = {})
    @device     = device
    @results    = self.class.format(results)
    @tunnel_ids = []
  end

  def process
    results.each do |result|
      next unless validate(result)

      tunnel = device.tunnels.where(tunnel_search_params(result)).first_or_create

      tunnel.assign_attributes({
        endpoint:      formatted_ip_address(result[:endpoint]),
        endpoint_ipv6: formatted_ip_address(result[:endpoint_ipv6]),
        endpoint_type: result[:endpoint_type],
        auth_type:     result[:auth_type],
        agns_managed:  (result[:server_mgmt].to_i == AGNS_MANAGED),
        initiator:     result[:initiator],
        tunnel_mode:   result[:mode],
        tunnel_name:   result[:name]      
      })

      tunnel.save!

      tunnel_ids << tunnel.id
    end

    destroy_obsolete_tunnels
    reset_tunnel_counter
  end

  def self.format(results)
    retval = []
    tunnel_limit = ::Gateway::TUNNEL_LIMIT

    (1..tunnel_limit).each do |i|
      retval << tunnel_data_for_results_and_index(results, i)
    end

    retval
  end

  def self.tunnel_data_for_results_and_index(results, i)
    retval = {}

    FIELDS.each do |field|
      oid           = tunnel_oid_for_index_and_field(i, field)
      result        = results[oid]
      retval[field] = result if result.present?
    end

    retval
  end

  def self.tunnel_oid_for_index_and_field(i, field)
    name = "TUNNEL_#{i}_#{field}".upcase

    @tunnel_oids       ||= {}
    @tunnel_oids[name] ||= ::SNMP.const_get(name) if ::SNMP.const_defined?(name)
  end

  private

  attr_reader :device, :results, :tunnel_ids

  def validate(tunnel)
    tunnel[:ipsec_iface].present? && has_valid_endpoints?(tunnel)
  end

  def valid_endpoint?(endpoint)
    endpoint.present? && !BLANK_ENDPOINTS.include?(endpoint)
  end

  # At least one interface needs a non-blank value
  def has_valid_endpoints?(tunnel)
    [:endpoint, :endpoint_ipv6].any? do |key|
      valid_endpoint?(tunnel[key])
    end
  end

  def tunnel_search_params(tunnel)
    params = { ipsec_iface: tunnel[:ipsec_iface].try(:upcase) }
    [:endpoint, :endpoint_ipv6].each do |key|
      params[key] = formatted_ip_address(tunnel[key]) if valid_endpoint?(tunnel[key])
    end
    params
  end

  def formatted_ip_address(address)
    return nil if BLANK_ENDPOINTS.include?(address)

    # Change IPv6 format from "00:00:00:00..." to "0000:0000..."
    if address.to_s.include?(':') && address.count(':') == 15
      address = address.split(':').each_slice(2).map { |octet| octet.join }.join(':')
    end

    ::TunnelServerIpAddress.formatted_string(address)
  end

  def destroy_obsolete_tunnels
    if tunnel_ids.empty?
      device.tunnels.destroy_all
    else
      device.tunnels.where("id NOT IN (?)", tunnel_ids).destroy_all
    end
  end

  def reset_tunnel_counter
    if device.reload.tunnels_count != tunnel_ids.count
      device.class.reset_counters(device.id, :tunnels)
    end
  end

end
