# Maps the Gateway SNMP modem data to the DeviceModem attributes.
class Platforms::Gateway::Snmp::ModemResultsFormatter

  LTE = 'LTE'

  RESULT_PREFIXES = [
    Platforms::Gateway::Snmp::Objects::MODEM_PREFIX,
    Platforms::Gateway::Snmp::Objects::WAN_PREFIX
  ].freeze

  def initialize(results, with_data_utilization: false)
    @results = results.select { |key, _| key.to_s.start_with?(*RESULT_PREFIXES) }
    @formatted_results = HashWithIndifferentAccess.new
    @with_data_utilization = with_data_utilization

    format_results
  end

  def to_h
    @formatted_results
  end
  alias_method :to_hash, :to_h

  private

  def base_oid(oid)
    oid_digits = oid.split('.')
    oid_digits.first(oid_digits.length-1).join('.')
  end

  def format_results
    return unless @results.present?

    @results.each do |oid, value|
      case base_oid(oid)
      when Platforms::Gateway::Snmp::Objects::MODEM_SLOT
        set_formatted_result(:sim_slot, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_TYPE
        set_formatted_result(:name, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_MANUFACTURER
        set_formatted_result(:maker, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_MODEL
        set_formatted_result(:model, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_REVISION
        set_formatted_result(:revision, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_ID
        # TODO: Check carrier to set esn or imei
        set_formatted_result(:imei, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_IMSI
        set_formatted_result(:imsi, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_PHONE_NUMBER
        set_formatted_result(:number, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_APN
        set_formatted_result(:apn, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_SIM_NUMBER
        set_formatted_result(:iccid, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_SUPPORT
        set_formatted_result(:support_status, value, format: :to_i)
      when Platforms::Gateway::Snmp::Objects::MODEM_UPDATED_AT
        # TODO: Add support for DateAndTime by using MIBs. See JIRA issue AV-1900.
      when Platforms::Gateway::Snmp::Objects::MODEM_CONN_STATUS
        # TODO: See if this provides any value.
      when Platforms::Gateway::Snmp::Objects::MODEM_NETWORK
        set_formatted_result(:cnti, value, format: :upcase)
      when Platforms::Gateway::Snmp::Objects::MODEM_CARRIER
        set_formatted_result(:carrier, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_MCC
        set_formatted_result(:mcc, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_MNC
        set_formatted_result(:mnc, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_LAC
        set_formatted_result(:lac, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_CID
        set_formatted_result(:cid, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_RSSI
        set_formatted_result(:rssi, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_ECIO
        set_formatted_result(:ecio, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_RSRP
        set_formatted_result(:rsrp, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_RSRQ
        set_formatted_result(:rsrq, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_SINR
        set_formatted_result(:sinr, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_SNR
        set_formatted_result(:snr, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_SIGNAL
        set_formatted_result(:bars, value)
      when Platforms::Gateway::Snmp::Objects::MODEM_BAND
        set_formatted_result(:band, value)
      end
    end

    format_signal_strength
    format_data_utilization if @with_data_utilization
    set_formatted_result(:active, true)
  end

  def set_formatted_result(key, value, options = {})
    @formatted_results[key] = formatted_value(value, options)
  end

  def formatted_value(value, options = {})
    retval = "#{value}".strip

    if options[:format] && retval.respond_to?(options[:format])
      retval = retval.send(options[:format])
    end

    retval
  end

  def format_signal_strength
    if lte?
      if @formatted_results[:rsrp].present?
        # Adjust RSRP to RSSI
        set_formatted_result(:signal, (@formatted_results[:rsrp].to_i + 20))
      end
    else
      if @formatted_results[:rssi].present?
        set_formatted_result(:signal, @formatted_results[:rssi])
      end
    end
  end

  def lte?
    @formatted_results[:cnti].present? && @formatted_results[:cnti].upcase == LTE
  end

  def format_data_utilization
    bytes_in  = @results[Platforms::Gateway::Snmp::Objects::WAN_BYTES_IN]
    bytes_out = @results[Platforms::Gateway::Snmp::Objects::WAN_BYTES_OUT]

    set_formatted_result(:rx, bytes_in)  if bytes_in.present?
    set_formatted_result(:tx, bytes_out) if bytes_out.present?
  end
end
