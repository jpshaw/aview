require 'platforms/gateway/snmp/objects'
require 'platforms/gateway/snmp/results'

module Platforms::Gateway::Snmp::Probeable
  extend ActiveSupport::Concern

  DEFAULT_SNMP_VERSION = '2c'
  DEFAULT_COMMUNITY    = 'public'

  def has_snmp_config?
    respond_to?(:configuration) &&
      configuration.present?    &&
      configuration.snmp.present?
  end

  def snmp_config
    {
      host:      snmp_host,
      version:   snmp_version,
      community: snmp_community,
      timeout:   snmp_timeout
    }.with_indifferent_access
  end

  def snmp_host
    domain_name || mgmt_host
  end

  def snmp_version
    DEFAULT_SNMP_VERSION
  end

  def snmp_community
    if has_snmp_config?
      configuration.snmp.community || DEFAULT_COMMUNITY
    else
      DEFAULT_COMMUNITY
    end
  end

  def snmp_timeout
    up? ? 45000 : 15000
  end

  def snmp_objects
    @snmp_objects ||= Platforms::Gateway::Snmp::Objects.new(self).to_a
  end

  def process_snmp_results(results)
    Platforms::Gateway::Snmp::Results.new(self, results).process
  end

  def cell_modem_collection_enabled?
    organization.try(:gateway_cell_modem_collection_enabled?) == true
  end
end
