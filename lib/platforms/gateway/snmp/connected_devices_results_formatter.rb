class Platforms::Gateway::Snmp::ConnectedDevicesResultsFormatter

  CONNECTED_DEVICE_MACS = Platforms::Gateway::Snmp::Objects::CONNECTED_DEVICE_MACS

  attr_reader :results, :formatted_results

  def initialize(results)
    @results = (results || {}).select do |oid, _|
      oid.end_with?(*CONNECTED_DEVICE_MACS)
    end
    @formatted_results = {}
  end

  def execute
    results.each do |oid, value|
      case base_oid(oid)
      when Platforms::Gateway::Snmp::Objects::WAN1_DEVICE_MAC
        set_formatted_result(:eth1, value)
      when Platforms::Gateway::Snmp::Objects::WAN2_DEVICE_MAC
        set_formatted_result(:eth2, value)
      end
    end

    formatted_results.with_indifferent_access
  end

  private

  def base_oid(oid)
    # Remove first dot, if set
    if oid.start_with?('.')
      oid.sub('.', '')
    else
      oid
    end
  end

  def set_formatted_result(key, value)
    @formatted_results[key] = "#{value}".strip
  end

end
