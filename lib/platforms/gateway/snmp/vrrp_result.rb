class Platforms::Gateway::Snmp::VRRPResult
  extend Memoist

  # ngvrMasterStatus(Integer),
  #   vrrpNotRunning(0),
  #   vrrpMasterState(1),
  #   vrrpBackupState(2),
  #   vrrpFaultState(3)
  NOT_RUNNING = 0
  MASTER      = 1
  BACKUP      = 2
  NOT_ACTIVE  = 3

  # TODO: Find a better way to do these strings - mainly used for testing
  NOT_RUNNING_STR = 'Not Running'.freeze
  MASTER_STR      = 'Master'.freeze
  BACKUP_STR      = 'Backup'.freeze
  NOT_ACTIVE_STR  = 'Not Active'.freeze

  STATUS_NAMES = {
    NOT_RUNNING => NOT_RUNNING_STR,
    MASTER      => MASTER_STR,
    BACKUP      => BACKUP_STR,
    NOT_ACTIVE  => NOT_ACTIVE_STR
  }.freeze

  UNKNOWN_STATUS    = 'Unknown'.freeze
  PRIMARY_INTERFACE = 'eth0.4069'.freeze

  attr_reader :index, :interface, :status_code

  def initialize(index: 0, interface: nil, status_code: nil)
    @index = index
    @interface = interface
    @status_code = status_code.to_i
  end

  def primary_interface?
    interface == PRIMARY_INTERFACE
  end

  def status_name
    STATUS_NAMES[status_code] || UNKNOWN_STATUS
  end
  memoize :status_name

  def status
    case status_code
    when NOT_RUNNING
      :not_running
    when MASTER
      :master
    when BACKUP
      :backup
    when NOT_ACTIVE
      :not_active
    else
      :unknown
    end
  end
  memoize :status

  def not_running?
    status == :not_running
  end

  def master?
    status == :master
  end

  def backup?
    status == :backup
  end

  def not_active?
    status == :not_active
  end

  def unknown?
    status == :unknown
  end
end
