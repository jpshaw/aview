require 'platforms/gateway/snmp/modem_results_formatter'

class Platforms::Gateway::Snmp::ModemResults

  MODEM_UNKNOWN = 0
  MODEM_PRESENT = 1
  MODEM_MISSING = 2

  attr_reader :device, :results

  def initialize(device, results = {})
    @device  = device
    @results = Platforms::Gateway::Snmp::ModemResultsFormatter.new(
      results,
      with_data_utilization: @device.using_cellular_as_primary?
    ).to_h
  end

  def process
    if modem_is_present?
      modem.assign_attributes(results)
      generate_events
      modem.save!
      modem.reload
      device.deactivate_old_modems(modem)
    elsif device.modems.any?
      modem_removed_event
      device.modems.destroy_all
    end
  end

  private

  def generate_events
    modem_added_event       if modem.new_record? && modem.valid?
    modem_status_event      if time_for_modem_status_event?
    modem_network_event     if modem.cnti_changed?
    modem_low_signal_event  if modem.signal_changed? && modem.signal_below_threshold?
  end

  def modem_is_present?
    return @modem_is_present if defined? @modem_is_present
    @modem_is_present = results.delete(:support_status) == MODEM_PRESENT
    @modem_is_present
  end

  def modem
    @modem ||= begin
      where_clause = { active: true }

      if modem_data[:iccid].present?
        where_clause[:iccid] = modem_data[:iccid]
      elsif modem_data[:sim_slot].present?
        where_clause[:sim_slot] = modem_data[:sim_slot]
      elsif modem_data[:maker] && modem_data[:model]
        where_clause[:maker] = modem_data[:maker]
        where_clause[:model] = modem_data[:model]
      end

      device.modems.where(where_clause).first_or_initialize
    end
  end

  def modem_added_event
    device.event(
      info: 'Cellular modem detected.', type: :modem, level: :notice,
      data: { modem: { name: modem.name } }
    )
  end

  def modem_removed_event
    device.event(
      info: 'Cellular modem removed.', type: :modem, level: :notice,
      data: { modem: { name: modem.name } }
    )
  end

  def modem_status_event
    device.event(
      info: status_message, type: :modem, data: modem_status_data
    )
  end

  def modem_network_event
    from = modem.cnti_was || Platforms::NONE
    to   = modem.cnti     || Platforms::NONE

    device.event(
      info: "Cellular network changed from #{from} to #{to}",
      type: :modem, level: :notice, uuid: device.uuid_for(:cnti_changed)
    )
  end

  def modem_low_signal_event
    percentage = AdminSettings.threshold_percentage

    device.event(
      info: "Signal below threshold of '#{percentage}%'",
      type: :modem, uuid: device.uuid_for(:signal_changed)
    )
  end

  def status_message
    message = 'Modem status.'

    parts = []
    parts << "Signal: #{modem.signal_pct}" if modem.signal.present?
    parts << "Network: #{modem.cnti}" if modem.cnti.present?

    if parts.any?
      message += ' '
      message += parts.join(', ')
      message += '.'
    end

    message
  end

  def modem_status_data
    modem_data.merge!(dbm: modem_data[:signal]) if modem_data[:signal].present?

    {
      modem: modem_data
    }
  end

  def modem_data
    @modem_data ||= results.dup
  end

  # This is an attempt to only create a modem status event every 30 minutes
  # instead of 15, similar to the frequency of uClinux devices.
  def time_for_modem_status_event?
    return true if modem.new_record?
    return true if last_modem_status_event_created_at.blank?
    last_modem_status_event_created_at.utc < 25.minutes.ago.utc
  end

  def last_modem_status_event_created_at
    @last_modem_status_event_created_at ||= begin
      device.events.where('information like "Modem status%"').order('created_at desc').limit(1).pluck(:created_at).first
    end
  end

end
