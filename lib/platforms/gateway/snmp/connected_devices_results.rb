require 'platforms/gateway/snmp/connected_devices_results_formatter'

class Platforms::Gateway::Snmp::ConnectedDevicesResults

  attr_reader :device, :results

  def initialize(device, results = {})
    @device  = device
    @results = Platforms::Gateway::Snmp::ConnectedDevicesResultsFormatter.new(results).execute
  end

  def process
    results.each do |interface, mac|
      process_interface_association(interface, mac)
    end
  end

  private

  def process_interface_association(interface, mac)
    if mac.blank?
      # Remove interface associations if no device mac is found
      device.device_associations_for_interface(interface).each(&:destroy)
    else
      # Associate to mac and interface
      device.associate_with_mac(mac, source_interface: interface,
                                          source_type: :wan)

      # Remove interface associations that don't match the mac
      associations_for_interface_not_equal_to_mac(interface, mac).each(&:destroy)
    end
  end

  def associations_for_interface_not_equal_to_mac(interface, mac)
    device.device_associations_for_interface(interface).select do |association|
      if association.is_inversed_for?(device)
        association.source_mac != mac
      else
        association.target_mac != mac
      end
    end
  end

end
