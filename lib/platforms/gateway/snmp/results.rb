require 'platforms/gateway/update_firmware_service'
require 'platforms/gateway/update_wan_interface_service'
require 'platforms/gateway/snmp/modem_results'
require 'platforms/gateway/snmp/tunnel_results'
require 'platforms/gateway/snmp/connected_devices_results'
require 'platforms/gateway/snmp/time_ticks'
require 'platforms/gateway/snmp/vrrp_results_processor'

class Platforms::Gateway::Snmp::Results

  attr_reader :device, :results

  def initialize(device, results = {})
    @device  = device
    @results = results
  end

  # Processes the SNMP probe results for the device.
  def process
    return false if results.blank?
    return false if mac_errors?

    device.build_details unless device.details

    results.each do |oid, result|
      next if result.blank?
      process_result(oid, result)
    end

    save_changes
    device.heartbeat!

    process_tunnels
    process_modems
    process_connected_devices
    process_vrrp

    device.save!

    return true
  end

  private

  def save_changes
    device.details.save! if device.details.changed?
    device.save!         if device.changed?
  end

  def mac_errors?
    normalized_mac = normalize_mac(results[mac_oid])

    no_mac_event                     and return true if normalized_mac.blank?
    wrong_mac_event(normalized_mac)  and return true if device.mac != normalized_mac

    return false
  end

  def mac_oid
    @mac_oid ||= Platforms::Gateway::Snmp::Objects.mac_oid_for_firmware_version(device.firmware)
  end

  def normalize_mac(mac)
    mac.upcase.gsub(':', '') if mac.present?
  end

  def no_mac_event
    device.event(
      info: "Failed to find device mac address in probe result",
      level: :error,
      data: { probe: results }
    )
  end

  def wrong_mac_event(probe_mac)
    device.event(
      info: "Wrong mac address returned from probe at host: " \
            "#{device.mgmt_host}. Probe MAC: #{probe_mac}, " \
            "Device MAC: #{device.mac}",
      level: :error,
      data: { probe: results }
    )

    device.update_attributes mgmt_host: nil
  end

  def process_result(oid, result)
    case oid
    # The model of the device. For example: 8100.
    when Platforms::Gateway::Snmp::Objects::HARDWARE_LEVEL
      process_hardware_level_result(result)

    # The device's firmware level.
    when Platforms::Gateway::Snmp::Objects::SOFTWARE_LEVEL
      process_software_level_result(result)

    # The device's primary connection interface.
    when Platforms::Gateway::Snmp::Objects::WAN_INTERFACE
      process_wan_interface_result(result)

    # The device's primary connection type.
    when Platforms::Gateway::Snmp::Objects::WAN_CONNECTION
      process_wan_connection_result(result)

    # How long the device has been in backup mode
    when Platforms::Gateway::Snmp::Objects::TIME_IN_BACKUP
      process_time_in_backup_result(result)

    when Platforms::Gateway::Snmp::Objects::FIRMWARE_ROLLBACK
      process_firmware_rollback_result(result)

    when Platforms::Gateway::Snmp::Objects::DUAL_WAN_STATE
      process_dual_wan_state_result(result)

    when Platforms::Gateway::Snmp::Objects::WAN_IPV4_STATE
      process_wan_ipv4_state_result(result)

    when Platforms::Gateway::Snmp::Objects::WAN_IPV6_STATE
      process_wan_ipv6_state_result(result)

    when Platforms::Gateway::Snmp::Objects::TUNNELS_TOTAL_COUNT
      device.details.tunnels_total_count = result.to_i

    when Platforms::Gateway::Snmp::Objects::TUNNELS_TOTAL_UP
      device.details.tunnels_total_up = result.to_i

    when Platforms::Gateway::Snmp::Objects::TUNNELS_STATUS
      device.details.tunnels_status = result.to_i

    end
  end

  def process_hardware_level_result(result)
    if device.hw_version.nil? || device.hw_version != result
      device.hw_version = result
    end
  end

  def process_software_level_result(result)
    Platforms::Gateway::UpdateFirmwareService.new(device, result).execute
  end

  def process_wan_interface_result(result)
    Platforms::Gateway::UpdateWanInterfaceService.new(
      device, ipv4_iface: result
    ).execute
  end

  def process_wan_connection_result(result)
    if device.details.active_conn_type.nil? || device.details.active_conn_type.to_i != result.to_i
      unless device.details.active_conn_type.nil?
        from_primary_connection_type_name = device.details.primary_connection_type_name
      end

      device.details.active_conn_type = result

      message = "Primary connection type has changed"
      message << " from #{from_primary_connection_type_name}" if from_primary_connection_type_name.present?
      message << " to #{device.details.primary_connection_type_name}"

      device.event(
        info: message,
        level: :notice,
        type: :dhcp,
        data: { primary_connection_type: device.details.primary_connection_type_name }
      )
    end
  end

  # Result is given as a timeticks string
  def process_time_in_backup_result(result)
    seconds_in_backup = Platforms::Gateway::Snmp::TimeTicks.new(result).to_i

    device.details.backup_started_at = \
      if seconds_in_backup.zero?
        nil
      else
        Time.at(Time.now.to_i - seconds_in_backup)
      end
  end

  def process_firmware_rollback_result(result)
    if device.firmware_rollback.nil? || device.firmware_rollback != result
      from_firmware_rollback   = device.firmware_rollback
      device.firmware_rollback = result

      message = "Firmware rollback version changed"
      message << " from #{from_firmware_rollback}" unless from_firmware_rollback.nil?
      message << " to #{result}"

      device.event(
        info:   message,
        level:  :notice,
        type:   :firmware
      )
    end
  end

  def process_dual_wan_state_result(result)
    device.dual_wan_enabled = !(result.to_i == ::GatewayDualWANService::DISABLED)
  end

  def process_wan_ipv4_state_result(result)
    device.details.wan_ipv4_state = result.to_i == 0 ? nil : result.to_i
  end

  def process_wan_ipv6_state_result(result)
    device.details.wan_ipv6_state = result.to_i == 0 ? nil : result.to_i
  end

  def process_tunnels
    Platforms::Gateway::Snmp::TunnelResults.new(device, results).process
  end

  def process_modems
    Platforms::Gateway::Snmp::ModemResults.new(device, results).process
  end

  def process_connected_devices
    Platforms::Gateway::Snmp::ConnectedDevicesResults.new(device, results).process
  end

  def process_vrrp
    Platforms::Gateway::Snmp::VRRPResultsProcessor.run(device, results)
  end

end
