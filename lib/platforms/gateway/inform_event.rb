module Platforms
  module Gateway
    class InformEvent

      attr_reader :event

      def initialize(event)
        @event = event
      end

      def [](key)
        data[key]
      end

      def data
        @data ||= to_h
      end

      def type
        @type ||= data[:type]
      end

      def to_h
        hash = ActiveSupport::HashWithIndifferentAccess.new

        event.instance_variables.each do |ivar|
          key = ivar.to_s.delete('@')
          hash[key] = event.instance_variable_get(ivar)
        end

        # set event type
        hash[:type] = hash.delete(:snmpTrapOID_0) if hash.has_key?(:snmpTrapOID_0)

        # remove logstash keys
        hash.delete(:peer_address)
        hash.delete(:_version)
        hash.delete(:tags)

        hash
      end
    end
  end
end
