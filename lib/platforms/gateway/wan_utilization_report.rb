module Platforms
  module Gateway
    class WanUtilizationReport

      INTERVAL_COUNT = 12

      attr_reader :data, :interface, :organization_id

      def initialize(data, interface, organization_id = 0)
        @data      = data
        @interface = interface
        @organization_id = organization_id
      end

      def mac
        @mac ||= "#{data[:mac]}".upcase
      end

      def device
        @device ||= Device.find_by(mac: mac)
      end

      def supports_single_wan_inform?
        #this version introduced the single WAN Utilization inform
        device.supports_tunnel_info?
      end

      def upload_hwm
        @upload_hwm ||= data.key?(:evDataUpBandw) ? data[:evDataUpBandw].to_i : data[:evDataUpHwm].to_i 
      end

      def download_hwm
        @download_hwm ||= data.key?(:evDataDownBandw) ? data[:evDataDownBandw].to_i : data[:evDataDownHwm].to_i
      end

      def bandwidth_type
        @bandwidth_type ||= data[:evBandwType].to_i
      end

      def bandwidth_test_status
        @bandwidth_test_status ||= data[:ngcoEnabled].to_i
      end

      def created_at
        @created_at ||= ::Gateway.time_from_epoch(data[:evTrapCreatedTime])
      end

      def upload_measurements
        @upload_measurements ||= measurements[:uploads]
      end

      def download_measurements
        @download_measurements ||= measurements[:downloads]
      end

      def report_interval
        @report_interval ||= ((data[:evDataStatsInterv].to_i*60)/5)*data[:evDataSamplesToAvg].to_i
      end

      def sampling_interval
        @sampling_interval ||= (report_interval/interval_count)
      end

      def save_metrics
        time_intervals.each_with_index do |time, index|
          Measurement::WanUtilization.create(
                     mac: mac,
         organization_id: organization_id,
               interface: interface,
                    time: time,
              upload_hwm: upload_hwm,
                  upload: upload_measurements[index],
            download_hwm: download_hwm,
                download: download_measurements[index],
          )
        end
        if supports_single_wan_inform?
          report = Platforms::Gateway::BandwidthTestResult.new(@data, @organization_id, @interface)
          report.save_metrics
        end
      end

      def interval_count
        INTERVAL_COUNT
      end

      private

      def time_intervals
        @time_intervals ||= build_time_intervals
      end

      # build the sampling intervals for this report interval
      def build_time_intervals
        intervals = []

        interval_count.times do |i|
          intervals << (start_time + (sampling_interval * i))
        end

        intervals
      end

      # Informs are sent for the previous interval's data
      def start_time
        @start_time ||= (created_at - report_interval)
      end

      def measurements
        @measurements ||= build_measurements
      end

      def build_measurements
        measurements = {
          uploads:   [],
          downloads: []
        }

        # The inform indexes start at 1 instead of 0.
        1.upto(interval_count) do |i|
          measurements[:uploads] << upload_at_index(i)
          measurements[:downloads] << download_at_index(i)
        end

        measurements
      end

      def upload_at_index(i)
        data["evDataUp#{i}".to_sym].to_i
      end

      def download_at_index(i)
        data["evDataDown#{i}".to_sym].to_i
      end

    end
  end
end
