require 'platforms/event_processor'
require 'platforms/gateway/inform_event'
require 'platforms/gateway/valid_ip_service'
require 'platforms/gateway/update_firmware_service'
require 'platforms/gateway/update_wan_interface_service'
require 'platforms/gateway/events/cellular_events'
require 'platforms/gateway/events/dial_events'
require 'platforms/gateway/events/network_events'
require 'platforms/gateway/events/power_events'
require 'platforms/gateway/events/rogue_mac_events'
require 'platforms/gateway/events/status_events'
require 'platforms/gateway/events/tunnel_events'
require 'platforms/gateway/events/user_events'
require 'platforms/gateway/events/utilization_events'
require 'platforms/gateway/events/ping_events'
require 'update_device_dns'

module Platforms
  module Gateway
    class InformProcessor < Platforms::EventProcessor
      include ::Kraken::Logger.file

      include CellularEvents
      include DialEvents
      include NetworkEvents
      include PowerEvents
      include RogueMacEvents
      include StatusEvents
      include TunnelEvents
      include UserEvents
      include UtilizationEvents
      include PingEvents

      def initialize(device, event)
        super(device, event)
      end

      def event_type
        inform.data[:type]
      end

      def default_event
        create_event "#{event_type} event"
      end

      protected

      def device_details
        device.details ||= device.build_details
      end

      def inform
        @inform ||= InformEvent.new(event)
      end

      def create_event(message, opts = {})
        details = { info: message, type: event_type, level: :info }.merge(opts)
        details[:data] ||= { details[:type] => inform.data }
        device.event(details)
        update_last_restart_timestamp
      end

      def update_last_restart_timestamp
        return unless inform.data[:sysUpTimeInstance].present?
        uptime = Platforms::Gateway::Snmp::TimeTicks.new(inform.data[:sysUpTimeInstance]).to_i
        uptime_seconds = Time.now.to_i - uptime
        converted_uptime = Time.at(uptime_seconds)
        if device.last_restarted_at.present?
          device.update_attribute(:last_restarted_at, converted_uptime) if device.last_restarted_at.to_i < uptime_seconds
        else
          device.update_attribute(:last_restarted_at, converted_uptime)
        end
      end

      def update_management_host
        tunnel_mode  = inform.data[:ngtuTunMode].to_i
        ipv4_address = inform.data[:ngevVPNServiceIPv4Address]
        ipv6_address = inform.data[:ngevVPNServiceIPv6Address]

        return unless tunnel_mode == ::GatewayTunnel::MANAGEMENT_MODE

        # Try IPv6 first
        if valid_ip?(ipv6_address)
          host = ipv6_address
        elsif valid_ip?(ipv4_address)
          host = ipv4_address
        end

        device.mgmt_host = host if host.present?

        if device.mgmt_host_changed?
          device.save!
          UpdateDeviceDns.call(host: host, mac: device.mac)
        end
      end

      def valid_ip?(ip)
        Platforms::Gateway::ValidIpService.new(ip).execute
      end
    end
  end
end
