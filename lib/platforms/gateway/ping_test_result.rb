module Platforms
  module Gateway
    class PingTestResult

      FAILURE = 0
      SUCCESS = 1

      # Used to determine if the destination is IPv4/6.
      BLANK_IPV4 = '0.0.0.0'
      BLANK_IPV6 = '0000:0000:0000:0000:0000:0000:0000:0000'

      attr_reader :data

      def initialize(data)
        @data = data
      end

      def mac
        @mac ||= data[:mac].to_s.upcase
      end

      def result
        @result ||= data[:evResult].to_i
      end

      def success?
        result == SUCCESS
      end

      def failure?
        !success?
      end

      def description
        @description ||= data[:evDescription].to_s.titlecase
      end

      def source
        @source ||= ipv4? ? source_ipv4 : source_ipv6
      end

      def destination
        @destination ||= ipv4? ? destination_ipv4 : destination_ipv6
      end

      # How long the test waited for a response (in milliseconds?)
      def response_max
        @response_max ||= data[:evResponseMax].to_i
      end

      def failure_count
        @failure_count ||= data[:evFailureCount].to_i
      end

      def created_at
        @created_at ||= ::Gateway.time_from_epoch(data[:evTrapCreatedTime])
      end

      private

      def ipv4?
        !ipv6?
      end

      def ipv6?
        destination_ipv6 != BLANK_IPV6
      end

      def source_ipv4
        @source_ipv4 ||= data[:evSrcV4].to_s
      end

      def source_ipv6
        @source_ipv6 ||= data[:evSrcV6].to_s
      end

      def destination_ipv4
        @destination_ipv4 ||= data[:evDestV4].to_s
      end

      def destination_ipv6
        @destination_ipv6 ||= data[:evDestV6].to_s
      end

    end
  end
end