require 'chronic_duration'

module Platforms::Gateway::DialEvents
  extend ActiveSupport::Concern

  included do
    event_type :dial_test_result, :excessive_dial
    alias_event :ngevDialAutotestFailure, :dial_test_result
    alias_event :ngevExcessiveDial, :excessive_dial
  end

  # ngevDialAutotestFailure - The Dial test finished (used to be failure and auto only but repurposed for successes)
  #
  # OBJECTS {
  #   ngevNetGateMac (DisplayString)
  #   ngevDialTestFailReason (DisplayString) - Reason the test failed or 'success'
  #   ngevDialTestFailCategory
  #     INTEGER {
  #       dialProblemError(0),
  #       dialProblemWarning(1),
  #       dialProblemMessage(2),
  #       dialProblemNoCategory(3)
  #     }
  #   ngdiLastTestResult
  #     INTEGER {
  #       failure(0),
  #       success(1),
  #       notNeeded(2)
  #     }
  #   evTestSrc
  #     INTEGER {
  #       webInterface(0),
  #       autoTest(1),
  #       armt(2)
  #     }
  # }
  #
  def dial_test_result
    reason         = inform.data[:ngevDialTestFailReason]
    category_code  = inform.data[:ngevDialTestFailCategory].to_i
    category       = Gateway::DIAL_PROBLEMS[category_code]
    result_code    = inform.data[:ngdiLastTestResult].presence.to_i # if not given, defaults to 0: failure
    result         = Gateway::DIAL_TEST_RESULT[result_code]
    source_code    = inform.data[:evTestSrc].to_i if inform.data[:evTestSrc].present?
    source         = Gateway::DIAL_TEST_SOURCE[source_code]
    log_level      = result_code == 0 ? :alert : :info

    message = "Backup WAN test result: #{result}, " \
              "Reason: #{reason}, Category: #{category}"
    message += ", Source: #{source}" if source.present?

    create_event message, level: log_level, uuid: device.uuid_for(:dbu_failed)

    DialTestResult.create!(device_id: device.id, result: result_code, reason: reason,
                           category: category_code, source: source_code)
  rescue => error
    logger.error "Dial Test Failure: #{error.message}\n  #{inform.data.inspect}"
  end

  # ngevExcessiveDial - The device has been in dial backup longer than the
  # allowed threshold.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #   ngevTimeInDial(Integer32) - Number of minutes the device has been connected via dial
  #   ngifTimeInDial(Timeticks) - How long the current dial connection has been up in hundredths of a second
  #   ngevMaxTimeInDialUntilAlert(Timeticks) - Number of minutes connected via dial before an alert is sent
  #   ngifActiveWanIface(DisplayString) - Primary connection interface
  #
  def excessive_dial
    # Human readable time in dial
    seconds_in_dial = inform.data[:ngevTimeInDial].to_i * 60
    seconds_in_dial = 1 if seconds_in_dial <= 0

    time_in_dial = ChronicDuration.output(seconds_in_dial, format: :long)

    # Human readable threshold
    timeticks = inform.data[:ngevMaxTimeInDialUntilAlert]
    hours, minutes, seconds = timeticks.split(':').map(&:to_i)
    total_seconds = (hours * 3600) + (minutes * 60) + seconds
    total_seconds = 1 if total_seconds <= 0

    threshold = ChronicDuration.output(total_seconds, format: :long)

    message = "Device has been in backup mode longer than the allowed threshold. " \
              "Time in backup: #{time_in_dial}, Threshold: #{threshold}"

    create_event message, level: :warning, uuid: device.uuid_for(:dbu_threshold)
  end

end
