module Platforms::Gateway::UserEvents
  extend ActiveSupport::Concern

  included do
    event_type :ssh_login, :secondary_auth_attempt, :ssh_user_switch
    alias_event :ngevSshLogin,             :ssh_login
    alias_event :ngevSecondaryAuthAttempt, :secondary_auth_attempt
    alias_event :ngevSwitchUseridAttempt,  :ssh_user_switch
  end

  # ngevSshLogin - Sent when a user accesses the device via SSH.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ngevSshSourceIp,
  #   ngevIPv6RequestorIp,
  #   ngevLoginResult - Result of the login attempt
  #     INTEGER {
  #       failure(0),
  #       success(1)
  #     }
  #   ngevUserid - User ID used for ssh login attempt (str)
  # }
  #
  def ssh_login
    source_ip   = inform.data[:ngevSshSourceIp]
    source_ipv6 = inform.data[:ngevIPv6RequestorIp]

    if valid_ip?(source_ip)
      source = source_ip
    elsif valid_ip?(source_ipv6)
      source = source_ipv6
    end

    message = 'SSH login detected.'
    message += " Source IP: #{source}" if source.present?

    create_event message, level: :warning
  end

  # ngevSecondaryAuthAttempt - Sent when a secondary login attempt occurs. Secondary logins are performed
  # when someone tries to ssh into the AT&T NetGate.
  #
  # OBJECTS {
  #   ngevNetGateMac
  #   ngevRequestorIp - IPv4
  #   ngevAccount - Account used for ssh login attempt (str)
  #   ngevUserid - User ID used for ssh login attempt (str)
  #   ngevLoginResult - Result of the login attempt
  #     INTEGER {
  #       failure(0),
  #       success(1)
  #     }
  #   ngevNewLocalID - Userid the user became after successful login attempt
  #   ngevIPv6RequestorIp - IPv6
  # }
  #
  def secondary_auth_attempt
    request_ipv4 = inform.data[:ngevRequestorIp]
    request_ipv6 = inform.data[:ngevIPv6RequestorIp]
    user_id      = inform.data[:ngevUserid]
    account      = inform.data[:ngevAccount]
    new_id       = inform.data[:ngevNewLocalID]
    login_code   = inform.data[:ngevLoginResult].to_i
    result       = Gateway::LOGIN_RESULTS[login_code]

    message = 'SSH login attempt detected. '

    if valid_ip?(request_ipv4)
      request_ip = request_ipv4
    elsif valid_ip?(request_ipv6)
      request_ip = request_ipv6
    end

    parts = []
    parts << "Result: #{result}"           if result.present?
    parts << "Requestor IP: #{request_ip}" if request_ip.present?
    parts << "User ID: #{user_id}"         if user_id.present?
    parts << "Account: #{account}"         if account.present?
    parts << "New Local ID: #{new_id}"     if new_id.present?

    message += parts.join(', ') if parts.any?

    create_event message, level: :warning
  end

  # ngevSwitchUseridAttempt - Sent when a user who is ssh'ed into the AT&T
  # NetGate tries to switch to another userid.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ngevRequestorIp - IPv4
  #   ngevAccount - Account used for ssh login attempt (str)
  #   ngevUserid - User ID used for ssh login attempt (str)
  #   ngevLoginResult - Result of the login attempt
  #     INTEGER {
  #       failure(0),
  #       success(1)
  #     }
  #   ngevNewLocalID - Userid the user became after successful login attempt
  #   ngevOldLocalID - Userid in use before use made switch
  #   ngevIPv6RequestorIp - IPv6
  # }
  #
  def ssh_user_switch
    request_ipv4 = inform.data[:ngevRequestorIp]
    request_ipv6 = inform.data[:ngevIPv6RequestorIp]
    user_id      = inform.data[:ngevUserid]
    account      = inform.data[:ngevAccount]
    new_id       = inform.data[:ngevNewLocalID]
    old_id       = inform.data[:ngevOldLocalID]
    login_code   = inform.data[:ngevLoginResult].to_i
    result       = Gateway::LOGIN_RESULTS[login_code]

    message = 'SSH user switch detected. '

    if valid_ip?(request_ipv4)
      request_ip = request_ipv4
    elsif valid_ip?(request_ipv6)
      request_ip = request_ipv6
    end

    parts = []
    parts << "Result: #{result}"           if result.present?
    parts << "Requestor IP: #{request_ip}" if request_ip.present?
    parts << "User ID: #{user_id}"         if user_id.present?
    parts << "Account: #{account}"         if account.present?
    parts << "Old Local ID: #{old_id}"     if old_id.present?
    parts << "New Local ID: #{new_id}"     if new_id.present?

    message += parts.join(', ') if parts.any?

    create_event message, level: :warning
  end

end
