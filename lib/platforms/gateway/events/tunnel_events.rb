module Platforms::Gateway::TunnelEvents
  extend ActiveSupport::Concern

  included do
    event_type :no_tunnel_endpoints, :tunnel_up, :tunnel_down
    alias_event :ngevNoTeps,     :no_tunnel_endpoints
    alias_event :ngevTunnelUp,   :tunnel_up
    alias_event :ngevTunnelDown, :tunnel_down
  end

  # ngevNoTeps - The AT&T NetGate cannot bring up a tunnel to any of the tunnel endpoints.
  #
  # Params:
  #  ngevNetGateMac(DisplayString)
  #  ngtuIpsecIface - The ipsec interface associated with the tunnel.
  def no_tunnel_endpoints
    interface = inform.data[:ngtuIpsecIface]

    message = 'Unable to bring up a tunnel to any of the tunnel endpoints'
    message += " for interface: #{interface}" if interface.present?

    create_event message, type: :tunnel, level: :notice
  end

  # ngevTunnelUp - Information about the results of a tunnel connection attempt.
  #
  # OBJECTS {
  #   ngevNetGateMac(DisplayString)
  #   ngevTunConnectionId(DisplayString) - ID used to tie the disconnect together
  #   ngevTunnelDateAndTime(DateAndTime) - Timestamp from when the tunnel came up
  #   ngtuTunMode(Integer32) - Tunnel control mode
  #       tunnelUnknown(0),
  #       tunnelUserControl(1),
  #       tunnelUserViewOnly(2),
  #       tunnelHidden(3),
  #       tunnelMaintenance(4),
  #       tunnelWSSInternet(5)
  #
  #   ngevConnectionType(Integer32) - Connection type: Broadband, dial, etc.
  #       connTypeBroadband(0),
  #       connTypeAgnsDial(1),
  #       connTypeOtherIspDial(2)
  #
  #   ngevAccount(DisplayString) - Customer account used to start the tunnel
  #   ngevUserid(DisplayString) - Customer userid used to start the tunnel
  #   ngtuEndpoint(IpAddress) - IP address of the endpoint the tunnel is connected to
  #   ngtuAuthType(Integer32) - Authentication server used by tunnel
  #       internetOnly(0),
  #       serviceManager(1),
  #       customerDirect(2),
  #       branchOffice(3),
  #       customerDirectSm(4),
  #       noTunnelYet(999)
  #
  #   ngtuAuthProt(Integer32) - Authentication protocol
  #       none(0),
  #       custRadius(1),
  #       custSecureID(2),
  #       custAxent(3),
  #       custCert(4),
  #       custLDAP(5),
  #       custInternal(6),
  #       branchSharedSecret(7),
  #       agnsManaged(8)
  #
  #   ngtuEndpointType(Integer32) - Type of endpoint the tunnel is connected to
  #       none(0),
  #       nortel(3),
  #       linuxFrameSig(4),
  #       gig(5),
  #       linuxSig(6),
  #       cisco(9),
  #       vig(20)
  #
  #   ngevTunConReason(DisplayString) - Results of the tunnel connection attempt
  #   nghwWanConnectionMethod(Integer32) - Current WAN connection setting - Not from SMX
  #       wanConfigUnknown(0),
  #       wanConfigStatic (1),
  #       wanConfigDHCP (2),
  #       wanConfigPPPoE (3),
  #       wanConfigDialPri(4),
  #       wanConfigISDNPri(5),
  #       wanConfigPPPoA (6),
  #       wanConfig3GPer (7),
  #       wanConfig3GPri (8)
  #
  #   ngtuTunInitiator(Integer32) - What initiated the tunnel
  #       attUnknown(0),
  #       attFromUser(1),
  #       attAutoLogon(2),
  #       attPersistentLogon(3),
  #       attVpnctl(4)
  #
  #   ngtuIpsecIface(DisplayString) - IPSec interface for the tunnel
  #   ngtuIPv6Endpoint(InetAddressIPv6) - IPv6 address of the endpoint the tunnel is connected to
  #   ngevVPNServiceIPv4Address(IpAddress) - VPN service IPv4 address assigned to the tunnel connection
  #   ngevVPNServiceIPv6Address(InetAddressIPv6) - VPN service IPv6 address assigned to the tunnel connection
  #   ngevTunConReasonNUM(Integer32) - Numeric value that indicates the result of the tunnel connection attempt
  #
  def tunnel_up
    interface = inform.data[:ngtuIpsecIface].to_s.upcase
    reason    = inform.data[:ngevTunConReason].to_s

    # Successfully established tunnel
    if reason =~ /success/i

      # Update the device's management host
      update_management_host

      tunnel = device.tunnels.where(ipsec_iface: interface).first_or_create

      tunnel.connection_id   = inform.data[:ngevTunConnectionId]
      tunnel.started_at      = device.class.time_from_epoch(inform.data[:ngevTunnelDateAndTime])
      tunnel.tunnel_mode     = inform.data[:ngtuTunMode]
      tunnel.connection_type = inform.data[:ngevConnectionType]
      tunnel.account         = inform.data[:ngevAccount].try(:upcase)
      tunnel.user            = inform.data[:ngevUserid].try(:upcase)
      tunnel.auth_type       = inform.data[:ngtuAuthType]
      tunnel.auth_protocol   = inform.data[:ngtuAuthProt]
      tunnel.reason          = inform.data[:ngevTunConReason]
      tunnel.wan_conn_method = inform.data[:nghwWanConnectionMethod]
      tunnel.initiator       = inform.data[:ngtuTunInitiator]
      tunnel.endpoint_type   = inform.data[:ngtuEndpointType]
      tunnel.tunnel_name     = inform.data[:tunName]

      if valid_ip?(inform.data[:ngtuEndpoint])
        tunnel.endpoint      = formatted_ip_address(inform.data[:ngtuEndpoint])
      elsif valid_ip?(inform.data[:ngtuIPv6Endpoint])
        tunnel.endpoint_ipv6 = formatted_ip_address(inform.data[:ngtuIPv6Endpoint])
      end

      tunnel.save!

      message = "#{interface} - #{inform.data[:tunName]} - Tunnel established"
      create_event message, type: :tunnel, level: :notice

    else

      message = "#{interface} Tunnel Error: #{inform.data[:ngevTunConReason]}"
      create_event message, type: :tunnel, level: :alert
    end
  end

  def formatted_ip_address(address)
    TunnelServerIpAddress.formatted_string(address)
  end

  # ngevTunnelDown - Information about a tunnel disconnect. The one exception is a tunnel 'alive' message, which is sent for a
  # tunnel that is still up. Tunnel 'alive' messages have a ngtuReasonCode of tnlAlive.
  #
  # OBJECTS {
  #   ngevNetGateMac (DisplayString)
  #   ngevTunConnectionId (DisplayString)
  #   ngevTunnelDateAndTime (DateAndTime)
  #   ngtuReasonCode
  #     INTEGER {
  #       discoButton(0),
  #       tnlInitiatedDisco(226),
  #       missedHeartBts(227),
  #       eth1Lost(258),
  #       eth1Restored(259),
  #       powerLost(503),
  #       tnlAlive(504),
  #       deviceStatusInactiveWasActive(506),
  #       noTunnInInetOnlyMode(507),
  #       tnlIdleTimeout(508),
  #       dialIdleTimeout(509),
  #       maxSessionTimeout(510),
  #       noReasonGiven(599)
  #     }
  # }
  #
  def tunnel_down
    connection_id = inform.data[:ngevTunConnectionId]
    reason_code   = inform.data[:ngtuReasonCode].to_i
    message_parts = []

    # Add ipsec interface to message if tunnel is found
    if tunnel = device.tunnels.find_by_connection_id(connection_id)
      message_parts << tunnel.ipsec_iface
    end

    message_parts << "#{inform.data[:tunName]}"

    if reason_code == GatewayTunnel::REASON_TUNNEL_ALIVE

      update_management_host

      message_parts << 'Tunnel heartbeat received'
      message = message_parts.join(' - ')

      create_event message, type: :tunnel

    else

      message_parts << 'Tunnel disconnected'
      reason = GatewayTunnel::REASON_CODES[reason_code]
      message_parts << "Reason: #{reason}"
      message = message_parts.join(' - ')

      create_event message, type: :tunnel, level: :warning

      tunnel.destroy if tunnel.present?
    end
  end

end
