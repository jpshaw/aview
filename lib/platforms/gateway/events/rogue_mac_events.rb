module Platforms::Gateway::RogueMacEvents
  extend ActiveSupport::Concern

  included do
    event_type :rogue_mac_detected, :rogue_mac_no_file
    alias_event :ngevRogueMacDetected, :rogue_mac_detected
    alias_event :ngevRogueMacNoFile,   :rogue_mac_no_file
  end

  # ngevRogueMacDetected - The unauthorized MAC detection feature is enabled
  # and an unauthorized MAC address was detected.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #   ngevRogueMac(DisplayString) - MAC address of the unauthorized machine
  #   ngevRogueIp(IpAddress) - IP Address of the unauthorized machine
  #   ngevRogueInterface(DisplayString) - Interface the unauthorized access was detected on
  #   ngevTrapDescription(DisplayString) - SMX description field
  #   ngevIPv6RogueIp(InetAddressIPv6) - The IPv6 IP address of the unauthorized machine detected when MAC monitoring is enabled. Set to zero if not used.
  #
  def rogue_mac_detected
    rogue_mac   = inform.data[:ngevRogueMac]
    rogue_ip_v4 = inform.data[:ngevRogueIp]
    rogue_ip_v6 = inform.data[:ngevIPv6RogueIp]
    rogue_iface = inform.data[:ngevRogueInterface]
    rogue_desc  = inform.data[:ngevTrapDescription]

    if valid_ip?(rogue_ip_v4)
      rogue_ip = rogue_ip_v4
    elsif valid_ip?(rogue_ip_v6)
      rogue_ip = rogue_ip_v6
    else
      rogue_ip = nil
    end

    message = 'Rogue MAC address detected. '

    parts = []
    parts << "MAC: #{rogue_mac}"          if rogue_mac.present?
    parts << "IP: #{rogue_ip}"            if rogue_ip.present?
    parts << "Interface: #{rogue_iface}"  if rogue_iface.present?
    parts << "Description: #{rogue_desc}" if rogue_desc.present?

    message += parts.join(', ') if parts.any?

    create_event message, type: :dhcp, level: :warning
  end

  # ngevRogueMacNoFile - The unauthorized MAC detection feature is enabled and the authorized MAC file doesn't exist.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #
  def rogue_mac_no_file
    message = 'Rogue MAC detection is enabled without a list of authorized '   \
              'addresses. The file needs to be provided. Please contact your ' \
              'support organization.'

    create_event message, type: :dhcp, level: :alert
  end

end
