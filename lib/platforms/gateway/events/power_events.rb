module Platforms::Gateway::PowerEvents
  extend ActiveSupport::Concern

  included do
    event_type :reboot, :shutdown
    alias_event :ngevDeviceReboot,   :reboot
    alias_event :ngevDeviceShutdown, :shutdown
  end

  # ngevDeviceReboot - Sent when the device reboots. Requestor IP will be set to
  # loopback if an automated process caused the reboot.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ngevRequestorIp - IPv4 of the reboot requestor
  #   ngevIPv6RequestorIp - IPv6 of the requestor
  # }
  #
  def reboot
    requestor_ip   = inform.data[:ngevRequestorIp]
    requestor_ipv6 = inform.data[:ngevIPv6RequestorIp]
    reason         = inform.data[:evRebootReason]

    if valid_ip?(requestor_ip)
      host = requestor_ip
    elsif valid_ip?(requestor_ipv6)
      host = requestor_ipv6
    else
      host = nil
    end

    message = 'Device is rebooting.'

    if host.present?
      message += " Requestor IP: #{host}"
    end

    unless reason.blank?
      message += (host.present? ? "," : "") + " Restart Reason: #{I18n.t("gateway_errors")[reason]}"
    end

    create_event message, type: :reboot, level: :alert, uuid: device.uuid_for(:rebooted)

    device.touch :last_restarted_at
    device.tunnels.destroy_all
    device.tunnel_servers.delete_all
  end

  # ngevDeviceShutdown - Sent when the device shuts down.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ngevRequestorIp,
  #   ngevIPv6RequestorIp
  # }
  #
  def shutdown
    requestor_ip   = inform.data[:ngevRequestorIp]
    requestor_ipv6 = inform.data[:ngevIPv6RequestorIp]

    if valid_ip?(requestor_ip)
      host = requestor_ip
    elsif valid_ip?(requestor_ipv6)
      host = requestor_ipv6
    else
      host = nil
    end

    message = 'Device shutdown.'

    if host.present?
      message += " Requestor IP: #{host}"
    end

    create_event message, type: :status, level: :alert

    device.tunnels.destroy_all
    device.tunnel_servers.delete_all
  end

end
