module Platforms::Gateway::StatusEvents
  extend ActiveSupport::Concern

  included do
    event_type :profile_not_found_or_discontinued, :oob_cable_required,
      :successful_query, :zero_touch_query_fail, :port_auth_or_fail, :boot

    alias_event :ngevDeviceDiscoOrNotFound, :profile_not_found_or_discontinued
    alias_event :evOOBCableRequired, :oob_cable_required
    alias_event :evSuccessfulQuery, :successful_query
    alias_event :evZeroTouchQueryFail, :zero_touch_query_fail
    alias_event :evPortAuthOrFail, :port_auth_or_fail
    alias_event :evBoot, :boot
  end

  # ngevDeviceDiscoOrNotFound - Sent if the AT&T NetGate profile is not found.
  # If the AT&T NetGate goes too long without finding its device profile then
  # the device will switch its status to discontinued, which will also trigger
  # a trap/inform.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #   ngevQueryProblem(Integer32) - Specifies what type of problem there was with the query
  #     profileNotFound(0)
  #     discontinuingTheDevice(1)
  #     bandwNoResults(2)
  #
  def profile_not_found_or_discontinued
    problem_id = inform.data[:ngevQueryProblem].to_i

    case problem_id
    when ::Gateway::PROFILE_NOT_FOUND
      message = 'Configuration profile not found for this device. ' \
                'Please contact your support organization.'
      create_event message, type: :config, level: :alert

    when ::Gateway::DISCONTINUED_DEVICE
      message = 'Device discontinued. Please contact your support organization.'
      create_event message, level: :alert

    when ::Gateway::BANDWIDTH_NO_RESULTS
      message = 'Bandwidth test failed and no previous bandwidth test results ' \
                'have been found. Please contact your support organization.'
      create_event message, level: :alert
    end
  end

  # evOOBCableRequired NOTIFICATION-TYPE
  #   OBJECTS { ngevMac, evNotFoundTime, evFoundOOB, evExpectedOOB
  # }
  # STATUS current
  # DESCRIPTION
  #   "Optional USB cables allow out of bound (OOB) access to the
  #   AT&T VPN Gateway. The number of OOB cables expected to be
  #   plugged into the AT&T VPN Gateway is controlled by a setting
  #   in Service Manager. This event is sent when either the
  #   AT&T VPN Gateway boots without the expected number of cables
  #   present, when the AT&T VPN Gateway detects that a cable was
  #   present but has been removed, or when all missing cables are
  #   returned."
  # ::= { ngEventNotification 23 }
  def oob_cable_required
    cables_found    = inform.data[:evFoundOOB].to_i
    cables_expected = inform.data[:evExpectedOOB].to_i

    # All missing cables have been returned
    if cables_found == cables_expected
      create_event "The configured number of OOB cables are present (#{cables_expected})",
        level: :notice, uuid: device.uuid_for(:oob_cables_present)

    # Device booted without expected cables or a cable was removed
    else
      create_event "OOB cables are missing - Found: #{cables_found}, Expected: #{cables_expected}",
        level: :alert, uuid: device.uuid_for(:oob_cables_missing)
    end
  end

  # evSuccessfulQuery NOTIFICATION-TYPE
  #   OBJECTS { ngevMac, evTrapCreatedTime,
  #             evIPv4Rig, evIPv6Rig,
  #             evQueryType, evQueryScope
  #           }
  #   STATUS  current
  #   DESCRIPTION
  #     "Sent after a query to the AT&T Administration Server is successfully
  #      performed."
  #   ::= { ngEventNotification 31 }
  #
  # evQueryType OBJECT-TYPE
  #   SYNTAX      INTEGER {
  #                 initialBootQuery(1),
  #                 dailyQuery(2),
  #                 loginQuery(3),
  #                 dapSetupQuery(4),
  #                 userInitiatedQuery(5),
  #                 partialUserInitiatedQuery(6),
  #                 zeroTouchQuery(8)
  #               }
  #   MAX-ACCESS  accessible-for-notify
  #   STATUS      current
  #   DESCRIPTION
  #     "Indicates what type of query was performed.
  #      initialBootQuery - First query when device boots.
  #      dailyQuery - Query performed daily.
  #      loginQuery - Query performed with certain tunnel login attempts.
  #      dapSetupQuery - Query for setting up Dial as Primary.
  #      userInitiatedQuery - A user initiated a full query.
  #      partialUserInitiatedQuery - A user initiated a partial query.
  #      zeroTouchQuery - A query that runs when the Zero Touch feature is enabled.
  #        This happens at boot time after the initialBootQuery.
  #
  #      Note that partial queries do not store any configuration changes that would
  #      cause a reboot."
  #   ::= { ngEventObjects 68 }
  #
  # evQueryScope OBJECT-TYPE
  #   SYNTAX      INTEGER {
  #                 fullQuery(0),
  #                 partialQuery(1)
  #               }
  #   MAX-ACCESS  accessible-for-notify
  #   STATUS      current
  #   DESCRIPTION
  #     "Indicates the scope of the query that was performed.
  #      Partial queries do not store any configuration changes that would
  #      cause a reboot."
  #   ::= { ngEventObjects 69 }
  #
  def successful_query
    query_scope =
      case inform.data[:evQueryScope].to_i
      when 0
        'Full'
      when 1
        'Partial'
      end

    query_type =
      case inform.data[:evQueryType].to_i
      when 1
        'Initial Boot'
      when 2
        'Daily'
      when 3
        'Login'
      when 4
        'Setting up Dial as Primary'
      when 5
        'User Initiated'
      when 6
        'User Initiated' # Scope will reflect `partial`
      when 8
        'Zero Touch'
      else
        'Unknown'
      end

    message = "Successfully queried the AT&T Administration Server - " \
              "Scope: #{query_scope}, Type: #{query_type}"

    create_event message
  end

  # evZeroTouchQueryFail NOTIFICATION-TYPE
  #   OBJECTS { ngevMac, evTrapCreatedTime
  #           }
  #   STATUS  current
  #   DESCRIPTION
  #           "Sent when Zero Touch is enabled, a Zero Touch query previously was
  #            successful, and the last Zero Touch query failed."
  #   ::= { ngEventNotification 32 }
  #
  def zero_touch_query_fail
    create_event 'Zero Touch query failed'
  end

  # evPortAuthOrFail NOTIFICATION-TYPE
  #   OBJECTS { ngevMac, evTrapCreatedTime,
  #             evExternalDeviceMac, evPort,
  #             evResult, ngvlId
  #           }
  #   STATUS  current
  #   DESCRIPTION
  #           "Sent when an 802.1x port has a successful auth or failure."
  #   ::= { ngEventNotification 33 }
  #
  # evExternalDeviceMac OBJECT-TYPE
  #   SYNTAX      DisplayString
  #   MAX-ACCESS  accessible-for-notify
  #   STATUS      current
  #   DESCRIPTION
  #     "The MAC address of the device that has been connected."
  #   ::= { ngEventObjects 71 }
  #
  # evPort OBJECT-TYPE
  #   SYNTAX      Integer32 (0..65535)
  #   MAX-ACCESS  accessible-for-notify
  #   STATUS      current
  #   DESCRIPTION
  #     "Generic field used to show the port being reported upon by this SNMP trap."
  #   ::= { ngEventObjects 72 }
  #
  # evResult  OBJECT-TYPE
  #   SYNTAX      INTEGER {
  #                 failure(0),
  #                 success(1),
  #                 reset(2)
  #               }
  #   MAX-ACCESS  accessible-for-notify
  #   STATUS      current
  #   DESCRIPTION
  #     "Generic field to indicate a result."
  #   ::= { ngEventObjects 40 }
  #
  # ngvlId OBJECT-TYPE
  #     SYNTAX      Integer32 (0..65535)
  #     MAX-ACCESS  read-only
  #     STATUS      current
  #     DESCRIPTION
  #       "This is the number assigned to the VLAN.
  #        Please note: A VLAN with an ID of 4070 is a special
  #        VLAN used to hold ports that are disabled."
  #     ::= { ngVlanEntry 2 }
  #
  def port_auth_or_fail
    vlan_id = inform.data[:ngvlId].to_i
    port    = inform.data[:evPort].to_i
    result  = inform.data[:evResult].to_i
    external_device_id = inform.data[:evExternalDeviceMac].to_s
    result_str =
      case result
      when 0
        'Failed'
      when 1
        'Succeeded'
      when 2
        'Reset'
      else
        'Unknown'
      end

    message = "802.1x Port Auth #{result_str} - " \
              "Port: #{port}, Device ID: #{external_device_id}, " \
              "VLAN ID: #{vlan_id}"
    level = result == 0 ? :alert : :info

    create_event message, level: level
  end

  # evBoot NOTIFICATION-TYPE
  #   OBJECTS { ngevMac, evTrapCreatedTime,
  #             nghwCodeVersion,
  #             ifIPv4ActiveWanIfaceDescr, ifIPv4ActiveWanIfaceIP,
  #             ifIPv6ActiveWanIfaceDescr, ifIPv6ActiveWanIfaceIP
  #           }
  #   STATUS  current
  #   DESCRIPTION
  #           "Sent only when the device boots up and connectivity is established."
  #   ::= { ngEventNotification 34 }
  #
  def boot
    create_event 'Boot complete'

    firmware = inform.data[:nghwCodeVersion]
    Platforms::Gateway::UpdateFirmwareService.new(device, firmware).execute

    interface_data = {
         ipv4_ip: inform.data[:ifIPv4ActiveWanIfaceIP],
      ipv4_iface: inform.data[:ifIPv4ActiveWanIfaceDescr],
         ipv6_ip: inform.data[:ifIPv6ActiveWanIfaceIP],
      ipv6_iface: inform.data[:ifIPv6ActiveWanIfaceDescr],
    }
    Platforms::Gateway::UpdateWanInterfaceService.new(device, interface_data).execute

    device.touch :last_restarted_at
  end
end
