module Platforms::Gateway::CellularEvents
  extend ActiveSupport::Concern

  included do
    event_type :cell_modem_required
    alias_event :ngevCellModemRequired, :cell_modem_required
  end

  # ngevCellModemRequired - Sent when either the AT&T NetGate boots without a
  # cell modem present or when the AT&T NetGate detects that the cell modem has
  # been removed. The cellular modem must be a supported unit that allows the
  # AT&T NetGate to query its current status.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ng3gModel - 3G card model
  #   ng3gManufacturer - 3G card manufacturer
  #   ng3gID - IMEI or ESN of card
  #   ng3gRevision - 3G card revision
  #   ng3gSIMCardNumber - Number of the card
  #   ng3gPhoneNum - Phone number
  #   ngevCardNotFoundTime - When the card went missing
  # }
  #
  def cell_modem_required
    modem_manufacturer = inform.data[:ng3gManufacturer]
    modem_model        = inform.data[:ng3gModel]

    message = "Cellular modem has been removed. "     \
              "Manufacturer: #{modem_manufacturer}, " \
              "Model: #{modem_model}"

    create_event message, level: :alert
  end

end
