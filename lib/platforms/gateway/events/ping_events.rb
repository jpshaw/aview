require 'platforms/gateway/ping_test_result'

module Platforms::Gateway::PingEvents
  extend ActiveSupport::Concern

  included do
    event_type :ping_test_result
    alias_event :evPingTestResult, :ping_test_result
  end

  # evPingTestResult - Sent to indicate the result of a ping test.
  #
  # OBJECTS {
  #   ngevMac(DisplayString),
  #   evTrapCreatedTime(DateAndTime),
  #   evResult(INTEGER),
  #     failure(0),
  #     success(1),
  #   evSrcV4(IPADDRESS),
  #   evDestV4(IPADDRESS),
  #   evSrcV6(InetAddressIPv6),
  #   evDestV6(InetAddressIPv6),
  #   evResponseMax(INTEGER32),
  #   evFailureCount(INTEGER32),
  #   evDescription(DISPLAYSTRING),
  #   evSendTrapWhen
  # }
  #
  def ping_test_result
    result = Platforms::Gateway::PingTestResult.new(inform.data)

    if result.success?
      create_event "Ping test to #{result.destination} succeeded.",
        uuid: device.uuid_for(:ping_test_succeeded)
    else
      create_event "Ping test to #{result.destination} failed: #{result.description}.",
        level: :alert, uuid: device.uuid_for(:ping_test_failed)
    end
  end

end
