module Platforms::Gateway::NetworkEvents
  extend ActiveSupport::Concern

  included do
    event_type :dhcp_pool_exhausted, :firewall_full, :ip_conn_track_table,
               :wan_ip_changed, :wan2_connection_error, :vrrp_state_change

    alias_event :ngevDhcpPoolExhausted,    :dhcp_pool_exhausted
    alias_event :ngevIpConntrackTableFull, :firewall_full
    alias_event :ngevIpConntrackTable,     :ip_conn_track_table
    alias_event :ngevActiveWanIpChanged,   :wan_ip_changed
    alias_event :evDualWanIfaceProblem,    :wan2_connection_error
    alias_event :evVrrpStateChange,        :vrrp_state_change
  end

  # ngevDhcpPoolExhausted - Sent when a VLAN's DHCP pool is full.
  #
  # OBJECTS {
  #   ngevNetGateMac,
  #   ngvlId - VLAN number (int)
  # }
  #
  def dhcp_pool_exhausted
    vlan_id = inform.data[:ngvlId]

    message = "DHCP pool exhausted. Contact your customer support " \
              "organization if needed. VLAN ID: #{vlan_id}"

    create_event message, type: :dhcp, level: :alert
  end

  # ngevIpConntrackTableFull - Indicates the firewall Connection table has
  # reached capacity.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #
  def firewall_full
    create_event 'Firewall connections table is full.', level: :alert
  end

  # ngevIpConntrackTable - Sent if the Firewall Connection table reaches the
  # specified threshold.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #   ngevIpConntrackCurrentPercentage(Integer32) - Current ip table percentage of max
  #   ngevIpConntrackMaxPercentage(Integer32) - Max percentage before alert is cut
  #
  def ip_conn_track_table
    current = inform.data[:ngevIpConntrackCurrentPercentage]
    maximum = inform.data[:ngevIpConntrackMaxPercentage]

    message = "Firewall connections threshold exceeded. " \
              "Current: #{current}%, Maximum: #{maximum}%"

    create_event message, level: :warning
  end

  # These descriptions are pulled from the ATT-VPN-GATEWAY MIB.
  VRRP_STATES = {
    0 => 'Not Running',
    1 => 'Master',
    2 => 'Backup',
    3 => 'Not Active'
  }

  # evVrrpStateChange - Sent when there is a VRRP state change.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngvlId(Integer32) - The number assigned to the VLAN.
  #   ngvrId(Integer32) - Virtual router ID
  #   ngvrMasterStatus(Integer),
  #     vrrpNotRunning(0),
  #     vrrpMasterState(1),
  #     vrrpBackupState(2),
  #     vrrpFaultState(3)
  #   evTrapCreatedTime(DateAndTime) - When the trap was created
  #
  def vrrp_state_change
    vlan_id    = inform.data[:ngvlId].to_i
    router_id  = inform.data[:ngvrId].to_i
    state_code = inform.data[:ngvrMasterStatus].to_i
    state      = VRRP_STATES[state_code] || 'Unknown'

    device_details.vrrp_state = state

    message = "VRRP state changed to '#{state}' for "
    message += "VLAN ID: #{vlan_id}, Virtual Router ID: #{router_id}."

    create_event message, type: :dhcp, level: :notice, uuid: device.uuid_for(:vrrp_state_changed)

    device_details.save!
    device.save!
  end

  # ngevActiveWanIpChanged - The IP address associated with the active WAN
  # interface has changed. For example, dial backup was initiated because WAN
  # connectivity was lost.  Another example is that a PPPoE connection changed
  # its IP address.
  #
  # Params:
  #   ngevNetGateMac(DisplayString)
  #   ngifActiveWanIface(DisplayString) - Primary connection interface
  #   ngevActiveWanIp(IpAddress) - Address of the active wan interface
  #   ngevIPv6ActiveWanIp(InetAddressIPv6) - Address of the active ipv6 interface (OCTET STRING (SIZE (16)))
  #
  def wan_ip_changed
    interface_data = {
         ipv4_ip: inform.data[:ngevActiveWanIp],
      ipv4_iface: inform.data[:ngifActiveWanIface],
         ipv6_ip: inform.data[:ngevIPv6ActiveWanIp],
      ipv6_iface: inform.data[:ngifActiveWanIface],
    }

    Platforms::Gateway::UpdateWanInterfaceService.new(device, interface_data).execute
  end


  # evDualWanIfaceProblem - Sent when the AT&T VPN Gateway is configured to
  # also use the second WAN port as Internet Primary and one of the interfaces
  # could not be brought up.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   evDualWanProb
  #   evDualWanV6Prob
  #   evTrapCreatedTime
  #
  def wan2_connection_error
    create_event 'Dual WAN mode: there was a problem bringing up the WAN2 connection.', level: :alert
  end

end
