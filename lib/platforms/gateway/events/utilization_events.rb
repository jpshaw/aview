require 'platforms/gateway/wan_utilization_report'

module Platforms::Gateway::UtilizationEvents
  extend ActiveSupport::Concern

  included do
    event_type :bandwidth_test_result, :wan_utilization
    alias_event :evBandwTestResult, :bandwidth_test_result
    alias_event :evDataUsageReport, :wan_utilization_report
  end

  def bandwidth_test_result
    report = Platforms::Gateway::BandwidthTestResult.new(inform.data, device.organization_id)
    report.save_metrics
    create_event 'Bandwidth Test Result Event'
  end

  # evDataUsageReport - Reporting of average bandwidth along with high water
  # mark calculations.
  #
  # Currently, 12 data points are generated for both up and down. This correlates
  # to a sample being taken every 5 minutes over 1 hour.
  #
  # OBJECTS {
  #   ngevMac(DisplayString),
  #   ngifActiveWanIface(DisplayString),
  #   evTrapCreatedTime(DateAndTime),
  #   evDataUpBandw(UNSIGNED32),
  #   evDataDownBandw(UNSIGNED32),
  #   evDataUp1(UNSIGNED32),
  #   evDataUp2(UNSIGNED32),
  #   evDataUp3(UNSIGNED32),
  #   evDataUp4(UNSIGNED32),
  #   evDataUp5(UNSIGNED32),
  #   evDataUp6(UNSIGNED32),
  #   evDataUp7(UNSIGNED32),
  #   evDataUp8(UNSIGNED32),
  #   evDataUp9(UNSIGNED32),
  #   evDataUp10(UNSIGNED32),
  #   evDataUp11(UNSIGNED32),
  #   evDataUp12(UNSIGNED32),
  #   evDataDown1(UNSIGNED32),
  #   evDataDown2(UNSIGNED32),
  #   evDataDown3(UNSIGNED32),
  #   evDataDown4(UNSIGNED32),
  #   evDataDown5(UNSIGNED32),
  #   evDataDown6(UNSIGNED32),
  #   evDataDown7(UNSIGNED32),
  #   evDataDown8(UNSIGNED32),
  #   evDataDown9(UNSIGNED32),
  #   evDataDown10(UNSIGNED32),
  #   evDataDown11(UNSIGNED32),
  #   evDataDown12(UNSIGNED32)
  # }
  #
  def wan_utilization_report
    interface = inform.data[:ngifActiveWanIface] || device.details.active_wan_iface
    interface.to_s.upcase!

    unless is_loopback_interface?(interface)
      utilization = Platforms::Gateway::WanUtilizationReport.new(inform.data, interface, device.organization_id)
      utilization.save_metrics
    end

    create_event 'WAN Utilization Report'
  end

  private

  def is_loopback_interface?(interface)
    interface.present? && interface.start_with?('LO')
  end
end
