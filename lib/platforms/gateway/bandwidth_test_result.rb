module Platforms
  module Gateway
    class BandwidthTestResult
      include ::Kraken::Logger.file

      PRIMARY = "0".freeze

      attr_reader :data, :mac, :interface, :upload_result, :download_result,
        :upload_hwm, :download_hwm, :organization_id, :bandwidth_type, :bandwidth_test_status

      def initialize(data, organization_id = 0, interface = "")
        @data = data
        @mac = data[:mac]
        @organization_id = organization_id
        @interface = get_interface(interface)
        @upload_result = data[:ngcoLastUpstreamResult].to_i
        @download_result = data[:ngcoLastDownstreamResult].to_i
        @upload_hwm = data.key?(:evDataUpBandw) ? data[:evDataUpBandw].to_i : data[:evDataUpHwm].to_i
        @download_hwm = data.key?(:evDataDownBandw) ? data[:evDataDownBandw].to_i : data[:evDataDownHwm].to_i
        @bandwidth_type = data[:evBandwType].to_i
        @bandwidth_test_status = data[:ngcoEnabled].to_i
      end

      def params
        {
          mac: mac,
          organization_id: organization_id,
          interface: interface,
          upload_result: upload_result,
          download_result: download_result,
          upload_hwm: upload_hwm,
          download_hwm: download_hwm,
          bandwidth_type: bandwidth_type,
          bandwidth_test_status: bandwidth_test_status
        }
      end

      def save_metrics
        if interface.present?
          Measurement::BandwidthTestResult.create(params)
        else
          logger.info "interface could not be set for #{mac}"
        end
      end

      private

      def active_wan_iface
        @active_wan_iface ||= device.try(:details).try(:active_wan_iface).to_s.upcase
      end

      def device
        @device ||= Device.find_by(mac: mac)
      end

      def device_in_backup?
        @device_in_backup ||= device.in_backup_mode?
      end

      def device_mode_matches_inform?
        (inform_for_primary? && !device_in_backup?) || (!inform_for_primary? && device_in_backup?)
      end

      def inform_for_primary?
        @inform_for_primary ||= data[:evBandwTestIfacePri].to_s == PRIMARY
      end

      def get_interface(interface)
        unless interface.blank?
          interface
        else
          data[:evTestingIfaceDescr].present? ? data[:evTestingIfaceDescr].to_s : legacy_set_interface
        end
      end

      # Required for FW 6.4.500
      def legacy_set_interface
        active_wan_iface if device_mode_matches_inform?
      end
    end
  end
end
