module Platforms
  module Gateway
    class UpdateWanInterfaceService
      include ::Kraken::Logger.file

      attr_reader :device, :ipv4_ip, :ipv4_iface, :ipv6_ip, :ipv6_iface

      def initialize(device, interface_data)
        @device     = device
        @ipv4_ip    = interface_data[:ipv4_ip].to_s
        @ipv4_iface = interface_data[:ipv4_iface].to_s
        @ipv6_ip    = interface_data[:ipv6_ip].to_s
        @ipv6_iface = interface_data[:ipv6_iface].to_s
      end

      def execute
        update_primary_ip
        update_primary_interface
      end

      private

      def update_primary_ip
        return if ipv4_ip.blank? && ipv6_ip.blank?

        if valid_ip?(ipv4_ip)
          update_host(ipv4_ip)
        elsif valid_ip?(ipv6_ip)
          update_host(ipv6_ip)
        else
          logger.warn "[#{device.mac}] Invalid ip addresses - " \
                      "IPv4: #{ipv4_ip}, IPv6: #{ipv6_ip}"
        end
      end

      def update_primary_interface
        return if ipv4_iface.blank? && ipv6_iface.blank?

        if valid_iface?(ipv4_iface)
          update_active_wan_iface(ipv4_iface)
        elsif valid_iface?(ipv6_iface)
          update_active_wan_iface(ipv6_iface)
        else
          logger.warn "[#{device.mac}] Invalid interfaces - " \
                      "IPv4: #{ipv4_iface}, IPv6: #{ipv6_iface}"
        end
      end

      def valid_ip?(ip)
        Platforms::Gateway::ValidIpService.new(ip).execute
      end

      def valid_iface?(iface)
        iface.present? && iface != 'lo'
      end

      def update_host(ip)
        device.host = ip

        if device.host_changed?
          from = device.host_was || Platforms::NONE

          if device.save
            message = "Primary connection IP changed from #{from} to #{ip}"
            device.event info: message, type: :dhcp, level: :notice,
              uuid: device.uuid_for(:ip_changed)
          else
            logger.error "[#{device.mac}] Failed to save host #{ip}. " \
                         "Errors: #{device.errors.full_messages.join(', ')}"
          end
        end
      end

      def update_active_wan_iface(iface)
        details = device.details || device.build_details
        details.active_wan_iface = iface

        if details.active_wan_iface_changed?
          from = details.active_wan_iface_was || Platforms::NONE

          if details.save
            message = "Primary connection interface changed " \
                      "from #{from} to #{iface}"
            device.event info: message, type: :dhcp, level: :notice,
              uuid: device.uuid_for(:wan_change)
            device.update primary_wan_iface: iface
          else
            logger.error "[#{device.mac}] Failed to save interface #{iface}. " \
                         "Errors: #{details.errors.full_messages.join(', ')}"
          end
        end
      end
    end
  end
end
