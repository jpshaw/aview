module Platforms
  module Gateway
    # TODO: Add ipaddress gem validation
    class ValidIpService

      INVALID_IPS = [
        '0',
        '0.0.0.0',
        '::0',
        '127.0.0.1',
        '1.2.3.4', # TODO: Figure out if this is still needed
        '0:0:0:0:0:0:0:0',
        '0:00:0:00:0:0',
        '0000:0000:0000:0000:0000:0000:0000:0000',
        '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:01'
      ].freeze

      attr_reader :ip

      def initialize(ip)
        @ip = ip.to_s
      end

      def execute
        ip.present? && INVALID_IPS.exclude?(ip)
      end
    end
  end
end
