class NetreachSyslog < DeviceSyslog
  extend DeviceEvents::Util

  def self.status(opts = {})
    opts[:mac] ||= random_mac_address

    syslog = new 'status', opts
    syslog.add :cnt, 3643
    syslog.add :rx, 3027795
    syslog.add :tx, 1949101
    syslog.add :rx10, 852
    syslog.add :tx10, 523
    syslog.add :uptime, '12:20:24 up 25 days, 11:14,  load average: 0.00, 0.00, 0.00'

    # We can also use this
    # syslog.message = 'cnt=3643~rx=3027795~tx=1949101~rx10=852~tx10=523~' \
    #                  'uptime= 12:20:24 up 25 days, 11:14,  load average: 0.00, 0.00, 0.00'

    syslog
  end

end
