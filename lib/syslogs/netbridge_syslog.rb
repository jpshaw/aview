class NetbridgeSyslog < DeviceSyslog
  extend DeviceEvents::Util

  def self.status(opts = {})
    opts[:mac] ||= random_mac_address

    syslog = new 'status', opts
    syslog.add :cnt, 32
    syslog.add :rx, 234350
    syslog.add :tx, 297480
    syslog.add :dbm, -83
    syslog.add :ecio, -31.500
    syslog.add :cnti, 'LTE'
    syslog.add :rssi, -77
    syslog.add :rsrp, -111.000
    syslog.add :rsrq, -15.000
    syslog.add :snr, -3.400
    syslog.add :sinr, 9.000
    syslog.add :mtu, 1422
    syslog.add :prl, 'VER:12135'
    syslog.add :css, '300,D,19'
    syslog.add :qcmipep, 1
    syslog.add :roam, 'All'
    syslog.add :usb, 12

    # We can also use this
    # syslog.message = 'cnt=32~rx=234350~tx=297480~dbm=-83~ecio=-31.500~cnti=LTE~'  \
    #                  'rssi=-77~rsrp=-111.000~rsrq=-15.000~snr=-3.400~sinr=9.000~' \
    #                  'mtu=1422~prl=VER:12135~css=300,D,19~qcmipep=1~roam=All~usb=12'

    syslog
  end

  def self.status_restart(opts = {})
    opts[:mac] ||= random_mac_address(false)

    syslog = new 'status', opts
    syslog.message = 'restart (configuration update)'
    syslog
  end

end
