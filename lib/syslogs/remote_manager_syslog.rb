class RemoteManagerSyslog < DeviceSyslog
  extend DeviceEvents::Util

  def self.status(opts = {})
    opts[:mac] ||= random_mac_address

    syslog = new 'status', opts
    syslog.add :rx, 941692
    syslog.add :tx, 1383585
    syslog.add :type, 'modem'
    syslog.add :idx, 0
    syslog.add :intf, 'eth1'
    syslog.add :dbm, -64
    syslog.add :per, 80
    syslog.add :cnti, 'umts'

    # We can also use this
    # syslog.message = 'type=modem~idx=0~intf=eth1~dbm=-64~per=80~cnti=umts~rx=941692~tx=1383585'

    syslog
  end

end
