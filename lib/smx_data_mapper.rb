# Internal: Maps parsed SMX data to database objects. The data hash used for
# initializing a SmxDataMapper object should come from the output of the `each`
# method in SmxParser.
#
# Examples
#
#   parser = SmxParser.new("/home/user/smx_netgate_list.tab")
#   parser.each do |parsed_data|
#     data = SmxDataMapper.new(parsed_data)
#     # => #<SmxDataMapper:0x44675f26 @data={:customer_name=>"URBAN OUTFITTERS",
#            :device=>{
#              :mac=>"00D0CF1AE26C",
#              :host=>"10.2.70.159",
#              :firmware=>"5.4.126",
#              :hw_version=>"8200"
#            },
#            :site=>{:name=>"TOBY HAMSON"},
#            :account=>"ANRUO",
#            :details=>{
#              :active_conn_type=>"2",
#              :wan_1_conn_type=>"2",
#              :wan_1_circuit_type=>"",
#              :wan_1_cell_extender=> '1',
#              :wan_2_cell_extender=> 'N',
#              :dial_mode=>"",
#              :dial_backup_enabled=>false,
#              :dial_backup_type=>"",
#              :dial_modem_type=>""
#            },
#            :portal_config=>{
#              :password=>"",
#              :ssl_password=>"...",
#              :ssl_port=>443,
#              :auth_user=>"",
#              :auth_password=>""
#            },
#            :snmp_config=>{
#              :version=>"2c",
#              :community=>"urban_5177"
#            },
#            :contact=>{:name=>"SERVICE DESK"},
#            :location=>{
#              :address_1=>"", :address_2=>"", :address_3=>"",
#              :line_1=>"5000 S. BROAD STREET",
#              :line_2=>"",
#              :line_3=>"ATTN: GLENN DONA",
#              :line_4=>"",
#              :city=>"PHILADELPHIA",
#              :state=>"PA",
#              :province=>"",
#              :postal=>"19112",
#              :country=>"UNITED STATES"}}>
#
#     data.customer
#     #=> #<Organization id: 127, name: "Urban Outfitters", ...>
#
#     data.device
#     #=> #<Gateway id: 763, mac: "00D0CF1AE26C", ...>
#
#     data.account
#     #=> #<Account id: 78, name: "ANRUO", ...>
#
#   end
#
class SmxDataMapper

  OBJECTS = [:customer, :device, :site, :snmp_profile, :portal_profile,
             :configuration, :device_details]

  def initialize(data)
    @data = data || {}
  end

  def mac
    @data[:device][:mac]
  end

  def host
    @data[:device][:host]
  end

  def firmware
    @data[:device][:firmware]
  end

  def hw_version
    @data[:device][:hw_version]
  end

  def serial
    @data[:device][:serial]
  end

  def wan_devices
    @data[:wan_devices]
  end

  def contact
    @data[:contact]
  end

  def location
    ::Locations::Upsert.execute(
      address_1:   @data[:location][:line_1],
      city:        @data[:location][:city],
      state:       @data[:location][:state],
      postal_code: @data[:location][:postal]
    )
  end

  def new_location?
    false
  end

  def organization
    if @data[:account].present?
      account_organization   = Organization.where('upper(import_name) = upper(?)', @data[:account]).first
      account_organization ||= Organization.where('upper(name) = upper(?)', @data[:account]).first

      if account_organization.blank?
        params = { name: @data[:account], import_name: @data[:account] }
        result = OrganizationCreator.call(params: params)

        if result.success?
          account_organization = result.organization
        end
      end

      unless customer.children.include?(account_organization)
        customer.add_child(account_organization)
      end

      account_organization
    else
      customer
    end
  end

  # Dynamically create the methods for each object.
  OBJECTS.each do |obj_name|

    # Create boolean accessor for the object, such as 'new_device'
    attr_accessor "new_#{obj_name}"

    # Set the default value for new object boolean to false.
    instance_variable_set("@new_#{obj_name}", false)

    # Create a getter function for each object. This will set the instance
    # variable if not already, and call the get_object method for the value.
    getter = <<-CODE
      def #{obj_name}
        @#{obj_name} ||= get_#{obj_name}
      end

      def new_#{obj_name}?
        @new_#{obj_name} == true
      end
    CODE

    class_eval getter
  end

  private

  # Internal: Gets the customer record.
  #
  # Returns an Organization object for given customer data.
  def get_customer
    get_data :customer do
      organization = Organization.find_by(import_name: customer_name)

      # Couldn't find organization by `import_name`, let's try `name`.
      if organization.blank?
        organization = Organization.find_by(name: customer_name)

        if organization.present?
          # If `import name` is blank, set it to `name`.
          organization.update_attributes(import_name: customer_name) if organization.import_name.blank?
        else
          params = { name: customer_name.titlecase, import_name: customer_name }
          result = OrganizationCreator.call(params: params)

          if result.success?
            organization = result.organization
          end
        end
      end

      organization
    end
  end

  # Internal: Gets the device record.
  # The serial may not be initially present, in which case the lookup
  # would fail.  However, to get the correct hw_version for some models,
  # we need the serial number to be set when setting up the record, so set
  # the serial if it exists, but do so after we lookup/initialize the record.
  #
  # Returns a Gateway object for the given device data.
  def get_device
    get_data :device do
      device_record = Gateway.where(mac: mac)
                             .includes(:details)
                             .first_or_initialize(organization_id: organization.id)
      device_record.serial = serial if serial.present?
      device_record
    end
  end

  # Internal: Get the device's details record. This will check if the active
  # connection type is set, and if so, will delete the key from the import
  # data. All other fields are valid for updating, since they aren't updated
  # by any other source.
  #
  # Returns a GatewayDetails object for the given details data.
  def get_device_details
    get_data :device_details do
      device.build_details unless device.details
      details = device.details
      @data[:details].delete(:active_conn_type) unless details.active_conn_type.blank?
      details.update_attributes(@data[:details])
      details
    end
  end

  # Internal: Gets the site record.
  #
  # Returns a Site object for the given site data.
  def get_site
    get_data :site do
      name = Site.sanitize(@data[:site][:name])

      if name.blank?
        organization.sites.default
      else
        organization.sites.where(:name => name).first_or_initialize
      end
    end
  end

  # Internal: Gets the snmp profile record.
  #
  # Returns an SnmpProfile object for the given snmp config data.
  def get_snmp_profile
    get_data :snmp_profile do
      SnmpProfile.where(@data[:snmp_config]).first_or_initialize
    end
  end

  # Internal: Gets the gateway portal record.
  #
  # Returns an GatewayPortalProfile object for the given portal
  #   config data.
  def get_portal_profile
    get_data :portal_profile do
      @data[:portal_config][:auth_user] ||= "support"
      GatewayPortalProfile.where(@data[:portal_config]).first_or_initialize
    end
  end

  # Internal: Gets the gateway configuration record.
  #
  # Returns an GatewayConfiguration object for the snmp and
  #   portal configurations.
  def get_configuration
    get_data :configuration do
      organization.gateway_configurations
                    .where(
                      snmp_id:    snmp_profile.id,
                      portal_id:  portal_profile.id
                    )
                    .first_or_initialize
    end
  end

  # Internal: Gets a data record from a given block and checks if it is new. If
  # so, it will set that objects new flag to true.
  #
  # name  - The String name of the object.
  # block - The Block that contains the logic for getting the object.
  #
  # Returns the object defined in the block.
  def get_data(name, &block)
    data = block.call

    if data.new_record?
      instance_variable_set("@new_#{name}", true)
      data.save!
    end

    data
  end

  # Internal: Gets the customer name. If the data for customer name is blank,
  # it will return the account name.
  #
  # Returns a String for the customer name.
  def customer_name
    if @data[:customer_name].blank?
      "UNKNOWN: #{@data[:account]}"
    else
      @data[:customer_name].strip
    end
  end

end
