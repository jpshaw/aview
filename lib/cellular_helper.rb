module CellularHelper

  MINIMUM_STRENGTH      = -112.freeze
  MAXIMUM_STRENGTH      = -50.freeze
  CELLULAR_FLAG         = 'cnti'.freeze
  SIGNAL_STRENGTH_FLAG  = 'dbm'.freeze
  DEVICE_BATCH_SIZE     = 250
  EVENT_BATCH_SIZE      = 250


  # Adjusts the singal strength based on the strength range.
  def adjust_signal_strength(signal_strength)
    strength = signal_strength.to_i

    case
      when strength < MINIMUM_STRENGTH; then MINIMUM_STRENGTH
      when strength > MAXIMUM_STRENGTH; then MAXIMUM_STRENGTH
      else; strength
    end
  end


  # Calculates the percentage representation of a signal strength based on the strength range.
  def signal_strength_percentage(signal_strength)
    (((adjust_signal_strength(signal_strength).to_f + 112.0) * 100.0) / 62.0).round(2).abs
  end


  # Calculates the signal strength average of a group of strength readings. `strength_measurements`
  # is a hash containing strength signals as keys and the seconds the strength was active as values.
  def signal_strength_average(strength_measurements)
    total_strength  = 0.0
    total_time      = 0.0

    strength_measurements.each do |strength, time_measurement|
      total_strength  += strength * time_measurement
      total_time      += time_measurement
    end

    total_time.zero? ? 0.0 : (total_strength / total_time).round(2)
  end

end
