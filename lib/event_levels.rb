module EventLevels
  include ConstantsHelper

  EMERGENCY = 0
  ALERT     = 1
  CRITICAL  = 2
  ERROR     = 3
  WARNING   = 4
  NOTICE    = 5
  INFO      = 6
  DEBUG     = 7

  def self.select_list
    [
      ["Debug",   DEBUG  ],
      ["Info",    INFO   ],
      ["Notice",  NOTICE ],
      ["Warning", WARNING],
      ["Error",   ERROR  ],
      ["Alert",   ALERT  ]
    ]
  end
end
