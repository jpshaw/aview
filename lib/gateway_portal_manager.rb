require 'uri'
require 'net/https'
require 'nokogiri'
require 'ipaddr'
require 'micromachine'

# Public: Handles connecting to a GateWay's web portal and executing useful
# commands, such as telling the device to reboot or update its configuration.
class GatewayPortalManager

  HOME_PATH = "/cgi-bin/cgimain?page=19"

  attr_reader :host, :port, :support_user, :support_password

  def initialize(opts = {})
    @host             = opts[:host]
    @admin_password   = opts[:admin_password]
    @support_user     = GatewayPortalProfile::SUPPORT_USER
    @support_password = opts[:support_password]
    @port             = opts[:port] || 443
    @timeout          = 5 # Seconds
    @message          = ""
  end

  class Wget

    attr_reader :manager, :path, :options

    def initialize(manager, path, options)
      @manager = manager
      @path = path
      @options = options
    end

    def perform
      base_uri = "https://#{manager.host_str}:443"
      full_path = "#{base_uri}#{path}"

      args = %W{
        --auth-no-challenge
        --user=#{manager.support_user}
        --password=#{manager.support_password}
        --no-check-certificate
        --post-data='#{post_data}'
        -O -
        '#{full_path}'
      }.join(' ')

      `wget #{args}`
    end

    def self.post(manager, path, options = {})
      new(manager, path, options).perform
    end

    def post_data
      URI.encode(options.map{|k,v| "#{k}=#{v}"}.join("&"))
    end

  end

  # Internal: Returns a formatted string for the host.
  def host_str
    IPAddr.new(@host).ipv6? ? "[#{@host}]" : @host
  end

  # Public: Performs the actions necesssary to reboot the device.
  #
  # Returns nothing.
  def reboot!
    return unless connected?
    html_for_page(disconnect_and_reboot_link, :get, :timeout)
    @message = "Successfully initiated the device reboot."
    success!
  end

  # Public: Performs the actions necessary to update the device's configuration.
  #
  # Returns a String containing the query type set for the device.
  def update_config!
    return unless connected?

    doc = Nokogiri::HTML(profile_update_page_html)
    elements = doc.search('input[@name="queryType"]')

    checked_query_type = checked_value_from_elements(elements)
    update_path = doc.xpath("//form[@name='form']").attr('action').value

    result_html = Wget.post(self, update_path, queryType: checked_query_type)

    if result_html =~ /Downloading and processing/i
      @message = "Successfully initiated the device configuration update."
      success!
    else
      @message = "Unexpected page result."
      failure!
    end

    self.class.query_name_for_type(checked_query_type)
  end

  # Public: Returns the Symbol state of the device.
  def state
    machine.state
  end

  # Public: Returns a String description of the state.
  def state_desc
    case state
    when :initializing
      "Initializing."
    when :connecting
      "Connecting to the device's web portal."
    when :connection_failed
      "Unable to reach device at #{host_str}:#{@port}."
    when :connection_refused
      "Connection refused for #{host_str}:#{@port}."
    when :http_auth_failed
      "HTTP authentication failed."
    when :connected
      "Connected to the device's web portal."
    when :admin_auth_failed
      "Admin authorization failed."
    when :success
      if @message.blank?
        "Successfully executed the command."
      else
        @message
      end
    when :failure
      if @message.blank?
        "Failed to execute the command."
      else
        @message
      end
    end
  end

  # Public: Checks if the command succeeded.
  #
  # Returns true if the state indicates that the command succeeded;
  #   otherwise false.
  def command_succeeded?
    state == :success
  end

  # Public: Returns the name of a query type, given the type.
  #
  # type - The String identifier for the type.
  #
  # Returns a String name for the query type, if found; otherwise nil.
  def self.query_name_for_type(type)
    case type.to_i
    when 2
      "full"
    when 5
      "partial"
    end
  end

  # Public: Returns a Hash of available access levels.
  def self.access_levels
    { :user  => '3', :admin => '2' }
  end

  protected

  # Internal: Changes the state to indicate that the connection to the device
  # has failed.
  #
  # Returns true if the state transition occurred; otherwise false.
  def connection_failed!
    machine.trigger(:connection_failed)
  end

  # Internal: Changes the state to indicate that the connection to the device
  # was refused.
  #
  # Returns true if the state transition occurred; otherwise false.
  def connection_refused!
    machine.trigger(:connection_refused)
  end

  # Internal: Changes the state to indicate that the connection to the device
  # has succeeded.
  #
  # Returns true if the state transition occurred; otherwise false.
  def connected!
    machine.trigger(:connected)
  end

  # Internal: Changes the state to indicate that the http authentication failed.
  #
  # Returns true if the state transition occurred; otherwise false.
  def http_auth_failed!
    machine.trigger(:http_auth_failed)
  end

  # Internal: Changes the state to indicate that a the manager is connecting to
  # the device's web portal.
  #
  # Returns nil if the state transition fails. If the transition occurs, then it
  #   returns the html of the home page.
  def connect!
    return unless machine.trigger(:connecting)

    if @host.blank?
      connection_failed!
      return
    end

    html_for_page(HOME_PATH)
  end

  # Internal: Changes the state to indicate that the admin authentication failed.
  #
  # Returns true if the state transition occurred; otherwise false.
  def admin_auth_failed!
    machine.trigger(:admin_auth_failed)
  end

  # Internal: Changes the state to indicate that a failure occurred.
  #
  # Returns true if the state transition occurred; otherwise false.
  def failure!
    machine.trigger(:failure)
  end

  # Internal: Changes the state to indicate that the command executed
  # successfully.
  #
  # Returns true if the state transition occurred; otherwise false.
  def success!
    machine.trigger(:success)
  end

  # Internal: Checks if the manager is connected to the device's web portal.
  # It will initiate a test connection, if one has not been done so already.
  #
  # Returns true if the manager connected to the device's web portal;
  #   otherwise false.
  def connected?
    connect!
    state == :connected
  end

  # Internal: The state machine.
  def machine
    @machine ||= begin
      fsm = MicroMachine.new(:initializing)

      fsm.when(:connecting,         :initializing => :connecting)
      fsm.when(:connection_failed,  :connecting   => :connection_failed)
      fsm.when(:connection_refused, :connecting   => :connection_refused)
      fsm.when(:http_auth_failed,   :connecting   => :http_auth_failed)
      fsm.when(:connected,          :connecting   => :connected)
      fsm.when(:admin_auth_failed,  :connected    => :admin_auth_failed)
      fsm.when(:failure,            :initializing => :failure,
                                    :connecting   => :failure,
                                    :connected    => :failure)
      fsm.when(:success,            :connected    => :success)

      fsm
    end
  end

  # Internal: Checks if the portal requires an admin password.
  #
  # Returns true if the portal requires an admin password; otherwise false.
  def login_required?
    authentication_form_present?(html_for_page(HOME_PATH))
  end

  # Internal: Checks if the login form is present in the given HTML.
  #
  # html - The String html to check.
  #
  # Returns true if the login form is found in the given HTML; otherwise false.
  def authentication_form_present?(html)
    retval = false

    doc  = Nokogiri::HTML(html)
    form = doc.xpath("//form[@name='form']").first

    unless form.nil?
      password_element = form.at('input[@name="accessPwd"]')
      unless password_element.nil?
        retval = true
      end
    end

    retval
  end

  # Internal: Returns the HTML for the home page.
  def home_html
    if login_required?
      options = {
        accessPwd: self.class.access_levels[:admin],
        password:  @admin_password
      }

      html = Wget.post(self, HOME_PATH, options)
    else
      http, request = http_request_for_page(HOME_PATH, :get)
      html = html_for_http_request(http, request)
    end

    # If login is still required, then we can assume that the admin
    # authorization failed.
    admin_auth_failed! if authentication_form_present?(html)

    html
  end

  # Internal: Returns a String of HTML for the reboot page.
  def reboot_page_html
    html_for_page(reboot_page_link)
  end

  # Internal: Returns a String of HTML for the disconnect/reboot page.
  def disconnect_and_reboot_page_html
    html_for_page(reboot_button_link)
  end

  # Internal: Returns a String of HTML for the 'update profile' page.
  def profile_update_page_html
    html_for_page(update_config_page_link)
  end

  # Internal: Returns a String containing a link to the reboot page.
  def reboot_page_link
    link = ""

    search_html(home_html) do |doc|
      commands = doc.xpath("//div[@id='cpanelSub3'] //table //table").first
      unless commands.nil?
        link = commands.css("td")[2]["onclick"].gsub(/window.location=|'|"|;/,'')
      end
    end

    link
  end

  # Internal: Returns a String containing a link to initiate a reboot.
  def reboot_button_link
    link = ""

    search_html(reboot_page_html) do |doc|
      anchor = doc.xpath("//div //table //tr //td //div //a").first
      unless anchor.nil?
        link = anchor["href"]
      end
    end

    link
  end

  # Internal: Returns a String containing a link to initiate a disconnect
  # and reboot.
  def disconnect_and_reboot_link
    link = ""

    search_html(disconnect_and_reboot_page_html) do |doc|
      anchor = doc.xpath("//div //table //tr //td //div //a").first
      unless anchor.nil?
        link = anchor["href"]
      end
    end

    link
  end

  # Internal: Returns a String containing a link to the 'update config' page.
  def update_config_page_link
    link = ""

    search_html(home_html) do |doc|
      commands = doc.xpath("//div[@id='cpanelSub3'] //table //table").first
      unless commands.nil?
        link = commands.css("td")[1]["onclick"].gsub(/window.location=|'|"|;/,'')
      end
    end

    link
  end

  # Internal: Provides a Nokogiri document containing the given HTML to the block.
  #
  # html  - The String of HTML to search.
  # block - The Block containing the search logic.
  #
  # Returns nothing.
  def search_html(html, &block)
    doc = Nokogiri::HTML(html)
    block.call(doc)
  end

  # Internal: Gets HTML for a given path and action.
  #
  # path   - The String path to navigate to.
  # action - The HTTP verb to use.
  #
  # Returns a String of HTML for the path and action.
  def html_for_page(path, action = :get, expectation = nil)
    http, request = http_request_for_page(path, action)
    html_for_http_request(http, request, expectation)
  end

  # Internal: Gets HTML for the given http and request objects.
  #
  # http        - The Net::HTTP object.
  # request     - The Net::HTTPRequest request object, such as Net::HTTP::Get.
  # expectation - The Symbol of the expected state for the request (optional).
  #
  # Returns a String of HTML for the http and request objects.
  def html_for_http_request(http, request, expectation = nil)
    html = ""

    begin
      response = http.request(request)

      case response
      when Net::HTTPSuccess
        html = response.body
        connected!
      when Net::HTTPUnauthorized
        http_auth_failed!
      else
        @message = response.message
        failure!
      end
    rescue Timeout::Error => e
      connection_failed! unless expectation == :timeout
    rescue Errno::ECONNREFUSED => e
      connection_refused!
    rescue => e
      @message = e.message
      failure! unless expectation == :failure
    end

    html
  end

  # Internal: Gets the http and request objects for a given path and action.
  #
  # path   - The String path.
  # action - The HTTP verb to use.
  #
  # Returns a tuple containing the Net::HTTP and Net::HTTPRequest objects
  #   for the given path and action.
  def http_request_for_page(path, action = :get)
    uri = URI.parse("http://#{host_str}:#{@port}#{path}")

    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = @timeout
    http.read_timeout = @timeout
    http.use_ssl      = true
    http.verify_mode  = OpenSSL::SSL::VERIFY_NONE

    request = case action
              when :get
                Net::HTTP::Get.new(uri.request_uri)
              when :post
                Net::HTTP::Post.new(uri.request_uri)
              end

    # Http auth is always turned on. We are guaranteed the support user will
    # always work.
    request.basic_auth(@support_user, @support_password)

    [http, request]
  end


  # Internal: Enumerates over the given elements and returns the value of the
  # first element that is 'checked'.
  #
  # elements - An Array of Nokogiri::XML::Element objects.
  #
  # Returns the value of the 'checked' element, if found; otherwise nil.
  def checked_value_from_elements(elements)
    elements.each do |element|
      element.attributes.each do |attribute|
        if attribute[0] == "checked"
          return element.attr("value")
        end
      end
    end
    nil
  end
end
