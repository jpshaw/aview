require 'gateway/command'

class GatewayCommand < DeviceCommand

  # Public: Attempts to send a command to a gateway devce. Depending on the
  # type of command, it will either attempt to probe the gateway, have it
  # check it's config, or try to reboot it.
  def send!
    if @device.supports_json_commands?
      if command == 'status'
        command_status
      else
        Gateway::Command.new(device: @device,
                            command: @command,
                               data: @data).perform
      end
    else
      case command
      when "status"
        command_status
      when "config"
        command_update_config
      when "reboot"
        command_reboot
      else
        command_failed!
        @message = "Invalid Command"
        @device.event :info => @message, :type => :remote, :level => :error
      end
    end
  end

  private

  # Internal: Probes a device for it's status. This will send an SNMP Get
  # to all of the OID objects defined in `snmp_objects`.
  def command_status
    @device.event info: 'Attempting to probe device ...',
      level: :info, type: :remote, data: { remote: snmp_data }

    begin
      probe = SnmpProber.new(:snmp    => @device.snmp_config,
                             :objects => @device.snmp_objects)
      probe.execute!

      if probe.successful? && @device.process_snmp_results(probe.results)

        command_succeeded!
        @device.fetch_lan_status
        @device.event info: 'Successfully probed device', level: :notice,
          type: :remote, data: { probe: probe.results }

      else

        command_failed!
        @device.event info: 'Failed to probe device', type: :remote, level: :error

      end

    rescue => e
      command_failed!
      @device.event info: 'No response received when contacting the device. Please verify ' \
                          'connectivity of the device and that its remote/maintenance ' \
                          'tunnel is established.',
                    level: :error, type: :remote, data: { error: e.message }
    end
  end

  # Internal: Tells the device to update it's configuration. This will use
  # the device's HTML web panel to execute the command.
  def command_update_config
    @device.event info: 'Attempting to initiate a device profile update ...',
      level: :info, type: :remote, data: { remote: portal_data }

    begin
      portal = GatewayPortalManager.new(@device.portal_config)
      query_type = portal.update_config!

      if portal.command_succeeded?
        command_succeeded!
        @device.event :info => portal.state_desc, :level => :info, :type => :remote
        @message = "Device is applying a #{query_type} profile update. " \
                  "Please allow a few minutes for the process to complete."
        @device.event :info => @message, :level => :info, :type => :remote
      else
        command_failed!
        @device.event :info => portal.state_desc, :level => :error, :type => :remote
        @message = "Failed to initiate the device profile update. Please " \
                  "contact your support organization for troubleshooting help."
        @device.event :info => @message, :level => :error, :type => :remote
      end
    rescue => e
      command_failed!
      @message = "Unexpected error occurred while executing the command. " \
                "Please contact application support."
      @device.event :info => @message, :level => :error, :type => :remote,
        :data => { :error => e.message }
    end
  end

  # Internal: Tells the device to reboot. This will use the device's HTML
  # web panel to execute the command.
  def command_reboot
    @device.event info: 'Attempting to reboot device ...',
      level: :info, type: :remote, data: { remote: portal_data }

    begin
      portal = GatewayPortalManager.new(@device.portal_config)
      portal.reboot!

      if portal.command_succeeded?
        command_succeeded!
        @device.event :info => portal.state_desc, :level => :info, :type => :remote
        @message = "Device is rebooting. Please allow a few minutes for the " \
                  "process to complete."
        @device.event :info => @message, :level => :notice, :type => :remote
      else
        command_failed!
        @device.event :info => portal.state_desc, :level => :error, :type => :remote
        @message = "Failed to initiate device reboot. Please contact your " \
                  "support organization for troubleshooting help."
        @device.event :info => @message, :level => :error, :type => :remote
      end
    rescue => e
      command_failed!
      @message = "Unexpected error occurred while executing the command. " \
                "Please contact application support."
      @device.event :info => @message, :level => :error, :type => :remote,
        :data => { :error => e.message }
    end
  end

  # A hash of the portal configuration with sanitized passwords.
  def portal_data
    data = @device.portal_config
    data[:support_password] = sanitize_password(data[:support_password])
    data
  end

  # A hash of the snmp configuration. We do not use SNMPv3 so there is no need
  # to sanitize the auth/priv passwords.
  def snmp_data
    @device.snmp_config
  end

  # Show the first and last characters of the password, with stars in between.
  def sanitize_password(str)
    str ||= ''

    first  = str.first
    middle = str[1..-2] || ''
    last   = str.last

    stars  = Array.new(middle.length) { '*' }.join

    "#{first}#{stars}#{last}"
  end
end
