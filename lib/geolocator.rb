class Geolocator
  attr_reader :latitude, :longitude, :accuracy

  def self.get_location(cell_tower_data)
    response = request_location(cell_tower_data)
    result_as_hash = JSON.parse(response.body, :symbolize_names => true) if response
    create_instance(result_as_hash) unless errors?(result_as_hash)
  end

  def self.can_get_cell_location?(cell_tower_data)
    cid_is_valid?(cell_tower_data[:cid]) &&
    lac_is_valid?(cell_tower_data[:lac]) &&
    cell_tower_data[:mnc].present? &&
    cell_tower_data[:mcc].present?
  end

  private

  def initialize(location_data)
    @latitude = location_data[:latitude]
    @longitude = location_data[:longitude]
    @accuracy = location_data[:accuracy]
  end

  def self.create_instance(lookup_result)
    Geolocator.new(latitude: lookup_result[:location][:lat],
                   longitude: lookup_result[:location][:lng],
                   accuracy: lookup_result[:accuracy])
  end

  def self.cid_is_valid?(cid)
    cid.present? &&
    string_is_integer(cid) &&
    cid.to_i > 0
  end

  def self.lac_is_valid?(lac)
    lac.present? &&
    string_is_integer(lac) &&
    lac.to_i > 0
  end

  def self.request_location(cell_tower_data)
    request_params = get_params cell_tower_data
    uri = URI.parse("https://www.googleapis.com/geolocation/v1/geolocate?key=#{Settings.api_keys.google}")
    request = get_request(uri, request_params)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    make_request(http, request)
  end

  def self.get_params(cell_tower_data)
    params = {homeMobileCountryCode: cell_tower_data[:mcc], homeMobileNetworkCode: cell_tower_data[:mnc]}
    cell_towers = [{cellId: cell_tower_data[:cid].to_i,
                    locationAreaCode: cell_tower_data[:lac].to_i,
                    mobileCountryCode: cell_tower_data[:mcc],
                    mobileNetworkCode: cell_tower_data[:mnc]}]
    params[:cellTowers] = cell_towers
    params
  end

  def self.get_request(uri, request_params)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = request_params.to_json
    request['Content-Type'] = 'application/json'
    request
  end

  def self.make_request(http, request)
    begin
      http.request(request)
    rescue => error
      Rails.logger.error("Geolocator failed to connect to the service\n#{error.message}\n#{error.backtrace[0, 3].join("\n")}" )
      return nil
    end
  end

  def self.errors?(result_hash)
    result_hash.nil? || result_hash.has_key?(:error)
  end

  def self.string_is_integer(string)
    /\A[-+]?\d+\z/ === string
  end
end
