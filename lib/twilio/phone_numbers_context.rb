module Twilio
  class PhoneNumbersContext
    def initialize(client)
      @client = client
    end

    def get(number)
      @client.fetch(
        :get,
        "#{Twilio.phone_numbers_url}/#{number}"
      )
    end
  end
end
