module Twilio
  class MessagesContext
    def initialize(client)
      @client = client
    end

    def create(to:, body:)
      @client.fetch(
        :post,
        "#{Twilio.accounts_url}/#{Twilio.account_sid}/Messages.json",
        {
          'From' => Twilio.from,
          'To'   => to,
          'Body' => body
        }
      )
    end
  end
end
