module Twilio
  class Client

    DEFAULT_OPTIONS = {
      ssl: {
        verify_mode: OpenSSL::SSL::VERIFY_NONE
      }
    }

    def phone_numbers
      ::Twilio::PhoneNumbersContext.new(self)
    end

    def messages
      ::Twilio::MessagesContext.new(self)
    end

    def fetch(method, uri, params = {})
      response = request(method, uri, params)

      if response.code < 200 || response.code >= 300
        raise exception(response, 'Unable to fetch record')
      end

      JSON.parse(response.body.to_s)
    end

    private

    def request(method, uri, params = {})
      HTTP.basic_auth(auth).public_send(
        method,
        uri,
        DEFAULT_OPTIONS.merge(params_key(method) => params)
      )
    end

    def auth
      { user: Twilio.account_sid, pass: Twilio.auth_token }
    end

    def params_key(method)
      method == :get ? :params : :form
    end

    def exception(response, header)
      message   = header
      code      = response.code
      json_body = JSON.parse(response.body.to_s) rescue nil

      if json_body
        message += ": #{json_body['message']}" if json_body['message']
        code = json_body['code'] if json_body['code']
      end

      ::Twilio::RestError.new(message, code, response.code)
    end
  end
end
