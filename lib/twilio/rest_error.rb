module Twilio
  class RestError < StandardError
    attr_reader :message, :code, :status_code

    def initialize(message, code, status_code)
      @message     = message
      @code        = code
      @status_code = status_code
    end

    def to_s
      "[HTTP #{status_code}] #{code} : #{message}"
    end
  end
end
