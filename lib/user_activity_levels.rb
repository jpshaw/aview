# Public: Contains the enumerations used for filtering user's activity field.
module UserActivityLevels
  ALL        = 1
  LAST_DAY   = 2
  LAST_WEEK  = 3
  LAST_MONTH = 4
  LAST_YEAR  = 5
  NEVER      = 6

  def self.select_list
    [
      ["All",        ALL       ],
      ["Last Day",   LAST_DAY  ],
      ["Last Week",  LAST_WEEK ],
      ["Last Month", LAST_MONTH],
      ["Last Year",  LAST_YEAR ],
      ["Never",      NEVER]
    ]
  end
end
