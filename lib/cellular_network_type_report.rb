class CellularNetworkTypeReport < CellularReport

  NO_NETWORK = 'NO NETWORK'.freeze


  private

  def interval_seconds
    return @interval_seconds if @interval_seconds

    # Divide all available points between all the networks and get seconds per interval.
    points_per_network    = HIGHCHARTS_POINTS_LIMIT / networks.size
    range_in_seconds      = (end_time - start_time).to_i
    seconds_per_interval  = range_in_seconds / points_per_network

    # Use either default interval (if more seconds than in calculation) or calculate interval seconds.
    @interval_seconds = \
      if seconds_per_interval <= INTERVAL_SECONDS
        INTERVAL_SECONDS
      else
        (seconds_per_interval.to_f / INTERVAL_SECONDS.to_f).ceil * INTERVAL_SECONDS
      end
  end


  # Retrieve networks first so we can calculate the intervals later.
  def networks
    return @networks if @networks

    # Get all networks for the events within the time range.
    @networks = [NO_NETWORK] +
                DeviceEvent
                  .where(device_id: report_record.devices.pluck(:id))
                  .where('created_at >= ?', start_time)
                  .where('created_at <= ?', end_time)
                  .where("raw_data like '%#{CELLULAR_FLAG}%'")
                  .group_by {|event| event.raw_data['modem']['cnti']}
                  .keys

    # Add all the initial networks for the devices.
    report_record.devices.find_each(batch_size: DEVICE_BATCH_SIZE) do |device|
      @networks << initial_network(device)
    end

    # De-dup array.
    @networks.uniq!

    @networks
  end


  # Get event right outside of start time bounds, sets the initial network. This allows devices that
  # have a network before the bounds to have statistics, even if they don't have any network events
  # within the defined bounds.
  def initial_value(device)
    initial_network(device)
  end


  def initial_network(device)
    event_network(initial_event_of_type(device, report_type_flag))
  end


  def event_network(event)
    network_name = event.raw_data['modem'][report_type_flag].upcase # TODO: The upcase can be removed after 3/26/16
  # Rescueing call to `.raw_data` on event being `nil` or raw_data without valid info.
  rescue NoMethodError => e
    network_name = default_value
  ensure
    return network_name
  end


  def report_type_flag
    CELLULAR_FLAG
  end


  def default_value
    NO_NETWORK
  end


  def event_value(event)
    event_network(event)
  end


  def update_report_record_stats_headers
    report_record.params.merge!(
      stats_headers: networks.sort.reduce({'most_used_network' => 'Most Used Network'}) do |hash, network|
        hash.merge(network.downcase => "#{network} (%)")
      end
    )
  end


  def table
    table = []

    results.each do |device, intervals|
      table << [
        device.mac,
        device.host,
        (device.category.name      if device.category),
        (device.series.name        if device.series),
        device.organization_name,
        nil,
        (device.site.name          if device.site),
        device.deployed? ? "Deployed" : "Undeployed",
        device.last_heartbeat_at,
        stats_to_percentages(statistics(intervals))
      ]
    end

    table
  end


  def statistics(intervals)
    stats = Hash.new(0)

    intervals.each do |_, networks_info|
      networks_info.each do |network, seconds|
        stats[network.downcase] += seconds
      end
    end

    stats.merge!(
      # Sort networks by seconds used ascending and get last one's name.
      'most_used_network' => (stats.sort_by {|_,v| v}.last.first if stats.present?)
    )

    stats
  end


  # Converts networks seconds usage to percentage usage in stats.
  def stats_to_percentages(stats)
    most_used_network = stats.delete('most_used_network')
    stats             = networks_seconds_to_percentages(stats)

    stats.each do |network, percentage|
      stats[network] = percentage
    end

    stats.merge('most_used_network' => most_used_network)
  end


  def networks_seconds_to_percentages(networks_info)
    total_seconds = networks_info.values.inject(:+)

    networks_info.each do |network, seconds|
      networks_info[network] = percentage_usage(seconds, total_seconds)
    end

    networks_info
  end


  def percentage_usage(network_seconds, total_seconds)
    total_seconds = total_seconds.to_f
    total_seconds.zero? ? 0 : (network_seconds.to_f * 100.0 / total_seconds).round(2)
  end


  def pie_chart
    {
      chart: {
        plotBackgroundColor:  nil,
        plotBorderWidth:      nil,
        plotShadow:           false
      },
      credits:    {enabled: false},
      exporting:  {enabled: Settings.charts.exporting},
      title:      {text: 'Cellular Network Type Summary'},
      tooltip:    {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor:           'pointer',
          dataLabels: {
            enabled:        true,
            color:          '#000000',
            connectorColor: '#000000',
            format:         '<b>{point.name}</b>: {point.percentage:.1f} %',
            distance:       20
          }
        }
      },
      series: [
        {
          type: 'pie',
          name: 'Total',
          data: pie_chart_series_data
        }
      ]
    }
  end


  # Generates series data for pie chart.
  def pie_chart_series_data
    # Initialize a hash of all networks found with 0 seconds each.
    networks_info = Hash.new(0)

    # Count all seconds for each network.
    results.each do |_, intervals|
      intervals.each do |_, networks|
        networks.each do |network, seconds|
          networks_info[network] += seconds
        end
      end
    end

    networks_seconds_to_percentages(networks_info).sort
  end


  def trend_chart
    {
      chart: {
        type:         'spline',
        zoomType:     'x',
        spacingRight: 20,
        marginTop:    '60'
      },
      credits:    {enabled: false},
      exporting:  {enabled: Settings.charts.exporting},
      title:      {text: "Cellular Network Type"},
      xAxis: {
        type:     'datetime',
        maxZoom:  3 * 3600000, # 1 day
        title:    {text: "Time"}
      },
      yAxis: {
        min:    0,
        max:    100,
        title:  {text: "Network (%)" }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.1f}%</b><br/>',
        shared: true
      },
      plotOptions: {
        spline: {
          lineWidth:  2,
          marker:     {enabled: false}
        }
      },
      series: trend_chart_series
    }
  end


  # Generates series for trend chart.
  def trend_chart_series
    intervals_summary_hash  = summarize_results_by_interval
    trend_series            = []

    networks.sort.each do |network|
      network_hash = {name: network, data: []}

      intervals_summary_hash.each do |interval, networks_data|
        total_interval_seconds  = networks_data.values.reduce(:+)
        network_seconds         = networks_data[network]

        network_hash[:data] << [
          interval.to_i * 1000,
          percentage_usage(network_seconds, total_interval_seconds)
        ]
      end

      trend_series << network_hash
    end

    trend_series
  end

end
