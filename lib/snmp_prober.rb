require 'snmp4jruby' if RUBY_PLATFORM == 'java' && ENV['RAILS_ENV'] != 'test'

class SnmpProber

  attr_accessor :errors, :results

  NO_AUTH_NO_PRIV = 1
  AUTH_NO_PRIV    = 2
  AUTH_PRIV       = 3

  def initialize(config = {})

    @config = config[:snmp]
    @config[:timeout] ||= 15000
    @objects = config[:objects]

    @errors = []
    @results = {}
    @valid = false
    _validate
  end

  def execute!

    unless valid?
      @errors << "Invalid probe request"
      return
    end

    # create snmp target object
    target = create_snmp_target

    # run object with oids
    results = target.get_all(snmp_objects)

    if results.nil?
      @errors << "Device unreachable"
    else

      # Add results
      results.each do |result|
        key   = result.oid.to_s.strip
        value = result.variable.to_s.strip

        next if value.blank?
        next if value == "noSuchObject"
        next if value == "noSuchInstance"
        next if value == "endOfMibView"

        @results[key] = value
      end

    end

  end

  def successful?
    @errors.empty?
  end

  # Return first error for event. Called after 'successful?' check.
  def error
    @errors.first
  end

  protected
  def valid?
    @valid == true
  end

  def _validate
    if @config[:host].blank?
      @errors << "No management address"
    else
      @valid = true
    end
  end

  def create_snmp_target
    SNMPTarget.new(snmp_config)
  end

  def snmp_config
    snmp_config_additions = case @config[:version]
                            when "3"
                              snmp_v3_config
                            else
                              snmp_v2_config
                            end
    snmp_base_config.merge(snmp_config_additions)
  end

  def snmp_base_config
    {
      :host      => @config[:host],
      :timeout   => @config[:timeout],
      :version   => version
    }
  end

  def snmp_v2_config
    {
      :community => @config[:community]
    }
  end

  def snmp_v3_config
    {
      :username       => @config[:username],
      :security_level => security_level,
      :auth_protocol  => auth_protocol,
      :auth_password  => @config[:auth_password],
      :priv_protocol  => priv_protocol,
      :priv_password  => @config[:priv_password]

    }
  end

  def version
    case @config[:version]
    when "3"
      SNMP4JR::MP::Version3
    else
      SNMP4JR::MP::Version2c
    end
  end

  def security_level
    case @config[:security_level]
    when NO_AUTH_NO_PRIV
      SNMP4JR::Security::SecurityLevel::NOAUTH_NOPRIV
    when AUTH_NO_PRIV
      SNMP4JR::Security::SecurityLevel::AUTH_NOPRIV
    else # AUTH_PRIV
      SNMP4JR::Security::SecurityLevel::AUTH_PRIV
    end
  end

  def auth_protocol
    case @config[:auth_protocol]
    when "SHA"
      SNMP4JR::Security::AuthSHA::ID
    when "MD5"
      SNMP4JR::Security::AuthMD5::ID
    end
  end

  def priv_protocol
    case @config[:priv_protocol]
    when "DES"
      SNMP4JR::Security::PrivDES::ID
    when "3DES"
      SNMP4JR::Security::Priv3DES::ID
    when "AES"
      SNMP4JR::Security::PrivAES::ID
    when "AES128"
      SNMP4JR::Security::PrivAES128::ID
    when "AES192"
      SNMP4JR::Security::PrivAES192::ID
    when "AES256"
      SNMP4JR::Security::PrivAES256::ID
    end
  end

  def snmp_objects
    if @objects.nil? || @objects.empty?
      ["1.3.6.1.2.1.1.1.0"]
    else
      @objects
    end
  end

end
