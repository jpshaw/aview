require 'singleton'


class HeartbeatCache

  include Singleton

  def keys
    cache.keys
  end


  def get(key)
    cache.get(key)
  end


  def put(key, value)
    cache.put(key, value)
  end


  def remove(key)
    cache.remove(key)
  end


  def clustering_mode
    cache.clustering_mode
  end


  def eviction_strategy
    cache.eviction_strategy
  end


  def max_entries
    cache.max_entries
  end


  def self.put(key, value)
    instance.put(key, value)
  end


  private

  attr_reader :cache

  def cache
    @cache ||= ::TorqueBox::Infinispan::Cache.new(
      name:     Settings.torquebox.services.DeviceHeartbeatMonitorService.cache_name,
      persist:  Settings.torquebox.services.DeviceHeartbeatMonitorService.cache_persist_directory,
      mode:     :replicated,
      sync:     false
    )
  end

end
