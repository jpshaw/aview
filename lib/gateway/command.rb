require 'gateway/api/client'

class Gateway < Device
  class Command
    attr_accessor :device, :command, :data, :host, :port, :user, :password, :ping_timeout

    TUNNEL_TYPE_INDEX = 'index'
    TUNNEL_TYPE_NAME  = 'name'

    def initialize(opts = {})
      @device   = opts[:device]
      @command  = opts[:command]
      @data     = opts[:data]
      @host     = device.mgmt_host
      @port     = 443
      @user     = device.user
      @password = device.password
      @ping_timeout = 30 # seconds
    end

    def perform
      validate!

      __send__ command

    rescue NoMethodError => e
      failure "Command not recognized: `#{command}`",
        data: {
          error:     e.message,
          data:      data,
          backtrace: e.backtrace[0,3]
        }
    rescue MissingCredentialsError
      failure "Unable to find remote auth credentials for device"
    rescue Errno::EHOSTUNREACH
      failure "Unable to reach device at host: #{host}"
    rescue Gateway::API::Error::NotFound
      failure "Device does not support remote commands"
    rescue Gateway::API::Error::Unauthorized
      failure "Invalid API credentials"
    rescue => e
      failure "Unexpected error",
        data: {
          error:     e.message,
          data:      data,
          backtrace: e.backtrace[0,3]
        }
    end

    private

    class MissingCredentialsError < StandardError; end

    def arpping
      data = data_for(:target, :src_int, :ipv6)

      info "Sending arpping test request for target '#{data[:target]}' ..."

      results = client.arpping(data)

      if results.ok?
        info 'Arpping test completed. Click to view results.', data: { results: results.to_a }
      else
        failure 'Arpping test failed'
      end
    end

    def ping
      data = data_for(:target, :src_int, :count, :size, :ipv6)

      info "Sending ping test request for target '#{data[:target]}' ..."

      results = client.ping(data)

      if results.ok?
        info 'Ping test completed. Click to view results.', data: { results: results.to_a }
      else
        failure 'Ping test failed'
      end
    end

    def full_profile_query
      info 'Attempting to initiate a full profile query ...'

      results = client.full_profile_query

      if results.ok?
        info 'Successfully initiated a full profile query. Please allow a few minutes for the process to complete.'
      else
        failure 'Failed to initiate full profile query'
      end
    end

    def partial_profile_query
      info 'Attempting to initiate a partial profile query ...'

      results = client.partial_profile_query

      if results.ok?
        info 'Successfully initiated a partial profile query. Please allow a few minutes for the process to complete.'
      else
        failure 'Failed to initiate partial profile query'
      end
    end

    def query_status
      info 'Checking profile query status ...'

      results = client.query_status

      if results.ok?
        info "Successfully fetched profile query status. Click to view results.",
          data: {
            results: {
              last_query: results.last_queried_at.utc,
              last_query_type: results.type,
              next_query: results.next_query_at.utc
            }
          }
      else
        failure 'Failed to get profile query status'
      end
    end

    def reboot
      info 'Attempting to reboot device ...'

      result = client.reboot

      if result.ok?
        info 'Device is rebooting. Please allow a few minutes for the process to complete.'
      else
        failure 'Failed to initiate device reboot'
      end
    end

    def tracepath
      data = data_for(:target, :src_int, :ipv6)

      info "Sending tracepath test request for target '#{data[:target]}' ..."

      results = client.tracepath(data)

      if results.ok?
        info 'Tracepath test completed. Click to view results.', data: { results: results.to_a }
      else
        failure 'Tracepath test failed'
      end
    end

    def traceroute
      data = data_for(:target, :src_int, :ipv6)

      info "Sending traceroute test request for target '#{data[:target]}' ..."

      results = client.traceroute(data)

      if results.ok?
        info 'Traceroute test completed. Click to view results.', data: { results: results.to_a }
      else
        failure 'Traceroute test failed'
      end
    end

    def tunnel_up
      info "Attempting to bring tunnel '#{tunnel_name}' up ..."

      result = client.tunnel_up(tunnel_data)

      if result.ok?
        info "Successfully brought tunnel '#{tunnel_name}' up"
      else
        failure "Failed to bring tunnel '#{tunnel_name}' up", support_message: ""
      end
    end

    def tunnel_down
      info "Attempting to bring tunnel '#{tunnel_name}' down ..."

      result = client.tunnel_down(tunnel_data)

      if result.ok?
        info "Successfully brought tunnel '#{tunnel_name}' down"
      else
        failure "Failed to bring tunnel '#{tunnel_name}' down", support_message: ""
      end
    end

    def tunnel_bounce
      info "Attempting to bounce tunnel '#{tunnel_name}' ..."

      result = client.tunnel_bounce(tunnel_data)

      if result.ok?
        info "Successfully bounced tunnel '#{tunnel_name}'"
      else
        failure "Failed to bounce tunnel '#{tunnel_name}'", support_message: ""
      end
    end

    def tunnel_status
      info "Attempting to get status for tunnel '#{tunnel_name}' ..."

      result = client.tunnel_status(tunnel_data)

      if result.ok?
        results = {
          tunnel: {
            name: result.name,
            index: result.index,
            up: result.up?
          }
        }

        if result.up?
          results[:tunnel][:uptime] = result.uptime
        end

        info "Successfully fetched status for tunnel '#{tunnel_name}'. Click to view results.",
          data: results
      else
        failure "Failed to get status for tunnel '#{tunnel_name}'", support_message: ""
      end
    end

    def tunnels
      info 'Fetching status for all tunnels ...'

      results = client.tunnels

      if results.any?
        tunnels = results.to_a.map(&:to_h)
        count   = tunnels.count

        info "Successfully fetched the status for #{count} device #{'tunnel'.pluralize(count)}. Click to view results.",
          data: {
            tunnels: tunnels
          }
      else
        failure "Failed to get status for device tunnels", support_message: ""
      end
    end

    def upgrade
      info 'Sending upgrade request ...'

      result = client.upgrade

      if result.ok?
        message = \
          if result.available?
            "The device is upgrading to firmware version #{result.available_version}. Please allow a few minutes for the process to complete."
          else
            "The device has the latest firmware installed: #{result.current_version}"
          end

        info message, data: {
          upgrade: {
            available: result.available_version,
            current: result.current_version,
            can_upgrade: result.available?
          }
        }
      else
        failure 'Upgrade failed'
      end
    end

    def upgrade_check
      info 'Checking upgrade status ...'

      result = client.upgrade_check

      if result.ok?
        info 'Successfully fetched upgrade status. Click to view results.',
          data: {
            upgrade: {
              available: result.available_version,
              current: result.current_version,
              can_upgrade: result.available?
            }
          }
      else
        failure 'Upgrade check failed'
      end
    end

    def rollback
      info 'Sending rollback request ...'

      result = client.rollback

      if result.ok?
        message = \
          if result.available?
            "The device is rolling back to firmware version #{result.available_version}. Please allow a few minutes for the process to complete."
          else
            "Unable to perform firmware rollback: The available rollback version (#{result.available_version}) is equal to or greater than the current version (#{result.current_version})."
          end

        info message, data: {
          upgrade: {
            available: result.available_version,
            current: result.current_version,
            can_rollback: result.available?
          }
        }
      else
        failure 'Rollback failed'
      end
    end

    def rollback_check
      info 'Checking firmware rollback status ...'

      result = client.rollback_check

      if result.ok?
        info 'Successfully fetched the firmware rollback version. Click to view results.',
          data: {
            rollback: {
              available: result.available_version,
              current: result.current_version,
              can_rollback: result.available?
            }
          }
      else
        failure 'Rollback check failed'
      end
    end

    # Internal command used to update device's LAN details
    def vlanconfig
      result = client.vlanconfig

      if result.ok?
        result.save_lan_networks(@device)
        # This command is only called internally, so no need to create an event
        # on a successful response.
      else
        failure 'Failed to fetch Vlan and LAN details.'
      end
    end

    def backup_wan_test
      info 'Performing manual backup WAN test ...'

      results = client.backup_wan_test

      if results.ok?
        info 'Running a manual backup WAN test. Please wait. A separate event containing '\
             'the test results will be generated from the device once the test is complete.'
      else
        failure 'Failed to run manual backup WAN test.'
      end
    end

    def wan_toggle
      data = data_for(:interface, :state, :time)

      info "Sending WAN toggle request for interface '#{data[:interface]}' ..."

      results = client.wan_toggle(data)

      if results.ok?
        info 'WAN toggle completed. Click to view results.', data: { results: results.to_a }
      else
        error 'WAN toggle error', data: { results: results.status }
      end
    end

    def cx_access
      data = data_for(:v4_src, :v6_src, :src_addr, :src_int, :dst_addr, :access, :maintenance_tunnel, :drop_interface)

      info "Sending CX Access command to device"

      results = client.cx_access(data)

      if results.ok?
        info 'CX Access completed. Click to view results.', data: { results: results.to_a }
      else
        error 'CX Access error', data: { results: results.status }
      end
    end

    def client
      @client ||= Gateway::API::Client.new do |client|
        client.host     = host
        client.user     = user
        client.password = password
        client.use_ssl  = true
      end
    end

    def device_uri
      ipv6_safe_addr = host.to_s.include?(':') ? "[#{host}]" : host
      "https://#{ipv6_safe_addr}:#{port}"
    end

    def validate!
      raise MissingCredentialsError.new unless has_credentials?
    end

    def has_credentials?
      @has_credentials ||= [user, password].all?(&:present?)
    end

    def data_for(*keys)
      hash = ActiveSupport::HashWithIndifferentAccess.new
      keys.each do |key|
        next unless data[key].present?
        hash[key] = data[key]
      end
      hash
    end

    def info(message, data = {})
      @device.event info: message, level: :info, type: :remote, data: data
    end

    def error(message, data = {})
      @device.event info: message, level: :error, type: :remote, data: data
    end

    def failure(message, data = {}, support_message: "Please contact your support organization for troubleshooting help.")
      error "#{message}. #{support_message}", data
    end

    def tunnel_params
      @tunnel_params ||= data_for(:tunnel_type, :index, :name)
    end

    def tunnel_type
      @tunnel_type ||= tunnel_params[:index].present? ? TUNNEL_TYPE_INDEX : TUNNEL_TYPE_NAME
    end

    def tunnel_id
      @tunnel_id ||= tunnel_params[tunnel_type]
    end

    def tunnel_data
      @tunnel_data ||= { tunnel_type => tunnel_id }.with_indifferent_access
    end

    def tunnel_name
      @tunnel_name ||= begin
        if tunnel_type == TUNNEL_TYPE_INDEX
          "IPSEC#{tunnel_id}"
        else
          "#{tunnel_id}".upcase
        end
      end
    end
  end
end
