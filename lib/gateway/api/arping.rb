require 'gateway/api/utils'
require 'gateway/api/arping_results'

class Gateway < Device
  module API
    module Arping
      include Gateway::API::Utils

      # @return [Gateway::API::ArpResults]
      def arping(params = {})
        raise ArgumentError.new('A :target must be given') unless params[:target]

        command = ipv6?(params) ? :arpping6 : :arpping

        options = {
          cmd:     command,
          target:  params[:target],
          src_int: (params[:src_int] || default_interface)
        }

        perform_get_with_object(default_path, options, Gateway::API::ArpingResults)
      end
      alias_method :arpping, :arping
    end
  end
end
