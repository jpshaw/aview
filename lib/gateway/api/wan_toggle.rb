require 'gateway/api/utils'
require 'gateway/api/wan_toggle_results'

class Gateway < Device
  module API
    module WanToggle
      include Gateway::API::Utils

      DEFAULT_INTERFACE = 'primary'
      DEFAULT_TIME = 0
      DEFAULT_STATE = 'on'
      MAX_TIME = 1440
      STATES = ['on', 'off', 'status'].freeze

      def wan_toggle(params = {})
        raise ArgumentError.new('invalid interface given') if params[:interface].present? && invalid_interface?(params[:interface])
        raise ArgumentError.new('invalid state given') if params[:state].present? && invalid_state?(params[:state])
        raise ArgumentError.new('invalid time given') if params[:time].present? && invalid_time?(params[:time])

        options = {
          cmd: 'interface_control',
          interface: (params[:interface] || DEFAULT_INTERFACE),
          sub_cmd: (params[:state] || DEFAULT_STATE),
          time: (params[:time] || DEFAULT_TIME)
        }

        perform_get_with_object(default_path, options, Gateway::API::WanToggleResults)
      end

      private

      def invalid_time?(time)
        return true unless time.scan(/\D/).empty?
        time.to_i < 0 || time.to_i > MAX_TIME
      end

      def invalid_state?(state)
        STATES.exclude?(state)
      end

      def invalid_interface?(state)
        state != 'primary' && state != 'secondary'
      end
    end
  end
end
