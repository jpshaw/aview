require 'gateway/api/results'

class Gateway < Device
  module API
    class Upgrade < Gateway::API::Results

      def current_version
        @current_version ||= results.fetch(:current_version)
      end

      def available_version
        @available_version ||= results.fetch(:available_version)
      end

      def performed?
        @performed ||= results.fetch(:performed) == true
      end

      def has_latest_firmware?
        @has_latest_firmware ||= results.fetch(:latest_firmware) == true
      end

      def available?
        current   = Gem::Version.new(current_version)
        available = Gem::Version.new(available_version)
        available > current
      end

    private

      def results
        return @results if @results

        @results = { performed: false, latest_firmware: false }

        self.each do |result|
          case result
          when /current.*version:/i
            @results[:current_version] = result.split(':').last.strip
          when /available.*version:/i
            @results[:available_version] = result.split(':').last.strip
          when /installed code is current/i
            @results[:latest_firmware] = true
          when /upgrade.*starting/i
            @results[:performed] = true
          end
        end

        @results
      end
    end
  end
end
