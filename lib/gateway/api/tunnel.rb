require 'gateway/api/object'

class Gateway < Device
  module API
    class Tunnel < Gateway::API::Object
      attr_reader :index, :name, :uptime, :status

      UP   = 'up'.freeze
      DOWN = 'down'.freeze

      # @return [Boolean]
      def up?
        status == UP
      end

      # @return [Boolean]
      def down?
        status == DOWN
      end

      def to_h
        {
          name:   name,
          index:  index,
          status: status,
          result: result,
          uptime: uptime
        }
      end
      alias_method :to_hash, :to_h
    end
  end
end
