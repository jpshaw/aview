class Gateway < Device
  module API
    module Enumerable
      include ::Enumerable

      # return [Enumerator]
      def each(start = 0)
        return to_enum(:each, start) unless block_given?
        Array(@collection[start..-1]).each do |element|
          yield(element)
        end
      end
    end
  end
end
