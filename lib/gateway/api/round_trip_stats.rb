require 'gateway/api/object'

class Gateway < Device
  module API
    class RoundTripStats < Gateway::API::Object
      REGEX = /^rtt/

      attr_accessor :min, :max, :avg, :mdev, :units
      alias_method :minimum, :min
      alias_method :maximum, :max
      alias_method :average, :avg
      alias_method :standard_deviation, :mdev

      # "rtt min/avg/max/mdev = 30.304/30.390/30.467/0.066 ms"
      def self.parse(result)
        unless result && result.match(REGEX)
          raise ArgumentError.new('A valid round trip result must be given')
        end

        # Remove the round trip token
        result.gsub!(REGEX, '')

        # Slice the units off the end
        units = result.slice(-2, 2)
        result.gsub!(units, '')

        # Remove extra spaces
        result.strip!

        # Get keys and values
        keys, values = result.split(' = ').map { |results| results.split('/') }

        # Convert stats to floats
        values.map! { |value| value.to_f }

        # Add units
        keys << 'units'
        values << units

        attributes = Hash[keys.zip(values)]

        new(attributes)
      end
    end
  end
end
