require 'http'
require 'addressable/uri'
require 'json'
require 'gateway/api/error'

class Gateway < Device
  module API
    class Request
      attr_accessor :request_method, :path, :options, :uri
      alias_method :verb, :request_method

      DEFAULTS = {
        ssl: {
          # NOTE: If we were able to configure Gateway certificates we could
          # start enforcing HTTPS hostnames, and/or use PowerDNS to map
          # the management tunnel IPv4/v6 Address to whatever is configured for
          # the device's HTTPS certificate.
          verify_mode: OpenSSL::SSL::VERIFY_NONE
        }
      }

      def initialize(client, request_method, path, options = {})
        @auth = client.auth
        @headers = client.headers
        @request_method = request_method
        @uri = Addressable::URI.parse("#{client.base_uri}#{path}")
        @options = DEFAULTS.merge(request_key => options)
      end

      def perform
        response = HTTP.basic_auth(@auth).headers(@headers).public_send(@request_method, @uri.to_s, http_options)

        response_body = ''
        while str = response.body.readpartial
          response_body += str
        end

        body = fail_or_return_response_body(response.code, response_body)

        JSON.parse(json_sanitize(body), symbolize_names: true)
      rescue JSON::ParserError
        body
      end

    private

      def json_sanitize(string)
        string.gsub("\t", '')
      end

      def http_options
        @options.dup.delete_if { |k, v| HTTP::Options.defined_options.exclude?(k) }
      end

      def request_key
        request_method == :get ? :params : :form
      end

      def fail_or_return_response_body(code, body)
        error = error(code)
        fail(error) if error
        body
      end

      def error(code)
        error = Gateway::API::Error::ERRORS[code]
        if !error.nil?
          error.new
        end
      end

    end
  end
end
