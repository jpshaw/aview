require 'gateway/api/utils'
require 'gateway/api/object'

class Gateway < Device
  module API
    module Reboot
      include Gateway::API::Utils

      # @return [Gateway::API::Object]
      def reboot
        options = { cmd: :reboot }
        perform_get_with_object(default_path, options, Gateway::API::Object)
      end
    end
  end
end
