require 'gateway/api/utils'
require 'gateway/api/vlan_results'

class Gateway < Device
  module API
    module Vlan
      include Gateway::API::Utils

      # @return [Gateway::API::Object]
      def vlanconfig
        options = { cmd: :adv_info, section: :vlanconfig }
        perform_get_with_object(default_path, options, Gateway::API::VlanResults)
      end
    end
  end
end
