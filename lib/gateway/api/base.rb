require 'gateway/api/arping'
require 'gateway/api/backup_wan_test'
require 'gateway/api/firmware'
require 'gateway/api/ping'
require 'gateway/api/cx_access'
require 'gateway/api/profile_queries'
require 'gateway/api/reboot'
require 'gateway/api/traces'
require 'gateway/api/tunnels'
require 'gateway/api/vlan'
require 'gateway/api/wan_toggle'

class Gateway < Device
  module API
    # @note All methods have been separated into common modules
    module Base
      include Gateway::API::Arping
      include Gateway::API::BackupWanTest
      include Gateway::API::CxAccess
      include Gateway::API::Firmware
      include Gateway::API::Ping
      include Gateway::API::ProfileQueries
      include Gateway::API::Reboot
      include Gateway::API::Traces
      include Gateway::API::Tunnels
      include Gateway::API::Vlan
      include Gateway::API::WanToggle
    end
  end
end
