require 'gateway/api/utils'
require 'gateway/api/tunnel'

class Gateway < Device
  module API
    module Tunnels
      include Gateway::API::Utils

      MAX_TUNNEL_COUNT = 4

      # @return [Gateway::API::Tunnel]
      def tunnel_up(params = {})
        tunnel_command(:tunnel_up, params)
      end

      # @return [Gateway::API::Tunnel]
      def tunnel_down(params = {})
        tunnel_command(:tunnel_down, params)
      end

      # @return [Gateway::API::Tunnel]
      def tunnel_bounce(params = {})
        tunnel_command(:tunnel_bounce, params)
      end

      # @return [Gateway::API::Tunnel]
      def tunnel_status(params = {})
        tunnel_command(:tunnel_status, params)
      end

      # @return [Array<Gateway::API::Tunnel>]
      def tunnels
        tunnels = []

        MAX_TUNNEL_COUNT.times do |i|
          tunnel = tunnel_status(index: i)
          break unless tunnel.ok?
          tunnels << tunnel
        end

        tunnels.compact
      end

    private

      def tunnel_command(cmd, params = {})
        options = { cmd: cmd }

        if params[:index]
          options[:index] = params[:index]
        elsif params[:name]
          options[:name] = params[:name].upcase
        else
          raise ArgumentError.new('You must provide the :name or :index of the tunnel')
        end

        perform_get_with_object(default_path, options, Gateway::API::Tunnel)
      end
    end
  end
end
