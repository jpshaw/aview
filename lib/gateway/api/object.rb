class Gateway < Device
  module API
    class Object
      attr_reader :cmd, :result

      # Result codes
      OK   = 'ok'.freeze
      FAIL = 'fail'.freeze

      def initialize(attrs = {})
        attrs.each do |key, value|
          instance_variable_set("@#{key}", value)
        end
      end

      # @return [Boolean]
      def ok?
        result == OK
      end

      # @return [Boolean]
      def fail?
        result == FAIL
      end
    end
  end
end
