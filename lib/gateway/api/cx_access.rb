require 'gateway/api/utils'
require 'gateway/api/cx_access_results'

class Gateway < Device
  module API
    module CxAccess
      include Gateway::API::Utils
      
      DEFAULT_V4_SRC = Settings.command_setting_defaults.cx_access.default_v4_src
      DEFAULT_V6_SRC = Settings.command_setting_defaults.cx_access.default_v6_src
      DEFAULT_SRC_ADDR = Settings.command_setting_defaults.cx_access.default_src_addr
      DEFAULT_SRC_INT = Settings.command_setting_defaults.cx_access.default_src_int
      DEFAULT_DST_ADDR = Settings.command_setting_defaults.cx_access.default_dst_addr
      DEFAULT_ACCESS = Settings.command_setting_defaults.cx_access.default_access
      DEFAULT_MAINTENANCE_TUNNEL = Settings.command_setting_defaults.default_maintenance_tunnel
      DEFAULT_DROP_INTERFACE = Settings.command_setting_defaults.default_drop_interface
      ACCESS_STATES = ['up', 'down'].freeze

      def cx_access(params = {})
        raise ArgumentError.new("invalid state given") if params[:access].present? && invalid_access?(params[:access])

        options = {
          cmd: :cx_access,
          v4_src: (params[:v4_src] || DEFAULT_V4_SRC),
          v6_src: (params[:v6_src] || DEFAULT_V6_SRC),
          src_addr: (params[:src_addr] || DEFAULT_SRC_ADDR),
          src_int: (params[:src_int] || DEFAULT_SRC_INT),
          dst_addr: (params[:dst_addr] || DEFAULT_SRC_ADDR),
          access: (params[:access] || DEFAULT_ACCESS),
          maintenance_tunnel: (params[:maintenance_tunnel] || DEFAULT_MAINTENANCE_TUNNEL),
          drop_interface: (params[:drop_interface] || DEFAULT_DROP_INTERFACE)
        }

        perform_get_with_object(default_path, options, Gateway::API::CxAccessResults)
      end

      private

      def invalid_access?(state)
        ACCESS_STATES.exclude?(state)
      end
    end
  end
end
