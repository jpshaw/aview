require 'gateway/api/utils'
require 'gateway/api/ping_results'

class Gateway < Device
  module API
    module Ping
      include Gateway::API::Utils

      DEFAULT_PING_COUNT = 5.freeze
      DEFAULT_PING_SIZE  = 100.freeze

      # @return [Gateway::API::PingResults]
      def ping(params = {})
        raise ArgumentError.new('A :target must be given') unless params[:target]

        command = ipv6?(params) ? :ping6 : :ping

        # Order of the keys must be explicitly set
        options = {
          cmd:     command,
          target:  params[:target],
          count:   (params[:count]   || DEFAULT_PING_COUNT),
          size:    (params[:size]    || DEFAULT_PING_SIZE),
          src_int: (params[:src_int] || default_interface)
        }

        perform_get_with_object(default_path, options, Gateway::API::PingResults)
      end
    end
  end
end
