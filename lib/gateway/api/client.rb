require 'gateway/api/base'

class Gateway < Device
  module API
    class Client
      include Gateway::API::Base

      attr_accessor :host, :user, :password, :use_ssl

      def initialize(options = {})
        options[:use_ssl] ||= true

        options.each do |key, value|
          instance_variable_set("@#{key}", value)
        end

        yield(self) if block_given?
      end

      # @return [String]
      def base_uri
        ipv6_safe_addr = host.to_s.include?(':') ? "[#{host}]" : host
        @base_uri ||= "#{scheme}://#{ipv6_safe_addr}"
      end

      # @return [String]
      def scheme
        @scheme ||= use_ssl ? 'https' : 'http'
      end

      # @return [Hash]
      def headers
        { content_type: 'application/json; charset=utf-8' }
      end

      # @return [Hash]
      def auth
        { user: user, pass: password }
      end

      # @return [Boolean]
      def has_auth?
        auth.values.all?
      end
    end
  end
end
