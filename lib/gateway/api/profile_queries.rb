require 'gateway/api/utils'
require 'gateway/api/profile_query'

class Gateway < Device
  module API
    module ProfileQueries
      include Gateway::API::Utils

      # @return [Gateway::API::ProfileQuery]
      def full_profile_query
        profile_query :full
      end

      # @return [Gateway::API::ProfileQuery]
      def partial_profile_query
        profile_query :partial
      end

      # @return [Gateway::API::ProfileQuery]
      def query_status
        profile_query :status
      end

    private

      def profile_query(type)
        options = { cmd: :query, type: type }
        perform_get_with_object(default_path, options, Gateway::API::ProfileQuery)
      end
    end
  end
end
