class Gateway < Device
  module API
    # Custom error class for rescuing from all Gateway HTTP errors
    class Error < StandardError
      # Raised when the Gateway returns a 4xx HTTP status code
      ClientError = Class.new(self)

      # Raised when the Gateway returns the HTTP status code 400
      BadRequest = Class.new(ClientError)

      # Raised when the Gateway returns the HTTP status code 401
      Unauthorized = Class.new(ClientError)

      # Raised when the Gateway returns the HTTP status code 403
      Forbidden = Class.new(ClientError)

      # Raised when the Gateway returns the HTTP status code 404
      NotFound = Class.new(ClientError)

      # Raised when the Gateway returns the HTTP status code 406
      NotAcceptable = Class.new(ClientError)

      # Raised when the Gateway returns the HTTP status code 422
      UnprocessableEntity = Class.new(ClientError)

      # Raised when the Gateway returns a 5xx HTTP status code
      ServerError = Class.new(self)

      # Raised when the Gateway returns the HTTP status code 500
      InternalServerError = Class.new(ServerError)

      # Raised when the Gateway returns the HTTP status code 502
      BadGateway = Class.new(ServerError)

      # Raised when the Gateway returns the HTTP status code 503
      ServiceUnavailable = Class.new(ServerError)

      # Raised when the Gateway returns the HTTP status code 504
      GatewayTimeout = Class.new(ServerError)

      ERRORS = {
        400 => Gateway::API::Error::BadRequest,
        401 => Gateway::API::Error::Unauthorized,
        403 => Gateway::API::Error::Forbidden,
        404 => Gateway::API::Error::NotFound,
        406 => Gateway::API::Error::NotAcceptable,
        422 => Gateway::API::Error::UnprocessableEntity,
        500 => Gateway::API::Error::InternalServerError,
        502 => Gateway::API::Error::BadGateway,
        503 => Gateway::API::Error::ServiceUnavailable,
        504 => Gateway::API::Error::GatewayTimeout
      }
    end
  end
end
