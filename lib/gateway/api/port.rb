module Gateway::API
  class Port
    attr_accessor :tx, :rx, :ip_addrs, :details
    attr_reader :id

    def initialize(id)
      @id = id
      @ip_addrs = {}
      @tx = 0
      @rx = 0
      @details = []
    end
  end
end
