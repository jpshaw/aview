require 'gateway/api/object'
require 'gateway/api/enumerable'

class Gateway < Device
  module API
    class Results < Gateway::API::Object
      include Gateway::API::Enumerable

      attr_reader :output

      def initialize(attrs = {})
        super(attrs)
        @collection = output || []
      end

      def to_s
        self.each { |line| puts line }
      end

    private

      def find_line_by_regex(regex)
        self.each { |line| return line if line =~ regex }
        nil
      end

      def where_line_matches(regex)
        self.select { |line| line =~ regex }
      end
    end
  end
end
