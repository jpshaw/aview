module Gateway::API
  class PortBuilder
    attr_writer :vlan_ip

    def vlan=(vlan_id)
      @current_vlan = vlan_id
    end

    def port=(port_number)
      @current_port_id = port_number.to_i
    end

    def add_detail(detail)
      current_port.details << detail if current_port.present?
    end

    def ports
      @ports ||= []
    end

    def add_tx(value)
      current_port.tx += value if current_port.present?
    end

    def add_rx(value)
      current_port.rx += value if current_port.present?
    end

    def add_vlan_ip_to_port
      return if @current_vlan.blank? || @vlan_ip.blank?
      current_port.ip_addrs["vlan#{@current_vlan}"] = ip_address
    end

    def remove_vlans_from_port
      current_port.ip_addrs = nil
    end

    def current_vlan
      @current_vlan
    end

    private

    def current_port
      return if @current_port_id.nil?
      ports[@current_port_id - 1] ||= Port.new(@current_port_id)
    end

    def ip_address
      ip = IPAddress(@vlan_ip)
      return "#{ip.address}/#{ip.prefix}"
    rescue ArgumentError
      return @vlan_ip
    end
  end
end
