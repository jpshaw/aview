require 'gateway/api/object'

class Gateway < Device
  module API
    class ProfileQuery < Gateway::API::Object
      attr_reader :last_query, :type, :next_query

      def partial?
        type == 'partial'
      end

      def full?
        type == 'full'
      end

      def last_queried_at
        return if last_query.nil?
        Time.strptime(last_query, '%m/%d/%Y %H:%M:%S')
      end

      def next_query_at
        return if next_query.nil?
        Time.strptime(next_query, '%m/%d/%Y %H:%M:%S')
      end
    end
  end
end
