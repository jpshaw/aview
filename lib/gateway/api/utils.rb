require 'ipaddr'
require 'gateway/api/request'

class Gateway < Device
  module API
    module Utils
      DEFAULT_PATH   = '/cgi-bin/armt'
      DEFAULT_IFACE  = 'Default'
      IPv6_ON        = '1'
      HOSTNAME_REGEX = /[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix

      def default_path
        @default_path ||= DEFAULT_PATH
      end

      def default_iface
        @default_iface ||= DEFAULT_IFACE
      end
      alias_method :default_interface, :default_iface

    private

      # If given a hostname, use checkbox to determine if to use ipv6. Otherwise
      # check the given ip address.
      def ipv6?(params)

        if params[:target] =~ HOSTNAME_REGEX
          return params[:ipv6] == IPv6_ON

        else # We have an ip address
          return IPAddr.new(params[:target]).ipv6?
        end
      end

      def perform_get(path, options = {})
        perform_request(:get, path, options)
      end

      def perform_post(path, options = {})
        perform_request(:post, path, options)
      end

      def perform_get_with_object(path, options, klass)
        perform_request_with_object(:get, path, options, klass)
      end

      def perform_post_with_object(path, options, klass)
        perform_request_with_object(:post, path, options, klass)
      end

      def perform_request_with_object(request_method, path, options, klass)
        response = perform_request(request_method, path, options)
        klass.new(response)
      end

      def perform_request(request_method, path, options = {})
        Gateway::API::Request.new(self, request_method, path, options).perform
      end
    end
  end
end
