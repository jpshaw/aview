require 'gateway/api/results'
require 'gateway/api/packet_stats'
require 'gateway/api/round_trip_stats'

class Gateway < Device
  module API
    class PingResults < Gateway::API::Results
      REGEX           = /icmp_seq=\d/
      TARGET_REGEX    = /starting test to/i
      COMPLETED_REGEX = /test complete/i

      def target
        @target ||= target_line.gsub(TARGET_REGEX, '').strip
      end

      def pings
        @pings ||= where_line_matches(REGEX)
      end

      def packets
        @packets ||= Gateway::API::PacketStats.parse(packet_line)
      end

      def round_trip
        @round_trip ||= begin
          Gateway::API::RoundTripStats.parse(round_trip_line)
        rescue ArgumentError
          nil
        end
      end

      def target_reached?
        packets.received > 0
      end

      def test_completed?
        @test_completed ||= where_line_matches(COMPLETED_REGEX).any?
      end

    private

      def target_line
        @target_line ||= find_line_by_regex(TARGET_REGEX)
      end

      def packet_line
        @packet_line ||= find_line_by_regex(Gateway::API::PacketStats::REGEX)
      end

      def round_trip_line
        @round_trip_line ||= find_line_by_regex(Gateway::API::RoundTripStats::REGEX)
      end
    end
  end
end
