require 'gateway/api/results'
require 'gateway/api/packet_stats'
require 'gateway/api/round_trip_stats'

class Gateway < Device
  module API
    class WanToggleResults < Gateway::API::Results
      attr_reader :status

      def output
        @output ||= [ state: @sub_cmd, time: @time, status: @status ]
      end
    end
  end
end
