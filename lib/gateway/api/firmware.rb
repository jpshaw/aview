require 'gateway/api/utils'
require 'gateway/api/upgrade'
require 'gateway/api/downgrade'

class Gateway < Device
  module API
    module Firmware
      include Gateway::API::Utils

      # Attempts to perform a firmware upgrade for the device.
      # @return [Gateway::API::Upgrade]
      def upgrade
        options = { cmd: :upgrade }
        perform_get_with_object(default_path, options, Gateway::API::Upgrade)
      end

      # Checks if there is a firmware upgrade available for the device.
      # @return [Gateway::API::Upgrade]
      def upgrade_check
        options = { cmd: :upgrade_check }
        perform_get_with_object(default_path, options, Gateway::API::Upgrade)
      end

      # Attempts to rollback the firmware for the device.
      # @return [Gateway::API::Downgrade]
      def rollback
        options = { cmd: :rollback }
        perform_get_with_object(default_path, options, Gateway::API::Downgrade)
      end

      # Checks the firmware rollback version for the device.
      # @return [Gateway::API::Downgrade]
      def rollback_check
        options = { cmd: :rollback_check }
        perform_get_with_object(default_path, options, Gateway::API::Downgrade)
      end
    end
  end
end
