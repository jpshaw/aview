require 'gateway/api/utils'
require 'gateway/api/object'

class Gateway < Device
  module API
    module BackupWanTest
      include Gateway::API::Utils

      def backup_wan_test
        options = { cmd: :secondary_test }
        perform_get_with_object(default_path, options, Gateway::API::Object)
      end
    end
  end
end
