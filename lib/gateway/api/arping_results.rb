require 'gateway/api/results'

class Gateway < Device
  module API
    class ArpingResults < Gateway::API::Results
      FAIL_REGEX = /arpping error/i
      TARGET_REGEX = /starting test to/i
      COMPLETED_REGEX = /test complete/i
      INTERFACE_REGEX = /using interface/i
      INTERFACE_END_REGEX = /for the arp.*/i
      INTERFACE_FAIL_REGEX = /no such device/i

      def interface
        return @interface if @interface

        line = find_line_by_regex(INTERFACE_REGEX)
        line.gsub!(INTERFACE_REGEX, '')
        line.gsub!(INTERFACE_END_REGEX, '')

        @interface = line.strip.downcase
        @interface
      end

      def target
        @target ||= target_line.gsub(TARGET_REGEX, '').strip
      end
      alias_method :destination, :target

      def arp_failed?
        @arp_failed ||= where_line_matches(FAIL_REGEX).any?
      end

      def test_completed?
        @test_completed ||= where_line_matches(COMPLETED_REGEX).any?
      end

    private

      def target_line
        @target_line ||= find_line_by_regex(TARGET_REGEX)
      end
    end
  end
end
