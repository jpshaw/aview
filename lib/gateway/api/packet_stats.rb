require 'gateway/api/object'

class Gateway < Device
  module API
    class PacketStats < Gateway::API::Object
      REGEX = /\d+ packet/

      attr_accessor :tx, :rx, :loss, :time
      alias_method :transmitted, :tx
      alias_method :received, :rx

      # "3 packets transmitted, 3 received, 0% packet loss, time 2007ms"
      def self.parse(result)
        unless result && result.match(REGEX)
          raise ArgumentError.new('A valid packet result must be given')
        end

        transmitted, received, loss, time = result.split(',').map(&:strip)

        transmitted = transmitted.to_i
        received    = received.to_i
        loss        = "#{loss.to_i}%"
        time        = time.gsub('time ', '')

        new(tx: transmitted, rx: received, loss: loss, time: time)
      end
    end
  end
end
