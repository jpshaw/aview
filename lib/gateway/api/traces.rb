require 'gateway/api/utils'
require 'gateway/api/trace_results'

class Gateway < Device
  module API
    module Traces
      include Gateway::API::Utils

      # @return [Gateway::API::TraceResults]
      def traceroute(params = {})
        raise ArgumentError.new('A :target must be given') unless params[:target]

        command = ipv6?(params) ? :traceroute6 : :traceroute

        options = {
          cmd:     command,
          target:  params[:target],
          src_int: (params[:src_int] || default_interface)
        }

        perform_get_with_object(default_path, options, Gateway::API::TraceResults)
      end

      # @return [Gateway::API::Results]
      def tracepath(params = {})
        raise ArgumentError.new('A :target must be given') unless params[:target]

        command = ipv6?(params) ? :tracepath6 : :tracepath

        options = {
          cmd:     command,
          target:  params[:target],
          src_int: (params[:src_int] || default_interface)
        }

        perform_get_with_object(default_path, options, Gateway::API::TraceResults)
      end
    end
  end
end
