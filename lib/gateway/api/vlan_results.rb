class Gateway < Device
  module API
    class VlanResults < Gateway::API::Results
      VLAN_HEADER_REGEX = /# vlan id (.*) includes/i
      VLAN_ADDRESS_REGEX = /Addr (.*) Mask .*/i
      VLAN_SUBNET_REGEX = /Addr .* Mask (.*)/i
      VLAN_PORT_UP_REGEX = /Link speed (.*)/i
      VLAN_PORT_DOWN_REGEX = /no link/i
      VLAN_PORT_REGEX = /Port # (.*)/i

      PORT_DISABLED_REGEX = /Disabled ports include/i
      PORT_DISABLED = 'disabled'

      CONNECTED = 'Connected'
      NOT_CONNECTED = 'Not Connected'
      UNKNOWN = 'Unknown'
      NO_LINK = 'No Link'

      def save_lan_networks(device)
        parsed_record.ports.each do |port|
          network = device.networks.lan.where(network_iface: "lan#{port.id}").first_or_initialize
          network.ip_addrs = port.ip_addrs
          network.tx = port.tx
          network.rx = port.rx
          network.status = status(port.details)
          network.speed  = speed(port.details)
          network.save if network && network.changed?
        end
      end

    private

      def record
        @record ||= self.to_a
      end

      def parsed_record
        port_builder = API::PortBuilder.new

        record.each_with_index do |line, i|
          if line =~ VLAN_HEADER_REGEX
            port_builder.vlan = line[VLAN_HEADER_REGEX, 1]
            port_builder.vlan_ip = ''
          elsif line =~ VLAN_ADDRESS_REGEX
            port_builder.vlan_ip = "#{line[VLAN_ADDRESS_REGEX, 1]}/#{line[VLAN_SUBNET_REGEX, 1]}"
          elsif line =~ PORT_DISABLED_REGEX
            port_builder.vlan = PORT_DISABLED
          elsif line =~ VLAN_PORT_REGEX
            port_builder.port = line[VLAN_PORT_REGEX, 1]
            if port_builder.current_vlan == PORT_DISABLED
              port_builder.remove_vlans_from_port
              port_builder.add_detail(PORT_DISABLED)
            else
              port_builder.add_vlan_ip_to_port
            end
          elsif line.include?('TX bytes:')
            port_builder.add_tx(line.split.last.to_i)
          elsif line.include?('RX bytes:')
            port_builder.add_rx(line.split.last.to_i)
          else
            port_builder.add_detail(line.lstrip)
          end
        end

        port_builder
      end

      def status(port_details)
        port_details.each do |detail|
          return PORT_DISABLED if detail == PORT_DISABLED
          return CONNECTED if detail =~ VLAN_PORT_UP_REGEX
          return NOT_CONNECTED if detail =~ VLAN_PORT_DOWN_REGEX
        end
        UNKNOWN
      end

      def speed(port_details)
        port_details.each_with_index do |detail, i|
          return port_details[i + 1][VLAN_PORT_UP_REGEX, 1] if detail =~ VLAN_PORT_UP_REGEX
        end
        NO_LINK
      end
    end
  end
end
