#!/usr/bin/ruby

require 'savon'

licenseKey = ENV['JASPER_API_KEY']
base_uri   = ENV['JASPER_API_URI']
wsdlUrl    = "#{base_uri}/ws/schema/Echo.wsdl"
message    = 'Hello World'

client = Savon.client(wsdl: wsdlUrl, log:true) # Change to log:false to turn off XML logging

begin
  response = client.call(:echo, message: { messageId:  '',
                                           version:    '',
                                           licenseKey: licenseKey,
                                           value:      message
                                         })

  puts "\nResult (#{response.http.code}): #{response.body}"
rescue Savon::Error => e
  puts "Error: #{e.to_hash}"
end
