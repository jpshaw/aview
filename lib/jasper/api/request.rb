require 'savon'

module Jasper
  module API
    class Request
      attr_accessor :request_method, :wsdlpath, :message_options

      JASPER_API_VERSION = '4'
      HTTP_SUCCESS = 200

      def initialize(client, request_method,  wsdlpath, message_options = {})
        @auth            = client.auth
        @api_key         = client.api_key
        @wsdlUrl         = "#{client.base_uri}#{wsdlpath}"
        @request_method  = request_method
        @message_options = default_message_options.merge(message_options)
      end

      def deliver
        soap_client = Savon.client(wsdl: @wsdlUrl, wsse_auth: @auth)
        response    = soap_client.call(@request_method, message: @message_options)

        fail_or_return_response_body(response.http.code, response.body)
      rescue Savon::Error => e
        e.to_hash
      end

    private

      # NOTE: the order of these options is important! Namely, the messageId,
      #       version, and licenseKey must be the first three key/values listed.
      #       We can override any of these options with the options passed to
      #       the Jasper::API::Request class if desired.
      def default_message_options
        { messageId: '', version: JASPER_API_VERSION, licenseKey: @api_key }
      end

      def fail_or_return_response_body(code, body)
        fail(code) unless code == HTTP_SUCCESS
        body
      end

    end
  end
end
