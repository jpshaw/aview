require 'jasper/api/utils'

module Jasper
  module API
    module Echo
      include Jasper::API::Utils

      # @return Hash
      def test
        wsdlpath = '/ws/schema/Echo.wsdl'
        message_options = { value: 'Hello World' }
        soap_request(:echo, wsdlpath, message_options)
      end
    end
  end
end
