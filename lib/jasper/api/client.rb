require 'jasper/api/base'

module Jasper
  module API
    class Client
      include Jasper::API::Base

      attr_accessor :api_key, :user, :pass, :base_uri

      def initialize(options = {})
        #instance_variable_set("@#{api_key}" , options[:key]  || Settings.api_keys.jasper.key
        @api_key  = options[:key]  || Settings.api_keys.jasper.key
        @user     = options[:user] || Settings.api_keys.jasper.user
        @pass     = options[:pass] || Settings.api_keys.jasper.pass
        @base_uri = options[:base_uri] || Settings.api_keys.jasper.base_uri

        yield(self) if block_given?
      end

      # @return [Array]
      def auth
        [ user, pass ]
      end
    end
  end
end
