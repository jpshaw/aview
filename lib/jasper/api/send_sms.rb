require 'jasper/api/utils'

module Jasper
  module API
    module SendSms
      include Jasper::API::Utils

      # @return Hash
      def send_sms(iccid, message)
        wsdlpath = '/ws/schema/Sms.wsdl'
        message_options = { messageId: iccid, sentToIccid: iccid, messageText: message }
        soap_request(:send_sms, wsdlpath, message_options)
      end
    end
  end
end
