require 'jasper/api/request'

module Jasper
  module API
    module Utils

    private

      def soap_request(request_method, wsdlpath, message_options)
        Jasper::API::Request.new(self, request_method, wsdlpath, message_options).deliver
      end

    end
  end
end
