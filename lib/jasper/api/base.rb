require 'jasper/api/echo'
require 'jasper/api/send_sms'

module Jasper
  module API
    # @note All methods have been separated into common modules
    module Base
      include Jasper::API::Echo
      include Jasper::API::SendSms
    end
  end
end
