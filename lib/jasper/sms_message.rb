#!/usr/bin/ruby

require 'savon'

licenseKey = ENV['JASPER_API_KEY' ]
userName   = ENV['JASPER_API_USER']
password   = ENV['JASPER_API_PASS']
base_uri   = ENV['JASPER_API_URI' ]
wsdlUrl    = "#{base_uri}/ws/schema/Sms.wsdl"
iccid      = '89011704252309057948' # sample ICCID on the Jasper network
message    = 'accns%U2FsdGVkX18Px616F0dUp4yDXHainzb8QOgH9Hv1Oz8=%accns' # sample reboot message

client = Savon.client(wsdl: wsdlUrl, wsse_auth: [userName, password])

begin
  response = client.call(:send_sms, message: { messageId: iccid,
                                               version:     '4',
                                               licenseKey:  licenseKey,
                                               sentToIccid: iccid,
                                               messageText: message})

  puts "\nResult (#{response.http.code}): #{response.body}"
rescue Savon::Error => e
  puts "Error: #{e.to_hash}"
end
