class DeviceConfigurationCompiler

  def initialize
    @failed_configuration_count = 0
    @successful_configuration_count = 0
  end

  # this assumes that the new settings are already set on configuration.settings and any locks
  # are in place
  def compile_device_configuration(configuration)
    # This is not actually needed in practice today, it is done on the front end. Consider removal
    if configuration.parent.present?
      compile(configuration)
    else
      configuration.compiled_settings = configuration.settings
    end
  end

  def compile_children(configuration)
    configuration.children.each do |child_configuration|
      compile(child_configuration)
      child_configuration.save
      compile_children(child_configuration)
    end
  end

  def errors
    @errors ||= []
  end

  def child_compile_results
    "Could not compile #{@failed_configuration_count} #{'device_configuration'.pluralize(@failed_configuration_count)}. "\
    "Successfully compiled #{@successful_configuration_count} #{'device_configuration'.pluralize(@successful_configuration_count)}."
  end

  def self.js_context
    @js_context ||= begin
      ExecJS.compile(
        Rails.application.assets_manifest.find_sources('config_compiler.js').first
      )
    end
  end

  private

  def compile(configuration)
    merge_grace_period_duration(configuration)
    merge_firmware(configuration)
    merge_settings(configuration)
  end

  def merge_grace_period_duration(configuration)
    return if configuration.grace_period_duration_locked
    configuration.grace_period_duration = configuration.parent.grace_period_duration
  end

  def merge_firmware(configuration)
    return if configuration.device_firmware_locked
    configuration.device_firmware = configuration.parent.device_firmware
  end

  def merge_settings(configuration)
    if firmware_cannot_be_migrated(configuration)
      merge_settings_failure(configuration, [{ configuration_name: configuration.name,
                                               message: 'firmware cannot be migrated' }])
      return
    end

    compiled_settings = compile_settings(configuration)
    validation_errors = configuration.validate_against_firmware_schema(compiled_settings)
    if validation_errors.any?
      merge_settings_failure(configuration, validation_errors)
    else
      merge_settings_success(configuration, compiled_settings)
    end
  end

  def firmware_cannot_be_migrated(configuration)
    compatible_firmware = configuration.parent.device_firmware.all_compatible_firmwares
    compatible_firmware.exclude?(configuration.device_firmware) &&
      configuration.device_firmware_locked
  end

  def compile_settings(configuration)
    parent_settings = configuration.parent.parsed_compiled_settings
    schema = configuration.device_firmware.schema
    self.class.js_context.call('compileConfigurationSettings',
                               configuration.parsed_settings,
                               parent_settings,
                               schema,
                               configuration.device_firmware.version)
  end

  def merge_settings_failure(configuration, validation_errors)
    @failed_configuration_count += 1
    add_to_errors(validation_errors, configuration)
    configuration.parent_disconnect = true
  end

  def add_to_errors(validation_errors, configuration)
    validation_errors.map { |error_hash| error_hash[:configuration_name] = configuration.name }
    errors.concat(validation_errors)
  end

  def merge_settings_success(configuration, compiled_settings)
    @successful_configuration_count += 1
    configuration.parent_disconnect = false
    configuration.compiled_settings = compiled_settings
  end
end