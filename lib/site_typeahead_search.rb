class SiteTypeaheadSearch
  LIMIT = 20

  def initialize(user, query, device_id, scope = :user)
    @user = user
    @query = query
    @scope = scope
    @organization_id = Device.find_by(mac: device_id).organization_id
  end

  def execute
    user.sites.select(:id, :name).where('sites.name like ? and organization_id = ?', "%#{@query}%", "#{@organization_id}").limit(LIMIT)
  end

  private

  attr_reader :user, :scope, :query
end
