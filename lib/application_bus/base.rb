module ApplicationBus
  class Base
    include ::Kraken::Logger.file

    class << self
      attr_accessor :topics

      def call(*args)
        new.call(*args)
      end

      def topic(name)
        self.topics ||= []
        self.topics << name
      end
    end

    delegate :topics, to: :class

    def call
      raise NotImplementedError
    end
  end
end
