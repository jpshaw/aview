class ApplicationProducer < ApplicationBus::Base

  def publish(data)
    serialized_data = serialize(data)

    topics.each do |topic|
      publish_to_topic(serialized_data, topic)
    end
  rescue => error
    logger.error error
  end

  private

  def publish_to_topic(data, topic)
    ApplicationBus.producer.produce(data, topic: topic)
  rescue Kafka::BufferOverflow
    logger.warn 'Message bus buffer overflow'
    sleep 1
    retry
  end

  def serialize(data)
    case data
    when String
      data
    when Hash, Array
      data.to_json
    else
      data.to_s
    end
  end

end
