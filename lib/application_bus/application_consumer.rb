class ApplicationConsumer < ApplicationBus::Base

  class << self
    def start
      @thread ||= Thread.new do
        begin
          self.new.consume
        rescue => error
          logger.error error
        end
      end
    end

    def stop
      @consumer.stop if @consumer
      @thread.join if @thread
      @thread = nil
    end

    def default_name
      @default_name ||= self.name.underscore.dasherize
    end

    def group(name)
      @group = "#{group_prefix}-#{name}"
    end

    def group_id
      @group ||= "#{group_prefix}-#{default_name}"
    end

    # So we can distinguish torquebox consumers
    def group_prefix
      :torquebox
    end

    # Allow each sublcass to have its own consumer
    def consumer
      @consumer ||= begin
        consumer = ApplicationBus.client.consumer(group_id: group_id)
        consumer.subscribe(topics.first)
        # TODO: Add offset & partition support
        #consumer.seek(topic, 0, offset) if offset
        ApplicationBus.consumers << consumer
        consumer
      end
    end
  end

  delegate :consumer, to: :class

  def consume
    raise NotImplementedError
  end

  def each_message(&block)
    consumer.each_message do |message|
      begin
        logger.debug { message.value }

        hash = JSON.parse(message.value)
        mash = Hashie::Mash.new(hash)

        yield(mash) if block_given?

      rescue TypeError, JSON::ParserError => ex
        logger.error ex
      rescue => ex
        logger.error message.value # Print offending message
        logger.error "Exception raised when processing " \
                     "#{message.topic}/#{message.partition} " \
                     "at offset #{message.offset}:"
        logger.error ex
      end
    end
  end

  def each_batch(max_wait_time: 1, &block)
    consumer.each_batch(max_wait_time: max_wait_time) do |batch|
      yield(batch)
    end
  end

end
