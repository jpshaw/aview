module DeviceEvents::Util
  def random_mac_address
    hex_random(6).upcase
  end

  def random_interface
    i = (rand(3) + 1)
    ["eth", "ppp"].sample + "#{i}"
  end

  def random_ip_address
    Array.new(4){rand(254) + 1}.join('.')
  end

  def random_ipv6_address
    hex_random(16)
  end

  def random_connection_id
    Array.new(20) { rand(9) + 1 }.join
  end

  def hex_random(length, slice = 2)
    SecureRandom.hex(length).chars.each_slice(slice).map(&:join).join(':')
  end
end
