class OrganizationTypeaheadSearch
  LIMIT = 100

  def initialize(user, query, scope = :all)
    @user = user
    @query = query
    @scope = scope
  end

  def execute
    search =
      if scope == :all
        user.all_organizations
      else
        user.organizations
      end

    search.select(:id, :name).where('organizations.name like ?', "%#{query}%").limit(LIMIT)
  end

  private

  attr_reader :user, :scope, :query
end
