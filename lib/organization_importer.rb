require 'csv'
require 'organization'

class OrganizationImporter
  include ::Kraken::Logger.file

  attr_accessor :file_path, :results

  def self.import_file(file_path)
    importer = new file_path
    importer.process!
    importer.results
  end

  def initialize(file_path)
    @file_path = file_path
    @results   = {
      added:   [],
      scanned: []
    }
  end

  def process!
    CSV.foreach(file_path, headers: true, encoding: 'ISO-8859-1') do |row|
      begin
        ActiveRecord::Base.transaction do
          process_row(row)
        end
      rescue => e
        logger.error "Error importing organization #{row}\n" \
                     "#{e.message}\n #{e.backtrace}"
      end
    end
  end

  protected

  def process_row(row)
    organization_name        = row['name']
    parent_organization_name = row['parent_name']

    organization = get_organization_for_name(organization_name)

    if organization.blank?
      logger.error "Could not find organization '#{organization_name}'."
      return
    end

    parent_organization = get_organization_for_name(parent_organization_name)

    if parent_organization.present?
      add_child organization, parent_organization
    else
      add_child organization, root_org
    end
  end

  def root_org
    @root_org ||= Organization.roots.first
  end

  def add_child(child, parent)
    return if child.blank? || parent.blank?
    unless parent.children.include?(child) || parent == child
      parent.add_child(child)
    end
  end

  def get_organization_for_name(name)
    return if name.blank?

    organization   = Organization.find_by(import_name: name)
    organization ||= Organization.find_by(name: name)

    if organization.blank?
      params = { name: name, import_name: name }
      result = OrganizationCreator.call(params: params)

      if result.success?
        organization = result.organization
      else
        logger.error "Error creating organization with params: #{params}\n" \
                     "Errors: #{result.organization.errors.full_messages}"
      end
    else
      if organization.import_name.blank?
        organization.update_attributes(import_name: name)
      end
    end

    organization
  end
end
