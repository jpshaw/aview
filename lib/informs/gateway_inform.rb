require 'snmp_inform'

class GatewayInform < SnmpInform
  extend DeviceEvents::Util

  # ngevIpConntrackTable - Sent if the Firewall Connection table reaches the
  # specified threshold.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngevIpConntrackCurrentPercentage(Integer32) - Current ip table percentage of max
  #   ngevIpConntrackMaxPercentage(Integer32) - Max percentage before alert is cut
  #
  def self.ip_conn_track_table(opts = {})
    opts[:mac]                 ||= random_mac_address
    opts[:current_percentage]  ||= rand(100) + 1
    opts[:max_percentage]      ||= rand(100) + 1

    inform = self.new 'ngevIpConntrackTable'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevIpConntrackCurrentPercentage', opts[:current_percentage], :int_32
    inform.add 'ngevIpConntrackMaxPercentage', opts[:max_percentage], :int_32
    inform
  end

  # ngevIpConntrackTableFull - Indicates the firewall Connection table has
  # reached capacity.
  #
  # Params:
  #   ngevMac(DisplayString)
  #
  def self.firewall_full(opts = {})
    opts[:mac] ||= random_mac_address

    inform = self.new 'ngevIpConntrackTableFull'
    inform.add 'ngevMac', opts[:mac], :string
    inform
  end

  # ngevExcessiveDial - The device has been in dial backup longer than the allowed threshold.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngevTimeInDial(Integer32) - Number of minutes the device has been connected
  #   ngifTimeInDial(Timeticks) - How long the current dial connection has been up
  #   ngevMaxTimeInDialUntilAlert(Timeticks) - Number of minutes connected via dial before an alert is sent
  #   ngifActiveWanIface(DisplayString) - Primary connection interface
  #
  def self.excessive_dial(opts = {})
    opts[:mac]          ||= random_mac_address
    opts[:interface]    ||= random_interface
    opts[:time_in_dial] ||= rand(3600) + 100
    opts[:dial_up]      ||= opts[:time_in_dial] * 60 * 100
    opts[:max_time]     ||= rand(360000) + 10000

    inform = self.new 'ngevExcessiveDial'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevTimeInDial', opts[:time_in_dial], :int_32
    inform.add 'ngifTimeInDial', opts[:dial_up], :timeticks
    inform.add 'ngevMaxTimeInDialUntilAlert', opts[:max_time], :timeticks
    inform.add 'ngifActiveWanIface', opts[:interface], :string
    inform
  end

  # ngevActiveWanIpChanged - The IP address associated with the active WAN
  # interface has changed. For example, dial backup was initiated because WAN
  # connectivity was lost.  Another example is that a PPPoE connection changed
  # its IP address.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngifActiveWanIface(DisplayString) - Primary connection interface
  #   ngevActiveWanIp(IpAddress) - Address of the active wan interface
  #   ngevIPv6ActiveWanIp(InetAddressIPv6) - Address of the active ipv6 interface (OCTET STRING (SIZE (16)))
  #
  def self.active_wan_ip_changed(opts = {})
    opts[:mac]          ||= random_mac_address
    opts[:interface]    ||= random_interface
    opts[:ip_address]   ||= random_ip_address
    opts[:ipv6_address] ||= '0:0:0:0:0:0:0:0'

    inform = self.new 'ngevActiveWanIpChanged'

    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngifActiveWanIface', opts[:interface], :string
    inform.add 'ngevActiveWanIp', opts[:ip_address], :ip_address
    inform.add 'ngevIPv6ActiveWanIp', opts[:ipv6_address], :ipv6_address
    inform
  end

  # IPv6 active wan change
  def self.active_wan_ipv6_changed(opts = {})
    opts[:ip_address]     = '0.0.0.0'
    opts[:ipv6_address] ||= random_ipv6_address

    active_wan_ip_changed(opts)
  end

  # ngevRogueMacDetected - The unauthorized MAC detection feature is enabled
  # and an unauthorized MAC address was detected.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngevRogueMac(DisplayString) - MAC address of the unauthorized machine
  #   ngevRogueIp(IpAddress) - IP Address of the unauthorized machine
  #   ngevRogueInterface(DisplayString) - Interface the unauthorized access was detected on
  #   ngevTrapDescription(DisplayString) - SMX description field
  #   ngevIPv6RogueIp(InetAddressIPv6) - The IPv6 IP address of the unauthorized machine detected when MAC monitoring is enabled. Set to zero if not used.
  #
  def self.rogue_mac_detected(opts = {})
    opts[:mac]             ||= random_mac_address
    opts[:rogue_mac]       ||= random_mac_address
    opts[:rogue_ip]        ||= random_ip_address
    opts[:rogue_ipv6]      ||= '0:0:0:0:0:0:0:0'
    opts[:rogue_interface] ||= random_interface
    opts[:trap_desc]       ||= 'This is a test'

    inform = self.new 'ngevRogueMacDetected'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevRogueMac', opts[:rogue_mac], :string
    inform.add 'ngevRogueIp', opts[:rogue_ip], :ip_address
    inform.add 'ngevRogueInterface', opts[:rogue_interface], :string
    inform.add 'ngevTrapDescription', opts[:trap_desc], :string
    inform.add 'ngevIPv6RogueIp', opts[:rogue_ipv6], :ipv6_address
    inform
  end

  def self.rogue_mac_detected_ipv6(opts = {})
    opts[:rogue_ip]     = '0.0.0.0'
    opts[:rogue_ipv6] ||= random_ipv6_address

    rogue_mac_detected(opts)
  end

  # ngevRogueMacNoFile - The unauthorized MAC detection feature is enabled and the authorized MAC file doesn't exist.
  #
  # Params:
  #   ngevMac(DisplayString)
  #
  def self.rogue_mac_no_file(opts = {})
    opts[:mac] ||= random_mac_address
    inform = self.new 'ngevRogueMacNoFile'
    inform.add 'ngevMac', opts[:mac], :string
    inform
  end

  # ngevDeviceDiscoOrNotFound - Sent if the AT&T NetGate profile is not found.
  # If the AT&T NetGate goes too long without finding its device profile then
  # the device will switch its status to discontinued, which will also trigger
  # a trap/inform.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   ngevQueryProblem(Integer32) - Specifies what type of problem there was with the query
  #     profileNotFound(0)
  #     discontinuingTheDevice(1)
  #     bandwNoResults(2)
  #
  def self.disco_or_not_found(opts = {})
    opts[:mac]     ||= random_mac_address
    opts[:problem] ||= Gateway::DISCONTINUED_DEVICE

    inform = self.new 'ngevDeviceDiscoOrNotFound'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevQueryProblem', opts[:problem], :int_32
    inform
  end

  def self.profile_not_found(opts = {})
    opts[:problem] = Gateway::PROFILE_NOT_FOUND
    disco_or_not_found(opts)
  end

  def self.no_bandwidth_results(opts = {})
    opts[:problem] = Gateway::BANDWIDTH_NO_RESULTS
    disco_or_not_found(opts)
  end

  # ngevTunnelUp - Information about the results of a tunnel connection attempt.
  #
  # OBJECTS {
  #   ngevMac(DisplayString)
  #   ngevTunConnectionId(DisplayString) - ID used to tie the disconnect together
  #   ngevTunnelDateAndTime(DateAndTime) - Timestamp from when the tunnel came up
  #   ngtuTunMode(Integer32) - Tunnel control mode
  #       tunnelUnknown(0),
  #       tunnelUserControl(1),
  #       tunnelUserViewOnly(2),
  #       tunnelHidden(3),
  #       tunnelMaintenance(4),
  #       tunnelWSSInternet(5)
  #
  #   ngevConnectionType(Integer32) - Connection type: Broadband, dial, etc.
  #       connTypeBroadband(0),
  #       connTypeAgnsDial(1),
  #       connTypeOtherIspDial(2)
  #
  #   ngevAccount(DisplayString) - Customer account used to start the tunnel
  #   ngevUserid(DisplayString) - Customer userid used to start the tunnel
  #   ngtuEndpoint(IpAddress) - IP address of the endpoint the tunnel is connected to
  #   ngtuAuthType(Integer32) - Authentication server used by tunnel
  #       internetOnly(0),
  #       serviceManager(1),
  #       customerDirect(2),
  #       branchOffice(3),
  #       customerDirectSm(4),
  #       noTunnelYet(999)
  #
  #   ngtuAuthProt(Integer32) - Authentication protocol
  #       none(0),
  #       custRadius(1),
  #       custSecureID(2),
  #       custAxent(3),
  #       custCert(4),
  #       custLDAP(5),
  #       custInternal(6),
  #       branchSharedSecret(7),
  #       agnsManaged(8)
  #
  #   ngtuEndpointType(Integer32) - Type of endpoint the tunnel is connected to
  #       none(0),
  #       nortel(3),
  #       linuxFrameSig(4),
  #       gig(5),
  #       linuxSig(6),
  #       cisco(9),
  #       vig(20)
  #
  #   ngevTunConReason(DisplayString) - Results of the tunnel connection attempt
  #   nghwWanConnectionMethod(Integer32) - Current WAN connection setting - Not from SMX
  #       wanConfigUnknown(0),
  #       wanConfigStatic (1),
  #       wanConfigDHCP (2),
  #       wanConfigPPPoE (3),
  #       wanConfigDialPri(4),
  #       wanConfigISDNPri(5),
  #       wanConfigPPPoA (6),
  #       wanConfig3GPer (7),
  #       wanConfig3GPri (8)
  #
  #   ngtuTunInitiator(Integer32) - What initiated the tunnel
  #       attUnknown(0),
  #       attFromUser(1),
  #       attAutoLogon(2),
  #       attPersistentLogon(3),
  #       attVpnctl(4)
  #
  #   ngtuIpsecIface(DisplayString) - IPSec interface for the tunnel
  #   ngtuIPv6Endpoint(InetAddressIPv6) - IPv6 address of the endpoint the tunnel is connected to
  #   ngevVPNServiceIPv4Address(IpAddress) - VPN service IPv4 address assigned to the tunnel connection
  #   ngevVPNServiceIPv6Address(InetAddressIPv6) - VPN service IPv6 address assigned to the tunnel connection
  #   ngevTunConReasonNUM(Integer32) - Numeric value that indicates the result of the tunnel connection attempt
  #
  def self.tunnel_up(opts = {})
    opts[:mac]           ||= random_mac_address
    opts[:connection_id] ||= random_connection_id
    opts[:datetime]      ||= java.util.GregorianCalendar.new
    opts[:tunnel_mode]   ||= GatewayTunnel::MODES.keys.sample
    opts[:conn_type]     ||= Gateway::CONNECTION_TYPES.keys.sample
    opts[:account]       ||= 'ATTDEMO'
    opts[:user]          ||= 'ACCEL6'
    opts[:endpoint]      ||= random_ip_address
    opts[:endpoint_ipv6] ||= '0:0:0:0:0:0:0:0'
    opts[:auth_type]     ||= GatewayTunnel::AUTH_TYPES.keys.sample
    opts[:auth_protocol] ||= GatewayTunnel::AUTH_PROTOCOLS.keys.sample
    opts[:endpoint_type] ||= GatewayTunnel::ENDPOINT_TYPES.keys.sample
    opts[:reason]        ||= 'Successful Logon'
    opts[:reason_num]    ||= 0
    opts[:wan_conn]      ||= Gateway::WAN_CONN_METHODS.keys.sample
    opts[:initiator]     ||= GatewayTunnel::INITIATORS.keys.sample
    opts[:interface]     ||= random_interface
    opts[:service_ip]    ||= random_ip_address
    opts[:service_ipv6]  ||= '0:0:0:0:0:0:0:0'

    inform = self.new 'ngevTunnelUp'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevTunConnectionId', opts[:connection_id], :string
    inform.add 'ngevTunnelDateAndTime', opts[:datetime], :date_and_time
    inform.add 'ngtuTunMode', opts[:tunnel_mode], :int_32
    inform.add 'ngevConnectionType', opts[:conn_type], :int_32
    inform.add 'ngevAccount', opts[:account], :string
    inform.add 'ngevUserid', opts[:user], :string
    inform.add 'ngtuEndpoint', opts[:endpoint], :ip_address
    inform.add 'ngtuIPv6Endpoint', opts[:endpoint_ipv6], :ipv6_address
    inform.add 'ngtuAuthType', opts[:auth_type], :int_32
    inform.add 'ngtuAuthProt', opts[:auth_protocol], :int_32
    inform.add 'ngtuEndpointType', opts[:endpoint_type], :int_32
    inform.add 'ngevTunConReason', opts[:reason], :string
    inform.add 'ngevTunConReasonNUM', opts[:reason_num], :int_32
    inform.add 'nghwWanConnectionMethod', opts[:wan_conn], :int_32
    inform.add 'ngtuTunInitiator', opts[:initiator], :int_32
    inform.add 'ngtuIpsecIface', opts[:interface], :string
    inform.add 'ngevVPNServiceIPv4Address', opts[:service_ip], :ip_address
    inform.add 'ngevVPNServiceIPv6Address', opts[:service_ipv6], :ipv6_address
    inform
  end

  def self.tunnel_up_ipv6(opts = {})
    opts[:endpoint]        = '0.0.0.0'
    opts[:endpoint_ipv6] ||= random_ipv6_address
    opts[:service_ip]      = '0.0.0.0'
    opts[:service_ipv6]  ||= random_ipv6_address

    tunnel_up(opts)
  end

  # ngevTunnelDown - Information about a tunnel disconnect. The one exception is a tunnel 'alive' message, which is sent for a
  # tunnel that is still up. Tunnel 'alive' messages have a ngtuReasonCode of tnlAlive.
  #
  # OBJECTS {
  #   ngevMac (DisplayString)
  #   ngevTunConnectionId (DisplayString)
  #   ngevTunnelDateAndTime (DateAndTime)
  #   ngtuReasonCode
  #     INTEGER {
  #       discoButton(0),
  #       tnlInitiatedDisco(226),
  #       missedHeartBts(227),
  #       eth1Lost(258),
  #       eth1Restored(259),
  #       powerLost(503),
  #       tnlAlive(504),
  #       deviceStatusInactiveWasActive(506),
  #       noTunnInInetOnlyMode(507),
  #       tnlIdleTimeout(508),
  #       dialIdleTimeout(509),
  #       maxSessionTimeout(510),
  #       noReasonGiven(599)
  #     }
  # }
  #
  def self.tunnel_down(opts = {})
    opts[:mac]           ||= random_mac_address
    opts[:connection_id] ||= random_connection_id
    opts[:datetime]      ||= java.util.GregorianCalendar.new
    opts[:reason_code]   ||= GatewayTunnel::REASON_CODES.keys.sample

    inform = self.new 'ngevTunnelDown'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevTunConnectionId', opts[:connection_id], :string
    inform.add 'ngevTunnelDateAndTime', opts[:datetime], :date_and_time
    inform.add 'ngtuReasonCode', opts[:reason_code], :int_32
    inform
  end

  # ngevDialAutotestFailure - The Dial Autotest failed.
  #
  # OBJECTS {
  #   ngevMac (DisplayString)
  #   ngevDialTestFailReason (DisplayString) - Reason the test failed (str)
  #   ngevDialTestFailCategory
  #     INTEGER {
  #       dialProblemError(0),
  #       dialProblemWarning(1),
  #       dialProblemMessage(2),
  #       dialProblemNoCategory(3)
  #     }
  # }
  #
  def self.dial_test_failed(opts = {})
    opts[:mac]      ||= random_mac_address
    opts[:reason]   ||= "Reason the test failed"
    opts[:category] ||= Gateway::DIAL_PROBLEMS.keys.sample

    inform = self.new 'ngevDialAutotestFailure'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevDialTestFailReason', opts[:reason], :string
    inform.add 'ngevDialTestFailCategory', opts[:category], :int_32
    inform
  end

  # ngevDeviceReboot - Sent when the device reboots. Requestor IP will be set to
  # loopback if an automated process caused the reboot.
  #
  # OBJECTS {
  #   ngevMac,
  #   ngevRequestorIp - IPv4 of the reboot requestor
  #   ngevIPv6RequestorIp - IPv6 of the requestor
  # }
  #
  def self.reboot(opts = {})
    opts[:mac]            ||= random_mac_address
    opts[:requestor]      ||= random_ip_address
    opts[:requestor_ipv6] ||= '0:0:0:0:0:0:0:0'

    inform = self.new 'ngevDeviceReboot'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevRequestorIp', opts[:requestor], :ip_address
    inform.add 'ngevIPv6RequestorIp', opts[:requestor_ipv6], :ipv6_address
    inform
  end

  def self.reboot_ipv6(opts = {})
    opts[:requestor]        = '0.0.0.0'
    opts[:requestor_ipv6] ||= random_ipv6_address

    reboot(opts)
  end

  # ngevDeviceShutdown - Sent when the device shuts down.
  #
  # OBJECTS {
  #   ngevMac,
  #   ngevRequestorIp,
  #   ngevIPv6RequestorIp
  # }
  #
  def self.shutdown(opts = {})
    opts[:mac]            ||= random_mac_address
    opts[:requestor]      ||= random_ip_address
    opts[:requestor_ipv6] ||= '0:0:0:0:0:0:0:0'

    inform = self.new 'ngevDeviceShutdown'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevRequestorIp', opts[:requestor], :ip_address
    inform.add 'ngevIPv6RequestorIp', opts[:requestor_ipv6], :ipv6_address
    inform
  end

  def self.shutdown_ipv6(opts = {})
    opts[:requestor]        = '0.0.0.0'
    opts[:requestor_ipv6] ||= random_ipv6_address

    shutdown(opts)
  end

  # ngevDhcpPoolExhausted - Sent when a VLAN's DHCP pool is full.
  #
  # OBJECTS {
  #   ngevMac,
  #   ngvlId - VLAN number (int)
  # }
  #
  def self.dhcp_pool_exhausted(opts = {})
    opts[:mac]     ||= random_mac_address
    opts[:vlan_id] ||= rand(10)

    inform = self.new 'ngevDhcpPoolExhausted'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngvlId', opts[:vlan_id], :int_32
    inform
  end

  # ngevSshLogin - Sent when a user accesses the device via SSH.
  #
  # OBJECTS {
  #   ngevMac,
  #   ngevSshSourceIp,
  #   ngevIPv6RequestorIp
  # }
  #
  def self.ssh_login(opts = {})
    opts[:mac]         ||= random_mac_address
    opts[:source]      ||= random_ip_address
    opts[:source_ipv6] ||= '0:0:0:0:0:0:0:0'

    inform = self.new 'ngevSshLogin'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevSshSourceIp', opts[:source], :ip_address
    inform.add 'ngevIPv6RequestorIp', opts[:source_ipv6], :ipv6_address
    inform
  end

  def self.ssh_login_ipv6(opts = {})
    opts[:source]        = '0.0.0.0'
    opts[:source_ipv6] ||= random_ipv6_address

    ssh_login(opts)
  end

  # ngevSecondaryAuthAttempt - Sent when a secondary login attempt occurs. Secondary logins are performed
  # when someone tries to ssh into the AT&T NetGate.
  #
  # OBJECTS {
  #   ngevMac
  #   ngevRequestorIp - IPv4
  #   ngevAccount - Account used for ssh login attempt (str)
  #   ngevUserid - User ID used for ssh login attempt (str)
  #   ngevLoginResult - Result of the login attempt
  #     INTEGER {
  #       failure(0),
  #       success(1)
  #     }
  #   ngevNewLocalID (DisplayString) - Userid the user became after successful login attempt
  #   ngevIPv6RequestorIp - IPv6
  # }
  #
  def self.secondary_auth_attempt(opts = {})
    opts[:mac]            ||= random_mac_address
    opts[:requestor]      ||= random_ip_address
    opts[:requestor_ipv6] ||= '0:0:0:0:0:0:0:0'
    opts[:account]        ||= 'ATTDEMO'
    opts[:user]           ||= 'ACCEL6'
    opts[:login_result]   ||= Gateway::LOGIN_RESULTS.keys.sample
    opts[:new_local_id]   ||= 'root'

    inform = self.new 'ngevSecondaryAuthAttempt'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevRequestorIp', opts[:requestor], :ip_address
    inform.add 'ngevIPv6RequestorIp', opts[:requestor_ipv6], :ipv6_address
    inform.add 'ngevAccount', opts[:account], :string
    inform.add 'ngevUserid', opts[:user], :string
    inform.add 'ngevLoginResult', opts[:login_result], :int_32
    inform.add 'ngevNewLocalID', opts[:new_local_id], :string
    inform
  end

  def self.secondary_auth_attempt_ipv6(opts = {})
    opts[:requestor]        = '0.0.0.0'
    opts[:requestor_ipv6] ||= random_ipv6_address

    secondary_auth_attempt(opts)
  end

  # ngevSwitchUseridAttempt - Sent when a user who is ssh'ed into the AT&T
  # NetGate tries to switch to another userid.
  #
  # OBJECTS {
  #   ngevMac,
  #   ngevRequestorIp - IPv4
  #   ngevAccount - Account used for ssh login attempt (str)
  #   ngevUserid - User ID used for ssh login attempt (str)
  #   ngevLoginResult - Result of the login attempt
  #     INTEGER {
  #       failure(0),
  #       success(1)
  #     }
  #   ngevNewLocalID - Userid the user became after successful login attempt
  #   ngevOldLocalID - Userid in use before use made switch
  #   ngevIPv6RequestorIp - IPv6
  # }
  #
  def self.ssh_user_switch(opts = {})
    opts[:mac]            ||= random_mac_address
    opts[:requestor]      ||= random_ip_address
    opts[:requestor_ipv6] ||= '0:0:0:0:0:0:0:0'
    opts[:account]        ||= 'ATTDEMO'
    opts[:user]           ||= 'ACCEL6'
    opts[:login_result]   ||= login_results.keys.sample
    opts[:new_local_id]   ||= 'root'
    opts[:old_local_id]   ||= 'admin'

    inform = self.new 'ngevSwitchUseridAttempt'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevRequestorIp', opts[:requestor], :ip_address
    inform.add 'ngevIPv6RequestorIp', opts[:requestor_ipv6], :ipv6_address
    inform.add 'ngevAccount', opts[:account], :string
    inform.add 'ngevUserid', opts[:user], :string
    inform.add 'ngevLoginResult', opts[:login_result], :int_32
    inform.add 'ngevNewLocalID', opts[:new_local_id], :string
    inform.add 'ngevOldLocalID', opts[:old_local_id], :string
    inform
  end

  def self.ssh_user_switch_ipv6(opts = {})
    opts[:requestor]        = '0.0.0.0'
    opts[:requestor_ipv6] ||= random_ipv6_address

    ssh_user_switch(opts)
  end

  # ngevCellModemRequired - Sent when either the AT&T NetGate boots without a
  # cell modem present or when the AT&T NetGate detects that the cell modem has
  # been removed. The cellular modem must be a supported unit that allows the
  # AT&T NetGate to query its current status.
  #
  # OBJECTS {
  #   ngevMac,
  #   ng3gModel - 3G card model
  #   ng3gManufacturer - 3G card manufacturer
  #   ng3gID - IMEI or ESN of card
  #   ng3gRevision - 3G card revision
  #   ng3gSIMCardNumber - Number of the card
  #   ng3gPhoneNum - Phone number
  #   ngevCardNotFoundTime - When the card went missing
  # }
  #
  def self.cell_modem_required(opts = {})
    opts[:mac]          ||= random_mac_address
    opts[:model]        ||= 'Shockwave'
    opts[:manufacturer] ||= 'Sierra Wireless'
    opts[:id]           ||= '1231414115'
    opts[:revision]     ||= '1.2.3'
    opts[:sim_card_id]  ||= '123456'
    opts[:phone_number] ||= '8131234567'
    opts[:removed_time] ||= java.util.GregorianCalendar.new

    inform = self.new 'ngevCellModemRequired'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ng3gModel', opts[:model], :string
    inform.add 'ng3gManufacturer', opts[:manufacturer], :string
    inform.add 'ng3gID', opts[:id], :string
    inform.add 'ng3gRevision', opts[:revision], :string
    inform.add 'ng3gSIMCardNumber', opts[:sim_card_id], :string
    inform.add 'ng3gPhoneNum', opts[:phone_number], :string
    inform.add 'ngevCardNotFoundTime', opts[:removed_time], :date_and_time
    inform
  end

  def self.unknown_event(opts = {})
    opts[:mac]      ||= random_mac_address
    opts[:datetime] ||= java.util.GregorianCalendar.new
    opts[:reason]   ||= 'Successful Logon'

    inform = self.new 'ngevForTestingOnly'
    inform.add 'ngevMac', opts[:mac], :string
    inform.add 'ngevTunConReason', opts[:reason], :string
    inform.add 'ngevTunnelDateAndTime', opts[:datetime], :date_and_time
    inform
  end


  # evDualWanIfaceProblem - Sent when the AT&T VPN Gateway is configured to
  # also use the second WAN port as Internet Primary and one of the interfaces
  # could not be brought up.
  #
  # Params:
  #   ngevMac(DisplayString)
  #   evDualWanProb
  #   evDualWanV6Prob
  #   evTrapCreatedTime
  #
  def self.wan2_connection_error(opts = {})
    opts[:mac] ||= random_mac_address

    inform = self.new 'evDualWanIfaceProblem'
    inform.add 'ngevMac', opts[:mac], :string
    inform
  end

end
