require 'pp'
require 'rake'

if defined?(RSpec)
  require 'rspec/core/rake_task'
end

class Engine

  CONFIG_NAME = 'config/engine.yml'

  FILES = {
    torquebox: {
      source: 'config/torquebox.yml',
      destination: 'config/torquebox.yml'
    },
    login: {
      source: 'app/views/devise/sessions/new.html.erb',
      destination: 'app/views/devise/sessions/new.html.erb'
    },
    devise: {
      source: 'config/initializers/devise.rb',
      destination: 'config/initializers/devise.rb'
    },
    newrelic: {
      source: 'config/newrelic.yml',
      destination: 'config/newrelic.yml'
    },
    translation: {
      source: 'config/locales/en/engine.yml',
      destination: 'config/locales/en/engine.yml'
    }
  }

  GEM_SOURCE = 'https://gems.accns.com'

  class EngineError < StandardError; end

  class << self

    def load(engine)
      if loaded?
        puts "Engine already loaded: #{engine}"
        puts
        puts "Please unload the current engine by running:"
        puts "  $ rake engine:unload"
        return
      end

      @engine = engine

      load_config
      copy_files
      copy_seed_migrations
      save_config
    end

    def unload
      cleanup_files
      cleanup_seed_migrations
      remove_config
    end

    def reload(engine)
      unload
      load(engine)
    end

    def loaded?
      File.exists?(config_path) && File.directory?(engine_path)
    end

    def run_tests(type = :all)
      spec = RSpec::Core::RakeTask.new(:app_spec)
      spec.rspec_opts = ['--format documentation', '--backtrace', '--color']
      spec_glob = '**{,/*/**}/*_spec.rb'

      case type
      when :core
        spec.rspec_opts << '--exclude-pattern "**/features/*_spec.rb"'
        spec.rspec_opts << '--tag ~type:integration'
        spec.pattern = "spec/#{spec_glob}"
      when :engine
        raise EngineError.new('Please load an engine to test') unless loaded?
        spec.pattern = "engines/#{engine}/spec/#{spec_glob}"
      when :features
        spec.pattern = "spec/features/#{spec_glob}"
      else
        spec.pattern = "{spec,engines/#{engine}/spec}/#{spec_glob}"
      end

      spec.run_task(true)
    end

    def display_info
      pp config
    end

    def config
      @config ||= File.exists?(config_path) ? YAML.load_file(config_path) : {}
    end

    def engine
      @engine ||= config[:engine]
    end
    alias_method :name, :engine

    def using_jruby?
      RUBY_PLATFORM == 'java'
    end

    def build
      raise EngineError.new('Please use JRuby to build the application') unless using_jruby?

      # Set git revision
      if ENV['STAGING'] == 'true'
        `echo $(git rev-parse HEAD) > #{root_path}/REVISION`
      else
        FileUtils.rm_f "#{root_path}/REVISION"
      end

      # Assets
      ENV['EXECJS_RUNTIME'] = 'Node'
      ENV['JRUBY_OPTS'] = '--dev'

      Rake::Task['assets:precompile'].invoke

      # Gems
      # TODO: See if removing this affects the bundle commands
      Bundler.clean_system(%{ bundle install --deployment --without development test })

      # Set engine gem
      set_engine_gem

      # Archive
      ENV['EXCLUDE'] = '^virt_env/'
      Rake::Task['torquebox:archive'].invoke
    end

    def gem_source
      @gem_source ||= ENV['GEM_SOURCE'] || GEM_SOURCE
    end

    def gem_source_line
      @gem_source_line ||= "source '#{gem_source}'\n"
    end

    def load_gem_source
      lines = read_gemfile

      unless lines.include?(gem_source_line)
        lines.each_with_index do |line, i|
          if line =~ /^source/
            lines.insert((i + 1), gem_source_line)
            break
          end
        end
      end

      write_gemfile(lines)
    end

    def unload_gem_source
      lines = read_gemfile
      lines.delete(gem_source_line) if lines.include?(gem_source_line)
      write_gemfile(lines)
    end

    def set_engine_gem
      lines = read_gemfile

      new_lines = []

      lines.each do |line|
        new_lines << line
        break if line =~ /^# Load engine/
      end

      new_lines << "gem '#{name}', path: '#{gem_path}'"

      write_gemfile(new_lines)
    end

    def read_gemfile
      lines = []
      File.open(app_gemfile_path, 'r') do |file|
        file.readlines.each do |line|
          yield line if block_given?
          lines << line
        end
      end
      lines
    end

    def write_gemfile(lines = [])
      raise ArgumentError.new('Please provide valid gemfile data') if lines.empty?
      File.open(app_gemfile_path, 'w') do |file|
        lines.each { |line| file.puts line }
      end
    end

    def root_path
      @root_path ||= Rails.root.to_s
    end

    def config_path
      @config_path ||= "#{root_path}/#{CONFIG_NAME}"
    end

    def engines_path
      @engines_path ||= "#{root_path}/engines"
    end

    def engine_path
      @engine_path ||= "#{engines_path}/#{engine}"
    end

    def seeds_path
      @seeds_path ||= "#{engine_path}/seed_migrations"
    end

    def gem_path
      @gem_path ||= "engines/#{engine}"
    end

    def spec_path
      @spec_path ||= "#{engine_path}/spec"
    end

    def db_data_path
      @db_data_path ||= "#{root_path}/db/data"
    end

    def app_gemfile_path
      @app_gemfile_path ||= Rails.root.join('Gemfile').to_s
    end

    def load_config
      @config = {
        engine:      engine,
        engine_path: engine_path,
        gem_path:    gem_path
      }
      validate!
    end

    def copy_files
      puts "Copying files ..."
      FILES.each do |key, data|
        source_path      = "#{engine_path}/#{data[:source]}"
        destination_path = "#{root_path}/#{data[:destination]}"

        next unless File.exists?(source_path)

        puts "--> #{data[:destination]}"

        backup key, destination_path

        FileUtils.cp source_path, destination_path
        config[key] = destination_path
      end
      puts
    end

    def cleanup_files
      FILES.each do |key, (source, destination)|
        # Remove file
        if config[key] && File.exists?(config[key])
          puts "Removing #{config[key]}"
          FileUtils.rm_f config[key]
        end

        # Check for backup
        backup_key = "backup_#{key}".to_sym
        if config[backup_key] && File.exists?(config[backup_key])
          puts "Restoring #{config[backup_key]}"
          FileUtils.mv config[backup_key], config[key]
        end
      end
    end

    def copy_seed_migrations
      return unless File.directory?(seeds_path)

      puts "Copying seed migrations ..."
      config[:seed_migrations] = []

      Dir[seeds_path + '/*.rb'].each do |seed_path|
        seed_name = File.basename(seed_path)
        puts "--> db/data/#{seed_name}"
        FileUtils.cp "#{seeds_path}/#{seed_name}", db_data_path
        config[:seed_migrations] << seed_name
      end
    end

    def cleanup_seed_migrations
      config.fetch(:seed_migrations, []).each do |seed_migration|
        seed_file = "#{db_data_path}/#{seed_migration}"
        next unless File.exists?(seed_file)
        puts "Removing #{seed_file}"
        FileUtils.rm_f "#{seed_file}"
      end
    end

    def backup(key, destination_path)
      return unless File.exists?(destination_path)

      file_name = File.basename(destination_path)
      directory = File.dirname(destination_path)

      backup_path = "#{directory}/.#{file_name}"

      FileUtils.cp destination_path, backup_path

      backup_key = "backup_#{key}".to_sym
      config[backup_key] = backup_path
    end

    def save_config
      File.open(config_path, 'w') { |f| f.write config.to_yaml }
    end

    def remove_config
      return unless File.exists?(config_path)
      puts "Removing config #{config_path}"
      FileUtils.rm_f config_path
    end

    def validate!
      raise ArgumentError.new('Please provide an :engine to setup') unless engine
      raise IOError.new("Unable to find engine path: #{engine_path}") unless File.directory?(engine_path)
    end
  end
end
