require 'csv'

class SmxParser
  include Enumerable

  class InvalidFileError < StandardError; end

  def initialize(filename)
    @filename = filename
  end

  def each
    begin
      File.open(File.expand_path(@filename), "r:ISO-8859-1") do |file|
        csv = CSV.new(file, :col_sep => "\t", :headers => true, :quote_char => "\x07")
        csv.each do |row|
          yield map_fields(clean_data(row.to_hash))
        end
      end
    rescue => e
      raise InvalidFileError, "Invalid import file - #{e.message}"
    end
  end

  private

  # Private: format a string to be a suitable key
  #
  # key - unformatted key
  #
  # Examples
  #
  #   format_key "ADDRESS_LINE1"
  #   #=> :address_line1
  #
  # Returns a symbolized key with only alphanumeric or underscore downcased characters
  def format_key key
    key.strip.
        downcase.
        gsub(/[^a-z0-9_]/,'').
        to_sym
  end

  # Private: clean up value strings
  #
  # value - unformatted value
  #
  # Examples # #   format_value "BOB      WHITE   "
  #   #=> "BOB WHITE"
  #
  # Returns a string with extra whitespace removed
  def format_value value
    "#{value}".strip.squeeze(" ")
  end

  # Private: convert "Y" and "N" to proper booleans
  #
  # value - string representation of a boolean
  #
  # Examples
  #
  #   booleanize "N"
  #   #=> false
  #
  #   booleanize "Y"
  #   #=> true
  #
  # Returns a boolean
  def booleanize value
    value === 'Y'
  end

  # Internal: Convert a variable to an integer. If the variable is blank, this
  # will fallback to the default.
  #
  # var     - The variable to convert to an integer.
  # default - The default value to use, if var is blank (optional).
  #
  # Returns an integer version of the variable, if it's not blank; otherwise
  #   the default value.
  def integer_or_default(var, default = nil)
    var.blank? ? default : var.to_i
  end

  # Internal: Check if the port is a valid port. If the variable is blank or the
  # port is not within the range of valid ports, this will return the default port.
  #
  # port    - The port.
  # default - The default value to use, if the port is invalid.
  #
  # Returns an integer version of the variable, if it's not blank; otherwise
  #   the default value.
  def validate_port(port, default = nil)
    (1..65535).include?(port.to_i) ? port.to_i : default
  end

  # Internal: Checks if the value is present.
  #
  # Returns the value if it is present; otherwise nil.
  def value_or_nil(value)
    value.present? ? value : nil
  end

  # Private: remove nil keys, symbolize keys, clean up keys and values
  #
  # data - hash with possibly nil keys and unformatted values
  #
  # Examples
  #
  #   clean_data {"ADDRESS_LINE3 " => "  @MEMPHIS TN38002   "
  #   #=> {:address_line3 => "MEMPHIS TN38002"}
  #
  # Returns a cleaned-up hash
  def clean_data data
    data.reject {|k,_| k.nil?}.reduce({}) do |record,set|
      record.merge(format_key(set[0]) => format_value(set[1]))
    end
  end

  # Private: maps Service Manager (SMX) fields to our fields
  #
  # orig - original data to be mapped
  #
  # Examples
  #
  #   map_fields {:customer_name => "fancypants",
  #               :device_id => "00C0CFFFFFFF",
  #               :software_level => "4.10.34"
  #              }
  #   #=> {:customer_name => "fancypants",
  #        :device        => {:mac => "00C0CFFFFF", :firmware => "4.10.34"},
  #        :details       => {},
  #        :portal_config => {},
  #        :snmp_config   => {},
  #        :contact       => {},
  #        :location      => {}
  #       }
  #
  # Returns hash with fields we're expecting containing data from equivalent SMX fields
  def map_fields(orig)
    {
      :customer_name => orig[:customer_name],

      :device => {
        :mac           => orig[:device_id],
        :host          => orig[:current_ip_address],
        :firmware      => orig[:software_level],
        :hw_version    => orig[:hardware_model],
        :serial        => orig[:serial]
      },

      :site => {
        :name => orig[:description]
      },

      :account       => orig[:account_id],

      :details => {
        :active_conn_type    => orig[:current_connection_type],
        :wan_1_conn_type     => orig[:primary_connection_type],
        :wan_1_circuit_type  => orig[:wan_circuit_type],
        :wan_1_cell_extender => orig[:wan_cell_extender],
        :wan_2_cell_extender => orig[:wan2_cell_extender],
        :dial_mode           => orig[:dial_mode],
        :dial_backup_enabled => booleanize(orig[:dial_backup_ind]),
        :dial_backup_type    => orig[:dbu_circuit_type],
        :dial_modem_type     => orig[:modem_type],
        :configuration_name  => orig[:model_id]
      },

      :portal_config => {
        :password      => value_or_nil(orig[:device_password]),
        :ssl_password  => value_or_nil(orig[:ssl_password]),
        :ssl_port      => validate_port(orig[:internet_ssl_port], 443),
        :auth_user     => value_or_nil(orig[:inet_admin_userid]),
        :auth_password => value_or_nil(orig[:inet_admin_pw])
      },

      :snmp_config => {
        :version   => "2c",
        # The AT&T VPN Gateway forces the community string to be lower-cased
        :community => orig[:community_name].to_s.downcase
      },

      :contact => {
        :label => ContactLabels::SITE,
        :name  => orig[:contact_name],
        :phone => orig[:contact_number],
        :email => orig[:contact_email]
      },

      :location => {
        :address_1 => orig[:customer_address1],
        :address_2 => orig[:customer_address2],
        :address_3 => orig[:customer_address3],
        :line_1    => orig[:address_line1],
        :line_2    => orig[:address_line2],
        :line_3    => orig[:address_line3],
        :line_4    => orig[:address_line4],
        :city      => orig[:city],
        :state     => orig[:state],
        :province  => orig[:province],
        :postal    => orig[:zip_code],
        :country   => orig[:country],
        :latitude  => orig[:location_latitude],
        :longitude => orig[:location_longitude],
      },

      :wan_devices => {
        :wan1 => orig[:wan1_gateway_mac],
        :wan2 => orig[:wan2_gateway_mac]
      }
    }
  end

end
