require 'digest'
require 'roo'

# Provides useful interfaces for interacting with the VIG Excel Spreadsheet.
class VigSpreadsheet

  DIR_PATH    = Rails.root.join('config').to_s
  FILE_NAME   = 'vig_ip_address_data.xlsx'
  DIGEST_NAME = 'vig_data_digest.txt'

  FIELDS = {
          name: 'CHASSIS',
      location: 'DESCRIPTION',
    ip_address: 'IP ADDRESS'
  }.freeze

  def initialize
    @spreadsheet = Roo::Spreadsheet.open(self.class.path)
    @sheet       = @spreadsheet.sheet(0)
  end

  def rows
    @sheet.each(FIELDS).map { |data| VigSpreadsheet::Row.new(data) }
  end

  class << self

    def path
      @path ||= "#{DIR_PATH}/#{FILE_NAME}"
    end

    def digest_path
      @digest_path ||= "#{DIR_PATH}/#{DIGEST_NAME}"
    end

    def exists?
      File.file?(path)
    end

    def digest_exists?
      File.file?(digest_path)
    end

    def current_digest
      Digest::SHA256.file(path).to_s
    end

    def last_digest
      File.read(digest_path)
    end

    def changed?
      return true unless digest_exists?
      current_digest != last_digest
    end

    def save_digest
      File.write(digest_path, current_digest)
    end

    def rows
      VigSpreadsheet.new.rows
    end

  end

  # Each row of the spreadsheet represents a mapping of an IPv4/v6 address to
  # a VIG name and location. This class parses a row and formats each field
  # accordingly.
  class Row

    attr_reader :name, :location, :ip_address

    def initialize(data = {})
      @data = data || {}
      parse
    end

    def header?
      name == VigSpreadsheet::FIELDS[:name]
    end

    private

    def parse
      parse_name
      parse_location
      parse_ip_address
    end

    def parse_name
      @name = @data[:name].to_s

      # IPv6 names have the format of "NAME-V6"
      #   Example: "VAUSYD10-V6"
      @name = @name.split('-').first if @name.include?('-')
    end

    def parse_location
      @location = @data[:location].to_s

      # Locations have the format of "VIG ID - LOCATION"
      #   Example: "VIG #1 - LYNNWOOD, WA"
      @location = @location.split('-').last if @location.include?('-')

      # IPv6 locations may contain "IPv6"
      #   Example: "VIG #1 - SYDNEY IPV6"
      @location = @location.strip.gsub(' IPV6', '')
    end

    def parse_ip_address
      @ip_address = TunnelServerIpAddress.formatted_string(@data[:ip_address].to_s)

    # Thrown for an invalid IP Address, which is OK to catch here.
    rescue ArgumentError => e
      @ip_address = ''
    end

  end

end