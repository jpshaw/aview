require 'constants_helper'

# Event categories for filtering
module EventUUIDS

  include ConstantsHelper

  GATEWAY_UP                      =  1
  GATEWAY_DOWN                    =  2
  GATEWAY_FW_CHANGED              =  3
  GATEWAY_REBOOTED                =  4
  GATEWAY_IP_CHANGED              =  5
  GATEWAY_UNREACHABLE             =  6
  GATEWAY_DBU_FAILED              =  7
  GATEWAY_TUN_DOWN                =  8
  GATEWAY_TUN_UP                  =  9
  GATEWAY_WAN_CHANGE              = 10
  NETBRIDGE_UP                    = 11
  NETBRIDGE_DOWN                  = 12
  NETBRIDGE_FW_CHANGED            = 13
  NETBRIDGE_IP_CHANGED            = 14
  NETBRIDGE_REBOOTED              = 15
  NETBRIDGE_UNREACHABLE           = 16
  NETBRIDGE_CNTI_CHANGED          = 17
  NETREACH_UP                     = 18
  NETREACH_DOWN                   = 19
  NETREACH_FW_CHANGED             = 20
  NETREACH_IP_CHANGED             = 21
  NETREACH_REBOOTED               = 22
  NETREACH_UNREACHABLE            = 23
  REMOTEMANAGER_UP                = 24
  REMOTEMANAGER_DOWN              = 25
  REMOTEMANAGER_FW_CHANGED        = 26
  REMOTEMANAGER_IP_CHANGED        = 27
  REMOTEMANAGER_UNREACHABLE       = 28
  REMOTEMANAGER_CNTI_CHANGED      = 29
  REMOTEMANAGER_WAN_CHANGED       = 30
  DIALTOIP_UP                     = 31
  DIALTOIP_DOWN                   = 32
  DIALTOIP_FW_CHANGED             = 33
  DIALTOIP_IP_CHANGED             = 34
  DIALTOIP_UNREACHABLE            = 35
  DIALTOIP_CNTI_CHANGED           = 36
  DIALTOIP_WAN_CHANGED            = 37
  GATEWAY_DBU_THRESHOLD           = 38
  EMBEDDEDCELLULAR_UP             = 39
  EMBEDDEDCELLULAR_DOWN           = 40
  EMBEDDEDCELLULAR_FW_CHANGED     = 41
  EMBEDDEDCELLULAR_IP_CHANGED     = 42
  EMBEDDEDCELLULAR_UNREACHABLE    = 43
  EMBEDDEDCELLULAR_CNTI_CHANGED   = 44
  EMBEDDEDCELLULAR_WAN_CHANGED    = 45
  NETREACH_ROGUE_ACCESS_POINT     = 46
  NETBRIDGE_SIGNAL_CHANGED        = 47
  REMOTEMANAGER_SIGNAL_CHANGED    = 48
  EMBEDDEDCELLULAR_SIGNAL_CHANGED = 49
  REMOTEMANAGER_PORT_DOWN         = 50
  USBCELLULAR_UP                  = 51
  USBCELLULAR_DOWN                = 52
  USBCELLULAR_FW_CHANGED          = 53
  USBCELLULAR_IP_CHANGED          = 54
  USBCELLULAR_UNREACHABLE         = 55
  USBCELLULAR_CNTI_CHANGED        = 56
  USBCELLULAR_WAN_CHANGED         = 57
  USBCELLULAR_SIGNAL_CHANGED      = 58
  REMOTEMANAGER_REBOOTED          = 59
  DIALTOIP_REBOOTED               = 60
  EMBEDDEDCELLULAR_REBOOTED       = 61
  USBCELLULAR_REBOOTED            = 62
  UCPE_UP                         = 63
  UCPE_DOWN                       = 64
  UCPE_FW_CHANGED                 = 65
  UCPE_IP_CHANGED                 = 66
  UCPE_UNREACHABLE                = 67
  UCPE_WAN_CHANGED                = 68
  UCPE_REBOOTED                   = 69

  GATEWAY_CELL_LOCATION_CHANGED   = 70
  NETBRIDGE_CELL_LOCATION_CHANGED = 71
  REMOTEMANAGER_CELL_LOCATION_CHANGED = 72
  DIALTOIP_CELL_LOCATION_CHANGED  = 73
  CELLULAR_CELL_LOCATION_CHANGED  = 74
  UCPE_CELL_LOCATION_CHANGED      = 75

  # TODO: Remove EMBEDDEDCELLULAR and USBCELLULAR events after 1.5.0
  CELLULAR_UP                     = 39
  CELLULAR_DOWN                   = 40
  CELLULAR_FW_CHANGED             = 41
  CELLULAR_IP_CHANGED             = 42
  CELLULAR_UNREACHABLE            = 43
  CELLULAR_CNTI_CHANGED           = 44
  CELLULAR_WAN_CHANGED            = 45
  CELLULAR_SIGNAL_CHANGED         = 49
  CELLULAR_REBOOTED               = 61

  GATEWAY_SIGNAL_CHANGED          = 76
  GATEWAY_CNTI_CHANGED            = 77

  GATEWAY_DATA_USAGE_OVER_LIMIT   = 78
  NETBRIDGE_DATA_USAGE_OVER_LIMIT = 79
  REMOTEMANAGER_DATA_USAGE_OVER_LIMIT = 80
  DIALTOIP_DATA_USAGE_OVER_LIMIT  = 81
  CELLULAR_DATA_USAGE_OVER_LIMIT  = 82
  UCPE_DATA_USAGE_OVER_LIMIT      = 83

  GATEWAY_PING_TEST_SUCCEEDED     = 84
  GATEWAY_PING_TEST_FAILED        = 85
  GATEWAY_VRRP_STATE_CHANGED      = 86
  GATEWAY_OOB_CABLES_MISSING      = 87
  GATEWAY_OOB_CABLES_PRESENT      = 88

  # TODO: Don't depend on static numbers for UUIDs
  CELLULAR_ETHERNET_CONNECTED         = 200 # Starting new range to prevent conflicts
  CELLULAR_ETHERNET_DISCONNECTED      = 201
  REMOTEMANAGER_ETHERNET_CONNECTED    = 202
  REMOTEMANAGER_ETHERNET_DISCONNECTED = 203

  SYSLOGGABLE                     = [
                                      REMOTEMANAGER_DOWN,
                                      REMOTEMANAGER_SIGNAL_CHANGED,
                                      REMOTEMANAGER_PORT_DOWN,
                                      REMOTEMANAGER_ETHERNET_DISCONNECTED,
                                      CELLULAR_DOWN,
                                      CELLULAR_SIGNAL_CHANGED,
                                      CELLULAR_ETHERNET_DISCONNECTED,
                                    ].freeze

  # Create constants with arrays of UUIDS per device type.
  # Example:
  #   GATEWAY_UUIDS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 38]
  %w(
    GATEWAY
    NETBRIDGE
    NETREACH
    REMOTEMANAGER
    DIALTOIP
    EMBEDDEDCELLULAR
    USBCELLULAR
    CELLULAR
    UCPE
  ).each do |device_type|
    # Select all constants with names starting by the device type and generate an array with their values.
    values = self.constants.select {|c| c =~ /^#{device_type}_/}.map {|c| const_get(c)}
    const_set("#{device_type}_UUIDS", values)
  end
end
