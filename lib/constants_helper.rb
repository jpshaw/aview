# Public: Helper mixin for modules with constants.
module ConstantsHelper
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    # Public: Creates a list of the constant's values.
    #
    # Returns an array of values for the constants.
    def constants_values_list
      constants.map do |constant|
        next if invalid_constant?(constant)
        const_get(constant)
      end.compact
    end

    # Public: Creates a hash of the constants mapped to their values.
    def constants_hash
      retval = {}
      constants.each do |constant|
        next if invalid_constant?(constant)
        retval[constant.downcase] = const_get(constant)
      end
      retval
    end

    private

    # Internal: Checks if the name of a constant is valid. This uses
    # a black-list of values we don't want included with the module's
    # constants.
    #
    # name - Name of the constant to check validity for.
    #
    # Returns true if the constant is invalid, otherwise false.
    def invalid_constant?(name)
      [:ClassMethods].include?(name)
    end
  end
end
