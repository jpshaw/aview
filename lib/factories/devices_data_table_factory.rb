class DevicesDataTableFactory
  def self.new(options = {})
    klass = find_class(options[:name])
    klass = DevicesDataTable unless klass.present?
    return klass.new(options[:view_context], options[:reference_id]) if options[:reference_id]
    klass.new(options[:view_context])
  end

  def self.find_class(name)
    DevicesDataTable.descendants.find{|klass| klass.to_s.include?(name.to_s.camelize)} if name.present?
  end

  private_class_method :find_class
end
