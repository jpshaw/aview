require 'filesize'
require 'action_view/helpers/date_helper'

# DS Traffic Reporting (^DSFLOWRPT)
#
# Parses and displays data from a DS Flow Report sent from devices that use
# huawei modems.
#
# Description from Huawei:
#
#   When the MS is in the online_data state, this proactive message is sent once
#   every other 2s. The reported contents include the connection time of this DS,
#   current transmitting rate, current receiving rate, current DS transmitting
#   traffic, current DS receiving traffic, the PDP connection transmitting rate
#   determined after negotiation with network side, the PDP connection receiving
#   rate determined after negotiation with network side.
#
#   N1: Connection duration (seconds)
#   N2: measured upload speed (bits/s)
#   N3: measured download speed (bits/s)
#   N4: number of sent data (byte)
#   N5: number of received data (byte)
#   N6: connection, supported by the maximum upload speed (bytes/s)
#   N7: connection, supported by a maximum download speed (bytes/s)
#
class DSFlowReport
  include ActionView::Helpers::DateHelper

  KILO = 1000.0.freeze

  KEYS = [:N1, :N2, :N3, :N4, :N5, :N6, :N7].freeze

  KEY_MAP = {
    N1: :uptime,
    N2: :upload_speed,
    N3: :download_speed,
    N4: :bytes_sent,
    N5: :bytes_received,
    N6: :max_upload_speed,
    N7: :max_download_speed
  }.freeze

  attr_reader *KEYS

  def initialize(attrs = {})
    attrs.each do |key, value|
      instance_variable_set("@#{key}", value.to_i.abs)
    end
  end

  # Provide a lower-case ruby method for accessing each data point.
  KEYS.each do |key|
    define_method(key.to_s.downcase) do
      instance_variable_get("@#{key}")
    end
  end

  def to_h
    return @hash if @hash

    @hash = {}

    @hash[:raw]    = raw_hash    if raw_hash.present?
    @hash[:parsed] = parsed_hash if parsed_hash.present?

    @hash
  end

  def uptime
    distance_of_time_in_words(n1)
  end

  def upload_speed
    pretty_bits_per_second(n2)
  end

  def download_speed
    pretty_bits_per_second(n3)
  end

  def bytes_sent
    pretty_bytes(n4)
  end

  def bytes_received
    pretty_bytes(n5)
  end

  def max_upload_speed
    pretty_bytes_per_second(n6)
  end

  def max_download_speed
    pretty_bytes_per_second(n7)
  end

  private

  def raw_hash
    KEYS.inject({}) do |hash, key|
      if value = send(key)
        hash[key] = value
      end
      hash
    end
  end

  def parsed_hash
    KEY_MAP.inject({}) do |hash, (key, method)|
      if value = send(key)
        hash[method] = send(method)
      end
      hash
    end
  end

  def pretty_bits_per_second(bits)
    "#{Filesize.from("#{bits.to_i / (8 * KILO)} KB").pretty}/s"
  rescue
    "#{bits} bits/s"
  end

  def pretty_bytes(bytes)
    Filesize.from("#{bytes.to_i / KILO} KB").pretty
  rescue
    "#{bytes} B"
  end

  def pretty_bytes_per_second(bytes)
    "#{pretty_bytes(bytes)}/s"
  end
end
