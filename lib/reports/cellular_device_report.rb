require 'reports/device_report'
require 'reports/modem_reportable'

module Reports

  class CellularDeviceReport < DeviceReport

    include ModemReportable

    def initialize(records = [], timezone_proc = nil)
      super(records, timezone_proc)
      @name = :cellular_device_report
    end

    protected

    def create_header
      @header = ["Cellular Device Report generated on #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S %z')}"]
    end

    def create_table_header
      @table_header = [
        "Status", t(:mac), "Serial", "Host", "Model", "Organization", "Site", "Firmware",
        "Configuration", "Client MAC", "Last Restart", "City", "Country", "Last Heartbeat"
      ] + modem_headers
    end

    def create_table_body
      @records.each do |record|
        model_name          = record.series.name        if record.series.present?
        organization_name   = record.organization.name  if record.organization.present?
        site_name           = record.site.name          if record.site.present?

        if record.location.present?
          location_city       = record.location.city
          location_ctry_code  = record.location.ctry_code
        end

        @table_body << [
          record.status_title,
          record.mac,
          record.serial,
          record.host,
          model_name,
          organization_name,
          site_name,
          record.firmware,
          (record.configuration.present? ? record.configuration.name : 'N/A'),
          (record.client_mac if record.respond_to?(:client_mac)),
          timestamp_in_timezone(record.last_restarted_at),
          location_city,
          location_ctry_code,
          timestamp_in_timezone(record.last_heartbeat_at)
        ] + modem_row(record.modem)
      end
    end

  end

end
