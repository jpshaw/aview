#
# report = ApplicationStatsReport.new
# report.run
# report.save
#
class ApplicationStatsReport

  attr_reader :report_classes

  def initialize
    @report_classes = [UsersReport, DevicesReport, OrganizationsReport]
  end

  def run
    report_classes.each do |klass|
      report = klass.new
      report.run

      workbook.add_worksheet(name: report.name) do |sheet|
        report.rows.each do |row|
          style = row.options[:header] ? header_text : wrap_text
          sheet.add_row(row.data, style: style)
        end
      end
    end
  end

  def name
    @name ||= "aview_stats_#{report_date}.xlsx"
  end

  def save
    package.serialize(name)
  end

  private

  def wrap_text
    @wrap_text ||= workbook.styles.add_style(wrap_text_style)
  end

  def header_text
    @header_text ||= workbook.styles.add_style(header_style)
  end


  def package
    @package ||= Axlsx::Package.new
  end

  def workbook
    @workbook ||= package.workbook
  end

  def report_date
    Time.now.strftime('%Y-%m-%d_%H-%M-%S')
  end

  def wrap_text_style
    {
      alignment: {
        horizontal: :center,
        vertical:   :center,
        wrap_text:  true
      }
    }
  end

  def header_style
    {
      fg_color: 'FFFFFF',
      bg_color: '990000',
      alignment: {
        horizontal: :center,
        vertical:   :center,
        wrap_text:  true
      }
    }
  end

  class BaseReport
    attr_accessor :name, :rows

    def initialize
      self.name = 'Report'
      self.rows = []
    end

    def set_name(name)
      self.name = name
    end

    def add_header(data, options = {})
      self.rows << Row.new(data, options.merge(header: true))
    end

    def add_row(data, options = {})
      self.rows << Row.new(data, options)
    end

    def blank_row
      add_row([''])
    end

    class Row
      attr_reader :data, :options

      def initialize(data, options = {})
        @data    = data
        @options = options || {}
      end
    end

  end

  class UsersReport < BaseReport
    def run
      set_name 'Users'

      add_header ['Total', 'Active', 'Inactive', 'Pending']

      add_row [
        User.count,
        User.with_status(:active).count,
        User.with_status(:inactive).count,
        User.with_status(:pending).count
      ]
    end
  end

  class DevicesReport < BaseReport

    def run
      set_name 'Devices'

      add_header ['Name', 'Total', 'Up', 'Down', 'Deployed', 'Undeployed']

      # All Devices
      add_row devices_row('All')

      # Each Device Category
      DeviceCategory.all.each do |category|

        # Add space between categories
        blank_row

        # Highlight each category row
        add_header devices_row(category.name, category_id: category.id)

        # Each Device Model
        category.models.each do |model|
          add_row devices_row(model.name, model_id: model.id)
        end
      end
    end

    private

    def devices_row(name, filter = {})
      [
        name,
        total_count(filter),
        up_count(filter),
        down_count(filter),
        deployed_count(filter),
        undeployed_count(filter)
      ]
    end

    def base_relation(filter)
      relation = Device
      relation = relation.where(filter) if filter.present?
      relation
    end

    def total_count(filter = {})
      base_relation(filter).count
    end

    def up_count(filter = {})
      base_relation(filter).with_status(DeviceState::States::UP).count
    end

    def down_count(filter = {})
      base_relation(filter).with_status(DeviceState::States::DOWN).count
    end

    def deployed_count(filter = {})
      base_relation(filter).with_deployment(Devices::Deployment::DEPLOYED).count
    end

    def undeployed_count(filter = {})
      base_relation(filter).with_deployment(Devices::Deployment::UNDEPLOYED).count
    end
  end

  class OrganizationsReport < BaseReport
    def run
      set_name 'Organizations'
      add_header ['Total']
      add_row [Organization.count]
    end
  end
end