
require 'reports/device_report'

module Reports

  class VpnDeviceReport < DeviceReport

    def initialize(records = [], timezone_proc = nil)
      super(records, timezone_proc)
      @name = :vpn_device_report
    end

    protected

    def create_header
      @header = ["VPN Device Report generated on #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S %z')}"]
    end

    def create_table_header
      @table_header = ["Status", t(:mac), "Serial", "Host", "Model", "Organization", "Site",
        "Firmware", "Tunnels", "Connection Type", "City", "Country", "Last Heartbeat"]
    end

    def create_table_body
      @records.each do |record|
        @table_body << [record.status_title, record.mac, record.serial, record.host, record.series_name, record.organization.name,
          record.site.name, record.firmware, record.tunnels_count, record.details.primary_connection_type_name, record.location.city,
          record.location.ctry_code, timestamp_in_timezone(record.last_heartbeat_at)]
      end
    end

  end

end
