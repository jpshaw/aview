
require 'reports/base_report'

module Reports

  class DeviceReport < BaseReport

    def initialize(records = [], timezone_proc = nil)
      super(records, timezone_proc)
      @name = :device_report
    end

    protected

    def create_header
      @header = ["Device Report generated on #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S %z')}"]
    end

    def create_table_header
      @table_header = ["Status", t(:mac), "Serial", "Host", "Category", "Model",
        "Organization", "Site", "Firmware", "City", "Country", "Last Heartbeat"]
    end

    def create_table_body
      @records.each do |record|
        # Get values from associated objects if they exist, otherwise values will be nil.
        # This will stop NoMethodError from being raised on methods for nil:NilClass.
        category_name       = record.category.name      if record.category
        model_name          = record.series.name        if record.series
        organization_name   = record.organization.name  if record.organization
        site_name           = record.site.name          if record.site

        if record.location
          location_city       = record.location.city
          location_ctry_code  = record.location.ctry_code
        end

        @table_body << [
          record.status_title,
          record.mac,
          record.serial,
          record.host,
          category_name,
          model_name,
          organization_name,
          site_name,
          record.firmware,
          location_city,
          location_ctry_code,
          timestamp_in_timezone(record.last_heartbeat_at)
        ]
      end
    end

    def create_footer
      @footer = []
    end

  end

end
