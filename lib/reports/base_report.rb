
require 'csv'

module Reports

  class BaseReport

    attr_reader :name
    def initialize(records = [], timezone_proc = nil)
      @records = records
      @header = []
      @table_header = []
      @table_body = []
      @footer = []
      @timezone_proc = timezone_proc
      @name = :report
    end

    def file_name
      "#{name}-#{Time.now.iso8601}.csv"
    end

    def generate!
      create_header
      create_table_header
      create_table_body
      create_footer
    end

    def to_csv
      CSV.generate do |csv|
        csv << @header
        csv << [nil]
        csv << @table_header
        @table_body.each do |row|
          csv << row
        end
        csv << [nil]
        csv << @footer
      end
    end

    protected

    def timestamp_in_timezone(timestamp)
      if @timezone_proc.respond_to?(:call)
        @timezone_proc.call(timestamp)
      else
        timestamp
      end
    end

    def create_header

    end

    def create_table_header

    end

    def create_table_body

    end

    def create_footer

    end

  end

end
