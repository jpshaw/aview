
require 'reports/device_report'

module Reports

  class WifiDeviceReport < DeviceReport

    def initialize(records = [], timezone_proc = nil)
      super(records, timezone_proc)
      @name = :wifi_device_report
    end

    protected

    def create_header
      @header = ["Wi-Fi Device Report generated on #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S %z')}"]
    end

    def create_table_header
      @table_header = ["Status", t(:mac), "Host", "Model", "Organization", "Site", "Firmware", "City", "Country", "Last Heartbeat"]
    end

    def create_table_body
      @records.each do |record|
        model_name          = record.series.name        if record.series
        organization_name   = record.organization.name  if record.organization
        site_name           = record.site.name          if record.site

        if record.location.present?
          location_city       = record.location.city
          location_ctry_code  = record.location.ctry_code
        end

        @table_body << [
          record.status_title,
          record.mac,
          record.host,
          model_name,
          organization_name,
          site_name,
          record.firmware,
          location_city,
          location_ctry_code,
          timestamp_in_timezone(record.last_heartbeat_at)
        ]
      end
    end

  end

end
