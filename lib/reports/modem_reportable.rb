module ModemReportable

  private

  def modem_headers
    report_columns.keys
  end


  def modem_row(modem=nil)
    report_columns.reduce([]) do |array, columns_map_array|
      array << (modem.present? ? modem.__send__(columns_map_array.last) : nil)
    end
  end


  def report_columns
    {
      'APN'       => :apn,
      'Band'      => :band,
      'Carrier'   => :carrier,
      'CID'       => :cid,
      'Ec/Io'     => :ecio,
      'ESN'       => :esn,
      'ICCID'     => :iccid,
      'IMEI'      => :imei,
      'IMSI'      => :imsi,
      'LAC'       => :lac,
      'MCC'       => :mcc,
      'MNC'       => :mnc,
      'Modem'     => :name,
      'Network'   => :cnti_details,
      'Phone#'    => :number,
      'RSRP'      => :rsrp,
      'RSRQ'      => :rsrq,
      'RSSI'      => :rssi,
      'Signal'    => :signal_str,
      'SiNR'      => :sinr,
      'SNR'       => :snr,
      'USB Speed' => :usb_speed,
      'Version'   => :revision
    }
  end

end
