# Basic Twilio HTTP client that supports JRuby, heavily inspirsed by the
# official twilio-ruby client found here: https://github.com/twilio/twilio-ruby

Dir[File.dirname(__FILE__) + '/twilio/*.rb'].each do |file|
  require file
end

module Twilio
  API_URL           = 'https://api.twilio.com'
  LOOKUPS_URL       = 'https://lookups.twilio.com'
  ACCOUNTS_VERSION  = '2010-04-01'
  LOOKUPS_VERSION   = 'v1'

  def self.accounts_url
    "#{API_URL}/#{ACCOUNTS_VERSION}/Accounts"
  end

  def self.phone_numbers_url
    "#{LOOKUPS_URL}/#{LOOKUPS_VERSION}/PhoneNumbers"
  end

  def self.account_sid
    Settings.twilio.account_sid
  end

  def self.auth_token
    Settings.twilio.auth_token
  end

  def self.from
    Settings.twilio.from
  end
end
