class SnmpResponse

  NO_SUCH_OBJECT   = 'noSuchObject'
  NO_SUCH_INSTANCE = 'noSuchInstance'
  END_OF_MIB_VIEW  = 'endOfMibView'

  INVALID_VALUES   = [NO_SUCH_OBJECT, NO_SUCH_INSTANCE, END_OF_MIB_VIEW]

  attr_reader :response, :results

  def initialize(response)
    @response = response
    @results  = {}

    parse_response if success?
  end

  def success?
    @success ||= response.present?
  end

  def data
    @data ||= {
      success: success?,
      results: results
    }
  end

  def to_h
    data
  end
  alias_method :to_hash, :to_h

  def to_json
    data.to_json
  end

  private

  def parse_response
    response.variable_bindings.each do |result|
      key   = result.oid.to_s
      value = result.variable.to_s

      next if value.blank?
      next if INVALID_VALUES.include?(value)

      @results[key] = value
    end
  end

end