require 'csv'
require 'device'
# FIXME: Temporarily commented out for Rails 4 upgrade
# require 'netbridge'
# require 'gateway'
# require 'netreach'
# require 'dial_to_ip'
# require 'remote_manager'
# require 'embedded_cellular'
# require 'device_factory'

class DeviceImporter
  include ::Kraken::Logger.file

  attr_reader :file_path

  KEYS = [
    :mac, :serial, :config_name, :host, :hw_version, :organization,
    :parent_organization, :deployed, :site_id, :last_updated_at, :address_1,
    :address_2, :city, :state, :postal_code, :country, :activated_at
  ].freeze

  # Internal: Processes the import file.
  #
  # file_path - The String path to the import file.
  #
  # Returns a Hash of processed objects.
  def self.import_file(file_path)
    # Process the import file
    importer = new file_path
    importer.process!
    importer.processed
  end

  def processed
    @processed ||= {
      devices: {
        added:   [],
        scanned: [],
        removed: [],
        skipped: []
      },
      customers: {
        added: []
      },
      accounts: {
        added: []
      },
      sites: {
        added: []
      },
      locations: {
        added: []
      },
      configurations: {
        added: []
      }
    }
  end

  def process!
    CSV.foreach(file_path, headers: true, encoding: 'ISO-8859-1') do |row|
      begin
        ActiveRecord::Base.transaction do
          process_row(row)
        end
      rescue => e
        logger.error "Error importing device #{row}\n" \
                     "#{e.message}\n #{e.backtrace}"
      end
    end
  end

  protected

  def initialize(file_path)
    @file_path = file_path
  end

  def root_org
    @root_org ||= Organization.order('id asc').first
  end

  def get_organization_for_name(name)
    organization = Organization.find_by(import_name: name)

    # Couldn't find organization by `import_name`, let's try `name`.
    if organization.blank?
      organization = Organization.find_by(name: name)

      if organization.present?
        # If `import name` is blank, set it to `name`.
        organization.update_attributes(import_name: name) if organization.import_name.blank?
      else
        params = { name: name, import_name: name }
        result = OrganizationCreator.call(params: params)

        if result.success?
          organization = result.organization
        else
          logger.error "Error creating organization with params: #{params}\n" \
                       "Errors: #{result.organization.errors.full_messages}"
        end
      end
    end

    organization
  end

  # Internal: Checks if the given `child` is a descendant of the `to` organization.
  # If not, it will add it as a child.
  def add_org_child(opts)
    parent = opts[:to]
    child  = opts[:child]
    unless parent.descendants.include?(child) || parent == child
      parent.add_child(child)
    end
  end

  def process_row(row)
    mac         = row['mac']
    serial      = row['serial']
    host        = row['host']
    hw_version  = row['hw_version']
    org_name    = row['organization']
    parent_org  = row['parent_organization']
    deployed    = row['deployed']
    site_id     = row['site_id']
    heartbeat   = row['last_updated_at']
    config_name = row['config_name']
    activated   = row['activated_at']

    return unless mac.present?

    mac.upcase!

    processed[:devices][:scanned] << mac

    return unless org_name.present?

    organization = get_organization_for_name(org_name)
    return unless organization.present?

    parent_organization = get_organization_for_name(parent_org) if parent_org.present?

    # If organization_parent is set, ensure the parent exists and add
    # the device's organization as a child.
    if parent_organization.present?
      # Ensure the parent org has a parent.
      add_org_child(to: root_org, child: parent_organization)

      # Ensure the organization's parent is the correct parent org.
      if organization.parent != parent_organization
        # `add_child` will automatically remove the old parent and associate
        # with the new.
        add_org_child(to: parent_organization, child: organization)
      end
    else
      add_org_child(to: root_org, child: organization)
    end

    device   = Device.find_by(mac: mac)
    device ||= Device.find_by_serial(serial) if serial.present?
    device ||= DeviceFactory.new(mac: mac, serial: serial, organization_id: organization.id)

    if device.present?

      # Configuration. We need to have a device record in order to lookup the correct
      # configuration class for the device.
      if config_name.present?
        config_class = device.configuration_class
        if config_class and config_class.attribute_method?(:name)
          configuration = config_class.where(organization_id: organization.id).
                                       where('upper(name) = upper(?)', config_name).first
        end
      end

      # Only set the host if given and one is not already set
      device.host          = host if host.present? && device.host.blank?
      device.is_deployed   = deployed == '1' if deployed.present?
      device.organization  = organization
      device.site          = organization.sites.for_name(site_id)
      device.configuration = configuration if configuration.present?
      device.serial        = serial if serial.present?

      if hw_version.present? && device_models.include?(hw_version)
        device.hw_version  = hw_version
        device_models_and_class_names.each do |model, class_name|
          device.object_type = class_name and break if model == hw_version
        end
      end

      if device.last_heartbeat_at.blank? && heartbeat.present?
        device.last_heartbeat_at = Time.parse(heartbeat) rescue nil
      end

      if activated.present?
        begin
          device.activated_at = Time.parse(activated)
        rescue ArgumentError
          logger.error "Error importing device #{mac}. activated_at is invalid"
        end
      end

      # The location minimally requires an address and city for geocoding.
      if row['address_1'].present? && row['city'].present?
        location_attributes = {
          address_1:   row['address_1'],
          address_2:   row['address_2'],
          city:        row['city'],
          state_code:  row['state'],
          postal_code: row['postal_code'],
          country:     row['country']
        }

        device.location = ::Locations::Upsert.execute(location_attributes)
      end

      processed[:sites][:added]   << device.site_name if device.site_id_changed?
      processed[:devices][:added] << device.mac       if device.new_record?

      device.save!
    else
      processed[:devices][:skipped] << mac
      raise "Could not create device from data: #{row.inspect}"
    end
  end

  def device_models
    @device_models ||= DeviceModel.all.pluck(:name)
  end

  def device_models_and_class_names
    @device_models_and_class_names ||= DeviceModel.all.pluck(:name, :class_name)
  end

end
