module DataTables
  class Base
    include ERB::Util

    attr_accessor :columns, :draw, :filtered_records_count, :length,
                  :order, :displayed_records, :search, :start, :total_records_count

    attr_reader :view

    delegate :current_user, :params, :link_to, :content_tag,
      :timestamp_in_users_timezone, :h, :t, :check_box_tag, to: :view

    EXPORT_LIMIT_TYPE_COUNT = 0
    EXPORT_LIMIT_TYPE_CURRENT_PAGE = 1
    EXPORT_LIMIT_TYPE_ALL_PAGES = 2
    EXPORT_LIMIT_TYPES = [EXPORT_LIMIT_TYPE_CURRENT_PAGE, EXPORT_LIMIT_TYPE_COUNT]
    HARD_EXPORT_LIMIT = 10_000
    EXPORT_LIMIT_RANGE = (1..HARD_EXPORT_LIMIT)
    DEFAULT_PAGE_LENGTH = 100
    LENGTH_MENU = [[ 25, 50, 100, 500 ], [ 25, 50, 100, 500 ]]

    def initialize(view)
      @view = view
      options = params.with_indifferent_access
      parse_options(options)
      parse_columns(options)
      parse_search(options)
      parse_orders(options)
    end

    private

    def export_limit
      @export_limit ||= begin
        if export_limit_type == EXPORT_LIMIT_TYPE_COUNT && EXPORT_LIMIT_RANGE.include?(params[:export_limit_count].to_i)
          params[:export_limit_count].to_i
        elsif export_limit_type == EXPORT_LIMIT_TYPE_CURRENT_PAGE
          @length
        else
          HARD_EXPORT_LIMIT
        end
      end
    end

    def export_limit_type
      @export_limit_type ||= begin
        if EXPORT_LIMIT_TYPES.include?(params[:export_limit_type].to_i)
          params[:export_limit_type].to_i
        else
          EXPORT_LIMIT_TYPE_COUNT
        end
      end
    end


    def export_offset
      @export_offset ||= @export_limit_type == EXPORT_LIMIT_TYPE_CURRENT_PAGE ? @start : nil
    end

    # Other options needed to draw the DataTable
    def parse_options(options)
      @draw = options[:draw].to_i
      @start = options[:start].to_i
      @length = options[:length].to_i
    end

    # Columns contain parameters specific to the DataTable column
    def parse_columns(options)
      if options[:columns].present?
        @columns = []
        options[:columns].each do |index, source_column|
          target_column = {}
          target_column[:data] = source_column[:data]
          target_column[:name] = source_column[:name]
          target_column[:searchable] = source_column[:searchable]
          target_column[:orderable] = source_column[:orderable]
          target_column[:search] = parse_columns_search(source_column[:search])
          @columns[index.to_i] = target_column
        end
      end
    end

    # Parse the search hash of a single column
    def parse_columns_search(search)
      result = {}
      result[:value] = search[:value]
      result[:regex] = search[:regex]
      result
    end

    # The search input for the DataTable
    def parse_search(options)
      @search = {}
      if options[:search].present?
        @search[:value] = options[:search][:value].to_s
        @search[:regex] = options[:search][:regex].to_s
      end
    end

    # Orders are the sorting columns for the DataTable
    def parse_orders(options)
      @order = {}
      if options[:order].present?
        options[:order].each do |col|
          index = col.first
          @order[index] = {}
          @order[index][:column] = options[:order][index][:column].to_s
          @order[index][:dir] = options[:order][index][:dir].to_s
          @order[index][:sort_scope] = options[:order][index][:sortScope].to_s
        end
      end
    end

    # Reduce columns hash to params needed for AR scope
    def column_search_params_vs
      params = {}

      @columns.each do |column|
        if value = safe_json_parse(column[:search][:value])
          params[column[:name]] = value
        end
      end

      format_params(params)
      params
    end

    def table_search_params_vs
      params = {}

      search_params = safe_json_parse(@search[:value], default: [])

      # Collect values of matching keys into an array
      search_params.each do |search_param|
        search_param.each do |key, value|
          params[key] ||= []
          params[key] << value
        end
      end

      format_params(params)
      params
    end

    def format_params(params)
      params.each do |key, value|
        # Remove array wrapper for single values
        if value.length == 1
          params[key] = value.first
        end
      end
    end

    def sort_params_vs
      params = {}
      @order.each_value do |sort|
        params[sort[:sort_scope]] = sort[:dir] if sort[:sort_scope].present?
      end
      params
    end

    # Return a default value if nil or invalid JSON is given; otherwise return
    # the parsed object.
    def safe_json_parse(input, default: nil)
      JSON.parse(input)
    rescue TypeError, JSON::ParserError
      default
    end
  end
end
