require 'logger'

class DeviceSyslog

  def initialize(type, opts = {})
    @type       = type
    @opts       = opts
    @attributes = {}
    @message    = nil

    # Defaults
    @opts[:local_hostname]  ||= '127.0.0.1'
    @opts[:ip_address]      ||= '127.0.0.1'
    @opts[:remote_hostname] ||= '127.0.0.1'
    @opts[:remote_port]     ||= 1514
  end

  def add(key, value)
    @attributes[key] = value
  end

  # Converts the attributes to k=v separated by "~"
  def message
    @message ||= @attributes.to_a.map { |k, v| "#{k}=#{v}" }.join("~")
  end

  def message=(new_message)
    @message = new_message
  end

  def send
    logger = Logger.new(DeviceSyslog::UdpSender.new(@opts))
    logger.info "#{@type}: #{message}"
  end

  # These hashes stolen from syslog_protocol gem

  FACILITIES = {
    'kern'     => 0,
    'user'     => 1,
    'mail'     => 2,
    'daemon'   => 3,
    'auth'     => 4,
    'syslog'   => 5,
    'lpr'      => 6,
    'news'     => 7,
    'uucp'     => 8,
    'cron'     => 9,
    'authpriv' => 10,
    'ftp'      => 11,
    'ntp'      => 12,
    'audit'    => 13,
    'alert'    => 14,
    'at'       => 15,
    'local0'   => 16,
    'local1'   => 17,
    'local2'   => 18,
    'local3'   => 19,
    'local4'   => 20,
    'local5'   => 21,
    'local6'   => 22,
    'local7'   => 23
  }

  FACILITY_INDEX = FACILITIES.invert

  SEVERITIES = {
    'emerg'   => 0,
    'alert'   => 1,
    'crit'    => 2,
    'err'     => 3,
    'warn'    => 4,
    'notice'  => 5,
    'info'    => 6,
    'debug'   => 7
  }

  SEVERITY_INDEX = SEVERITIES.invert
end
