require 'csv'
require 'netbridge_configuration'

class ConfigurationImporter
  include ::Kraken::Logger.file

  attr_accessor :file_path, :results

  def self.import_file(file_path)
    importer = new file_path
    importer.process!
    importer.results
  end

  def initialize(file_path)
    @file_path = file_path
    @results   = {
      added:   [],
      scanned: []
    }
  end

  def process!
    CSV.foreach(file_path, headers: true, encoding: 'ISO-8859-1') do |row|
      begin
        ActiveRecord::Base.transaction do
          process_row(row)
        end
      rescue => e
        logger.error "Error importing configuration #{row}\n" \
                     "#{e.message}\n #{e.backtrace}"
      end
    end
  end

  protected

  def process_row(row)
    organization_name               = row['organization']
    configuration_name              = row['name']
    parent_config_name              = row['parent_config_name']
    parent_config_organization_name = row['parent_config_organization']

    return unless configuration_name.present?

    results[:scanned] << configuration_name

    organization = get_organization_for_name(organization_name)

    if organization.blank?
      logger.error "Could not find organization '#{organization_name}' " \
                   "for configuration '#{configuration_name}'."
      return
    end

    # Get parent configuration.
    parent_config_organization = get_organization_for_name(parent_config_organization_name)
    if parent_config_name.present? && parent_config_organization.present?
      parent_configuration = NetbridgeConfiguration.where(
        organization_id: parent_config_organization.id,
        name:            parent_config_name
      ).first
    end

    # Find config by name or initialize a new one.
    configuration = get_configuration(organization.id, configuration_name)

    # Check if configuration is new
    new_record = configuration.new_record?

    # Extract row data into hash of config attributes
    config_data = cellular_variables(row)

    # Get locked fields
    locked_data = locked_variables(row)

    # Check if a Netbridge mac matches the configuration name. If so, we will
    # need to assign a configuration to it, whether it's a "Mac" config or
    # a "Group" config.
    device = Netbridge.find_by(mac: configuration_name.upcase)

    is_default_config = configuration_name == 'default'
    is_group_config   = device.blank?
    is_valid_data     = config_data.present? ||
                        locked_data.values.include?(NetbridgeConfiguration::UNLOCKED_VALUE)
    is_device_config  = device.present? && is_valid_data

    # Update configuration and ensure it's persisted if we have data, or it
    # is a default config.
    if is_default_config || is_group_config || is_device_config
      configuration.apply_attributes(config_data.with_indifferent_access, locked_data)

      if configuration.valid?
        results[:added] << configuration_name if new_record
      end
    end

    # Add child config to parent config if not already associated.
    if parent_configuration.present? && configuration.persisted?
      unless parent_configuration.children.include?(configuration)
        parent_configuration.add_child(configuration)
      end
    end

    # Ensure the device is pointed to the correct config.
    if device.present?
      device.configuration = configuration.persisted? ? configuration : parent_configuration
      device.save! if device.configuration_id_changed?
    end
  end

  def get_organization_for_name(name)
    return if name.blank?

    organization   = Organization.find_by(import_name: name)
    organization ||= Organization.find_by(name: name)

    if organization.blank?
      params = { name: name, import_name: name }
      result = OrganizationCreator.call(params: params)

      if result.success?
        organization = result.organization
      else
        logger.error "Error creating organization with params: #{params}\n" \
                     "Errors: #{result.organization.errors.full_messages}"
      end
    else
      if organization.import_name.blank?
        organization.update_attributes(import_name: name)
      end
    end

    organization
  end

  def get_configuration(organization_id, name)
    # First check if we can find the config by `import name`.
    config   = NetbridgeConfiguration.where(organization_id: organization_id)
                                     .find_by(import_name: name)

    # If we didn't find anything for `import name`, try `name`.
    config ||= NetbridgeConfiguration.where(organization_id: organization_id)
                                     .find_by(name: name)

    # If no configs found, initialize a new one.
    config ||= NetbridgeConfiguration.new(
      organization_id: organization_id,
      name:            name,
      import_name:     name
    )

    # If the import name is not set, update it to match the name. The should only
    # apply to existing configs.
    config.update_attributes(import_name: name) if config.import_name.blank?

    config
  end

  def cellular_variables(row)
    retval = {}

    cellular_keys.each do |key|
      unless row[key].nil?
        retval[key] = row[key]
      end
    end

    retval
  end

  def locked_variables(row)
    retval = {}

    cellular_keys.each do |key|
      if row["#{key}_locked"] == 'false'
        retval["#{key}_locked"] = NetbridgeConfiguration::UNLOCKED_VALUE
      elsif row["#{key}_locked"] == 'true'
        retval["#{key}_locked"] = NetbridgeConfiguration::LOCKED_VALUE
      end
    end

    retval
  end

  def cellular_keys
    @cellular_keys ||= NetbridgeConfiguration::CONFIG_KEYS.map(&:to_s)
  end
end
