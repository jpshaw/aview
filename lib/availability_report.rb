class AvailabilityReport < Report
  # This attribute is used by the form in the view.
  attribute :categories, Array[Integer], default: []

  def initialize(report_record=nil)
    super(report_record)
  end

  # TODO: Remove this method - doesn't look like it's used anywhere
  def device_categories
    DeviceCategory.order('device_categories.name asc')
  end

  private

  def big_query_enabled?
    @big_query_enabled ||= Settings.druid_big_query_service.enabled
  end

  def batch_size
    @batch_size ||= big_query_enabled? ? 1000 : 100
  end

  def generate_data
    update_report_record_stats_headers

    report_record.devices.find_in_batches(batch_size: batch_size) do |devices|
      macs = devices.map{ |device| device.mac }
      metrics = Measurement::DeviceAvailability.get(
        range: start_range,
        granularity: 'all',
        filters: [{ dimension: :mac,  value: macs }]
      )
      devices.each do |device|
        results[device] = metrics[device.mac]
      end
    end
  end

  def start_range
    @start_range ||=
      case report_range
      when 'last_day'
        '24h'
      when 'last_week'
        '7d'
      when 'last_month'
        '30d'
      else
        '24h'
      end
  end

  def table
    table = []
    results.each do |device, data|
      stats = data.present? ? statistics(data) : {}
      table << [
        device.mac,
        device.host,
        device.category_name,
        device.series_name,
        device.organization_name,
        nil,
        (device.site.name          if device.site),
        (device.deployed? ? "Deployed" : "Undeployed"),
        device.last_heartbeat_at,
        { 'up' => stats[:up], 'down' => stats[:down] }
      ]
    end

    table
  end


  def pie_chart
    up = 0
    down = 0

    results.each do |device, data|
      if data.present?
        stats = statistics(data)
        up += stats[:up]
        down += stats[:down]
      end
    end

    total = up + down

    if total > 0
      up = (up / total * 100).round(1)
      down = (down / total * 100).round(1)
    else
      down = 100.0
    end

    {
      chart: {
        plotBackgroundColor:  nil,
        plotBorderWidth:      nil,
        plotShadow:           false
      },
      credits:    {enabled: false},
      exporting:  {enabled: Settings.charts.exporting},
      title:      {text: 'Availability Summary'},
      tooltip:    {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
      colors:     [Settings.colors.primary, Settings.colors.danger],
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor:           'pointer',
          dataLabels: {
            enabled:        true,
            color:          '#000000',
            connectorColor: '#000000',
            format:         '<b>{point.name}</b>: {point.percentage:.1f} %',
            distance:       20
          }
        }
      },
      series: [
        {
          type: 'pie',
          name: 'Total',
          data: [
            ['Up', up],
            ['Down', down]
          ]
        }
      ]
    }
  end

  def device_availability_for_macs(macs)
    Measurement::DeviceAvailability.get(
      dimensions: %w(state),
      range: start_range,
      granularity: 'hour',
      filters: [{ dimension: :mac,  value: macs }]
    ).values.first
  end

  def process_trend_interval(interval, data)
    interval.present? ? interval + data : data
  end

  def metrics_batch_size
    @metrics_batch_size ||= big_query_enabled? ? 1000 : begin
      case report_range
      when 'last_month'
        1
      when 'last_week'
        10
      else
        100
      end
    end
  end

  def trend_chart
    macs = results.map{ |device, data| device.mac }
    metrics = {}

    macs.each_slice(metrics_batch_size) do |batch|
      results = device_availability_for_macs(batch)
      results.each { |interval, data| metrics[interval] = process_trend_interval(metrics[interval], data) } if results.present?
    end

    if metrics.present?
      up_series = metrics.map do |k,v|
        time = k.to_i * 1000
        up = v.reduce(0){|sum, h| h['state'].to_i == 1 ? sum + h['count'] : sum }
        total = v.reduce(0){ |sum, h| sum + h['count'] }
        up = (up / total.to_f * 100 ).round(2)
        [ time, up ]
      end
    else
      up_series = []
      Rails.logger.warn "DeviceAvailabilityReport no metrics found for #{report_record.id}"
    end

    {
      chart: {
        type:         'spline',
        zoomType:     'x',
        spacingRight: 20,
        marginTop:    '60'
      },
      credits:    {enabled: false},
      exporting:  {enabled: Settings.charts.exporting},
      colors:     [Settings.colors.primary, Settings.colors.danger],
      title:      {text: "Uptime"},
      xAxis: {
        type:     'datetime',
        maxZoom:  3 * 3600000, # 1 day
        title:    {text: "Time"}
      },
      yAxis: {
        min:    0,
        max:    100,
        title:  {text: "Uptime (%)" }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.1f}%</b><br/>',
        shared: true
      },
      plotOptions: {
        spline: {
          lineWidth:  2,
          marker:     {enabled: false}
        }
      },
      series: [
        {
          name: 'Up',
          data: up_series
        }
      ]
    }
  end


  def update_report_record_stats_headers
    report_record.params.merge!(
      stats_headers: {
        'up' => 'Up (%)',
        'down' => 'Down (%)'
      }
    )
    report_record.save!
  end

  def statistics(data)

    if big_query_enabled?
      avail_data = []
      data.each_value {|v| avail_data.push(v[0]) }
      data = avail_data.map{ |group| { state: group['state'], count: group['count'] } }
    else
      data = data.values.first
      data = data.map{ |group| { state: group['state'], count: group['count'] } }
    end

    up = data.detect{ |group| group[:state].to_i == 1 }
    up = up ? up[:count] : 0

    down = data.detect{ |group| group[:state].to_i == 0 }
    down = down ? down[:count] : 0

    total = up + down
    up = (up / total.to_f * 100).round(2)
    down = (down / total.to_f * 100).round(2)

    { up: up, down: down }
  end
end
