class CellularUtilizationReport < CellularReport

  private

  def big_query_enabled?
    @big_query_enabled ||= Settings.druid_big_query_service.enabled
  end

  def batch_size
    @batch_size ||= big_query_enabled? ? 1000 : 250
  end

  def generate_data
    big_query_enabled? ? generate_data_big_query : generate_data_druid
  end

  def generate_data_druid
    report_record.devices.find_each(batch_size: 250) do |device|
      results[device] = Measurement::CellularUtilization.utilization_totals_druid(device.mac, utilization_range).map{|carrier| map_carrier(carrier) }.compact
    end
  end

  def generate_data_big_query
    report_record.devices.find_in_batches(batch_size: batch_size) do |devices|
      macs = devices.map{ |device| device.mac }
      metrics = Measurement::CellularUtilization.utilization_totals(
        range: utilization_range,
        filters: [{ dimension: :mac,  value: macs }]
      )
      results_by_mac = convert_to_hash(metrics)
      devices.each do |device|
        results[device] = results_by_mac[device.mac] ? results_by_mac[device.mac].map{|carrier| map_carrier(carrier) }.compact : {}
        # results[device] = results_by_mac[device.mac].map{|carrier| map_carrier(carrier) }.compact if results_by_mac[device.mac]
      end
    end
  end

  def convert_to_hash(metrics)
    m = {}
    metrics.each { |record| record.each {|k,v| m[k] = v} }
    m
  end

  def bytes_to_megabytes(bytes)
    (bytes * 0.000001).round(2)
  end

  def map_carrier(carrier)
    result = {}

    if carrier["values"].present?
      result[:received] = bytes_to_megabytes(carrier["values"][0]["received"]) if carrier["values"][0]["received"].present?
      result[:transmitted] = bytes_to_megabytes(carrier["values"][0]["transmitted"]) if carrier["values"][0]["transmitted"].present?
      result[:name] = carrier["tags"]["carrier"] if result.present?
    end

    result.present? ? result : nil
  end

  def utilization_range
    @utilization_range ||= case report_range
      when 'last_day'
        '24h'
      when 'last_week'
        '7d'
      when 'last_month'
        '30d'
      else
        '24h'
    end
  end

  def table
    table = []

    results.each do |device, totals|
      table << [
        device.mac,
        device.host,
        (device.category.name      if device.category),
        (device.series.name        if device.series),
        device.organization_name,
        nil,
        (device.site.name          if device.site),
        device.deployed? ? "Deployed" : "Undeployed",
        device.last_heartbeat_at,
        usage_totals(totals)
      ]
    end

    update_report_record_stats_headers
    table
  end

  def update_report_record_stats_headers
    if @carriers
      report_record.params.merge!(
        stats_headers: @carriers.inject({}) do |result, carrier|
          result["#{carrier}_received"] = "#{carrier} Received (MB)"
          result["#{carrier}_transmitted"] = "#{carrier} Transmitted (MB)"
          result
        end
      )
    end
  end

  def usage_totals(totals)
    @carriers ||= Set.new
    results = {}

    if totals
      totals.each do |total|
        results["#{total[:name]}_received"] = total[:received]
        results["#{total[:name]}_transmitted"] = total[:transmitted]
        @carriers << total[:name]
      end
    end

    results
  end
end
