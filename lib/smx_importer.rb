require 'smx_parser'
require 'smx_data_mapper'

# Public: Processes a SMX import file.
class SmxImporter
  include ::Kraken::Logger.file

  VALID_COMMUNITIES = ['armt-snmp', 'aview-snmp'].freeze

  # Public: Imports a file with the sync action.
  #
  # file_path - The String path to the import file.
  #
  # Returns a Hash of processed objects.
  def self.sync_from_file(file_path)
    import_file(file_path, :sync)
  end

  # Public: Imports a file with the append action.
  #
  # file_path - The String path to the import file.
  #
  # Returns a Hash of processed objects.
  def self.append_from_file(file_path)
    import_file(file_path, :append)
  end

  # Public: Imports a file with the update action.
  #
  # file_path - The String path to the import file.
  #
  # Returns a Hash of processed objects.
  def self.update_from_file(file_path)
    import_file(file_path, :update)
  end

  # Internal: Processes the import file.
  #
  # file_path - The String path to the import file.
  # action    - The Symbol action for how to process to import.
  #
  # Returns a Hash of processed objects.
  def self.import_file(file_path, action = :sync)

    # Ensure action is a symbol, if given.
    action = action.to_sym if action.is_a? String
    action ||= :sync

    # Process the import file
    processed = process_file(file_path, action)

    # Return the Hash of processed objects.
    processed
  end

  # Internal: Iterates over a given tab-delimited file and inserts records.
  #
  # file_path - The String path to a tab-delimited data file.
  # action    - import behavior
  #               :sync   - Remove devices if they are not in the file (default).
  #               :append - Only adds devices.
  #               :update - Only updates devices already in the database.
  #
  # Examples
  #
  #   process_file("/home/user/smx_netgate_list.tab", :append)
  #   # => { :devices => {
  #            :added   => [5,6,7],
  #            :scanned => ["00D0CF000123", "00D0CF000124", "00D0CF000125"],
  #            :removed => [],
  #            :skipped => ["00D0CF000126"]
  #          },
  #          :customers => {
  #            :added => [2,3]
  #          },
  #          :accounts => {
  #            :added => [2,3,4,5]
  #          },
  #          :sites => {
  #            :added => [2,3,4]
  #          },
  #          :locations => {
  #            :added => [1,2,3]
  #          },
  #          :configurations => {
  #            :added => [1,2,3]
  #          }
  #        }
  #
  # Returns a Hash containing the processed objects ids. This includes ids for
  #   any records that were added, and the MAC addresses of any devices skipped.
  def self.process_file(file_path, action)
    stats_hash = empty_stats_hash

    SmxParser.new(file_path).each do |smx_data|

      # Skip if the device is not updateable.
      next if action == :update && !mac_updatable?(smx_data[:device][:mac])

      data = process_data(smx_data)

      if data.nil?
        stats_hash[:devices][:skipped] << smx_data[:device][:mac]
      else
        stats_hash[:devices][:scanned] << data.device.mac

        # Iterate over the processed hash keys and check if any new objects
        # were added. If so, then add the object's ID to the processed hash.
        stats_hash.keys.each do |key|
          obj_name = key.to_s.singularize

          if data.send("new_#{obj_name}?")
            stats_hash[key][:added] << data.send(obj_name).send(:id)
          end
        end
      end
    end

    # If the action is sync, then remove any devices from the database that are
    # not in the import file.
    if action == :sync
      stats_hash[:devices][:removed] = remove_devices_unless_in stats_hash[:devices][:scanned]
    end

    stats_hash
  end



  # Internal: Checks if a device can be updated.
  #
  # mac - The String MAC address for the device.
  #
  # Examples
  #
  #   mac_updatable?("002704030201")
  #   #=> true
  #
  # Returns true if the device is updateable; otherwise false.
  def self.mac_updatable?(mac)
    Device.find_by(mac: mac).present?
  end

  # Internal: Removes devices from the database if they are not in a given list.
  #
  # device_list - An Array of device MAC addresses to keep.
  #
  # Examples
  #
  #   remove_devices_unless_in(["002704030201", "002704030202"])
  #   # => ... any devices in the database other than these two are destroyed.
  #   # => ["002704030203", "002704030204"]
  #
  # Returns an Array of MAC addresses for devices that were removed.
  def self.remove_devices_unless_in(device_mac_list)
    removed_macs      = []
    device_mac_list ||= []
    retry_limit       = 5
    mac_chunk_size    = 100

    if device_mac_list.any?

      # Get array of macs currently in the database
      current_device_mac_list = Gateway.pluck(:mac)

      # Get list of device macs that should be removed
      removed_devices_macs = current_device_mac_list - device_mac_list

      if removed_devices_macs.any?

        # Split device removal lookups into chunks. We cannot use `find_each`
        # here as it is possible it will use an `IN` statement with more than
        # 1000 entries, causing the query to fail in Oracle.
        chunked_macs = removed_devices_macs.each_slice(mac_chunk_size).to_a

        chunked_macs.each do |macs|
          Gateway.where(mac: macs).each do |device|

            retries = 0

            begin
              device.destroy
              removed_macs << device.mac
            rescue ActiveRecord::StaleObjectError

              retries += 1

              if retries <= retry_limit
                # Remove the device state, the most frequent source of state object errors.
                device.device_state.delete if device.device_state.present?

                logger.warn "SmxImporter failed to remove device '#{device.mac}' - automatically retrying after StaleObjectError (attempt = #{retries})"
                retry
              else
                logger.error "SmxImporter failed to remove device '#{device.mac}' due to StaleObjectError"
              end
            end
          end
        end
      end
    end

    removed_macs
  end

  # Internal: Processes the given data hash into business objects, ties those
  # objects together, and persists them.
  #
  # smx_data - A Hash of data parsed from the line.
  #
  # Examples
  #
  #   process_data {
  #                  :customer_name => "fancypants",
  #                  :device        => {:mac => "00C0CFFFFF", :firmware => "4.10.34"},
  #                  :details       => {},
  #                  :portal_config => {},
  #                  :snmp_config   => {},
  #                  :contact       => {},
  #                  :location      => {}
  #                }
  #   # => Creates the customer, and devices, linking them together.
  #   # => "00C0CFFFFF"
  #
  # Returns a SmxDataMapper object containing the database objects for the line
  #   data if all database transactions were successful; otherwise nil.
  def self.process_data(smx_data)
    return nil unless whitelist?(smx_data)

    begin
      data = SmxDataMapper.new(smx_data)

      # Wrap database calls in a transaction. This will rollback if any errors
      # are encountered.
      ActiveRecord::Base.transaction do

        # Get root organization and add customer to hierarchy.
        root_org = Organization.first

        unless root_org.children.include?(data.customer) || root_org == data.customer
          root_org.children << data.customer
        end

        update_device_data(data)
      end

      # Return data object
      data
    rescue => e
      logger.error "Error importing device #{smx_data}\n" \
                   "#{e.message}\n #{e.backtrace[0,3].join("\n")}\n"
      nil
    end
  end

  # Internal: Whitelists records that should be included.
  #
  # smx_data - A Hash of device data parsed from an import file line.
  #
  # Examples
  #
  #   whitelist?({:device => {:special_key => "something whitelisted"})
  #   #=> true
  #
  # Returns true if the record should be included; otherwise false.
  def self.whitelist?(smx_data)
    (smx_data[:account] =~ /\ADTREE/) == 0 ||\
    VALID_COMMUNITIES.include?(smx_data[:snmp_config][:community])
  end


  def self.update_device_data(data)
    device                = data.device
    device.host           = data.host       if device.host.blank?
    device.firmware       = data.firmware   if device.firmware.blank?
    device.hw_version     = data.hw_version if device.hw_version.blank?
    device.serial         = data.serial     if device.serial.blank? && data.serial.present?
    device.details        = data.device_details
    device.organization   = data.organization
    device.site           = data.site
    device.location       = data.location
    device.configuration  = data.configuration

    device.save!
    post_device_save_tasks(device, data)
  end

  def self.post_device_save_tasks(device, data)
    add_contact(device, data.contact)
  end

  def self.add_contact(device, contact_data)
    # Minimally require that the contact name is given
    return unless contact_data[:name].present?

    # Get contact or build for device
    contact = Contact.where(contact_data).first
    contact ||= device.contacts.new(contact_data)

    # Set token, if not already set
    contact.token ||= Contact.token(contact_data.values.join(' '))

    # Save without validations, since not all contacts will have phone numbers
    # or emails.
    contact.save(validate: false) if contact.new_record? || contact.changed?

    device.contacts << contact unless device.contacts.include?(contact)
  end

  def self.empty_stats_hash
    {
      devices: {
        added:    [],
        scanned:  [],
        removed:  [],
        skipped:  []
      },
      customers: {
        added: []
      },
      sites: {
        added: []
      },
      locations: {
        added: []
      },
      configurations: {
        added: []
      }
    }
  end


  private_class_method :process_file, :mac_updatable?, :remove_devices_unless_in,
    :process_data, :whitelist?, :update_device_data, :empty_stats_hash

end
