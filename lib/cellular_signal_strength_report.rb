class CellularSignalStrengthReport < CellularReport

  private

  # Get event right outside of start time bounds, sets the initial strength. This allows devices
  # that have a signal before the bounds to have statistics, even if they don't have any signal
  # strength events within the defined bounds.
  def initial_value(device)
    initial_strength(device)
  end


  def initial_strength(device)
    event_strength(initial_event_of_type(device, report_type_flag))
  end


  def event_strength(event)
    signal_strength = event.raw_data['modem'][report_type_flag]
  # Rescueing call to `.raw_data` on event being `nil` or raw_data without valid info.
  rescue NoMethodError => e
    signal_strength = default_value
  ensure
    return adjust_signal_strength(signal_strength)
  end


  def report_type_flag
    SIGNAL_STRENGTH_FLAG
  end


  def default_value
    MINIMUM_STRENGTH
  end


  def event_value(event)
    event_strength(event)
  end


  def update_report_record_stats_headers
    report_record.params.merge!(
      stats_headers: {
        'average_signal_strength'   => 'Average (dBm)',
        'average_signal_percentage' => 'Average (%)'
      }
    )
  end


  def table
    table = []

    results.each do |device, intervals|
      table << [
        device.mac,
        device.host,
        (device.category.name      if device.category),
        (device.series.name        if device.series),
        device.organization_name,
        nil,
        (device.site.name          if device.site),
        device.deployed? ? "Deployed" : "Undeployed",
        device.last_heartbeat_at,
        stats_to_averages(statistics(intervals))
      ]
    end

    table
  end


  def statistics(intervals)
    stats = Hash.new(0)

    intervals.each do |_, signals_info|
      signals_info.each do |signal, seconds|
        stats[signal] += seconds
      end
    end

    stats
  end


  # Converts signal strength data to averages.
  def stats_to_averages(stats)
    average_strength = signal_strength_average(stats)

    {
      'average_signal_strength'   => average_strength,
      'average_signal_percentage' => signal_strength_percentage(average_strength)
    }
  end


  def trend_chart
    {
      chart: {
        type:         'spline',
        zoomType:     'x',
        spacingRight: 20,
        marginTop:    '60'
      },
      credits:    {enabled: false},
      exporting:  {enabled: Settings.charts.exporting},
      title:      {text: "Cellular Signal Strength"},
      xAxis: {
        type:     'datetime',
        maxZoom:  3 * 3600000, # 1 day
        title:    {text: "Time"}
      },
      yAxis: {
        min:    MINIMUM_STRENGTH,
        max:    100,
        title:  {text: "Signal Strength (dBm & %)" }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.1f}</b><br/>',
        shared: true
      },
      plotOptions: {
        spline: {
          lineWidth:  2,
          marker:     {enabled: false}
        }
      },
      series: trend_chart_series
    }
  end


  # Generates series for trend chart.
  def trend_chart_series
    signal_strength_averages = {
      name: 'Average dBm',
      data: []
    }
    signal_strength_percentages = {
      name: 'Average %',
      data: []
    }

    intervals_summary_hash = summarize_results_by_interval

    intervals_summary_hash.each do |interval, signals_info|
      interval_milliseconds   = interval.to_i * 1000
      average_strength_signal = signal_strength_average(signals_info)

      signal_strength_averages[:data]     << [interval_milliseconds, average_strength_signal]
      signal_strength_percentages[:data]  << [
        interval_milliseconds,
        signal_strength_percentage(average_strength_signal)
      ]
    end

    [signal_strength_percentages, signal_strength_averages]
  end

end
