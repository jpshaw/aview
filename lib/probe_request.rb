require 'snmp4jruby' if RUBY_PLATFORM == 'java' && ENV['RAILS_ENV'] != 'test'

class ProbeRequest

  attr_accessor :config, :objects, :results
  attr_reader :device_id

  NO_AUTH_NO_PRIV = 1
  AUTH_NO_PRIV    = 2
  AUTH_PRIV       = 3

  DEFAULTS = {
    :config => {
      :version => "2c",
      :community => "public",
      :host => "10.10.10.10",
      :timeout => 15000
    },
    :objects => [".1.3.6.1.2.1.1.1.0"]
  }

  def initialize(opts = {})
    opts = DEFAULTS.merge(opts)

    @status = :initialized
    @device_id = opts[:device_id]

    self.results   = {}
    self.config    = opts[:config]
    self.objects   = opts[:objects]
  end

  def success!
    @status = :success
  end

  def success?
    @status == :success
  end

  def fail!
    @status = :fail
  end

  def started!
    @started = Time.now.utc
  end

  def finished!
    @finished = Time.now.utc
  end

  def elapsed_time
    if !@started.nil? && !@finished.nil?
     "Device: #{@device_id} finished in #{(@finished - @started).round(5)} seconds"
    else
      "@started or @finished is nil"
    end
  end

  def target_config
    {
      host:      self.config[:host],
      version:   version,
      timeout:   15000,
      community: 'public',
      oids:      self.objects
    }.merge(self.snmp_config)
  end

  protected

  def snmp_config
    case self.config[:version]
    when "3"
      snmp_v3_config
    else
      snmp_v2_config
    end
  end

  def snmp_v2_config
    {
      :community => self.config[:community]
    }
  end

  def snmp_v3_config
    {
      :username       => self.config[:username],
      :security_level => security_level,
      :auth_protocol  => auth_protocol,
      :auth_password  => self.config[:auth_password],
      :priv_protocol  => priv_protocol,
      :priv_password  => self.config[:priv_password]

    }
  end

  def version
    case self.config[:version]
    when "3"
      SNMP4JR::MP::Version3
    else
      SNMP4JR::MP::Version2c
    end
  end

  def security_level
    case self.config[:security_level]
    when NO_AUTH_NO_PRIV
      SNMP4JR::Security::SecurityLevel::NOAUTH_NOPRIV
    when AUTH_NO_PRIV
      SNMP4JR::Security::SecurityLevel::AUTH_NOPRIV
    else # AUTH_PRIV
      SNMP4JR::Security::SecurityLevel::AUTH_PRIV
    end
  end

  def auth_protocol
    case self.config[:auth_protocol]
    when "SHA"
      SNMP4JR::Security::AuthSHA::ID
    when "MD5"
      SNMP4JR::Security::AuthMD5::ID
    end
  end

  def priv_protocol
    case self.config[:priv_protocol]
    when "DES"
      SNMP4JR::Security::PrivDES::ID
    when "3DES"
      SNMP4JR::Security::Priv3DES::ID
    when "AES"
      SNMP4JR::Security::PrivAES::ID
    when "AES128"
      SNMP4JR::Security::PrivAES128::ID
    when "AES192"
      SNMP4JR::Security::PrivAES192::ID
    when "AES256"
      SNMP4JR::Security::PrivAES256::ID
    end
  end

end
