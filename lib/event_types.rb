# Event categories for filtering
module EventTypes
  include ConstantsHelper

  ALL        = -1
  STATUS     = 0
  CONFIG     = 1
  FIRMWARE   = 2
  REBOOT     = 3
  DHCP       = 4
  TUNNEL     = 5
  REMOTE     = 6
  DIALUP     = 7
  MODEM      = 8
  SMS        = 9
  LOCATION   = 10
  DATA_USAGE = 11

  def self.select_list
    [
      ["All",           ALL       ],
      ["Status",        STATUS    ],
      ["Configuration", CONFIG    ],
      ["Firmware",      FIRMWARE  ],
      ["Reboot",        REBOOT    ],
      ["DHCP",          DHCP      ],
      ["Tunnel",        TUNNEL    ],
      ["Remote",        REMOTE    ],
      ["DialUp",        DIALUP    ],
      ["Modem",         MODEM     ],
      ["SMS",           SMS       ],
      ["Location",      LOCATION  ],
      ["Data Usage",    DATA_USAGE],
    ]
  end

  # Encapsulate the event constants and present as an enumeration.
  def self.each
    return to_enum(:each) unless block_given?
    self.constants_hash.each do |key, value|
      yield(key, value)
    end
  end

end
