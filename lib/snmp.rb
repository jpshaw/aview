require 'devices/gateway'

module SNMP
  include ConstantsHelper

  # General device information.
  HARDWARE_LEVEL = "1.3.6.1.4.1.74.1.30.1.1.0"
  SOFTWARE_LEVEL = "1.3.6.1.4.1.74.1.30.1.2.0"
  WAN_INTERFACE  = "1.3.6.1.4.1.74.1.30.2.1.0"
  WAN_CONNECTION = "1.3.6.1.4.1.74.1.30.1.7.0"
  SMX_ACCOUNT    = "1.3.6.1.4.1.74.1.30.1.3.0"
  MAC_ADDRESS    = "1.3.6.1.2.1.1.5.0"


  # Device vpn tunnels.
  (1..Gateway::TUNNEL_LIMIT).each do |i|
    class_eval <<-TUNNEL_VARS
      TUNNEL_#{i}_IPSEC_IFACE   = "1.3.6.1.4.1.74.1.30.6.1.1.2.#{i}"
      TUNNEL_#{i}_DURATION      = "1.3.6.1.4.1.74.1.30.6.1.1.5.#{i}"
      TUNNEL_#{i}_ENDPOINT      = "1.3.6.1.4.1.74.1.30.6.1.1.6.#{i}"
      TUNNEL_#{i}_ENDPOINT_IPV6 = "1.3.6.1.4.1.74.1.30.6.1.1.19.#{i}"
      TUNNEL_#{i}_ENDPOINT_TYPE = "1.3.6.1.4.1.74.1.30.6.1.1.7.#{i}"
      TUNNEL_#{i}_AUTH_TYPE     = "1.3.6.1.4.1.74.1.30.6.1.1.13.#{i}"
      TUNNEL_#{i}_SERVER_MGMT   = "1.3.6.1.4.1.74.1.30.6.1.1.16.#{i}"
      TUNNEL_#{i}_INITIATOR     = "1.3.6.1.4.1.74.1.30.6.1.1.17.#{i}"
      TUNNEL_#{i}_MODE          = "1.3.6.1.4.1.74.1.30.6.1.1.18.#{i}"
      TUNNEL_#{i}_NAME          = "1.3.6.1.4.1.74.1.30.6.1.1.24.#{i}"
    TUNNEL_VARS
  end
end
