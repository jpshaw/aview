require 'kafka'
require 'kafka/datadog'
require 'kraken/logger'

module ApplicationBus
  extend self
  include ::Kraken::Logger.file

  # The list of brokers used to initialize the client. Either an Array of
  # connections, or a comma separated string of connections. A connection can
  # either be a string of "host:port" or a full URI with a scheme. If there's
  # a scheme it's ignored and only host/port are used.
  mattr_accessor(:brokers) { ['localhost:9092'] }

  # If greater than zero, the number of buffered messages that will
  # automatically trigger a delivery.
  mattr_accessor(:producer_threshold) { 1000 }

  # If greater than zero, the number of seconds between automatic
  # message deliveries.
  mattr_accessor(:producer_interval)  { 5 }

  # List of active consumers
  mattr_accessor(:consumers) { [] }

  # List of active producers
  mattr_accessor(:producers) { [] }

  def configure(&block)
    yield self
  end

  # TODO: Abstract this out to an adapter if we need to support other busses.
  def client
    @client ||= ::Kafka.new(seed_brokers: brokers, logger: logger)
  end

  # A single producer is generally more performant than a producer for each
  # implementation of ApplicationProducer.
  def producer
    @producer ||= begin
      producer = client.async_producer(
        delivery_threshold: producer_threshold,
         delivery_interval: producer_interval,
      )
      producers << producer
      producer
    end
  end

  def shutdown
    consumers.each(&:stop)
    producers.each(&:shutdown)
  end
end

require 'application_bus/base'
require 'application_bus/application_consumer'
require 'application_bus/application_producer'

at_exit do
  ApplicationBus.shutdown
end
