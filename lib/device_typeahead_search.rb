class DeviceTypeaheadSearch
  LIMIT = 10

  def initialize(user, query)
    @user = user
    @query = query
  end

  def execute
    user.devices.
      select('devices.id, devices.mac as name, devices.mac').
      where('devices.mac like ?', "%#{query}%").
      limit(LIMIT)
  end

  private

  attr_reader :user, :query
end
