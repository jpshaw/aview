class Report

  include Virtus.model

  # Hichart limitations obtained by trial and error. Although the data set might be correct
  # Highcharts fails to display the data correctly when the number of points to display goes over
  # this value.
  HIGHCHARTS_POINTS_LIMIT = 1820
  INTERVAL_SECONDS        = 3600.seconds.freeze  # 1 hour intervals

  # These attributes are used by the form in the view.
  attribute :organization_id,   Integer
  attribute :include_hierarchy, Integer
  attribute :deployment,        Integer,  default: 1
  attribute :sort,              String,   default: 'devices.last_heartbeat_at'
  attribute :direction,         String,   default: 'desc'
  attribute :range,             String,   default: 'last_day'
  attribute :report_type,       String
  attribute :models,            Array[Integer], default: []

  def initialize(report_record)
    @report_record = report_record
  end

  def title
    report_type.titleize
  end

  def report_type
    self.class.to_s.underscore =~ /(.*)_report/
    $1
  end

  def device_models
    if @categories
      DeviceModel
        .joins(:category)
        .includes(:category)
        .where(device_categories: { id: @categories.map(&:id) })
    else
      DeviceModel.all
    end
  end

  def generate!
    generate_data
    report_record.table        = table
    report_record.pie_chart    = pie_chart
    report_record.trend_chart  = trend_chart
    report_record.finished_at  = Time.now.utc
    report_record.save!
  end

  private

  #TODO: This regular expression for later use if range functionality is extended.
  # RANGE_REGEXP = /(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sept|Oct|Nov|Dec).*_[0-9]{4}/.freeze

  def report_record
    @report_record
  end

  def results
    @results ||= {}
  end

  def generate_data
    message = "Tried to generate report using directly the base Report class."
    Rails.logger.error message
    raise NotImplementedError, message
  end

  def table
  end

  def pie_chart
  end

  def trend_chart
  end

  def create_interval_template
    retval    = {}
    interval  = start_time

    begin
      # Initialize the interval hash with value of 0. This will enable us to use += without the need
      # of worrying to initialize the key first later on in `update_network_seconds`.
      retval[interval.beginning_of_hour] = Hash.new(0)
    end while (interval += interval_seconds) <= end_time
    retval
  end

  def interval_seconds
    INTERVAL_SECONDS
  end

  def grouped_events_for_device(device)
    query = yield device_events_query_for_device(device)
    query.group_by { |item| item.created_at.utc.beginning_of_hour }
  end

  def device_events_query_for_device(device)
    device.events
      .where("device_events.created_at >= ?", start_time)
      .where("device_events.created_at <= ?", end_time)
      .order("device_events.created_at asc")
  end

  def start_time
    @start_time ||= case report_range
    when 'last_day'
      24.hours.ago.utc.beginning_of_hour
    when 'last_week'
      7.days.ago.utc.beginning_of_hour
    when 'last_month'
      30.days.ago.utc.beginning_of_hour
    else
      24.hours.ago.utc.beginning_of_hour
    #TODO: This `else` for later use if range functionality is extended.
    # else
    #   if report_range =~ RANGE_REGEXP
    #     month, year = report_range.split("_")
    #     Time.new(year.to_i, Date::MONTHNAMES.index(month), 1, 0, 0, 0, 0)
    #   else
    #     24.hours.ago.utc.beginning_of_hour
    #   end
    end
  end

  def end_time
    #TODO: This `if` for later use if range functionality is extended.
    #      Replace the only line of this method with the following logic:
    #         @end_time = if report_range =~ RANGE_REGEXP
    #           month, year = report_range.split("_")
    #           month       = Date::MONTHNAMES.index(month)
    #           year        = year.to_i
    #           Time.new(year, month, Time.days_in_month(month, year), 23, 59, 59, 0)
    #         else
    #           Time.now.utc
    #         end
    @end_time ||= Time.now.utc
  end

  def report_range
    @report_range ||= report_record.params[:range]
  end

  def csv_statistics_headers
    report_record.stats_headers.values
  end

  def csv_statistics_keys
    report_record.stats_headers.keys
  end

end
