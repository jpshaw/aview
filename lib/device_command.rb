class DeviceCommand

  attr_reader :command, :command_data

  def initialize(device, data = {})
    @device       = device
    @data         = data
    @command      = data[:command]
    @command_data = data[:command_data]
    @state        = nil
    @message      = ""
  end

  def send!
    raise NotImplementedError, "The DeviceCommand class is an abstract class and should not be used directly. Please use a class that inherits from it instead."
  end

  # Public: Returns true if the state is equal to :succeeded, otherwise false.
  def succeeded?
    @state == :succeeded
  end

  # Public: Returns true if the state is equal to :failed, otherwise false.
  def failed?
    @state == :failed
  end

  # Public: Returns the resulting message if set, otherwise blank string.
  def result_message
    @message
  end

  private

  def command_succeeded!
    @state = :succeeded
  end

  def command_failed!
    @state = :failed
  end
end
