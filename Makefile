PROJECT_ID = charybdis-cloud
ENGINE     = aview
PLATFORM   = mri
IMAGE      = gcr.io/$(PROJECT_ID)/webapp/$(ENGINE)/$(PLATFORM)
VERSION    = latest

build:
	docker build \
		--file docker/aview/mri/Dockerfile \
		--build-arg GEMSTASH_TOKEN=${BUNDLE_GEMSTASH__ACCNS__COM} \
		--tag gcr.io/$(PROJECT_ID)/webapp/$(ENGINE)/mri:$(VERSION) \
		.

jruby-build:
	docker build \
		--file docker/aview/jruby/Dockerfile \
		--build-arg GEMSTASH_TOKEN=${BUNDLE_GEMSTASH__ACCNS__COM} \
		--tag gcr.io/$(PROJECT_ID)/webapp/$(ENGINE)/jruby:$(VERSION) \
		.

console:
	docker run \
		--interactive \
		--tty \
		--link webapp_mysql_1:mysql \
		--net webapp_default \
		gcr.io/$(PROJECT_ID)/webapp/$(ENGINE)/mri \
		/bin/bash --login

jruby-console:
	docker run \
		--interactive \
		--tty \
		--link webapp_mysql_1:mysql \
		--net webapp_default \
		-p 8080:8080 \
		gcr.io/$(PROJECT_ID)/webapp/$(ENGINE)/jruby \
		/bin/bash --login

db-migrate:
	docker-compose run \
		-e RAILS_ENV=production \
		console bundle exec rake db:migrate

db-seed:
	docker-compose run \
		-e RAILS_ENV=production \
		console bundle exec rake seed:migrate
