xml.instruct!
xml.response do
  xml.code 429
  xml.message 'Rate limit exceeded'
end
