# vim: set ft=ruby:

if @records && @records.length > 0 then
  json.result @records do |record|
    json.qtype   record.type
    json.qname   record.name
    json.content record.content
    json.ttl     record.ttl
  end
else
  json.result false
end
