json.metadata do
  json.pages @page_count
  json.next_page @next_page if @next_page
end

json.results do
  json.array! @results do |result|
    json.mac result[:mac]
    json.usage do
      json.array! result[:usage] do |usage|
        json.timestamp usage['timestamp']
        json.upload usage['result']['transmitted']
        json.download usage['result']['received']
      end
    end
  end
end