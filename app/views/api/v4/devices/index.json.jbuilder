json.results do
  json.metadata do
    json.pages @page_count
    json.next_page @next_page if @next_page
  end

  json.devices do
    json.array! @devices, partial: 'limited_show', as: :device
  end
end
