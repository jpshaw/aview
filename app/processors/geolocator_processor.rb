class GeolocatorProcessor < TorqueBox::Messaging::MessageProcessor
  def on_message(attrs)
    parse_attrs(attrs)
    return unless @modem
    new_cell_location = get_cellular_location
    return unless new_cell_location
    create_event(new_cell_location)
    set_new_cell_location(new_cell_location)
  end

  private

  def parse_attrs(attrs)
    @lac = attrs[:lac]
    @cid = attrs[:cid]
    @mnc = attrs[:mnc]
    @mcc = attrs[:mcc]
    @modem = DeviceModem.find_by(id: attrs[:id])
  end

  def get_cellular_location
    existing_cell_location = CellularLocation.where(lac: @lac, cid: @cid, mnc: @mnc, mcc: @mcc).first
    existing_cell_location.present? ? existing_cell_location : create_cellular_location
  end

  def create_cellular_location
    return unless Geolocator.can_get_cell_location?({lac: @lac, cid: @cid, mnc: @mnc, mcc: @mcc})
    geolocator = Geolocator.get_location({lac: @lac, cid: @cid, mnc: @mnc, mcc: @mcc})
    return unless geolocator
    CellularLocation.create(lac: @lac, cid: @cid, mcc: @mcc, mnc: @mnc,
                            lat: geolocator.latitude, lon: geolocator.longitude, accuracy: geolocator.accuracy)
  end

  def create_event(new_cell_location)
    information = "Cellular location changed from " \
                  "(#{@modem.cellular_location.try(:lat)},#{@modem.cellular_location.try(:lon)}) "\
                  "to (#{new_cell_location.lat}, #{new_cell_location.lon})"
    event_data = { info: information, type: :location, level: :notice, data: raw_data(new_cell_location),
                   uuid: @modem.device.uuid_for(:cell_location_changed) }
    @modem.device.event(event_data)
  end

  def raw_data(new_cell_location)
    { old: { latitude: @modem.cellular_location.try(:lat),
             longitude: @modem.cellular_location.try(:lon) },
      new: { latitude: new_cell_location.lat,
             longitude: new_cell_location.lon } }
  end

  def set_new_cell_location(new_cell_location)
    @modem.cellular_location = new_cell_location
    @modem.save
  end
end
