class ImportProcessor < TorqueBox::Messaging::MessageProcessor
  include ::Kraken::Logger.file

  def on_message(args)
    ops_metric_measure 'imports.elapsed' do
      import = Import.find_by(id: args[:import_id])

      if import.present?
        import.run!
      else
        logger.error "Import[#{args[:import_id]}] does not exist"
      end
    end

    ops_metric_increment 'imports.processed'

  rescue => exception
    logger.error "Import[#{args[:import_id]}] failed"
    logger.error exception
    ops_metric_increment 'imports.failed'
  end
end
