# Public: Process a bulk notification.
#
# bulk_notification - An array containing a notification subscription and the
#   notifications to send.
#
# Examples
#
#   queue = TorqueBox.fetch('/queues/bulk_notifications')
#   Notification.for_minute_of_day(minute_of_day).each do |subscription, notifications|
#     queue.publish [subscription, notifications.uniq], :encoding => :marshal
#   end
#   # => BulkNotificationProcessor.new.on_message([subscription, notifications.uniq])
#
class BulkNotificationProcessor < TorqueBox::Messaging::MessageProcessor

  def initialize(options = {})
    @logger = Rails.logger
  end

  # Public: Process a bulk notification.
  def on_message(bulk_notification)
    begin
      subscription, notifications = bulk_notification
      UserMailer.bulk_subscription_email(subscription, notifications).deliver_now
      notifications.each &:delivered!
      ops_metric_increment 'notifications.bulk.processed'

    rescue => e
      @logger.error "Bulk notification processor failed with error: #{e.message}"
      ops_metric_increment 'notifications.bulk.failed'
    end
  end
end
