class ReportProcessor < TorqueBox::Messaging::MessageProcessor
  include ::Kraken::Logger.file

  def on_message(options = {})
    @report_id = options[:device_report_id]

    if report_record
      logger.info "#{report_name} started"
      generate_report
      logger.info "#{report_name} finished"
    else
      logger.error "#{report_name} not found"
    end

  rescue => error
    logger.error "#{report_name} failed with error:"
    logger.error error
  end

  private

  def report
    report_class.new(report_record)
  end

  def report_class
    "#{report_record.report_type.tableize.classify}Report".constantize
  end

  def report_record
    @report_record ||= DeviceReport.find_by(id: @report_id)
  end

  def report_name
    @report_name ||= "Report[#{@report_id}]"
  end

  def generate_report
    ops_metric_measure 'reports.elapsed' do
      report.generate!
    end

    ops_metric_increment 'reports.processed'
  end

end
