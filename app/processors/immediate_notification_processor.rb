# Public: Process an immediate notification.
class ImmediateNotificationProcessor < TorqueBox::Messaging::MessageProcessor

  def initialize(options = {})
    @logger = Rails.logger
  end

  # Public: Process an immediate notification.
  def on_message(notification_id)
    retries = 5
    i       = 0

    begin
      notification = Notification.find(notification_id)
      UserMailer.subscription_email(notification).deliver_now
      notification.delivered!
      ops_metric_increment 'notifications.immediate.processed'

    rescue ActiveRecord::RecordNotFound
      i += 1
      if i <= retries
        @logger.warn "Couldn't find notification #{notification_id}. Retrying after #{i} seconds ..."
        sleep i
        retry
      end

    rescue => e
      @logger.error "immediate notification processor failed with error: #{e.message}"
      ops_metric_increment 'notifications.immediate.failed'
    end
  end
end
