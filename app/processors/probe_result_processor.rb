# Public: Processes probe results. If a probe is successful, this will update
# the database. If a failure is detected, the probe can be retried, or the
# device will be flagged as 'down'.
class ProbeResultProcessor < TorqueBox::Messaging::MessageProcessor
  include ::Kraken::Logger.file

  def on_message(request)
    begin
      if device = Device.find_by(id: request.device_id)

        # If the probe was successful, update the database with the results.
        if request.success?
          device.update_attribute(:probed_at, Time.now.utc)
          device.process_snmp_results(request.results)
          ops_metric_increment 'probes.processed'
        else
          ops_metric_increment 'probes.unreachable'
        end

      end # End device lookup
    rescue => e
      logger.error "Error: #{e.message}, request: #{request}"
      logger.error e
      ops_metric_increment 'probes.failed'
    end
  end
end
