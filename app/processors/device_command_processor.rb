# Public: Runs a single command for a device.
#
# Examples
#
#   device = Device.first
#   queue  = TorqueBox.fetch('/queues/device_commands')
#   data   = { device_id: device.id, command: 'status' }
#   queue.publish data
#   # => The 'status' command will be processed for the device.
#
class DeviceCommandProcessor < TorqueBox::Messaging::MessageProcessor

  def on_message(data)
    if device = Device.find_by(id: data[:device_id])
      ops_metric_measure 'commands.elapsed' do
        device.send_command(data)
      end

      ops_metric_increment 'commands.processed'
    end
  rescue => e
    Rails.logger.error "Failed to process command: #{data}. Error: #{e.message}"
    ops_metric_increment 'commands.failed'
  end

end
