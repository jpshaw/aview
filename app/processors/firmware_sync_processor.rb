require "resolv-replace.rb" 
# Internal: Syncs a device firmware with the firmware management API.
class FirmwareSyncProcessor < TorqueBox::Messaging::MessageProcessor

  include HTTParty

  # HTTParty methods
  base_uri   Settings.firmwares_api.uri
  if Settings.firmwares_api.proxy_addr.present? && Settings.firmwares_api.proxy_port.present?
    http_proxy Settings.firmwares_api.proxy_addr, Settings.firmwares_api.proxy_port
  end


  def on_message(params)
    @request = params

    if device_model
      sync_firmware
    else
      log_error "Unable to find device model with ID '#{params[:device_model_id]}'"
    end
  rescue => e
    log_error e.message
  end


  private

  attr_accessor :request


  def device_model
    @device_model ||= DeviceModel.find_by(id: request[:device_model_id])
  end


  def sync_firmware
    if firmware = device_model.device_firmwares.find_by_version(request[:firmware_version])
      firmware.update_attributes(
        production: api_firmware['production'],
        deprecated: api_firmware['deprecated']
      )

      get_remote_files(firmware)
    else
      firmware  = device_model.device_firmwares.new(
                    version:            api_firmware['firmware_version'],
                    schema_version:     api_firmware['schema_version'],
                    migration_versions: api_firmware['migration_versions'],
                    production:         api_firmware['production'],
                    deprecated:         api_firmware['deprecated']
                  )

      get_remote_files(firmware)
    end
  end


  def api_firmware
    @api_firmware ||= call_api.parsed_response['firmware']
  end


  def call_api(action = nil)
    response = self.class.get(path_for(action), {query: {model_name: device_model.try(:name)}})
    raise response.message and return unless response.code == 200
    response
  end


  def path_for(action)
    path = "/#{request[:firmware_version]}"
    path += "/#{action}" if action.present?
    path
  end


  def get_remote_files(firmware)
    firmware_file_exists = firmware.try(:firmware_file).try(:file).try(:exists?)
    schema_file_exists = firmware.try(:schema_file).try(:file).try(:exists?)

    firmware.remote_firmware_file_url = firmware_url unless firmware_file_exists
    firmware.remote_schema_file_url = schema_url if schema_file? && !schema_file_exists

    # Uploads happen when saving firmware.
    if  firmware.new_record?                        ||
        firmware.remote_firmware_file_url.present?  ||
        firmware.remote_schema_file_url.present?
      firmware.save!
    end
  end


  def firmware_url
    @firmware_url ||= url_for('firmware_url')
  end


  def schema_url
    @schema_url ||= url_for('schema_url')
  end


  def url_for(action)
    call_api(action).parsed_response['firmware'][action]
  end


  def schema_file?
    schema_url.present?
  end


  def log_error(message)
    logger.error("#{Time.now} - #{self.class.to_s}: #{message}")
  end


  def logger
    @logger ||= Rails.logger
  end

end
