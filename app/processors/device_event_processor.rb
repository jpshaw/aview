require 'json'
require 'platforms/event'

# Public: Processes individual device events into the database.
class DeviceEventProcessor < TorqueBox::Messaging::MessageProcessor
  include ::Kraken::Logger.file

  attr_reader :event

  def on_message(message)

    ops_metric_measure('events.elapsed') { process(message) }
    ops_metric_increment 'events.processed'

  rescue => error
    event_failed(error)
  end

  private

  def process(message)
    @event = Platforms::Event.from_json(message)

    invalid_event    and return unless event.valid?
    device_not_found and return unless device.present?

    device.heartbeat!
    device.process_event(event)
  end

  def device
    @device ||= Device.find_by(mac: event.mac)
  end

  def invalid_event
    logger.error "Invalid event: #{event.inspect}"
    ops_metric_increment 'events.invalid'
  end

  def device_not_found
    logger.warn "Unable to find device: #{event.mac}"
    ops_metric_increment 'events.device_not_found'
  end

  def event_failed(error)
    logger.error "Failed to process device event: #{error.message}\n#{event.inspect}"
    ops_metric_increment 'events.failed'
  end

end
