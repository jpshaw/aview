class Configuration::BaseController < ApplicationController

  # TODO: authorize user access to device & ability to modify device config
  before_action :resource

  rescue_from 'ActiveRecord::RecordNotFound' do |exception|
    head :not_found
  end

  private

  # TODO: Add support for other resource types (model/system)

  def resource
    @resource ||= Device.find_by!(mac: params[:device_id])
  end

  def authorize_modify_device_configuration!
    authorize resource, :change_configuration?
  end

end
