class Configuration::InternetRoutesController < ::Configuration::BaseController

  before_action :authorize_modify_device_configuration!, only: [:update, :destroy, :destroy_all]

  def show
    render json: serialize(configuration)
  end

  def create
    result = ::Configurations::InternetRoutes::CreateService.new(
      configuration,
      internet_route_params,
      principal_id
    ).execute

    render_result(result, result.routes.last)
  end

  def update
    result = ::Configurations::InternetRoutes::UpdateService.new(
      configuration,
      internet_route_params,
      principal_id
    ).execute

    route = result.routes.find do |internet_route|
      internet_route[:ip_address] == internet_route_params[:ip_address]
    end

    render_result(result, route || {})
  end

  def destroy
    result = ::Configurations::InternetRoutes::DestroyService.new(
      configuration,
      internet_route_params
    ).execute

    render_result(result)
  end

  def destroy_all
    result = ::Configurations::InternetRoutes::DestroyAllService.new(
      configuration
    ).execute

    render_result(result, serialize(result))
  end

  private

  def configuration
    @configuration ||= begin
      ::Configurations::InternetRoutes::FindOrCreateService.new(@resource).execute
    end
  end

  def internet_route_params
    params.require(:internet_route).permit(
      :ip_address, :subnet_mask, :description
    )
  end

  def serialize(data)
    ::Configuration::InternetRoutesSerializer.new(data).execute
  end

  def render_result(result, object = {})
    if result.errors.any?
      render json: { errors: result.errors.full_messages }
    else
      render json: object
    end
  end

end
