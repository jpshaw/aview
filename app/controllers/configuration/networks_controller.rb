class Configuration::NetworksController < ::Configuration::BaseController

  before_action :network
  before_action :authorize_modify_device_configuration!, only: :update

  def show
    render json: { network: serialize(network) }
  end

  def update
    result = ::Configurations::Networks::UpdateService.new(
      network, deserialize(network_params), principal_id
    ).execute

    if result.errors.any?
      render json: { errors: result.errors.full_messages }
    else
      render json: { network: serialize(result), errors: [] }
    end
  end

  private

  def network
    @network ||= NetworkConfiguration.find_by!(resource: @resource)
  end

  def network_params
    params.require(:network).permit!
  end

  def serialize(data)
    ::Configuration::NetworkSerializer.new(data).execute
  end

  def deserialize(data)
    ::Configuration::NetworkDeserializer.new(data).execute
  end

end
