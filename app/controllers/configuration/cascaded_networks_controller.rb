class Configuration::CascadedNetworksController < ::Configuration::BaseController

  before_action :authorize_modify_device_configuration!, only: [:update, :destroy, :destroy_all]

  def show
    render json: serialize(configuration)
  end

  def create
    result = ::Configurations::CascadedNetworks::CreateService.new(
      configuration,
      cascaded_network_params,
      principal_id
    ).execute

    render_result(result, result.cascaded_networks.last)
  end

  def update
    result = ::Configurations::CascadedNetworks::UpdateService.new(
      configuration,
      cascaded_network_params,
      principal_id
    ).execute

    network = result.cascaded_networks.find do |cascaded_network|
      cascaded_network[:ip_address] == cascaded_network_params[:ip_address]
    end

    render_result(result, network || {})
  end

  def destroy
    result = ::Configurations::CascadedNetworks::DestroyService.new(
      configuration,
      cascaded_network_params
    ).execute

    render_result(result)
  end

  def destroy_all
    result = ::Configurations::CascadedNetworks::DestroyAllService.new(
      configuration
    ).execute

    render_result(result, serialize(result))
  end

  private

  def configuration
    @configuration ||= begin
      ::Configurations::CascadedNetworks::FindOrCreateService.new(@resource).execute
    end
  end

  def cascaded_network_params
    params.require(:cascaded_network).permit(
      :ip_address,
      :subnet_mask,
      :router_ip_address,
      :description,
      :local_lan_alias,
      :internet_only_route,
      :routing_metric,
      :restrict_vpn_advertisement
    )
  end

  def serialize(data)
    ::Configuration::CascadedNetworksSerializer.new(data).execute
  end

  def render_result(result, object = {})
    if result.errors.any?
      render json: { errors: result.errors.full_messages }
    else
      render json: object
    end
  end
end
