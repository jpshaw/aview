class RolesController < ApplicationController
  respond_to :js, :json

  before_action :get_organization
  before_action :authorize_organization!
  before_action :get_role,         only: [:show, :edit, :update, :destroy]
  before_action :get_abilities,    only: [:show, :edit]
  before_action :set_view_path

  def index
    respond_to do |format|
      format.js { render partial: 'roles/index_select' }
      format.json { render json: RoleDataTable.new(view_context, @organization) }
    end
  end

  def new
    @role = @organization.roles.new
    @abilities = current_user.abilities_for(@organization)
  end

  def create
    result = CreateRole.call(organization: @organization, params: role_params, user: current_user)
    @role  = result.role
    @abilities = current_user.abilities_for(@organization) unless result.success?
  end

  def edit
  end

  def update
    result = UpdateRole.call(role: @role, params: role_params, user: current_user)
    @role  = result.role
  end

  def destroy
    @role.destroy if @role
  end

  def abilities
    @role = @organization.roles.find(params[:role_id])

    respond_to do |format|
      format.js { render partial: 'roles/abilities', locals: { abilities: @role.abilities } }
    end
  end

  protected

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/roles"
  end

  def get_organization
    @organization   = current_user.organizations.find_by(slug: params[:organization_id])
    @organization ||= current_user.organizations.find_by(id: params[:organization_id])
    if @organization.blank?
      flash[:error] = "Could not find organization '#{params[:organization_id]}'."
      redirect_to organizations_path and return
    end
  end

  def get_role
    @role = @organization.roles.find_by(id: params[:id])
    if @role.blank?
      flash[:error] = "Could not find role '#{params[:id]}'."
      redirect_to organization_roles_path(@organization)
    end
  end

  def get_abilities
    @role_abilities = @role.abilities
    @user_abilities = current_user.abilities_for(@organization)
    ability_ids     = (@role_abilities + @user_abilities).map(&:id).uniq
    @abilities      = Ability.where(id: ability_ids)
  end

  def authorize_organization!
    authorize @organization, :modify?
  end

  def role_params
    params.require(:role).permit(:name, :description, abilities_attributes: [:id])
  end
end
