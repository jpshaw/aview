class ChartsController < ApplicationController

  before_filter :authenticate_user!
  before_filter :get_device
  before_filter :base_breadcrumbs

  # Availability
  def index
    @box_title = "Availability"
    add_breadcrumb @box_title, device_charts_path(@device)
    params[:range] = 'last_week' if params[:range].nil?
    @chart_type = "availability"
  end

  def signal
    @box_title = "Signal Strength"
    add_breadcrumb @box_title, signal_device_charts_path(@device)
    params[:range] = 'last_week' if params[:range].nil?
    @chart_type = "signal_strength"
    render "index"
  end

  def network
    @box_title = "Network"
    add_breadcrumb @box_title, network_device_charts_path(@device)
    params[:range] = 'last_week' if params[:range].nil?
    @chart_type = "network"
    render "index"
  end

  def get_device
    @device = current_user.devices.find_by(mac: params[:device_id])
    redirect_to devices_path and return if @device.nil?
  end

  protected

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Devices", devices_path
    add_breadcrumb @device.mac, device_path(@device)
    add_breadcrumb "Analytics", device_charts_path(@device)
  end

end
