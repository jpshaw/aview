class AdminController < ApplicationController
  before_filter :authorize_admin_user!
  before_filter :base_breadcrumbs

  def index
    @presenter = AdminIndexPresenter.new(view_context)
  end


  def server_log
    send_data(
      File.read(Settings.server.log_file_path),
      type: 'text; charset=iso-8859-1;',
      disposition: "attachment"
    )
  rescue => e
    ExceptionNotifier.notify_exception(e, env: request.env, data: {message: e.message})
    redirect_to '/file_not_found.html', status: :not_found
  end

  def device_statistics
    respond_to do |format|
      filename = "#{params[:report_type]}-export-#{Time.now.iso8601}"
      format.csv { send_data DeviceStatisticsService.new(params[:report_type], "csv").execute, filename: "#{filename}.csv" }
      format.xlsx { send_data DeviceStatisticsService.new(params[:report_type], "xlsx").execute, filename: "#{filename}.xlsx" }
    end
  end

  protected

  def authorize_admin_user!
    authorize AdminController, :view?
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Admin Panel", admin_path
  end
end
