class DevicesController < ApplicationController

  before_filter :base_breadcrumbs,    only:   [:index, :vpn_gateway, :wifi, :legacy_cellular, :cellular, :dial_to_ip,
                                               :remote_manager, :ucpe, :show, :events]
  before_action :set_device_instance, only:   [:commands_list, :replace, :replace_with, :add_contact, :remove_contact,
                                               :remote]

  def index
    add_breadcrumb DeviceCategory::ALL
    presenter_klass = Presenters::Devices::All
    render_index(presenter_klass)
  end

  def vpn_gateway
    add_breadcrumb DeviceCategory::GATEWAY_CATEGORY
    presenter_klass = Presenters::Devices::Gateway
    render_index(presenter_klass)
  end

  def wifi
    add_breadcrumb DeviceCategory::WIFI_CATEGORY
    presenter_klass = Presenters::Devices::Netreach
    render_index(presenter_klass)
  end

  def legacy_cellular
    add_breadcrumb DeviceCategory::LEGACY_CELLULAR_CATEGORY
    presenter_klass = Presenters::Devices::Netbridge
    render_index(presenter_klass)
  end

  def cellular
    add_breadcrumb DeviceCategory::CELLULAR_CATEGORY
    presenter_klass = Presenters::Devices::Cellular
    render_index(presenter_klass)
  end

  def dial_to_ip
    add_breadcrumb DeviceCategory::DIAL_TO_IP_CATEGORY
    presenter_klass = Presenters::Devices::DialToIp
    render_index(presenter_klass)
  end

  def remote_manager
    add_breadcrumb DeviceCategory::REMOTE_MANAGER_CATEGORY
    presenter_klass = Presenters::Devices::RemoteManager
    render_index(presenter_klass)
  end

  def ucpe
    add_breadcrumb DeviceCategory::UCPE_CATEGORY
    presenter_klass = Presenters::Devices::Ucpe
    render_index(presenter_klass)
  end

  def show
    device = current_user.devices.find_by(mac: params[:id])
    redirect_with_no_device_flash and return if device.nil?

    @presenter = DevicePresenter.new(view_context, device)
    device_breadcrumbs @presenter.device
  end

  def terminal
    device = current_user.devices.find_by(mac: params[:id])
    redirect_to devices_path and return if device.nil?

    @presenter = DevicePresenter.new(view_context, device)
    device_breadcrumbs @presenter.device
    add_breadcrumb "Terminal"
  end

  def update_with_redirect(result)
    if result.success?
      flash[:success] = 'Successfully updated device.'
      redirect_to device_path(@device)
    else
      show_instance_variables
      device_breadcrumbs
      render :show
    end
  end

  def update_for_json(result)
    if result.success?
      render json: {id: @device.mac, message: "Successfully updated device."}
    else
      render nothing: true, status: :bad_request
    end
  end

  def update
    @device = current_user.devices.readonly(false).find_by(mac: params[:id])
    redirect_with_no_device_flash and return if @device.nil?

    result = UpdateDevice.call(device: @device,
                                    user: current_user,
                                  params: device_params)
    respond_to do |format|
      format.json do
        update_for_json(result)
      end
      format.html do
        update_with_redirect(result)
      end
    end
  end

  # Displays a list of events for an individual device
  def events
    respond_to do |format|
      format.html do
        current_device ? render_events_by_type(current_device) : redirect_to(devices_path)
      end
      format.js do
        save_user_pagination_limit
        @events = raw_events.paginate(page: current_page, per_page: current_user.pagination_limit(:device_events))
        @presenter = DevicesRawEventsPresenter.new(view_context, current_device, @events)
        render :raw_events
      end
    end
  end

  def events_since
    no_cache

    respond_to do |format|
      format.json do
        if current_device
          render_events_since
        else
          render json: :unprocessable_entity
        end
      end
    end
  end

  def toggle_deployment
    @device = current_user.devices.readonly(false).find_by(mac: params[:id])
    redirect_with_no_device_flash and return if @device.nil?

    authorize @device, :deploy?

    return_path = request.referer || root_path
    deployment  = @device.deployed? ? 'undeploy' : 'deploy'

    if @device.toggle_deployment
      flash[:success] = "Device successfully #{deployment}ed."
    else
      flash[:error] = "Failed to #{deployment} device."
    end

    redirect_to return_path
  end

  def replace
    authorize @device

    replace_breadcrumbs
  end

  def replace_with
    authorize @device, :replace?

    @new_device     = @device.organization.devices.new(@device.replaceable_attributes)
    @new_device.mac = params[:replacement_device][:mac].upcase

    if @new_device.save
      # transition state
      @device.update_attributes({ is_deployed: false, replaced_with_id: @new_device.id })
      flash[:notice] = "Successfully replaced unit"
      redirect_to device_path(@new_device)
    else
      replace_breadcrumbs
      render 'replace'
    end

  end

  def add_contact
    authorize @device, :modify?

    contact = contact_form

    if contact
      add_contact_to_device(contact)
    else
      flash[:error] = "Invalid Contact"
      redirect_to device_path(@device, contact: @contact.to_hash_without_token)
    end
  end

  def remove_contact
    authorize @device, :modify?

    contact = @device.contacts.where(id: params[:contact_id]).first

    if contact
      @device.contacts.delete(contact)
      # Remove Contact from Database if it is no longer linked to any devices
      contact.destroy if contact.devices.empty?
      flash[:success] = "Contact Removed From device"
    end

    redirect_to device_path(@device)
  end

  def commands_list
    authorize @device, :send_commands?

    render json: ::DeviceCommandsBuilder.new(@device, current_user).execute
  end

  def remote
    authorize @device, :send_commands?

    @device.queue_command(params[:remote_data])

    respond_to do |format|
      format.html { redirect_to remote_redirect_path(@device) }
    end
  end

  def organization_configurations
    @device       = Device.find_by(mac: params[:id])
    @organization = Organization.find_by(id: params[:organization_id])

    respond_to do |format|
      format.js
    end
  end

  def move_to_organization
    @device = Device.find_by(mac: params[:id])
    authorize @device, :change_organization?
    respond_to do |format|
      format.js
    end
  end

  def save_configuration
    @result = SaveDeviceConfiguration.call(device: current_user.devices.find_by(mac: params[:id]),
                                 group_configuration_id: params[:device_group_configuration_id],
                                 firmware_id: params[:device_configuration_device_firmware_id],
                                 grace_period_duration: params[:grace_period_duration],
                                 settings: params[:device_configuration][:description])
    respond_to do |format|
      format.js
    end
  end

  protected

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Devices", devices_path
  end

  # Internal: Creates a string category path for the given device and options.
  def category_path(device, opts = {})
    return "/" unless device.present?

    # We may encounter category names like 'Dial To IP', where we need to remove
    # white space and underscore the name.
    path = "#{device.category_name}_devices_path".gsub(/\s+/, '').underscore

    send(path, opts) rescue "/"
  end

  # Internal: Creates a string model path for the given device.
  def model_path(device)
    category_path(device, filter: { models: [device.model_id] })
  end

  private

  def set_device_instance
    @device = current_user.devices.find_by(mac: params[:id])
    redirect_with_no_device_flash and return if @device.nil?
  end

  def device_params
    params.require(:device).permit(:organization_id, :site_name, :configuration_id, :comment)
  end

  def current_device
    @current_device ||= begin
      current_user.devices.find_by(mac: params[:id] || params[:device_id])
    end
  end

  def current_page
    params[:page].to_i + 1
  end

  def events_instance_variables(device)
    current_user_instance_variables(device)
    @types  = EventTypes.select_list
    @levels = EventLevels.select_list
    @ranges = EventRanges.select_list
  end

  def current_user_instance_variables(device)
    @policy                     = Pundit.policy(current_user, device)
    @subscription               = current_user
                                    .notification_subscriptions
                                    .devices
                                    .where(publisher_id: device.id)
                                    .first
    @profiles                   = current_user.notification_profiles
    @notification_subscription  = current_user.notification_subscriptions.build
  end

  def device_breadcrumbs(device = @device)
    add_breadcrumb device.category_name, category_path(device)
    add_breadcrumb device.series_name,   model_path(device)
    add_breadcrumb device.mac,           device_path(device)
  end

  def replace_breadcrumbs
    device_breadcrumbs
    add_breadcrumb "Replace Device", replace_device_path(@device)
  end

  def events_breadcrumbs(device = @device)
    device_breadcrumbs(device)
    add_breadcrumb events_breadcrumb_title(params[:type]), path_to_events(device, params[:type])
  end

  def events_breadcrumb_title(type)
    is_raw?(type) ? 'Raw Events' : 'Events'
  end

  def path_to_events(device, type)
    is_raw?(type) ? events_device_path(device, type: :raw) : events_device_path(device)
  end

  def is_raw?(string)
    string == 'raw'
  end

  def raw_event_values(event)
    {
      id:           event.id,
      information:  event.formatted_raw_data,
      date:         timestamp_in_users_timezone(event.created_at)
    }
  end

  def contact_form
    @contact = Forms::ContactForm.new(params[:contact])

    if params[:commit] == "Add"
      @contact.retrieve_contact
    else
      @contact.save
    end
  end

  def add_contact_to_device(contact)
    @device.organization.contacts << contact

    if @device.tag_contact(contact)
      flash[:success] = "Contact Successfully Added"
      redirect_to device_path(@device)
    else
      flash[:notice] = "Contact Already Added on the Current Device"
      redirect_to device_path(@device)
    end
  end

  def raw_events
    @raw_events ||= current_device.events.order(created_at: :desc)
  end

  def remote_redirect_path(device)
    if request.referrer.present?
      request.referrer.include?('raw') ? path_to_events(device, 'raw') : path_to_events(device, nil)
    else
      events_device_path(@device)
    end
  end

  def render_events(device)
    @presenter = DevicesEventsPresenter.new(view_context, device)
  end

  def render_events_by_type(device)
    events_breadcrumbs(device)
    params[:type] == 'raw' ? render_raw_events(device) : render_events(device)
  end

  def render_index(presenter_klass)
    respond_to do |format|
      format.html {
        @presenter = presenter_klass.new(view_context)
        render 'index'
      }
    end
  end

  def render_events_since
    @events = raw_events.where('device_events.id > ?', params[:event_id]).map { |event| raw_event_values(event) }
    render json: @events.reverse.to_json
  end

  def render_raw_events(device)
    events_instance_variables(device)
    @events = raw_events.paginate(page: current_page, per_page: current_user.pagination_limit(:device_events))
    @presenter = DevicesRawEventsPresenter.new(view_context, device, @events)
    render :raw_events
  end

  def save_user_pagination_limit
    if params[:pagination_limit]
      current_user.device_events_pagination_limit = params[:pagination_limit]
      current_user.save!
    end
  end

  def redirect_with_no_device_flash
    flash[:info] = t('devices.no_device_found')
    redirect_to devices_path
  end
end
