class DeviceModelsController < ApplicationController

  before_action :set_device_model, only: :firmware_select

  def index
    respond_to do |format|
      format.html { redirect_to previous_page }
      format.json { handle_index_as_json }
    end
  end

  # GET /device_models/:device_model_id/firmware_select.html
  def firmware_select
    @firmwares = @device_model.cached_firmwares
    render partial: 'firmware_select'
  end

  private

  def set_device_model
    @device_model = DeviceModel.find(params[:id])
  end

  def handle_index_as_json
    if DeviceModelPolicy.new(current_user, DeviceModel).index?
      render json: DeviceModelsDataTable.new(view_context)
    else
      render_401
    end
  end
end