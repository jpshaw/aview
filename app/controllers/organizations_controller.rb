class OrganizationsController < ApplicationController

  before_filter :get_customers,            only: [:index, :list]
  before_filter :get_profiles,             only: [:index, :list, :show]
  before_filter :build_subscription,       only: [:index, :list, :show]
  before_filter :get_publisher_map,        only: [:index, :list]
  before_filter :modifiable_organizations, only: [:new, :create, :edit, :update]
  before_filter :get_parent_organization,  only: [:new, :create]
  before_filter :service_names,            only: [:new, :edit]
  before_filter :get_organization,         only: [:show, :edit, :update, :update_device_settings, :destroy, :actions_menu]
  before_filter :get_abilities,            only: [:new, :create]
  before_filter :get_parent_organizations, only: [:edit, :update]
  before_filter :base_breadcrumbs,         except: [:actions_menu]
  before_filter :show_breadcrumbs,         only: [:show, :edit, :update]
  before_filter :edit_breadcrumbs,         only: [:edit, :update]

  # Collection Methods

  def index
    respond_to do |format|
      format.html { @presenter = OrganizationPresenter.new(view_context, { organization: @organization }) }
    end
  end

  def list
    respond_to do |format|
      format.html { render 'list' }
    end
  end

  # Member methods

  def show
    respond_to do |format|
      format.html do
        add_breadcrumb "Devices", organization_path(@organization)
        @presenter = OrganizationDevicesPresenter.new(view_context, { organization: @organization })
        render '/devices/index'
      end
    end
  end

  def new
    respond_to do |format|
      format.html do
        authorize Organization, :new?
        @organization = Organization.new
        add_breadcrumb 'New', new_organization_path
      end
    end
  end

  def create
    respond_to do |format|
      format.html do
        unless @parent_organization.present?
          flash[:error] = 'Please select a valid parent organization.'
          redirect_to new_organization_path and return
        end

        authorize @parent_organization, :modify?

        attributes = {
          abilities: (params[:abilities] || {}),
          params:    organization_params,
          parent:    @parent_organization,
          user:      current_user
        }

        result        = OrganizationCreator.call(attributes)
        @organization = result.organization

        if result.success?
          flash[:success] = "Organization #{@organization.name} successfully created."
          redirect_to @organization
        else
          service_names
          render :new
        end
      end
    end
  end

  def edit
    respond_to do |format|
      format.html do
        authorize @organization, :modify?
        @presenter = OrganizationPresenter.new(view_context, { organization: @organization })
        @page_title = @organization.name
        @roles = @organization.roles
      end
    end
  end

  def update
    respond_to do |format|
      format.html do
        @page_title = @organization.name
        old_logo = @organization.logo.dup

        if update_organization.success?
          flash[:success] = 'Successfully updated the organization.'
          redirect_to edit_organization_path(@organization)
        else
          # file uploader is cleared on reload and cannot be set via javascript,
          # so this is to avoid misinformation to the user
          replace_old_logo(old_logo)
          service_names
          @presenter = OrganizationPresenter.new(view_context, { organization: @organization })
          render :edit
        end
      end
    end
  end

  def update_device_settings
    respond_to do |format|
      format.js do
        authorize @organization, :modify?
        @organization.update(organization_params)
      end
    end
  end

  def destroy
    respond_to do |format|
      format.html do
        authorize @organization, :destroy?

        # clear filter
        if current_user.filter_organization_id == @organization.id
          current_user.update(filter_organization_id: nil)
        end

        @organization.destroy

        if @organization.destroyed?
          flash[:success] = "Organization '#{@organization.name}' was successfully removed."
        else
          flash[:error] = "Failed to remove organization '#{@organization.name}'."
        end

        redirect_to organizations_path
      end
    end
  end

  def actions_menu
    respond_to do |format|
      format.html do
        if request.xhr?
          subscription = current_user.notification_subscriptions
                           .organizations
                           .where(publisher_id: @organization.id).first

          @presenter = OrganizationPresenter.new(view_context, { organization: @organization, subscription: subscription })

          render(partial: 'organization_action_dropdown',
                 locals: { organization: @organization, subscription: subscription },
                 layout: false)
        else
          redirect_to @organization
        end
      end
    end
  end

  def move_devices_to
    respond_to do |format|
      format.js { @result = MoveDevicesToOrganization.call(current_user: current_user,
                                                           organization_id: params[:organization_id],
                                                           device_macs: params[:device_macs],
                                                           copy_site_id: params[:copy_site_id_to_organization],
                                                           copy_data_plan: params[:copy_data_plan_to_organization]) }
    end
  end

  def search
    render json: { results: organization_search_results }, root: false
  end

  protected

  def modifiable_organizations
    @modifiable_organizations = current_user.organizations
  end

  def get_parent_organization
    @parent_organization = current_user.organizations.find_by(id: params[:parent_id])
  end

  def get_parent_organizations
    @parent_organizations = (@modifiable_organizations - [@organization]).map { |o| [o.name, o.id] }
  end

  def get_organization
    @organization = current_user.organizations.readonly(false).find_by(slug: params[:id])
    if @organization.blank?
      flash[:error] = "Could not find organization '#{params[:id]}'."
      redirect_to organizations_path and return
    end
  end

  def get_abilities
    @abilities = current_user.abilities_for(@parent_organization)
  end

  def get_customers
    if current_user.filter_organization.nil?
      @customers = current_user.head_manageable_organizations
    else
      @customers = [current_user.filter_organization].compact
    end
  end

  def get_profiles
    @profiles = current_user.notification_profiles
  end

  def build_subscription
    @notification_subscription = current_user.notification_subscriptions.build
  end

  def get_publisher_map
    @subscriptions = Hash[current_user.organization_subscriptions.map { |s| [s.publisher_id, s] }]
  end

  def base_breadcrumbs
    add_breadcrumb 'Home', root_path
    add_breadcrumb 'Organizations', organizations_path
  end

  def show_breadcrumbs
    add_breadcrumb @organization.name, organization_path(@organization)
  end

  def edit_breadcrumbs
    add_breadcrumb 'Edit', edit_organization_path(@organization)
  end

  def organization_params
    params.require(:organization).permit(
      :name, :description, :location_desc, :parent_id, :logo, :footer, :remove_logo, :resize_to_fit,
      :customer_service_id, :gateway_cell_modem_collection_enabled,
      :gateway_vlan_collection_enabled
    )
  end

  def update_organization
    authorize @organization, :modify?

    attributes = {
      organization: @organization,
      params:       organization_params,
      user:         current_user
    }

    result        = UpdateOrganization.call(attributes)
    @organization = result.organization

    result
  end

  def service_names
    @service_names = CustomerService.all_for_select
  end

  # a blank logo cannot be assigned, thus the need for this if check and remove_logo!
  def replace_old_logo(old_logo)
    if old_logo.blank?
      @organization.remove_logo!
    else
      @organization.logo = old_logo
    end
  end

  def organization_search_results
    OrganizationTypeaheadSearch.new(current_user, params[:query]).execute
  end
end
