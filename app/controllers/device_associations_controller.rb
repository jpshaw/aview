class DeviceAssociationsController < ApplicationController

  before_action :get_device
  before_action :get_association, only: [:destroy]

  def table
    respond_to do |format|
      format.html { render partial: 'table', locals: { device: @device } }
    end
  end

  def destroy
    @association.destroy if @association

    respond_to do |format|
      format.js   {}
    end
  end

  private

  def get_device
    @device = current_user.devices.find_by!(mac: params[:device_id])
  end

  def get_association
    @association = @device.device_associations.find(params[:id])
  end

end
