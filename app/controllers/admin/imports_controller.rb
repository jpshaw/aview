require 'device_importer'

class Admin::ImportsController < AdminController
  respond_to :json, :js

  before_action :authorize_imports!
  before_action :get_import, only: [:show, :destroy, :objects]
  before_action :verify_import_finished!, only: [:show, :objects]
  before_action :get_import_files, only: [:new, :create]
  before_action :set_view_path, only: [:new, :show]

  def index
    render json: ImportsDataTable.new(view_context)
  end

  def new
    action  = @import_file ? @import_file.action  : "append"
    type    = @import_file ? @import_file.type    : "accelerated"
    @import = Import.new(
                the_action:   action,
                the_type:     type,
                the_trigger:  'admin'
              )

    set_import_file if @import_file.present?
  end

  def create
    @import = Import.new(params[:import])

    set_import_file if @import.the_source_name.settings?

    if @import.save
      @import.enqueue!
    end
    redirect_to admin_path(anchor: 'imports')
  end

  def show
    @presenter = Admin::ImportShowPresenter.new(view_context, @import)
  end

  def objects
    @presenter = Admin::ImportObjectsPresenter.new(view_context, @import)
  end

  def destroy
    @import.destroy
  end

  def destroy_all
    Import.finished.destroy_all
  end

  def template

    template = CSV.generate do |csv|
      csv << DeviceImporter::KEYS
    end

    send_data template,
      type: 'text/csv; charset=iso-8859-1; header=present',
      disposition: "attachment; filename=template.csv"
  end


  private

  def authorize_imports!
    authorize Admin::ImportsController, :view?
  end

  def get_import
    @import = Import.find_by(id: params[:id])
    if @import.nil?
      flash[:error] = "Import ##{params[:id]} not found."
      redirect_to admin_imports_path and return
    end
  end

  def verify_import_finished!
    unless @import.finished?
      flash[:info] = "Import ##{@import.id} is in progress. Please wait until it is finished before viewing."
      redirect_to admin_imports_path and return
    end
  end

  # TODO: Add support for handling multiple input files.
  def get_import_files
    @import_file = Import.files.first
  end

  def set_import_file
    if ImportsHelper.import_file_exists?(@import_file)
      @import.import_file = ImportFileUploader.new
      @import.import_file.cache!(File.open(ImportsHelper.import_file_path(@import_file)))
    end
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/admin/imports"
  end
end
