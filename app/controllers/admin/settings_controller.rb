class Admin::SettingsController < AdminController
  respond_to :js

  before_filter :find_admin_settings

  def update
    params[:admin_settings].each do |key, value|
      AdminSettings[key] = value
    end
  end

  private

  def find_admin_settings
    admin_settings = AdminSettings.all
    unless admin_settings.present?
      flash[:error] = 'No admin settings could be found. Please contact Technical Support.'
      redirect_to root_path
    end
  end

end
