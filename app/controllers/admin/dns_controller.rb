class Admin::DnsController < AdminController
  respond_to :json, :js

  before_filter :get_entry,        only: [:edit, :update, :destroy]
  before_filter :get_domains,      only: [:new, :edit]
  before_filter :get_domain_types, only: [:new, :edit]
  before_filter :set_view_path, only: [:new, :edit]

  def index
    render json: DnsDataTable.new(view_context, params[:type])
  end

  def new
    @entry = entry_class.new
  end

  def create
    @entry = entry_class.new(dns_entry_params)
    @entry.save
  end

  def edit
  end

  def update
    @entry.update_attributes(dns_entry_params)
  end

  def destroy
    @entry.destroy
  end

  private

  def get_entry
    @entry = entry_class.find_by(id: params[:id])
  end

  def get_domains
    @domains = PowerDnsRecordManager::Domain.all.to_a
  end

  def get_domain_types
    @domain_types = PowerDnsRecordManager::DOMAIN_TYPES
  end

  def entry_class
    case params[:entry_type]
    when 'domain'
      PowerDnsRecordManager::Domain
    when 'soa'
      PowerDnsRecordManager::Record.soa
    when 'nameserver'
      PowerDnsRecordManager::Record.ns
    end
  end

  def dns_entry_params
    params.require(:dns_entry).permit(:type, :name, :content, :ttl, :domain_id)
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/admin/dns"
  end
end
