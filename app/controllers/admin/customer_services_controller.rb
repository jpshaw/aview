class Admin::CustomerServicesController < AdminController

  respond_to :json, :js
  before_action :find_customer_service, only: [:edit, :update, :destroy]
  before_action :set_view_path

  def index
    render json: CustomerServicesDataTable.new(view_context)
  end

  def new
  end

  def create
    @customer_service = CustomerService.new
    @customer_service.update_attributes(params[:customer_service])
  end

  def edit
  end

  def update
    @customer_service.update_attributes(params[:customer_service])
  end

  def destroy
    @customer_service.destroy
  end

  private

  def find_customer_service
    @customer_service = CustomerService.find_by(id: params[:id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/admin/customer_services"
  end
end
