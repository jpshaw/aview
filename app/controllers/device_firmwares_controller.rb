class DeviceFirmwaresController < ApplicationController
  def index
    @device_firmwares = DeviceModel.find(params[:device_model_id]).device_firmwares
    @presenter = DeviceFirmwaresPresenter.new(@device_firmwares)
    respond_to do |format|
      format.html { redirect_to previous_page }
      format.js { handle_index_as_js }
    end
  end

  def schema_file
    if firmware = DeviceFirmware.find_by(id: params[:id])
      if schema = firmware.schema_file.read
        render json: {schema: schema, status: 200, message: 'OK'}
      else
        render json: {status: 404, message: 'The JSON schema was not found. Please contact support.'}
      end
    else
      render json: {status: 404, message: 'The firmware record was not found. Please contact support.'}
    end
  rescue => e
    render json: {status: 404, message: 'The JSON schema could not be retrieved. Please contact support.'}
  end

  private

  def handle_index_as_js
    if DeviceFirmwarePolicy.new(current_user, DeviceFirmware).index?
      render :index
    else
      render_401
    end
  end
end
