class IdsAlertsController < ApplicationController
  before_action :get_device

  def index
    render json: {
      data: IDSAlert.recent_for_host(@device.mac).grouped_by_sig.map(&:row_format)
    }
  end

  private

  def get_device
    @device = current_user.devices.find_by!(mac: params[:device_id])
  end
end
