class Services::Configurations::SchemasController < ApplicationController
  def ui
    schema = ConfigurationSchemaFinder.new(params[:model], :ui).execute

    if schema
      render json: schema
    else
      render_404
    end
  end
end
