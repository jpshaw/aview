class HighchartsController < ApplicationController
  before_action :authorize_device!, only: :ids_alerts_histogram

  def cellular_utilization
    respond_to do |format|
      format.json do
        params = cellular_utilization_params.with_indifferent_access
        if params[:id].present? && !current_user.devices.find_by(mac: params[:id])
          render_401
        else
          render json: Highcharts::CellularUtilizationChart.new(params).response
        end
      end
    end
  end

  def connectivity_status_chart
    respond_to do |format|
      format.json { render json: Highcharts::ConnectivityStatusChart.new(current_user).response }
    end
  end

  def deployed_status_chart
    respond_to do |format|
      format.json { render json: Highcharts::DeployedStatusChart.new(current_user).response }
    end
  end

  def ids_alerts_histogram
    respond_to do |format|
      format.json { render json: Highcharts::IDSAlertsHistogram.new(params).response }
    end
  end

  def network_type_chart
    respond_to do |format|
      format.json { render json: Highcharts::NetworkTypeChart.new(current_user).response }
    end
  end

  def signal_strength_chart
    respond_to do |format|
      format.json { render json: Highcharts::SignalStrengthChart.new(current_user).response }
    end
  end

  def intelliflow_app_usage
    respond_to do |format|
      format.json { render json: Highcharts::IntelliflowAppUsageChart.new({ device_mac: params[:id], range: params[:range] }) }
    end
  end

  def intelliflow_app_usage_over_time
    respond_to do |format|
      format.json { render json: Highcharts::IntelliflowAppUsageOverTimeChart.new({ device_mac: params[:id],
                                                                                range: params[:range],
                                                                                apps: params[:apps],
                                                                                timezone: current_user.timezone.name }) }
    end
  end

  def wan_utilization
    respond_to do |format|
      format.json do
        params = wan_utilization_params.with_indifferent_access
        if params[:mac].present? && current_user.devices.find_by(mac: params[:mac])
          render json: Highcharts::WanUtilizationCharts.new(params).response
        else
          render_401
        end
      end
    end
  end

  private

  def cellular_utilization_params
    params.permit(:range, :id)
  end

  def authorize_device!
    current_user.devices.find_by!(mac: params[:mac])
  end

  def wan_utilization_params
    params.permit(:mac, :range)
  end
end
