require 'device_chart'

class ChartDataController < ApplicationController

  before_filter :authenticate_user!
  before_filter :get_device

  def device

    chart = DeviceChart.new(@device, params).generate || {}

    respond_to do |format|
      format.json { render :json => chart.to_json }
      format.html { render :text => chart.to_json }
    end

  end

  def get_device
    @device = current_user.devices.find_by(mac: params[:device_id])
    redirect_to devices_path and return if (@device.nil? || @device.id.nil?)
  end

end
