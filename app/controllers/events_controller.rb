
class EventsController < ApplicationController

  before_filter :base_breadcrumbs

  def index
    redirect_to root_path
    # TODO: Turn back on or remove completely.
    # respond_to do |format|
    #   format.html { @presenter = EventsIndexPresenter.new(view_context) }
    # end
  end

  def show
    @event = current_user.device_events.find(params[:id])
    add_breadcrumb "#{@event.id}", "/#{@event.id}"
  end

  protected

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Device Events", "/events"
  end

end
