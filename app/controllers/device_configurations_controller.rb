class DeviceConfigurationsController < ApplicationController

  before_action :set_paper_trail_whodunnit
  before_action :set_device_config_and_check_in_user_scope, only: [:show, :edit, :update, :destroy, :edit_linked_devices, :update_linked_devices]
  before_filter :base_breadcrumbs, except: [:exists_for_organization]
  before_filter :hidden_fields, only: [:edit_linked_devices, :update_linked_devices]

  def dial_to_ip
    authorize DeviceConfiguration, :modify?
    category = DeviceCategory.dial_to_ip.first
    render_index(category, "Dial-to-IP Device Configurations")
  end

  def remote_manager
    authorize DeviceConfiguration, :modify?
    category = DeviceCategory.remote_manager.first
    render_index(category, "Remote Manager Device Configurations")
  end

  def cellular
    authorize DeviceConfiguration, :modify?
    category = DeviceCategory.cellular.first
    render_index(category, "Cellular Device Configurations")
  end

  def ucpe
    authorize DeviceConfiguration, :modify?
    category = DeviceCategory.ucpe.first
    render_index(category, "uCPE Device Configurations")
  end


  # GET /device_configurations/1
  # GET /device_configurations/1.json
  def show
    device_configuration_type_breadcrumb(@device_configuration.category.name)
    add_breadcrumb "Show '#{@device_configuration.name}'"

    respond_to do |format|
      format.html { @devices = @device_configuration.all_associated_devices }
      format.json { render json: @device_configuration }
      format.js do
        @compatible_firmwares = @device_configuration.device_firmware.compatible_firmwares(current_user)
        @firmware_options = {}
        @compatible_firmwares.sort.reverse.each do |firmware|
          @firmware_options[firmware.version] = firmware.id
        end
      end
    end
  end


  # GET /device_configurations/new
  # GET /device_configurations/new.json
  def new
    @category             = DeviceCategory.find(params[:category_id])
    @device_configuration = DeviceConfiguration.new
    authorize DeviceConfiguration, :modify?

    @category_name  = @category.class_name.underscore
    @models         = @category.models
    @firmwares      = firmware_for(@models)

    device_configuration_type_breadcrumb(@category.name)
    add_breadcrumb "New"

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @device_configuration }
    end
  end


  # POST /device_configurations
  # POST /device_configurations.json
  def create
    @device_configuration = DeviceConfiguration.new(device_configuration_params)
    @device_configuration.compiled_settings = @device_configuration.settings
    authorize @device_configuration

    respond_to do |format|
      if @device_configuration.save
        flash[:success] = 'Device configuration was successfully created.'

        format.html { redirect_to device_configuration_path(@device_configuration) }
        format.json { render json: @device_configuration, status: :created, location: @device_configuration }
      else
        @category       = DeviceCategory.find_by(id: params[:category_id])
        @category_name  = @category.class_name.underscore
        @models         = @category.models
        @firmwares      = firmware_for(@models)

        device_configuration_type_breadcrumb(@category.name)
        add_breadcrumb "New"

        format.html { render action: "new" }
        format.json { render json: @device_configuration.errors, status: :unprocessable_entity }
      end
    end
  end


  # GET /device_configurations/1/edit
  def edit
    @category     = @device_configuration.category
    @category_id  = @category.id
    @firmwares    = @device_configuration.device_firmware.compatible_firmwares(current_user)

    device_configuration_type_breadcrumb(@category.name)
    add_breadcrumb "Edit '#{@device_configuration.name}'"
  end


  # PATCH/PUT /device_configurations/1
  # PATCH/PUT /device_configurations/1.json
  def update
    @device_configuration.assign_attributes(device_configuration_params)
    @compile_result = CompileDeviceConfiguration.call({ device_configuration: @device_configuration })

    respond_to do |format|
      if @compile_result.success?
        flash[:success] = 'Device configuration was successfully updated.'

        format.html { redirect_to device_configuration_path(@device_configuration) }
        format.json { head :no_content }
      else
        @category     = @device_configuration.category
        @category_id  = @category.id
        @firmwares    = @device_configuration.device_firmware.compatible_firmwares(current_user)

        device_configuration_type_breadcrumb(@category.name)
        add_breadcrumb "Edit '#{@device_configuration.name}'"

        format.html { render action: "edit" }
        format.json { render json: @device_configuration.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /device_configurations/1
  # DELETE /device_configurations/1.json
  def destroy
    category_name = @device_configuration.category.class_name.underscore

    @device_configuration.destroy

    respond_to do |format|
      format.html { redirect_to [category_name, :device_configurations] }
      format.js   {}
      format.json { head :no_content }
    end
  end


  # POST /device_configurations/:id/copy
  # POST /device_configurations/:id/copy.json
  def copy
    @copied_device_configuration = DeviceConfiguration.find_by(id: params[:id])
    return unless device_configuration_exists?(@copied_device_configuration, 'Unable to find the device configuration to be copied.')

    @device_configuration         = @copied_device_configuration.dup
    @device_configuration.name    = params[:name]

    authorize @device_configuration

    respond_to do |format|
      if @device_configuration.save
        flash[:success] = 'Device configuration was successfully copied.'

        format.html { redirect_to device_configuration_path(@device_configuration) }
        format.json { render json: @device_configuration, status: :created, location: @device_configuration }
      else
        flash[:error] = @device_configuration.errors.full_messages.join(', ')

        format.html { redirect_to device_configuration_path(@copied_device_configuration) }
        format.json { render json: @device_configuration.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit_linked_devices
    @devices = current_user.devices.where(model_id: @device_configuration.device_model.id,
                                   organization_id: @device_configuration.organization_id)

    @checked_lambda = lambda do |device|
      @device_configuration == device.configuration ||
        (device.configuration.present? && @device_configuration == device.configuration.parent)
    end

    device_configuration_type_breadcrumb(@device_configuration.category.name)
    add_breadcrumb "'#{@device_configuration.name}'", device_configuration_path(@device_configuration)
    add_breadcrumb "Link Devices"
  end


  def update_linked_devices
    devices = current_user.devices.where(model_id: @device_configuration.device_model.id,
                                  organization_id: @device_configuration.organization_id)

    # Reduce selection if user chooses to process only selected devices.
    if params[:commit] == 'Link Selected Devices' && params[:selected_records].present?
      device_ids  = params[:selected_records].select { |k,v| v == '1' }.keys
      devices     = devices.where(id: device_ids)
    end

    # Cannot go to the DB directly for speed, Oracle seems to have an issue with the generated SQL
    # statement, which seems to be related to the use of single quotes.
    devices.each do |device|
      device.configuration.destroy if device.configuration.present? && device.configuration.individual?
      device.update_attribute(:configuration_id, @device_configuration.id)
    end

    # TODO: Only show flash message when devices are linked. Note that simply
    # adding "devices.count" doesn't work. See AVD-201 for more information.
    flash[:success] = "Successfully linked devices"
    redirect_to(device_configuration_path(@device_configuration))
  end

  def exists_for_organization
    @organization = current_user.organizations.find(params[:organization_id])
    @configuration = @organization.device_configurations.find_by(name: params[:configuration_name]) if @organization.present?
    respond_to do |format|
      format.js { render 'shared/configuration_exists_for_organization' }
    end
  end

  private

    def render_index(category, breadcrumb)
      respond_to do |format|
        format.html do
          add_breadcrumb breadcrumb
          @presenter = DeviceConfigurationsPresenter.new(view_context, category)
          render 'index'
        end
        format.json do
          render json: DeviceConfigurationDataTable.new(view_context, category)
        end
      end
    end

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def device_configuration_params
      params
        .require(:device_configuration)
        .permit(:organization_id, :description, :compiled_settings, :name, :device_firmware_id, :grace_period_duration)
    end


    def base_breadcrumbs
      add_breadcrumb "Home", "/"
    end


    def hidden_fields
      params[:filter] = {}  unless params[:filter].present?
      params[:page]   = 1   unless params[:page].present?

      if params[:filter].is_a?(String)
        params[:filter].gsub!('=>', ':')
        params[:filter] = JSON.parse(params[:filter])
      end
    end

    def device_configuration_exists?(dc = @device_configuration, notice = 'Unable to find the requested device configuration.')
      # Most common case, it exists.
      return true if dc.present?

      # Redirect to index if it does not exist.
      redirect_to device_configurations_path, notice: notice
      return false
    end


    def device_configuration_type_breadcrumb(category_class_name)
      add_breadcrumb(
        "#{category_class_name} Device Configurations",
        __send__("#{category_class_name.parameterize.underscore}_device_configurations_path".to_sym)
      )
    end

    def firmware_for(models)
      if Pundit.policy(current_user, DeviceFirmware).assign_non_production?
        models.map { |model| model.device_firmwares }.flatten
      else
        models.map { |model| model.device_firmwares.active }.flatten
      end
    end

    def set_device_config_and_check_in_user_scope
      @device_configuration = current_user.device_configurations.find_by(id: params[:id])
      redirect_with_no_device_config_flash and return if @device_configuration.nil?
    end

    def redirect_with_no_device_config_flash
      flash[:info] = t('device_configurations.no_config_found')
      redirect_to cellular_device_configurations_path
    end
end
