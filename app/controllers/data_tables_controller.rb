class DataTablesController < ApplicationController
  require 'factories/devices_data_table_factory'

  def devices
    options = { name: params[:data_table_class], view_context: view_context, reference_id: params[:reference_id] }
    data_table = DevicesDataTableFactory.new(options)
    handle_response(data_table, 'devices')
  end

  def events
    data_table = DeviceEventsDataTable.new(view_context)
    handle_response(data_table, 'events')
  end

  def notifications
    data_table = NotificationsDataTable.new(view_context)
    handle_response(data_table, 'notifications')
  end

  def searches
    search = Search.new(current_user, params[:search])
    data_table = SearchesDataTable.new(view_context, search.devices)
    handle_response(data_table, 'searches')
  end

  def sites
    data_table = SitesDataTable.new(view_context)
    handle_response(data_table, 'sites')
  end

  def users
    data_table = UsersDataTable.new(view_context)
    handle_response(data_table, 'users')
  end

  def groups
    data_table = GroupsDataTable.new(view_context)
    handle_response(data_table, 'groups')
  end

  def group_devices
    data_table = GroupDevicesDataTable.new(view_context)
    handle_response(data_table, 'group_devices')
  end

  def intelliflow_client_usage
    respond_to do |format|
      format.json { render json: IntelliflowClientUsageDataTable.new(view_context, { device_mac: params[:deviceId],
                                                                                 range: params[:range],
                                                                                 app: params[:app]}) }
    end
  end

  private

  def handle_response(data_table, filename_prefix)
    filename = "#{filename_prefix}-export-#{Time.now.iso8601}"
    respond_to do |format|
      format.csv { send_data data_table.to_csv, filename: "#{filename}.csv" }
      format.json { render json: data_table }
      format.xlsx { send_data data_table.to_xlsx, type: 'application/xlsx', filename: "#{filename}.xlsx" }
    end
  end
end
