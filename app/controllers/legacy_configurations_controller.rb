class LegacyConfigurationsController < ApplicationController
  skip_filter   :authenticate_user!,   only: :device_config
  before_filter :authenticate_device!, only: :device_config

  before_filter :base_breadcrumbs, except: [ :exists_for_organization ]
  before_filter :get_firmware_images, only: [:show, :new, :edit, :update, :create]
  before_filter :get_configuration,   only: [:show, :edit, :update, :destroy]
  before_filter :get_version,         only: :edit

  after_filter  :verify_authorized, except: [ :index, :show, :inherited_values, :device_config,
                                              :organization_configurations, :parent_configuration, :exists_for_organization ]

  def index
    respond_to do |format|
      format.html do
        @presenter = LegacyCellularDeviceConfigurationsPresenter.new(view_context)
        render 'index'
      end
      format.json do
        render json: LegacyCellularDeviceConfigurationDataTable.new(view_context)
      end
    end
  end

  def cellular
  end

  def show
    @devices              = @configuration.devices
    @parent_configuration = @configuration.parent

    add_breadcrumb @configuration.name, legacy_configuration_path(@configuration)
  end

  def new
    authorize NetbridgeConfiguration, :modify?

    @configuration        = NetbridgeConfiguration.new
    @parent_configuration = @configuration.parent
    @parent_candidates    = parent_candidates(@configuration, @configuration.organization)

    add_breadcrumb 'New', new_legacy_configuration_path
  end

  def create
    authorize NetbridgeConfiguration, :modify?

    result = CreateCellularConfiguration.call(
      params: cellular_configuration_params,
      locks:  cellular_configuration_locks,
      user:   current_user
    )

    @configuration = result.configuration

    if result.success?
      flash[:success] = 'Successfully created configuration.'
      redirect_to legacy_configuration_path(@configuration)
    else
      flash[:error]         = 'Error creating configuration, please check information and try again.'
      @parent_configuration = @configuration.parent
      @parent_candidates    = parent_candidates(@configuration, @configuration.organization)

      add_breadcrumb 'New', new_legacy_configuration_path
      render :new
    end
  end

  def edit
    @parent_configuration = @configuration.parent
    @parent_candidates    = parent_candidates(@configuration, @configuration.parent.try(:organization))

    add_breadcrumb @configuration.name, legacy_configuration_path(@configuration)
    add_breadcrumb "Edit", edit_legacy_configuration_path(@configuration)
    if @breadcrumb_addition
      add_breadcrumb @breadcrumb_addition, edit_legacy_configuration_path(@configuration)
    end
  end

  def update
    result = UpdateCellularConfiguration.call(
      configuration: @configuration,
      params:        cellular_configuration_params,
      locks:         cellular_configuration_locks,
      user:          current_user
    )

    @configuration = result.configuration

    if result.success?
      flash[:success] = 'Successfully updated the configuration.'
      redirect_to legacy_configuration_path(@configuration)
    else
      flash[:error]         = 'Error updating configuration, please check changes and try again.'
      @parent_configuration = @configuration.parent
      @parent_candidates    = parent_candidates(@configuration, @configuration.parent.try(:organization))

      add_breadcrumb @configuration.name, legacy_configuration_path(@configuration)
      add_breadcrumb 'Edit', edit_legacy_configuration_path(@configuration)
      render :edit
    end
  end

  def destroy
    @configuration.destroy

    respond_to do |format|
      format.html {
        if @configuration.destroyed?
          flash[:success] = 'Successfully deleted the configuration.'
          redirect_to legacy_configurations_path
        else
          flash[:error] = 'There was an error deleting the configuration, please ' \
                          'make sure that your configuration does not have any devices ' \
                          'linked to it before attempting to delete it.'
          redirect_to legacy_configuration_path(@configuration)
        end
      }
      format.js {}
    end
  end


  # POST /configurations/:id/copy
  def copy
    authorize NetbridgeConfiguration, :modify?

    @copied_configuration = NetbridgeConfiguration.find_by(id: params[:id])
    @configuration        = @copied_configuration.dup
    @configuration.name   = params[:name]

    if @configuration.save
      flash[:success] = 'Configuration was successfully copied.'
      redirect_to legacy_configuration_path(@configuration)
    else
      flash[:error] = @configuration.errors.full_messages.join(', ')
      redirect_to legacy_configuration_path(@copied_configuration)
    end
  end

  # Public: Ajax action used to retrieve a list of inherited values based on
  # the passed parent configuration.
  #
  # Returns a json hash of values if configuration exists, otherwise 404 error.
  def inherited_values
    no_cache
    render nothing: true and return if params[:parent_id].blank?

    configuration = current_user.netbridge_configurations.find(params[:parent_id])
    @inherited_values = configuration.config_values_with_inherited_for_form

    render json: @inherited_values
  end

  # Renders the requested device configuration, if found.
  #
  # GET /:id.mac?subdomain=configuration.accns.com
  # GET /configurations/:id/device_config
  #
  # Note: SX models don't support SSL client certificates, so we don't validate
  # access to their configurations. This means that all SX configurations
  # are visible to anyone who has the mac address.
  #
  def device_config
    configuration = NetbridgeConfiguration.find_by_device_mac(params[:id])

    if configuration
      render text: configuration.to_keyval
    else
      head :not_found
    end
  end

  def organization_configurations
    # The id parameter will be empty when creating a new configuration. Need to account for it.
    configuration         = NetbridgeConfiguration.find_by(id: params[:id]) || NetbridgeConfiguration.new
    organization          = Organization.find_by(id: params[:organization_id])
    @parent_configuration = configuration.parent
    @parent_candidates    = parent_candidates(configuration, organization)
  end


  def parent_configuration
    @parent_configuration = NetbridgeConfiguration.find_by(id: params[:configuration_id])
  end

  def exists_for_organization
    @organization = current_user.organizations.find(params[:organization_id])
    @configuration = @organization.netbridge_configurations.find_by(name: params[:configuration_name]) if @organization.present?
    respond_to do |format|
      format.js { render 'shared/configuration_exists_for_organization' }
    end
  end

  protected

  def copy_config(source, dest)
    dest.assign_attributes(source.
                           attributes.
                           symbolize_keys.
                           slice(*NetbridgeConfiguration::CONFIG_KEYS,
                                 :organization_id, :parent_id, :name, :key_meta))
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "#{DeviceCategory::LEGACY_CELLULAR_CATEGORY} Configurations", legacy_configurations_path
  end

  def get_configuration
    @configuration = current_user.netbridge_configurations.readonly(false).find(params[:id])
    authorize @configuration, :modify?
  end

  def get_version
    if params[:as_of]
      timestamp = DateTime.parse(params[:as_of])
      copy_config(@configuration.version_at(timestamp), @configuration)
      @version = @configuration.version

      if parent = @configuration.parent
        @parent_was = parent.version_at(:as_of)
      end

      @breadcrumb_addition = "Version from #{params[:as_of]}"
    elsif params[:version]
      @version = @configuration.versions[params[:version].to_i]
      copy_config(@version.reify, @configuration)

      if parent = @configuration.parent
        @parent_was = parent.version_at(@version.created_at)
      end

      @breadcrumb_addition = "Version from #{@version.created_at}"
    end
  end

  def get_firmware_images
    @img_list   = cellular_firmware_versions_for(Netbridge::SX_MODEL)
    @img_2_list = cellular_firmware_versions_for(Netbridge::FX_6200_MODEL)
  end

  def cellular_configuration_params
    params.require(:configuration).permit(:name, :parent_id, :organization_id, *NetbridgeConfiguration::CONFIG_KEYS)
  end

  def cellular_configuration_locks
    params.require(:configuration).permit(*NetbridgeConfiguration::CONFIG_KEYS.map { |k| "#{k}_locked" } )
  end

  private

  def authenticate_device!
    ::LegacyDeviceAuthenticationService.new(params[:id], request.headers.to_h).execute
  end

  def parent_candidates(configuration, organization)
    # Organization could be nil.
    return [] unless organization.present?

    # Return organization's configurations that are not descendants of the current configuration or
    # the current configuration itself (cannot be its own parent).
    organization.netbridge_configurations - configuration.descendants - [configuration]
  end


  def cellular_firmware_versions_for(model_name)
    DeviceModel.find_by(name: model_name).device_firmwares.active.by_latest.map(&:version).unshift("&nbsp;".html_safe)
  end

end
