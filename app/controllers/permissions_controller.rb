class PermissionsController < ApplicationController
  respond_to :js, :json

  before_action :get_manager
  before_action :authorize_manager!
  before_action :base_breadcrumbs
  before_action :get_permission, only: [:edit, :update, :destroy]
  before_action :get_manageable_organizations, only: :new
  before_action :get_manageable_organization, only: [:edit]
  before_action :get_roles, only: [:new, :edit]
  before_action :get_abilities, only: :new
  before_action :show_breadcrumbs, only: [:edit, :update]

  helper_method :manager_class_pluralized_name, :manager_class_name

  def index
    render json: PermissionDataTable.new(view_context, @manager)
  end

  def new
    @abilities  = @manageable_organizations.first.try(:roles).try(:first).try(:abilities) || []
    @permission = Permission.new
  end

  def create
    attributes = {
      params:  permission_params,
      user:    current_user,
      manager: @manager
    }

    result      = CreatePermission.call(attributes)
    @permission = result.permission

    unless result.success?
      @permission.errors[:base] << result.message if result.message.present?
    end
  end

  def edit
  end

  def update
    attributes = {
      params:     permission_params,
      permission: @permission
    }

    result      = UpdatePermission.call(attributes)
    @permission = result.permission

    unless result.success?
      @permission.errors[:base] << result.message if result.message.present?
    end
  end

  def destroy
    @permission.destroy if @permission
  end

  protected

  def get_manager
    if params[:user_id]
      @manager = current_user.users.find_by(id: params[:user_id])
    elsif params[:organization_id]
      @manager = current_user.organizations.find_by(slug: params[:organization_id])
    end

    if @manager.blank?
      flash[:error] = "Could not find manager."
      redirect_to root_path and return
    end
  end

  def get_manageable_organizations
    @manageable_organizations = \
      if @manager.respond_to? :organization
        @manager.organization.organizations
      else
        current_user.organizations
      end
  end

  def get_roles
    if @manager.respond_to? :organization
      @roles = @manager.organization.roles
    else
      if @manageable_organizations.present? && @manageable_organizations.any?
        @roles = @manageable_organizations.first.roles
      elsif @manageable_organization.present?
        @roles = @manageable_organization.roles
      end
    end

    @roles ||= []

    @roles.collect! { |r| [r.name, r.id] }
  end

  def get_permission
    @permission = @manager.manager_permissions.find_by(id: params[:id])
    if @permission.blank?
      flash[:error] = "Unable to find permission '#{params[:id]}'."
      redirect_to [@manager, :permissions] and return
    end
  end

  def get_manageable_organization
    @manageable_organization = @permission.manageable
  end

  def get_abilities
    abilities_params = \
      if @manager.present? && @manageable_organizations.present? && @roles.present?
        {
          manager_id:      @manager.id,
          manager_type:    @manager.class.to_s,
          manageable_id:   @manageable_organizations.first.id,
          manageable_type: @manageable_organizations.first.class.to_s,
          role_id:         @roles.first[1]
        }
      else
        {}
      end

    @abilities = GetAbilities.call(user: current_user, params: abilities_params).abilities
  end

  def authorize_manager!
    authorize @manager, :modify?
  end

  def base_breadcrumbs
    add_breadcrumb 'Home', root_path
    add_breadcrumb manager_class_pluralized_name, url_for([manager_class_pluralized_name.downcase])

    if @manager.respond_to? :organization
      add_breadcrumb @manager.organization.name,
          url_for(
              controller: manager_class_pluralized_name.downcase,
              params: { filter: { organization_id: @manager.organization_id } }
          )
    end

    add_breadcrumb @manager.name, url_for([@manager])
    add_breadcrumb 'Permissions', url_for([@manager, :permissions])
  end

  def show_breadcrumbs
    add_breadcrumb @permission.id, '#'
  end

  def permission_params
    params.require(:permission).permit(:id, :manager_id, :manager_type,
      :manageable_id, :manageable_type, :role_id)
  end

  def manager_class_pluralized_name
    @manager_class_pluralized_name ||= @manager.class.to_s.pluralize
  end

  def manager_class_name
    @manager_class_name ||= @manager.class.to_s.downcase
  end
end
