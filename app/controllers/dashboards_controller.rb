class DashboardsController < ApplicationController
  add_breadcrumb "Home", "/"
  respond_to :json

  def show
    respond_to do |format|
      format.html { @presenter = DashboardPresenter.new view_context }
      format.json { raise ActionController::UnknownFormat }
    end
  end
end
