class DataPlansController < ApplicationController
  respond_to :js, :json

  before_action :get_organization, except: [:exists_for_organization]
  before_action :authorize_organization!, except: [:exists_for_organization]
  before_action :set_view_path, except: [:exists_for_organization]
  before_action :find_data_plan, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.js do
        render :index_for_select
      end
      filename = "#{params[:organization_id]}-export-#{Time.now.iso8601}"
      format.json { render json: DataPlansDataTable.new(view_context, params[:organization_id]) }
      format.csv { send_data DataPlansDataTable.new(view_context, params[:organization_id]).to_csv, filename: "#{filename}.csv" }
      format.xlsx { send_data DataPlansDataTable.new(view_context, params[:organization_id]).to_xlsx, filename: "#{filename}.xlsx" }
    end
  end

  def show
    set_show_breadcrumbs
    @presenter = DataPlanShowPresenter.new(view_context, @data_plan)
    render '/devices/index'
  end

  def new
    @data_plan = DataPlan.new
    @data_plan.cycle_type = DataPlan.cycle_types[:monthly]
    @data_plan.data_limit = DataPlan::DEFAULT_DATA_LIMIT_IN_MB
  end

  def create
    convert_data_limits_to_bytes
    @data_plan = DataPlan.new(data_plan_params)
    @data_plan.organization = @organization
    @data_plan.save
  end

  def edit
  end

  def update
    if @data_plan
      convert_data_limits_to_bytes
      @data_plan.update_attributes(data_plan_params)
    end
  end

  def destroy
    @data_plan.destroy if @data_plan
  end

  def move_devices_to
    @result = MoveDevicesToDataPlan.call(data_plan_id: params[:data_plan_id], device_macs: params[:device_macs])
  end

  def exists_for_organization
    @organization = current_user.organizations.find(params[:organization_id])
    @data_plan = @organization.data_plans.find_by(name: params[:data_plan_name]) if @organization.present?
    respond_to do |format|
      format.js
    end
  end

  protected

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/data_plans"
  end

  def get_organization
    @organization   = current_user.organizations.find_by(slug: params[:organization_id])
    @organization ||= current_user.organizations.find_by(id: params[:organization_id])
    if @organization.blank?
      flash[:error] = "Could not find organization '#{params[:organization_id]}'."
      redirect_to organizations_path and return
    end
  end

  def authorize_organization!
    authorize @organization, :modify?
  end

  def find_data_plan
    @data_plan = DataPlan.find(params[:id])
  end

  def data_plan_params
    params.require(:data_plan).permit(:name, :cycle_type, :cycle_start_day, :data_limit, :individual_data_limit,
                                      :organization, :notify_enabled)
  end

  def set_show_breadcrumbs
    add_breadcrumb 'Home', root_path
    add_breadcrumb @organization.name, organization_path(@organization)
    add_breadcrumb 'Data Plans', edit_organization_path(@organization, anchor: 'data-plans')
    add_breadcrumb @data_plan.name, organization_data_plan_path(@organization, @data_plan)
  end

  def convert_data_limits_to_bytes
    params[:data_plan][:data_limit] = params[:data_plan][:data_limit].to_f * 1000000
    if params[:data_plan][:individual_data_limit].present?
      params[:data_plan][:individual_data_limit] = params[:data_plan][:individual_data_limit].to_f * 1000000
    end
  end
end
