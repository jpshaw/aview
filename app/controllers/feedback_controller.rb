class FeedbackController < ApplicationController
  before_action :feedback_params

  def create
    respond_to do |format|
      format.json do
        ::FeedbackMailService.new(current_user, params[:last_url], params[:category], params[:domain], params[:message]).execute
        render json: {}, status: :ok
      end
    end
  end

  private
  def feedback_params
    params.require(:feedback).permit(:last_url, :category, :domain, :message)
  end
end