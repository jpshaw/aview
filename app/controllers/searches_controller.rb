class SearchesController < ApplicationController

  before_filter :base_breadcrumbs

  def new
    add_breadcrumb 'Advanced Search', "#"
  end

  def index
    @search = Search.new(current_user, params[:search])
    respond_to do |format|
      format.html { respond_to_index_html }
    end
  end

  protected

  def respond_to_index_html
    if @search.valid?
      @presenter = SearchesIndexPresenter.new(view_context, @search)
      add_breadcrumb @search.description, "#"
    else
      flash.now[:error] = @search.errors.full_messages.join
      render :new
    end
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
  end

end
