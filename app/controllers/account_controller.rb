class AccountController < ApplicationController
  before_filter :authenticate_user!
  before_filter :users_enabled, :only => [:change_password, :update_password]
  before_filter :base_breadcrumbs, :except => [:change]

  def index
    @user = current_user
    @timezones = Timezone.all
    @presenter = AccountPresenter.new(view_context)
  end

  def update
    @user = current_user
    if @user.update_attributes(params[:user])
       flash[:success] = 'Account successfully updated.'
       redirect_to account_index_path
    else
      @timezones = Timezone.all
      render :index
    end
  end

  def api
    @user = current_user
    @user.reset_authentication_token! if @user.authentication_token.blank?
  end

  def generate_api_token
    @user = current_user
    @user.reset_authentication_token!
    flash[:success] = 'Successfully generated a new API token.'
    redirect_to api_account_index_path
  end

  def change_password
    @user = current_user
    add_breadcrumb "Change Password", change_password_account_index_path
  end

  def update_password
    @user = current_user

    if @user.update_with_password(params[:user])
      sign_in @user, :bypass => true
      flash[:success] = "Successfully updated password."
      redirect_to account_index_path
    else
      add_breadcrumb "Change Password", change_password_account_index_path
      render :change_password
    end
  end

  def change
    return_path = request.referer || root_path
    redirect_to return_path and return unless params[:user].is_a?(Hash)

    params[:user].each do |key, value|
      case key
      when "filter_organization_id"
        filter_organization_id = "#{value}".to_i
        if organization = current_user.all_organizations.find_by(id: filter_organization_id)
          current_user.filter_organization_id = organization.id
          flash[:info] = "Now viewing #{organization.name}"
        else
          current_user.filter_organization_id = nil
        end
        return_path = uri_without_params(return_path)
      when /pagination_limit/
        current_user.send("#{key}=", value)
        return_path = uri_without_params(return_path, :page)
      end
    end

    current_user.save
    redirect_to return_path
  end

  protected

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Account", account_index_path
  end
end
