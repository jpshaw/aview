class VvigPncController < ApplicationController
  before_action :filter_query_params, only: [:get_data]

  def index
  end

  def get_data
    render json: ::VvigPncUtilizationService.new(params[:organization_ids], params[:vlans], params[:vigs], params[:start_ts], params[:end_ts], params[:format]).execute
  end

  private

  def filter_query_params
    params.permit(:organization_ids, :vlans, :vigs, :start_ts, :end_ts, :format)
  end
end