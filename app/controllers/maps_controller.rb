class MapsController < ApplicationController
  before_filter :maps_enabled?
  before_filter :base_breadcrumbs, :only => [:index]

  def index
    respond_to do |format|
      format.html { @presenter = MapPresenter.new(view_context) }
      format.js do
        @filter = params[:filter] || {}
        render :refresh
      end
    end
  end

  def pins
    no_cache

    results = ::Devices::MapPinsService.new(
      current_user,
      filter_params,
      include_cellular: true
    ).execute

    render json: results.to_json
  end

  def alert_pins
    no_cache

    results = ::Devices::MapPinsService.new(
      current_user,
      filter_params.merge(
        status: DeviceState::States::DOWN,
        deployment: Devices::Deployment::DEPLOYED
      )
    ).execute

    render json: results.to_json
  end

  protected

  def filter_params
    params[:filter] || {}
  end

  def maps_enabled?
    unless current_user.maps_enabled?
      flash[:error] = "Maps are disabled, please re-enable maps in your account settings if you wish to view them"
      redirect_to root_path and return
    end
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Maps", maps_path
  end
end
