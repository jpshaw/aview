class GroupsController < ApplicationController

  before_filter :base_breadcrumbs
  before_action :set_group, except: [:index, :new, :create, :destroy_multiple]

  # GET /groups
  def index
    authorize Group, :modify?
    @presenter = GroupsPresenter.new(view_context)
  end

  # GET /groups/new.js
  def new
    @group = Group.new
    authorize Group, :modify?

    respond_to do |format|
      format.js {}
    end
  end

  # POST /groups
  # POST /groups.json
  # POST /groups.js
  def create
    @group = Group.new(group_params)
    authorize @group, :modify?

    respond_to do |format|
      if @group.save
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
        format.js   {}
      else
        format.html { redirect_to groups_path, error: 'Group was not created.' }
        format.json { render json: @group.errors, status: :unprocessable_entity }
        format.js   { render partial: 'modal_errors' }
      end
    end
  end

  # GET /groups/1
  def show
    @presenter = GroupDevicesPresenter.new(view_context)
    add_breadcrumb @group.name
  end

  # GET /groups/1/group_stats.html
  def group_widgets
    @presenter = GroupDevicesPresenter.new(view_context)
    render partial: 'group_widgets'
  end

  # GET /groups/1/configuration
  def configuration
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_path, notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
      format.js   {}
    end
  end

  # GET /groups/1/device_search.json?query=002704
  def device_search
    # Only search for devices with model
    @devices = @group.organization.devices.not_in_group(@group.id)
                     .select(:id, :mac)
                     .where('devices.mac like ?', "%#{params[:query]}%")
                     .where(model_id: @group.device_model_id)
                     .limit(10)

    respond_to do |format|
      format.json { render json: { results: @devices }, root: false }
    end
  end

  def add_devices
    @devices = @group.organization.devices.where(id: params[:device_ids],
                                           model_id: @group.device_model_id)
    @devices.update_all(group_id: @group.id)
  end

  def remove_devices
    @devices = @group.devices.where(id: params[:device_ids])
    @count = @devices.count
    @devices.update_all(group_id: nil)
  end

  # GET /groups/1/edit_firmware.js
  def edit_firmware
    include_beta = ::Pundit.policy(current_user, DeviceFirmware).assign_non_production?
    @firmwares = @group.firmware.self_and_parents(include_beta)
  end

  # POST /groups/1/update_firmware.js
  def update_firmware
    @group.assign_attributes(group_params)

    respond_to do |format|
      if @group.save
        format.js {}
      else
        format.js { render partial: 'modal_errors' }
      end
    end
  end

  # POST /groups/destroy_multiple
  def destroy_multiple
    authorize Group, :modify?

    @groups = current_user.groups.where(id: params[:group_ids])
    @group_names = @groups.map(&:name)
    @groups.destroy_all

    respond_to do |format|
      format.html { redirect_to groups_path, notice: 'Groups were successfully destroyed.' }
      format.json { head :no_content }
      format.js   {}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = current_user.groups.find(params[:id])
    authorize @group, :modify?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params[:group].permit(:name, :organization_id, :device_model_id, :firmware_id)
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Groups", '/groups'
  end
end
