class NotificationsController < ApplicationController

  before_filter :base_breadcrumbs

  def index
    respond_to do |format|
      format.html do
        @presenter = NotificationPresenter.new(view_context)
        render 'events/index'
      end
    end
  end

  protected

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Notifications", "/notifications"
  end

end
