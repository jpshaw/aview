class ModemsController < ApplicationController

  before_filter :authenticate_user!
  before_filter :device
  before_filter :modem

  def destroy
    @modem.destroy
  end

  private

  def device
    @device = current_user.devices.find_by!(mac: params[:device_id])
  end

  def modem
    @modem = @device.modems.find(params[:id])
  end
end
