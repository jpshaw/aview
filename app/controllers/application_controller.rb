require 'addressable/uri'
require 'forms'

class ApplicationController < ActionController::Base
  before_filter :authenticate_user!

  # Enable CSRF protection
  protect_from_forgery with: :exception

  include Pundit
  include TimezoneHelper
  include Operations::Metrics::Helpers

  helper_method :sort_column, :sort_direction, :timestamp_in_users_timezone

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActionController::InvalidCrossOriginRequest, with: :invalid_cross_origin_request
  rescue_from ActionController::UnknownFormat, with: :unknown_format
  rescue_from ActionView::MissingTemplate, with: :missing_template
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActionController::InvalidAuthenticityToken, with: :invalid_authenticity_token
  rescue_from Aview::Device::NotAuthorizedError do |exception|
    render_403
  end

  if RUBY_PLATFORM == 'java'
    rescue_from 'Java::JavaLang::IllegalStateException', with: :illegal_state_exception
  end

  def no_cache
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  protected

  def principal_id
    current_user.username
  end

  def cache_principal_id
    ::RequestStore.store[:principal_id] = principal_id
  end

  def unathorized_access
    flash[:error] = "Unknown Action"
    redirect_to root_path and return
  end

  def sort_column
    params[:sort] || "created_at"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def users_enabled
    unless Settings.users.enabled
      flash[:error] = "Unknown Action"
      redirect_to root_path
    end
  end

  def prevent_double_render_error
    self.response_body = nil
  end

  def previous_page
    request.referrer || dashboard_path
  end

  def render_200
    respond_to do |format|
      format.json { render json: {}, status: 200 }
    end
  end

  def render_400
    prevent_double_render_error
    respond_to do |format|
      format.any { render nothing: true, status: 400 }
    end
  end
  alias_method :bad_request, :render_400

  def render_401
    respond_to do |format|
      format.html { render template: '/errors/unauthorized', status: :unauthorized }
      format.any  { head :unauthorized }
    end
  end

  def render_403
    head :forbidden
  end

  def render_404
    prevent_double_render_error
    respond_to do |format|
      format.html { render template: '/errors/not_found', status: :not_found }
      format.any  { head :not_found }
    end
  end

  def render_422
    respond_to do |format|
      format.any { render nothing: true, status: 422 }
    end
  end
  alias_method :unprocessable_entity, :render_422

  def render_429
    respond_to do |format|
      format.xml  { render status: 429, template: '/statuses/429' }
      format.json { render status: 429, template: '/statuses/429' }
    end
  end
  alias_method :render_max_request_status, :render_429

  def record_not_found(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.record_not_found'
      render_404
    end
  end

  def invalid_authenticity_token(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.invalid_authenticity_token'
      render_422
    end
  end

  def illegal_state_exception(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.illegal_state_exception'
      render_401
    end
  end

  def missing_template(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.missing_template'
      render_404
    end
  end

  def unknown_format(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.unknown_format'
      render_404
    end
  end

  def invalid_cross_origin_request(exception)
    with_exception_handler(exception) do
      ops_metric_increment 'requests.invalid_cross_origin_request'
      render_400
    end
  end

  # Internal: Strips params from a given URI. If specific params are given, it
  # will only strip those; otherwise all params are removed.
  #
  # uri              - The URI String.
  # params_to_remove - An Array of params to remove.
  #
  # Returns a String containing the URI.
  def uri_without_params(uri, *params_to_remove)
    return root_path if uri.nil?

    uri = Addressable::URI.parse(uri)
    uri_params = uri.query_values

    if uri_params.present?
      if params_to_remove.empty?
        uri.query_values = {}
      else
        uri_params = uri_params.with_indifferent_access
        params_to_remove.each { |p| uri_params.delete(p) }
        uri.query_values = uri_params
      end
    end

    uri.to_s
  end

  private

  def user_not_authorized
    ops_metric_increment 'requests.not_authorized'

    respond_to do |format|
      format.html do
        flash[:error] = "You are not authorized to perform this action."
        redirect_to root_path
      end
      format.json { head :unauthorized }
      format.js   { head :unauthorized }
      format.xml  { head :unauthorized }
    end
  end

  def with_exception_handler(exception)
    if Rails.application.config.consider_all_requests_local
      raise exception
    else
      yield
    end
  end
end
