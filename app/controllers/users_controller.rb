class UsersController < ApplicationController

  before_filter :authenticate_user!
  before_filter :users_enabled
  before_filter :base_breadcrumbs,    except: :destroy
  before_filter :timezones,           only:   [:show]
  before_filter :get_user,            only:   [:show, :permissions, :destroy]
  before_filter :redirect_to_account, only:   [:show, :permissions, :destroy]

  def index
    authorize Organization, :modify_users?
    @presenter = UsersPresenter.new(view_context)
  end

  # TODO: Move methods from api/v2/users_controller.rb to here.

  protected

  def get_user
    @user = current_user.users.readonly(false).find_by(id: params[:id])
    if @user.blank?
      flash[:error] = "Unable to find user with id: '#{params[:id]}'."
      redirect_to users_path
    end
  end

  def base_breadcrumbs
    add_breadcrumb 'Home', '/'
    add_breadcrumb 'Users', users_path
  end

  def show_breadcrumbs
    add_breadcrumb @user.organization.name,
      users_path(filter: { organization_id: @user.organization_id })
    add_breadcrumb @user.name_or_email, user_path(@user)
  end

  def timezones
    @timezones = Timezone.all
  end

  def sort_column
    params[:sort] || 'email'
  end

  def sort_direction
    params[:direction] || 'asc'
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password,
      :password_confirmation, :organization_id, :timezone_id,
      manager_permissions_attributes: [:manageable_id, :manageable_type, :role_id])
  end

  def redirect_to_account
    redirect_to account_index_path and return if @user.eql?(current_user)
  end
end
