
class PowerDnsBackendController < ApplicationController

  class MissingKeys < RuntimeError
    attr_reader :keys

    def initialize(keys)
      @keys = keys
    end
  end

  skip_before_action :verify_authenticity_token
  skip_filter :authenticate_user!
  before_filter :default_format_json

  def default_format_json
    request.format = "json"
  end

  def lookup
    begin
      params[:parameters] ||= {}.to_json
      pdns = ActiveSupport::JSON.decode((params[:parameters]))

      missing_keys = %w(qtype qname).find_all { |k| not pdns.key? k }
      raise MissingKeys.new(missing_keys) if missing_keys.length > 0

      qtype, qname = pdns["qtype"].upcase, pdns["qname"].chomp(".")

      @records = \
        if qtype == "ANY" then
          PowerDnsRecordManager::Record.where(name: qname)
        elsif qtype == "SOA" then
          PowerDnsRecordManager::Record.where(type: qtype)
        else
          PowerDnsRecordManager::Record.where(type: qtype, name: qname)
        end

    rescue MultiJson::LoadError => e
      logger.error "Error parsing json parameters: #{e}"
      response.status = 400
    rescue MissingKeys => m
      logger.error "Error, missing keys in json parameters: #{m.keys.join(', ')}"
      response.status = 400
    end
  end

end
