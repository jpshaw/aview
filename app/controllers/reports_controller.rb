require 'will_paginate/array'
require 'vig_report'

class ReportsController < ApplicationController
  include Operations::Metrics::Helpers

  before_filter :base_breadcrumbs, :except => [:create, :finished_generating]
  before_filter :get_report, only: [:show, :export, :destroy]

  around_action :show_metrics, only: :show

  def index
    respond_to do |format|
      format.html do
        add_breadcrumb "History", reports_path
        @presenter = Presenters::Reports::Index.new(view_context)
      end

      format.json do
        render json: ReportsDataTable.new(view_context)
      end
    end
  end

  def show
    redirect_to reports_path and return if @report.nil?
    add_breadcrumb @report.title,
                   url_for(controller: :reports, action: @report.report_type, only_path: true) if @report.title.present?
    add_breadcrumb timestamp_in_users_timezone(@report.created_at), report_path(@report)
  end

  def export
    if @report
      filename = "#{@report.report_type}-report-#{@report.id}-export"
      options = { column_headers: params['column-headers'], column_values: params['column-values'], report: @report }
      data_table = ReportDataTable.new(options)
      respond_to do |format|
        format.csv { send_data data_table.to_csv, type: 'text/csv', filename: "#{filename}.csv" }
        format.xlsx { send_data data_table.to_xlsx, type: 'application/xlsx', filename: "#{filename}.xlsx" }
      end
    else
      flash[:error] = t '.error'
      redirect_to reports_path
    end
  end

  def availability
    @presenter = Presenters::Reports::Availability.new(view_context)
    @report = @presenter.report
    add_breadcrumb 'Availability', '#'
    render :new
  end

  def cellular_network_type
    @presenter = Presenters::Reports::CellularNetworkType.new(view_context)
    @report = @presenter.report
    add_breadcrumb 'Cellular Network Type', '#'
    render :new
  end

  def cellular_utilization
    @presenter = Presenters::Reports::CellularUtilization.new(view_context)
    @report = @presenter.report
    add_breadcrumb 'Cellular Utilization', '#'
    render :new
  end

  def cellular_signal_strength
    @presenter = Presenters::Reports::CellularSignalStrength.new(view_context)
    @report = @presenter.report
    add_breadcrumb 'Cellular Signal Strength', '#'
    render :new
  end

  def vig
    @presenter = Presenters::Reports::Vig.new(view_context)
    @report = @presenter.report
    add_breadcrumb 'VIG', '#'
    render :new
  end

  def create
    if @report = current_user.reports.create(params: params[:report])
      @report_queue = TorqueBox.fetch('/queues/reports')
      @report_queue.publish(device_report_id: @report.id)
    end
    if request.xhr?
      render js: "document.location = '#{report_path(@report)}'"
    else
      redirect_to report_path(@report)
    end
  end

  def destroy
    respond_to do |format|
      if @report && @report.destroy
        format.html { redirect_to reports_path, flash[:success] = 'Successfully removed report.' }
        format.js   {}
      else
        format.html { redirect_to reports_path, flash[:error] = 'Failed to remove report. Please try again.' }
        format.js   {}
      end
    end
  end

  def finished_generating
    report = current_user.reports.find_by(id: params[:report_id]) || false

    unless report == false
      report = report.is_done?
    end

    respond_to do |format|
      format.json { render :json => report.to_json }
      format.html { render :text => report.to_json }
    end
  end

  private

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Reports", reports_path
  end

  def get_report
    @report = current_user.reports.find_by(id: params[:id])
  end

  def show_metrics
    ops_metric_increment 'reports.show'
    ops_metric_measure 'reports.show.elapsed' do
      yield
    end
  end
end
