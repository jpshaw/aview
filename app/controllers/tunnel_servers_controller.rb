class TunnelServersController < ApplicationController

  before_action :get_tunnel_server, only: [:ip_addresses]
  before_action :set_view_path

  def index
    respond_to do |format|
      format.html { @presenter = TunnelServersPresenter.new(view_context) }
      format.json { render json: TunnelServersDataTable.new(view_context) }
    end
  end

  # list of devices that belong to the tunnel server, within the user's scope
  def show
    respond_to do |format|
      format.html { @presenter = TunnelServerDevicesPresenter.new(view_context) }
    end
  end

  def ip_addresses
    respond_to do |format|
      format.js {}
    end
  end

  private

  def get_tunnel_server
    @tunnel_server = current_user.tunnel_servers.find(params[:id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/tunnel_servers"
  end

end
