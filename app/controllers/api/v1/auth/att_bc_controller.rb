class Api::V1::Auth::AttBcController < ApplicationController
  skip_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token

  rescue_from Apipie::ParamMissing do |e|
    render_400
  end

  resource_description do
    api_versions '1.0'
    formats ['json']

    error 400, 'Invalid Parameters'
    error 401, 'Unauthorized'
    error 404, 'Missing device'
    error 429, 'Rate limit exceeded'
    error 500, 'Internal server error'
  end

  def_param_group :required_fields do
    param :token, String, desc: 'AT&T Business Center authentication token', required: true
  end

  api :POST, '/att_bc', 'Request authentication with a BC token'
  param_group :required_fields
  def create
    authenticate_user
  end

  api :GET, '/att_bc', 'Request authentication with a BC token'
  param_group :required_fields
  def index
    authenticate_user
  end

  private

    def authenticate_user
      begin
        result = AuthenticateBusinessCenterUser.call(auth_params)

        if result.success?
          sign_in result.user

          if result.device.present?
            redirect_to device_path(result.device)
          else
            redirect_to root_path
          end
        else
          flash[:error] = result.message
          redirect_to new_user_session_path
        end
      rescue => e
        flash[:error] = "Unable to authenticate user: #{e.message}"
        redirect_to new_user_session_path
      end
    end

    def auth_params
      params.permit(:token)
    end

end
