class Api::V3::ContactsController < ApplicationController

  before_action :authorize_contacts, except: :index
  before_action :cache_principal_id

  # GET /api/v3/devices/:device_id/contacts.json
  def index
    if current_device
      respond_to do |format|
        format.json do
          render json: ContactDataTable.new(current_device, view_context)
        end
      end
    else
      render_401
    end
  end

  # GET /api/v3/devices/:device_id/contacts/new.js
  def new
    respond_to do |format|
      format.js { @device = current_device }
    end
  end

  # POST /api/v3/devices/:device_id/contacts.js
  def create
    respond_to do |format|
      format.js do
        if params[:commit] == 'create'
          @contact = ::Contacts::CreateService.new(current_device, contact_params).execute
        else # 'Add'
          assign_existing_contact
        end
      end
    end
  end

  #GET /api/v3/devices/:device_id/contacts/:id.js
  def show
    respond_to do |format|
      format.js do
         set_device_and_contact
      end
    end
  end

  #PATCH /api/v3/devices/:device_id/contacts/:id.js
  def update
    respond_to do |format|
      format.js do
        set_device_and_contact
        ::Contacts::UpdateService.new(current_device, current_contact, contact_params).execute
      end
    end
  end

  #DELETE /api/v3/devices/:device_id/contacts/:id.js
  def destroy
    respond_to do |format|
      format.js do
        ::Contacts::DestroyService.new(current_device, current_contact).execute
      end
    end
  end

  private

  def authorize_contacts
    authorize current_device, :modify_contacts?
  end

  def assign_existing_contact
    @contact = current_device.organization_contacts.find_by(id: contact_params[:id])
    if @contact && !current_device.contacts.exists?(@contact.id)
      # TODO: Find out why ActiveRecord::AssociationTypeMismatch is thrown
      # when the contact object is passed in like this:
      # current_device.contacts << @contact
      current_device.contacts << Contact.find(@contact.id)
    elsif @contact.nil?
      @contact = Contact.none
    else
      @contact.errors.add(:contact, 'already assigned to device.')
    end
  end

  def set_device_and_contact
    @device = current_device 
    @contact = current_contact
  end

  def contact_params
    params.require(:contact).permit(:id, :name, :email, :phone, :label)
  end

  def current_contact
    @current_contact ||= current_device.contacts.find_by(id: params[:id])
  end

  def current_device
    @current_device ||= current_user.devices.find_by(mac: params[:device_id])
  end

end
