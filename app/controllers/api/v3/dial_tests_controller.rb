class Api::V3::DialTestsController < ApplicationController

  # GET /api/v3/devices/:device_id/dial_tests.json
  def index
    if current_device
      respond_to do |format|
        format.json do
          render json: DialTestDataTable.new(current_device, view_context)
        end
      end
    else
      render_401
    end
  end
  private

  def current_device
    @current_device ||= current_user.devices.find_by(mac: params[:device_id])
  end

end
