class Api::V3::DevicesController < ApplicationController

  before_action :device
  before_action :location, only: :update_location
  before_action :cache_principal_id

  # PATCH /api/v3/devices/:id/clear_location.js
  def clear_location
    @device.update_attributes(location_id: nil)

    respond_to do |format|
      format.js {}
    end
  end

  # PATCH /api/v3/devices/:id.js
  def update
    @result = UpdateDevice.call(
      device: device,
      user: current_user,
      params: device_params
    )

    respond_to do |format|
      format.js {}
      format.json do
        if @result.success?
          render json: {}, status: :ok
        else
          render json: { error: @result.message }, status: :bad_request
        end
      end
    end
  end

  # PATCH /api/v3/devices/:id/update_location.js
  def update_location
    if @location.blank?
      @device.update(location_id: nil)
    else
      @device.update(location_id: @location.id)
      wait_for_location_geocoding
    end

    respond_to do |format|
      format.js {}
    end
  end

  private

  def device_params
    params.require(:device).permit(:organization_id, :site_name,
      :configuration_id, :data_plan_id, :mgmt_host, :hostname, :comment)
  end

  def device
    @device = current_user.devices.find_by!(mac: params[:id])
  end

  def location
    @location = ::Locations::FindOrUpsertByDeviceAddress.execute(@device, location_params)
  end

  def location_params
    params.require(:location).permit(:address_1, :address_2, :city, :region,
      :postal_code, :country_id, :latitude, :longitude)
  end

  def wait_for_location_geocoding
    return if @device.location.geocoding_done?

    begin
      Timeout::timeout(15) do
        sleep(1) until @device.reload.location.geocoding_done?
      end
      @location.reload
    rescue Timeout::Error
      @location.errors.add(:base, 'Location geocoding timed out.')
    end
  end
end
