module Api

  module Certificates

    class V2Controller < Api::ApiController

      MAC_REGEXP = /[A-Fa-f0-9]{12}/.freeze

      # POST /certificates/v2
      def create
        render(text: "CSR missing", status: :bad_request) and return unless params[:csr]

        certificate_authority = CertificateAuthority.new(params[:csr])
        mac                   = certificate_authority.common_name.try(:upcase)

        unless mac =~ MAC_REGEXP
          render(text: "Invalid common name: '#{mac}'", status: :bad_request) and return
        end

        unless device = Device.find_by(mac: mac)
          render(text: 'Device Not Found', status: :not_found) and return
        end

        certificate = device.certificates.find_by(md5: Certificate.md5(params[:csr]))

        if certificate.blank?
          # Increment our metrics counter for new certs, and sign the new cert
          ops_metric_increment 'certificates.new_requests'
          client_cert = certificate_authority.sign_request
          certificate = device.certificates.create(
                          cert:     client_cert,
                          approved: true,
                          signed:   true
                        )

          if certificate.errors.any?
            full_messages = certificate.errors.full_messages.join('; ')
            Rails.logger.error "Error creating certificate for device '#{device.mac}': #{full_messages}"
            render(text: full_messages, status: :bad_request)
            return
          else
            # Delete older revoked certificates (approved == false).
            device.certificates.where(approved: false).where('id != ?', certificate.id).destroy_all
          end
        end

        if certificate.approved?
          unless certificate.signed?
            certificate.update_attributes(cert: certificate_authority.sign_request, signed: true)
          end

          if certificate.errors.any?
            full_messages = certificate.errors.full_messages.join('; ')
            Rails.logger.error "Error signing certificate for device '#{device.mac}': #{full_messages}"
            render(text: full_messages, status: :bad_request)
            return
          end

          # Delete all other certificates.
          certificate.device.certificates.where('id != ?', certificate.id).destroy_all

          render(text: certificate.cert)
        else
          render(text: "Request submitted: awaiting approval\n", status: :accepted)
        end
      rescue => e
        render text: e.message, status: :bad_request
      end

    end

  end

end
