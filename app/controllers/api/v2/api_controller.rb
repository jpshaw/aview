require 'api/rate_limiter'

class Api::V2::ApiController < ApplicationController
  include Operations::Metrics::Helpers

  skip_filter :authenticate_user!
  before_filter :check_api_setting
  before_filter :authenticate_user_from_token!
  before_filter :throttle, if: :signed_in?

  protected

  def check_api_setting
    render_404 and return unless Settings.user_api.enabled?
  end

  def authenticate_user_from_token!
    ops_metric_increment 'api.requests'

    user = User.find_by_authentication_token(params[:auth_token])

    if user
      sign_in user, store: false
    else
      ops_metric_increment 'api.unauthorized'
      render_401
    end
  end

  def throttle
    rate_limiter = Api::RateLimiter.new(user: current_user)

    if rate_limiter.max_requests_reached?
      render_max_request_status
    else
      rate_limiter.increment
    end

    response.headers['X-Rate-Limit-Limit']     = "#{rate_limiter.api_request_limit}"
    response.headers['X-Rate-Limit-Remaining'] = "#{rate_limiter.remaining_calls}"
    response.headers['X-Rate-Limit-Reset']     = "#{rate_limiter.expires_at.to_i}"
  end

  # TODO: Use ActiveSupport::Notifications for metrics collection
  def collect_metrics(name)
    ops_metric_increment name
    ops_metric_measure "#{name}.elapsed" do
      yield
    end
  end

end
