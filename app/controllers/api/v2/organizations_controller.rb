class Api::V2::OrganizationsController < ApplicationController

  respond_to :js

  before_action :set_view_path
  before_action :set_parent_organization, only: [:create]
  before_action :set_organization,        only: [:abilities]

  # GET /api/v2/organizations/:id/abilities.js
  def abilities
    @abilities = current_user.abilities_for(@organization)
  end

  # GET /api/v2/organizations/new.js
  def new
    authorize Organization, :new?
    @organization         = Organization.new
    @parent_organizations = current_user.organizations
    @abilities            = current_user.abilities_for(@parent_organizations.first)
  end

  # POST /api/v2/organizations.js
  def create
    authorize @parent_organization, :modify?
    @organization = \
      OrganizationCreator
        .call(
          abilities: abilities_params,
          params:    organization_params,
          parent:    @parent_organization,
          user:      current_user)
        .organization

    @customers =  @organization.errors.any? ? [] : get_customers
  end

  private

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/organizations"
  end

  def set_organization
    @organization ||= current_user.organizations.find_by(id: params[:id])
  end

  def set_parent_organization
    @parent_organization ||= current_user.organizations.find_by(id: organization_params[:parent_id])
  end

  def organization_params
    params.require(:organization).permit(
      :name, :description, :location_desc, :parent_id, :logo, :footer, :remove_logo, :resize_to_fit,
      :customer_service_id
    )
  end

  def abilities_params
    params[:abilities] || {}
  end

  def get_customers
    if current_user.filter_organization.nil?
      current_user.head_manageable_organizations
    else
      [current_user.filter_organization].compact
    end
  end
end
