class Api::V2::SitesController < ApplicationController

  respond_to :js

  before_action :set_site
  before_action :set_view_path

  # GET /api/v2/sites/:id/edit.js
  def edit
    authorize @site
  end

  # PATCH /api/v2/sites/:id.js
  def update
    authorize @site
    @site.update_attributes(site_params) if @site
  end

  # DELETE /api/v2/sites/:id.js
  def destroy
    authorize @site, :delete?
    @site.destroy
  end

  private

  def set_site
    @site = current_user.sites.readonly(false).find_by(id: params[:id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/sites"
  end

  def site_params
    params.require(:site).permit(:name)
  end

end
