class Api::V2::AccountsController < ApplicationController

  respond_to :js
  before_action :set_user
  before_action :set_filter_organization, only: [:update_filter]

  # PATCH /api/v2/accounts/:id.js
  def update
    @user.update(user_params)
  end

  # PATCH /api/v2/accounts/:id/update_password.js
  def update_password
    if @user.update_with_password(user_params)
      sign_in @user, bypass: true
    end
  end

  # PATCH /api/v2/accounts/:id/update_filter.js
  def update_filter
    @user.update_attributes(filter_organization_id: @filter_organization.try(:id))
    flash[:info] = t('accounts.filter_applied', name: @filter_organization.name) if @filter_organization
  end

  # PATCH /api/v2/accounts/:id/clear_filter.js
  def clear_filter
    @user.update_attributes(filter_organization_id: nil)
  end

  # PATCH /api/v2/accounts/:id/generate_api_token.js
  def generate_api_token
    @user.reset_authentication_token!
  end

  private

  def set_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:name, :email, :timezone_id, :notify_enabled,
      :maps_enabled, :current_password, :password, :password_confirmation, :cellular_pins_on_map, 
      :clock_enabled)
  end

  def set_filter_organization
    @filter_organization = @user.all_organizations.find_by(id: params[:filter_organization_id])
  end

end
