class Api::V2::UsersController < ApplicationController

  respond_to :json, :js

  before_action :set_user,         only: [:update, :destroy]
  before_action :set_view_path
  before_action :set_organization, only: [:create]
  before_action :set_role,         only: [:create]

  # GET /api/v2/users.json
  def index
    render json: UsersDataTable.new(view_context)
  end

  # GET /api/v2/users/new.js
  def new
    @user          = User.new
    @organizations = current_user.organizations
    @organization  = @organizations.find_by(id: params[:organization_id]) || @organizations.first
    @roles         = @organization.try(:roles) || []
    authorize @organization, :create_users?
  end

  # POST /api/v2/users.js
  def create
    authorize @organization, :create_users?
    result = SetupNewUser.call(organization: @organization,
                                       role: @role,
                                     params: user_params)
    @user = result.user
  end

  # GET /api/v2/users/:id/edit.js
  def edit

  end

  # PATCH /api/v2/users/:id.js
  def update
    authorize @user, :modify?
    @user.update(user_params) if @user
  end

  # DELETE /api/v2/users/:id.js
  def destroy
    authorize @user, :destroy?
    @user.destroy if @user
  end

  private

  def set_user
    @user = current_user.users.readonly(false).find_by(id: params[:id])
  end

  def set_organization
    @organization ||= current_user.organizations.find_by(id: params[:user][:organization_id])
  end

  def set_role
    @role ||= @organization.roles.find_by(id: params[:user][:role_id]) if @organization
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/users"
  end

  def user_params
    params.require(:user).permit(:name, :email, :timezone_id, :notify_enabled,
      :maps_enabled, :cellular_pins_on_map)
  end

end
