class Api::V2::DevicesController < Api::V2::ApiController

  before_filter :default_params
  before_filter :get_device, only: :show

  around_action :show_metrics, only: :show
  around_action :index_metrics, only: :index

  resource_description do
    formats ['json', 'xml']

    error 401, 'Unauthorized'
    error 404, 'Missing device'
    error 429, 'Rate limit exceeded'
    error 500, 'Internal server error'
  end

  def_param_group :required_fields do
    param :auth_token, String, desc: 'User authentication token', required: true
  end

  api :GET, '/devices', 'Get a paginated list of all devices'
  param_group :required_fields
  example <<-EOS
    curl -v https://aview.accns.com/api/devices.json\?auth_token\=Mmxm3sJrZBhkJXX7s2Dn
    >
    < HTTP/1.1 200 OK
    < X-Rate-Limit-Limit: 1000
    < X-Rate-Limit-Remaining: 998
    < X-Rate-Limit-Reset: 1431968672
    < Content-Type: application/json; charset=utf-8
    < X-UA-Compatible: IE=Edge
    < ETag: "e1726d0e792d164e1f84b447782242de"
    < Cache-Control: max-age=0, private, must-revalidate
    < X-Request-Id: a6fd06cfcf3aefd2322bee02c2c0d431
    < X-Runtime: 1.351979
    < Connection: close
    <
    {"results"=>
      {"metadata"=>{"pages"=>13, "next_page"=>2},
      "devices"=>
       [{"mac"=>"00D0CF1B3AA0",
         "site"=>"TMW_1117_NG",
         "deployed"=>true,
         "status"=>"Device is up",
         "category"=>"VPN Gateway",
         "model"=>"8200",
         "host"=>"64.105.254.162",
         "last_heartbeat_at"=>"2015-05-14T21:10:42Z",
         "last_restarted_at"=>"2014-10-14T06:56:11Z",
         "activated_at"=>"2014-03-14T07:38:27Z"},
        {"mac"=>"00270423C4A4",
         "site"=>"USAXTAWESIA01B",
         "deployed"=>true,
         "status"=>"Device is on notice",
         "category"=>"VPN Gateway",
         "model"=>"8300",
         "host"=>"209.23.136.170",
         "last_heartbeat_at"=>"2015-02-09T18:22:26Z",
         "last_restarted_at"=>"2014-12-10T23:03:56Z",
         "activated_at"=>"2014-12-04T22:08:38Z"},
        {"mac"=>"00270401058E",
         "serial"=>"6200010361301393",
         "site"=>"DTS01328",
         "deployed"=>true,
         "status"=>"Device is up",
         "category"=>"USB Cellular",
         "model"=>"6200-FX",
         "host"=>"166.136.28.42",
         "last_heartbeat_at"=>"2015-02-05T20:28:21Z",
         "last_restarted_at"=>"2015-01-08T05:32:15Z",
         "activated_at"=>"2014-04-02T00:21:04Z",
         "modem"=>
          {"carrier"=>"AT&T",
           "name"=>"SierraWireless 313u",
           "model"=>"313u",
           "maker"=>"SierraWireless",
           "phone_number"=>"18134708823",
           "apn"=>"managedvpn",
           "dbm"=>-85,
           "signal"=>"-85 dBm (43%)",
           "signal_pct"=>"43%",
           "cid"=>"213934671",
           "cnti"=>"HSPA+",
           "ecio"=>"0.000",
           "esn"=>"",
           "iccid"=>"89014103276027028650",
           "imei"=>"012615000652413",
           "lac"=>"18453",
           "mcc"=>"310",
           "mnc"=>"410",
           "revision"=>"SWI9200X_03.05.20.03ap",
           "imsi"=>"310410602702865",
           "rsrp"=>"",
           "rsrq"=>"",
           "rssi"=>"-1",
           "sinr"=>"0.000",
           "snr"=>"-99.000",
           "usb_speed"=>"480",
           "rx"=>2141885722,
           "tx"=>2147483647,
           "rx_30"=>nil,
           "tx_30"=>nil}}]}}
    EOS
  def index
    per_page      = (params[:per_page] || 100).to_i
    current_page  = (params[:page] || 1).to_i
    current_page  = 1 if current_page <= 0
    total_entries = current_user.devices.count
    devices      = current_user.devices.limit(per_page).offset((current_page - 1) * per_page)
    page_count   = total_entries.zero? ? 1 : (total_entries / per_page.to_f).ceil
    unless current_page >= page_count
      next_page  = current_page + 1
    end

    respond_to do |format|
      format.json do
        serialized_devices = devices.as_json(serialize_options)
        render json: { results: index_result(page_count, next_page, serialized_devices) }
      end
      format.xml do
        result = index_result(page_count, next_page, devices)
        render xml: result.to_xml(serialize_options.merge(root: :results))
      end
    end
  end

  api :GET, '/devices/:id', 'Get information for a single device'
  param_group :required_fields
  param :id, String, desc: 'The device mac address, serial, or hostname', required: true
  example <<-EOS
    curl -v https://aview.accns.com/api/devices/002704010CFD.xml\?auth_token\=Mmxm3sJrZBhkJXX7s2Dn
    >
    < HTTP/1.1 200 OK
    < X-Rate-Limit-Limit: 1000
    < X-Rate-Limit-Remaining: 999
    < X-Rate-Limit-Reset: 1431968672
    < Content-Type: application/xml; charset=utf-8
    < X-UA-Compatible: IE=Edge
    < ETag: "ef475702fab275dfb009715d9fe5136b"
    < Cache-Control: max-age=0, private, must-revalidate
    < X-Request-Id: c29d0b9c889d0495f67ae4cd73c6cb28
    < X-Runtime: 0.707748
    < Connection: close
    <
    <?xml version="1.0" encoding="UTF-8"?>
    <device>
    <mac>002704010CFD</mac>
    <serial>6200010764461329</serial>
    <site>Default</site>
    <deployed type="boolean">true</deployed>
    <status>Device is down</status>
    <category>Cellular</category>
    <model>6200-FX</model>
    <host>173.6.152.155</host>
    <last-heartbeat-at type="datetime">2014-09-12 21:34:15 UTC</last-heartbeat-at>
    <last-restarted-at type="datetime">2014-09-12 21:34:10 UTC</last-restarted-at>
    <activated-at type="datetime">2014-08-21 16:02:59 UTC</activated-at>
    <modem>
      <carrier>Sprint</carrier>
      <name>SierraWireless 598</name>
      <model>598</model>
      <maker>SierraWireless</maker>
      <phone-number>8137894218</phone-number>
      <apn></apn>
      <dbm>-106</dbm>
      <signal>-106 dBm (9%)</signal>
      <signal-pct>9%</signal>
      <cid></cid>
      <cnti>EV-DO</cnti>
      <ecio></ecio>
      <esn>09610413134</esn>
      <iccid></iccid>
      <imei></imei>
      <lac></lac>
      <mcc/>
      <mnc/>
      <revision>p2511202,61090</revision>
      <imsi></imsi>
      <rsrp></rsrp>
      <rsrq></rsrq>
      <rssi>-102</rssi>
      <sinr></sinr>
      <snr></snr>
      <usb_speed>12</usb_speed>
      <rx>2288</rx>
      <tx>1865</tx>
      <rx_30/>
      <tx_30/>
    </modem>
    </device>
  EOS
  def show
    respond_to do |format|
      format.json { render json: { device: @device.as_json(serialize_options) } }
      format.xml { render xml: @device.to_xml(serialize_options.merge(root: :device)) }
    end
  end

  private

  def default_params
    params[:sort]      = 'devices.mac'
    params[:direction] = 'asc'
  end

  def get_device
    @device = current_user.devices.find_by(mac: device_params[:id])
    @device ||= current_user.devices.find_by(serial: device_params[:id])
    @device ||= current_user.devices.find_by(hostname: device_params[:id])
    render_404 and return unless @device
  end

  def device_params
    params.permit(:id)
  end

  def show_metrics(&block)
    collect_metrics 'api.devices.show', &block
  end

  def index_metrics(&block)
    collect_metrics 'api.devices.index', &block
  end

  def index_result(page_count, next_page, devices)
    results = {
      metadata: {
        pages: page_count
      },
      devices: devices
    }
    results[:metadata][:next_page] = next_page if next_page.present?
    results
  end

  def serialize_options
    { only: [:mac, :host, :last_heartbeat_at, :last_restarted_at, :activated_at],
      custom: true }
  end
end
