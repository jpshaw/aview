class Api::V2::RolesController < ApplicationController

  respond_to :json, :js

  before_action :set_organization
  before_action :set_view_path

  # GET /api/v2/roles.json
  def index
    respond_to do |format|
      format.json { render json: @organization.roles }
    end
  end

  # GET /api/v2/permissions/datatable.json
  def datatable
    render json: RoleDataTable.new(view_context, @organization)
  end

  private

  def set_organization
    @organization ||= current_user.organizations.find_by(id: params[:organization_id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/roles"
  end

end
