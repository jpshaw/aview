class Api::V2::NotificationProfilesController < ApplicationController
  include AccountHelper

  respond_to :json, :js

  before_action :set_user
  before_action :set_profile,      only: [:edit, :update, :destroy, :subscriptions]
  before_action :set_form_objects, only: [:new, :edit]
  before_action :set_view_path

  # GET /api/v2/accounts/:account_id/notification_profiles.json
  def index
    render json: NotificationProfileDataTable.new(view_context)
  end

  # GET /api/v2/accounts/:account_id/notification_profiles/new.js
  def new
    @profile = @user.notification_profiles.new
  end

  # POST /api/v2/accounts/:account_id/notification_profiles.js
  def create
    @profile = @user.notification_profiles.create(filtered_profile_params)
  end

  # GET /api/v2/accounts/:account_id/notification_profiles/:id/edit.js
  def edit
    @subscribed_uuids = @profile.subscribed_events.pluck(:id, :uuid_id).to_h
  end

  # PATCH /api/v2/accounts/:account_id/notification_profiles/:id.js
  def update
    if @profile
      UpdateNotificationProfile.call(
        profile: @profile,
        params: filtered_profile_params
      )
    end
  end

  # DELETE /api/v2/accounts/:account_id/notification_profiles/:id.js
  def destroy
    @profile.destroy if @profile
  end

  # GET /api/v2/accounts/:account_id/notification_profiles/:id/subscriptions.js
  def subscriptions
  end

  private

  def set_user
    @user = current_user_from_params
  end

  def set_profile
    @profile = @user.notification_profiles.find(params[:id])
  end

  def set_form_objects
    @frequencies       = NotificationProfile::Frequencies.select_list
    @device_categories = current_user.device_categories.to_a
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/notification_profiles"
  end

  def profile_params
    params.require(:notification_profile).permit(
      :name, :frequency, subscribed_events_attributes: [:id, :uuid_id, :_destroy]
    )
  end

  # Set individual frequency
  # TODO: Remove this once notifications depend solely on the frequency
  # in the notification profile.
  def filtered_profile_params
    @filtered_profile_params ||= begin
      filtered_profile_params = profile_params

      filtered_profile_params.fetch(:subscribed_events_attributes, []).each do |subscribed_event|
        if subscribed_event[:uuid_id].present?
          subscribed_event[:frequency] = profile_params[:frequency]
        end
      end

      filtered_profile_params
    end
  end

end
