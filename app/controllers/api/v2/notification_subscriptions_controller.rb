class Api::V2::NotificationSubscriptionsController < ApplicationController
  include AccountHelper

  respond_to :json, :js

  before_action :set_user
  before_action :set_subscription, only: [:edit, :update, :destroy]
  before_action :set_publisher,    only: [:create]
  before_action :set_profile,      only: [:create, :update]
  before_action :set_view_path

  # GET /api/v2/accounts/:account_id/notification_subscriptions.json
  def index
    render json: NotificationSubscriptionDataTable.new(view_context)
  end

  # GET /api/v2/accounts/:account_id/notification_subscriptions/reset.js
  def reset
  end

  # GET /api/v2/accounts/:account_id/notification_subscriptions/search.json
  def search
    render json: { results: publisher_search_results }, root: false
  end

  # GET /api/v2/accounts/:account_id/notification_subscriptions/new.js
  def new
    @subscription = @user.notification_subscriptions.new
  end

  # POST /api/v2/accounts/:account_id/notification_subscriptions.js
  def create
    @subscription = @user.notification_subscriptions.new
    @subscription.profile   = @profile
    @subscription.publisher = @publisher
    @subscription.save
  end

  # GET /api/v2/accounts/:account_id/notification_subscriptions/:id/edit.js
  def edit
  end

  # PATCH /api/v2/accounts/:account_id/notification_subscriptions/:id.js
  def update
    if @subscription
      @subscription.profile = @profile
      @profile_changed = @subscription.profile_id_changed?
      @subscription.save
    end
  end

  # DELETE /api/v2/accounts/:account_id/notification_subscriptions/:id.js
  def destroy
    @subscription.destroy if @subscription
  end

  private

  def set_user
    @user = current_user_from_params
  end

  def set_subscription
    @subscription = @user.notification_subscriptions.find_by(id: params[:id])
  end

  def set_publisher
    @publisher ||= begin
      case publisher_params[:publisher_type]
      when NotificationSubscription::Types::ORGANIZATION
        @user.organizations.find_by(id: publisher_params[:publisher_id])
      when NotificationSubscription::Types::DEVICE
        @user.devices.find_by(id: publisher_params[:publisher_id])
      end
    end
  end

  def set_profile
    @profile ||= @user.notification_profiles.find_by(id: publisher_params[:profile_id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/app/views/api/v2/notification_subscriptions"
  end

  def publisher_params
    params.permit(:profile_id, :publisher_type, :publisher_id)
  end

  def publisher_search_results
    case params[:publisher_type]
    when NotificationSubscription::Types::ORGANIZATION
      OrganizationTypeaheadSearch.new(@user, params[:query], :filtered).execute
    when NotificationSubscription::Types::DEVICE
      DeviceTypeaheadSearch.new(@user, params[:query]).execute
    else
      []
    end
  end
end
