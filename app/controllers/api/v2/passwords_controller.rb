class Api::V2::PasswordsController < ApplicationController
  skip_before_filter :authenticate_user!
  respond_to :js

  # PATCH /api/v2/accounts/:id.js
  def update
    @user = User.reset_password_by_token(user_params)
    sign_in @user if @user.errors.empty?
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :reset_password_token)
  end

end