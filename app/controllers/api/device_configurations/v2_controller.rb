module Api

  module DeviceConfigurations

    class V2Controller < Api::ApiController

      before_filter :validate_sdn!
      before_filter :set_modified_at_header, only: :show
      after_filter  :save_config_checked_at, only: :show

      around_action :show_metrics, only: :show

      # GET /device_configurations/v2/002704030201.json
      def show
        if device.blank?
          ops_metric_increment 'configurations.device_not_found'
          render json: {message: 'Device Not Found'}, status: :not_found
        elsif device.configuration.blank?
          ops_metric_increment 'configurations.not_found'
          render json: {message: 'Configuration Not Found'}, status: :not_found
        elsif !config_modified?
          ops_metric_increment 'configurations.not_modified'
          render json: {message: 'Configuration Not Modified'}, status: :not_modified
        else
          ops_metric_increment 'configurations.rendered'
          if Settings.views.device.configuration_inheritance
            render json: device.configuration.compiled_settings
          else
            render json: device.configuration.description
          end
        end
      end

      private

      def show_metrics(&block)
        ops_metric_measure 'configurations.elapsed' do
          yield
        end
      end

      def validate_sdn!
        distinguished_name = ::DistinguishedName.parse(sdn)

        unless distinguished_name.common_name_matches?(params[:id], 'universal')
          ops_metric_increment 'configurations.unauthorized'
          render json: { message: 'Forbidden' }, status: :forbidden
        end
      end

      def save_config_checked_at
        device.update_column(:configuration_checked_at, Time.now) if device
      end

      def set_modified_at_header
        if device && device.configuration && modified_at
          response.headers['Last-Modified'] = modified_at.rfc2822
        end
      end

      def device
        @device ||= Device.where('upper(mac) = upper(?)', params[:id]).first
      end

      def modified_since
        @modified_since ||= begin
          Time.rfc2822(request.env['HTTP_IF_MODIFIED_SINCE']) if request.env['HTTP_IF_MODIFIED_SINCE']
        rescue
          nil
        end
      end

      def modified_at
        @modified_at ||= begin
          if device.configuration_associated_at && (device.configuration_associated_at > device.configuration.updated_at)
            device.configuration_associated_at
          else
            device.configuration.updated_at
          end
        end
      end

      def config_modified?
        return true unless modified_since
        modified_at > modified_since
      end

    end
  end
end
