class Api::V4::DevicesController < Api::V2::ApiController

  resource_description do
    api_versions '4.0'
    formats ['json']

    error 400, 'Invalid Parameters'
    error 401, 'Unauthorized'
    error 404, 'Missing device'
    error 429, 'Rate limit exceeded'
    error 500, 'Internal server error'
  end

  def_param_group :required_fields do
    param :auth_token, String, desc: 'User authentication token', required: true
  end

  api :GET, '/devices', 'Get a paginated list of all devices'
  param_group :required_fields
  param :page_limit, String, desc: 'The number of devices per page (default: 100)', required: false
  param :page, String, desc: 'The page number of results to get', required: false
  param :organization, String, desc: 'Limit results to devices within a given organization', required: false
  param :include_hierarchy, String, desc: 'Limit results to devices within a given organization and its sub-organizations (default: false)', required: false
  param :start_date, String, desc: 'The start date of the heartbeat filter. The ISO 8601 standard is used for the format. If a date is provided without a time, the beginning of the day given is used.', required: false
  param :end_date, String, desc: 'The end date of the heartbeat filter. The ISO 8601 standard is used for the format. If a date is provided without a time, the end of the day given is used.', required: false
  param :interval, String, desc: 'The number of seconds for the heartbeat filter. If given, the only devices returned will be those which have checked in to AView with a heartbeat in the last n seconds', required: false
  example <<-EOS
    curl -v https://aview.accns.com/api/v4/devices.json\?auth_token\=Mmxm3sJrZBhkJXX7s2Dn&page_limit=50&page=2&organization=Accelerated&include_hierarchy=true
    >
    < HTTP/1.1 200 OK
    < X-Rate-Limit-Limit: 1000
    < X-Rate-Limit-Remaining: 998
    < X-Rate-Limit-Reset: 1431968672
    < Content-Type: application/json; charset=utf-8
    < X-UA-Compatible: IE=Edge
    < ETag: "e1726d0e792d164e1f84b447782242de"
    < Cache-Control: max-age=0, private, must-revalidate
    < X-Request-Id: a6fd06cfcf3aefd2322bee02c2c0d431
    < X-Runtime: 1.351979
    < Connection: close
    <
    {"results":
      {"metadata":{"pages":13, "next_page":2},
      "devices":[
        {"mac":"00D0CF1B3AA0",
         "deployed":true
        },
        {"mac":"00270423C4A4",
         "deployed":true
        },
        {"mac":"00270401058E",
         "deployed":true,
         {"modem":
          {"dbm":-85,
           "rx":2141885722,
           "tx":2147483647
          }
         }
        }
       ]
      }
    }
  EOS
  def index
    render_400 and return unless start_date_precedes_end_date
    @devices = device_index_relation.offset(per_page * (current_page - 1)).limit(per_page)
    set_pagination(device_index_relation)
    @included_fields = %w(deployed modem_dbm modem_tx modem_rx)
    @included_fields.concat(params[:include].split(',')) if params[:include].present?
  end

  api :GET, '/devices/usage', 'Get the cellular usage of a given device'
  param_group :required_fields
  param :device_id, String, desc: 'The mac address of the device to get cellular usage for', required: false
  param :page_limit, String, desc: 'The number of devices per page (default: 100)', required: false
  param :page, String, desc: 'The page number of results to get', required: false
  param :organization, String, desc: 'Limit results to devices within a given organization', required: false
  param :include_hierarchy, String, desc: 'Limit results to devices within a given organization and its sub-organizations (default: false)', required: false
  param :start_date, String, desc: 'The start date of range. The ISO 8601 standard is used for the format. If a date is provided without a time, the beginning of the day given is used. (default: beginning of today) * limited to the past 30 days', required: false
  param :end_date, String, desc: 'The end date of range. The ISO 8601 standard is used for the format. If a date is provided without a time, the end of the day given is used. (default: time of query)', required: false
  param :interface, String, desc: 'Limit results to the given interface. (default: all interfaces)', required: false
  example <<-EOS
    curl -v https://aview.accns.com/api/v4/devices/usage.json?auth_token=Mmxm3sJrZBhkJXX7s2Dn&device_id=002704010CFD&start_date=2016-12-05&end_date=2016-12-07&interface=PPP0
    >
    < HTTP/1.1 200 OK
    < X-Rate-Limit-Limit: 1000
    < X-Rate-Limit-Remaining: 998
    < X-Rate-Limit-Reset: 1431968672
    < Content-Type: application/json; charset=utf-8
    < X-UA-Compatible: IE=Edge
    < ETag: "e1726d0e792d164e1f84b447782242de"
    < Cache-Control: max-age=0, private, must-revalidate
    < X-Request-Id: a6fd06cfcf3aefd2322bee02c2c0d431
    < X-Runtime: 1.351979
    < Connection: close
    <
    {"metadata":{"pages":1},
     "results":[
       {"mac":"0027040302C1",
        "usage":[
          {"timestamp":"2016-12-05T00:00:00.000Z","upload":70,"download":50},
          {"timestamp":"2016-12-06T00:00:00.000Z","upload":0,"download":0},
          {"timestamp":"2016-12-07T00:00:00.000Z","upload":35,"download":25}
        ]
       }
     ]
    }
  EOS
  def usage
    render_404 and return if params[:device_id].present? && device_usage_relation.first.blank?
    render_400 and return unless start_date_precedes_end_date
    device_macs = device_usage_relation.offset(per_page * (current_page - 1)).limit(per_page).pluck(:mac)
    @results = usage_results(device_macs)
    set_pagination(device_usage_relation)
  end

  private

  def device_index_relation
    @device_relation ||= begin
      relation = current_user.devices
      relation = with_organization(relation)
      updated_at(relation)
    end
  end

  def with_organization(relation)
    if params[:organization].present?
      relation.with_organization({ organization_id: organization.try(:id),
                                   include_hierarchy: params[:include_hierarchy] })
    else
      relation
    end
  end

  def organization
    @organization ||= Organization.find_by(name: params[:organization])
  end

  def updated_at(relation)
    if params[:interval].present?
      relation.last_heartbeat_since(params[:interval].to_i)
    elsif params[:start_date].present? || params[:end_date].present?
      relation.last_heartbeat_between(start_date, end_date)
    else
      relation
    end
  end

  def device_usage_relation
    @device_usage_relation ||= begin
      relation = current_user.devices
      relation = with_organization(relation)
      with_mac_address(relation)
    end
  end

  def with_mac_address(relation)
    if params[:device_id].present?
      relation.with_mac(params[:device_id])
    else
      relation
    end
  end

  def set_pagination(device_relation)
    total_devices = device_relation.count
    @page_count = total_devices.zero? ? 1 : (total_devices / per_page.to_f).ceil
    @next_page = current_page + 1 if current_page < @page_count
  end

  def per_page
    (params[:page_limit] || 100).to_i
  end

  def current_page
    current_page = (params[:page] || 1).to_i
    current_page > 0 ? current_page : 1
  end

  def start_date_precedes_end_date
    start_date < end_date
  end

  def usage_results(device_macs)
    results = []
    device_macs.each do |mac|
      results << { mac: mac,
                   usage: Measurement::CellularUtilization.daily_utilization_totals(usage_options(mac)) }
    end
    results
  end

  def usage_options(device_mac)
    options = { mac: device_mac, start_date: start_date, end_date: end_date }
    options[:interface] = params[:interface] if params[:interface]
    options
  end

  def start_date
    @start_date ||= params[:start_date].present? ? formatted_date(params[:start_date]) : default_start_date
  end

  def end_date
    @end_date ||= params[:end_date].present? ? formatted_date(end_of_day(params[:end_date])) : default_end_date
  end

  def default_start_date
    DateTime.now.in_time_zone(current_user.timezone.name).beginning_of_day.utc.iso8601
  end

  def default_end_date
    DateTime.now.in_time_zone(current_user.timezone.name).utc.iso8601
  end

  def formatted_date(date)
    return DateTime.iso8601(date)
  rescue ArgumentError
    render_400
    DateTime.now
  end

  def end_of_day(date)
    date.include?('T') ? date : "#{date}T23:59:59"
  end
end
