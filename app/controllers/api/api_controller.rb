class Api::ApiController < ActionController::Base
  include Operations::Metrics::Helpers

  respond_to :json

  before_filter :authorize_device!


  private

  attr_reader :sdn

  def authorize_device!
    unless @sdn = request.env["HTTP_X_SSL_CERT_SDN"]
      ops_metric_increment 'configurations.unauthorized'
      render(json: {message: 'Forbidden'}, status: :forbidden) and return
    end
  end

end
