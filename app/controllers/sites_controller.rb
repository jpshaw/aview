class SitesController < ApplicationController

  before_filter :authenticate_user!
  before_filter :base_breadcrumbs

  before_filter :get_site, only: [:show, :edit, :update, :destroy]
  before_filter :check_if_default, only: [:edit, :update, :destroy]

  after_filter :verify_authorized, :except => [:index, :show, :search]

  def index
    @presenter = SitesPresenter.new(view_context)
    render 'index'
  end

  def show
    @presenter = SiteDevicesPresenter.new(view_context, @site)
    add_breadcrumb @site.organization.name, sites_path(:filter => { :organization_id => @site.organization.id })
    add_breadcrumb @site.name, site_path(@site)
    render '/devices/index'
  end

  def edit
    authorize @site
    add_breadcrumb @site.organization.name, sites_path(:filter => { :organization_id => @site.organization.id })
    add_breadcrumb @site.name, site_path(@site)
    add_breadcrumb "Edit Name", edit_site_path(@site)
  end

  def update
    authorize @site

    unless params[:site][:name].strip.match(/^default$/ix)
      @site.name = params[:site][:name].strip
      if @site.save
        flash[:success] = "Name Successfully Updated"
        redirect_to site_path(@site) and return
      else
        flash[:error] = "There was an error updating your site's name, please check any errors and try again"
      end
    else
      flash[:error] = "A site's name can not be \"default\", please try again"
    end
    add_breadcrumb @site.organization.name, sites_path(:filter => { :organization_id => @site.organization.id })
    add_breadcrumb @site.name, site_path(@site)
    add_breadcrumb "Edit Name", edit_site_path(@site)
    render "edit"
  end

  def destroy
    authorize @site, :delete?
    if @site.destroy
      flash[:success] = "Site was successfully deleted"
    else
      flash[:error] = "There was a problem deleting the selected site, plase try again"
    end
    redirect_to sites_path
  end

  def search
    render json: { results: site_search_results }, root: false
  end

  private

  def get_site
    @site = current_user.sites.readonly(false).find(params[:id])
  end

  def check_if_default
    if @site.is_default?
      flash[:error] = "Default sites cannot be changed."
      redirect_to sites_path and return
    end
  end

  def base_breadcrumbs
    add_breadcrumb "Home", "/"
    add_breadcrumb "Sites", sites_path
  end

  def not_available
    flash[:error] = "Action Not Available"
    redirect_to sites_path and return
  end


  def site_search_results
    SiteTypeaheadSearch.new(current_user, params[:query], params[:device_id]).execute
  end
end
