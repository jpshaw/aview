import Vue from 'vue';
import LiveClock from '../shared/live-clock.vue';

document.addEventListener('DOMContentLoaded', () => {
  let element = document.getElementById("live-clock")
  let timezone = element.getAttribute("data-timezone")
  const Clock = Vue.extend(LiveClock)
  var disClock = new Clock({
    propsData: {
      timezone: timezone
    }
  });

  disClock.$mount("#live-clock");
  
})