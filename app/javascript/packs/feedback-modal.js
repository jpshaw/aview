import FeedbackModal from '../modals/feedback-modal.vue';
import Vue from 'vue';

const Feedback = Vue.extend(FeedbackModal);

window.mountFeedbackModal = (el, isARMT) => {
  var view = new Feedback({
    propsData: {
    	armt: isARMT
    }
  });
  view.$mount(el);
}