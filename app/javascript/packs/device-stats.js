import DeviceStats from '../device-stats/device-stats.vue';
import Vue from 'vue';

const DeviceStatsExporter = Vue.extend(DeviceStats);

window.mountDeviceStats = (el) => {
	var vm = new DeviceStatsExporter().$mount(el);
}