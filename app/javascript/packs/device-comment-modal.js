import CommentModal from '../modals/device-comment-modal.vue';
import Vue from 'vue';

const DeviceCommentModal = Vue.extend(CommentModal);

window.mountCommentModal = (mac, comment, el) => {
  var view = new DeviceCommentModal({
    propsData: {
      mac: mac,
      comment: comment
    }
  });
  view.$mount(el);
}