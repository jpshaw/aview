import DeviceCommandBtns from '../device-command-menu/command-menu-buttons.vue';
import Vue from 'vue';

const CommandMenu = Vue.extend(DeviceCommandBtns);

window.createCommandMenu = (id, el) => {
  var commandView = new CommandMenu({
  	propsData: {
  		id: id
  	}
  });
  commandView.$mount(el);
};