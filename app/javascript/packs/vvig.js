import VvigChartContainer from '../vvig-pnc/vvig-container.vue';
import HighchartsVue from 'highcharts-vue';
import HighchartsLib from 'highcharts';
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(HighchartsVue, {HighchartsLib});
Vue.use(VueRouter);

const VvigCharts = Vue.extend(VvigChartContainer);

window.renderVvigPnc = (el, orgs) => {
	var vm = new VvigCharts({propsData: {organizations: orgs}}).$mount(el);
}