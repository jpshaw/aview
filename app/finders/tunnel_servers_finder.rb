class TunnelServersFinder < BaseFinder

  def execute
    scoped_collection(
      TunnelServer
        .joins(tunnel_servers_on_devices)
        .joins(organizations_on_devices)
    )
  end

  private

  def tunnel_servers_on_devices
    tunnel_servers
      .join(gateway_tunnels)
        .on(tunnel_servers[:id].eq(gateway_tunnels[:tunnel_server_id]))
      .join(devices)
        .on(gateway_tunnels[:device_id].eq(devices[:id]))
      .join_sources
  end

end
