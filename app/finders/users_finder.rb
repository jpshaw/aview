class UsersFinder < BaseFinder

  def execute
    scoped_collection(
      User.joins(organizations_on_users)
    )
  end

  private

  def organizations_on_users
    users
      .join(organizations)
      .on(users[:organization_id].eq(organizations[:id]))
      .join_sources
  end

end
