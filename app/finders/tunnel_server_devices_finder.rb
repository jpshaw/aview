class TunnelServerDevicesFinder < BaseFinder

  def execute
    scoped_collection(
      Gateway
        .joins(devices_on_tunnel_servers)
        .joins(organizations_on_devices)
        .where(
          gateway_tunnels[:tunnel_server_id].eq(params[:tunnel_server_id])
        )
    )
  end

  private

  def devices_on_tunnel_servers
    devices
      .join(gateway_tunnels)
        .on(devices[:id].eq(gateway_tunnels[:device_id]))
      .join(tunnel_servers)
        .on(gateway_tunnels[:tunnel_server_id].eq(tunnel_servers[:id]))
      .join_sources
  end

end
