class ConfigurationSchemaFinder
  def initialize(model, type)
    @cache_key = "schemas/#{model}/#{type}"
    @file_path = Rails.root.join('schemas', "#{model}-#{type}.schema").to_s
  end

  def execute
    return nil unless File.exist?(@file_path)

    Rails.cache.fetch(@cache_key, expires_in: 15.minutes) do
      JSON.parse(File.read(@file_path))
    end
  end
end
