class BaseFinder

  attr_reader :current_user, :params

  delegate :filter_organization_id, to: :current_user

  def initialize(current_user, params = {})
    @current_user = current_user
    @params       = params.dup
  end

  protected

  def scoped_collection(query)
    if params[:organization_id]
      filtered_objects(query, scoped_filter_id(params[:organization_id]))
    elsif filter_organization_id
      filtered_objects(query, scoped_filter_id(filter_organization_id))
    else
      managed_objects(query)
    end
  end

  def managed_organizations
    Organization
      .joins(organization_hierarchies_on_organizations)
      .where(manageable_organization_hierarchies)
      .uniq
  end

  def filtered_organizations
    scoped_collection Organization
  end

  def managed_objects(query)
    query
      .joins(organization_hierarchies_on_organizations)
      .where(manageable_organization_hierarchies)
      .uniq
  end

  def filtered_objects(query, filter_id)
    query
      .joins(organization_hierarchies_on_organizations)
      .where(filtered_organization_hierarchies(filter_id))
      .uniq
  end

  def manageable_organization_hierarchies
    organization_hierarchies[:ancestor_id].in(manageable_organization_ids)
  end

  def filtered_organization_hierarchies(filter_id)
    organization_hierarchies[:ancestor_id].eq(filter_id)
  end

  # Ensure user is authorized for the given filter
  def scoped_filter_id(filter_id)
    Rails.cache.fetch("#{current_user.id}/#{filter_id}/scoped_filter_id") do
      managed_organizations.select(:id).find_by(id: filter_id).try(:id)
    end
  end

  # TODO: Invalidate cache when user permissions are changed
  def manageable_organization_ids
    Rails.cache.fetch("permissions/#{current_user.id}/manageable_organization_ids", expires_in: 30.minutes) do
      Permission
        .where(user_manager_permissions)
        .where(organization_manageable_permissions)
        .pluck(:manageable_id)
    end
  end

  def organization_hierarchies_on_organizations
    organizations
      .join(organization_hierarchies)
      .on(organizations[:id].eq(organization_hierarchies[:descendant_id]))
      .join_sources
  end

  def organizations_on_devices
    devices
      .join(organizations)
      .on(devices[:organization_id].eq(organizations[:id]))
      .join_sources
  end

  def user_manager_permissions
    permissions[:manager_id]
      .eq(current_user.id)
      .and(permissions[:manager_type].eq(:User))
  end

  def organization_manageable_permissions
    permissions[:manageable_type].eq(:Organization)
  end

  def devices
    Device.arel_table
  end

  def device_events
    DeviceEvent.arel_table
  end

  def organizations
    Organization.arel_table
  end

  def permissions
    Permission.arel_table
  end

  def groups
    Group.arel_table
  end

  def sites
    Site.arel_table
  end

  def tunnel_servers
    TunnelServer.arel_table
  end

  def gateway_tunnels
    GatewayTunnel.arel_table
  end

  def device_configurations
    DeviceConfiguration.arel_table
  end

  def netbridge_configurations
    NetbridgeConfiguration.arel_table
  end

  def device_categories
    DeviceCategory.arel_table
  end

  def users
    User.arel_table
  end

  def organization_hierarchies
    Arel::Table.new(:organization_hierarchies)
  end

  def devices_tunnel_servers
    Arel::Table.new(:devices_tunnel_servers)
  end

end
