class DeviceByNetworkTypeFinder
  def initialize(params = {})
    @network = params[:network]
    @user = params[:user]
  end

  def execute
    results = init_collection
    @network ? by_network(results) : deployed_devices_per_network(results)
  end

  private

  def init_collection
    relation = @user ? @user.devices : Device.where(nil)
    relation.with_active_modem.deployed
  end

  def by_network(relation)
    case @network
      when '4G'
        relation.where('upper(device_modems.cnti) IN (?)', DeviceModem::FOUR_G_NETWORK)
      when '3G'
        relation.where('upper(device_modems.cnti) IN (?)', DeviceModem::THREE_G_NETWORK)
      when '2G'
        relation.where('upper(device_modems.cnti) IN (?)', DeviceModem::TWO_G_NETWORK)
      when 'unknown'
        relation.where('upper(device_modems.cnti) NOT IN (?)', DeviceModem::NETWORKS)
    end
  end

  def map_to_network_counts(counts)
    counts.map{|k,v| {cnti: k, network: DeviceModem.network_type(k), count: v}}.
      group_by{|x| x[:network]}.
      tap{|result| result.map{|network| result[network.first] = network.last.reduce(0){|sum, n| sum + n[:count]}}}
  end

  def deployed_devices_per_network(relation)
    map_to_network_counts(relation.group('device_modems.cnti').count)
  end
end
