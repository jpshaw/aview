class DeviceEventsFinder < BaseFinder

  def execute
    scoped_collection(
      DeviceEvent
        .from('`device_events` FORCE INDEX(PRIMARY)')
        .joins(devices_on_device_events)
        .joins(organizations_on_devices)
    )
  end

  private

  def devices_on_device_events
    device_events
      .join(devices)
      .on(device_events[:device_id].eq(devices[:id]))
      .join_sources
  end

end
