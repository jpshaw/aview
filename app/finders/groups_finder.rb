class GroupsFinder < BaseFinder

  def execute
    scoped_collection(
      Group.joins(organizations_on_groups)
    )
  end

  private

  def organizations_on_groups
    groups
      .join(organizations)
      .on(groups[:organization_id].eq(organizations[:id]))
      .join_sources
  end

end
