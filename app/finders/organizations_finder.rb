class OrganizationsFinder < BaseFinder

  def execute
    params[:all] ? managed_organizations : filtered_organizations
  end

end
