class DevicesFinder < BaseFinder

  def execute
    collection = init_collection
    collection = by_type(collection)
    collection = by_deployment(collection)
    collection = by_status(collection)
    collection = by_category(collection)
    collection = by_model(collection)
    collection
  end

  private

  def init_collection
    scoped_collection(
      Device.joins(organizations_on_devices)
    )
  end

  def by_type(devices)
    params[:type].present? ? devices.with_type(params[:type].to_s.classify) : devices
  end

  def by_deployment(devices)
    params[:deployment].present? ? devices.with_deployment(params[:deployment]) : devices
  end

  def by_status(devices)
    params[:status].present? ? devices.with_device_status(params[:status]) : devices
  end

  def by_category(devices)
    params[:categories].present? ? devices.with_categories(params[:categories]) : devices
  end

  def by_model(devices)
    params[:models].present? ? devices.with_models(params[:models]) : devices
  end

end
