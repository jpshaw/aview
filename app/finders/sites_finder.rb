class SitesFinder < BaseFinder

  def execute
    scoped_collection(
      Site.joins(organizations_on_sites)
    )
  end

  private

  def organizations_on_sites
    sites
      .join(organizations)
      .on(sites[:organization_id].eq(organizations[:id]))
      .join_sources
  end

end
