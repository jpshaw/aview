class DeviceConfigurationsFinder < BaseFinder

  def execute
    scoped_collection(
      DeviceConfiguration.joins(organizations_on_device_configurations)
    )
  end

  private

  def organizations_on_device_configurations
    device_configurations
      .join(organizations)
      .on(device_configurations[:organization_id].eq(organizations[:id]))
      .join_sources
  end

end
