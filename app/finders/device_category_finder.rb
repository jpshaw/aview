class DeviceCategoryFinder < BaseFinder

  def execute
    scoped_collection(
      DeviceCategory
        .joins(devices_on_categories)
        .joins(organizations_on_devices)
        .uniq
    )
  end

  private

  def devices_on_categories
    device_categories
      .join(devices)
      .on(device_categories[:id].eq(devices[:category_id]))
      .join_sources
  end

end
