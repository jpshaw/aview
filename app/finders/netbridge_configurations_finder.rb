class NetbridgeConfigurationsFinder < BaseFinder

  def execute
    scoped_collection(
      NetbridgeConfiguration.joins(organizations_on_netbridge_configurations)
    )
  end

  private

  def organizations_on_netbridge_configurations
    netbridge_configurations
      .join(organizations)
      .on(netbridge_configurations[:organization_id].eq(organizations[:id]))
      .join_sources
  end

end
