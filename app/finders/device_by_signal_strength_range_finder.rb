class DeviceBySignalStrengthRangeFinder
  def initialize(params = {})
    @user = params[:user]
    @range = params[:range].to_i if params[:range]
  end

  def execute
    results = init_collection
    results = by_deployment(results)
    @range ? by_signal_strength_range(results) : devices_per_signal_strength_range(results)
  end

  private

  def init_collection
    relation = @user ? @user.devices : Device.where(nil)
    relation.with_active_modem
  end

  def by_deployment(relation)
    @range ? relation : relation.deployed
  end

  def by_signal_strength_range(relation)
    if (0..9).include?(@range)
      signals = range_from_signal_strength(@range)
      relation.where('device_modems.signal BETWEEN ? AND ?', signals.first, signals.last)
    end
  end

  def devices_per_signal_strength_range(relation)
    buckets = relation.
      select("count(*) as device_count, case #{signal_strength_counts_by_range_case_statement} end as signal_range").
      where('device_modems.signal is not null').
      group('signal_range').
      order('signal_range')

    map_signal_strengths_counts_by_range(buckets)
  end

  def map_signal_strengths_counts_by_range(buckets)
    result = signal_strength_buckets.each_index.map do |i|
      bucket = buckets.detect{|bucket| bucket.signal_range.to_i == i}
      bucket ? bucket.device_count : 0
    end

    result = [] unless result.any?{|count| count != 0}
    result
  end

  def range_from_signal_strength(range)
    signal_strength_buckets[range]
  end

  def signal_strength_buckets
    @signal_strength_buckets ||= begin
      buckets = [[0, 9], [10, 19], [20, 29], [30, 39], [40, 49], [50, 59], [60, 69], [70, 79], [80, 89], [90, 100]]
      buckets.map! do |buckets|
        [
          DeviceModem.percent_to_signal(buckets.first).round,
          DeviceModem.percent_to_signal(buckets.last).round
        ]
      end
      remove_overlapping_signals(buckets)
    end
  end

  def signal_strength_counts_by_range_case_statement
    @signal_strength_counts_by_range_case_statement ||= begin
      signal_strength_buckets.each_with_index.map do |range, index|
        "when device_modems.signal between #{range.first} and #{range.last} then '#{index}'"
      end.join(' ')
    end
  end

  def previous_bucket_ceil_equal_to_floor?(previous_bucket, floor)
    previous_bucket.last == floor
  end

  def remove_overlapping_signals(buckets)
    buckets.each_with_index.map do|bucket, i|
      previous_bucket = buckets[i-1]
      floor = previous_bucket_ceil_equal_to_floor?(previous_bucket, bucket.first) ? bucket.first + 1 : bucket.first
      ceil = bucket.last
      [floor, ceil]
    end
  end
end
