class DataPlansDataTable < DataTables::Base
  include Exportable

  delegate :link_to, :number_to_human_size, :timestamp_in_users_timezone, :organization_data_plan_path,
           :edit_organization_data_plan_path, to: :@view

  def initialize(view_context, organization_id)
    super(view_context)
    @organization_id = organization_id
  end

  def as_csv
    get_raw_records.map { |record| exportable_data_row(record) }
  end
  alias_method :as_xlsx, :as_csv

  def as_json(options = {})
    {
        draw: params[:draw].to_i,
        recordsTotal:    get_raw_records.count(:all),
        recordsFiltered: get_raw_records.count(:all),
        data: data
    }
  end

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def exportable_data_row(record)
    {
    'Name' => record.name,
    'Number of Devices' => number_of_devices(record),
    'Data Limit' => Filesize.as_si(record.data_limit).pretty,
    'Data User' => data_used(record),
    'Individual Data Limit' => individual_data_limit(record),
    'Start Day' => record.cycle_start_day,
    'Overage Notification' => (record.notify_enabled ? 'Yes' : 'No'),
    'Created' => timestamp_in_users_timezone(record.created_at)
    }
  end

  def data_row(record)
    {
    '0' => show_link(record),
    '1' => number_of_devices(record),
    '2' => Filesize.as_si(record.data_limit).pretty,
    '3' => data_used(record),
    '4' => individual_data_limit(record),
    '5' => record.cycle_start_day,
    '6' => (record.notify_enabled ? 'Yes' : 'No'),
    '7' => timestamp_in_users_timezone(record.created_at),
    '8' => edit_button(record),
    '9' => delete_button(record),
    'DT_RowClass' => row_class_for(record)
    }
  end

  def get_raw_records
    @data_plans ||= DataPlan.for_organization(@organization_id)
  end

  def show_link(record)
    link_to(record.name, organization_data_plan_path(@organization_id, record))
  end

  def number_of_devices(record)
    Device.with_data_plan(record).size
  end

  def individual_data_limit(record)
    record.individual_data_limit ? Filesize.as_si(record.individual_data_limit).pretty : nil
  end

  def data_used(record)
    record.data_used ? Filesize.as_si(record.data_used).pretty : 0
  end

  def edit_button(record)
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
            edit_organization_data_plan_path(@organization_id, record, format: :js),
            class: 'btn btn-white btn-xs table-button-remote',
            id: "edit_data_plan_#{record.id}",
            title: 'Edit Data Plan',
            remote: true)
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
            organization_data_plan_path(@organization_id, record, format: :js),
            class: 'btn btn-danger btn-xs table-button-remote',
            id: "delete_data_plan_#{record.id}",
            method: :delete,
            title: 'Delete Data Plan',
            remote: true,
            data: {
                confirm: "Are you sure you want to delete this data plan?\n"\
                "There are #{number_of_devices(record)} devices associated with this configuration."
            }
    )
  end

  def row_class_for(record)
    return record.data_used && record.data_used > record.data_limit ? 'highlight-alert' : ''
  end
end