class TunnelServersDataTable < DataTables::Base

  delegate :tunnel_server_path, :ip_addresses_tunnel_server_path, to: :view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      search: @search,
      start: @start
    }
  end

  private

  def base_relation
    @base_relation ||= current_user.tunnel_servers
  end

  def data
    get_records.map do |record|
      {
        name: link_to(record.name, tunnel_server_path(record)),
        device_count: scoped_device_count(record),
        ip_addresses: ip_addresses_link(record),
        type: record.server_type,
        location: record.location,
      }
    end
  end

  def scoped_device_count(record)
    current_user.tunnel_server_devices(tunnel_server_id: record.id).count
  end

  def ip_addresses_link(record)
    count = record.ip_addresses.count

    if count > 0
      link_to(
        count,
        ip_addresses_tunnel_server_path(record, format: :js),
        class: 'single-button-remote',
        remote: true
      )
    else
      '0'
    end
  end

  def get_records(limit = @length)
    @total_records_count = base_relation.size
    filtered_records = base_relation.merge(TunnelServer.search!(search_params))
    @filtered_records_count = filtered_records.size
    @displayed_records = filtered_records.limit(limit).offset(@start)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if { |k,v| v.blank? }.symbolize_keys!
    end
  end

end
