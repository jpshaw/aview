class GroupsDataTable < DataTables::Base
  delegate :organization_path, :group_path, to: :view

  def as_json(options = {})
    {
                 data: data,
                 draw: params[:draw].to_i,
         recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
                start: @start
    }
  end

  private

  def data
    get_records_vs.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
            DT_RowId: "group_row_#{record.id}",
            checkbox: group_checkbox(record),
                name: group_link(record),
        organization: organization_link(record),
            category: record.device_model.category.name,
               model: record.device_model.name,
            firmware: record.firmware.version,
        device_count: record.devices.count,
      online_devices: online_devices(record)
    }
  end

  def preloads
    [:organization, { device_model: :category }, :firmware]
  end

  def group_checkbox(record)
    check_box_tag "group_ids[]", record.id, false, class: 'group-select', id: "group_ids_#{record.id}"
  end

  def group_link(record)
    link_to record.name, group_path(record)
  end

  def organization_link(record)
    link_to record.organization.name, organization_path(record.organization)
  end

  def online_devices(record)
    "#{record.devices.good.count} of #{record.devices.count}"
  end

  def get_records_vs
    @total_records_count = base_relation.uniq.size
    filtered_records = base_relation.merge(Group.search!(search_params)).uniq
    @filtered_records_count = filtered_records.size
    @displayed_records = filtered_records.limit(@length).offset(@start)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if { |k,v| v.blank? }.symbolize_keys!
    end
  end

  def base_relation
    @base_relation ||= current_user.groups.preload(preloads)
  end

end
