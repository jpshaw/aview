class PermissionDataTable < DataTables::Base
  include AbilityBadgeable

  delegate :h, :link_to, :url_for,
    :organization_role_path,
    :edit_organization_permission_path,
    :edit_user_permission_path,
    :organization_permission_path,
    :user_permission_path,
    :t, :i18n_set?, :content_tag,
    to: :@view

  attr_reader :manager

  def initialize(view_context, manager)
    @manager = manager
    super(view_context)
  end

  def as_json(options = {})
    {
        draw: params[:draw].to_i,
        recordsTotal:    get_raw_records.count(:all),
        recordsFiltered: get_raw_records.count(:all),
        data: data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    row = []
    row << record.manageable.name
    row << record.role.name
    row << generate_badges(record.allowed_abilities.to_a)

    row << link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
                   edit_manager_path(record),
                   class: 'btn btn-white btn-xs table-button-remote',
                   id: "edit_permission_#{record.id}",
                   title: 'Edit Permission',
                   remote: true)

    row << link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
                   manager_permission_path(record),
                   class: 'btn btn-danger btn-xs table-button-remote',
                   id: "delete_permission_#{record.id}",
                   remote: true,
                   title: 'Delete Permission',
                   method: :delete,
                   data: { confirm: "Are you sure you want to delete permission '#{record.manageable.name}'? This action cannot be undone." })
    row
  end

  def edit_manager_path(record)
    send("edit_#{manager_name}_permission_path", manager, record, format: :js)
  end

  def manager_permission_path(record)
    send("#{manager_name}_permission_path", manager, record, format: :js)
  end

  def manager_name
    @manager_name ||= @manager.class.to_s.underscore
  end

  def get_raw_records
    @permissions ||= @manager.manager_permissions
  end
end