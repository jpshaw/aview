class GroupDevicesDataTable < DataTables::Base

  delegate :device_path, :host_link, :site_path,
    :timestamp_in_users_timezone, to: :@view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      search: @search,
      start: @start
    }
  end

  private

  def data
    get_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
            DT_RowId: "device_row_#{record.id}",
            checkbox: device_checkbox(record),
  with_device_status: status_icon(record),
            with_mac: link_to(record.mac, device_path(record)),
              serial: record.serial,
                host: host_link(record),
                site: link_to(record.site_name, site_path(record.site)),
            firmware: record.firmware,
      last_heartbeat: timestamp_in_users_timezone(record.last_heartbeat_at)
    }
  end

  def device_checkbox(record)
    check_box_tag "device_ids[]", record.id, false, class: 'device-select', id: "device_ids_#{record.id}"
  end

  def status_icon(record)
    "<i class='#{record.status_indicator}' data-toggle='tooltip' data-placement='right' title='#{record.status_title}'></i><span class='sr-only'>#{record.status_title}</span>"
  end

  def get_records(limit = @length)
    @total_records_count = base_relation.uniq.size
    filtered_records = base_relation.merge(Device.search!(search_params)).uniq
    @filtered_records_count = filtered_records.size
    filtered_records.limit(limit).offset(@start)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if{ |k,v| v.blank? }.symbolize_keys!
    end
  end

  def base_relation
    @base_relation ||= Group.find(params[:group_id]).devices.preload(preloads)
  end

  def preloads
    [:organization, :category, :series, :site, location: :country]
  end
end
