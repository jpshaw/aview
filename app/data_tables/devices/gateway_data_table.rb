module Devices
  class GatewayDataTable < DevicesDataTable
    include GatewayHelper

    private

    def data
      super do |record, serialized_record|
        serialized_record.merge!(
          tunnels: get_tunnel_html(record),
          vrrp_state: record.vrrp_state,
          connection: (record.details ? record.details.primary_connection_type_name : '')
        )
      end
    end

    def map_to_csv(records)
      super do |record, serialized_record|
        serialized_record.merge!(
          'Tunnels' => record.tunnels_count,
          'VRRP' => record.vrrp_state,
          'Connection' => (record.details ? record.details.primary_connection_type_name : '')
        )
      end
    end

    def get_tunnel_html(record)
      tunnel_class = get_tunnel_class(record, {up: "badge badge-success", warning: "badge badge-warning", down: "badge badge-danger", default: ""})
      tunnel_text = get_tunnel_text(record)
      "<span class='#{tunnel_class}'>#{tunnel_text}</span>"
    end

    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::GATEWAY_CATEGORY).id
      super.merge(Device.with_categories category)
    end

    def add_preloads
      [:details]
    end
  end
end
