module Devices
  class CellularDataTable < NetbridgeDataTable
    private

    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::CELLULAR_CATEGORY).id
      # TODO: When super is called, it puts a dependency on the app having a 'legacy cellular' device category in the db.
      # I don't think this was intended or very intuitive, it would probably be best to remove that dependency.
      super.merge(Device.with_categories category)
    end

    def add_preloads
      [:modems]
    end
  end
end
