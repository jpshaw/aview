class TunnelServerDevicesDataTable < DevicesDataTable

  attr_reader :tunnel_server_id

  def initialize(view_context, tunnel_server_id = nil)
    @tunnel_server_id = tunnel_server_id
    super(view_context)
  end

  def base_relation
    @base_relation ||= begin
      relation = current_user.tunnel_server_devices(tunnel_server_id: tunnel_server_id)
      relation = relation.preload(preloads)
      relation = relation.search!(scope_params) if scope_params.present?
      relation
    end
  end

end
