class SiteDevicesDataTable < DevicesDataTable

  def initialize(view_context, site_id)
    @site_id = site_id
    super(view_context)
  end

  private

  def base_relation
    @base ||= Device.with_site(@site_id)
  end

end
