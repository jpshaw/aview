module Devices
  class RemoteManagerDataTable < DevicesDataTable
    private

    def data
      super do |record, serialized_record|
        if record.modem
          serialized_record.merge!(
            phone_number: record.modem.number,
            iccid: record.modem.iccid
          )
        else
          serialized_record.merge!(
            phone_number: '',
            iccid: ''
          )
        end
      end
    end

    def map_to_csv(records)
      super do |record, serialized_record|
        serialized_record.merge!(
          'Phone Number' => record.try(:modem).try(:number).to_s,
          'ICCID' => record.try(:modem).try(:iccid).to_s,
          'Hostname' => record.try(:hostname).to_s
        )
      end
    end

    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::REMOTE_MANAGER_CATEGORY).id
      super.merge(Device.with_categories category)
    end

    def add_preloads
      [:modems]
    end
  end
end
