module Devices
  class NetreachDataTable < DevicesDataTable
    private
    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::WIFI_CATEGORY).id
      super.merge(Device.with_categories category)
    end
  end
end
