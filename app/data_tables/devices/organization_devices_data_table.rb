class OrganizationDevicesDataTable < DevicesDataTable
  DEFAULT_HIERARCHY = 'true'

  def initialize(view_context, organization_id)
    @organization_id = organization_id
    super(view_context)
  end

  private

  def base_relation
    @base ||= Device.with_organization({ organization_id: @organization_id, include_hierarchy: include_hierarchy? })
  end

  #this can only be called once since it modifies the search_params as a side effect
  def include_hierarchy?
    if search_params[:include_hierarchy]
      value = search_params[:include_hierarchy]
      remove_invalid_search_scope
      return value
    else
      return DEFAULT_HIERARCHY
    end
  end

  def remove_invalid_search_scope
    search_params.except! :include_hierarchy
  end
end