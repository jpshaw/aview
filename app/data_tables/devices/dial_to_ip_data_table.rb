module Devices
  class DialToIpDataTable < DevicesDataTable
    private
    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::DIAL_TO_IP_CATEGORY).id
      super.merge(Device.with_categories category)
    end
  end
end
