class DataPlanDevicesDataTable < DevicesDataTable
  def initialize(view_context, data_plan_id)
    @data_plan_id = data_plan_id
    super(view_context)
  end

  def data
    super do |record, serialized_record|
      serialized_record.merge!(data_used: (record.modem.present? ? Filesize.as_si(record.modem.data_used).pretty : ''))
    end
  end

  private

  def base_relation
    @base ||= Device.with_data_plan(@data_plan_id)
  end
end