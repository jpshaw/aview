module Devices
  class UcpeDataTable < DevicesDataTable
    private

    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::UCPE_CATEGORY).id
      super.merge(Device.with_categories category)
    end
  end
end
