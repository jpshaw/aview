module Devices
  class NetbridgeDataTable < DevicesDataTable
    private

    def configuration_name(record)
      record.configuration.present? ? record.configuration.name : 'N/A'
    end

    def client_mac(record)
      record.client_mac if record.respond_to? :client_mac
    end

    def data
      # TODO: Refactor to use .try().to_s ?
      super do |record, serialized_record|
        if record.modem
          serialized_record.merge!(
            carrier: record.modem.carrier,
            sku: record.modem.sku,
            dbm: record.modem.signal,
            signal: record.modem.signal_pct,
            network: record.modem.cnti_details,
            phone_number: record.modem.number,
            configuration: configuration_name(record),
            client_mac: client_mac(record),
            last_restart: timestamp_in_users_timezone(record.last_restarted_at),
            apn: record.modem.apn,
            band: record.modem.band,
            cid: record.modem.cid,
            ecio: record.modem.ecio,
            esn: record.modem.esn,
            iccid: record.modem.iccid,
            imei: record.modem.imei,
            imsi: record.modem.imsi,
            lac: record.modem.lac,
            mcc: record.modem.mcc,
            mnc: record.modem.mnc,
            modem: record.modem.name,
            rsrp: record.modem.rsrp,
            rsrq: record.modem.rsrq,
            rssi: record.modem.rssi,
            sinr: record.modem.sinr,
            snr: record.modem.snr,
            usb_speed: record.modem.usb_speed,
            version: record.modem.revision
          )
        else
          serialized_record.merge!(
            carrier: '',
            sku: '',
            dbm: '',
            signal: '',
            network: '',
            phone_number: '',
            configuration: '',
            client_mac: '',
            last_restart: '',
            apn: '',
            band: '',
            cid: '',
            ecio: '',
            esn: '',
            iccid: '',
            imei: '',
            imsi: '',
            lac: '',
            mcc: '',
            mnc: '',
            modem: '',
            rsrp: '',
            rsrq: '',
            rssi: '',
            sinr: '',
            snr: '',
            usb_speed: '',
            version: ''
          )
        end
      end
    end

    def map_to_csv(records)
      super do |record, serialized_record|
        serialized_record.merge!(
          'Carrier' => record.try(:modem).try(:carrier).to_s,
          'Modem SKU' => record.try(:modem).try(:sku).to_s,
          'DBM' => record.try(:modem).try(:signal).to_s,
          'Signal' => record.try(:modem).try(:signal_pct).to_s,
          'Network' => record.try(:modem).try(:cnti_details).to_s,
          'Phone Number' => record.try(:modem).try(:number).to_s,
          'Configuration' => configuration_name(record),
          "Client #{t(:mac)}" => client_mac(record),
          'Last Restart' => timestamp_in_users_timezone(record.last_restarted_at),
          'APN' => record.try(:modem).try(:apn).to_s,
          'Band' => record.try(:modem).try(:band).to_s,
          'CID' => record.try(:modem).try(:cid).to_s,
          'ECIO' => record.try(:modem).try(:ecio).to_s,
          'ESN' => record.try(:modem).try(:esn).to_s,
          'ICCID' => record.try(:modem).try(:iccid).to_s,
          'IMEI' => record.try(:modem).try(:imei).to_s,
          'IMSI' => record.try(:modem).try(:imsi).to_s,
          'LAC' => record.try(:modem).try(:lac).to_s,
          'MCC' => record.try(:modem).try(:mcc).to_s,
          'MNC' => record.try(:modem).try(:mnc).to_s,
          'Modem' => record.try(:modem).try(:name).to_s,
          'RSRP' => record.try(:modem).try(:rsrp).to_s,
          'RSRQ' => record.try(:modem).try(:rsrq).to_s,
          'RSSI' => record.try(:modem).try(:rssi).to_s,
          'SINR' => record.try(:modem).try(:sinr).to_s,
          'SNR' => record.try(:modem).try(:snr).to_s,
          'Last Speed Test' => record.try(:modem).try(:speed_tests).try(:last).try(:formatted_results).to_s,
          'USB Speed' => record.try(:modem).try(:usb_speed).to_s,
          'Version' => record.try(:modem).try(:revision).to_s
        )
      end
    end

    def base_relation
      category = DeviceCategory.find_by(name: DeviceCategory::LEGACY_CELLULAR_CATEGORY).id
      super.merge(Device.with_categories category)
    end

    def add_preloads
      [:modems]
    end
  end
end
