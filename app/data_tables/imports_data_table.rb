class ImportsDataTable < DataTables::Base
  delegate :h, :link_to, :admin_import_path, :content_tag, :distance_of_time_in_words,
           :timestamp_in_users_timezone, to: :@view

  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data:            data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
      id: import_id(record),
      trigger: record.trigger.text,
      source: record.the_source_name.text,
      type: record.type.text,
      action: record.action.text,
      status: status_badge(record),
      total_time: total_time(record),
      started: timestamp_in_users_timezone(record.created_at),
      delete_button: remove_link(record)
    }
  end

  def import_id(import)
    if import.finished?
      link_to(import.id, admin_import_path(import, format: :js), remote: true, class: 'single-button-remote')
    else
      import.id
    end
  end

  def get_raw_records
    @imports ||= Import.all
  end

  def import_status_label_class(import)
    case import.status
    when "initializing"
      "info"
    when "running"
      "info"
    when "success"
      "success"
    else
      "important"
    end.prepend("label-")
  end

  def total_time(import)
    if import.finished?
      distance_of_time_in_words(import.created_at, import.finished_at,
                                :include_seconds =>true)
    else
      "N/A"
    end
  end

  def remove_link(import)
    link_to('<i class="fa fa-trash"></i> Remove'.html_safe,
            admin_import_path(import),
            class: 'btn btn-danger btn-xs table-button-remote',
            id: "delete_service_#{import.id}",
            remote: true,
            title: 'Delete Import',
            method: :delete,
            data: { confirm: 'Are you sure you want to delete this import?' })
  end

  def status_badge(record)
    content_tag(:span, record.status.text,
                :class => "label #{import_status_label_class(record)}")
  end
end
