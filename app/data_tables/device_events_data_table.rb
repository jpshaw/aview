class DeviceEventsDataTable < DataTables::Base
  include Exportable::Batchable

  attr_accessor :name, :description
  delegate :h, :link_to, :timestamp_in_users_timezone, :current_user, to: :@view


  def initialize(view_context)
    super(view_context)
    @export_limit_type = export_limit_type
    @export_limit = export_limit
  end

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsFiltered: @verified_records_count,
      start: @start
    }
  end

  def as_csv
    export_records_in_batches(export_options) { |records| map_to_csv(records) }
  end
  alias_method :as_xlsx, :as_csv

  private

  def data
    get_records_vs.map do |record|
      {
          type: record.type,
          level: record.lvl,
          information: information_link(record),
          created_at: created_at(record)
       }
    end
  end

  def created_at(record)
    "<span style='white-space: nowrap;'>#{timestamp_in_users_timezone(record.created_at)}</span>"
  end

  def information_link(record)
    "<a href='#' id=#{record.id} class='information' data-toggle='modal' data-target='#event-information-modal'
      data-content='#{get_data_content(record)}'>#{record.information}</a>"
  end

  def get_data_content(record)
    if record.raw_data
      "<pre>#{JSON.pretty_generate(record.raw_data)}</pre>"
    else
      "<pre>#{record.information}</pre>"
    end
  end

  def export_records_for_batch_processing
    get_filtered_records.limit(export_limit).offset(export_offset)
  end

  def get_filtered_records
    params[:mac] ? get_filtered_records_for_device : get_filtered_records_for_user
  end

  def get_filtered_records_for_device
    device = current_user.devices.find_by(mac: params[:mac])

    if device
      device.events.search!(search_params) || DeviceEvent.none
    else
      DeviceEvent.none
    end
  end

  def get_filtered_records_for_user
    current_user.device_events.search!(search_params)
  end

  def get_records_vs
    get_records_with_data_table_next_button_fix(get_filtered_records)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if{ |k,v| v.blank? }.symbolize_keys!
      params = { sort_by_created_at: "desc" } if params.blank?
      params
    end
  end

  def get_records_with_data_table_next_button_fix(filtered_records)
    displayed_records_with_pagination_record = filtered_records.limit(@length + 1).offset(@start).to_a
    @verified_records_count = @start + displayed_records_with_pagination_record.size
    displayed_records_with_pagination_record.first(@length)
  end

  def map_to_csv(records)
    records.map do |record|
      {
          t(:mac) => record.device.mac,
          'Category' => record.device.category.name,
          'Model' => record.device.series.name,
          'Organization' => record.device.organization.name,
          'Site' => record.device.site_name,
          'Type' => record.type,
          'Level' =>record.lvl,
          'Information' => record.info,
          'Created' => timestamp_in_users_timezone(record.created_at),
          'Raw Data' => record.raw_data
      }
    end
  end

  def export_options
    @options ||= {
        klass: 'DeviceEvent',
    }
  end
end
