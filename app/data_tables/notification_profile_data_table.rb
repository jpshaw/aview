class NotificationProfileDataTable

   delegate :params, :h, :link_to,
    :timestamp_in_users_timezone, :current_user, :publisher_link,
    :api_v2_account_notification_profile_path,
    :edit_api_v2_account_notification_profile_path,
    :subscriptions_api_v2_account_notification_profile_path,
    :current_user_from_params,
    to: :@view

   def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data:            data
    }
  end

  private

  def data
    get_raw_records.by_created_at.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
      DT_RowId: "profile_row_#{record.id}",
      name: record.name,
      frequency: NotificationProfile::Frequencies.select_list.to_h.key(record.frequency),
      subscribed_events_count: record.subscribed_events.count,
      subscription_count: subscriptions_button(record),
      created_at: timestamp_in_users_timezone(record.created_at),
      edit_button: edit_button(record),
      delete_button: delete_button(record)
    }
  end

  def user
    @user ||= current_user_from_params
  end

  def get_raw_records
    user.notification_profiles
  end

  def subscriptions_button(record)
    link_to(
      record.subscriptions.count,
      subscriptions_api_v2_account_notification_profile_path(user, record, format: :js, profile_id: record.id),
      remote: true
    )
  end

  def edit_button(record)
    link_to(
      '<i class="fa fa-edit"> </i> Edit'.html_safe,
      edit_api_v2_account_notification_profile_path(user, record, format: :js),
      class: 'btn btn-white btn-xs table-button-remote',
      id: "edit_profile_#{record.id}",
      remote: true
    )
  end

  def delete_button(record)
    link_to(
      '<i class="fa fa-trash"> </i> Delete'.html_safe,
      api_v2_account_notification_profile_path(user, record, format: :js),
      class: 'btn btn-danger btn-xs table-button-remote',
      id: "delete_profile_#{record.id}",
      remote: true,
      method: :delete,
      data: { confirm: 'Are you sure you want to delete this profile?' }
    )
  end

end
