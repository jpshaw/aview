class UsersDataTable < DataTables::Base
  include Exportable::Batchable

  delegate :params, :h, :link_to, :organization_path,
    :timestamp_in_users_timezone, :current_user, :publisher_link,
    :user_path,
    :api_v2_user_path,
    to: :@view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: base_relation.count,
      recordsFiltered: records.count,
      search: @search,
      start: @start
    }
  end

  def as_csv
    export_records_in_batches(export_options) { |records| map_to_csv(records) }
  end
  alias_method :as_xlsx, :as_csv

  private

  def data
    @data ||= begin
      records.map do |record|
        data_row(record)
      end
    end
  end

  def data_row(record)
    {
      DT_RowId: "user_row_#{record.id}",
      name: (record.name.present? ? link_to(record.name, user_path(record)) : ''),
      email: link_to(record.email, user_path(record)),
      organization: link_to(record.organization.name, organization_path(record.organization)),
      timezone: record.timezone.name,
      last_login: timestamp_in_users_timezone(record.last_sign_in_at),
      status: record.status,
      edit: edit_button(record),
      delete: delete_button(record)
    }
  end

  def edit_button(record)
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
                 user_path(record),
                 class: 'btn btn-white btn-xs',
                 id: "user_#{record.id}")
  end

  def export_records_for_batch_processing
    records(export_limit).offset(export_offset)
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
                 api_v2_user_path(record, format: :js, reference: :index),
                 class: 'btn btn-danger btn-xs table-button-remote',
                 id: "delete_user_#{record.id}",
                 remote: true,
                 method: :delete,
                 data: { confirm: 'Are you sure you want to delete this user?' })
  end

  def export_options
    @options ||= {
      klass: 'User'
    }
  end

  def map_to_csv(records)
    records.map do |record|
      {
        'Name' => record.name,
        'Email' => record.email,
        'Organization' => record.organization.name,
        'Timezone' => record.timezone.name,
        'Last Login' => timestamp_in_users_timezone(record.last_sign_in_at),
        'Status' => record.status
      }
    end
  end

  def records(limit = @length)
    @records ||= begin
      query = base_relation.preload(:timezone, :organization).merge(User.search!(search_params)) || []
      @total_records_count = query.count
      query = query.limit(limit).offset(@start)
      @filtered_records_count = query.size
      query
    end
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if{ |k,v| v.blank? }.symbolize_keys!
    end
  end

  def base_relation
    @base_relation ||= current_user.users
  end

end
