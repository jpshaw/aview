class ReportDataTable
  include Exportable

  attr_reader :column_headers, :column_values, :report

  def initialize(options)
    @report = options[:report]
    @column_headers = parse_column_headers(options[:column_headers])
    @column_values = parse_column_values(options[:column_values])
  end

  def as_csv
    column_values.map{|row| Hash[row.each_with_index.map{ |v,i| [column_headers[i], v ]}]}
  end
  alias_method :as_xlsx, :as_csv

  private

  def headers_from_file
    report.stats_headers ? static_headers + report.stats_headers.values : static_headers
  end

  def parse_column_headers(column_headers)
    column_headers ? JSON.parse(column_headers) : headers_from_file
  end

  def parse_column_values(column_values)
    column_values ? JSON.parse(column_values): parse_table
  end

  def parse_table
    report.table.map do |row|
      row.first(static_headers.size) + parse_stats_details(row.last)
    end
  end

  def parse_stats_details(stats_details)
    return [] if report.stats_headers.blank?
    report.stats_headers.keys.map{|key| stats_details[key]}
  end

  def static_headers
    [I18n.t(:mac), I18n.t(:primary_ip), 'Category', 'Model','Organization', 'Site', 'Deployment', 'Last Activity']
  end
end
