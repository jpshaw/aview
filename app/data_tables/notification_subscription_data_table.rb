class NotificationSubscriptionDataTable

  delegate :params, :h, :link_to,
    :timestamp_in_users_timezone, :current_user, :publisher_link,
    :api_v2_account_notification_subscription_path,
    :edit_api_v2_account_notification_subscription_path,
    :edit_api_v2_account_notification_profile_path,
    :current_user_from_params,
    to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data:            data
    }
  end

  private

  def data
    get_raw_records.by_created_at.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
      DT_RowId: "subscription_row_#{record.id}",
      name: publisher_link(record),
      devices: device_count(record.publisher),
      profile: profile_button(record),
      last_day: record.notification_delivery_count_for_last_day,
      last_week: record.notification_delivery_count_for_last_week,
      last_month: record.notification_delivery_count_for_last_month,
      created_at: timestamp_in_users_timezone(record.created_at),
      edit_button: edit_button(record),
      delete_button: delete_button(record)
    }
  end

  def device_count(publisher)
    if publisher.is_a?(Organization)
      publisher.self_and_descendant_devices.uniq.count
    else
      0
    end
  end

  def get_raw_records
    records = \
      if organization_type?
        user.notification_subscriptions.organizations
      elsif device_type?
        user.notification_subscriptions.devices
      else
        user.notification_subscriptions
      end

    # Add profile filtering
    records = records.where(profile_id: params[:profile_id]) if params[:profile_id].present?

    records
  end

  def user
    @user ||= current_user_from_params
  end

  def organization_type?
    params[:publisher_type] == NotificationSubscription::Types::ORGANIZATION
  end

  def device_type?
    params[:publisher_type] == NotificationSubscription::Types::DEVICE
  end

  def profile_button(record)
    link_to(
      record.profile.name,
      edit_api_v2_account_notification_profile_path(user, record.profile, format: :js),
      class: "table-anchor-remote subscription-profile-#{record.profile.id}",
      remote: true
    )
  end

  def edit_button(record)
    link_to(
      '<i class="fa fa-edit"> </i> Edit'.html_safe,
      edit_api_v2_account_notification_subscription_path(user, record, format: :js),
      class: 'btn btn-white btn-xs table-button-remote',
      id: "edit_subscription_#{record.id}",
      remote: true
    )
  end

  def delete_button(record)
    link_to(
      '<i class="fa fa-trash"> </i> Delete'.html_safe,
      api_v2_account_notification_subscription_path(user, record, format: :js),
      class: 'btn btn-danger btn-xs table-button-remote',
      id: "delete_subscription_#{record.id}",
      remote: true,
      method: :delete,
      data: { confirm: 'Are you sure you want to delete this subscription?' }
    )
  end

end
