class NotificationsDataTable < DeviceEventsDataTable
  include Exportable::Batchable

  def as_csv
    export_records_in_batches(export_options) { |records| map_to_csv(records) }
  end
  alias_method :as_xlsx, :as_csv

  private

  def data
    get_records_vs.map do |record|
      {
          mac: link_to(record.device.mac, @view.device_path(record.device)),
          categories: record.device.category_name,
          models: record.device.series_name,
          organization: link_to(
            record.device.organization_name,
            @view.organization_path(record.device.organization)
          ),
          site: link_to(record.device.site_name, @view.site_path(record.device.site)),
          type: record.type,
          level: record.lvl,
          information: information_link(record),
          created_at: timestamp_in_users_timezone(record.delivered_at)
      }
    end
  end

  def get_filtered_records
    current_user.notifications.search!(search_params).joins(:device)
  end

  def get_records_vs
    @displayed_records = get_records_with_data_table_next_button_fix(get_filtered_records)
  end

  def map_to_csv(records)
    records.map do |record|
      {
          t(:mac) => record.device.mac,
          'Category' => record.device.category_name,
          'Model' => record.device.series_name,
          'Organization' => record.device.organization_name,
          'Site' => record.device.site_name,
          'Type' => record.type,
          'Level' => record.lvl,
          'Information' => record.message,
          'Created_at' => record.delivered_at
      }
    end
  end

  def export_options
    @options ||= {
        klass: 'Notification',
    }
  end

  def information_link(record)
    "<a href='#' id=#{record.id} class='information' data-toggle='modal' data-target='#event-information-modal'
      data-content='#{get_data_content(record)}'>#{record.message}</a>"
  end

  def get_data_content(record)
    content =
      if record.data
        JSON.pretty_generate(record.data)
      else
        record.message
      end

    "<pre>#{content}</pre>"
  end
end
