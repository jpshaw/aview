class SearchesDataTable < DevicesDataTable

  def initialize(view_context, devices)
    @devices = devices
    super(view_context)
  end

  def get_records(limit = @length)
    @total_records_count = @devices.size
    @filtered_records_count = @total_records_count
    sort_relation = Device.search!(sort_params)
    @displayed_records = @devices.merge(sort_relation).limit(limit).offset(@start)
  end

  def sort_params
    @sort_params ||= sort_params_vs.delete_if{ |k,v| v.blank? }.symbolize_keys!
  end
end
