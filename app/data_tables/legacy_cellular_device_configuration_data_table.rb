class LegacyCellularDeviceConfigurationDataTable < DataTables::Base
  delegate :h, :link_to, :timestamp_in_users_timezone, :current_user,
    :edit_legacy_configuration_path,
    :legacy_configuration_path,
    to: :@view

  def as_json(options = {})
    { data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      start: @start }
  end

  private

  def data
    get_records_vs.map do |record|
      {
          with_name: link_to(record.name, @view.legacy_configuration_path(record)),
          with_organization: link_to(record.organization.name, @view.organization_path(record.organization)),
          sort_by_updated_at: timestamp_in_users_timezone(record.updated_at),
          edit: edit_button(record),
          delete: delete_button(record)
       }
    end
  end

  def get_records_vs
    search_params = @columns.present? ? column_search_params_vs : {}
    search_params.merge!(table_search_params_vs).merge!(sort_params_vs)

    search_params.delete_if { |k,v| v.blank? }.symbolize_keys!

    @total_records_count = base_relation.size
    filtered_records = base_relation.merge(NetbridgeConfiguration.search!(search_params))
    @filtered_records_count = filtered_records.size
    @displayed_records = filtered_records.limit(@length).offset(@start)
  end

  def edit_button(record)
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
                 edit_legacy_configuration_path(record),
                 class: 'btn btn-white btn-xs',
                 title: 'Edit Device Configuration',
                 id: "configuration_#{record.id}")
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
            legacy_configuration_path(record, format: :js),
            class: 'btn btn-danger btn-xs table-button-remote',
            id: "delete_configuration_#{record.id}",
            remote: true,
            method: :delete,
            data: {
                confirm: "Are you sure you want to delete this configuration?\n"\
                "There are #{record.devices.count} devices associated with this configuration."
            }
    )
  end

  def base_relation
    @base_relation ||= current_user.netbridge_configurations.preload(:organization)
  end
end
