class DnsDataTable < DataTables::Base
  delegate :h, :link_to, :edit_admin_dns_path, :admin_dns_path, to: :view

  def initialize(view, type)
    super(view)
    @type = type.to_sym
  end
  
  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.size,
      recordsFiltered: get_raw_records.size,
      data:            data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    row = []
    row << record.name
    row << record.type if type_is_domain?
    row << record.domain.try(:name) unless type_is_domain?
    row << record.content unless type_is_domain?
    row << record.ttl unless type_is_domain?
    row << edit_link(record)
    row << delete_link(record)
    row
  end

  def get_raw_records
    return @domains ||= PowerDnsRecordManager::Domain.all.to_a if type_is_domain?
    return @soas ||= PowerDnsRecordManager::Record.soa.to_a if type_is_soa?
    return @nameservers ||= PowerDnsRecordManager::Record.ns.to_a if type_is_nameserver?
  end

  def edit_link domain
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
            edit_admin_dns_path(domain, entry_type: @type),
            class: 'btn btn-white btn-xs table-button-remote',
            title: edit_button_title,
            remote: true)
  end

  def edit_button_title
    return 'Edit Domain' if type_is_domain?
    return 'Edit SOA' if type_is_soa?
    'Edit Nameserver'
  end

  def delete_link domain
    link_to('<i class="fa fa-trash"></i> Delete'.html_safe,
            admin_dns_path(domain, entry_type: @type),
            class: 'btn btn-danger btn-xs table-button-remote',
            title: delete_button_title,
            method: :delete,
            remote: true,
            data: { confirm: delete_confirmation })
  end

  def delete_button_title
    return 'Delete Domain' if type_is_domain?
    return 'Delete SOA' if type_is_soa?
    'Delete Nameserver'
  end

  def delete_confirmation
    case @type
      when :domain
        'Are you sure you want to delete this domain? All SOA and nameserver entries associated with the domain will be deleted as well.'
      when :soa
        'Are you sure you want to delete this SOA?'
      when :nameserver
        'Are you sure you want to delete this nameserver?'
    end
  end

  def type_is_domain?
    @type == :domain
  end

  def type_is_soa?
    @type == :soa
  end

  def type_is_nameserver?
    @type == :nameserver
  end
end