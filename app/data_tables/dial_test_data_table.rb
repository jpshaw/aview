class DialTestDataTable
  delegate :params, :api_v3_device_dial_test_path, :t,
           :timestamp_in_users_timezone, to: :@view

  def initialize(device, view)
    @device = device
    @view = view
  end

  def as_json(options = {})
    {
      draw: params[:draw].to_i,
      recordsTotal: raw_records.count(:all),
      recordsFiltered: raw_records.count(:all),
      data: data
    }
  end

  private

  def data
    raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    row = []

    row << Gateway::DIAL_TEST_RESULT[record.result]
    row << record.reason
    row << Gateway::DIAL_PROBLEMS[record.category]
    row << Gateway::DIAL_TEST_SOURCE[record.source]
    row << timestamp_in_users_timezone(record.created_at)

    row
  end

  def raw_records
    @raw_records ||= @device.dial_test_results.order(id: :desc)
  end
end
