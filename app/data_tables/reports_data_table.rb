class ReportsDataTable < DataTables::Base

   delegate :params, :h, :link_to, :button_to, :content_tag,
    :timestamp_in_users_timezone, :current_user,
    :report_path, :export_report_path,
    :distance_of_time_in_words,
    to: :@view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: records.count,
      recordsFiltered: records.count,
      search: @search,
      start: @start
    }
  end

  private

  def data
    @data ||= begin
      records.map do |record|
        data_row(record)
      end
    end
  end

  def data_row(record)
    {
      DT_RowId: "report_#{record.id}",
      id: link_to(record.id, report_path(record)),
      type: link_to(record.title, report_path(record)),
      status: record.status,
      organization: record.organization,
      generated_in: generated_in(record),
      created_at: timestamp_in_users_timezone(record.created_at),
      download: download_button(record),
      delete: delete_button(record)
    }
  end

  def generated_in(record)
    if record.is_done?
      distance_of_time_in_words(record.created_at, record.finished_at, include_seconds: true)
    else
      'N/A'
    end
  end

  def download_button(record)
    if record.is_done?
      field = link_to export_report_path(record, format: :csv), method: :post, class: 'btn btn-primary btn-xs', id: 'csv-export' do
        '<i class="fa fa-file-text-o"></i>'.html_safe + ' CSV'
      end.html_safe
      field += ' '
      field += link_to export_report_path(record, format: :xlsx), method: :post, class: 'btn btn-primary btn-xs', id: 'xlsx-export' do
        '<i class="fa fa-file-excel-o"></i>'.html_safe + ' Excel'
      end.html_safe
      field
    else
      'N/A'
    end
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
                 report_path(record, format: :js),
                 class: 'btn btn-danger btn-xs table-button-remote',
                 id: "delete_report_#{record.id}",
                 method: :delete,
                 remote: true,
                 data: { confirm: 'Are you sure you want to delete this report?' })
  end

  def records
    @records ||= base_relation
  end

  def base_relation
    @base_relation ||= current_user.reports.order(:created_at).reverse
  end

end
