class RoleDataTable < DataTables::Base
  include AbilityBadgeable

  delegate :h, :link_to, :edit_organization_role_path, :organization_role_path,
           :i18n_set?, :t, :content_tag, to: :@view

  def initialize(view_context, organization)
    @organization = organization
    super(view_context)
  end

  def as_json(options = {})
    {
        draw: params[:draw].to_i,
        recordsTotal:    get_raw_records.count(:all),
        recordsFiltered: get_raw_records.count(:all),
        data: data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    row = []

    row << record.name
    row << record.description
    row << generate_badges(record.abilities)
    row << link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
                   edit_organization_role_path(@organization, record, format: :js),
                   class: 'btn btn-white btn-xs table-button-remote',
                   id: "edit_role_#{record.id}",
                   title: "Edit Role",
                   remote: true)
    row << link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
                   organization_role_path(@organization, record, format: :js),
                   class: 'btn btn-danger btn-xs table-button-remote',
                   id: "delete_role_#{record.id}",
                   remote: true,
                   :title => 'Delete Role',
                   :method => :delete,
                   data: { confirm: ("Are you sure you want to delete role '#{record.name}'? This will remove any permissions associated with the role for any users and organizations. This action cannot be undone.") })
    row
  end

  def get_raw_records
    @organization.roles
  end
end