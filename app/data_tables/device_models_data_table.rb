class DeviceModelsDataTable < DataTables::Base
  def as_json(options = {})
    {
        draw:            params[:draw].to_i,
        recordsTotal:    get_raw_records.count(:all),
        recordsFiltered: get_raw_records.count(:all),
        data:            data
    }
  end

  private

  def data
    get_records.map { |record| data_row(record) }
  end

  def data_row(record)
    {
        name: record.name,
        category: record.category.name,
        device_count: record.devices.count,
        firmware_count: firmware_count(record)
    }
  end

  def firmware_count(record)
    count = record.device_firmwares.count
    if count > 0
      link_to(record.device_firmwares.count, @view.device_model_device_firmwares_path(record), remote: true)
    else
      count
    end
  end

  def get_raw_records
    @imports ||= DeviceModel.includes(:category).all
  end

  def get_records
    get_raw_records
  end
end