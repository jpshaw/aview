class DevicesDataTable < DataTables::Base
  include Exportable::Batchable

  # Required for use with data_table_factory.rb in development
  Dir["app/data_tables/devices/*.rb"].each { |file| require_dependency "devices/#{file.split('/').last}" }

  delegate :params, :h, :link_to, :truncate,
           :timestamp_in_users_timezone, :current_user, :publisher_link, to: :@view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      search: @search,
      start: @start
    }
  end

  def as_csv
    export_records_in_batches(export_options) { |records| map_to_csv(records) }
  end
  alias_method :as_xlsx, :as_csv

  private

  def data
    get_records.map do |record|
      {
        with_device_status: "<i class='#{record.status_indicator}' data-toggle='tooltip' data-placement='right' title='#{record.status_title}'></i><span class='sr-only'>#{record.status_title}</span>",
        with_mac: link_to(record.mac, @view.device_path(record)),
        comment: "<i data-id='#{record.mac}' data-toggle='tooltip' data-placement='right' data-container='body' title='#{html_escape(record.comment)}' class='fa fa-comment #{device_comment_active_class(record)} edit-comment comment-icon'></i>",
        serial: record.serial,
        host: @view.host_link(record),
        mgmt_host: record.mgmt_host,
        hostname: record.hostname,
        with_categories: record.category_name,
        with_models: record.series_name,
        with_organization: @view.link_to_object_show(:name, :organization_path, record.organization),
        site: @view.link_to_object_show(:name, :site_path, record.site),
        firmware: record.firmware,
        city: (record.location ? record.location.city : ''),
        country: record.location.try(:country).try(:code),
        activated_at: timestamp_in_users_timezone(record.activated_at),
        last_heartbeat: timestamp_in_users_timezone(record.last_heartbeat_at)
       }.tap{ |serialized_record| yield record, serialized_record if block_given? }
    end
  end

  def device_comment_active_class(record)
    record.comment.present? ? "active" : ""
  end

  def export_records_for_batch_processing
    get_records(export_limit).offset(export_offset)
  end

  def get_records(limit = @length)
    @total_records_count = base_relation.size
    filtered_records = base_relation.merge(Device.search!(search_params))
    @filtered_records_count = filtered_records.size
    filtered_records.limit(limit).offset(@start)
  end

  def scope_params
    params.permit(:with_deployment, :with_device_status, :with_status, :with_network, :with_signal_range)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if{ |k,v| v.blank? }.symbolize_keys!
    end
  end

  def base_relation
    @base_relation ||= begin
      relation = current_user.devices.preload(preloads)
      relation = relation.search!(scope_params) if scope_params.present?
      relation
    end
  end

  def map_to_csv(records)
    records.map do |record|
      {
          'Status' => record.status_title,
          t(:mac) => record.mac,
          'Serial Number' => record.serial,
          t(:primary_ip) => record.host.to_s.gsub(/^=/, ''),
          'Management IP' => record.mgmt_host,
          'Category' => record.category.name,
          'Model' => record.series.name,
          'Organization' => record.organization.name,
          'Site' => record.site_name,
          'Firmware' => record.firmware,
          'City' => (record.location ? record.location.city : ''),
          'Country' => (record.location ? record.location.try(:country).try(:code) : ''),
          'Activation Date' =>timestamp_in_users_timezone(record.activated_at),
          'Last Heartbeat' =>timestamp_in_users_timezone(record.last_heartbeat_at)
      }.tap{ |serialized_record| yield record, serialized_record if block_given? }
    end
  end

  def export_options
    @export_options ||= {
        klass: 'Device',
        preloads: preloads
    }
  end

  def preloads
    @preloads ||= begin
      preloads = [:organization, :category, :series, :site, location: :country]
      preloads += add_preloads
      preloads
    end
  end

  def add_preloads
    []
  end
end
