class ContactDataTable
  delegate :params, :current_user, :api_v3_device_contact_path, :t,
           :link_to, :can_modify_device_contacts?, to: :@view

  def initialize(device, view)
    @device = device
    @view = view
  end

  def as_json(options = {})
    {
      draw: params[:draw].to_i,
      recordsTotal: get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data: data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    row = []

    row << record.label_name
    row << record.name
    row << record.phone
    row << record.email

    if can_modify_device_contacts?
      row << link_to('<i class="fa fa-pencil"> </i> Edit'.html_safe,
                     api_v3_device_contact_path(@device.mac, record.id, format: :js),
                     class: 'btn btn-success btn-xs table-button-remote',
                     id: "update_contact_#{record.id}",
                     remote: true)
      row << link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
                     api_v3_device_contact_path(@device.mac, record.id, format: :js),
                     class: 'btn btn-danger btn-xs table-button-remote',
                     id: "delete_contact_#{record.id}",
                     remote: true,
                     method: :delete,
                     data: { confirm: t('contacts.delete') })
    end

    row
  end

  def get_raw_records
    @device.contacts
  end
end
