class SitesDataTable < DataTables::Base
  include Exportable::Batchable

  delegate :params, :h, :link_to, :timestamp_in_users_timezone, :current_user, :site_path,
           :edit_api_v2_site_path, :api_v2_site_path, :organization_path, :user_timezone, to: :@view

  def as_json(options = {})
    {
      data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      search: @search,
      start: @start
    }
  end

  def as_csv
    export_records_in_batches(export_options) { |records| map_to_csv(records) }
  end
  alias_method :as_xlsx, :as_csv

  private

  def data
    get_records.map do |record|
      {
        with_names: link_to(record.name, site_path(record)),
        with_organization: link_to(record.organization.name, organization_path(record.organization)),
        site_devices_count: record.device_count,
        updated_at: timestamp_in_users_timezone(record.updated_at),
        created_at: timestamp_in_users_timezone(record.created_at),
        edit: edit_button(record),
        delete: delete_button(record)
       }.tap{ |serialized_record| yield record, serialized_record if block_given? }
    end
  end

  def export_records_for_batch_processing
    get_records(export_limit).offset(export_offset)
  end

  def get_records(limit = @length)
    @total_records_count = base_relation.size
    filtered_records = base_relation.with_device_counts.merge(Site.search!(search_params))
    #size returns a hash for grouped queries, so the value we're now looking for is the length of this hash
    @filtered_records_count = filtered_records.reorder('').size.length
    @displayed_records = filtered_records.limit(limit).offset(@start)
  end

  def search_params
    @search_params ||= begin
      params = @columns.present? ? column_search_params_vs : {}
      params.merge!(table_search_params_vs).merge!(sort_params_vs)
      params.delete_if { |k,v| v.blank? }.symbolize_keys!
    end
  end

  def edit_button(record)
    if record.is_default?
      link_to(
        '<i class="fa fa-edit"> </i> Edit'.html_safe,
        '#',
        class: 'btn btn-white btn-xs',
        id: "edit_site_#{record.id}",
        title: 'Cannot edit a default site',
        disabled: true
      )
    else
      link_to(
        '<i class="fa fa-edit"> </i> Edit'.html_safe,
        edit_api_v2_site_path(record, format: :js),
        class: 'btn btn-white btn-xs table-button-remote',
        id: "edit_site_#{record.id}",
        remote: true
      )
    end
  end


  def delete_button(record)
    if record.is_default?
      link_to(
        '<i class="fa fa-trash"> </i> Delete'.html_safe,
        '#',
        class: 'btn btn-danger btn-xs',
        id: "delete_site_#{record.id}",
        title: 'Cannot delete a default site',
        disabled: true
      )
    else
      link_to(
        '<i class="fa fa-trash"> </i> Delete'.html_safe,
        api_v2_site_path(record, format: :js),
        class: 'btn btn-danger btn-xs table-button-remote',
        id: "delete_site_#{record.id}",
        remote: true,
        method: :delete,
        data: { confirm: 'Are you sure you want to delete this site?' }
      )
    end
  end

  def base_relation
    @base_relation ||= current_user.sites.preload(:organization)
  end

  def map_to_csv(records)
    records.map do |record|
      {
          'Name' => record.name,
          'Organization' => record.organization.name,
          'Devices' => record.devices.count,
          "Updated (#{user_timezone})" => record.updated_at,
          "Created (#{user_timezone})"=> record.created_at,
      }
    end
  end

  def export_options
    @options ||= {
        klass: 'Site'
    }
  end
end
