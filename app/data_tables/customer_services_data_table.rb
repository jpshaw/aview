class CustomerServicesDataTable < DataTables::Base
  delegate :h, :link_to, :admin_customer_service_path, :edit_admin_customer_service_path, to: :@view

  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data:            data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
      name: record.name,
      edit_button: edit_button(record),
      delete_button: delete_button(record),
    }
  end

  def get_raw_records
    @customer_services ||= CustomerService.order(:name)
  end

  def edit_button(record)
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
            edit_admin_customer_service_path(record),
            class: 'btn btn-white btn-xs table-button-remote',
            id: "edit_service_#{record.id}",
            remote: true,
            title: "Edit Service")
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"></i> Delete'.html_safe,
            admin_customer_service_path(record),
            class: 'btn btn-danger btn-xs table-button-remote',
            id: "delete_service_#{record.id}",
            remote: true,
            title: 'Delete Service',
            method: :delete,
            data: { confirm: 'Are you sure you want to delete this service?' })
  end
end