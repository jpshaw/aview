class IntelliflowClientUsageDataTable < DataTables::Base
  delegate :number_to_percentage, to: :view

  def initialize(view_context, attrs)
    @range = attrs[:range]
    @device_mac = attrs[:device_mac]
    @app = attrs[:app]
    super(view_context)
  end

  def as_json(options = {})
    { data: data }
  end

  private

  def data
    get_raw_records.each_with_index.map do |record, index|
      data_row(record, index)
    end
  end

  def data_row(record, index)
    row = []

    row << index + 1 #non zero based numbers
    row << client_identifier(record)
    row << Filesize.as_si(client_usage(record)).pretty
    row << percentage_of_total_usage(client_usage(record))

    row
  end

  def get_raw_records
    @app.present? ? total_usage_per_client_for_app : total_usage_per_client
  end

  def total_usage_per_client
    intelliflow_repository.data_usage_per_client({ device_mac: @device_mac,
                                               from_date_time: intelliflow_repository.from_date_time(@range),
                                               to_date_time: Time.now.utc,
                                               sample_size: 10 })
  end

  def total_usage_per_client_for_app
    intelliflow_repository.data_usage_per_client_for_app({ device_mac: @device_mac,
                                               app: @app,
                                               from_date_time: intelliflow_repository.from_date_time(@range),
                                               to_date_time: Time.now.utc,
                                               sample_size: 10 })
  end

  def client_identifier(record)
    record[0]
  end

  def client_usage(record)
    record[1]
  end

  def percentage_of_total_usage(usage)
    total_usage = @app.present? ? total_client_usage_for_app : total_client_usage
    number_to_percentage((usage / total_usage) * 100, precision: 1)
  end

  def total_client_usage
    @total_client_usage ||= intelliflow_repository.total_client_usage({ device_mac: @device_mac,
                                                                    from_date_time: intelliflow_repository.from_date_time(@range),
                                                                    to_date_time: Time.now.utc })
  end

  def total_client_usage_for_app
    @total_client_usage_for_app ||= intelliflow_repository.total_client_usage_for_app({ device_mac: @device_mac,
                                                                                        app: @app,
                                                                                        from_date_time: intelliflow_repository.from_date_time(@range),
                                                                                        to_date_time: Time.now.utc })
  end

  def intelliflow_repository
    @intelliflow_repository ||= Intelliflow.new
  end
end
