class DeviceConfigurationDataTable < DataTables::Base
  delegate :h, :link_to,
           :timestamp_in_users_timezone, :current_user, :publisher_link,
           :edit_device_configuration_path,
           :device_configuration_path,
           to: :@view

  def initialize(view, category)
    @view = view
    @category = category
    super(view)
  end

  def as_json(options = {})
    { data: data,
      draw: draw,
      recordsTotal: @total_records_count,
      recordsFiltered: @filtered_records_count,
      start: @start }
  end

  private

  def data
    get_records_vs.map do |record|
      {
          with_name: link_to(record.name, @view.device_configuration_path(record)),
          with_organization: link_to(record.organization.name, @view.organization_path(record.organization)),
          with_device_category: record.category.name,
          with_device_model: record.device_model.name,
          firmware_version: firmware_version(record),
          sort_by_updated_at: timestamp_in_users_timezone(record.updated_at),
          updated_by: updated_by(record),
          edit: edit_button(record),
          delete: delete_button(record)
       }
    end
  end

  def get_records_vs
    search_params = @columns.present? ? column_search_params_vs : {}
    search_params.merge!(table_search_params_vs).merge!(sort_params_vs)

    search_params.
        merge!(with_category: @category).
        delete_if{ |k,v| v.blank? }.
        symbolize_keys!

    @total_records_count = base_relation.size
    filtered_records = base_relation.merge(DeviceConfiguration.search!(search_params))
    @filtered_records_count = filtered_records.size
    @displayed_records = filtered_records.limit(@length).offset(@start)
  end

  def firmware_version(record)
    return record.device_firmware.version if record.device_firmware.production?
    non_production_firmware_version(record)
  end

  def non_production_firmware_version(record)
    "<div class='alert-info' data-toggle='tooltip' data-placement='right' "\
      "title='non-production firmware'>#{record.device_firmware.version}</div>".html_safe
  end

  def edit_button(record)
    link_to('<i class="fa fa-edit"> </i> Edit'.html_safe,
                 edit_device_configuration_path(record),
                 class: 'btn btn-white btn-xs',
                 title: 'Edit Device Configuration',
                 id: "configuration_#{record.id}")
  end

  def delete_button(record)
    link_to('<i class="fa fa-trash"> </i> Delete'.html_safe,
            device_configuration_path(record, format: :js),
            class: 'btn btn-danger btn-xs table-button-remote',
            id: "delete_configuration_#{record.id}",
            remote: true,
            method: :delete,
            data: {
                confirm: "Are you sure you want to delete this configuration?\n"\
                         "There are #{record.all_associated_devices.count} devices associated with this configuration."
            }
    )
  end

  def updated_by(object)
    object.paper_trail.originator.present? ? User.find_by_id(object.paper_trail.originator).try(:email) : ''
  end

  def base_relation
    @base_relation ||= current_user.device_configurations.preload(preloads).with_category(@category).collection
  end

  def preloads
    [:organization, :versions, device_firmware: { device_model: :category }]
  end
end
