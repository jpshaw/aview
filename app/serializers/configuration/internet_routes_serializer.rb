class Configuration::InternetRoutesSerializer

  attr_reader :config, :parent_config

  def initialize(config)
    @config        = config.data || {}
    @parent_config = config.parent_inherited_data || {}
  end

  def execute
    {
      inherited: routes.none?,
      internet_routes: routes,
      parent_internet_routes: parent_routes
    }
  end

  private

  def routes
    @routes ||= config.fetch(:routes, {}).map(&:to_h)
  end

  def parent_routes
    @parent_routes ||= parent_config.fetch(:routes, {}).map(&:to_h)
  end

end
