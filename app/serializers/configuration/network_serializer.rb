class Configuration::NetworkSerializer

  NETWORK_DATA = {
    intelliflow_enabled: {},
    ids_enabled: {},
    dhcp: {
      object: true,
      authoritative_server: {},
      # TODO: Enable DHCP Type once support for Address Translation is added.
      # type: {
      #   options: [
      #     { value: 'R', label: 'DHCP Relay' }
      #   ]
      # },
      start_address: {},
      end_address: {},
      default_route: {}
    },
    lan: {
      object: true,
      ethernet_speed: {
        options: [
          { value: 1, label: 'Automatic Negotiation' },
          { value: 2, label: '10 Megabit, Half Duplex' },
          { value: 3, label: '10 Megabit, Full Duplex' },
          { value: 4, label: '100 Megabit, Half Duplex' },
          { value: 5, label: '100 Megabit, Full Duplex' },
          { value: 6, label: '1000 Megabit, Half Duplex' },
          { value: 7, label: '1000 Megabit, Full Duplex' },
        ]
      },
      address: {},
      subnet: {}
    }
  }.freeze

  attr_reader :config, :parent_config

  def initialize(config)
    @config        = config.data || {}
    @parent_config = config.parent_inherited_data || {}

    @config.extend Hashie::Extensions::DeepFetch
    @parent_config.extend Hashie::Extensions::DeepFetch
  end

  def execute
    network_data
  end

  private

  def network_data
    {}.tap { |data| build_data(data) }
  end

  def build_data(data, properties = NETWORK_DATA, path = [])
    properties.each do |key, value|
      data[key] ||= {}

      if value[:object]
        build_data(data[key], value.except(:object), path + [key])
      else
        child  = config.deep_fetch(*path, key) { nil }
        parent = parent_config.deep_fetch(*path, key) { nil }

        data[key][:individual] = child
        data[key][:parent] = parent
        data[key][:inherited] = child.nil?
        data[key][:options] = value[:options] if value[:options]
      end
    end
  end

end
