class Configuration::CascadedNetworksSerializer
  attr_reader :config, :parent_config

  def initialize(config)
    @config        = config.data || {}
    @parent_config = config.parent_inherited_data || {}
  end

  def execute
    {
      inherited: cascaded_networks.none?,
      cascaded_networks: cascaded_networks,
      parent_cascaded_networks: parent_cascaded_networks
    }
  end

  private

  def cascaded_networks
    @cascaded_networks ||= config.fetch(:cascaded_networks, {}).map(&:to_h)
  end

  def parent_cascaded_networks
    @parent_cascaded_networks ||= parent_config.fetch(:cascaded_networks, {}).map(&:to_h)
  end
end
