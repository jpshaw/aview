class Configuration::NetworkDeserializer

  attr_reader :params

  def initialize(params)
    @params = params.dup
  end

  def execute
    network_data
  end

  private

  def network_data
    {}.tap { |data| build_data(data) }
  end

  def build_data(data, properties = params)
    properties.each do |key, value|
      if value.has_key?(:individual)
        data[key] = value[:inherited] ? nil : value[:individual]
      else
        data[key] ||= {}
        build_data(data[key], value)
      end
    end
    data.compact!
  end
end
