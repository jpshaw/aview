class DeviceStatisticsService
  include Exportable

  def initialize(type, format)
    @type = type
    @format = format
  end

  def execute
    formatted_report
  end

  private
  def device_counts_by_status_type_and_org
    data = []
    hw_versions = Device.select(:hw_version).map(&:hw_version).uniq
    active = Device.active.group(:hw_version)
    up = Device.up.group(:hw_version)
    undeployed = Device.undeployed.group(:hw_version)
    active_by_hw = active.count
    up_by_hw = up.count
    undeployed_by_hw = undeployed.count
    hw_versions.each do |hw_version|
      key = hw_version
      row = create_row("All", 
                       (active_by_hw.key?(key) && !active_by_hw[key].nil? ?  active_by_hw[key] : 0), 
                       (up_by_hw.key?(key) && !up_by_hw[key].nil? ?  up_by_hw[key] : 0),
                       (undeployed_by_hw.key?(key) && !undeployed_by_hw[key].nil? ?  undeployed_by_hw[key] : 0),
                       hw_version)
      data << row
    end
    active_by_account = active.group(:organization_id).count
    up_by_account = up.group(:organization_id).count
    undeployed_by_account = undeployed.group(:organization_id).count
    Organization.find_each do |org|
      hw_versions.each do |hw_version|
        key = [hw_version, org.id]
        row = create_row(org.name, 
                         (active_by_account.key?(key) && !active_by_account[key].nil? ?  active_by_account[key] : 0), 
                         (up_by_account.key?(key) && !up_by_account[key].nil? ?  up_by_account[key] : 0),
                         (undeployed_by_account.key?(key) && !undeployed_by_account[key].nil? ?  undeployed_by_account[key] : 0),
                         hw_version)
        data << row
      end
    end
    data
  end

  def create_row(org_name, active_count, up_count, undeployed_count, device_model)
    {
      "Organization" => org_name,
      "Active" => active_count,
      "Up" => up_count,
      "Undeployed" => undeployed_count,
      "Device Model" => device_model
    }
  end

  def as_csv
    case @type.to_sym
    when :device_model_counts_by_org
      device_counts_by_status_type_and_org
    end
  end
  alias_method :as_xlsx, :as_csv

  def formatted_report
    case @format.to_sym
    when :csv
      to_csv
    when :xlsx
      to_xlsx
    end
  end
end