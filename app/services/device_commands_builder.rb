class DeviceCommandsBuilder

  COMMAND_CONFIG   = YAML.load_file("#{Rails.root}/config/command_menus.yml")
  TUNNEL_COMMANDS  = COMMAND_CONFIG['tunnel']
  DEFAULT_COMMANDS = COMMAND_CONFIG['default']

  attr_reader :device, :tunnel_commands

  def initialize(device, user)
    @device = device
    @user = user

    @tunnel_commands = []
  end

  def execute
    commands = extract_user_commands((COMMAND_CONFIG[device_key] || DEFAULT_COMMANDS).deep_dup)
    build_tunnel_commands if has_tunnels?
    set_command_to_send_json(commands) if remote_disabled?
    commands += tunnel_commands if tunnel_commands.any?
    commands
  end

  private

  def build_tunnel_commands
    device.tunnels.outbound.by_interface.each do |tunnel|
      commands = TUNNEL_COMMANDS.deep_dup

      if tunnel.management?
        commands['commands'].pop
      end

      commands['commands'].each do |command|
        command['send_json'] = true if remote_disabled?
        command['command']['<index>'] = "#{tunnel.ipsec_index}"
      end

      tunnel_commands << { label: tunnel.ipsec_iface, commands: commands['commands'] }
    end
  end

  def device_key
    @device_key ||= device.class.name.downcase
  end

  def remote_disabled?
    device.is_platform?(:uc_linux) && device.remote_control_disabled?
  end

  def has_tunnels?
    device.respond_to?(:tunnels) && device.tunnels.outbound.count > 0
  end

  def set_command_to_send_json(array)
    array.each do |item|
      if item.key?('command')
        item['send_json'] = true
      elsif item.key?('commands')
        set_command_to_send_json(item['commands'])
      end
    end
  end

  def extract_user_commands(commands)
      commands.select { |item| user_has_access?(item) }
  end

  def user_has_access?(cmd)
    case cmd['min_perms']
    when "admin"
      admin_user?
    else
      true
    end
  end

  def admin_user?
     @is_admin_user ||= AdminControllerPolicy.new(@user, nil).view?
  end
end
