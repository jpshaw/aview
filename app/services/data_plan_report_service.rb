require 'csv'

class DataPlanReportService

  CSV_HEADERS = ["MAC Adress", "Data Limit", "Individual Data Limit", "Data Used", "Start Day"]

  attr_reader :data_plan, :devices

  def initialize(data_plan)
    @data_plan = data_plan
  end

  def execute
    create_device_csv()
  end

  private

  def create_device_csv
    csv_report = CSV.generate do |csv|
      csv << CSV_HEADERS
      @data_plan.devices.find_each do |dev|
        csv << build_device_row(dev)
      end
    end
  end

  def build_device_row(device)
    row = []
    row << device.mac
    row << Filesize.as_si(pooled_limit).pretty
    row << Filesize.as_si(individual_limit).pretty
    row << Filesize.as_si(device.data_used).pretty
    row << start_day
  end

  def start_day
    @start_day ||= @data_plan.current_period_start_time.strftime( "%Y-%m-%d" )
  end

  def pooled_limit
    @pooled_limit ||= @data_plan.data_limit
  end

  def individual_limit
    @individual_limit ||= @data_plan.individual_data_limit
  end
end