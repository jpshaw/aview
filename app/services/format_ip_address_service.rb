class FormatIpAddressService
  def initialize(address)
    @address = address
  end

  def execute
    ip_address = IPAddress(@address)

    if ip_address.ipv4?
      ip_address.octets.join('.')
    else
      ip_address.compressed.upcase
    end
  end
end
