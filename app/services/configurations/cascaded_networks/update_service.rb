module Configurations
  module CascadedNetworks
    class UpdateService < ::Configurations::CascadedNetworks::BaseService

      def execute
        smx_transaction do
          client.update_cascaded_network(
            account_id,
            resource_id,
            network_id,
            network_data.merge(principal_id: principal_id)
          )

          upsert_network
        end

        configuration
      end

    end
  end
end
