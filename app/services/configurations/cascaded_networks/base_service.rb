module Configurations
  module CascadedNetworks
    class BaseService < ::Configurations::BaseService

      private

      def client
        @client ||= ::ServiceManagerRest::CascadedNetworksApi.new
      end

      def network_id
        @network_id ||= format_ip_address(params[:ip_address])
      end

      def network_data
        @network_data ||= begin
          ip_address.ipv4? ? ipv4_network_data : ipv6_network_data
        end
      end

      def ip_address
        @ip_address ||= IPAddress(params[:ip_address])
      end

      def subnet_mask
        @subnet_mask ||= begin
          if ip_address.ipv4?
            format_subnet(params[:subnet_mask])
          else
            format_prefix(params[:subnet_mask])
          end
        end
      end

      def router_address
        @router_address ||= begin
          if local_lan_alias == 'N' # router ip address is required
            format_ip_address(params[:router_ip_address].to_s)
          else
            params[:router_ip_address].to_s
          end
        end
      end

      def description
        @description ||= params[:description].to_s.upcase.truncate(30, omission: '')
      end

      def local_lan_alias
        @local_lan_alias ||= booleanize(params[:local_lan_alias])
      end

      def internet_only_route
        @internet_only_route ||= booleanize(params[:internet_only_route])
      end

      def restrict_vpn_advertisement
        @restrict_vpn_advertisement ||= booleanize(params[:restrict_vpn_advertisement])
      end

      def routing_metric
        @routing_metric ||= format_metric(params[:routing_metric])
      end

      def ipv4_network_data
        {
          data: {
            ipAddress: network_id,
            subnetMask: subnet_mask,
            routerIPAddress: router_address,
            description: description,
            lanAliasInd: local_lan_alias,
            internetOnlyInd: internet_only_route,
            routingMetric: routing_metric,
            restrictVPNAdv: restrict_vpn_advertisement
          }
        }
      end

      def ipv6_network_data
        {
          data: {
            ipv6Address: network_id,
            ipAddressPrefix: subnet_mask,
            ipv6RouterAddr: router_address,
            description: description,
            lanAliasInd: local_lan_alias,
            internetOnlyInd: internet_only_route,
            routingMetric: routing_metric,
            restrictVPNAdv: restrict_vpn_advertisement
          }
        }
      end

      def network_params
        params.merge(
          ip_address: network_id,
          subnet_mask: subnet_mask,
          description: description,
          router_ip_address: router_address,
          routing_metric: routing_metric
        )
      end


      def upsert_network
        network_exists? ? update_network : create_network
      end

      def create_network
        configuration.update(cascaded_networks: networks_with_create)
      end

      def update_network
        configuration.update(cascaded_networks: networks_with_update)
      end

      def networks_with_create
        configuration.cascaded_networks.push(network_params)
      end

      def networks_with_update
        configuration.cascaded_networks.map do |network|
          if is_network?(network)
            updated_network = network_params.dup

            # Copy history fields if present
            [:created_on, :created_by, :updated_by, :updated_on].each do |key|
              updated_network[key] = network[key] if network[key].present?
            end

            updated_network
          else
            network
          end
        end
      end

      def network_exists?
        configuration.cascaded_networks.find { |network| is_network?(network) }.present?
      end

      def is_network?(network)
        network[:ip_address] == network_id
      end
    end
  end
end
