module Configurations
  module CascadedNetworks
    class FindOrCreateService

      def initialize(resource)
        @resource = resource
      end

      def execute
        ::CascadedNetworksConfiguration.where(resource: @resource).first_or_create
      end

    end
  end
end
