module Configurations
  module CascadedNetworks
    class DestroyService < ::Configurations::CascadedNetworks::BaseService

      def execute
        smx_transaction do
          begin
            client.delete_cascaded_network(
              account_id,
              resource_id,
              network_id
            )
          rescue ServiceManagerRest::ApiError => error
            raise unless error.code == 404
          end

          configuration.cascaded_networks.delete_if do |network|
            network[:ip_address] == params[:ip_address]
          end
          configuration.save
        end

        configuration
      end

    end
  end
end
