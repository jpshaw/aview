module Configurations
  module CascadedNetworks
    class DestroyAllService < ::Configurations::CascadedNetworks::BaseService

      def execute
        smx_transaction do
          configuration.cascaded_networks.each do |network|
            begin
              network_id = network[:ip_address]

              client.delete_cascaded_network(
                account_id,
                resource_id,
                network_id
              )
            rescue ServiceManagerRest::ApiError => error
              error.code == 404 ? next : raise
            end
          end

          configuration.update(cascaded_networks: [])
        end

        configuration
      end

    end
  end
end
