module Configurations
  class BaseService
    include ::Kraken::Logger.file

    def initialize(configuration, params = {}, principal_id = nil)
      @configuration = configuration
      @params = params.dup
      @principal_id = principal_id
    end

    private

    attr_reader :configuration, :params, :principal_id

    def account_id
      @account_id ||= configuration.resource.organization_name
    end

    def resource_id
      @resource_id ||= configuration.resource.name
    end

    def smx_transaction

      yield if block_given?

    rescue ServiceManagerRest::ApiError => error
      case error.code
      when 400
        handle_error_response_body(error.response_body)
      when 404
        handle_not_found
      else
        handle_error(error)
      end

    rescue ArgumentError => error
      handle_argument_error(error)

    rescue => error
      handle_error(error)
    end

    def handle_error_response_body(response_body)
      response = JSON.parse(response_body) || {}
      message  = response['message']

      configuration.errors.add(:base, message) if message
    end

    def handle_not_found
      configuration.errors.add(:base, 'Resource not found in Service Manager')
    end

    def handle_argument_error(error)
      if error.message.start_with?('Invalid IP')
        if error.message.end_with?('nil')
          message = ' - Blanks are not permitted'
          configuration.errors.add(:base, error.message.gsub('nil', message))
        else
          configuration.errors.add(:base, error.message)
        end
      else
        handle_error(error)
      end
    end

    def handle_error(error)
      logger.error(error)
      configuration.errors.add(:base, 'Unexpected Error')
    end

    def booleanize(field)
      field == 'true' ? 'Y' : 'N'
    end

    def format_ip_address(address)
      ::FormatIpAddressService.new(address).execute
    end

    def format_subnet(subnet)
      ::FormatIpAddressService.new(subnet).execute
    end

    def format_prefix(prefix)
      prefix.to_i.to_s
    end

    def format_metric(metric)
      if metric.present?
        metric.to_i.to_s
      else
        metric.to_s
      end
    end
  end
end
