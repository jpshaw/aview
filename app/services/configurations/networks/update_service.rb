module Configurations
  module Networks
    class UpdateService < ::Configurations::BaseService

      def execute
        smx_transaction do
          update_service_manager if ServiceManager.enabled?
          configuration.update_attributes(params)
        end

        configuration
      end

      private

      def update_service_manager
        client      = ::ServiceManagerRest::GatewaysApi.new
        data        = ::DeviceDataMapper.gateway_config_ui_to_smx(smx_params)
        account_id  = configuration.resource.organization_name
        resource_id = configuration.resource.name

        client.update_gateway_device(account_id, resource_id, { data: data, principal_id: principal_id } )
      end

      def smx_params
        {
          network: params,
          model_id: configuration.parent_name,
          model_ind: false
        }
      end

    end
  end
end
