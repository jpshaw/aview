module Configurations
  module InternetRoutes
    class FindOrCreateService

      def initialize(resource)
        @resource = resource
      end

      def execute
        ::InternetRoutesConfiguration.where(resource: @resource).first_or_create
      end

    end
  end
end
