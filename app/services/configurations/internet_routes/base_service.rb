module Configurations
  module InternetRoutes
    class BaseService < ::Configurations::BaseService

      private

      def client
        @client ||= ::ServiceManagerRest::InternetRoutesApi.new
      end

      def route_id
        @route_id ||= ::FormatIpAddressService.new(params[:ip_address]).execute
      end

      def route_data
        @route_data ||= begin
          ip_address.ipv4? ? ipv4_route_data : ipv6_route_data
        end
      end

      def ip_address
        @ip_address ||= IPAddress(params[:ip_address])
      end

      def subnet_mask
        @subnet_mask ||= begin
          if ip_address.ipv4?
            format_subnet(params[:subnet_mask])
          else
            format_prefix(params[:subnet_mask])
          end
        end
      end

      def description
        @description ||= params[:description].to_s.upcase.truncate(30, omission: '')
      end

      def ipv4_route_data
        {
          data: {
            ipAddress: route_id,
            subnetMask: subnet_mask,
            description: description
          }
        }
      end

      def ipv6_route_data
        {
          data: {
            ipv6Address: route_id,
            ipAddressPrefix: subnet_mask,
            description: description
          }
        }
      end

      def route_params
        params.merge(
          ip_address: route_id,
          subnet_mask: subnet_mask,
          description: description
        )
      end

      def upsert_route
        route_exists? ? update_route : create_route
      end

      def create_route
        configuration.update(routes: routes_with_create)
      end

      def update_route
        configuration.update(routes: routes_with_update)
      end

      def routes_with_create
        configuration.routes.push(route_params)
      end

      def routes_with_update
        configuration.routes.map do |route|
          if is_route?(route)
            updated_route = route_params.dup

            # Copy history fields if present
            [:created_on, :created_by, :updated_by, :updated_on].each do |key|
              updated_route[key] = route[key] if route[key].present?
            end

            updated_route
          else
            route
          end
        end
      end

      def route_exists?
        configuration.routes.find { |route| is_route?(route) }.present?
      end

      def is_route?(route)
        route[:ip_address] == route_id
      end
    end
  end
end
