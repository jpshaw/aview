module Configurations
  module InternetRoutes
    class UpdateService < ::Configurations::InternetRoutes::BaseService

      def execute
        smx_transaction do
          client.update_internet_route(
            account_id,
            resource_id,
            route_id,
            route_data.merge(principal_id: principal_id)
          )

          upsert_route
        end

        configuration
      end

    end
  end
end
