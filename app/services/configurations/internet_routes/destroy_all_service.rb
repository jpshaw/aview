module Configurations
  module InternetRoutes
    class DestroyAllService < ::Configurations::InternetRoutes::BaseService

      def execute
        smx_transaction do
          configuration.routes.each do |route|
            begin
              route_id = route[:ip_address]

              client.delete_internet_route(
                account_id,
                resource_id,
                route_id
              )
            rescue ServiceManagerRest::ApiError => error
              error.code == 404 ? next : raise
            end
          end

          configuration.update(routes: [])
        end

        configuration
      end

    end
  end
end
