module Configurations
  module InternetRoutes
    class DestroyService < ::Configurations::InternetRoutes::BaseService

      def execute
        smx_transaction do
          begin
            client.delete_internet_route(
              account_id,
              resource_id,
              route_id
            )
          rescue ServiceManagerRest::ApiError => error
            raise unless error.code == 404
          end

          configuration.routes.delete_if do |route|
            route[:ip_address] == params[:ip_address]
          end
          configuration.save
        end

        configuration
      end

    end
  end
end
