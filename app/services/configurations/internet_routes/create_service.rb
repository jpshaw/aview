module Configurations
  module InternetRoutes
    class CreateService < ::Configurations::InternetRoutes::BaseService

      def execute
        smx_transaction do
          client.create_internet_route(
            account_id,
            resource_id,
            route_data.merge(principal_id: principal_id)
          )

          upsert_route
        end

        configuration
      end

    end
  end
end
