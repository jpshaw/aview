if RUBY_PLATFORM == 'java' and not Rails.env.test?
  require 'snmp4jruby'
end

require 'snmp_response'

class DeviceProbeService
  include ::Kraken::Logger.file

  attr_reader :name, :threads, :workers, :timeout, :batch, :done, :ttl,
    :requests, :responses, :pool

  def initialize(options = {})
    @name      = 'probe-service'
    @threads   = 25 # How many snmp threads to spawn
    @workers   = 25 # How many parallel workers to spawn
    @batch     = 25 # SNMP OID batch size
    @timeout   = 500 # Milliseconds to wait for requests from HornetQ
    @ttl       = 900_000 # Milliseconds before response times out in HornetQ
    @requests  = TorqueBox::Messaging::Queue.new('/queues/scheduled_probes')
    @responses = TorqueBox.fetch('/queues/scheduled_probe_results')
    @pool      = Concurrent::ThreadPoolExecutor.new(
      min_threads: @workers,
      max_threads: @workers,
      max_queue: 100,
      fallback_policy: :caller_runs
    )
    @done      = false
  end

  # Starts the snmp listener and kicks off a thread that sends snmp requests
  # popped from the requests queue.
  def start
    logger.info 'Starting snmp probe service ...'
    start_snmp
    @done = false
    @requests_thread = Thread.new { requests_loop }
    logger.info 'Started snmp probe service.'
  end

  # Stops the snmp listener and closes the requests thread.
  def stop
    logger.info 'Stopping snmp probe service ...'
    stop_snmp
    @done = true
    @requests_thread.join
    logger.info 'Stopped snmp probe service.'
  end

  private

  def start_snmp
    logger.info "Starting snmp listener with #{threads} threads ..."

    # Adds a message processing model to this message dispatcher
    snmpv2c_model = SNMP4JR::MP::MPv2c.new
    snmp.message_dispatcher.add_message_processing_model(snmpv2c_model)

    # Puts all associated transport mappings into listen mode.
    snmp.listen

    logger.info 'Started snmp listener.'
  end

  def stop_snmp
    logger.info 'Stopping snmp listener ...'

    snmp.try(:close)
    thread_pool.try(:stop)

    logger.info 'Stopped snmp listener.'
  end

  def requests_loop
    logger.info 'Started requests loop thread.'

    begin
      until done
        if request = requests.receive(timeout: timeout)
          pool.post do
            process_request(request)
          end
        end
      end
    rescue => e
      logger.error "Exception thrown in requests loop thread: #{e.message}"

      unless done
        sleep 5 # Wait for HornetQ to restart
        logger.info 'Restarting requests loop thread'
        retry
      end
    end

    logger.info 'Stopped requests loop thread.'
  end

  def process_request(request)
    results = {}

    # Split oid list into chunks and send a request for each chunk
    snmp_oid_chunks = request.objects.each_slice(batch).to_a
    snmp_oid_chunks.each do |oid_chunk|
      chunked_request = ::ProbeRequest.new(
        device_id: request.device_id,
           config: request.config,
          objects: oid_chunk
      )

      target = ::SNMPTarget.new(chunked_request.target_config)
      response_event = snmp.send(target.pdu, target.snmp_target)
      response = response_event.response

      if response.present?
        response_results = ::SnmpResponse.new(response).results
        results.merge!(response_results)
      end

      target.close
    end

    if results.present?
      request.results = results
      request.success!
    else
      request.fail!
    end

    responses.publish(request, encoding: :marshal, ttl: ttl)
  rescue => e
    logger.error "process_request failed with error: #{e.message}."
  end

  # The ThreadPool provides a pool of a fixed number of threads that are capable
  # to execute tasks that implement the Runnable interface concurrently. The
  # ThreadPool blocks when all threads are busy with tasks and an additional
  # task is added.
  #
  # http://www.snmp4j.org/doc/org/snmp4j/util/ThreadPool.html
  def thread_pool
    @thread_pool ||= SNMP4JR::Util::ThreadPool.create(name, threads)
  end

  # The MultiThreadedMessageDispatcher class is a decorator for any
  # MessageDispatcher instances that processes incoming message with a supplied
  # ThreadPool. The processing is thus parallelized on up to the size of the
  # supplied thread pool threads.
  #
  # In contrast to a MessageDispatcherImpl a MultiThreadedMessageDispatcher
  # copies the incoming ByteBuffer for processMessage(TransportMapping
  # sourceTransport, Address incomingAddress, ByteBuffer wholeMessage,
  # TransportStateReference tmStateReference) to allow concurrent processing
  # of the buffer.
  #
  # http://www.snmp4j.org/doc/org/snmp4j/util/MultiThreadedMessageDispatcher.html
  def dispatcher
    @dispatcher ||= SNMP4JR::Util::MultiThreadedMessageDispatcher.new(thread_pool, SNMP4JR::MessageDispatcherImpl.new)
  end

  # The DefaultUdpTransportMapping implements a UDP transport mapping based on
  # Java standard IO and using an internal thread for listening on the inbound
  # socket.
  #
  # http://www.snmp4j.org/doc/org/snmp4j/transport/DefaultUdpTransportMapping.html
  def transport
    @transport ||= SNMP4JR::Transport::DefaultUdpTransportMapping.new
  end

  # The Snmp class is the core of SNMP4J. It provides functions to send and
  # receive SNMP PDUs. All SNMP PDU types can be send. Confirmed PDUs can be
  # sent synchronously and asynchronously.
  #
  # The Snmp class is transport protocol independent. Support for a specific
  # TransportMapping instance is added by calling the
  # addTransportMapping(TransportMapping transportMapping) method or creating a
  # Snmp instance by using the non-default constructor with the corresponding
  # transport mapping. Transport mappings are used for incoming and outgoing
  # messages.
  #
  # http://www.snmp4j.org/doc/org/snmp4j/Snmp.html
  def snmp
    @snmp ||= SNMP4JR::Snmp.new(dispatcher, transport)
  end
end
