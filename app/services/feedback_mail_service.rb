class FeedbackMailService

  attr_reader :user, :last_url, :category, :domain, :message, :recipients

  def initialize(user, last_url, category, domain, message)
    @user = user
    @last_url = last_url
    @category = category
    @domain = domain
    @message = message
    @recipients = "#{AdminSettings.support_contact_list}".split(",")
  end

  def execute
    @recipients.each do |recipient|
      FeedbackMailer.new_feedback(@user, @last_url, @category, @domain, @message, recipient).deliver_now
    end
  end
end
