module Locations
  class Upsert
    include Locations::Upsertable

    def self.execute(params)
      new(params).execute
    end

    def initialize(params)
      @params = params.to_h.with_indifferent_access
    end

    def execute
      normalize_params
      ::Kraken::Location.upsert(params)
    end

    private

    attr_reader :params

  end
end
