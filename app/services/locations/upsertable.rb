module Locations
  module Upsertable

    private

    def normalize_params
      normalize_country
      normalize_region
    end

    def normalize_country
      if country_id = params.delete(:country_id)
        params[:country_code] = ::Country.code_for_id(country_id)
      elsif country_name = params.delete(:country)
        params[:country_code] = ::Country.code_for_name(country_name)
      end
    end

    def normalize_region
      if region = params.delete(:region)
        if country_is_usa?
          params[:state_code] = region
        else
          params[:province] = region
        end
      end
    end

    US_CODE = 'US'.freeze

    def country_is_usa?
      params[:country_code] == US_CODE
    end
  end
end
