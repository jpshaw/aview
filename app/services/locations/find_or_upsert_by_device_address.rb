module Locations
  # Process address form input by a user on the device location page
  class FindOrUpsertByDeviceAddress
    include Locations::Upsertable

    def self.execute(device, params)
      new(device, params).execute
    end

    def initialize(device, params)
      @device = device
      @params = params.to_h.with_indifferent_access
    end

    def execute
      normalize_params

      if address_changed?
        ::Kraken::Location.upsert(params.except(:latitude, :longitude))
      else
        location = device.location
        location.update(address_2: params[:address_2]) if location
        location
      end
    end

    private

    attr_reader :device, :params

    def current_address
      @current_address ||= device.location.try(:address)
    end

    def params_address
      @params_address ||= Kraken::Location::Address.new(params)
    end

    def address_changed?
      if both_addresses_are_present?
        current_address.checksum != params_address.checksum
      elsif both_addresses_are_blank?
        false # If both are blank then there's no change
      else
        true  # One is present while the other is blank
      end
    end

    def both_addresses_are_present?
      current_address.present? && params_address.present?
    end

    def both_addresses_are_blank?
      current_address.blank? && params_address.blank?
    end
  end
end
