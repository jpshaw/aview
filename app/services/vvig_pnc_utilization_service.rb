require 'googleauth'

class VvigPncUtilizationService
	include HTTParty

  base_uri Settings.vvig.service_url

  def initialize(org_ids, vlans, vigs, start_ts, end_ts, format)
    @options = { 
      query: {
        accountIds: org_ids.split(","),
        vlans: vlans,
        vigs: vigs,
        startTs: start_ts.to_i / 1000,
        endTs: end_ts.to_i / 1000
      }
    }
  end

  def execute
    {
      data: get_data
    }
  end

  private
  def get_data
    begin
      resp_data = make_request
      if resp_data.code == 401
        raise 'Token invalid.' 
      end
    rescue => error
      #retry once more with a new token
      resp_data = make_request(force_new_token=true)
    end

    data = JSON.parse(resp_data.body)
    if data.key? "accounts"
      process_response_data(data)
    else
      []
    end
  end

  def make_request(force_new_token=false)
    self.class.get("/v1/getdata", :headers => get_headers(force_new_token), :body => get_body, :verify => false)
  end

  def get_body
    params = {}
    @options[:query].each do |k, v| 
      unless v.nil? 
        params[k] = v 
      end
    end
    params.to_json
  end

  def get_headers(force_new_token)
    if force_new_token
      generate_new_token
    end
    headers = {
      "Authorization" => "Bearer #{oauth_token}"
    }
  end

  def generate_new_token
    @oauth_token = generate_token
  end

  def oauth_token
    @oauth_token ||= generate_token
  end

  def generate_token
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: key_file,
      scope: token_scopes)
    (authorizer.fetch_access_token!)["id_token"]
  end

  def key_file
    File.open(key_file_loc)
  end

  def token_scopes
    @token_scopes ||= Settings.vvig.service_url
  end

  def key_file_loc
    @key_file ||= Settings.vvig.service_account_key_file
  end

  def process_response_data(data)
    data["accounts"].map { |org| create_org_data(org) }
  end

  def create_org_data(org_data)
    {org_id: org_data["accountId"], vlans: org_data["vlans"].map { |vlan| create_vlan_data(vlan) }}
  end

  def create_vlan_data(vlan_data)
    {vlan_id: vlan_data["vlanId"], vigs: vlan_data["vigs"].map { |vig| create_vig_data(vig) }}
  end

  def create_vig_data(vig_data)
    {vig_id: vig_data["vigId"], vig_location: get_vig_location(vig_data["vigId"]), data: create_vig_data_response_data_array(vig_data["data"])}
  end

  def get_vig_location(vig_id)
    tunnel = TunnelServer.find_by(name: vig_id)
    unless tunnel.nil?
      tunnel.location
    else
      "location unavailable"
    end
  end

  def create_vig_data_response_data_array(data_points)
    chart_formatted_data = {}
    data_points.each do |response_data|
      create_reponse_data_map(response_data, chart_formatted_data)
    end
    chart_formatted_data
  end

  def create_reponse_data_map(response_data, chart_formatted_data)
    ts = response_data["ts"].to_i * 1000
    response_data.keys.each do |key|
      unless key == "ts"
        unless chart_formatted_data.key? key
          chart_formatted_data[key] = []
        end
        chart_formatted_data[key] << [ts, response_data[key].to_i]
      end
    end
  end
end