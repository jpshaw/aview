# Services

There are two types of services in the application:

* [TorqueBox Services](http://torquebox.org/documentation/3.2.0/services.html)
* [Service Objects](https://hackernoon.com/service-objects-in-ruby-on-rails-and-you-79ca8a1c946e)

## TorqueBox Services

TorqueBox Services are persistent background PORO daemons that run within the context of the web application. They are prime candidates to move to "microservices" in the future. The current services managed by TorqueBox are:

* [DeviceProbeService](device_probe_service.rb)
* [DeviceHeartbeatMonitorService](device_heartbeat_monitor_service.rb)
* [CrossAccountService](/engines/armt/app/services/cross_account_service.rb)

## Service Objects

All other classes in `app/services` are considered "Service Objects". There isn't a defined pattern for existing services, but all new services should follow the guidelines listed here.

A Service Object should use `initialize` to pass any context variables, such as `user` or `params`, and should provide a public instance method to invoke it, preferable `execute`. For example:

```ruby
class DoSomethingService

  def initialize(user, params = {})
    @user, @params = user, params.dup
  end

  def execute
    # do something
  end

end
```

Now invoke the service:

```ruby
  result = DoSomethingService.new(user, { hello: 'world' }).execute
```

The `result` can be anything that is expected from the service, such as a `boolean`, `hash`, `object`, etc. Also, the `execute` method can take any arguments relevant to the context of the service invocation.

See [GitLab's service objects](https://github.com/gitlabhq/gitlabhq/tree/master/app/services) for good examples of this pattern.
