module Contacts
  class DestroyService
    def initialize(device, contact)
      @device = device
      @contact = contact
    end

    def execute
      association = ::DevicesContacts.find_by(device_id: device.id,
                                             contact_id: contact.id)
      association.destroy if association
    end

    private

    attr_reader :contact, :device

  end
end
