module Contacts
  class CreateService
    def initialize(device, params = {})
      @device = device
      @params = params.dup
    end

    def execute
      create_contact

      if contact.valid? && contact.persisted?
        assign_to_device
        assign_to_organization
        clear_extra_contacts
      end

      contact
    end

    private

    attr_reader :device, :params

    def contact
      @contact ||= Contact.find_by_token(params) || Contact.new
    end

    def create_contact
      if device.is_smx_device?
        # SMX capitalizes contact name, email and phone
        params.each { |k, v| v.upcase! if v.is_a? String }

        # Truncate fields to SMX limits
        params[:name]  = params[:name].truncate(30, omission: '')  if params[:name]
        params[:phone] = params[:phone].truncate(30, omission: '') if params[:phone]
        params[:email] = params[:email].truncate(50, omission: '') if params[:email]
      end

      contact.assign_attributes(params)
      contact.save
    end

    def assign_to_device
      unless device.contacts.exists?(contact.id)
        device.contacts << contact
      end
    end

    def assign_to_organization
      unless device.organization.contacts.exists?(contact.id)
        device.organization.contacts << contact
      end
    end

    def clear_extra_contacts
      return unless device.is_smx_device?

      device.contacts.where.not(id: contact.id).each do |extra_contact|
        device.contacts.delete(extra_contact)
      end
    end
  end
end
