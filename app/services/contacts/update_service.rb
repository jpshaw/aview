module Contacts
  class UpdateService
    def initialize(device, contact, params = {})
      @device = device
      @params = params.dup
      @contact = contact
    end

    def execute
      @contact.update(@params)
    end

    private

    attr_reader :device, :params, :contact
  end
end
