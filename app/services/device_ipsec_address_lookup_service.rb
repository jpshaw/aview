class DeviceIpsecAddressLookupService
  TIMEOUT = 5 # seconds

  class << self
    extend Memoist

    def server
      Settings.remote_control.try(:server).try(:name)
    end
    memoize :server

    def enabled?
      server.present?
    end
    memoize :enabled?

    def dns_resolver
      return unless enabled?

      resolver = ::Resolv::DNS.new(nameserver: server, search: '')
      resolver.timeouts = TIMEOUT
      resolver
    end
    memoize :dns_resolver
  end

  attr_reader :device_id

  def initialize(device_id)
    @device_id = device_id
  end

  def execute
    return unless self.class.enabled? && device_id.present?

    address = self.class.dns_resolver.getaddress(device_id).to_s

    if address.present? && IPAddress.valid?(address)
      address
    else
      nil
    end
  rescue Resolv::ResolvError
    nil # DNS has no address for this device
  end
end
