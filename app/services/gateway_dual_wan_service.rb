class GatewayDualWANService

  ## hwDualWan
  #   notEnabled(0),
  #   wan1AndWan2BackupEachOther(1),
  #   noInetIfEitherWanIsDown(2)
  DISABLED            = 0
  ENABLED             = 1
  NO_INET_IF_ANY_DOWN = 2

  ## ifWanStateIPv4 / ifWanStateIPv6
  #   noneActive(0),
  #   primaryActive(1),
  #   secondaryActive(2),
  #   primaryPendingSecondaryActive(3),
  #   secondaryPending(4),
  #   primaryPendingSecondaryComingDown(5),
  #   primaryActiveSecondaryPending(6),
  #   primaryInactiveSecondaryActive(7),
  #   primaryActiveSecondaryInactive(8),
  #   primaryActiveSecondaryActive(9)
  NONE_ACTIVE                           = 0
  PRIMARY_ACTIVE                        = 1
  SECONDARY_ACTIVE                      = 2
  PRIMARY_PENDING_SECONDARY_ACTIVE      = 3
  SECONDARY_PENDING                     = 4
  PRIMARY_PENDING_SECONDARY_COMING_DOWN = 5
  PRIMARY_ACTIVE_SECONDARY_PENDING      = 6
  PRIMARY_INACTIVE_SECONDARY_ACTIVE     = 7
  PRIMARY_ACTIVE_SECONDARY_INACTIVE     = 8
  PRIMARY_ACTIVE_SECONDARY_ACTIVE       = 9

  # Dual WAN Status
  #   Both up           = [9]
  #   Both down         = [0,4,5]
  #   WAN1 up WAN2 down = [1,6,8]
  #   WAN1 down WAN2 up = [2,3,7]
  BOTH_UP           = [PRIMARY_ACTIVE_SECONDARY_ACTIVE].freeze
  BOTH_DOWN         = [NONE_ACTIVE, SECONDARY_PENDING,
                       PRIMARY_PENDING_SECONDARY_COMING_DOWN].freeze
  WAN1_UP_WAN2_DOWN = [PRIMARY_ACTIVE, PRIMARY_ACTIVE_SECONDARY_PENDING,
                       PRIMARY_ACTIVE_SECONDARY_INACTIVE].freeze
  WAN1_DOWN_WAN2_UP = [SECONDARY_ACTIVE, PRIMARY_PENDING_SECONDARY_ACTIVE,
                       PRIMARY_INACTIVE_SECONDARY_ACTIVE].freeze

  attr_reader :wan_states

  delegate :blank?, :present?, to: :wan_states

  def initialize(wan_states)
    @wan_states = (wan_states || []).compact.map(&:to_i)
  end

  def both_up?
    any_wan_states_in? BOTH_UP
  end

  def both_down?
    any_wan_states_in? BOTH_DOWN
  end

  def wan1_up_and_wan2_down?
    any_wan_states_in? WAN1_UP_WAN2_DOWN
  end

  def wan1_down_and_wan2_up?
    any_wan_states_in? WAN1_DOWN_WAN2_UP
  end

  private

  # Intersect the array with the wan states.
  #
  # Returns true if any wan states are in the array; otherwise false.
  def any_wan_states_in?(array)
    (array & wan_states).any?
  end

end
