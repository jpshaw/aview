module SMS
  class CommandService
    def initialize(number, message)
      @number  = number
      @message = message
      @client  = ::Twilio::Client.new
    end

    def execute
      @client.messages.create(
          to: @client.phone_numbers.get(@number).fetch('phone_number'),
        body: @message
      )
    end
  end
end
