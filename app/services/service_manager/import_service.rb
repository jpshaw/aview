module ServiceManager
  class ImportService
    include ::Kraken::Logger.file

    def initialize(source_path = default_source_path)
      @source = ::ServiceManager::GatewayTabfile.new(source_path)
      @cache  = ActiveSupport::Cache::MemoryStore.new(size: 256.megabytes)
    end

    def results
      @results ||= {
               devices: { added: [], scanned: [], removed: [], skipped: [] },
             customers: { added: [] },
                 sites: { added: [] },
             locations: { added: [] },
        configurations: { added: [] }
      }
    end

    def execute
      source.each_row { |row| process_row(row) }
      device_sync_producer.shutdown
      self
    end

    # Legacy import interface
    def self.import_file(source_path)
      import = new(source_path)
      import.execute
      import.results
    end

    private

    attr_reader :source, :cache

    def process_row(row)
      return if is_filtered?(row.snmp_community)

      customer     = get_node(row.customer_name)
      organization = get_node(row.organization_name)

      # TODO: Check for isolated organizations
      ensure_relationship_exists(root_node, customer)    if customer.parent_id.nil?
      ensure_relationship_exists(customer, organization)

      device = Gateway.where(mac: row.mac).first_or_initialize

      if device.new_record?
        logger.info "Creating #{row.mac} (#{customer.name}/#{organization.name})"
        create_device(device, row, organization)
      else
        logger.info "Updating #{row.mac} (#{customer.name}/#{organization.name})"
        update_device(device, row, organization)
      end
    rescue ActiveRecord::RecordInvalid => exception
      logger.error exception.message
    rescue => exception
      logger.error exception
    end

    def is_filtered?(name)
      ::ServiceManager.gateway_whitelist.exclude?(name)
    end

    def create_device(device, row, organization)
      configuration        = get_configuration(device)
      configuration.snmp   = get_snmp_profile(row.snmp_attributes)
      configuration.portal = get_portal_profile(row.ssl_attributes)
      configuration.save!

      device.assign_attributes(row.device_attributes)
      device.organization  = organization
      device.configuration = configuration
      device.site          = get_site(row.site_attributes, organization)
      device.location      = get_location(row.location_attributes)
      device.save!

      details = get_details(device)
      details.assign_attributes(row.device_details_attributes)
      details.save!

      if contact = get_contact(row.contact_attributes)
        device.contacts              << contact unless device.contacts.include?(contact)
        device.organization.contacts << contact unless device.organization.contacts.include?(contact)
      end

      device_sync_producer.publish(device_id: device.mac)
    end

    def update_device(device, row, organization)
      configuration      = get_configuration(device)
      configuration.snmp = get_snmp_profile(row.snmp_attributes)
      configuration.save!

      update_device_attributes(device, row.device_attributes)
      device.organization  = organization
      device.configuration = configuration
      device.save!
    end

    def update_device_attributes(device, attributes)
      device.firmware   ||= attributes[:firmware]
      device.host       ||= attributes[:host]
      device.hw_version ||= attributes[:hw_version]
      device.serial     ||= attributes[:serial]
    end

    def root_node
      @root_node ||= Organization.root
    end

    def get_node(name)
      cache.fetch("organizations/#{name}") do
        ::Organizations::FindOrCreateService.new(name).execute
      end
    end

    def ensure_relationship_exists(parent, child)
      return if parent == child
      cache.fetch("organizations/#{parent.id}/children/#{child.id}") do
        parent.children << child unless parent.children.include?(child)
      end
    end

    def get_configuration(device)
      device.configuration || device.build_configuration
    end

    def get_snmp_profile(snmp)
      community = snmp[:community]
      return if community.blank?

      cache.fetch("snmp/v2/#{community}") do
        SnmpProfile.where(snmp).first_or_create
      end
    end

    def get_portal_profile(portal)
      GatewayPortalProfile.where(portal).first_or_create
    end

    def get_details(device)
      device.details || device.build_details
    end

    def get_site(site, organization)
      name = site[:name]

      cache.fetch("organizations/#{organization.id}/sites/#{name}") do
        if name.blank?
          organization.sites.default
        else
          organization.sites.where(name: name).first_or_create
        end
      end
    end

    def get_location(location)
      ::Locations::Upsert.execute(location)
    end

    def get_country(country)
      cache.fetch("countries/#{country}") do
        Country.find_by(code: country)
      end
    end

    def get_contact(contact)
      return if contact.blank?

      token = Contact.tokenize(contact)

      cache.fetch("contacts/#{token}") do
        record = Contact.find_by_token_or_initialize(contact)
        record.token ||= token
        record.save(validate: false)
        record
      end
    end

    def default_source_path
      "#{Dir.home}/armt_device_list.tab"
    end

    def device_sync_producer
      @device_sync_producer ||= begin
        ::ServiceManager::KafkaProducer.new(Settings.kafka.device_sync.topic)
      end
    end
  end
end
