module ServiceManager
  class DeviceSyncService
    include ::Kraken::Logger.file

    def initialize(opts = {})
      @brokers  = Settings.kafka.brokers
      @group_id = Settings.kafka.device_sync.group_id
      @topic    = Settings.kafka.device_sync.topic
    end

    def start
      logger.info 'Starting service ...'
      @consumer_thread = Thread.new { consumer_loop }
      logger.info 'Started service'
    end

    def stop
      logger.info 'Stopping service ...'

      @consumer.stop if @consumer
      @consumer_thread.join

      @network_producer.shutdown           if @network_producer
      @internet_routes_producer.shutdown   if @internet_routes_producer
      @cascaded_networks_producer.shutdown if @cascaded_networks_producer

      logger.info 'Stopped service'
    end

    private

    def consumer_loop
      logger.info 'Started consumer thread ...'

      @consumer = Kafka.new(seed_brokers: @brokers).consumer(group_id: @group_id)
      @consumer.subscribe(@topic)

      @consumer.each_message do |message|
        process_message(message)
      end

      logger.info 'Stopped consumer thread'

    rescue => exception
      logger.error exception
    end

    # TODO: Allow groups to be given here instead of just devices.
    def process_message(message)
      logger.debug(message.value) if logger.debug?

      data   = JSON.parse(message.value)
      device = Device.find_by!(mac: data['device_id'])

      # Network
      group_network_config  = get_network_config(device.group_name, device.organization)
      device_network_config = get_network_config(device.mac, device)

      process_config(group_network_config, system_network_config, network_sync_service)
      process_config(device_network_config, group_network_config, network_sync_service)

      # Internet Routes
      group_internet_routes_config  = get_internet_routes_config(device.group_name, device.organization)
      device_internet_routes_config = get_internet_routes_config(device.mac, device)

      process_config(group_internet_routes_config, system_internet_routes_config, internet_routes_sync_service)
      process_config(device_internet_routes_config, group_internet_routes_config, internet_routes_sync_service)

      # Cascaded Networks
      group_cascaded_networks_config  = get_cascaded_networks_config(device.group_name, device.organization)
      device_cascaded_networks_config = get_cascaded_networks_config(device.mac, device)

      process_config(group_cascaded_networks_config, system_cascaded_networks_config, cascaded_networks_sync_service)
      process_config(device_cascaded_networks_config, group_cascaded_networks_config, cascaded_networks_sync_service)

    rescue ActiveRecord::RecordNotFound
      logger.error "Couldn't find Device #{data[:device_id]}"
    rescue => exception
      logger.error "Exception raised when processing #{message.topic}/#{message.partition} at offset #{message.offset}"
      logger.error exception
    end

    # Only perform syncs on new configuration records. Any updates to existing
    # records will flow through smx inbound or the manual sync task.
    def process_config(config, parent_config, service)
      return unless config.new_record?

      config.save!

      ensure_relationship_exists(parent_config, config)

      case config.resource
      when Device
        service.perform_sync(config.resource.name, config.resource.organization_name)
      when Organization
        service.perform_sync(config.name, config.resource.name)
      end
    end

    def network_sync_service
      @network_sync_service ||= begin
        ::Tasks::Helpers::ServiceManager::BaseSync.new(
          network_producer
        )
      end
    end

    def internet_routes_sync_service
      @internet_routes_sync_service ||= begin
        ::Tasks::Helpers::ServiceManager::InternetRoutesSync.new(
          internet_routes_producer
        )
      end
    end

    def cascaded_networks_sync_service
      @internet_routes_sync_service ||= begin
        ::Tasks::Helpers::ServiceManager::CascadedNetworksSync.new(
          cascaded_networks_producer
        )
      end
    end

    def network_producer
      @network_producer ||= begin
        ::ServiceManager::KafkaProducer.new(
          Settings.kafka.network_config_consumer.topic
        )
      end
    end

    def internet_routes_producer
      @internet_routes_producer ||= begin
        ::ServiceManager::KafkaProducer.new(
          Settings.kafka.internet_routes_consumer.topic
        )
      end
    end

    def cascaded_networks_producer
      @cascaded_networks_producer ||= begin
        ::ServiceManager::KafkaProducer.new(
          Settings.kafka.cascaded_networks_consumer.topic
        )
      end
    end

    def system_network_config
      @system_network_config ||= NetworkConfiguration.system
    end

    def system_internet_routes_config
      @system_internet_routes_config ||= InternetRoutesConfiguration.system
    end

    def system_cascaded_networks_config
      @system_cascaded_networks_config ||= CascadedNetworksConfiguration.system
    end

    def get_network_config(name, resource)
      NetworkConfiguration.where(
        name: name,
        resource: resource
      ).first_or_initialize
    end

    def get_internet_routes_config(name, resource)
      InternetRoutesConfiguration.where(
        name: name,
        resource: resource
      ).first_or_initialize
    end

    def get_cascaded_networks_config(name, resource)
      CascadedNetworksConfiguration.where(
        name: name,
        resource: resource
      ).first_or_initialize
    end

    def ensure_relationship_exists(parent, child)
      return if parent == child
      parent.children << child unless parent.children.include?(child)
    end

  end

end
