module ServiceManager
  class KafkaProducer
    include ::Kraken::Logger.file

    def initialize(topic)
      @topic = topic
      @client = Kafka.new(
        seed_brokers: Settings.kafka.brokers,
        client_id: 'smx-kafka-producer'
      )
      @producer = @client.async_producer(
        delivery_threshold: 10,
        delivery_interval: 5
      )
    end

    def publish(message)
      return unless ::ServiceManager.enabled?
      logger.debug("[#{@topic}]: #{message.to_json}") if logger.debug?
      @producer.produce(message.to_json, topic: @topic)
    rescue Kafka::BufferOverflow
      sleep 1
      retry
    end

    def shutdown
      @producer.shutdown if @producer
    end
  end
end
