module ServiceManager
  class GatewayTabfile
    include ::Kraken::Logger.file

    def initialize(tab_file_path)
      @tab_file_path = tab_file_path
      prepare_csv_opts
    end

    def each_row(&block)
      File.open(@tab_file_path, 'r:ISO-8859-1') do |file|
        CSV.foreach(file, @csv_opts) do |row|
          yield GatewayTabfileRow.new(row)
        end
      end
      self

    rescue => exception
      logger.error exception
      self
    end

    private

    def prepare_csv_opts
      CSV::HeaderConverters[:smx_header] = lambda do |key|
        key.parameterize.camelize(:lower)
      end

      CSV::Converters[:smx_value] = lambda do |value|
        value.to_s.strip.squeeze(' ')
      end

      @csv_opts = {
        col_sep: "\t",
        quote_char: "\x07",
        skip_blanks: true,
        headers: true,
        encoding: 'ISO8859-1',
        converters: :smx_value,
        header_converters: :smx_header
      }
    end
  end
end
