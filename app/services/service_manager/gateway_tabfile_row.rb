module ServiceManager
  class GatewayTabfileRow

    attr_reader :data

    delegate :pretty_print, to: :data

    def initialize(row)
      @data = ::DeviceDataMapper.gateway_smx_to_universal(row.to_h)
    end

    def customer_name
      @customer_name ||= data[:customer_id]
    end

    def organization_name
      @organization_name ||= data[:account_id]
    end

    def group_name
      @group_name ||= data[:model_id]
    end

    def mac
      @mac ||= data[:device_id]
    end

    def details
      @details ||= data.fetch(:details, {})
    end
    alias_method :device_details_attributes, :details

    def snmp
      @snmp ||= data.fetch(:snmp, {})
    end

    def ssl
      @ssl ||= data.fetch(:ssl, {})
    end

    def location
      @location ||= data.fetch(:location, {})
    end
    alias_method :location_attributes, :location

    def contact
      @contact ||= data.fetch(:contact, {})
    end
    alias_method :contact_attributes, :contact

    def category
      @category ||= DeviceCategory.find_by(name: DeviceCategory::GATEWAY_CATEGORY)
    end

    def model
      @model ||= begin
        category.models.find { |model| model.name == data[:hw_version] }
      end
    end

    def device_attributes
      {
        host:        data[:host],
        firmware:    data[:firmware],
        hw_version:  data[:hw_version],
        serial:      data[:serial],
        category_id: category.try(:id),
        model_id:    model.try(:id),
      }
    end

    def snmp_attributes
      {
        version: '2c',
        community: snmp_community
      }
    end

    def snmp_community
      @snmp_community ||= snmp[:community].to_s.downcase
    end

    def ssl_attributes
      {
        auth_user: 'support',
        ssl_password: ssl[:password]
      }
    end

    def site_attributes
      {
        name: data[:site_name]
      }
    end
  end
end
