module Devices
  class MapPinsService

    attr_reader :current_user, :params, :include_cellular

    def initialize(current_user, params, include_cellular: false)
      @current_user = current_user
      @params = params.dup
      @include_cellular = include_cellular && current_user.cellular_pins_on_map?
    end

    def execute
      map_pins = []

      devices.find_each do |device|
        map_pins << device_pin(device)   if has_location?(device)
        map_pins << cellular_pin(device) if has_cellular_location?(device)
      end

      map_pins
    end

    private

    SELECTS = [
      'devices.*',
      'locations.latitude AS location_lat',
      'locations.longitude AS location_long',
      'cellular_locations.lat AS cellular_lat',
      'cellular_locations.lon AS cellular_long'
    ].join(', ').freeze

    JOINS = [
      'LEFT JOIN locations ON devices.location_id = locations.id',
      'LEFT JOIN device_modems ON device_modems.device_id = devices.id',
      'LEFT JOIN cellular_locations ON device_modems.cellular_location_id = cellular_locations.id'
    ].freeze

    WHERE = '(locations.latitude IS NOT NULL AND locations.longitude IS NOT NULL) ' \
            'OR ' \
            '(cellular_locations.lat IS NOT NULL AND cellular_locations.lon IS NOT NULL)'

    PRELOADS = [:organization, :site, :device_state, :series, :category].freeze

    def devices
      collection = ::DevicesFinder.new(current_user, params).execute
      collection = collection.select(SELECTS)
      JOINS.each { |join_str| collection = collection.joins(join_str) }
      collection = collection.where(WHERE)
      collection = collection.preload(*PRELOADS)
      collection
    end

    def has_location?(device)
      device.location_lat.present? &&
        device.location_long.present?
    end

    def has_cellular_location?(device)
      include_cellular &&
        device.cellular_lat.present? &&
        device.cellular_long.present?
    end

    def device_pin(device)
      {
        title:              "#{device.category_name} #{device.series_name}",
        marker:             device.status_pin,
        latitude:           device.location_lat.to_f,
        longitude:          device.location_long.to_f,
        device_mac:         device.mac,
        site_id:            device.site_id,
        site_name:          device.site_name,
        organization_slug:  device.organization_slug,
        organization_name:  device.organization_name
      }
    end

    def cellular_pin(device)
      {
        title:              'Cellular Location',
        marker:             cellular_marker,
        latitude:           device.cellular_lat,
        longitude:          device.cellular_long,
        device_mac:         device.mac,
        site_id:            device.site_id,
        site_name:          device.site_name,
        organization_slug:  device.organization_slug,
        organization_name:  device.organization_name
      }
    end

    def cellular_marker
      @cellular_marker ||= ActionController::Base.helpers.asset_path('markers/orange_marker.png')
    end
  end
end
