class CarrierInformationService
  attr_accessor :records

  def initialize
    @records = ActiveSupport::OrderedHash.new
  end

  def drop
    CarrierDetail.destroy_all
    Carrier.destroy_all
  end

  def load_records_from_database
    @records[:carrier_details] = CarrierDetail.all
    @records[:carriers] = Carrier.all
    @records
  end

  def load_records_from_file
    @records = YAML.load(File.read('db/seeds/carrier_info.yml'))
  end

  def seed
    Carrier.skip_callback(:create, :after, :send_creation_notification)
    add_carrier_details
    add_carriers
    Carrier.set_callback(:create, :after, :send_creation_notification)
  end

  def serialized_records
    @records = load_records_from_database
    @records[:carrier_details] = carrier_details_to_seed_format(@records[:carrier_details])
    @records[:carriers] = carriers_to_seed_format(@records[:carriers])
    records_to_yaml
  end

  private

  def add_carriers
    carriers.each do |carrier|
      Carrier.create(
        carrier_detail_id: carrier_detail_id(carrier[:carrier_detail_name]),
        name: carrier[:name]
      )
    end
  end

  def add_carrier_details
    carrier_details.each do |carrier_detail|
      CarrierDetail.create(
        display_name: carrier_detail[:display_name],
        color: carrier_detail[:color],
        regexp: carrier_detail[:regexp],
        tail: carrier_detail[:tail]
      )
    end
  end

  def carriers_to_seed_format(carriers)
    carriers.all.map do |carrier|
      {
        carrier_detail_name: display_name_reference(carrier.carrier_detail) ,
        name: carrier.name
      }
    end
  end

  def carrier_detail_id(name)
    CarrierDetail.find_by(display_name: name).try(:id)
  end

  def carrier_details
    @carrier_details ||= records[:carrier_details]
  end

  def carrier_details_to_seed_format(carrier_details)
    carrier_details.all.map do |carrier_detail|
      {
        display_name: display_name_anchor(carrier_detail.display_name),
        color: carrier_detail.color,
        tail: carrier_detail.tail
      }
    end
  end

  def carriers
    @carriers ||= records[:carriers]
  end

  def display_name_anchor(display_name)
    "&#{display_name.parameterize('_')} #{display_name}"
  end

  def display_name_reference(carrier_detail)
    carrier_detail.present? ? "*#{carrier_detail.display_name.parameterize('_')}" : nil
  end

  # Necessary because to_yaml wraps anchors and refs in quotes which breaks them.
  def records_to_yaml
    output = ''
    records.to_yaml.each_line do |l|
      l.delete!('\\"') if l.include?(':display_name:') || l.include?(':carrier_detail_name:')
      output << l
    end
    output
  end
end
