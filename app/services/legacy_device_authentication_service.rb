class LegacyDeviceAuthenticationService

  attr_reader :device_id, :headers

  def initialize(device_id, headers)
    @device_id = device_id || ''
    @headers   = headers   || {}
  end

  def execute
    if is_sx_device? || is_valid_certificate?
      true
    else
      raise ::Aview::Device::NotAuthorizedError.new
    end
  end

  private

  def is_sx_device?
    device_id.start_with?('00270400')
  end

  def is_valid_certificate?
    is_ssl? && common_name_matches_device_id?
  end

  def is_ssl?
    headers['HTTP_X_FORWARDED_PROTO'] == 'https'
  end

  def common_name_matches_device_id?
    distinguished_name.common_name_matches?(device_id)
  end

  def distinguished_name
    ::DistinguishedName.parse(headers['HTTP_X_SSL_CERT_SDN'])
  end

end
