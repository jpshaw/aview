class DeviceAssociationService

  attr_reader :data

  def initialize(association_data)
    @data = OpenStruct.new(association_data.select { |_, v| v.presence })
  end

  def associate_devices
    return unless data_is_valid?

    association = ::DeviceAssociation.find_or_initialize_by(devices_data)

    # Set association details
    association.ip_address = data.ip_address if data.ip_address
    association.lease_type = data.lease_type if data.lease_type
    assign_device_details(association)

    association.save
  end

  def dissociate_devices
    return unless data_is_valid?

    association = ::DeviceAssociation.find_by_source_and_target(devices_data)
    association.destroy if association
  end

  private

  def data_is_valid?
    data.source_id &&
      (data.target_id || data.target_mac)
  end

  def devices_data
    @devices_data ||=
      data.to_h.slice(:source_id, :source_mac, :target_id, :target_mac)
  end

  # Assigns device-specific details from the data struct to the given association.
  #
  # Since associations can be inversed for source and target devices, this
  # method is an attempt to deduplicate the code needed to assign data.
  def assign_device_details(association)
    is_inversed = association.is_inversed_for?(data.source_id)

    self.class.association_directions.each do |direction|
      detail_direction = \
        if is_inversed
          self.class.inverse_direction(direction)
        else
          direction
        end

      self.class.association_direction_details.each do |detail|
        detail_getter = "#{detail_direction}_#{detail}"
        detail_setter = "#{direction}_#{detail}="

        next unless data.respond_to?(detail_getter) &&
          association.respond_to?(detail_setter)

        detail_value = data.send(detail_getter)
        association.send(detail_setter, detail_value) if detail_value
      end
    end
  end

  def self.association_directions
    ::DeviceAssociation::DIRECTIONS
  end

  def self.association_direction_details
    ::DeviceAssociation::DIRECTION_DETAILS
  end

  def self.inverse_direction(direction)
    inversed_association_directions_map[direction]
  end

  # A hash that maps each direction to the other
  def self.inversed_association_directions_map
    @inversed_association_directions_map ||= begin
      association_directions.reduce({}) do |hash, direction|
        hash[direction] = association_directions.find do |other_direction|
          other_direction != direction
        end
        hash
      end
    end
  end

end
