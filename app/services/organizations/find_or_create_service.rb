module Organizations
  class FindOrCreateService

    def initialize(name)
      @name = name.to_s.strip.squeeze(' ')
    end

    def execute
      find_organization || create_organization
    end

    private

    attr_reader :name

    #TODO: Check AVIEW/ARMT if find_by_name is still needed (if import_name is nil)
    def find_organization
      Organization.find_by(import_name: name) || Organization.find_by(name: name)
    end

    def create_organization
      result = OrganizationCreator.call(params: { name: name, import_name: name })
      result.organization if result.success?
    end

  end
end
