# This service keeps track of monitorable devices and flags them as being down if the devices don't
# contact the application within a predetermined period of time, calculated by adding to the last
# heartbeat value the heartbeat frequency that the device is supposed to contact the application and
# a grace period the application waits to give some extra time to the device to make contact.
#
# The first thing this service does is update the cache with latest monitorable devices information.
# In theory the cache update should be needed only the first time the service is ever run because the
# application is supposed to keep the cache information updated.
#
# After the cache is sync'ed the keys (mac addresses) are read from the cache and one by one the values
# for each key are retrieved from the cache and processed.
#
# The service keeps the cache clean by removing any invalid data it finds.
#
# The values in the cache contain enough information to calculate a device's expiration time
# (last_heartbeat_at + heartbeat frequency + grace period). After the expiration time is calculated
# it is compared against the current time and if the expiration time is prior to the current time
# the device is sent a 'down!' command and the information for the device is removed from the cache.
class DeviceHeartbeatMonitorService
  include ::Kraken::Logger.file

  DEFAULT_INTERVAL_SECONDS = 5.freeze

  attr_reader :thread

  def initialize(opts = {})
    log_info "Initializing..."
    @opts = opts
    log_cache_properties
    log_info "Initialized"
  end


  def start
    log_info "Starting..."
    @thread = Thread.new { run }
    log_info "Started"
  end


  def stop
    log_info "Stopping..."
    @done = true
    @thread.join
    log_info "Stopped"
  end


  def run
    log_info "Running..."

    sync_cache_with_monitorable_devices

    until done
      process_cache
      sleep(opts['sleep_seconds'] || DEFAULT_INTERVAL_SECONDS)
    end

    log_info "Finished running"
  end


  private

  attr_reader :opts, :mac, :cached_info, :done


  # This method has a dual purpose. The first one is to log the properties of the cache in case they
  # are needed for debugging purposes. The other is to synchronize the nodes caches when running in
  # clustered mode.
  #
  # Services run as singletons by default and the nodes that don't need to run a service (because
  # it is already being run by another node) will only instantiate (not start) the service and wait
  # until the currently running one goes down, so they can pick up the slack if necessary (by starting
  # the already instantiated service). Synchronization of cached data only happens when the cache is
  # used (by posting to, reading from or querying the cache) at least once. It follows that a dormant
  # service needs to use the cache during initialization, otherwise the cache will not be synchronized.
  # Just as an aside, instantiating the cache object is not enough, it needs to be accessed/used.
  # This method is called during initialization and it accesses the cache properties so it will sync
  # the cache with the currently running node's cache.
  def log_cache_properties
    log_info "Cache properties:"
    log_info "\tClustering mode   (should be REPL_ASYNC): #{cache.clustering_mode}"
    log_info "\tEviction strategy (should be empty) ....: #{cache.eviction_strategy}"
    log_info "\tMax entries       (should be <= 0) .....: #{cache.max_entries}"
  end


  def sync_cache_with_monitorable_devices
    log_info "Sync'ing cache with monitorable devices..."

    Device.monitorable.find_each do |device|
      cache.put(device.mac, device_info(device)) if device.last_heartbeat_at
    end

    log_info "Sync'ed cache with monitorable devices"
  end


  def cache
    @cache ||= ::HeartbeatCache.instance
  end


  def device_info(device)
    {
      last_heartbeat_at:    device.last_heartbeat_at,
      heartbeat_frequency:  device.heartbeat_frequency,
      grace_period:         device.grace_period
    }
  end


  def process_cache
    cache.keys.each do |mac|
      cached_info_for mac
      expire_device if valid_cached_info? && device_expired?
    end
  end


  def cached_info_for(mac)
    @mac          = mac
    @cached_info  = cache.get(mac)
  end


  def expire_device
    if device = Device.find_by(mac: mac)
      device.down!
    else
      log_error "Unable to find device with #{I18n.t(:mac)} '#{mac}'."
    end

    log_info "Unmonitoring device with #{I18n.t(:mac)} '#{mac}'."

    cache.remove mac
  end


  def valid_cached_info?
    if  cached_info.is_a?(Hash)             &&
        last_heartbeat_at.is_a?(Time)       &&
        heartbeat_frequency.is_a?(Integer)  &&
        grace_period.is_a?(Integer)
      return true
    end

    log_error "Invalid cached info for #{I18n.t(:mac)} '#{mac}': #{cached_info}. Unmonitoring device for the #{I18n.t(:mac)}."
    cache.remove mac
    false
  end


  def last_heartbeat_at
    cached_info[:last_heartbeat_at]
  end


  def heartbeat_frequency
    cached_info[:heartbeat_frequency]
  end


  def grace_period
    cached_info[:grace_period]
  end


  def device_expired?
    Time.now > expiration_time
  end


  def expiration_time
    last_heartbeat_at + heartbeat_frequency + grace_period
  end


  def log_info(message)
    logger.info("#{Time.now} - Info:  #{message}")
  end


  def log_error(message)
    logger.error("#{Time.now} - Error: #{message}")
  end

end
