class MacAddressCorrelationService

  attr_reader :mac

  def initialize(mac)
    @mac = mac
  end

  # Attempts to correlate the given mac address to a known device. If none are
  # found it will return the original mac, or nil if the mac is invalid.
  def execute
    return if mac.blank? || formatted_mac.blank?

    # It is possible for a correlated mac cache entry to become stale if a
    # device mac is imported and falls within the correlated range. Since the
    # chances of this are low, we can store the expensive `find_base_mac`
    # result for a while.
    ::Rails.cache.fetch("correlated_macs/#{formatted_mac}", expires_in: 6.hours) do
      correlated_mac || formatted_mac
    end
  end

  private

  def formatted_mac
    @formatted_mac ||= begin
      ::MacAddress.new(mac).to_s.upcase
    rescue ArgumentError # Invalid MAC address
      nil
    end
  end

  def correlated_mac
    correlated_mac = nil

    ::Device.subclasses.each do |klass|
      if klass.valid_mac?(formatted_mac) && klass.respond_to?(:find_base_mac)
        correlated_mac = klass.find_base_mac(formatted_mac)
        break
      end
    end

    correlated_mac
  end

end
