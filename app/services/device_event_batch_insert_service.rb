# TorqueBox service for running the consumer
class DeviceEventBatchInsertService

  def start
    ::DeviceEventBatchInsertConsumer.start
  end

  def stop
    ::DeviceEventBatchInsertConsumer.stop
  end

end
