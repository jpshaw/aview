# TorqueBox service for running the consumer
class DeviceEventNotificationService

  def start
    ::DeviceEventNotificationConsumer.start
  end

  def stop
    ::DeviceEventNotificationConsumer.stop
  end

end
