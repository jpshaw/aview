class DeviceEventProducer < ApplicationProducer

  topic 'db.devices.events'

  EXCLUDE_FIELDS = ['id', 'created_at', 'updated_at'].freeze

  def call(device, event)
    payload = {
      data: event.attributes.except(*EXCLUDE_FIELDS)
    }

    publish payload
  end

end
