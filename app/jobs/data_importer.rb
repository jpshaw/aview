class DataImporter
  include ::Kraken::Logger.file

  def initialize(options = {})
    @options = options
  end

  def run
    begin
      Import.enqueue_all_files!
    rescue => e
      logger.error "Data import job failed with error: #{e.message}"
    end
  end
end
