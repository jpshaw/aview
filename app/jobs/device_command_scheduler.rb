class DeviceCommandScheduler

  attr_reader :batch_size, :queue, :logger, :timeout, :count

  def initialize(options = {})
    @options    = options
    @batch_size = options['batch_size'] || 250
    @queue      = TorqueBox.fetch('/queues/scheduled_device_commands')
    @logger     = Rails.logger
    @timeout    = false
    @count      = 0
  end

  def on_timeout
    @timeout = true
  end

  def run
    ScheduledDeviceCommand.all_for_current_interval(batch_size: batch_size).each do |command|
      raise Timeout::Error.new('Device command scheduler timed out') if timeout

      begin
        queue.publish(command)

        @count +=  1
      rescue => e
        logger.error "Failed to schedule device command: #{command} - #{e.message}"
      end
    end

    logger.info "Scheduled #{count} device commands" if count > 0

  rescue => e
    logger.error "Device command scheduler failed with error: #{e.message}"
  end
end
