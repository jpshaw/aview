class DeviceAvailabilityMetrics
  attr_accessor :timeout
  def initialize
    @timeout = false
  end

  def run
    Operations::Metrics.measure('jobs.device.availability.metrics.elapsed') { process }
  end

  private

  def map_device_state(device_state)
    device_state.up? ? 1 : 0
  end

  def on_timeout
    @timeout = true
    raise StandardError, "#{self.class.name} job exceeded maximum time"
  end

  def process
    Device.deployed.joins(:device_state).find_each do |device|
      process_device(device)
      break if @timeout
    end
  end

  def process_device(device)
    state = map_device_state(device.device_state)
    if state
      Measurement::DeviceAvailability.create(
        mac: device.mac,
        organization_id: device.organization_id,
        state: state,
        count: 1
      )
    end
  end
end
