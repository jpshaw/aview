require 'probe_request'

# Schedules devices to be probed
class ProbeScheduler
  include ::Kraken::Logger.file
  include Operations::Metrics::Helpers

  def initialize(options = {})
    @options    = options
    @batch_size = options['batch_size'] || 250
    @queue      = TorqueBox.fetch('/queues/scheduled_probes')
    @timeout    = false
    @count      = 0
    @ttl        = 900_000 # milliseconds
  end

  def on_timeout
    @timeout = true
  end

  def run
    Gateway.probes_for_current_minute.find_each(batch_size: @batch_size) do |device|
      begin
        raise Timeout::Error.new('Probe scheduler timed out') if @timeout

        request = ProbeRequest.new(
          device_id: device.id,
          config:    device.snmp_config,
          objects:   device.snmp_objects
        )

        @queue.publish(request, encoding: :marshal, ttl: @ttl)

        @count +=  1
      rescue => e
        logger.error "Failed to schedule probe request for device #{device.mac}: #{e.message}"
      end
    end

    logger.info "Scheduled #{@count} devices to be probed" if @count > 0
    ops_metric_increment 'probes.scheduled', by: @count

  rescue => e
    logger.error "Probe scheduler failed with error: #{e.message}"
  end
end
