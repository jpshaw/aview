# Public: Periodic job that collects any notifications that should be sent
# and queues them for delivery.
class BulkNotifier
  include ::Kraken::Logger.file

  def initialize(options = {})
    @options = options
    @queue = TorqueBox.fetch('/queues/bulk_notifications')
  end

  def run
    # Must be a multiple of 15
    interval = 15.minutes
    now = Time.at((Time.now.utc.to_f/interval).round * interval).utc
    minute = now.hour * 60 + now.min

    begin
      # Returns a hash with subscription => [notifications]
      #
      # Example: {#<NotificationSubscription id: 18, user_id: 2, publisher_id: 4, publisher_type: 0, profile_id: 2, created_at: "2013-07-24 23:04:15",
      # updated_at: "2013-07-24 23:04:15">=>[#<Notification id: 14, event_id: 696, notification_subscription_id: 18, delivered_at: nil>, #<Notification id: 15,
      # event_id: 699, notification_subscription_id: 18, delivered_at: nil>]}
      #
      notifications_by_subscription = Notification.available_for_minute_of_day(minute)

      logger.info "Now: #{now}, minute: #{minute}, notifications: #{notifications_by_subscription.count}"

      # Iterate through each and add to processor queue
      notifications_by_subscription.each do |subscription, notifications|
        @queue.publish [subscription, notifications.uniq], :encoding => :marshal
      end
    rescue => e
      logger.error "Bulk notifier job failed with error: #{e.message}"
    end
  end
end
