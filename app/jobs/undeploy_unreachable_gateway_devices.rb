class UndeployUnreachableGatewayDevices
  def initialize(options = {})
    @options = options
  end

  def run
    Gateway.where('last_heartbeat_at <= ?', 30.days.ago.utc).deployed.update_all(is_deployed: false)
  end
end
