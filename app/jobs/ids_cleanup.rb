class IdsCleanup
  include ::Kraken::Logger.file

  DEFAULT_DAYS = 30
  BATCH_SIZE = 1000

  attr_reader :keep, :alerted_at, :batch_size, :elapsed, :alerts_removed

  def initialize(options = {})
    @keep = (options["days"] || DEFAULT_DAYS).to_i
    @batch_size = (options["batch_size"] || BATCH_SIZE).to_i
    @alerted_at = @keep.days.ago.beginning_of_day
    @elapsed = 0
    @alerts_removed = 0
  end

  def run
    logger.info "Removing IDS alerts older than #{keep} days in batches of #{batch_size}."
    track_time { remove_ids_alerts }
    logger.info "Finished in #{elapsed} seconds. Removed #{alerts_removed} alerts."
  end

  private

  def remove_ids_alerts
    @alerts_removed = IDSAlert.where("alerted_at < ?", alerted_at).delete_all
  end

  def track_time(&block)
    beginning_time = Time.now
    yield if block_given?
    end_time = Time.now
    @elapsed = (end_time - beginning_time).round(2)
  end
end
