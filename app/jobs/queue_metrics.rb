class QueueMetrics
  include Operations::Metrics::Helpers

  QUEUE_METRICS_MAP ||= {
    'torquebox_backgroundable'  => 'backgroundable.queued',
    'device_events'             => 'events.queued',
    'probes'                    => 'probes.queued',
    'probe_results'             => 'probes.results.queued',
    'immediate_notifications'   => 'notifications.immediate.queued',
    'bulk_notifications'        => 'notifications.bulk.queued',
    'reports'                   => 'reports.queued',
    'device_commands'           => 'commands.queued',
    'scheduled_device_commands' => 'commands.scheduled.queued',
    'firmwares_sync'            => 'firmwares.queued',
    'cellular_location'         => 'locations.cellular.queued'
  }.freeze

  def run
    TorqueBox::Messaging::Queue.list.each { |queue| process(queue) }
  end

  private

  def process(queue)
    queue_name   = queue.name.split('/').last
    metrics_name = QUEUE_METRICS_MAP[queue_name]

    return if metrics_name.nil?

    ops_metric_guage metrics_name, queue.count_messages
  end

end
