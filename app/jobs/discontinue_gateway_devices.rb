class DiscontinueGatewayDevices
  include ::Kraken::Logger.file

  def run
    if provisioned_device_macs.count.zero?
      logger.warn "No devices provisioned"
    else
      discontinued_device_macs.each do |mac|
        discontinue_device(mac)
      end
    end
  rescue => exception
    logger.error exception
  end

  private

  def discontinue_device(mac)
    retry_count ||= 3
    logger.info "Discontinuing device: #{mac}"
    device = Gateway.find_by!(mac: mac)
    device.destroy
  rescue ActiveRecord::RecordNotFound
    logger.error "Device not found: #{mac}"
  rescue ActiveRecord::StaleObjectError
    logger.warn "Stale device: #{mac} (attempt = #{retry_count})"
    # Remove the device state, the most frequent source of stale object errors
    device.device_state.try(:delete)
    retry unless (retry_count -= 1).zero?
  rescue => exception
    logger.error exception
  end

  def discontinued_device_macs
    @discontinued_device_macs ||= current_device_macs - provisioned_device_macs
  end

  def current_device_macs
    @current_device_macs ||= ::Gateway.pluck(:mac)
  end

  def provisioned_device_macs
    @provisioned_device_macs ||= begin
      macs = []
      ::ServiceManager::GatewayTabfile.new(tabfile_path).each_row do |row|
        macs << row.mac
      end
      macs
    end
  end

  def tabfile_path
    gateway_file = Import.files.first

    if gateway_file
      File.expand_path(gateway_file.location)
    else
      "#{Dir.home}/armt_device_list.tab"
    end
  end

end
