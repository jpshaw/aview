# Public: Removes carrierwave cached files older than 1 day.
class CarrierwaveCachedFilesCleanup

  ONE_DAY_IN_SECONDS = 1.day.seconds

  def initialize(options = {})
    @logger   = Rails.logger
    @options  = options
    @seconds  = (@options[:seconds] || ONE_DAY_IN_SECONDS).to_i
  end


  def run
    Dir.glob(File.join(tmp_path, '**')).each do |dir|
      Dir.foreach(dir) do |file|
        unless ['..', '.'].include?(file)
          File.delete(File.join(dir, file)) if File.mtime(File.join(dir, file)) < limit_time
        end
      end

      # Try to delete directory. Will raise SystemCallError if not empty (rescued below).
      begin
        Dir.delete(dir)
      rescue SystemCallError
        # Nothing to do. It happens if not all files inside of dir were deleted.
      end
    end
  rescue => e
    @logger.error "Carrierwave cached files cleanup job failed with error: #{e.message}"
  end


  private

  def limit_time
    @limit_time ||= Time.now - @seconds
  end


  def tmp_path
    @tmp_path ||= File.join(Rails.root, 'public', 'uploads', 'tmp')
  end

end
