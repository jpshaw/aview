class DataPlanUsage
  include ::Kraken::Logger.file
  include ActionView::Helpers::NumberHelper

  def run
    logger.info 'Started data plan usage job'
    DataPlan.find_each do |data_plan|
      process_data_usage_for data_plan if data_plan.notify_enabled && data_plan.devices.count > 0
    end
    logger.info 'Finished data plan usage job'
  end

  private

  def process_data_usage_for(data_plan)
    plan_usage = 0
    device_data = data_plan.current_usage
    device_data.each do |usage|
      device = Device.find_by(mac: usage['event']['mac'])
      if device
        device_usage = usage['event']['data_used']
        create_device_event(data_plan, device, device_usage) if data_plan.exceeds_individual_limit_for_first_time?(device, device_usage)
        device.modem.update_attribute(:data_used, device_usage) if device.modem
        plan_usage += device_usage
      end
    end
    notify_organization(data_plan, plan_usage) if data_plan.exceeds_total_limit_for_first_time?(plan_usage)
    data_plan.save_data_usage(plan_usage)
  end

  def create_device_event(data_plan, device, device_usage)
    information = 'Device data usage has exceeded individual device data usage set on data plan'
    event_data = { info: information, type: :data_usage, level: :notice, data: raw_data(data_plan, device_usage),
                   uuid: device.uuid_for(:data_usage_over_limit) }
    device.event(event_data)
  end

  def raw_data(data_plan, device_usage)
    { data_plan: data_plan.name,
      data_plan_individual_data_limit: Filesize.as_si(data_plan.individual_data_limit).pretty,
      device_data_usage_this_period: Filesize.as_si(device_usage).pretty }
  end

  def notify_organization(data_plan, plan_usage)
    data_plan.organization.users.notifiable.find_each do |user|
      email = UserMailer.data_plan_over_limit(user, data_plan, plan_usage)
      email.deliver_now if email
    end
  end
end
