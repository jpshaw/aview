require "resolv-replace.rb"
# Public: Periodic job that syncs firmwares with the firmware manager.
class FirmwaresSync

  include HTTParty

  # HTTParty methods
  base_uri Settings.firmwares_api.uri
  if Settings.firmwares_api.proxy_addr.present? && Settings.firmwares_api.proxy_port.present?
    http_proxy Settings.firmwares_api.proxy_addr, Settings.firmwares_api.proxy_port
  end

  def run
    logger.info "Starting firmware sync job"

    get_firmwares_info

    return unless valid_firmwares_info?

    enqueued = 0

    models_info.each_pair do |model_name, firmware_versions|
      if device_model = DeviceModel.find_by(name: model_name)
        firmware_versions.each do |firmware_version|
          queue.publish(
            {
              device_model_id:  device_model.id,
              firmware_version: firmware_version
            }
          )

          enqueued += 1
        end
      else
        log_error "Model '#{model_name}' was reported by the firmware management but the model " +
                  "does not exist in AView"
      end
    end

    logger.info "Enqueued #{enqueued} firmwares for processing"
  rescue => e
    log_error "FirmwaresSync job failed with error: #{e.message}"
  end

  private

  def get_firmwares_info
    response.parsed_response
  end


  def response
    @response ||= self.class.get('/list')
  end


  def valid_firmwares_info?
    error_message = "FirmwaresSync job failed with error: #{response.message}"  unless response.code == 200
    error_message = 'Firmware management did not return firmwares information'  unless models_info.present?
    log_error(error_message) and return false if error_message.present?
    true
  end


  def models_info
    @models_info ||= get_firmwares_info["firmwares"]
  end


  def log_error(message)
    logger.error("#{Time.now} - #{self.class.to_s}: #{message}")
  end

  def logger
    @logger ||= Rails.logger
  end


  def queue
    @queue ||= TorqueBox.fetch('/queues/firmwares_sync')
  end

end
