# Public: Removes events (and notifications by association) that are older
# than the configured day count (default is 30 days).
class EventCleanup
  include ::Kraken::Logger.file

  DEFAULT_DAYS = 30
  BATCH_SIZE   = 1000

  attr_reader :keep, :created_at, :batch_size, :elapsed, :events_removed,
    :notifications_removed

  def initialize(options = {})
    @keep       = (options["days"]       || DEFAULT_DAYS).to_i
    @batch_size = (options["batch_size"] || BATCH_SIZE).to_i
    @created_at = @keep.days.ago.beginning_of_day
    @elapsed    = 0 # seconds
    @events_removed        = 0
    @notifications_removed = 0
  end

  def run
    logger.info "Removing events and notifications older than #{keep} days in batches of #{batch_size}."
    track_time { remove_events }
    logger.info "Finished in #{elapsed} seconds. Removed #{events_removed} events and #{notifications_removed} notifications."
  end

  private

  def remove_events
    begin
      @events_removed        = DeviceEvent.where("created_at < ?", created_at).delete_all
      @notifications_removed = Notification.where("created_at < ?", created_at).delete_all
    rescue => e
      logger.error "Event cleanup job failed with error: #{e.message}"
    end
  end

  def track_time(&block)
    beginning_time = Time.now
    yield if block_given?
    end_time = Time.now
    @elapsed = (end_time - beginning_time).round(2) # seconds
  end
end
