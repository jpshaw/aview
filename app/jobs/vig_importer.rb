require 'set'
require 'vig/spreadsheet'

class VigImporter
  include ::Kraken::Logger.file

  def initialize
    @server_ids = Set.new
    @server_ip_address_ids = Set.new
  end

  def run
    logger.info 'Starting VIG Data Import'
    logger.error 'VIG file does not exist' and return unless VigSpreadsheet.exists?
    logger.info 'VIG file has not changed' and return unless VigSpreadsheet.changed?

    process_spreadsheet
    remove_old_objects

    logger.info "Finished VIG Data Import. " \
                "Tunnel Servers: #{@server_ids.count}, " \
                "IP Addresses: #{@server_ip_address_ids.count}"
  end

  private

  def process_spreadsheet
    VigSpreadsheet.rows.each { |row| process_row(row) }
    VigSpreadsheet.save_digest
  end

  # Finds or creates a Tunnel Server object for the given vig name + location,
  # and associates the given IP Address to it.
  def process_row(row)
    return if row.header?

    tunnel_server = TunnelServer.find_or_create_by!(name: row.name, location: row.location)
    tunnel_ip_address = TunnelServerIpAddress.find_by(ip_address: row.ip_address)

    # Tunnel Server IP Addresses are unique, so we must ensure the assocation to
    # the Tunnel Server object is correctly set.
    if tunnel_ip_address.present?

      # Check if the IP Address is associated to the current tunnel server. If
      # not, update the association.
      if tunnel_ip_address.tunnel_server_id != tunnel_server.id
        logger.warn "Reassigning IP #{row.ip_address} to server #{tunnel_server.name}"
        tunnel_ip_address.update_attributes(tunnel_server: tunnel_server)
      end

    else
      tunnel_ip_address = tunnel_server.ip_addresses.create!(ip_address: row.ip_address)
    end

    # Store object IDs to protect from removal
    @server_ids << tunnel_server.id
    @server_ip_address_ids << tunnel_ip_address.id

  rescue => e
    logger.error "Failed to process row: #{row.inspect}\n" \
                 " -> Error: #{e.message}\n" \
                 "#{e.backtrace[0,3].join("\n")}\n"
  end

  def remove_old_objects
    TunnelServer.where(id: removed_server_ids).destroy_all if removed_server_ids.any?
    TunnelServerIpAddress.where(id: removed_ip_address_ids).destroy_all if removed_ip_address_ids.any?
  end

  def removed_server_ids
    @removed_server_ids ||= TunnelServer.pluck(:id).to_a - @server_ids.to_a
  end

  def removed_ip_address_ids
    @removed_ip_address_ids ||= TunnelServerIpAddress.pluck(:id).to_a - @server_ip_address_ids.to_a
  end

end
