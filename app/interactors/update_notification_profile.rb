class UpdateNotificationProfile
  include Interactor

  def call
    context.params[:subscribed_events_attributes] = filtered_subscribed_events
    context.fail! unless context.profile.update_attributes(context.params)
  end

  private

  def filtered_subscribed_events
    subscribed_events = context.params[:subscribed_events_attributes] || []

    # Remove blank entries
    subscribed_events.reject! { |s| subscribed_event_blank?(s) }

    # If `uuid_id` is not present but the `id` is, we should remove the
    # association from the database
    subscribed_events.each do |subscribed_event|
      if subscribed_event[:id].present? && subscribed_event[:uuid_id].blank?
        subscribed_event[:_destroy] = true
      end
    end

    subscribed_events
  end

  def subscribed_event_blank?(subscribed_event)
    (subscribed_event.has_key?(:id) &&
      !subscribed_event.has_key?(:uuid_id)) &&
      subscribed_event[:id].blank?
  end
end
