class GetAbilities
  include Interactor

  def call
    context.abilities = []

    return context.fail!(message: 'No params given')           unless params.present?
    return context.fail!(message: 'No user given')             unless user.present?
    return context.fail!(message: 'Unable to find manager')    unless manager.present?
    return context.fail!(message: 'Unable to find manageable') unless manageable.present?
    return context.fail!(message: 'Unable to find role')       unless role.present?


    # If a permission is given, we can determine the new abilities by setting
    # the role and calling `allowed_abilities`.
    if permission.present?
      permission.role = role
      abilities       = permission.allowed_abilities
    else

      # Get the base abilities from the given role and intersect them by the
      # allowed abilities dicated by the manageable.
      abilities = role.abilities
      if manager.respond_to? :organization
        abilities &= manager.organization.abilities_for(manageable)
      end
    end

    context.abilities = abilities
  end

  protected

  def params
    @params ||= context.params
  end

  def user
    @user ||= context.user
  end

  def manager
    return @manager if @manager

    case params[:manager_type]
    when Permission::MANAGER_TYPE_ORGANIZATION
      @manager = user.organizations.find_by(id: params[:manager_id])
    when Permission::MANAGER_TYPE_USER
      @manager = user.users.find_by(id: params[:manager_id])
    end

    @manager
  end

  def manageable
    return @manageable if @manageable

    case params[:manageable_type]
    when Permission::MANAGEABLE_TYPE_ORGANIZATION
      # If a permission exists, we can check through the manager's managed organizations.
      @manageable = \
        if permission.present?
          manager.organizations.find_by(id: params[:manageable_id])
        else
          user.organizations.find_by(id: params[:manageable_id])
        end
    end

    @manageable
  end

  def role
    return @role if @role

    if manager.respond_to? :organization
      @role = manager.organization.roles.find_by(id: params[:role_id])
    elsif manageable.respond_to? :roles
      @role = manageable.roles.find_by(id: params[:role_id])
    end

    @role
  end

  def permission
    return @permission if @permission

    if params[:id]
      @permission = manager.manager_permissions.find_by(id: params[:id])
    end

    @permission
  end
end
