class UpdateUser
  include Interactor

  def call
    return context.fail!(message: 'No user given')   unless user.present?
    return context.fail!(message: 'No params given') unless params.present?

    begin
      # Wrap user updates in a transaction. This will roll back the database if
      # any errors are thrown during the update.
      User.transaction do
        process_associations
        context.fail! unless user.update_attributes(params)
      end
    rescue ActiveRecord::RecordInvalid  => e
      user.errors[:base] << e.message
      context.fail!
    rescue ActiveRecord::RecordNotFound => e
      # Using a manual message here, since `e.message` exposes information
      # about our database we probably don't want users to see.
      user.errors[:base] << 'Unable to find record. Please try again.'
      context.fail!
    end
  end

  protected

  # Internal: Gets the user object passed to the interactor.
  #
  # Returns the User object, if set; otherwise nil.
  def user
    @user ||= context.user
  end

  # Internal: Gets the params hash passed to the interactor.
  #
  # Returns a Hash containing the params.
  def params
    return @params if @params.present?
    @params = context.params || {}
    @params['manager_permissions_attributes'] ||= {}
    @params
  end

  # Internal: Checks if any user association params are passed, and if so, will
  # call the corresponding methods to handle them.
  def process_associations
    params.each do |key, value|
      case key
      when 'manager_permissions_attributes'
        process_permissions if value.present?
      end
    end
  end

  # Internal: Handles updating the user's permissions. It will first update any
  # existing permissions, remove leftovers, then create new permissions.
  #
  # TODO: refactor this down to separate methods, and look into leveraging
  # rails `assign_attributes` so we can access the permission objects.
  def process_permissions
    permission_params = params.delete('manager_permissions_attributes') || {}

    permission_ids   = []
    organization_ids = []

    permission_params.each do |k, v|
      id   = v['manageable_id'].to_i
      type = v['manageable_type']

      # Remove any permission params with the same manageable id.
      if type == Permission::MANAGEABLE_TYPE_ORGANIZATION && organization_ids.exclude?(id)
        organization_ids << id
      else
        permission_params.delete(k)
      end
    end

    # Update current permissions.
    user.manager_permissions.where(id: permission_params.keys).each do |p|
      data = permission_params[p.id.to_s]
      next unless data.present?

      permission_ids << p.id
      p.manageable   = organization_for_id(data['manageable_id'])
      p.role         = role_for_id(data['role_id'])
      p.save!
    end

    # Remove permissions not passed in.
    if permission_ids.any?
      user.manager_permissions
          .where('permissions.id NOT IN (?)', permission_ids)
          .destroy_all

    # If no permissions are passed in, this removes all permissions for the user.
    else
      user.manager_permissions.destroy_all
    end

    # Add new permissions for the user.
    permission_params.each do |k, v|
      next if permission_ids.include?(k.to_i)
      user.manager_permissions.create! do |p|
        p.manageable = organization_for_id(v['manageable_id'])
        p.role       = role_for_id(v['role_id'])
      end
    end
  end

  # Internal: Gets an organization within the user's scope for the given id.
  def organization_for_id(id)
    user.organization.organizations.find(id)
  end

  # Internal: Gets a role within the user's organization scope for the given id.
  def role_for_id(id)
    user.organization.roles.find(id)
  end
end
