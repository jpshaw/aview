class UpdatePermission
  include Interactor

  def call
    return context.fail!(message: 'No params given')           unless params.present?
    return context.fail!(message: 'No permission given')       unless permission.present?
    return context.fail!(message: 'Unable to find manager')    unless manager.present?
    return context.fail!(message: 'Unable to find manageable') unless manageable.present?
    return context.fail!(message: "Unable to find role with id `#{params[:role_id]}`") unless role.present?

    permission.role = role

    context.fail! unless permission.save
  end

  protected

  def params
    @params ||= context.params
  end

  def permission
    @permission ||= context.permission
  end

  def manager
    @manager ||= permission.manager
  end

  def manageable
    @manageable ||= permission.manageable
  end

  def role
    return @role if @role

    if manager.respond_to? :organization
      manager.organization.organizations.each do |org|
        @role = org.roles.find_by(id: params[:role_id])
        break if @role.present?
      end
    else
      @role = manageable.roles.find_by(id: params[:role_id])
    end

    @role
  end
end
