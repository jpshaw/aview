class PrepareForDeviceMove
  include Interactor::Organizer

  organize CopySite, CopyConfiguration, CopyDataPlan
end