require 'netbridge_configuration'

class CreateCellularConfiguration
  include Interactor

  def call
    context.configuration = configuration

    return context.fail!(message: 'No params given') unless params.present?
    return context.fail!(message: 'No user given')   unless user.present?

    # Check if the parent configuration id is within user's scope.
    if organization_id_invalid?
      return context.fail!(message: 'Unable to find organization for user')
    end

    # Check if the organization_id is within user's scope.
    if parent_id_invalid?
      return context.fail!(message: 'Unable to find parent configuration for user')
    end

    context.fail! unless configuration.apply_attributes(params, locks)
  end

  protected

  def params
    @params ||= context.params
  end

  def locks
    @locks ||= context.locks
  end

  def configuration
    @configuration ||= NetbridgeConfiguration.new
  end

  def user
    @user ||= context.user
  end

  def organization_id_invalid?
    user.organizations.find_by(id: params[:organization_id]).blank?
  end

  def parent_id_invalid?
    return false if params[:parent_id].blank?
    user.netbridge_configurations.find_by(id: params[:parent_id]).blank?
  end

end
