class UpdateRole
  include Interactor

  def call
    return context.fail!(message: 'No role given')   unless role.present?
    return context.fail!(message: 'No params given') unless params.present?
    return context.fail!(message: 'No user given')   unless user.present?

    # Set ability associations
    process_abilities

    # Update attributes
    context.fail! unless role.update_attributes(params)
  end

  protected

  def process_abilities
    abilities   = params.delete('abilities_attributes') || {}
    ability_ids = abilities.keys

    # There may be abilities that are set for the role that are aren't available
    # to the user who is updating the role. These should always be set.
    abilities_not_available_to_user = role.abilities - user.abilities

    # The abilities the user wants to apply to the role.
    updated_abilities = user.abilities_for(role.organization).where(id: ability_ids)

    # All of the abilities the role should have, which is the abilities the
    # user selected plus any abilities already associated to the role that aren't
    # available to the user.
    all_role_abilities = abilities_not_available_to_user + updated_abilities

    # The abilities that should be removed from the role.
    abilities_to_remove = role.abilities - all_role_abilities

    # Ensure the selected abilities are associated with the role.
    updated_abilities.each do |ability|
      role.abilities << ability unless role.abilities.include?(ability)
    end

    # Remove the abilities not selected by the user.
    abilities_to_remove.each do |ability|
      role.abilities.delete(ability)
    end
  end

  def role
    @role ||= context.role
  end

  def params
    @params ||= context.params
  end

  def user
    @user ||= context.user
  end
end
