class MoveDevicesToOrganization
  include Interactor

  def call
    context.errors = errors
    return context.fail!(message: "No #{I18n.t(:macs)} given") unless context.device_macs.present?
    return context.fail!(message: 'Unable to find organization') unless context.organization_id.present? && organization.present?
    context.organization = organization
    context.device_count = device_macs.size
    assign_organization_to_devices
    return context.fail!(message: "Could not assign the organization to #{failed_device_count} #{'device'.pluralize(failed_device_count)}.
      Successfully assigned #{@successful_device_count} #{'device'.pluralize(@successful_device_count)}.") unless errors.empty?
  end

  protected

  def current_user
    context.current_user
  end

  def device_macs
    @device_macs ||= context.device_macs.split(',')
  end

  def organization
    @organization ||= current_user.organizations.find_by(id: context.organization_id)
  end

  def device
    context.device
  end

  def errors
    @errors ||= []
  end

  def failed_device_count
    device_macs.size - @successful_device_count
  end

  def assign_organization_to_devices
    @successful_device_count = 0
    device_macs.each do |mac|
      set_organization_to_device(mac)
    end
  end

  def set_organization_to_device(mac)
    context.device = current_user.devices.find_by(mac: mac)
    return errors << "#{mac}: Failed to find device" unless device.present?
    preparation_result = PrepareForDeviceMove.call(context.dup)
    return errors << preparation_result.message unless preparation_result.success?
    device.organization = organization
    return add_device_errors unless device.save
    @successful_device_count += 1
  end

  def add_device_errors
    device.errors.full_messages.each do |message|
      errors << "#{device.mac}: #{message}"
    end
  end
end
