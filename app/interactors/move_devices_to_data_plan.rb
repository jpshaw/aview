class MoveDevicesToDataPlan
  include Interactor

  def call
    context.errors = errors
    return context.fail!(message: "No #{I18n.t(:macs)} given") unless context.device_macs.present?
    return context.fail!(message: 'Unable to find data plan') if context.data_plan_id.present? && !data_plan.present?
    context.data_plan = data_plan
    context.device_count = device_macs.size
    assign_data_plan_to_devices
    return context.fail!(message: "Could not assign the data plan to #{failed_device_count} #{'device'.pluralize(failed_device_count)}.
      Successfully assigned #{@successful_device_count} #{'device'.pluralize(@successful_device_count)}.") unless errors.empty?
  end

  protected

  def device_macs
    @device_macs ||= context.device_macs.split(',')
  end

  def data_plan
    @data_plan ||= DataPlan.find_by(id: context.data_plan_id)
  end

  def errors
    @errors ||= []
  end

  def failed_device_count
    device_macs.size - @successful_device_count
  end

  def assign_data_plan_to_devices
    @successful_device_count = 0
    device_macs.each do |mac|
      set_data_plan_to_device(mac)
    end
  end

  def set_data_plan_to_device(mac)
    device = Device.find_by(mac: mac)
    return errors << "#{mac}: Failed to find device" unless device
    return errors << "#{mac}: Device does not support a cellular modem" unless device.cellular_modem_supported?
    device.data_plan = data_plan
    return add_device_errors(device) unless device.save
    @successful_device_count += 1
  end

  def add_device_errors(device)
    device.errors.full_messages.each do |message|
      errors << "#{device.mac}: #{message}"
    end
  end
end
