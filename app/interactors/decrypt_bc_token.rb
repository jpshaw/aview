class DecryptBcToken
  include Interactor

  def call
    client = VoltageClient::DefaultApi.new
    result = JSON.parse(client.decrypt_get(context.token))
    decrypted_text = result["decryptedText"]

    params = decrypted_text.split("&").each_with_object({}) do |pair, hash|
      key, value = pair.split(/\s*=\s*/)
      hash[key] = value
    end

    context.account_id = params["smAccountID"]
    context.user_id    = params["smUserID"]
    context.device_id  = params["mac"]
    context.timestamp  = DateTime.parse(params["timestamp"]).utc

    if context.timestamp < 60.seconds.ago.to_datetime.utc
      context.fail!(message: "Invalid token timestamp")
    end

  rescue VoltageClient::ApiError => e
    context.fail!(message: "Failed to decrypt Business Center Token: #{e.message}")
  end
end
