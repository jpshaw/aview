class CompileDeviceConfiguration
  include Interactor

  def call
    return context.fail!(message: 'No configuration given') unless device_configuration.present?
    return context.fail!(message: 'Configuration is invalid') unless device_configuration.valid?

    compiler = DeviceConfigurationCompiler.new
    compiler.compile_device_configuration(device_configuration)

    return context.fail!(message: 'Configuration failed to save') unless device_configuration.save

    compiler.compile_children(device_configuration)
    context.errors = compiler.errors

    context.message = compiler.child_compile_results if context.errors.any?
  end

  protected

  def device_configuration
    @device_configuration ||= context.device_configuration
  end
end