class CreateUserOrganizationPermission
  include Interactor

  def call
    context.permission            = context.user.manager_permissions.new
    context.permission.manageable = context.organization
    context.permission.role       = context.role

    context.fail! unless context.permission.save
  end

  def rollback
    context.permission.destroy
  end

end