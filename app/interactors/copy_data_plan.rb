class CopyDataPlan
  include Interactor

  def call
    device.data_plan = copy_data_plan? ? get_data_plan : nil
  end

  def rollback
    new_data_plan.destroy
  end

  private

  def device
    context.device
  end

  def organization
    context.organization
  end

  def copy_data_plan?
    context.copy_data_plan && device.data_plan.present?
  end

  def get_data_plan
    organization.data_plans.find_by(name: device.data_plan.name) || duplicate_data_plan(device.data_plan)
  end

  def new_data_plan
    context.new_data_plan
  end

  def duplicate_data_plan(data_plan)
    context.new_data_plan = data_plan.dup
    new_data_plan.organization = organization
    return context.fail!(message: "#{device.mac}: Failed to save new data plan") unless new_data_plan.save
    new_data_plan
  end
end