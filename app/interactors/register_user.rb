require 'permission'

class RegisterUser
  include Interactor

  def call
    return context.fail!(message: 'No organization given') unless organization.present?
    return context.fail!(message: 'No params given')       unless params.present?

    context.user = new_user

    User.transaction do
      build_permissions
      context.fail! unless new_user.save
    end
  end

  protected

  # Internal: Gets the organization passed to the interactor.
  def organization
    @organization ||= context.organization
  end

  # Internal: Gets the params passed to the interactor.
  def params
    @params ||= context.params
  end

  def new_user
    @new_user ||= organization.users.new params
  end

  # Internal: Builds permissions for the new user.
  def build_permissions
    user_permissions = []
    manageables      = []

    new_user.manager_permissions.each do |permission|
      # Ensure the manageable is within the user organization's scope.
      next if manageable_orgs.exclude?(permission.manageable)

      # Ensure the role is within the user organization's scope.
      next if org_roles.exclude?(permission.role)

      # Users should only have a single permission for a manageable.
      next if manageables.include?(permission.manageable)

      # Assign user to permission. This is required.
      permission.manager = new_user

      manageables        << permission.manageable
      user_permissions   << permission
    end

    new_user.manager_permissions = user_permissions
  end

  # Internal: Returns an array of manageable organizations for the given org.
  def manageable_orgs
    @manageable_orgs ||= organization.organizations
  end

  def org_roles
    @org_roles ||= organization.roles
  end
end
