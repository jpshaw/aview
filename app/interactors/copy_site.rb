class CopySite
  include Interactor

  def call
    device.site = copy_site_id? ? get_site_id : nil
  end

  def rollback
    new_site.destroy if new_site.present?
  end

  private

  def device
    context.device
  end

  def organization
    context.organization
  end

  def new_site
    context.new_site
  end

  def get_site_id
    return nil unless device.site.present?
    organization.sites.find_by(name: device.site_name) || duplicate_site(device.site)
  end

  def copy_site_id?
    context.copy_site_id
  end

  def duplicate_site(site)
    context.new_site = site.dup
    new_site.organization = organization
    return context.fail!(message: "#{device.mac}: Failed to save new site") unless new_site.save
    new_site
  end
end
