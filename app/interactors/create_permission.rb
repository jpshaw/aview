class CreatePermission
  include Interactor

  def call
    context.permission = Permission.new

    return context.fail!(message: 'No params given')  unless params.present?
    return context.fail!(message: 'No user given')    unless user.present?
    return context.fail!(message: 'No manager given') unless manager.present?

    if Permission::VALID_MANAGEABLE_TYPES.exclude?(params[:manageable_type])
      return context.fail!(message: 'Invalid manageable type')
    end

    if manageable.blank?
      return context.fail!(message: "Unable to find #{params[:manageable_type]} for the current user")
    end

    if role.blank?
      return context.fail!(message: "Unable to find role with id `#{params[:role_id]}`")
    end

    context.permission.manager    = manager
    context.permission.manageable = manageable
    context.permission.role       = role

    context.fail! unless context.permission.save
  end

  protected

  def params
    @params ||= context.params
  end

  def user
    @user ||= context.user
  end

  def manager
    @manager ||= context.manager
  end

  def manageable
    return @manageable if @manageable

    case params[:manageable_type]
    when Permission::MANAGEABLE_TYPE_ORGANIZATION
      @manageable = user.organizations.find_by(id: params[:manageable_id])
    end

    @manageable
  end

  def role
    return @role if @role

    if manager.respond_to? :organization
      manager.organization.organizations.each do |org|
        @role = org.roles.find_by(id: params[:role_id])
        break if @role.present?
      end
    else
      @role = manageable.roles.find_by(id: params[:role_id])
    end

    @role
  end
end
