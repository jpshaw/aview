class CreateRole
  include Interactor

  def call
    return context.fail!(message: 'No organization given') unless organization.present?
    return context.fail!(message: 'No params given')       unless params.present?
    return context.fail!(message: 'No user given')         unless user.present?

    # Remove the ability attributes so we can create the role directly from
    # the params hash.
    abilities   = params.delete('abilities_attributes') || {}
    ability_ids = abilities.keys

    role = organization.roles.new params
    context.role = role

    if role.save
      # For new roles, only use abilities that are within the user's scope.
      user.abilities_for(organization).where(id: ability_ids).each do |ability|
        role.abilities << ability
      end
    else
      context.fail!
    end
  end

  protected

  def organization
    @organization ||= context.organization
  end

  def params
    @params ||= context.params
  end

  def user
    @user ||= context.user
  end
end
