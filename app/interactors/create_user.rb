require 'securerandom'

class CreateUser
  include Interactor

  def call
    context.fail! and return if context.organization.blank?
    context.fail! and return if context.role.blank?

    context.user = context.organization.users.new(context.params)

    # Generate random password for validation
    password                           = SecureRandom.hex(20).to_s
    context.user.password              = password
    context.user.password_confirmation = password

    context.fail! unless context.user.save
  end

  def rollback
    context.user.destroy
  end

end