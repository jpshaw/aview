require 'ipaddr'

class UpdateDeviceDns
  include Interactor

  IPV4_RECORD_TYPE = 'A'
  IPV6_RECORD_TYPE = 'AAAA'

  def call
    context.fail! unless update_dns_record
  end

  def update_dns_record
    # Grab or create the dns record
    record = PowerDnsRecordManager::Record.where(name: dns_hostname,
                                                 type: ip_record_type)
                                          .first_or_initialize

    # Update the DNS record with the device's IP Address
    record.content = ip_address.to_s

    if record.save
      # Destroy any dns records for the other type (IPv4/IPv6)
      PowerDnsRecordManager::Record.destroy_all(name: dns_hostname,
                                                type: other_record_type)
      true
    else
      false
    end

  # Thrown if an invalid IP Address is given
  # rescue IPAddr::InvalidAddressError, IPAddr::AddressFamilyError TODO: uncomment once we are using 2.0 syntax (AV-2186)
  rescue ArgumentError
    false
  end

  def dns_hostname
    @dns_hostname ||= "#{context.mac}.#{Settings.powerdns.domain.name}"
  end

  def ip_address
    @ip_address ||= IPAddr.new(context.host)
  end

  # Check if this is an IPv4 (A) or IPv6 (AAAA) record type.
  def ip_record_type
    @ip_record_type ||= ip_address.ipv6? ? IPV6_RECORD_TYPE : IPV4_RECORD_TYPE
  end

  # Used to clear out stale dns entries for the device
  def other_record_type
    @other_record_type ||= ip_record_type == IPV4_RECORD_TYPE ? IPV6_RECORD_TYPE : IPV4_RECORD_TYPE
  end

end
