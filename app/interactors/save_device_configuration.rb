class SaveDeviceConfiguration
  include Interactor

  def call
    return context.fail!(message: 'No device found') unless device.present?
    return context.fail!(message: 'No group configuration given') if context.group_configuration_id.nil?
    return context.fail!(message: 'Can not assign this configuration to this device') if context.group_configuration_id.blank? && device.configuration.blank?

    if context.group_configuration_id == '-1'
      remove_configuration
    else
      update_configuration
    end
  end

  private

  def device
    @device ||= context.device
  end

  def group_configuration
    @group_configuration ||= \
      if group_configuration_changed?
        DeviceConfiguration.find_by(id: context.group_configuration_id)
      else
        current_group_configuration
      end
  end

  def group_configuration_changed?
    return true if current_group_configuration.nil?
    return false if context.group_configuration_id.blank?
    current_group_configuration.id != context.group_configuration_id
  end

  def current_group_configuration
    @current_group_configuration ||= device_config_is_individual? ? device.configuration.parent : device.configuration
  end

  def individual_configuration
    @individual_configuration ||= device.configuration if device_config_is_individual?
    @individual_configuration ||= DeviceConfiguration.find_by(name: individual_config_name)
    @individual_configuration ||= group_configuration.dup
  end

  def firmware
    @firmware ||= group_configuration.device_firmware if context.firmware_id.blank?
    @firmware ||= DeviceFirmware.find_by(id: context.firmware_id)
  end

  def grace_period_duration
    @grace_period_duration ||= group_configuration.grace_period_duration if context.grace_period_duration.nil?
    @grace_period_duration ||= context.grace_period_duration
  end

  def individual_config_name
    "#{device.mac} Config"
  end

  def new_settings
    @new_settings ||= JSON.parse(context.settings)
  rescue JSON::ParserError
    return context.fail!(message: 'Settings are invalid')
  end

  def remove_configuration
    configuration = device.configuration if device_config_is_individual?
    device.configuration = nil
    context.message = 'Successfully removed configuration' if device.save
    configuration.destroy if configuration.present?
  end

  def update_configuration
    return context.fail!(message: 'Group configuration not found') unless group_configuration.present?
    return context.fail!(message: 'Settings are empty') if context.settings.blank?
    update_firmware
    update_grace_period
    update_settings
    update_parent_disconnect
    update_individual_configuration
    context.message = 'Successfully saved configuration'
  end

  def update_firmware
    individual_configuration.device_firmware = firmware
    individual_configuration.device_firmware_locked = context.firmware_id.present?
  end

  def update_grace_period
    individual_configuration.grace_period_duration = grace_period_duration
    individual_configuration.grace_period_duration_locked = !context.grace_period_duration.nil?
  end

  def update_settings
    individual_configuration.compiled_settings = context.settings
    individual_configuration.settings = difference(new_settings,
                                                   group_configuration.parsed_compiled_settings).to_json
  end

  def update_parent_disconnect
    compatible_firmwares = group_configuration.device_firmware.all_compatible_firmwares
    individual_configuration.parent_disconnect = !compatible_firmwares.include?(firmware)
  end

  def update_individual_configuration
    if settings_different? || firmware_different? || grace_period_different?
      update_basic_attributes
      save_individual_configuration
    else
      device.configuration = group_configuration
      return context.fail!(message: 'Failed to save device') unless device.save
      individual_configuration.destroy
    end
  end

  def update_basic_attributes
    individual_configuration.organization = device.organization
    individual_configuration.name = individual_config_name
    individual_configuration.parent = group_configuration
    individual_configuration.record_type = 'individual'
  end

  def save_individual_configuration
    return context.fail!(message: 'Failed to save configuration') unless individual_configuration.save
    device.configuration = individual_configuration
    context.fail!(message: 'Failed to save device') unless device.save
  end

  def device_config_is_individual?
    device.configuration.present? && device.configuration.individual?
  end

  def firmware_different?
    individual_configuration.device_firmware_locked
  end

  def grace_period_different?
    individual_configuration.grace_period_duration_locked
  end

  def settings_different?
    group_configuration.parsed_compiled_settings != new_settings
  end

  def difference(hash_a, hash_b)
    hash_a.reject do |k,v|
      hash_b.has_key?(k) && hash_b[k] == v
    end
  end
end
