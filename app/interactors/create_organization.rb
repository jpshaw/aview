class CreateOrganization

  include Interactor

  def call
    organization        = Organization.new(context.params)
    organization.parent = context.parent if context.parent.present?

    context.organization = organization
    context.fail! unless organization.save
  end

  def rollback
    context.organization.destroy
  end

end
