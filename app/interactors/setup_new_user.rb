class SetupNewUser
  include Interactor::Organizer

  organize CreateUser, CreateUserOrganizationPermission, SendRegistrationEmail
end