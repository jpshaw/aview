class FetchBcUserSmxAuthLevel
  include Interactor

  def call
    client = SmxIdentityClient::DefaultApi.new
    result = client.get_authorization(context.account_id, context.user_id)
    auth_level = result.try(:data).try(:level)

    if User.valid_auth_level?(auth_level)
      context.auth_level = auth_level
    else
      context.fail!(message: "Invalid Service Manager Authorization Level: #{auth_level}")
    end

  rescue SmxIdentityClient::ApiError => e
    context.fail!(message: "Unexpected error occured when contacting Service Manager: #{e.message}")
  end
end
