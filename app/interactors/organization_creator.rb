require 'create_organization'
require 'create_organization_admin_role'
require 'create_organization_self_managing_permission'

class OrganizationCreator

  include Interactor::Organizer

  organize CreateOrganization, CreateOrganizationAdminRole, CreateOrganizationSelfManagingPermission

end
