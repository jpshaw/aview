class CreateOrganizationSelfManagingPermission

  include Interactor

  def call
    organization = context.organization
    organization.manageable_permissions.create!(
      manager:  organization,
      role:     organization.admin_role
    )
  rescue
    organization.errors.add :base, "Self managing permission could not be created."
    context.fail!
  end

  def rollback
    context.organization.self_managing_permission.destroy
  end

end
