class UpdateOrganization
  include Interactor

  def call
    return context.fail!(message: 'No organization given') unless organization.present?
    return context.fail!(message: 'No params given')       unless params.present?
    return context.fail!(message: 'No user given')         unless user.present?

    process_parent

    context.fail! unless organization.update_attributes(params)

    expire_brandable_attributes if brandable_attributes_changed?
  end

  protected

  def process_parent
    parent_id = params.delete(:parent_id).to_i
    return if [organization.id, organization.parent_id].include?(parent_id)

    return if organization.root?

    # User must be able to modify the existing parent before moving the
    # organization from it to another.
    return unless organization.parent.present?
    return unless user.has_ability_on?(organization.parent, to: 'Modify Organizations')

    # Search within user's modifiable organizations for the parent.
    new_parent = user.modifiable_organizations.find_by(id: parent_id)
    return unless new_parent.present?

    # Add organization to parent. Closure tree will handle updating the hiearachy
    # table to move the organization from the old parent to the new one.
    new_parent.add_child(organization)
  end

  def organization
    @organization ||= context.organization
  end

  def params
    @params ||= context.params
  end

  def user
    @user ||= context.user
  end


  private

  def expire_brandable_attributes
    # A change in a brandable attribute can affect all users in the organization's hierarchy. Changes
    # in brandable attributes are unlikely to happen often so expiring the cache of brandable attributes
    # of all users in the hierarchy should not have a major impact on the application's performance.
    organization.self_and_descendants.pluck(:id).each do |org_id|
      changed_attributes.each do |attr|
        Rails.cache.delete("#{org_id}_#{attr}")
      end
    end
  end


  def brandable_attributes_changed?
    changed_attributes.present?
  end


  def changed_attributes
    @changed_attributes ||= begin
      # Using variables to avoid hard to find typo style bugs.
      remove_logo = 'remove_logo'
      logo        = 'logo'

      # When removing a logo the key 'logo' is not present in the parameters, instead 'remove_logo'
      # is. We need to account for that special case.
      attrs = (params.keys & (Organization::BRANDABLE_ATTRIBUTES + [remove_logo]))
      attrs << logo if !attrs.include?(logo) && attrs.delete(remove_logo)
      attrs
    end
  end

end
