class CopyConfiguration
  include Interactor

  def call
    device.configuration.destroy and return if individual_configuration?
    device.configuration = copy_configuration? ? get_configuration : nil
  end

  def rollback
    new_configuration.destroy
  end

  private

  def device
    context.device
  end

  def organization
    context.organization
  end

  def new_configuration
    context.new_configuration
  end

  def individual_configuration?
    device.configuration.present? && device.is_platform?(:uc_linux) && device.configuration.individual?
  end

  def copy_configuration?
    device.has_gateway_configuration? ||
      (context.copy_configuration && device.configuration.present?)
  end

  def get_configuration
    existing_configuration || duplicate_configuration(device.configuration)
  end

  def duplicate_configuration(configuration)
    context.new_configuration = configuration.dup
    new_configuration.organization = organization
    return context.fail!(message: "#{device.mac}: Failed to save new configuration") unless new_configuration.save
    new_configuration
  end

  def existing_configuration
    return if device.has_gateway_configuration?
    return organization.netbridge_configurations.find_by(name: device.configuration_name) if device.has_legacy_configuration?
    organization.device_configurations.find_by(name: device.configuration_name)
  end
end