class AuthenticateBusinessCenterUser
  include Interactor::Organizer

  organize DecryptBcToken, FetchBcUserSmxAuthLevel, CreateBcUser
end
