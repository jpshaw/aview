class CreateBcUser
  include Interactor

  def call
    user = User.for_smx_auth(auth_level: context.auth_level,
                                account: context.account_id,
                               username: context.user_id)
    user.set_authorizations
    context.user = user

    if context.device_id.present?
      context.device = context.user.devices.find_by(mac: context.device_id)
    end

  rescue => e
    context.fail!(message: "Failed to create user: #{e.message}")
  end
end
