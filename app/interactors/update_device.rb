class UpdateDevice
  include Interactor

  def call
    return context.fail!(message: 'No device given') unless device.present?
    return context.fail!(message: 'No user given')   unless user.present?
    return context.fail!(message: 'No params given') unless params.present?

    apply_modifiable_attributes
    apply_site
    apply_modifiable_hosts
    apply_configuration
    apply_data_plan
    apply_comment

    unless device.update_attributes(attributes)
      context.fail!(message: device.errors.full_messages.join(', '))
    end
  end

  protected

  def device
    @device ||= context.device
  end

  def user
    @user ||= context.user
  end

  def params
    @params ||= context.params
  end

  def policy
    @policy ||= Pundit.policy(user, device)
  end

  def attributes
    @attributes ||= {}
  end

  def apply_modifiable_attributes
    return unless policy.modify?

    add_attribute(:hostname)
    add_attribute(:organization_id) if policy.change_organization?
  end

  def apply_site
    return unless policy.modify_site?

    # Did site change?
    if params.has_key?(:site_name)
      if params[:site_name].blank?
        return context.fail!(message: "Site ID can't be blank")
      end

      # If so, ensure the site id belongs to the device's organization
      site = device_organization.sites.for_name(params[:site_name])
      attributes[:site_id] = site.id if site.present?
    end
  end

  def apply_modifiable_hosts
    return unless policy.modify_hosts?
    add_attribute(:mgmt_host)
  end

  def apply_configuration
    return unless policy.change_configuration?
    add_attribute :configuration_id
  end

  def apply_data_plan
    add_attribute(:data_plan_id) if modify_device_organization?
  end

  def apply_comment
    add_attribute(:comment) if policy.modify_comment?
  end

  def modify_device_organization?
    Pundit.policy(user, device.organization).modify?
  end

  def add_attribute(key)
    if params.has_key?(key)
      attributes[key] = params[key]
    end
  end

  # If the organization has changed, return the new one. Otherwise return the
  # device's current organization.
  def device_organization
    Organization.find_by(id: params[:organization_id]) || device.organization
  end

end
