class SendRegistrationEmail
  include Interactor

  def call
    # Generate new password token
    token = context.user.send(:set_reset_password_token)

    # Send registration email
    RegistrationMailer.welcome_email(context.user, token).deliver_now
  end

end