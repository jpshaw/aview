class CreateOrganizationAdminRole

  include Interactor

  def call
    organization.build_admin_role(name: 'Admin', description: 'Has all abilities')
    organization.admin_role.admin = true

    if organization.save
      process_abilities
    else
      organization.errors.add :base, "An admin role for the organization could not be created"
      context.fail!
    end
  end

  def rollback
    organization.admin_role.destroy
  end

  def process_abilities
    abilities   = context.abilities
    return unless abilities.present?

    ability_ids = abilities.keys

    user.abilities_for(organization).where(id: ability_ids).each do |ability|
      organization.admin_role.abilities << ability
    end
  end

  def user
    @user ||= context.user
  end

  def organization
    @organization ||= context.organization
  end
end
