# Public: A mixin that injects the event-related methods.
module Events
  extend ActiveSupport::Concern

  included do
    has_many :events, class_name: 'DeviceEvent'
  end

  # The interface method used to process events for the device.
  def process_event(event)
    raise NotImplementedError, 'Please implement an event processor method for the device.'
  end

  # Public: Creates a device event.
  #
  # data - The Hash of data used to create the event (default: {}):
  #        :info  - The String that describes the event.
  #        :type  - The Symbol used to classify the event type.
  #        :level - The Symbol used to classify the event severity level.
  #        :data  - The Hash of data that represents the event (optional).
  #        :uuid  - The Symbol of the uuid that represents the event, used to
  #                 determine whether to send a notification (optional).
  #
  # Examples
  #
  #   device.event :info => "Primary connection has changed", :level => :notice,
  #     :type => :dhcp, :data => { :dhcp => { ... data ...} }, :uuid => :gateway_ip_changed
  #   # => #<DeviceEvent id: 3199, device_id: 2, severity: 5, information: "Primary connection has changed",
  #         event_type: 4, raw_data: {"dhcp"=>{"hello"=>"world"}}, uuid_id: 5 ...>
  #
  # Returns the newly created event if valid data is passed, otherwise nil.
  def event(data = {})
    return unless data.is_a?(Hash)

    data[:info] ||= ''
    data[:type] ||= :status

    details = {
      information: data[:info],
      event_type:  events.type_code_for_symbol(data[:type]),
      level:       events.level_code_for_symbol(data[:level])
    }

    details[:raw_data] = data[:data].nil? ? { data[:type] => data[:info] } : data[:data]

    if data[:created_at].present?
      details[:created_at] = data[:created_at]
    end

    if uuid = DeviceEventUUID.find_by_symbol(data[:uuid])
      details[:uuid_id] = uuid.id
    end

    event = ::DeviceEvent.new(details.merge(device_id: id))
    ::DeviceEventProducer.call(self, event)
    event
  end

  # Create a symbol for the device's uuid for the given name.
  def uuid_for(name)
    self.class.uuid_for(name)
  end

  module ClassMethods
    # Create a symbol for the device's uuid for the given name.
    #
    # Examples:
    #
    #   RemoteManager.uuid_for(:signal_changed)
    #    => :remotemanager_signal_changed
    #
    #   Netbridge.uuid_for(:ip_changed)
    #    => :netbridge_ip_changed
    #
    def uuid_for(name)
      "#{uuid_prefix}_#{name}".downcase.to_sym
    end

    # Return the device's uuid prefix.
    def uuid_prefix
      @uuid_prefix ||= self.to_s
    end

    # Fetches and memoizes the notification options for the device model, if
    # any are set. Otherwise it will return an empty array.
    def notification_options
      @notification_options ||= begin
        constant = "#{uuid_prefix}_UUIDS".upcase

        if EventUUIDS.const_defined?(constant)
          EventUUIDS.const_get(constant).map do |code|
            DeviceEventUUID.find_by_code(code)
          end.compact
        else
          []
        end
      end
    end
  end

end
