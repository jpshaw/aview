require 'platforms'

module ActsAsPlatform
  extend ActiveSupport::Concern

  def platform
    self.class.platform
  end

  def is_platform?(name)
    self.class.is_platform?(name)
  end

  def is_smx_device?
    is_platform?(:gateway)
  end

  module ClassMethods
    def acts_as_platform(name)
      @platform = name
      send :include, "Platforms::#{name.to_s.classify}".constantize
    end

    def platform
      @platform
    end

    def is_platform?(name)
      self.platform.to_s == name.to_s
    end
  end
end
