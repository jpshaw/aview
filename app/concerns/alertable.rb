module Alertable
  extend ActiveSupport::Concern
  include Events

  def alert_up
    event(info: 'Device is up', level: :notice, type: :status, uuid: uuid_for(:up))
    Operations::Metrics.increment 'monitoring.up'
  end

  def alert_down
    event(info: 'Device is down', level: :alert, type: :status, uuid: uuid_for(:down))
    Operations::Metrics.increment 'monitoring.down'
  end

  def alert_unreachable
    event(info: 'Device is unreachable', level: :warning, type: :status, uuid: uuid_for(:unreachable))
    touch :unreachable_at
  end
end
