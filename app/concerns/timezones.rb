# Public: Adds the timezone association and relevant methods.
module Timezones
  extend ActiveSupport::Concern

  included do
    belongs_to :timezone

    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :timezone_id
    validates :timezone_id, :presence => true, :numericality => { :only_integer => true }
    after_initialize :default_timezone, :if => :new_record?
  end

  private

  # Internal: Set the default timezone, if one isn't already set.
  def default_timezone
    self.timezone ||= Timezone.default
  end
end
