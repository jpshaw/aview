module ApiAuthenticatable
  extend ActiveSupport::Concern

  # NOTE: These may eventually be based on the user's organization.
  TIME_WINDOW  = 15.minutes.to_i
  MAX_REQUESTS = 100

  included do
    devise :token_authenticatable

    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :authentication_token
  end

  def api_request_limit
    MAX_REQUESTS
  end

  def api_time_window
    TIME_WINDOW
  end

  module ClassMethods

    def find_by_authentication_token(token)
      # Ensure we have a token to search for, as blank tokens can return any
      # user that hasn't activated their api token.
      return nil if token.blank?

      user = User.where(authentication_token: token).first

      # Use Device.secure_compare to mitigate timing attacks.
      if user and Devise.secure_compare(user.authentication_token, token)
        return user
      else
        return nil
      end
    end

  end

end
