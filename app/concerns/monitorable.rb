require 'heartbeat_cache'

module Monitorable
  extend ActiveSupport::Concern
  include StateMachine
  include Deployable

  included do
    validates_numericality_of :lifetime, only_integer: true

    scope :up,   -> { joins(:device_state).where(device_states: { state: ::DeviceState::States::UP }) }
    scope :down, -> { joins(:device_state).where(device_states: { state: ::DeviceState::States::DOWN }) }

    scope :good, -> { where("device_states.state = ?", DeviceState::States::UP).deployed }
    scope :alert, -> { where("device_states.state = ?", DeviceState::States::DOWN).deployed }
    scope :unreachable, -> { where("device_states.state IS NULL OR device_states.state = ?", DeviceState::States::UNREACHABLE).deployed }

    scope :active,   -> { where('devices.last_heartbeat_at >= ?', 30.days.ago.utc) }
    scope :inactive, -> { where('devices.last_heartbeat_at < ?',  30.days.ago.utc) }

    scope :monitorable, -> {
      joins(:device_state)
      .where(
        'device_states.state in (?)',
        [DeviceState::States::UP, DeviceState::States::UNREACHABLE]
      )
    }
  end

  def heartbeat_frequency
    ::DurationCalculator.calculate(default_status_freq)
  end

  def grace_period
    ::DurationCalculator.calculate(default_grace_period_duration)
  end

  def down_time_in_seconds
    Time.now.utc.to_i - last_heartbeat_at.to_i
  end

  def lifetime
    124.minutes.to_i
  end

  def lifetime_expired_at?(time)
    self.last_heartbeat_at < (time - self.lifetime)
  end

  def lifetime_expired?
    self.lifetime_expired_at?(Time.now)
  end

  def activated?
    activated_at.present?
  end

  def heartbeat
    ActiveSupport::Deprecation.warn "heartbeat is deprecated, use heartbeat! instead.", caller
    heartbeat!
  end

  def heartbeat!
    RetryableRecord.retry(device_state, attempts: 5) do
      state_machine.trigger(:heartbeat)
      touch :last_heartbeat_at
      deploy if is_platform?(:gateway) && !is_deployed?
      activate!
      save! # Ensure state changes propagate
      cache_heartbeat!
    end
  end

  def timeout
    ActiveSupport::Deprecation.warn "timeout is deprecated, use timeout! instead.", caller
    timeout!
  end

  def timeout!
    begin
      state_machine.trigger(:timeout)
    rescue ActiveRecord::StaleObjectError
    end
  end

  def unreachable
    ActiveSupport::Deprecation.warn "unreachable is deprecated, use unreachable! instead.", caller
    unreachable!
  end

  def unreachable!
    begin
      state_machine.trigger(:timeout)
    rescue ActiveRecord::StaleObjectError
    end
  end

  def recovery_failed
    ActiveSupport::Deprecation.warn "recovery_failed is deprecated, use recovery_failed! instead.", caller
    recovery_failed!
  end

  def recovery_failed!
    begin
      state_machine.trigger(:recovery_fail)
    rescue ActiveRecord::StaleObjectError
    end
  end
  alias_method :alert!, :recovery_failed!

  def down!
    begin
      state_machine.trigger(:down)
      save! # Ensure state changes propagate
    rescue ActiveRecord::StaleObjectError
    end
  end

  def activate!
    return if activated?
    touch :activated_at
    deploy
  end

  private

  def default_status_freq
    Settings.devices.default_status_freq
  end

  def default_grace_period_duration
    Settings.devices.default_grace_period_duration
  end

  def cache_heartbeat!
    ::HeartbeatCache.put(self.mac, cache_info)
  end

  def cache_info
    {
      last_heartbeat_at:    self.last_heartbeat_at,
      heartbeat_frequency:  heartbeat_frequency,
      grace_period:         grace_period
    }
  end
end
