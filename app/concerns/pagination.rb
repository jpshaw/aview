module Pagination
  extend ActiveSupport::Concern

  included do
    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :pagination_limits
    serialize :pagination_limits
  end

  # Public: Gets the default pagination limit.
  def default_pagination_limit
    50
  end

  # Internal: Custom logic for handling missing methods. It is
  # primarily used for dynamic pagination methods. If nothing
  # matches the method, we defer logic to the parent class.
  #
  # meth - The Symbol name of the missing method.
  # args -  The Array of arguments passed to the method.
  # block - A Block passed to the method (optional).
  #
  # Returns nothing.
  def method_missing(meth, *args, &block)
    if meth.to_s =~ /_pagination_limit.?\z/
      run_pagination_method(meth, *args)
    else
      super
    end
  end

  # Internal: Gets or sets a pagination limit.
  def run_pagination_method(meth, *args)
    key = meth.to_s.split("_pagination_limit").first.to_sym
    args.empty? ? pagination_limit(key) : set_pagination_limit(key, args.first)
  end

  # Public: Gets a pagination limit for a specified key.
  #
  # key - The Symbol key to get.
  #
  # Returns the pagination limit for key if found, otherwise
  # it returns the default limit.
  def pagination_limit(key)
    return default_pagination_limit if key.nil? || !self.pagination_limits.is_a?(Hash)
    self.pagination_limits[key.to_sym] || default_pagination_limit
  end

  # Public: Sets a pagination limit for a key.
  #
  # name - The String name of the pagination limit.
  # value - The Integer value of the pagination limit.
  #
  # Examples
  #
  #   user = User.first
  #   user.set_pagination_limit(:devices, 25)
  #   # => UPDATE `users` SET `pagination_limits` = '---\n:devices: 12\n', ...
  #
  def set_pagination_limit(name, value)
    return if name.nil? || value.nil?

    key   = name.to_sym
    limit = value.to_i

    # Ensure limit is within a sane range.
    limit = default_pagination_limit if limit < 1 || limit > 1000

    # Explicitly set a hash for storing limits. This alleviates issues where
    # the current value is 'null', a string, or some other format (like JSON).
    self.pagination_limits = {} unless self.pagination_limits.is_a?(Hash)

    # Set pagination limit for key
    self.pagination_limits[key] = limit
  end
end
