module ConfigurationMonitorable
  extend ActiveSupport::Concern

  def heartbeat_frequency
    configuration.try(:heartbeat_frequency) || super
  end

  def grace_period
    configuration.try(:heartbeat_grace_period) || super
  end
end
