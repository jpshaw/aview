module Deployable
  extend ActiveSupport::Concern
  include StateMachine

  included do
    scope :deployed, -> { joins(:device_state).where("devices.is_deployed = ?", true) }
    scope :undeployed, -> { joins(:device_state).where("devices.is_deployed = ?", false) }

    define_model_callbacks :deploy, :undeploy
  end

  def deployed?
    is_deployed == true
  end

  def deploy
    run_callbacks :deploy do
      update_attribute :is_deployed, true
    end
  end

  def undeploy
    run_callbacks :undeploy do
      update_attribute :is_deployed, false
    end
  end

  def toggle_deployment
    deployed? ? undeploy : deploy
  end
end
