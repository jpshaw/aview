module DeviceAssociatable
  extend ActiveSupport::Concern

  included do
    define_model_callbacks :device_association
    after_destroy :delete_device_associations
  end

  def device_associations
    ::DeviceAssociation.by_device_id(id)
  end

  def device_wan_associations
    device_associations_for_type(:wan)
  end

  def device_lan_associations
    device_associations_for_type(:lan)
  end

  def device_associations_for_type(type)
    device_associations.by_updated_at.select do |association|
      if association.is_inversed_for?(self)
        association.target_type == type.to_s
      else
        association.source_type == type.to_s
      end
    end
  end

  def device_associations_for_interface(interface)
    device_associations.by_updated_at.select do |association|
      if association.is_inversed_for?(self)
        association.target_interface == interface.to_s
      else
        association.source_interface == interface.to_s
      end
    end
  end

  def associated_with_device?(target_device)
    ::DeviceAssociation.by_source_id_and_target_id(id, target_device.id).exists?
  end

  def associated_with_mac?(target_mac)
    ::DeviceAssociation.by_source_id_and_target_mac(id, target_mac).exists?
  end

  def associate_with_device(target_device, details = {})
    associate_with(target_device.target_association_data, details)
  end

  def associate_with_mac(target_mac, details = {})
    associate_with({ target_mac: target_mac }, details)
  end

  def dissociate_from_device(target_device)
    dissociate_from(target_device.target_association_data)
  end

  def dissociate_from_mac(target_mac)
    dissociate_from(target_mac: target_mac)
  end

  def source_association_data
    { source_id: id, source_mac: mac }
  end

  def target_association_data
    { target_id: id, target_mac: mac }
  end

  # TODO: Nullify device id fields for associations to other devices in the app
  def delete_device_associations
    device_associations.delete_all
  end

  private

  def associate_with(target_data, details = {})
    association_data = build_association_data(target_data, details)

    run_callbacks :device_association do
      ::DeviceAssociationService.new(association_data).associate_devices
    end
  end

  def dissociate_from(target_data)
    association_data = build_association_data(target_data)

    ::DeviceAssociationService.new(association_data).dissociate_devices
  end

  def build_association_data(target_data, details = {})
    target_data ||= {}

    [
      details,
      target_data,
      source_association_data
    ].reduce(&:merge).with_indifferent_access
  end

end
