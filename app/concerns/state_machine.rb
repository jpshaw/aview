require 'device_state'

module StateMachine
  extend ActiveSupport::Concern

  delegate :up?, :down?, to: :device_state, allow_nil: true

  included do
    has_one :device_state, dependent: :destroy
    delegate :state_machine, to: :device_state

    before_create :build_default_device_state
  end

  def has_state?
    !self.device_state.nil?
  end

  def build_default_device_state
    build_device_state
    true # Always return true in callbacks as the normal 'continue' state
  end
end
