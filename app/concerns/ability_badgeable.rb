module AbilityBadgeable
  extend ActiveSupport::Concern

  def generate_badges(abilities)
    if abilities.any?
      abilities.map { |ability| ability_badge(ability.name) }.join(' ').html_safe
    else
      ability_badge('View Only').html_safe
    end
  end

  private

  def ability_badge(name)
    key     = "abilities.#{name.parameterize.underscore}"
    options = {
      class: 'badge',
      style: 'cursor: pointer;'
    }

    if i18n_set?(key)
      options['data-toggle']         = 'tooltip'
      options['data-placement']      = 'top'
      options['data-container']      = 'body'
      options['data-original-title'] = t(key)
    end

    content_tag(:span, options) { name }
  end
end