module Exportable
  extend ActiveSupport::Concern

  def to_csv(options = {})
    rows = as_csv
    CSV.generate(options) do |csv|
      csv << export_column_headers(rows)
      rows.each do |row|
        csv << row.values
      end
    end
  end

  def to_xlsx
    package = Axlsx::Package.new
    wb = package.workbook
    rows = as_xlsx
    wb.add_worksheet(name: 'Export') do |sheet|
      sheet.add_row export_column_headers(rows)
      rows.each do |row|
        sheet.add_row row.values, types: export_column_types
      end
    end
    package.to_stream.read
  end

  private

  def as_csv
    raise NotImplementedError, "This #{self.class} cannot respond to #as_csv"
  end

  def as_xlsx
    raise NotImplementedError, "This #{self.class} cannot respond to #as_xlsx"
  end

  def export_column_headers(rows = nil)
    @export_column_headers ||= begin
      if rows
        headers = rows.first.try(:keys)
        headers = [] unless headers.present?
        headers
      else
        raise ArugmentError, "This #{self.class} did not define rows on the first call to #export_column_headers"
      end
    end
  end

  def export_column_types
    @export_column_types ||= export_column_headers.map{ :string }
  end
end
