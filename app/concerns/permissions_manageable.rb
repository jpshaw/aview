module PermissionsManageable

  extend ActiveSupport::Concern

  included do
    has_many :manageable_permissions, as: :manageable, dependent: :destroy, class_name: "Permission"
    has_many :managing_organizations,
      through:      :manageable_permissions,
      source:       :manager,
      source_type:  Permission::MANAGER_TYPE_ORGANIZATION
    has_many :managing_users,
      through:      :manageable_permissions,
      source:       :manager,
      source_type:  Permission::MANAGER_TYPE_USER
  end

  def permission_managed_by(manager)
    manageable_permissions.find { |permission| permission.manager == manager }
  end

end
