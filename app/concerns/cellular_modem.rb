
module CellularModem
  extend ActiveSupport::Concern

  included do
    has_many :modems, class_name: 'DeviceModem', foreign_key: :device_id, dependent: :destroy

    delegate :sent_sms_recently?, to: :modem
    delegate :time_left_til_sms_allowed, to: :modem
    delegate :can_receive_sms_commands?, to: :modem, allow_nil: true
    delegate :name, to: :modem, prefix: true, allow_nil: true
    delegate :number, to: :modem, prefix: true, allow_nil: true

    def modem
      @modem ||= begin
        modems.find_by(active: true) || modems.order('updated_at asc').last
      end
    end

    def deactivate_old_modems(new_modem)
      return unless modems.count > 1
      old_modems = modems - [ new_modem ]
      old_modems.each do |modem|
        modem.update_attribute(:active, false)
      end
    end

    scope :sort_by_modem_carrier, ->(direction) {
      includes(:modems)
        .order("device_modems.carrier #{sort_direction direction}")
    }
    scope :sort_by_modem_signal, ->(direction) {
      includes(:modems)
        .order("device_modems.signal #{sort_direction direction}")
    }
    scope :sort_by_modem_cnti, ->(direction) {
      includes(:modems)
        .order("device_modems.cnti #{sort_direction direction}")
    }
    scope :sort_by_modem_number, ->(direction) {
      includes(:modems)
        .order("device_modems.number #{sort_direction direction}")
    }
    scope :sort_by_modem_iccid, ->(direction) {
      includes(:modems)
        .order("device_modems.iccid #{sort_direction direction}")
    }
  end

end
