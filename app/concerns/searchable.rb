module Searchable
  extend ActiveSupport::Concern

  class InvalidSearchException < Exception
  end

  included do
    @allowable_filters = []
    class << self
      attr_accessor :allowable_filters
    end

    has_search_scope :with_organization_id, ->(id) { where(organization_id: id) }
    has_search_scope :with_organization, ->(search_hash) {
      if search_hash[:include_hierarchy] == 'true'
        organization = ::Organization.find(search_hash[:organization_id])
        where(organization_id: organization.self_and_descendant_ids)
      else
        where(organization_id: search_hash[:organization_id])
      end
    }
    has_search_scope :sort_by_organizations_name, ->(direction) {
      joins(:organization)
        .order("organizations.name #{sort_direction direction}")
    }
    has_search_scope :sort_by_created_at, ->(direction) { order("#{table_name}.created_at #{sort_direction direction}") }
    has_search_scope :sort_by_updated_at, ->(direction) { order("#{table_name}.updated_at #{sort_direction direction}") }

  end

  module ClassMethods

    def search(search_hash = {})
      result = nil
      search_hash.each do |scope_to_run, argument|
        result = extend_result(result, scope_to_run, argument)
      end
      result
    end

    def search!(search_hash = {})
      result = nil
      search_hash.each do |scope_to_run, argument|
        raise InvalidSearchException, "Invalid filter for #{self}: #{scope_to_run}" unless self.allowable_filters.include? scope_to_run.to_sym
        result = extend_result(result, scope_to_run, argument)
      end
      result
    end

    # Convenience method for declaring scopes enabled for searching.
    #
    # Example
    #
    #   has_search_scope :hello, ->(foo) { where(name: foo) }
    #   Record.search!({hello: 'world'})
    #   #=>  SELECT `records`.* FROM `records` WHERE `records`.`name` = 'foo'
    #
    def has_search_scope(scope_name, scope_lambda)
      allowed_search_filters(scope_name)
      scope(scope_name, scope_lambda)
    end

    # Simple sorting for a single column in this table
    def has_sort_scope(column)
      scope_lambda = ->(direction) {
        order("#{sort_nulls(column)}, #{table_name}.#{column} #{sort_direction(direction)}")
      }

      scope_name = "sort_by_#{column}"

      allowed_search_filters(scope_name)
      scope(scope_name, scope_lambda)
    end

    # Adds filters to the list of allowable_filters.
    def allowed_search_filters(*filters)
      self.allowable_filters += filters.to_a.map(&:to_sym)
    end

    private

    def extend_result(result, scope_to_run, argument)
      if result.nil?
        if argument.nil?
          self.__send__ scope_to_run
        else
          self.__send__ scope_to_run, argument
        end
      else
        result.merge(self.__send__ scope_to_run, argument)
      end
    end

    def sort_direction(direction)
      direction.try(:to_sym) == :desc ? :desc : :asc
    end

    # Stick NULL fields at the end of the sort
    def sort_nulls(column)
      "case when #{table_name}.#{column} is null then 1 else 0 end"
    end
  end

end
