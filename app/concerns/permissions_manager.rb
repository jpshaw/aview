module PermissionsManager

  extend ActiveSupport::Concern

  included do
    has_many :manager_permissions, as: :manager, dependent: :destroy, class_name: "Permission"
    has_many :permission_roles, through: :manager_permissions, source: :role
    has_many :role_abilities,   through: :permission_roles
    has_many :abilities,        through: :role_abilities

    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :manager_permissions_attributes

    accepts_nested_attributes_for :manager_permissions, allow_destroy: true,
      reject_if: :all_blank
  end

  def head_manageable_organizations
    Organization.head_organizations_in(manageable_organizations)
  end

  def manageable_organizations
    manageables_of_type(Permission::MANAGEABLE_TYPE_ORGANIZATION)
  end

  # Public: Check whether this user has the named ability.
  # name - A String with the ability name.
  #
  # Examples
  #   @user.has_ability?("Modify Devices")
  #     # => true
  #
  # Returns Boolean true if the user has the named ability, false otherwise.
  def has_ability?(ability_name)
    self.abilities.exists?(name: ability_name)
  end

  # Public: Check whether this user has the named ability on a given resource.
  # resource  - A resource such as a account/device/organization/user instance.
  # ability   - A hash containing a key symbol ":to" which value is an ability name.
  #
  # Examples
  #   @user.has_ability_on?(Device.first, to: "Modify Devices")
  #     # => false
  #   @user.has_ability_on? User.first, to: "Modify Users"
  #     # => true
  #
  # Returns Boolean true if the user has the named ability on the resource, false otherwise.
  def has_ability_on?(resource, ability)
    # Using intermediate variables for readability only.
    ability       = {} unless ability.is_a?(Hash)
    resource_type = resource.class
    ability_name  = ability[:to]

    return false unless resource_type < ActiveRecord::Base

    permission = permission_for(resource)
    return false unless permission.present?

    permission.allowed_abilities.map(&:name).include?(ability_name)
  end

  # Public: Checks if the user has the ability on any of its manageable
  # organizations.
  #
  # ability - The String ability to check for.
  #
  # Examples
  #
  #   user.has_ability_for_any_organization_to?('Modify Sites')
  #   # => true # Can be used to show 'new' button
  #
  # Returns true if the user has the ability on any of its organizations;
  #   otherwise false.
  def has_ability_for_any_organization_to?(ability)
    manageables(of_type: Permission::MANAGEABLE_TYPE_ORGANIZATION,
                for_ability: ability).any?
  end

  # Public: For a manager, retrieve all resources of a given type for which a given allowed ability exists.
  # opts - A hash containing the following keys and values:
  #   :of_type      - A symbol for the type of the resource (i.e.: :account).
  #   :for_ability  - An ability name (i.e.: 'Manage Accounts').
  #
  # Examples
  #   @user.manageables(of_type: :account, for_ability: 'Manage Accounts')
  #     # => [#<Account id: 1, name: "ANRID", organization_id: 5, created_at: "2014-02-24 19:57:29", updated_at: "2014-02-24 19:57:29">]
  #
  # Returns Boolean true if the user has the named ability on the resource, false otherwise.
  def manageables(opts)
    opts          = {} unless opts.is_a?(Hash)
    resource_type = opts[:of_type].to_s.classify.constantize
    ability_name  = opts[:for_ability]

    permitted_resources(of_type: resource_type, with_ability: ability_name)
  end

  # Public: Gets a list of modifiable organizations for the manager.
  #
  # Returns an Array of Organizations the Manager has the ability to modify.
  def modifiable_organizations
    manageables(for_ability: 'Modify Organizations',
                of_type: Permission::MANAGER_TYPE_ORGANIZATION)
  end

  def permitted_resources_of_type(klass)
    opts = query_values_for_type(klass)

    klass
      .joins(opts[:join_value])
      .joins(organization_hierarchy_joins_sql)
      .joins(organization_hierarchy_joins_permissions_sql)
      .where(
        permissions: {
          manager_id:       id,
          manager_type:     self.class.to_s,
          manageable_type:  Permission::MANAGEABLE_TYPE_ORGANIZATION
        }
      )
      .uniq
  end

  # Public: Gets the nearest permission for the manager and given resource, if
  # one exists.
  #
  # Returns the nearest Permission object for the given resource, if the user
  #   has one; otherwise nil.
  def permission_for(resource)
    return unless resource.present?

    # If the resource is an owned object, look up it's organization.
    if resource.respond_to?(:organization)
      descendant_id = resource.organization.id
    else
      descendant_id = resource.id
    end

    Permission.joins('INNER JOIN organization_hierarchies ON organization_hierarchies.ancestor_id = permissions.manageable_id')
              .where(
                permissions: {
                  manager_type:    self.class.to_s,
                  manager_id:      self.id,
                  manageable_type: Permission::MANAGEABLE_TYPE_ORGANIZATION
                },
                organization_hierarchies: {
                  descendant_id: descendant_id
                })
              .order('organization_hierarchies.generations asc')
              .first
  end

  # Public: Gets the abilities a manager has for a particular resource.
  #
  # Returns an ActiveRecord::Relation of Ability objects that the manager has
  #   for the given resource.
  def abilities_for(resource)
    permission = permission_for(resource)

    return Ability.limit(0) unless permission.present? && permission.role.present?

    Ability.where(id: permission.allowed_abilities.map(&:id))
  end

  private

  def query_values_for_type(klass)
    if klass.name == 'Organization'
      {join_value: nil, group_value: "id"}
    else
      {join_value: :organization, group_value: "#{klass.table_name}.id"}
    end
  end

  def permitted_resources(opts)
    # Using intermediate variables for readability only.
    resource_type = opts[:of_type]
    ability_name  = opts[:with_ability]

    # Return an empty collection if the resource is not an ActiveRecord model.
    return [] unless resource_type < ActiveRecord::Base

    permitted_resources_of_type(resource_type)
      .joins(role_abilities_joins_sql)
      .joins(abilities_joins_sql)
      .where(
        abilities: {
          name: ability_name
        }
      )
  end

  def manageables_of_type(manageable_type)
    manager_permissions.where(manageable_type: manageable_type).map &:manageable
  end

  def organization_hierarchy_joins_sql
    'INNER JOIN organization_hierarchies ON organizations.id = organization_hierarchies.descendant_id'
  end

  def organization_hierarchy_joins_permissions_sql
    'INNER JOIN permissions ON organization_hierarchies.ancestor_id = permissions.manageable_id'
  end

  def role_abilities_joins_sql
    'INNER JOIN role_abilities ON role_abilities.role_id = permissions.role_id'
  end

  def abilities_joins_sql
    'INNER JOIN abilities ON abilities.id = role_abilities.ability_id'
  end

  def device_category_joins_sql
    'INNER JOIN device_categories ON device_categories.id = devices.category_id'
  end

end
