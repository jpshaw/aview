module OrganizationManageable

  extend ActiveSupport::Concern

  def manageable
    self.organization
  end

end
