module Exportable
  module Batchable
    include Exportable

    def export_records_in_batches(options, &block)
      results = []
      record_ids = export_records_for_batch_processing.pluck(:id)
      record_ids.each_slice(Settings.batch_size) do |subset|
        options[:subset] = subset
        results += block.call find_records_with_persisted_sort(options)
      end
      results
    end

    private

    def find_records_with_persisted_sort(params)
      klass = params[:klass].constantize
      preloads = params[:preloads]
      ids = params[:subset]
      klass.preload(preloads).find(ids).index_by(&:id).values_at(*ids)
    end

    def export_records_for_batch_processing
      raise NotImplementedError, "This #{self.class} cannot respond to #get_export_records"
    end
  end
end
