# Public: A mixin that injects the notification subscriptions accessor method.
module Subscriptions

  # Public: Gets notifiable subscriptions for an event uuid.
  #
  # uuid_id - The Integer ID of the UUID object in NotificationsProfile that holds
  #  the preference for the type of event we're interested in.
  #
  # Examples
  #
  #    notifiable_subscriptions(uuid)
  #    # => [array of subscriptions]
  #
  # Returns an array of subscriptions interested in the uuid.
  def notifiable_subscriptions(uuid_id)
    retval = []
    return retval if uuid_id.nil?

    # Grab subscriptions explicitly interested in the device.
    retval += NotificationSubscription.notifiable.for_device_and_uuid(self.id, uuid_id)

    # Grab subscriptions interested in the device's organization.
    unless self.organization.nil?
      retval += NotificationSubscription.notifiable.for_organizations_and_uuid(self.organization.ancestor_id_list, uuid_id)
    end

    retval
  end
end
