# Public: A mixin that injects the send_command method.
module Commands

  # Public: Sends a command to the device
  def send_command(opts = {})
    Platforms::CommandFactory.command(
      opts.merge(device: self)
    ).perform
  end

end
