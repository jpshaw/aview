# Contains the logic used to uniquely identify a device. We initially used
# MAC Addresses to identify devices, but have recently moved to using serial
# numbers as well.
#
# Examples
#
#   Netbridge.serial_ranges
#   # => {6200010000000000..6200019999999999=>"6200-FX"}
#
#   Netbridge.mac_ranges
#   # => {167570833408..167570898944=>"SX",
#         167570898945..167570964479=>"6200-FX",
#         167503724544..167503724545=>"6200-FX"}
#
#   Netbridge.valid_mac?('002704000123')
#   # => true
#
#   Netbridge.valid_mac?('002700000000')
#   # => false
#   Netbridge.has_mac_range 0x002700000000..0x002700000001
#   Netbridge.valid_mac?('002700000000')
#   # => true
#
#   Netbridge.model_name_for_serial('6200010000000000')
#   # => "6200-FX"
#
module DeviceIdentifiable
  extend ActiveSupport::Concern

  MAC_REGEX = /\A[0-9a-fA-F]{12}\z/

  included do
    validates :mac, presence: true, length: { is: 12 },
      format: { with: MAC_REGEX, message: :bad_mac },
      uniqueness: { case_sensitive: false }

    validate :check_mac_address
    validate :prevent_mac_change, on: :update

    validate :check_serial_number

    before_validation :default_hw_version
    before_validation :set_category_and_model
  end

  # The `hw_version` field is used to set a device's category and model. If it
  # is not set, this method will check each identity for a name, or fallback on
  # the default model for the device.
  def default_hw_version
    self.hw_version ||= self.class.model_name_for_serial(serial) if serial.present?
    self.hw_version ||= self.class.model_name_for_mac(mac)       if mac.present?
    self.hw_version ||= self.class.default_model
  end

  # A device's category and model are currently determined by the `hw_version`
  # field. If the field changes, or they are not set, this will attempt to
  # set them.
  def set_category_and_model
    if hw_version_changed? || category_id.blank? || model_id.blank?
      device_model = ::DeviceModel.lookup(object_type, hw_version)

      if device_model.present?
        self.category_id = device_model.category_id
        self.model_id    = device_model.id
      end
    end
  end

  def valid_mac?
    self.class.valid_mac?(mac)
  end

  def valid_serial?
    self.class.valid_serial?(serial)
  end

  def valid_serial_check_digit?
    self.class.valid_serial_check_digit?(serial)
  end

  def check_mac_address
    if self.mac_changed? && self.class.mac_ranges.present?
      errors.add(:mac, 'is invalid.') unless self.valid_mac?
    end
  end

  def check_serial_number
    if self.serial_changed? && self.class.serial_ranges.present?
      errors.add(:serial, 'number is invalid.')            unless self.valid_serial?
      errors.add(:serial, 'number failed ISO 7064 check.') unless self.valid_serial_check_digit?
    end
  end

  def prevent_mac_change
    if self.mac_changed?
      errors.add(:mac, 'cannot be changed for existing devices.')
    end
  end

  module ClassMethods

    # Set the device class's default model. This is used to set the `hw_version`
    # field to associate the device with a category and models.
    def set_default_model(name)
      @default_model = name
    end

    def default_model
      @default_model
    end

    def mac_ranges
      @mac_ranges ||= {}
    end

    def mac_ranges=(value)
      @mac_ranges = value
    end

    def serial_patterns
      @serial_patterns ||= {}
    end

    def serial_patterns=(value)
      @serial_patterns = value
    end

    def serial_ranges
      @serial_ranges ||= {}
    end

    def serial_ranges=(value)
      @serial_ranges = value
    end

    def has_mac_range(range, opts = {})
      mac_ranges[range] = opts[:model_name] || default_model
    end

    def has_serial_pattern(pattern, opts = {})
      serial_patterns[pattern] = opts[:model_name] || default_model
    end

    def has_serial_range(range, opts = {})
      serial_ranges[range] = opts[:model_name] || default_model
    end

    def valid_mac?(mac)
      return false unless mac.present? && mac.valid_mac?
        # valid_mac? in this case is from the MacAddress gem
        #   https://github.com/uceem/mac_address

      mac = mac.hex

      mac_ranges.keys.each do |range|
        return true if range.include?(mac)
      end

      false
    end

    def valid_serial?(serial)
      return false if serial.blank?

      serial_int = serial.to_i

      serial_ranges.keys.each do |range|
        return true if range.include?(serial_int)
      end

      serial_patterns.keys.each do |pattern|
        # TODO: replace with pattern.match?(serial) once we move to ruby-2.4+
        # https://bugs.ruby-lang.org/issues/8110
        return true if Regexp.new(pattern).match(serial).present?
      end

      false
    end

    def model_name_for_mac(mac)
      return nil if mac.blank?

      mac = mac.hex

      mac_ranges.each do |range, name|
        return name if range.include?(mac)
      end

      nil
    end

    def model_name_for_serial(serial)
      return nil if serial.blank?

      serial = serial.to_i

      serial_ranges.each do |range, name|
        return name if range.include?(serial)
      end

      nil
    end

    # Checks if the given device identities fall within the ranges set for the
    # device class. If there are ranges defined for the identifiers then they
    # must all fall within each identity range to be condidered a match.
    #
    # Examples
    #
    #   Gateway.valid_identifiers?(mac: '002700000000')
    #   # => false
    #
    #   Gateway.valid_identifiers?(mac: '002700000000', serial: '8300010000000000')
    #   # => false
    #
    #   Gateway.valid_identifiers?(mac: '00D0CF000000', serial: '8300010000000000')
    #   # => true
    #
    # Returns true if all of the device identities match the ranges set for the
    # device class, otherwise false.
    def valid_identifiers?(attributes = {})
      match_results = []

      if serial_ranges.present? && attributes[:serial].present?
        match_results << valid_serial?(attributes[:serial])
      end

      if mac_ranges.present? && attributes[:mac].present?
        match_results << valid_mac?(attributes[:mac])
      end

      # All match results must be true in order to be valid.
      match_results.empty? ? false : match_results.all?
    end

    # Public: Perform a mod97,10 to validate numeric serials.
    #
    # Examples
    #   Device.valid_serial_check_digit?(1)
    #   => true
    #   Device.valid_serial_check_digit?(98)
    #   => true
    #   Device.valid_serial_check_digit?(97)
    #   => false
    #   Device.valid_serial_check_digit?(5300030032091435)
    #   => true
    #   Device.valid_serial_check_digit?(5300010032091435)
    #   => false
    #   Device.valid_serial_check_digit?('WR14-123456')
    #   => true
    #
    # Returns true if the given number is a valid serial, otherwise false.
    def valid_serial_check_digit?(number)
      number = number.to_i unless number.is_a? Integer
      (number % 97) == 1 || number == 0
    end
  end
end
