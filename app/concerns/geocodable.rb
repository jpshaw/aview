module Geocodable
  extend ActiveSupport::Concern

  included do
    belongs_to :location

    delegate :city,      to: :location, prefix: true, allow_nil: true
    delegate :ctry_code, to: :location, prefix: true, allow_nil: true
    delegate :latitude,  to: :location, prefix: true, allow_nil: true
    delegate :longitude, to: :location, prefix: true, allow_nil: true
  end

  def has_location?
    location.present? and location.is_verified?
  end

  # Returns a hash of map pin data.
  def to_map_pin_hash
    {
      title:              "#{category_name} #{series_name}",
      marker:             status_pin,
      latitude:           location_latitude,
      longitude:          location_longitude,
      device_mac:         mac,
      site_id:            site_id,
      site_name:          site_name,
      organization_slug:  organization_slug,
      organization_name:  organization_name
    }
  end

  # Returns a hash of map pin data for a device's cellular location.
  def cellular_map_pin_hash
    {
      title:              "Cellular Location",
      marker:             ActionController::Base.helpers.asset_path("markers/orange_marker.png"),
      latitude:           cellular_location.lat,
      longitude:          cellular_location.lon,
      device_mac:         mac,
      site_id:            site_id,
      site_name:          site_name,
      organization_slug:  organization_slug,
      organization_name:  organization_name
    }
  end

  alias_method :cellular_map_pin, :cellular_map_pin_hash

  alias_method :to_map_pin, :to_map_pin_hash
end
