module DeviceActionable
  extend ActiveSupport::Concern

  def data_plans_for_select
    DataPlan.for_organization(current_organization).map { |plan| [plan.name, plan.id] }
  end

  def show_organization_selection?
    true
  end

  def usable_organizations
    @usable_organizations ||= current_user.organizations
  end

  def current_organization
    @organization ||= current_user.filter_organization || current_user.organization
  end

  def show_move_to_data_plan_action?
    true
  end
end
