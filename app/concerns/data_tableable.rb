module DataTableable
  extend ActiveSupport::Concern

  def column_data(column)
    result = {}
    result[:direction] = column[:direction] if column[:direction].present? # Sorting Direction
    result[:name] = column[:name] # Used by DataTable to identify column
    result[:sort_scope] = column[:sort_scope] # Used by DataTable to send sorting scope of column
    result
  end
end