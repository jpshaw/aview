# Subscribe to events and notify subscribed users
class DeviceEventNotificationConsumer < ApplicationConsumer

  topic 'db.devices.events'

  def consume
    logger.info 'Starting consumer ...'

    each_message do |message|
      process(message)
    end

    logger.info 'Stopping consumer ...'
  end

  private

  def process(event)
    return unless valid?(event)

    device_id = event.data.device_id
    uuid_id   = event.data.uuid_id
    device    = Device.deployed.find(device_id)

    device.notifiable_subscriptions(uuid_id).each do |subscription|
      notify(device_id, uuid_id, subscription, event)
    end

  rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotFound => error
    logger.error error
  end

  def notify(device_id, uuid_id, subscription, event)
    notification = {
      device_id: device_id,
      uuid_id: uuid_id,
      user_id: subscription.user_id,
      severity: event.data.severity,
      event_type: event.data.event_type,
      message: event.data.information,
      data: event.data.raw_data.to_h
    }

    subscription.notifications.create!(notification)
  end

  def valid?(event)
    event.present? &&
      event.data.present? &&
      event.data.device_id.present? &&
      event.data.uuid_id.present?
  end

end
