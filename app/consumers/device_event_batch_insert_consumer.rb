# Subscribe to events topic and batch insert to SQL database.
class DeviceEventBatchInsertConsumer < ApplicationConsumer

  topic 'db.devices.events'

  attr_reader :batch_size

  def initialize(batch_size: 1_000)
    @batch_size = batch_size
  end

  def consume
    logger.info 'Starting consumer ...'

    each_batch do |batch|
      batch.messages.each_slice(batch_size).to_a.each do |batched_messages|
        events = []

        batched_messages.each do |message|
          hash = JSON.parse(message.value)
          data = hash.fetch('data', {})
          events << DeviceEvent.new(data) if valid?(data)
        end

        if events.present?
          logger.info "Inserting #{events.length} events"
          DeviceEvent.import!(events)
        end
      end
    end

    logger.info 'Stopping consumer ...'
  end

  private

  def valid?(data)
    data.present? &&
      data['device_id'].present? &&
      data['severity'].present? &&
      data['information'].present? &&
      data['event_type'].present?
  end

end
