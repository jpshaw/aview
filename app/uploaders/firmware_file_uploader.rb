# encoding: utf-8

class FirmwareFileUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  after  :store,   :create_symlinks
  before :remove,  :remove_symlinks

  # Choose what kind of storage to use for this uploader:
  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    # "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    "device_firmware/AcceleratedConcepts/#{model.device_model.name}"
  end


  private

  def create_symlinks(file)
    File.symlink(file_location, symlink)
    File.symlink(file_location, "#{symlink}.tar")
  end


  def remove_symlinks
    File.delete(symlink)
    File.delete("#{symlink}.tar")
  end


  def symlink
    "#{File.dirname(file_location)}/#{model.version}"
  end

  def file_location
    # In production, we store files in a shared directory that gets preserved
    # across application deployments (see the lib/task/deploy.rake file).
    # Carrierwave does not recognized this, so we need to specify the correct
    # linked directory path if it exists.
    if File.directory?(Settings.firmwares_api.base_directory)
      "#{Settings.firmwares_api.base_directory}#{self.url}"
    else
      self.path
    end
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
