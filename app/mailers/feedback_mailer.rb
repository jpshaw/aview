class FeedbackMailer < ApplicationMailer
  layout 'plain_mailer'

  def new_feedback(user, last_url, category, domain, message, recipient)
    @user_name = user.username.blank? ? user.email : user.username
    @last_url = last_url
    @category = category
    @domain = domain
    @message = message
    @recipient = recipient

    subject = "#{user.full_name} Submitted New Feedback"
    
    begin
      mail(to: @recipient, subject: subject)
    rescue => e
      logger.error "Failed to send feedback email to #{@recipient} - Error: #{e.message}"
    end
  end
end
