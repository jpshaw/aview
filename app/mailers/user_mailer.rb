class UserMailer < ActionMailer::Base
  helper TimezoneHelper

  default from: Settings.email_addresses.alerts

  # Public: Sends an email with a single notification to a user.
  #
  # notification - The Notification object to send to the user.
  #
  # Returns a mailer object for a single notification email.
  def subscription_email(notification)
    begin
      @subscription = notification.notification_subscription
      @event        = notification.event
      @user         = @subscription.user
      @service_name = @event.device.service_name

      subject_parts = []
      subject_parts << "#{site_name} Notification"
      subject_parts << "#{@event.device.site.name}" unless @event.device.site.nil?
      subject_parts << "#{@event.device.organization.name}" unless @event.device.organization.nil?
      subject_parts << "#{@event.info}"

      mail(:to => @user.email, :subject => subject_parts.join(" - "))
    rescue => e
      logger.error "Failed to send email for notification: #{notification} - Error: #{e.message}"
    end
  end

  # Public: Sends a bulk notification email to a user.
  #
  # subscription - The NotificationSubscription object for the notifications.
  # notifications - The Notification objects to send to the user.
  #
  # Returns a mailer object for a bulk notification email.
  def bulk_subscription_email(subscription, notifications)
    begin
      @subscription        = subscription
      @notifications       = notifications
      unique_device_count  = Set.new(@notifications.map { |notif| notif.event.device_id}).length
      notif_timestamp      = Time.now.utc.strftime("%Y-%m-%d %H:%M:%S (%Z)")
      @user                = @subscription.user

      subject_parts = []
      subject_parts << "#{site_name} Notification"
      subject_parts << "#{@subscription.publisher_name} Bulk Notification For #{unique_device_count} Device(s)"
      subject_parts << "#{notif_timestamp}"

      mail(:to => @user.email, :subject => subject_parts.join(" - "))
    rescue => e
      logger.error "Failed to send email for subscription: #{subscription} - Error: #{e.message}"
    end
  end

  def data_plan_over_limit(user, data_plan, plan_usage)
    begin
      @data_plan = data_plan
      @current_usage = plan_usage
      attachments["usage_report.csv"] = DataPlanReportService.new(@data_plan).execute
      mail(to: user.email, subject: "#{site_name} Notification - Data Plan Overage Notification For #{@data_plan.name}", attachments: attachments)
    rescue => e
      logger.error "Failed to send overage email for data_plan: #{@data_plan} - Error: #{e.message}"
    end
  end

  protected

  # Internal: Prints the site name.
  def site_name
    Settings.site_name.nil? ? "Accelerated View" : Settings.site_name
  end
end
