class AdminMailer < ApplicationMailer
  layout 'inspinia_mail'
  default to: Settings.email_addresses.aview

  def new_carrier(carrier)
    begin
      @carrier = carrier
      mail(subject: "#{site_name} Notification - New Carrier (#{carrier.name}) Created")
    rescue => e
      logger.error "Failed to send notification for #{@carrier.name} - Error: #{e.message}"
    end
  end
end
