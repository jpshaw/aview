require 'socket'

class ImportMailer < ApplicationMailer
  layout 'inspinia_mail'
  default to: Settings.email_addresses.imports

  def long_runner(import)
    begin
      @import = import
      @alert_time = humanize(Settings.imports.long_running_time)
      @host = Socket.gethostname
      mail(subject: "#{site_name} Notification - Import exceeded the alert time")
    rescue => e
      logger.error "Failed to send notification for import #{@import.id} - Error: #{e.message}"
    end
  end

  def multiple_imports_running(import)
    begin
      @import = import
      @host = Socket.gethostname
      mail(subject: "#{site_name} Notification - Multiple imports running. One may be stuck")
    rescue => e
      logger.error "Failed to send notification for import #{@import.id} - Error: #{e.message}"
    end
  end

  private

  def humanize(seconds)
    [[60, :seconds], [60, :minutes], [24, :hours], [1000, :days]].map{ |count, name|
      if seconds > 0
        seconds, n = seconds.divmod(count)
        "#{n.to_i} #{name}" if n > 0
      end
    }.compact.reverse.join(' ')
  end
end
