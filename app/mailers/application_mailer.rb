class ApplicationMailer < ActionMailer::Base
  default from: Settings.email_addresses.alerts
  layout 'mailer'

  protected

  # Internal: Prints the site name.
  def site_name
    Settings.site_name.nil? ? "Accelerated View" : Settings.site_name
  end
end
