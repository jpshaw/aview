class RegistrationMailer < ApplicationMailer
  layout 'inspinia_mail'

  def welcome_email(user, token)
    @user  = user
    @token = token
    mail(to: @user.email, subject: '[Accelerated View] Welcome to Accelerated View')
  end
end