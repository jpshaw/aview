var IndividualDeviceConfiguration = function (configurationSelect2Options, currentConfigurationId, parentConfiguration) {
  var configurationSelection = $('#device-configuration-selection').select2(configurationSelect2Options);
  var firmwareGroup = $('#firmware-group');
  var gracePeriodGroup = $('#grace-period-duration-group');
  var settingsWarningGroup = $('#settings-warning-group');
  var settingsGroup = $('#settings-group');
  var groupConfigurationLinkContainer = $('#group-configuration-link-container');

  var deviceConfigurationSettings = $('#device_configuration_description');
  var firmwareSelection = $('#device_configuration_device_firmware_id');
  var gracePeriodDurationInput = $('#grace-period-duration-text');
  var form = $('#device-configuration-form');
  var firmwareOverrideButton = $('#firmware-override');
  var firmwareInheritButton = $('#firmware-inherit');
  var gracePeriodDurationOverrideButton = $('#grace-period-duration-override');
  var gracePeriodDurationInheritButton = $('#grace-period-duration-inherit');

  IndividualDeviceConfiguration.parentFirmwareId = parentConfiguration.firmwareId;
  IndividualDeviceConfiguration.parentFirmwareVersion = parentConfiguration.firmwareVersion;
  IndividualDeviceConfiguration.parentGracePeriod = parentConfiguration.gracePeriodDuration;
  IndividualDeviceConfiguration.parentSettings = parentConfiguration.compiledSettings;

  configurationSelection.on("change", function () {
    var id = configurationSelection.val();
    if (id == '') {
      showConfiguration(currentConfigurationId);
    } else if (id == '-1') {
      hideConfigurationOptions();
    } else {
      showConfiguration(id);
    }
  });

  function showConfiguration(id) {
    showConfigurationOptions();
    getConfiguration(id);
  }

  function getConfiguration(id) {
    $.get('/device_configurations/' + id, function (data, status) {}, 'script');
  }

  function hideConfigurationOptions() {
    firmwareGroup.hide();
    gracePeriodGroup.hide();
    settingsWarningGroup.hide();
    settingsGroup.hide();
    groupConfigurationLinkContainer.hide();
  }

  function showConfigurationOptions() {
    firmwareGroup.show();
    gracePeriodGroup.show();
    settingsWarningGroup.show();
    settingsGroup.show();
    groupConfigurationLinkContainer.show();
  }

  this.hideConfigurationOptions = function() {
    hideConfigurationOptions();
  };

  this.firmwareOverrideOnClick = function(alert) {
    firmwareOverrideButton.on('click', function(event) {
      event.preventDefault();
      $(this).hide();
      firmwareInheritButton.show().css('display', 'block');
      firmwareSelection.prop('disabled', false);
      firmwareSelection.trigger("change");
      form.find('.alert').remove();
      form.prepend(alert);
    });
  };

  this.firmwareInheritOnClick = function(alert) {
    firmwareInheritButton.on('click', function(event) {
      event.preventDefault();
      form.find('.alert').remove();
      if(groupFirmwareIsSelectable()) {
        inheritFirmware();
      } else {
        form.prepend(alert);
      }
    });
  };

  this.gracePeriodDurationOverrideOnClick = function() {
    gracePeriodDurationOverrideButton.on('click', function(event) {
      event.preventDefault();
      $(this).hide();
      gracePeriodDurationInheritButton.show().css('display', 'block');
      gracePeriodDurationInput.prop('disabled', false);
    });
  };

  this.gracePeriodDurationInheritOnClick = function() {
    gracePeriodDurationInheritButton.on('click', function(event) {
      event.preventDefault();
      inheritGracePeriodDuration();
    });
  };

  $('#inherit-all-group-configuration').on('click', function(event) {
    event.preventDefault();
    clearChildSettings();
    if(!groupFirmwareIsSelectable()) {
      addGroupFirmwareSelection();
    }
    inheritFirmware(IndividualDeviceConfiguration.parentFirmwareId);
    inheritGracePeriodDuration();
  });

  function clearChildSettings() {
    deviceConfigurationSettings.val('');
  }

  function inheritFirmware() {
    firmwareInheritButton.hide();
    firmwareOverrideButton.show().css('display', 'block');
    firmwareSelection.prop('disabled', true);
    firmwareSelection.val(IndividualDeviceConfiguration.parentFirmwareId).trigger("change");
  }

  function groupFirmwareIsSelectable() {
    var parentFirmwareValue = 0;
    firmwareSelection.find('option').each(function(){
      if ($(this).text() == IndividualDeviceConfiguration.parentFirmwareVersion) {
        parentFirmwareValue = $(this).val();
        return false;
      }
    });
    return parentFirmwareValue > 0;
  }

  function addGroupFirmwareSelection() {
    var option = new Option(IndividualDeviceConfiguration.parentFirmwareVersion,
      IndividualDeviceConfiguration.parentFirmwareId);
    firmwareSelection.append(option);
  }

  function inheritGracePeriodDuration() {
    gracePeriodDurationInheritButton.hide();
    gracePeriodDurationOverrideButton.show().css('display', 'block');
    gracePeriodDurationInput.prop('disabled', true);
    gracePeriodDurationInput.val(IndividualDeviceConfiguration.parentGracePeriod);
  }
};