$(document).ready(function() {
  var table = $('table.dataTable');

  if ( table.length > 0 ) {
    var columns = table.find('thead th');
    initDataTable(columns, table);
  }
});

function getColumnSettings(columns) {
  var columnSettings = [];

  for (var i = 0; i < columns.length; i++) {
    var column = {};
    column.data = $(columns[i]).data('name');
    column.name = $(columns[i]).data('name');
    column.searchable = isColumnSearchable(columns[i]);
    column.sortable = isColumnSortable(columns[i]);
    column.visible = isColumnInitiallyVisible(columns[i]);

    if ( $(columns[i]).data('width') ) {
      column.width = $(columns[i]).data('width')
    }

    if ( $(columns[i]).hasClass('text-center') ) {
      column.className = 'text-center';
    }

    columnSettings.push(column);
  }

  return columnSettings;
}

function getSorting(columns, table) {
  var sortColumn = getSortColumn(columns, table);
  return [[sortColumn.index, sortColumn.direction]];
}

function getSortColumn(columns, table) {
  var primarySortColumns = table.find('.sortable-primary');
  if ( primarySortColumns.length > 0 ) {
    return getPrimarySortColumn(columns, primarySortColumns.first());
  }
  return getFirstSortableColumn(columns, table);
}

function getPrimarySortColumn(columns, primarySortColumn) {
  var sortColumn = {};
  sortColumn.index = columns.index(primarySortColumn);
  sortColumn.direction = primarySortColumn.data('direction');
  return sortColumn;
}

function getFirstSortableColumn(columns, table) {
  var sortColumn = {};
  var column = table.find('.sortable').first();
  sortColumn.index = columns.index(column);
  if ( column.data('direction') ) {
    sortColumn.direction = column.data('direction');
  } else {
    sortColumn.direction = 'desc';
  }
  return sortColumn;
}

function formatData(columns, data, table) {
  data.klass = table.data('klass'); // Used by the DataTable model to retrieve the correct records

  for ( var i = 0; i < data.order.length; i++ ) {
    var order = data.order[i];
    order.sortScope = $(columns[order.column]).data('sort-scope');
  }
}

function isColumnSearchable(column) {
  return $(column).hasClass('searchable');
}

function isColumnSortable(column) {
  return $(column).hasClass('sortable');
}

function isColumnInitiallyVisible(column) {
  return !$(column).hasClass('init-hidden');
}
