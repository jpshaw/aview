Highcharts.setOptions({
  chart: {
    style: {
      color: '#676a6c',
      fontFamily: "'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif"
    }
  },
  loading: {
    labelStyle: {
      top: "35%"
    }
  },
  lang: {
    noData: 'No data to display for this range.',
    thousandsSep: ','
  },
  xAxis: {
    dateTimeLabelFormats: {
      millisecond: '%H:%M:%S.%L',
      second: '%H:%M:%S',
      minute: '%H:%M',
      hour: '%H:%M',
      day: '%b %d',
      week: '%b %d',
      month: '%b \'%y',
      year: '%Y'
    }
  },
  tooltip: {
    dateTimeLabelFormats: {
      millisecond:"%A, %b %e, %H:%M:%S.%L (UTC)",
      second:"%A, %b %e, %H:%M:%S (UTC)",
      minute:"%A, %b %e, %H:%M (UTC)",
      hour:"%A, %b %e, %H:%M (UTC)",
      day:"%A, %b %e, %Y (UTC)",
      week:"Week from %A, %b %e, %Y (UTC)",
      month:"%B %Y (UTC)",
      year:"%Y (UTC)"
    }
  }
});
