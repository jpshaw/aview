/*
Description:
  This JS dictates the behavior of the visual-search box and tries to make
  as few assumptions about the implementation as possible.

Input/Assumptions:
  This require a DOM element with a class of '.vs-target-wrapper'. This DOM
  element should have a data-facets attribute that defines the facets for that page
  for VisualSearch.

  Additionally, a DOM element (probably an empty div) should have a class of
  '.visual_search' which is where the actual search box will be rendered.

Output:
  When the user completes a facet, a custom event is triggered: 'vs:search' with a
  payload containing the facets objects in JSON.
*/

$(document).ready(function() {
  var facetModels = $('.vs-target-wrapper').data('facets');
  var currentFacets = [];

  window.visualSearch = VS.init({
    container: $('.visual-search'),
    callbacks: {
      search: function() { triggerSearchEvent(); },
      facetMatches: function(callback) { callback(getFacetMatches()); },
      valueMatches: function(facet, searchTerm, callback) { callback(getFacetValues(facet)); }
    }
  });

  // Custom event payload on user facet completion.
  function getFacets() {
    var queryFacets = visualSearch.searchQuery.facets(); // The user's query terms
    currentFacets = []; // The return hash of facets matching our scopes

    for ( var i = 0; i < queryFacets.length; i++) {
      var queryFacetMatch = Object.keys(queryFacets[i])[0]; // Get the facet key (i.e. category)
      var facet = {}; // Object to hold the return data

      facet.name = queryFacetMatch; //TODO: Refactor facet.match
      facet.scope = facetModels[queryFacetMatch].scope; // Lookup scope on html element (i.e. with_category)
      facet.value = queryFacets[i][queryFacetMatch]; // Use the key to get the value (i.e. Cellular)
      currentFacets.push(facet);
    }

    return currentFacets;
  }

  // Returns an array of the values that should populate a facet after it is
  // selected. Trims values that have already been selected.
  function getFacetValues(facetMatch) {
    var facetValues = Object.keys(facetModels[facetMatch].facet_values || {});

    // Overwrite facetValues to only include values not currently in the search.
    for ( var i = 0; i < currentFacets.length; i++ ) {
      facetValues = $.grep(facetValues, function(v) {
        return v !== currentFacets[i].value;
      });
    }

    return facetValues;
  }

  // Returns an array of the facets that should be displayed when the search
  // box is clicked. Trims facetMatches that are marked as {select_type: :single}
  // if that facetMatch is already in the query.
  function getFacetMatches() {
    var facetMatches = Object.keys(facetModels);

    // Overwrite facetMatches to only include matches not currently in the search.
    for ( var i = 0; i < currentFacets.length; i++ ) {
      var targetMatch = currentFacets[i].name;
      if ( facetModels[targetMatch].select_type === 'single' ){
        facetMatches = $.grep(facetMatches, function(v) {
          return v !== targetMatch;
        });
      }
    }

    return facetMatches;
  }

  // Trigger the 'vs:search' event on the document whenever a facet is completed.
  function triggerSearchEvent() {
    $(document).trigger('vs:search', [{facets: getFacets()}]);
  }
}); 
