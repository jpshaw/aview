$(document).ready(function () {
  initFilterDropdown();
  initSelect2();
});

function initFilterDropdown() {
  var dropdown = $('#dropdown-filters');

  listenForUnselecting(dropdown);
  preventClickClosingDropdown(dropdown);
  setSelectValue();
}

function initSelect2() {
  $('.select2').select2({
    theme: 'bootstrap',
    width: '100%'
  });

  $('.select2-without-search').livequery(function() {
    $(this).select2({
      theme: 'bootstrap',
      width: '100%',
      minimumResultsForSearch: -1
    });
  });

  $('.multiselect-dropdown').select2({
    placeholder: 'All',
    allowClear: true,
    closeOnSelect: false,
    theme: 'bootstrap',
    width: '100%'
  });
}

function resetSelect2WithoutSearchOptions(element, newOptions) {
  element.find('option').remove();
  element.select2({
    theme: 'bootstrap',
    width: '100%',
    minimumResultsForSearch: -1,
    data: newOptions
  }).trigger("change");
}

function resetSelect2Options(element, newOptions, initialValue) {
  element.find('option').remove();
  element.select2({
    theme: 'bootstrap',
    width: '100%',
    data: newOptions
  }).val(initialValue).trigger("change");
}

function listenForUnselecting(dropdown) {
  dropdown.find('.select2').on('select2:unselecting', function() {
    $('#exclude_hierarchy').attr('checked', false);
  });
}

function preventClickClosingDropdown(dropdown) {
  dropdown.find('.dropdown-menu .clickable').on('click', function(e) { e.stopPropagation(); });
  dropdown.find('.select2').on('select2:open', function () {
    $('.select2-dropdown').on('click', function(e) { e.stopPropagation(); });
  });
}

function setSelectValue() {
  var select_input = $('#dropdown-filters select#organization_id');
  select_input.val(select_input.data('value')).trigger('change');
}
