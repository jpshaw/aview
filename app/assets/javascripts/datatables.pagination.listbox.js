/**
 * This pagination plug-in provides a `dt-tag select` menu with the list of the page
 * numbers that are available for viewing.
 *
 *  @name Select list
 *  @summary Show a `dt-tag select` list of pages the user can pick from.
 *  @author _jneilliii_
 *
 *  @example
 *    $(document).ready(function() {
 *        $('#example').dataTable( {
 *            "sPaginationType": "listbox"
 *        } );
 *    } );
 */

(function ($) {

	var firstClassName = 'first';
	var previousClassName = 'previous';
	var nextClassName = 'next';
	var lastClassName = 'last';

	var paginateClassName = 'paginate';
	var paginateOfClassName = 'paginate_of';
	var paginatePageClassName = 'paginate_page';
	var paginateInputClassName = 'paginate_select';

	function calcPages(oSettings) {
		return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
	}

	function calcCurrentPage(oSettings) {
		return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	}

	function calcDisableClasses(oSettings) {
		var start = oSettings._iDisplayStart;
		var length = oSettings._iDisplayLength;
		var visibleRecords = oSettings.fnRecordsDisplay();
		var all = length === -1;

		// Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
		var page = all ? 0 : Math.ceil(start / length);
		var pages = all ? 1 : Math.ceil(visibleRecords / length);

		var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
		var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

		return {
			'first': disableFirstPrevClass,
			'previous': disableFirstPrevClass,
			'next': disableNextLastClass,
			'last': disableNextLastClass
		};
	}

	$.fn.dataTableExt.oPagination.listbox = {

		/*
		 * Function: oPagination.listbox.fnInit
		 * Purpose:  Initalise dom elements required for pagination with listbox input
		 * Returns:  -
		 * Inputs:   object:oSettings - dataTables settings object
		 *             node:nPaging - the DIV which contains this pagination control
		 *             function:fnCallbackDraw - draw function which must be called on update
		 */
		"fnInit": function (oSettings, nPaging, fnCallbackDraw) {

			var nFirst = document.createElement('span');
			var nPrevious = document.createElement('span');
			var nNext = document.createElement('span');
			var nLast = document.createElement('span');
			var nInput = document.createElement('select');
			var nPage = document.createElement('span');
			var nOf = document.createElement('span');

			var language = oSettings.oLanguage.oPaginate;
			var classes = oSettings.oClasses;

			nFirst.innerHTML =  language.sFirst;
			nPrevious.innerHTML = language.sPrevious;
			nNext.innerHTML = language.sNext;
			nLast.innerHTML = language.sLast;

			nFirst.className = firstClassName + ' ' + classes.sPageButton;
			nPrevious.className = previousClassName + ' ' + classes.sPageButton;
			nNext.className = nextClassName + ' ' + classes.sPageButton;
			nLast.className = lastClassName + ' ' + classes.sPageButton;

			nOf.className = paginateOfClassName;
			nPage.className = paginatePageClassName;
			nInput.className = paginateInputClassName + ' ' + 'input-sm';

			// nOf.className = "paginate_of";
			// nPage.className = "paginate_page";

			if (oSettings.sTableId !== '') {
				nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
				nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
				nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
				nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
				nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
			}

			nInput.style.display = "inline";
			nPage.innerHTML = "Page ";

			nPaging.appendChild(nFirst);
			nPaging.appendChild(nPrevious);
			nPaging.appendChild(nPage);
			nPaging.appendChild(nInput);
			nPaging.appendChild(nOf);
			nPaging.appendChild(nNext);
			nPaging.appendChild(nLast);

			$(nFirst).click(function() {
				var iCurrentPage = calcCurrentPage(oSettings);
				if (iCurrentPage !== 1) {
					oSettings.oApi._fnPageChange(oSettings, 'first');
					fnCallbackDraw(oSettings);
				}
			});

			$(nPrevious).click(function() {
				var iCurrentPage = calcCurrentPage(oSettings);
				if (iCurrentPage !== 1) {
					oSettings.oApi._fnPageChange(oSettings, 'previous');
					fnCallbackDraw(oSettings);
				}
			});

			$(nNext).click(function() {
				var iCurrentPage = calcCurrentPage(oSettings);
				if (iCurrentPage !== calcPages(oSettings)) {
					oSettings.oApi._fnPageChange(oSettings, 'next');
					fnCallbackDraw(oSettings);
				}
			});

			$(nLast).click(function() {
				var iCurrentPage = calcCurrentPage(oSettings);
				if (iCurrentPage !== calcPages(oSettings)) {
					oSettings.oApi._fnPageChange(oSettings, 'last');
					fnCallbackDraw(oSettings);
				}
			});

			$(nInput).change(function (e) { // Set DataTables page property and redraw the grid on listbox change event.
				window.scroll(0,0); //scroll to top of page
				if (this.value === "" || this.value.match(/[^0-9]/)) { /* Nothing entered or non-numeric character */
					return;
				}
				var iNewStart = oSettings._iDisplayLength * (this.value - 1);
				if (iNewStart > oSettings.fnRecordsDisplay()) { /* Display overrun */
					oSettings._iDisplayStart = (Math.ceil((oSettings.fnRecordsDisplay() - 1) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
					fnCallbackDraw(oSettings);
					return;
				}
				oSettings._iDisplayStart = iNewStart;
				fnCallbackDraw(oSettings);
			});

			// Take the brutal approach to cancelling text selection.
			$('span', nPaging).bind('mousedown', function () { return false; });
			$('span', nPaging).bind('selectstart', function() { return false; });

			// If we can't page anyway, might as well not show it.
			var iPages = calcPages(oSettings);
			if (iPages <= 1) {
				$(nPaging).hide();
			}
		},

		/*
		 * Function: oPagination.listbox.fnUpdate
		 * Purpose:  Update the listbox element
		 * Returns:  -
		 * Inputs:   object:oSettings - dataTables settings object
		 *             function:fnCallbackDraw - draw function which must be called on update
		 */
		"fnUpdate": function (oSettings, fnCallbackDraw) {
			if (!oSettings.aanFeatures.p) {
				return;
			}

			var iPages = calcPages(oSettings); //Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength);
			var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1; /* Loop over each instance of the pager */
			var an = oSettings.aanFeatures.p;

			// if (iPages <= 1) // hide paging when we can't page
			// {
			// 	$(an).hide();
			// 	return;
			// }

			var disableClasses = calcDisableClasses(oSettings);

			$(an).show();

			// Enable/Disable `first` button.
			$(an).children('.' + firstClassName)
				.removeClass(oSettings.oClasses.sPageButtonDisabled)
				.addClass(disableClasses[firstClassName]);

			// Enable/Disable `prev` button.
			$(an).children('.' + previousClassName)
				.removeClass(oSettings.oClasses.sPageButtonDisabled)
				.addClass(disableClasses[previousClassName]);

			// Enable/Disable `next` button.
			$(an).children('.' + nextClassName)
				.removeClass(oSettings.oClasses.sPageButtonDisabled)
				.addClass(disableClasses[nextClassName]);

			// Enable/Disable `last` button.
			$(an).children('.' + lastClassName)
				.removeClass(oSettings.oClasses.sPageButtonDisabled)
				.addClass(disableClasses[lastClassName]);

			for (var i = 0, iLen = an.length; i < iLen; i++) {
				var spans = an[i].getElementsByTagName('span');
				var inputs = an[i].getElementsByTagName('select');
				var elSel = inputs[0];
				if(elSel.options.length != iPages) {
					elSel.options.length = 0; //clear the listbox contents
					for (var j = 0; j < iPages; j++) { //add the pages
						var oOption = document.createElement('option');
						oOption.text = j + 1;
						oOption.value = j + 1;
						try {
							elSel.add(oOption, null); // standards compliant; doesn't work in IE
						} catch (ex) {
							elSel.add(oOption); // IE only
						}
					}
					spans[3].innerHTML = "&nbsp;of&nbsp;" + iPages;
				}
			  elSel.value = iCurrentPage;
			}
		}
	};
})(jQuery);
