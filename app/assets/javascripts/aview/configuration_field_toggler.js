function disableField(inputField) {
  if(inputField.get(0).tagName == "INPUT") {
    inputField.val("");
    inputField.prop("disabled", true);
  } else {
    inputField.select2("val", "");
    inputField.select2("enable", false);
  }
}

function enableField(inputField) {
  if(inputField.get(0).tagName == "INPUT") {
    inputField.prop("disabled", false);
  } else {
    inputField.select2("enable", true);
  }
}

function toggleField(clickedCheckbox) {
  var fieldNameBase = clickedCheckbox.attr("id").replace(/^configuration_/,'').replace(/_locked$/,'');
  var inputField = $("#configuration_" + fieldNameBase);
  var inherited_block = $("#inherited_" + fieldNameBase);
  if (clickedCheckbox.is(':checked')) {
    disableField(inputField);
    inherited_block.removeClass("de-emphasize");
  } else {
    enableField(inputField);
    inherited_block.addClass("de-emphasize");
  }
}

$(document).ready(function(){
  $("#configuration_form input:checkbox").each(function () {
    toggleField($(this));
  });

  $("#configuration_form input:checkbox").click(function () {
    toggleField($(this));
  });
});
