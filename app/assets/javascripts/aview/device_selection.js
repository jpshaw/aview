$(document).ready(function() {
  $("#model-multiselect").on("select2:select", function() { addCategory(); });
  $("#category-multiselect").on("select2:unselect", function() { removeModel(); });

  function addCategory() {
    var selectedCategories = getSelectedCategories();
    selectedModelOptions().each(function(){
      var group = $(this).parent().attr("label");
      selectedCategories.push(getCategoryId(group));
    });
    $("#category-multiselect").val(selectedCategories).trigger("change");
  }

  function getSelectedCategories() {
    var selectedCategories = $("#category-multiselect").val();
    if( selectedCategories == null ){ selectedCategories = []; }
    return selectedCategories;
  }

  function getCategoryId(group) {
    var id = null;
    var categories = $("#category-multiselect > option");
    categories.each(function() {
      if( group === $(this).text() ){ return id = $(this).attr('value'); }
    });
    return id;
  }

  function removeModel() {
    var selectedCategories = $("#category-multiselect").val();
    var selectedModels = $('#model-multiselect').val();

    selectedModelOptions().each(function() {
      var group = $(this).parent().attr("label");
      var modelId = $(this).attr("value");
      if( shouldRemoveModel(group, selectedCategories) ) {
        var elementToRemove = selectedModels.indexOf(modelId);
        selectedModels.splice(elementToRemove, 1);
      }
    });

    $("#model-multiselect").val(selectedModels).trigger("change");
  }

  function selectedModelOptions() {
    return $("#model-multiselect").find("option:selected");
  }

  function shouldRemoveModel( group, selectedCategories ) {
    var categoryId = getCategoryId(group);
    return (selectedCategories == null || selectedCategories.indexOf(categoryId) < 0);
  }
});