// Start/stop spinner for table buttons
$(document).on('ajax:beforeSend', '.table-button-remote', function(e, data, status, xhr) {
  $('.table-button-remote').toggleClass('disabled', true);
  icon = $(this).find('i')
  iconClass = icon.attr('class').split(' ').pop()
  icon.attr("data-icon-class", iconClass)
  icon.removeClass(iconClass)
  icon.addClass('fa-spinner spinning')
});

$(document).on('ajax:complete', '.table-button-remote', function(e, data, status, xhr) {
  icon = $(this).find('i')
  iconClass = icon.attr("data-icon-class")
  icon.removeClass('fa-spinner spinning')
  icon.addClass(iconClass)
  $('.table-button-remote').toggleClass('disabled', false);
});

// Start/stop spinner for table anchors
$(document).on('ajax:beforeSend', '.table-anchor-remote', function(e, data, status, xhr) {
  anchor = $(this);

  if (anchor.data("disabled")) {
    return false;
  }

  anchor.data("disabled", "disabled");
  anchor.prepend('<i class="fa fa-spinner spinning">&nbsp;</i>');
});

$(document).on('ajax:complete', '.table-anchor-remote', function(e, data, status, xhr) {
  anchor = $(this)

  if (anchor.data("disabled")) {
    anchor.find('i.spinning').remove();
    anchor.removeData("disabled");
    return true;
  }

  return false;
});