$(document).ready(function () {
  enableInlineEditable();
});

function enableInlineEditable() {
  $('.inline-editable').editable({
    ajaxOptions: { type: 'PATCH' },
    mode: 'inline',
    send: 'always',
    params: function(params) { return mapParams(params); }
  });
}

function mapParams(params) {
  var mapped_params = {};
  mapped_params[params.pk] = {};
  mapped_params[params.pk][params.name] = params.value;
  return mapped_params;
}
