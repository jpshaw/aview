function SearchSelect(elementId, searchPath, options) {
  options = options ? options : {};
  var filterElement = $(elementId);
  var publisherType = "Organization";
  var staticParams = options.queryParams ? options.queryParams : {};

  this.setPublisherType = function(type) {
    publisherType = type;
  }
  
  this.draw = function() {
    var extraOptions = options.enableCreateNew ? {
      tags: true,
      createTag: function (params) {
        return {
          id: params.term,
          name: params.term
        }
      }
    } : {};
    filterElement.select2(Object.assign({
      theme: 'bootstrap',
      ajax: {
        url: searchPath,
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return Object.assign({
            publisher_type: publisherType,
            query: params.term,
            page: params.page
          }, staticParams);
        },
        processResults: function (data, params) {
          if(options.enableCreateNew){
            data = data.results.filter(function(item) {
              return !(item.name === params.term);
            });
            return { results: data };
          }else{
            return data;
          }
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 3,
      templateResult: formatPublisher,
      templateSelection: formatPublisherSelection
    }, extraOptions));

    filterElement.on("select2:open", function(e){
      $(".select2-container").on("click", function(e){
        e.stopPropagation();
      });
    });

    filterElement.off("select2:closing", function(e){
      $(".select2-container").off();
    });
    if(options.tagMaxLength){
      filterElement.data().select2.$dropdown.find("input").attr("maxlength", options.tagMaxLength);
    }
  }

  // While select2 searches it will stub a default value:
  // { disabled: true, loading: true, text: "Searching..." }
  function formatPublisher(publisher) {
    if (publisher.loading) return publisher.text;
    var markup = '<div class="clearfix">' +
    '<div clas="col-sm-10">' +
    '<div class="clearfix">' +
    '<div class="col-sm-12">' + publisher.name + '</div>' +
    '</div>';

    markup += '</div></div>';

    return markup;
  }

  // Return the search results. If nothing has been found yet, publisher.text
  // will be "Searching ....".
  function formatPublisherSelection(publisher) {
    return publisher.name || publisher.text;
  }
}
