function refreshPermissionsPanel(alert) {
  $('#permissions-table').DataTable().ajax.reload(function() {
    $('#permissions > .panel-body').prepend(alert);
    setAlertExpiration($('#permissions > .panel-body > .alert'));
  });
}

$('.permission_manageable').livequery(function() {
  $(this).on('change', function() {
    var organizationId = $(this).val();
    $.ajax({
      url: '/organizations/' + organizationId + '/roles.js',
      dataType: 'html',
      success: function(data){
        $('#new-permission-roles').html(data);
      }
    });
  });
});

$('.permission_role').livequery(function() {
  $(this).on('change', function() {

    var roleId = $(this).val();
    var organizationId = $('#permission_manageable_id').val();
    var form = $(this).parents('form');

    $.ajax({
      url: '/organizations/' + organizationId + '/roles/' + roleId + '/abilities.js',
      dataType: 'html',
      beforeSend: function(xhr) {
        form.find('.sk-spinner').show();
        form.find('.alert').remove();
      },
      success: function(data){
        $('#permission_abilities').html(data);
      },
      complete: function(xhr, status) {
        form.find('.sk-spinner').hide();
      }
    });
  });
});