$(document).ready(function() {
    $("#location_country_id").change(function () {
      var countries_need_select = [];
      var country_id = $( "#location_country_id option:selected" ).val();
      var select_input_id = $("#location_region_select");
      var text_input_id = $("#location_region_text");

      countries_need_select.push($("#location_country_need_select").val())

      if (countries_need_select.includes(country_id)) {
        $(select_input_id).show().prop( "disabled", false);
        $(select_input_id).next().show();
        $(text_input_id).hide().prop( "disabled", true);
      }
      else {
        $(text_input_id).show().prop( "disabled", false);
        $(select_input_id).hide().prop( "disabled", true);
        $(select_input_id).next().hide();
      }
    }).trigger("change");
});
