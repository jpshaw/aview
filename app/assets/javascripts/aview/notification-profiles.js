
$(document).on('click', '.notifications-section-title', function() {
  current_section = $(this).siblings('.notifications-section-content').slideToggle();
  $('.notifications-section-content').not(current_section).slideUp();
  $('.notifications-section-content').not(current_section).siblings('.notifications-section-title').find('i').removeClass('fa-minus-square-o').addClass('fa-plus-square-o')
  $(this).find('i').toggleClass('fa-plus-square-o fa-minus-square-o');
});
