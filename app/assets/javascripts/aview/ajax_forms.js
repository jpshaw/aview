// Start/stop spinner for account form ajax calls
$(document).on('ajax:beforeSend', '.ajax-form', function(e, data, status, xhr) {

  form = $(this);

  if (form.data('disabled')) {
    return false;
  }

  if ( ( form.find('button[type="submit"] > i') ).length ) {
    buttonIcon = form.find('button[type="submit"] > i')
    buttonIcon.hide();
    buttonIcon.after('<i class="fa fa-spinner spinning"></i>')
  }

  form.data('disabled', 'disabled');
  form.find(':submit').toggleClass('disabled', true);
  form.find('.sk-spinner').show();
  form.find('.alert').remove();
  return true;
});

$(document).on('ajax:complete', '.ajax-form', function(e, data, status, xhr) {

  form = $(this)

  if (form.data('disabled')) {

    if ((form.find('button[type="submit"] > i')).length) {
      form.find('button[type="submit"] > i.spinning').remove();
      form.find('button[type="submit"] > i:first').show();
    }

    form.find('.sk-spinner').hide();
    form.find(':submit').toggleClass('disabled', false);
    form.removeData('disabled');
    return true;
  }

  return false;
});

