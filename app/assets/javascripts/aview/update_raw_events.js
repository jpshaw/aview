// Update events table

(function($) {

  $.update_raw_events = {
    options: {
      endpoint: '/events/since',
      pollInterval: 2,   // seconds
      insertInterval: 50, // milliseconds
      limit: 100,
      run: true
    },
    lastEventID: 0,
    params: "type=raw"
  };

  $.fn.update_raw_events = function(options) {
    options = $.extend($.update_raw_events.options, options);

    function getEventBox() {
      $.update_raw_events.eventBox = $('#event-box');
    }

    function getLatestEventID() {
      event_id = parseInt($('#event-box').data('event-id')) || 0 ;
      $.update_raw_events.lastEventID = event_id;
    }

    function updateEvents() {
      if($.update_raw_events.options.run === false) return;
      var params =  $.update_raw_events.params + "&event_id=" + $.update_raw_events.lastEventID;

      $.ajax({
        type: 'GET',
        data: params,
        dataType: 'json',
        url: options.endpoint,
        success: function(events) {
          if (events && events.length > 0) {
            $.each(events, function(index, event) {
              insertEventRow(event)
            });
            removeExtraRows();
            getLatestEventID();
          }
        },
        complete: function() {
          setTimeout(updateEvents, (options.pollInterval * 1000));
        }
      });
    }

    function insertEventRow(event) {
      row = event.date + " " + event.information + "\n";
      $.update_raw_events.eventBox.prepend(row);
      $('#event-box').data('event-id', event.id);
    }

    function removeExtraRows() {
      rowCount = $.update_raw_events.eventBox.html().split(/\n/).length;

      if( rowCount > options.limit ) {
        newHtml = "";

        lines = $.update_raw_events.eventBox.html().split(/\n/);

        for(var i in lines) {
          newHtml += "" + lines[i] + "\n";
          if(i >= (options.limit - 1)) {
            break
          }
        }
        $.update_raw_events.eventBox.html(newHtml)
      }
    }

    return this.each(function() {
      getEventBox();
      getLatestEventID();
      updateEvents();
    });
  };
})(jQuery);
