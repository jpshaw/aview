var DataTableReloader = function(table) {
  this.table = table;
  var timer = null;

  this.stop = function() {
    timer = clearTimeout(timer);
    cancelAnyRunningAJAX();
  };

  this.start = function() {
    if(timer == null && table.DataTable().page.info().page === 0) {
      timer = setTimeout( function (){ reloadDataTable(); }, 3000 );
    }
  };

  var cancelAnyRunningAJAX = function () {
    var all_settings = $($.fn.dataTable.tables()).DataTable().settings();
    for (var i = 0, settings; (settings = all_settings[i]); ++i) {
      if (settings.jqXHR) {
        settings.jqXHR.abort();
      }
    }
  };

  var reloadDataTable = function() {
    table.DataTable().ajax.reload( null, false );
    timer = null;
  };
};
