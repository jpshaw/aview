function selectedDeviceActions(dataTable) {
  var selectedMacList = [];
  var canModifyOrganization = $('#can_modify_organization').val();

  this.setupMoveToDataPlan = function(modal) {
    $('#move-to-data-plan-action').on('click', function () {
      $('#modals-container').html(modal);
      $('#selected-device-macs').val(selectedMacList);
      setOrganizationSelect2Handler();
      $('#move-to-data-plan-modal').modal({backdrop: 'static'});
      return false;
    });

    function setOrganizationSelect2Handler() {
      var organizationSelect = $('#organization_id');
      organizationSelect.select2({
        theme: 'bootstrap',
        width: '100%'
      });

      organizationSelect.on('change', function () {
        var organizationId = $('#organization_id').val();
        var form = $(this).parents('form');

        $.ajax({
          url: '/organizations/' + organizationId + '/data_plans',
          beforeSend: function (xhr) {
            form.find('.sk-spinner').show();
            form.find('.alert').remove();
          },
          complete: function (xhr, status) {
            form.find('.sk-spinner').hide();
          }
        });
      });
    }
  };

  $('#device-table tbody').on('click', 'input[type="checkbox"]', function(e){
    var selectedRow = $(this).closest('tr');
    var rowData = dataTable.row(selectedRow).data();
    var selectedMac = $.parseHTML(rowData.with_mac)[0].textContent;

    updateSelectedMacList(this, selectedMac);
    highlightRow(this, selectedRow);
    updateDataTableSelectAllCheckbox();
    updateActionsMenu();
    e.stopPropagation();
  });

  $('#device-table').on('click', 'tbody td, thead th:first-child', function(){
    $(this).parent().find('input[type="checkbox"]').trigger('click');
  });

  $('#data-table-select-all').on('click', function(e){
    if(this.checked){
      $('#device-table tbody input[type="checkbox"]:not(:checked)').trigger('click');
    } else {
      $('#device-table tbody input[type="checkbox"]:checked').trigger('click');
    }
    e.stopPropagation();
  });

  dataTable.on('draw', function(){
    selectedMacList = [];
    $('#data-table-select-all').prop('checked', false);
    $('#selected-devices-dropdown').prop("disabled", true);
  });

  function updateSelectedMacList(checkbox, macAddress) {
    var index = $.inArray(macAddress, selectedMacList);
    if(checkbox.checked && index === -1){
      selectedMacList.push(macAddress);
    } else if (!checkbox.checked && index !== -1){
      selectedMacList.splice(index, 1);
    }
  }

  function highlightRow(element, elementRow) {
    if(element.checked){
      elementRow.addClass('selected');
    } else {
      elementRow.removeClass('selected');
    }
  }

  function updateDataTableSelectAllCheckbox(){
    var tableNode = dataTable.table().node();
    var allCheckboxes = $('tbody input[type="checkbox"]', tableNode);
    var checkedCheckboxes = $('tbody input[type="checkbox"]:checked', tableNode);
    var selectAllCheckbox = $('thead input[name="select_all"]', tableNode).get(0);
    var noCheckboxesChecked = checkedCheckboxes.length === 0;
    var allCheckboxesChecked = checkedCheckboxes.length === allCheckboxes.length;

    if(noCheckboxesChecked){
      selectAllCheckbox.checked = false;
      if('indeterminate' in selectAllCheckbox){
        $('#data-table-select-all').prop("indeterminate", false);
      }
    } else if(allCheckboxesChecked){
      selectAllCheckbox.checked = true;
      if('indeterminate' in selectAllCheckbox){
        $('#data-table-select-all').prop("indeterminate", false);
      }
    } else {
      selectAllCheckbox.checked = true;
      if('indeterminate' in selectAllCheckbox){
        $('#data-table-select-all').prop("indeterminate", true);
      }
    }
  }

  function updateActionsMenu() {
    var checkedCheckboxes = $('tbody input[type="checkbox"]:checked', dataTable.table().node());
    if(checkedCheckboxes.length === 0 || canModifyOrganization === 'false') $('#selected-devices-dropdown').prop("disabled", true);
    else $('#selected-devices-dropdown').prop("disabled", false);
  }

  // this stops links in the table from selecting the row/device
  $('tr a').livequery(function() {
    $(this).on('click', function(e) {
      e.stopPropagation();
    });
  });
}