
function animateElement(element, animation) {
  var animationClass = animation + ' animated';
  var animationEnds = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

  element.removeClass(animationClass).addClass(animationClass).one(animationEnds, function() {
    $(this).removeClass(animationClass);
  });
}

function flashElement(element) {
  animateElement(element, 'flash');
}

function bounceElement(element) {
  animateElement(element, 'bounce');
}

function pulseElement(element) {
  animateElement(element, 'pulse');
}

function tadaElement(element) {
  animateElement(element, 'tada');
}

function shakeElement(element) {
  animateElement(element, 'shake');
}

function fadeInElement(element) {
  animateElement(element, 'fadeIn');
}

function rubberBandElement(element) {
  animateElement(element, 'rubberBand');
}

function incrementElement(element) {
  var current_count = parseInt(element.text());

  if (current_count < 0) {
    current_count = 0;
  }

  current_count++;

  element.text(current_count);
}

function decrementElement(element) {
  var current_count = parseInt(element.text());

  current_count--;

  if (current_count < 0) {
    current_count = 0;
  }

  element.text(current_count);
}