$(document).ready(function() {
  $('.check-all').livequery(function() {
    $(this).on('click', function() {
      var targetClass = $(this).attr('data-target-class');
      $('.'+targetClass + ' input:checkbox').iCheck('check');
      return false;
    });
  });

  $('.uncheck-all').livequery(function() {
    $(this).on('click', function() {
      var targetClass = $(this).attr('data-target-class');
      $('.'+targetClass + ' input:enabled:checkbox').iCheck('uncheck');
      return false;
    });
  });
});
