
function remoteButtonSend(button, icon) {
  if (icon === undefined) {
    icon = "fa-spinner";
  }

  if ((button.find('i') ).length) {
    button.find('i').hide();
  }

  if ((button.find('i.spinning')).length) {
    button.find('i.spinning').remove();
  }

  button.prepend('<i class="fa ' + icon + ' spinning"></i>');
  button.toggleClass('disabled', true);
}

function remoteButtonComplete(button) {
  button.find('i.spinning').remove();
  if ((button.find('i') ).length) {
    button.find('i:first').show();
  }
  button.toggleClass('disabled', false);
}

// Start/stop spinner for single buttons
$(document).on('ajax:beforeSend', '.single-button-remote', function(e, data, status, xhr) {
  remoteButtonSend($(this));
});

$(document).on('ajax:complete', '.single-button-remote', function(e, data, status, xhr) {
  remoteButtonComplete($(this));
});

// Catch clicks for complicated ajax forms with multiple buttons
$(document).on('click', '.ajax-submit-button', function(e, data, status, xhr) {
  var button = $(this);

  remoteButtonSend(button);

  $(this).parents('form').on('ajax:complete', function() {
    remoteButtonComplete(button);
  });
});

$(document).ready( function() {
  listenForButtonGroupLatchingClick();
});

function listenForButtonGroupLatchingClick() {
  $('.btn-group-latching > .btn').click(function () {
    setCheckButtons($(this));
  });
}

function setCheckButtons(selectedButton) {
  var btnGroup = selectedButton.closest('.btn-group');
  btnGroup.find('.btn.btn-primary').removeClass('btn-primary active').addClass('btn-white');
  selectedButton.removeClass('btn-white').addClass('btn-primary active');
}
