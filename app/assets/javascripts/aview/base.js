
$('.i-checks input').livequery(function() {
  $(this).iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green'
  });
});

$("[data-toggle=popover]").livequery(function() {
  $(this).popover();
});

$("[data-toggle=tooltip]").livequery(function() {
  $(this).tooltip();
});


function currentQueryString() {
  return window.location.search;
}

function queryStringParts(queryString) {
  var result = {};
  var parts = queryString.split("&");

  $.each(parts, function(i,v) {
    var tokens = parts[i].split("=");
    result[decodeURIComponent(tokens[0])] = decodeURIComponent(tokens[1]);
  });

  return result;
}

function formatTransferBytes(bytes, round) {
  if (round === undefined) { round = 0; }
  return filesize(bytes, { base:10, round: round });
}

function splitPathAndParams(uri) {
  var URIElements = {};
  var splitURI = uri.split('?');
  if ( splitURI.length === 2 ) {
    URIElements.base = splitURI[0];
    URIElements.queryParams = splitURI[1];
  } else if ( splitURI.length === 1 ) {
    URIElements.base = splitURI[0];
  } else {
    throw "ArgumentException: Unexpected number of URIElements";
  }

  return URIElements;
}

String.prototype.toTitleCase = function () {
  return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};
