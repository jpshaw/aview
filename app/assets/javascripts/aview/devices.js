$(document).on('ajax:beforeSend', '#update-device-location', function(e, data, status, xhr) {
  $(this).find('.sk-spinner').show();
  $(this).find('.alert').remove();
});

$(document).on('ajax:complete', '#update-device-location', function(e, xhr, status, error) {
  $(this).find('.sk-spinner').hide();
});

$(document).on('ajax:complete', '#location-clear-button', function(e, xhr, status, error) {
  $('#update-device-location .form-control').val('');

  if ( $('#update-device-location .select2') ) {
    $('#update-device-location .select2').select2('val', '');
  }
});
