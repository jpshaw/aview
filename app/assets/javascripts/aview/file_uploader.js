function extractFilenameFromPath(path) {
  return path.replace(/^.*[\\\/]/, '');
}

$(document).ready(function(){
  $("#upload-file").val($('#current_logo').val());

  $("#clear-logo").on('click', function() {
    $("#upload-file").val('');
    $("#organization_remove_logo").val('1');
    resetFormElement($('#upload-btn'));
    return false;
  });

  function resetFormElement(e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
  }
});

$(document).on('change', '#upload-btn', function() {
  var file = extractFilenameFromPath(this.value);
  $("#upload-file").val(file);
  $("#organization_remove_logo").val('0');
});
