$(function () {

  // Prepare user/password prompt modal form for UC Linux commands.
  $('.send_command.uc_linux').click(function(event) {
    // Clear user and password values.
    $('#device_user').val('');
    $('#device_password').val('');

    // Update remote_data value.
    $('#remote-data-command').val($(this).data('command'));
  });

  // Restore commands drop down.
  $('.sendCommandClose').click(function(event) {
    $("#spinner-icon").hide();
    $("#show-hide-toggle-commands").show("slide", { direction: "right" }, 800);
  });

  $('#tunnel_up, #tunnel_down, #tunnel_bounce, #tunnel_status').submit(function(event) {
    var form    = $(this);
    var index   = form.find('#remote_data_index').val().trim();
    var name    = form.find('#remote_data_name').val().trim();
    var message = '';

    if (index === '' && name === '') {
      message = 'Please enter an Interface or a Name value.';
    } else if (index !== '' && name !== '') {
      message = 'Please enter only the Interface or the Name value.';
    }

    if (message !== '') {
      alert(message);
      return false;
    }

    return true;
  });

  $('#wan-toggle-time').on('input', function() {
    var value = Number(this.value);
    var submitButton = $('#wan_toggle').find(':submit.command-submit-btn');
    var timeDiv = $(this).parent();

    if (this.value != '' && Math.floor(value) == value && this.value >= 0 && this.value <= 1440) {
      timeDiv.removeClass('has-error');
      submitButton.prop('disabled', false);
    } else {
      timeDiv.addClass('has-error');
      submitButton.prop('disabled', true);
    }
  });

});
