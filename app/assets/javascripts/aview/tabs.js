$(document).ready(function() {
  // Allow tabs to be triggered by links with data-trigger set.
  $('[data-trigger="tab"]').click(function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $('[data-toggle="tab"][href="' + href + '"]').trigger('click');
  });
});