function SingleDeviceMap(options) {
  var mapElement = document.getElementById(options.mapId);
  var devicePinMarker = mapElement.getAttribute('data-user-pin');
  var cellPinMarker = mapElement.getAttribute('data-cell-pin');
  var userLatitude = mapElement.getAttribute('data-user-latitude');
  var userLongitude = mapElement.getAttribute('data-user-longitude');
  var cellLatitude = mapElement.getAttribute('data-cell-latitude');
  var cellLongitude = mapElement.getAttribute('data-cell-longitude');
  var infoWindow = null;
  var mapOptions = {
    credentials: options.apiKey,
    mapTypeId: 'roadmap',
    zoom: 12,
    keyboardShortcuts: false,
    scrollwheel: false,
    panControl: true,
    rotateControl: false,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: false,
    maxZoom: 18,
    minZoom: 3
  };

  this.draw = function() {
    if (this.showUserLocation() && this.showCellLocation()) {
      drawMultiPointMap();
    } else if (this.showUserLocation()) {
      drawSinglePointMap(userLatitude, userLongitude, devicePinMarker);
    } else if (this.showCellLocation()) {
      drawSinglePointMap(cellLatitude, cellLongitude, cellPinMarker);
    }
  }

  this.showUserLocation = function() {
    return userLatitude != 'null' && userLongitude != 'null';
  }

  this.showCellLocation = function() {
    return cellLatitude != 'null' && cellLongitude != 'null' && options.showCellPins;
  }

  function drawSinglePointMap(centerLatitude, centerLongitude, marker) {
    mapOptions.center = new google.maps.LatLng(centerLatitude, centerLongitude);
    var map = new google.maps.Map(mapElement, mapOptions);
    new google.maps.Marker({ position: mapOptions.center, icon: marker, map: map });
  }

  function drawMultiPointMap() {
    var userEnteredLocation = new google.maps.LatLng(userLatitude, userLongitude);
    var cellLocation = new google.maps.LatLng(cellLatitude, cellLongitude);
    mapOptions.center = userEnteredLocation;
    var map = new google.maps.Map(mapElement, mapOptions);
    addPins(map, cellLocation, userEnteredLocation);
    setBounds(map, cellLocation, userEnteredLocation);
    addLine(map, cellLocation, userEnteredLocation);
  }

  function addPins(map, cellLocation, userEnteredLocation) {
    new google.maps.Marker({ position: userEnteredLocation, icon: devicePinMarker, map: map } );
    new google.maps.Marker({ position: cellLocation, icon: cellPinMarker, map: map } );
  }

  function addLine(map, cellLocation, userEnteredLocation) {
    var lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };
    var line = new google.maps.Polyline({
      path: [cellLocation, userEnteredLocation],
      map: map,
      strokeOpacity: 0,
      icons: [{
        icon: lineSymbol,
        offset: '0',
        repeat: '20px'
      }]
    });
    var lineLength = Math.round(google.maps.geometry.spherical.computeDistanceBetween(cellLocation, userEnteredLocation));

    line.addListener('click', function(e) {
      if (infoWindow != null) {
        infoWindow.close();
      }
      var infoWindowOptions = {
        content: "Distance: " + lineLength + " meters",
        position: e.latLng
      };
      infoWindow = new google.maps.InfoWindow(infoWindowOptions);
      infoWindow.open(map);
    });
  }

  function setBounds(map, cellLocation, userEnteredLocation) {
    var newBounds = new google.maps.LatLngBounds();
    newBounds.extend(userEnteredLocation);
    newBounds.extend(cellLocation);
    map.fitBounds(newBounds);
  }
}
