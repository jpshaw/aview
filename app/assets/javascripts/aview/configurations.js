$(function() {

  // Parent configuration modal organization change.
  $('#parent_configuration_organization_id').change(function(event) {
    $('.select2-drop').hide();  // Hide sticky select2 drop down.
    $.get(
      '/legacy_configurations/organization_configurations',
      {
        id:               $('#configuration_id').val(),
        organization_id:  $('#parent_configuration_organization_id').val()
      }
    );
  });

  // Parent configuration modal close.
  $('.parentConfigurationClose').click(function(event) {
    //This had to be added because the new modals are rendering outside
    //of the form. Thus the select box for parent id is outside of the form.
    var selectedParentId = $('#parent_configuration_id').val();
    $('#configuration_parent_id').val(selectedParentId);
    $('.select2-drop').hide();  // Hide sticky select2 drop down.
  })

  // Parent configuration modal select.
  $('#parentConfigurationSelect').click(function(event) {
    event.preventDefault();   // Do not fire form submission.

    var configuration_id = $('#parent_configuration_id').val();

    if (configuration_id == '') {
      alert('A configuration must be selected.');
    } else {
      $.get('/legacy_configurations/parent_configuration', {configuration_id: configuration_id});
      $('.parentConfigurationClose').click();
      $('#remove_parent_configuration').show();
      loadInheritedValues();
    }
  })

  // Remove parent configuration button.
  $('#remove_parent_configuration').click(function(event) {
    event.preventDefault();   // Do not fire form submission.

    $('#parent_configuration_name').val('No Configuration');
    $('#parent_configuration_id').val('');
    loadInheritedValues();

    // Hide this button.
    $(this).hide();
  });

});

function fixSelect2Styling() {
  $('.select2-container--default').addClass('select2-container--bootstrap').removeClass('select2-container--default');
  $('.select2-container--bootstrap').click(function() {
    $('.select2-container--default').addClass('select2-container--bootstrap').removeClass('select2-container--default');
  });
}