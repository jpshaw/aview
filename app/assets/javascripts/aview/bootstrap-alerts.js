

// Handles expiring and removing the given bootstrap alert elements
function setAlertExpiration(alert) {
  window.setTimeout(function() {
    alert.fadeTo(1000, 0).slideUp(1000, function() {
      $(this).remove();
    });
  }, 7000);
}