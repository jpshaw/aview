function DeviceMap(options) {
  var usaCenter = new google.maps.LatLng(38, -97);
  var canadaCenter = new google.maps.LatLng(56.7577, -86.4196);
  var ukCenter = new google.maps.LatLng(53.1142, 2.5771);
  var auCenter = new google.maps.LatLng(-25.910630, 134.249645);
  var africaCenter = new google.maps.LatLng(10.453231, 19.949793);
  var southAmericaCenter = new google.maps.LatLng(-14.914769, -60.763260);
  var eastAsiaCenter = new google.maps.LatLng(31.817102, 115.458362);
  var southAsiaCenter = new google.maps.LatLng(24.291614, 78.560773);
  var southEastAsiaCenter = new google.maps.LatLng(6.244556, 114.623402);
  var westAsiaCenter = new google.maps.LatLng(33.591944, 46.859730);

  var mapElement = document.getElementById(options.mapId);
  var overlayElement = $('#' + options.overlayId);
  var mapOptions = {
    credentials: options.apiKey,
    center: usaCenter,
    mapTypeId: 'terrain',
    zoom: 3,
    zoomControl: true,
    maxZoom: 18,
    minZoom: 3,
    mapTypeControl: true
  };
  var map;
  var markers;
  var markerClusterer;
  var spiderfier;

  this.draw = function(filter) {
    filter = filter || {};
    markers = [];
    map = new google.maps.Map(mapElement, mapOptions);;
    markerClusterer = new MarkerClusterer(map, markers, {
      maxZoom: 16,
      imagePath: 'images/markers/m'
    });
    spiderfier = new OverlappingMarkerSpiderfier(map, {
      markersWontMove: true,
      markersWontHide: true,
      basicFormatEvents: true
    });
    overlayElement.width(mapElement.offsetWidth);
    addPins(filter);
  }

  this.refresh = function(filter) {
    overlayElement.fadeIn();
    clearPins();
    addPins(filter);
  }

  function clearPins() {
    markerClusterer.clearMarkers();
    spiderfier.forgetAllMarkers();
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }

  function addPins(filter) {
    $.ajax({
      type: 'GET',
      url: options.dataURL,
      data: { filter: filter },
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      success: function(data) {
        addPinsSuccess(data);
      }
    });
  }

  function addPinsSuccess(pins) {
    $.each(pins, function (index, pin) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(pin.latitude, pin.longitude),
        icon: pin.marker,
        map: map
      });

      marker.addListener('spider_click', function() {
        displayInfobox(pin, marker);
      });

      markers.push(marker);
      markerClusterer.addMarker(marker);
      spiderfier.trackMarker(marker);
    });

    overlayElement.fadeOut();
  }

  function displayInfobox(pin, marker) {
    var description = pinDescription(pin);
    var window = new google.maps.InfoWindow({ content: description });
    window.open(map, marker);
  }

  // TODO: it would be nice to put this html into a template
  function pinDescription(pin) {
    return '<h3>' + pin.title + '</h3>' +
            'Organization: <br />' +
            mapLink('organizations', pin.organization_slug, pin.organization_name) +
            '<br /><br />MAC: <br />' +
            mapLink('devices', pin.device_mac, pin.device_mac) +
            '<br /><br />Site ID: <br />' +
            mapLink('sites', pin.site_id, pin.site_name) +
            '<br /><br />';
  }

  // TODO: it would be nice to put this html into a template
  function mapLink(link_base, link_suffix, link_title) {
    return '<a href="/'+ link_base + '/' + link_suffix +'">' + link_title + '</a>';
  }

  this.addRegionClickListeners = function() {
    $('#to_usa').click(function (event) {
      centerOnPoint(usaCenter, event);
    });
    $('#to_canada').click(function (event) {
      centerOnPoint(canadaCenter, event);
    });
    $('#to_uk').click(function (event) {
      centerOnPoint(ukCenter, event);
    });
    $('#to_au').click(function (event) {
      centerOnPoint(auCenter, event);
    });
    $('#to_africa').click(function (event) {
      centerOnPoint(africaCenter, event);
    });
    $('#to_south_america').click(function (event) {
      centerOnPoint(southAmericaCenter, event);
    });
    $('#to_east_asia').click(function (event) {
      centerOnPoint(eastAsiaCenter, event);
    });
    $('#to_south_asia').click(function (event) {
      centerOnPoint(southAsiaCenter, event);
    });
    $('#to_west_asia').click(function (event) {
      centerOnPoint(westAsiaCenter, event);
    });
    $('#to_southeast_asia').click(function (event) {
      centerOnPoint(southEastAsiaCenter, event);
    });
  }

  function centerOnPoint(centerPoint, event, zoom) {
    zoom = typeof zoom !== 'undefined' ? zoom : 1;
    map.setCenter(centerPoint);
    event.preventDefault();
  }
}
