var FirmwareSelect = function(nonProductionFirmwares) {
  var firmwareDropdown = $('.select2-firmware');

  firmwareDropdown.select2({
    theme: 'bootstrap',
    width: '100%',
    templateResult: highlightNonProductionFirmware
  });

  function highlightNonProductionFirmware(firmware) {
    if(nonProductionFirmwares.indexOf(parseInt(firmware.id)) > -1) {
      return $('<div class="alert-info">' + firmware.text + '</div>');
    }
    return firmware.text;
  }

  var container = $('#select2-device_configuration_device_firmware_id-container');
  highlightSelection();

  firmwareDropdown.on("select2:select", function() {
    highlightSelection();
  });

  function highlightSelection() {
    if(nonProductionFirmwares.indexOf(firmwareDropdown.val()) > -1) {
      container.addClass('alert-info');
    } else {
      container.removeClass('alert-info');
    }
  }

  this.onChange = function (onChangeFunction) {
    firmwareDropdown.on("change", function() {
      onChangeFunction($(this).val());
    });
  };

  this.val = function () {
    return firmwareDropdown.val();
  }
};