(function(){
  var _orgTextProto =  $.fn.editabletypes.text.prototype;
  var _orgDefaults = $.fn.editabletypes.text.defaults;
  $.fn.editabletypes.text = function(options){
    var data = $(options.scope).data();
    if(Object.keys(data).length > 0){
      for(var key in data){
        if(key.indexOf("attr") > -1){
          options.attrs = options.attrs ? options.attrs : {};
          options.attrs[key.split("attr")[1].toLowerCase()] = data[key];
        }
      }
    }
    this.init('text', options, _orgDefaults);
  };
  var _orgPrerender = $.fn.editabletypes.abstractinput.prototype.prerender;
  $.fn.editabletypes.abstractinput.prototype._orgPrerender = _orgPrerender;
  $.fn.editabletypes.abstractinput.prototype.prerender = function(){
    this._orgPrerender();
    if(this.options.attrs){
      for(var key in this.options.attrs){
        $(this.$tpl).attr(key, this.options.attrs[key]);
      }
    }
  };
  $.fn.editableutils.inherit($.fn.editabletypes.text, $.fn.editabletypes.abstractinput);
  $.fn.editabletypes.text.prototype = _orgTextProto;
  $.fn.editabletypes.text.defaults = _orgDefaults;
})();