$(document).ready(function() {
  var vsTargetWrapper = $('.vs-target-wrapper');
  if ( vsTargetWrapper.length > 0 ){
    vsSearchListener(vsTargetWrapper);
    initColumnVisibility();

    if (vsTargetWrapper.data('no-vs-spinner') != true) {
      addProcessingListener($('table.dataTable'));
    }
  }
});

// Sets spinners on DataTable processing start and end events.
function addProcessingListener(table) {
  table.on( 'processing.dt', function ( e, settings, processing ) {
    if ( processing ) {
      $('.visual-search').animate({'padding-right': '40px'});
      $('.visual-search-wrapper .circle-border-spinner').fadeIn(300);
      $('.dataTables_wrapper .lower .circle-border-spinner').fadeIn(150).css('display', 'inline-block');
    }
    else {
      $('.visual-search').animate({'padding-right': '0px'});
      $('.visual-search-wrapper .circle-border-spinner').fadeOut(300);
      $('.dataTables_wrapper .lower .circle-border-spinner').fadeOut(150);
    }
  }).dataTable();
}

function facetModelHasValues(facet, facetModels) {
  return facetModels[facet.name].facet_values !== '';
}

// Gets the actual value of the option selected (e.g. the category id) and formats it
// properly for serialization.
function getFilterValue(facet, target, vsTargetWrapper){
  var facetModels = vsTargetWrapper.data('facets');
  var filterValue = facetModelHasValues(facet, facetModels) ? lookupFacetValue(facet, facetModels) : facet.value;

  if ( !target.is_table ){ return filterValue; }
  var result = {}; result[facet.scope] = filterValue; return result;
}

function getSearchObj(target) {
  var searchObj = target.selector.search();
  if (searchObj.length < 1) { searchObj = []; }
  else { searchObj = JSON.parse(searchObj); }
  return searchObj;
}

function getTarget(facet, table) {
  var target = {};
  target.selector = table.column(facet.scope + ':name'); // Set target column

  // If column doesn't exist with scope, then set target to table
  if ( target.selector.length < 1 ) {
    target.selector = table;
    target.is_table = true;
  }

  return target;
}

function lookupFacetValue(facet, facetModels) {
  return facetModels[facet.name].facet_values[facet.value];
}

function resetSearch(table) {
  table.search(''); // Clear table search params
  table.columns().every( function() {
    this.search(''); // Clear individual column search params
  });
}

function setDataTableSearch(queryFacets, tableAPI, vsTargetWrapper) {
  $.each(queryFacets, function(i, facet){
    var target = getTarget(facet, tableAPI);
    var filterValue = getFilterValue(facet, target, vsTargetWrapper);
    var searchObj = getSearchObj(target);

    searchObj.push(filterValue);
    target.selector.search(JSON.stringify(searchObj));
  });
}

// Visual Search Box Enabled Table
// Listen for VisualSearch search event, get facets, redraw table.
function vsSearchListener(vsTargetWrapper){
  var tableAPI = $('.dataTable').dataTable().api();

  $(document).on('vs:search', function(e, query) {
    var queryFacets = query.facets;
    resetSearch(tableAPI);
    setDataTableSearch(queryFacets, tableAPI, vsTargetWrapper);
    tableAPI.draw();
  });
}
