function clientUsageTable() {
  var table;

  this.load = function(options) {
    table = $(options.target).DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        type: 'POST',
        url: options.url,
        data: {
          range: options.range,
          deviceId: options.deviceId,
          app: options.app
        }
      },
      "bInfo" : false,
      "pagingType": "full_numbers",
      "bFilter": false,
      "bSort": false,
      "bPaginate": false,
      "bRetrieve": true,
      "scrollX": true,
      "columns": [
        {className: "text-center"},
        {className: "text-center"},
        {className: "text-center"},
        {className: "text-center"}
      ]
    });
  };

  this.reload = function(options) {
    table.destroy(); //used to capture new range data
    this.load(options);
  }
}