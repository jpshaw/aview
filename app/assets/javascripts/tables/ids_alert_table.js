// eslint-disable-next-line no-unused-vars
function idsAlertsTable(options) {
  this.load = function() {
    $(options.target).DataTable({
      ajax: function (data, callback) {
        $.getJSON(options.uri).done( function(alerts) { callback(formatAlerts(alerts)); } );
      },
      bInfo: false,
      bFilter: false,
      bSort: false,
      bPaginate: false
    });
  };

  function formatAlerts(alerts) {
    var RULE_CODE = 1;
    var REFERENCES = 2;

    alerts.data.forEach(function(alert){
      alert[RULE_CODE] = makeRuleLink(alert[RULE_CODE]);
      alert[REFERENCES] = makeRefLinks(alert[REFERENCES]);
    });

    return alerts;
  }

  function makeRefLink(uri, code) {
    var WHITEHATS_URI = "whitehats.com";

    if (uri.indexOf(WHITEHATS_URI) !== -1 || uri.length === 0)
      return '';

    if (uri == "url") {
      uri = code.startsWith("http://") ? code : "http://" + code;
      code = code.split("/").shift()
      return  "<a target='_blank' href='" + uri + "'>" + code + "</a>";
    }

    return  "<a target='_blank' href='" + uri + code + "'>" + code + "</a>";
  }

  function makeRefLinks(references) {
    if (references == null)
      return "";

    var links = "";
    references = references.split(",");

    while (references.length > 0) {
      var systemURI = references.shift();
      var link = references.shift();
      var referenceLink = makeRefLink(systemURI, link)

      if(referenceLink != '') {
        links += referenceLink + ", ";
      }
    }

    return links.substring(0, links.length - 2);
  }

  function makeRuleLink(code) {
    return "<a target='_blank' href='https://www.snort.org/rule-docs/" + code + "'>" + code + "</a>";
  }
}
