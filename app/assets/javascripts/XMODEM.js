/*
 * XMODEM.js - browser based XMODEM protocol sender.
 *
 * - Supports XMODEM and XMODEM1K transfers.
 * - Supports CRC16 and checksum.
 * - Can be hooked into terminal clients like Shellinabox and Websockify's Telnet client.
 * - Compatible at least with 'rx' receiver from BusyBox v1.21.0
 *
 * XMODEM protocol:
 *   http://wiki.synchro.net/ref:xmodem
 *   http://techheap.packetizer.com/communication/modems/xmodem.html
 *
 * @author Samuel Brian <sam.brian@accelerated.com>
 */

/** A JavaScript XMODEM implementation.
 * Provide a function to send data to the receiver, and `pushRecvBuffer()` data
 * from the receiver when it becomes available.
 *
 * @param {Function} sendFunction - A function that takes arguments `(data)` that the XMODEM instance is to write
 *                                  output with.
 * @param {boolean} [sendAsString=false] - True to have data passed to `sendFunction` as a string, otherwise data is
 *                                         passed as an array-of-ints.
 * @constructor
 */
function XMODEM(sendFunction, sendAsString) {
  this.sendAsString = sendAsString;
  this.maxBlockSendAttempts = 10;
  this.useXMODEM1K();
  this.debug = false;
  this.warnings = true;
  this.expectEOTReply = false;
  this.rQ = [];
  this._recvHandler = null;
  this._recvTimer = null;
  this._userSend = sendFunction;
}

XMODEM.TIMEOUT_LONG = 30000;
XMODEM.TIMEOUT_SHORT = 1000;

XMODEM.SOH = 0x01;
XMODEM.STX = 0x02;
XMODEM.ETX = 0x03; // ^C
XMODEM.EOT = 0x04;
XMODEM.ACK = 0x06;
XMODEM.NAK = 0x15;
XMODEM.ETB = 0x17; // ^W
XMODEM.CAN = 0x18; // ^X
XMODEM.SUB = 0x1A; // ^Z
XMODEM.CRC = 'C'.charCodeAt(0);

XMODEM.CANCEL_TIMEOUT = -1;
XMODEM.CANCEL_USER = -2;

XMODEM.prototype.profileBusyBoxRx = function() {
  // rx from BusyBox
  this.useXMODEM1K();
  this.expectEOTReply = true;
}

XMODEM.prototype.profileLRZ = function() {
  // lrz from lrzsz [https://ohse.de/uwe/software/lrzsz.html]
  this.useXMODEM1K();
  this.expectEOTReply = false;
}

/** Call to use standard XMODEM packet sizes during transfers. */
XMODEM.prototype.useXMODEM = function() {
  this.startByte = XMODEM.SOH;
};

/** Call to use XMODEM1K packet sizes during transfers (default). */
XMODEM.prototype.useXMODEM1K = function() {
  this.startByte = XMODEM.STX;
};

/** Transfer data using the currently selected protocol.
 *
 * @param {number[]|string} data - The data to send, either as array of numbers [0 - 255], or as a string.
 * @param {Function} callback - Function called when the transfer is complete. Arguments are `(error)`.
 * @param {Function} [progressCallback] - Function called after each succesful block transfer.
 *                                        Arguments are `(blockCount, numBlocks)`.
 */
XMODEM.prototype.transferData = function(data, callback, progressCallback) {
  this.stopFlag = false;
  if (typeof data === "string") {
    data = XMODEM.str2ints(data);
  }

  var useCRC = false;
  var blk = 0x01;
  var blkLen;
  if (this.startByte == XMODEM.SOH) {
    blkLen = 128;
    if (this.debug) console.log("XMODEM: using 128 byte blocks (XMODEM protocol).");
  } else if (this.startByte == XMODEM.STX) {
    blkLen = 1024;
    if (this.debug) console.log("XMODEM: using 1024 byte blocks (XMODEM1K protocol).");
  } else {
    callback(new Error("XMODEM: invalid start byte '"
        + this.startByte + "', expecting SOH (0x01) or STX (0x02)"));
    return;
  }

  var blockCount = 0;
  var numBlocks = Math.ceil(data.length/blkLen);

  if (this.debug) console.log("XMODEM: waiting for receiver.");
  var that = this;
  this._recv(1, function(transfer) {
    if (transfer[0] == XMODEM.CRC) {
      if (that.debug) console.log("XMODEM: using CRC16.");
      useCRC = true;
    } else if (transfer[0] == XMODEM.NAK) {
      if (that.debug) console.log("XMODEM: using checksum.");
      useCRC = false;
    } else if (transfer[0] == XMODEM.CANCEL_USER) {
      callback(null, false);
      return;
    } else if (transfer[0] == XMODEM.CANCEL_TIMEOUT) {
      callback(new Error("XMODEM: timed out waiting for transfer initiation.", false));
      return;
    } else {
      callback(new Error("XMODEM: couldn't initiate transfer. Received start byte "
          + (transfer.length ? "0x" + transfer[0].toString(16) : 'null')
          + ", expecting 'C' (0x43) for CRC16 or NAK (0x15) for checksum"));
      return;
    }
    if (progressCallback) progressCallback(0, numBlocks);
    sendNextPacket();
  });

  function sendNextPacket(sendAttempts) {
    if (blockCount >= numBlocks) {
      if (that.debug) console.log("XMODEM: ending transmission.");
      that._send([ XMODEM.EOT ]);
      if (that.expectEOTReply) {
        that._recv(1, function(reply) {
          if (reply[0] == XMODEM.ACK) {
            callback(null, true);
          } else if (reply[0] == XMODEM.NAK) {
            blockCount--;
            sendNextPacket();
          } else if (reply[0] == XMODEM.CAN) {
            callback(new Error("XMODEM: transfer cancelled by receiver."));
          } else if (reply[0] == XMODEM.CANCEL_USER) {
            callback(null, false);
          } else if (transfer[0] == XMODEM.CANCEL_TIMEOUT) {
            callback(new Error("XMODEM: timed out waiting for EOT response.", false));
          } else {
            callback(new Error("XMODEM: received unknown EOT response "
                + (reply.length ? "0x" + reply[0].toString(16) : 'null')
                + ", expecting ACK (0x06) for success or NAK 0x15 for resend"));
          }
        });
      } else {
        callback(null, true);
      }
      return;
    }

    var packetInfo = [ that.startByte, blk, 0xFF-blk ];
    var blkData = data.slice(blockCount*blkLen, (blockCount + 1)*blkLen);
    if (!Array.isArray(blkData)) blkData = [].slice.call(blkData); // Convert block from typed array to array

    // padding for last block
    while (blkData.length < blkLen) {
      blkData.push(XMODEM.SUB);
    }

    if (useCRC) {
      var crc = XMODEM.genCRC16(blkData);
      blkData = packetInfo.concat(blkData);
      XMODEM.push16(blkData, crc);
    } else {
      var checksum = XMODEM.genChecksum(blkData);
      blkData = packetInfo.concat(blkData);
      XMODEM.push8(blkData, checksum);
    }

    blk = (blk < 0xFF) ? blk + 1 : 0x00;
    blockCount++;

    if (that.debug) console.log("XMODEM: sending block " + blockCount + " of " + numBlocks + "." );
    that._send(blkData);
    that._recv(1, function(reply) {
      if (reply[0] == XMODEM.ACK) {
        if (progressCallback) progressCallback(blockCount, numBlocks);
        setTimeout(sendNextPacket, 0);
      } else if (reply[0] == XMODEM.NAK) {
          if (that.warnings || that.debug) console.log("XMODEM: resending block " + blockCount + ".");
          blockCount--;
          setTimeout(sendNextPacket, 0);
      } else if (reply[0] == XMODEM.CAN) {
        callback(new Error("XMODEM: transfer cancelled by receiver."));
      } else if (reply[0] == XMODEM.CANCEL_USER) {
        callback(null, false);
      } else if (reply[0] == XMODEM.CANCEL_TIMEOUT) {
        callback(new Error("XMODEM: timed out waiting for block transfer response."), false);
      } else {
        callback(new Error("XMODEM: received unknown response byte "
            + (reply.length ? "0x" + reply[0].toString(16) : 'null')
            + ", expecting ACK (0x06) for packet success or NAK (0x15) for resend"));
      }
    });
  }
};

XMODEM.prototype.cancel = function(currentBlockOnly, errorByte) {
  this.stopFlag = currentBlockOnly == null ? true : !currentBlockOnly;
  if (this._recvTimer) {
    clearTimeout(this._recvTimer);
    this._recvTimer = null;
  }
  var handler = this._recvHandler;
  if (handler) {
    this._recvHandler = null;
    handler.cb([ errorByte || XMODEM.CANCEL_USER ], 1);
  }
}

/** Invoke the remote host's "rx" command and upload a file.
 *
 * @param {string} filename - The destination filename on the host.
 * @param {number[]|string} data - The data to send, either as array of numbers [0 - 255], or as a string.
 * @param {Function} callback - Function called when the transfer is complete. Arguments are `(error)`.
 * @param {Function} [progressCallback] - Function called after each succesful block transfer.
 *                                        Arguments are `(blockCount, numBlocks)`.
 */
XMODEM.prototype.sendFileRx = function(filename, data, callback, progressCallback) {
  var that = this;
  this.stopFlag = false;
  var cmd = "rx " + filename + "\r\n";
  this._send(cmd);
  this._recv(cmd.length, function(echoCmd) {
    echoCmd = XMODEM.ints2str(echoCmd);
    if (echoCmd[0] == -1) {
      callback(null, false);
    } else if (echoCmd != cmd) {
      callback(new Error("XMODEM: expecting echo of '" + cmd
          + "', instead received '" + echoCmd + "'"));
    } else {
      that.transferData(data, callback, progressCallback);
    }
  });
};

/** Push data into the receive buffer.
 * The transfer process will poll the receive buffer, so you **MUST** put data in it or the transfer will time out.
 *
 * @param {number[]|string} data - Received data as an array of integers ranging from 0 to 255 (preferable) or string.
 */
XMODEM.prototype.pushRecvBuffer = function(data) {
  if (typeof data === "string") {
    data = XMODEM.str2ints(data);
  }

  this.rQ = this.rQ.concat(data);
  var handler = this._recvHandler;
  if (handler && this.rQ.length >= handler.nbytes) {
    clearTimeout(this._recvTimer);
    this._recvTimer = null;
    this._recvHandler = null;
    handler.cb(this.rQ.splice(0, handler.nbytes), handler.nbytes);
  }
};

/** Empty the receive buffer. */
XMODEM.prototype.emptyRecvBuffer = function() {
  this.rQ = [];
}

/** Internal function to poll the receive buffer.
 * @param {number} n - The number of bytes to read.
 * @param {Function} callback - Callback function called when the requested number of bytes are read, or the most
 *                              available after max timeouts exceeded. Arguments `(bytes, numBytes)`.
 * @param {number} [timeout=XMODEM.TIMEOUT_LONG] -
 * @private
 */
XMODEM.prototype._recv = function (n, callback, timeout) {
  if (this.stopFlag) {
    callback([ XMODEM.CANCEL_USER ], 1);
    return;
  }

  // Already buffered enough data
  if (this.rQ.length >= n) {
    callback(this.rQ.splice(0, n), n);
  } else {
    this._recvHandler = { cb: callback, nbytes: n };
    this._recvTimer = setTimeout(this.cancel.bind(this, true, XMODEM.CANCEL_TIMEOUT), timeout || XMODEM.TIMEOUT_LONG, XMODEM.CANCEL_TIMEOUT);
  }
};

/** Internal function to send data to the receiver.
 * @param {number[]|string} data
 * @private
 */
XMODEM.prototype._send = function(data) {
  if (this.sendAsString && typeof data !== "string") {
    data = XMODEM.ints2str(data);
  } else if (!this.sendAsString && typeof data === "string") {
    data = XMODEM.str2ints(data);
  }
  this._userSend(data);
};

/** Generate CRC16.
 * @param {number[]} data
 * @return {number}
 */
XMODEM.genCRC16 = function(data) {
  var expected = 0;
  for (var i = 0; i < data.length; i++) {
    expected = expected ^ data[i] << 8;
    for (var j = 0; j < 8; j++) {
      if (expected & 0x8000) {
        expected = (expected << 1) ^ 0x1021;
      } else {
        expected = (expected << 1);
      }
    }
  }
  return expected & 0xffff;
};

/** Generate checksum.
 * @param {number[]} data
 * @return {number}
 */
XMODEM.genChecksum = function(data) {
  var checksum = 0;
  for (var i = 0; i < data.length; i++) {
    checksum = (checksum + data[i]) & 0xFF;
  }
  return checksum;
};

/** Convert a string to an array of integers in 8-bit unsigned range.
 * @param {string} str
 * @return {number[]}
 */
XMODEM.str2ints = function(str) {
  var ints = [];
  for (var i = 0; i < str.length; i++) {
    ints[i] = str.charCodeAt(i);
  }
  return ints;
};

/** Convert an array of integers in 8-bit unsigned range to a string.
 * @param {number[]} ints
 * @return {string}
 */
XMODEM.ints2str = function(ints) {
  var str = "";
  for (var i = 0; i < ints.length; i++) {
    str += String.fromCharCode(ints[i]);
  }
  return str;
};

XMODEM.push16 = function(array, num) {
  array.push((num >> 8) & 0xFF, num & 0xFF);
};

XMODEM.push8 = function(array, num) {
  array.push(num & 0xFF);
};
