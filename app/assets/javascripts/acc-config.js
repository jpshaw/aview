// vim: set ts=8 sw=4 et softtabstop=4 ai: some formatting helpers

/* NOTES:
 *
 * - Don't forget to escape.
 *   Use jQuery.fn.text and jQuery.fn.attr rather than string concatenation where possible.
 */


/*FIXME: Monkey-patching is not recommended */

// Monkey-patch for browser with no Array.indexOf support
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        "use strict";
        if (this === void 0 || this === null)
            throw new TypeError();
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0)
            return -1;
        var n = 0;
        if (arguments.length > 0) {
            n = Number(arguments[1]);
            if (n !== n)
                n = 0;
            else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0))
                n = (n > 0 ? 1 : -1) * Math.floor(Math.abs(n));
        }
        if (n >= len)
            return -1;
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement)
                return k;
        }
        return -1;
    };
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (prefix) {
        "use strict";
        return this.lastIndexOf(prefix, 0) === 0;
    };
}

var accfg = {};

accfg.Accfg = function (formElement, panelElement, schema) {
    var _inst = this;
    var clicks = 0, timer = null;
    this.bcrypt = new bCrypt();
    this.fieldPrivate = '\u2022\u2022\u2022\u2022\u2022';
    this.fieldNamespaceSeparator = '.';
    this.fieldNamespaceSeparatorRegex = /\./g;
    this.formElement = $(formElement);
    this.panelElement = $(panelElement) ||
            this.formElement.find('.accfg-panel');
    this.documentSchema = schema;
    this.configurationUtils = new ConfigurationUtils();

    // Validation and Alias fixing
    this.panelElement.on('change', '.value-input', function (evt) {
        var nodeElement = $(this).data('accfg-element');
        _inst.syncLabels(nodeElement);
        _inst.syncAlias(nodeElement);
        _inst.syncOneDepend(nodeElement);
        _inst.validateDataLater();
    });
    this.panelElement.on('keyup', '.value-input', function (evt) {
        var nodeElement = $(this).data('accfg-element');
        _inst.syncLabels(nodeElement);
        _inst.syncAlias(nodeElement);
        _inst.syncOneDepend(nodeElement);
        _inst.validateDataLater();
    });

    // Object property adder
    this.panelElement.on('click', '.property-add', function (evt) {
        evt.preventDefault();
        _inst.onAddObjectProperty($(this));
    });

    // Array item adder
    this.panelElement.on('click', '.item-add', function (evt) {
        evt.preventDefault();
        _inst.onAddListItem($(this));
    });

    // Action: set to default
    this.panelElement.on('click', '.field-setdefault', function (evt) {
        evt.preventDefault();
        _inst.onSetDefaultField($(this));
    });

    // Action: set to original
    this.panelElement.on('click', '.field-setoriginal', function (evt) {
        evt.preventDefault();
        _inst.onSetOriginalField($(this));
    });

    // Action: inherit
    this.panelElement.on('click', '.field-inherit', function (evt) {
        evt.preventDefault();
        _inst.onInheritField($(this));
    });

    // Action: override
    this.panelElement.on('click', '.field-override', function (evt) {
        evt.preventDefault();
        _inst.onOverrideField($(this));
    });

    // Action: delete
    this.panelElement.on('click', '.field-delete', function (evt) {
        evt.preventDefault();
        _inst.onDeleteField($(this));
    });

    // Stop collapsing for dropdown menu
    this.panelElement.on('click', '.dropdown', function (evt) {
        evt.preventDefault();
    });

    // Collapsible field (object and array)
    this.panelElement.on('click', '.collapser', function (evt) {
        if (evt.isDefaultPrevented()) return;
        var collapser = $(this);
        var nodeElement = collapser.data('accfg-element');
        clicks++;

        if ( clicks === 1 ) {
            if (nodeElement) {
                _inst.setExpanded(nodeElement, !nodeElement.expanded);
            }
            timer = setTimeout(function() { clicks = 0; }, 400);
        } else if (clicks === 2) {
            clearTimeout(timer);
            _inst.setExpandedAll(nodeElement.parent, nodeElement.field.attr("id"), nodeElement.expanded);
            clicks = 0;
        }
    });

    // This prevents pressing the enter key from submitting the form
    this.panelElement.on('keypress',function(evt){
        if (evt.which == 13 && !($(evt.target).is('textarea'))) {
            evt.preventDefault();
        }
    });

    //this.panelElement.hide();
};

accfg.Accfg.prototype.render = function (schema, data, opts) {
    this.documentSchema = schema || this.documentSchema;
    if (!this.documentSchema) {
        //CHECK: Bail out or freestyle object?
    }
    var defaults = { view: null, readOnly: false };
    this.options = $.extend({}, defaults, opts);
    this.validateTimer = null;
    this.validateOnly = false;
    this.validateExpand = false;
    var migrator = new Migrator(this.documentSchema);
    migrator.migrate(data);
    data = this.configurationUtils.schemaDefault(schema, data);
    var inheritData = this.options.parentData;
    var override = null;
    if (inheritData != null) {
        migrator.migrate(inheritData);
        inheritData = this.configurationUtils.schemaDefault(schema, inheritData);
        if (data.hasOwnProperty('schema')) {
            if (data.schema.hasOwnProperty('override')) {
                override = data.schema.override;
            }
        }
        data = this.configurationUtils.merge(schema, inheritData, data, override);
    }

    this.panelElement.empty();
    this.instanceId = this._generateFieldId();
    this.documentTree = this.createTreeNode({
        parent: null,
        name: null,
        schema: this.documentSchema,
        data: data,
        originalData: data,
        inheritData: inheritData,
        inheritFixedParent: false
    }, override);
    var nodeElement = this.createTreeElement(null, this.documentTree);
    nodeElement.container = this.panelElement;
    this.renderObject(nodeElement);
    this.renderAllRefs(this.panelElement);
    this.syncAllDepends(this.documentTree);
    if (!this.options.readOnly) {
        this.validateExpand = true;
        this.validateDataNow();
    }
    if (opts.renderFinished) {
        opts.renderFinished(this.panelElement);
    }
};


/*
 * documentTree:
 *
 * node.schema is the schema for the field. This may be a copy of the
 * schema containing specific changes for this field (eg title).
 *
 * node.parent is the parent object/array.
 *
 * node.name is unique amongst the children of the parent. It is the
 * property name for children of objects, or the original array index
 * for children of arrays, but is not updated when array fields are
 * reordered.
 *
 * node.path is the concatenation of node.parent.path and node.name.
 *
 * node.label is the label for this node.
 *
 * node.itemLabel is the label used for describing the children of this
 * node. This is used for references to children.
 *
 * node.labelElement is the handle to the field label DOM element.
 *
 * node.dependentLabels is an array of nodes of which the labels depend
 * on the value of this node.
 *
 * node.dependents is an array of nodes that depend on the value of
 * this node.
 *
 * node.ref is an array of nodes that this node can refer to.
 *
 * node.properties contains the properties of object fields.
 * node.items contains the items of array fields.
 *
 * node.data is the value of the node to use when rendering.
 *
 * node.originalData is the value of the node when the form is first
 * created.
 *
 * node.inheritData is the value of the node in the parent configuration.
 *
 * node.inherit is a boolean to indicate whether this node's data is
 * inherited or not. For objects, this field may also be set to null
 * to indicate that inheritance status is determined by the individual
 * properties, not the object as a whole.
 *
 * node.inheritFixed is a boolean to indicate whether node.inherit
 * can be changed independently of other nodes. It is derived from
 * node.inheritFixedParent and node.inheritFixedDepend.
 *
 * node.overrideAll is a boolean to indicate whether the user selected
 * to override the node and all of its children.
 *
 * node.elements is an array of objects containing handles to DOM elements.
 * This will always contain at least one object, and more than one if
 * there are aliases.
 *
 * node.elements[].parent is set to node.
 *
 * node.elements[].parentElement is set to one of node.parent.elements.
 *
 * node.elements[].field is the outermost element of the field. Set by
 * renderField() at return. field.data('accfg-element') points to this
 * element object.
 *
 * node.elements[].container is the element that field value should be
 * place in.  Set by renderField() before calling renderFieldValue().
 *
 * node.elements[].value is the innermost element of the field value.
 * This is the element that should be accessed to determine the value
 * of the field. Set by renderFieldValue() at return.
 *
 * node.elements[].errormsg is the element that will contain any validation
 * errors messages for the field.  Set by renderFieldValue() at return.
 *
 * For objects, node.elements[].add is the element containing the new
 * property name when adding fields.
 *
 * For arrays, node.lastIndex is the highest used index.
 */

/* Caller must set the following in node:
 *      parent, name, schema
 * and optionally set the following:
 *      data, originalData, inheritData, inheritFixedParent
 */
accfg.Accfg.prototype.createTreeNode = function (node, override) {
    this.createTreeNodeInternal(node, override);
    this.setTreeNodeFlags(node);
    return node;
};

accfg.Accfg.prototype.setTreeNodeFlags = function (node) {
    var changed = false;
    var changedNodes = [];

    // Check if any dependency in the dependency chain is overridden
    node.inheritFixedDepend = false;
    var dependNode = node;
    while (dependNode.schema.depend) {
        var dependNode = this.treeLookup(dependNode, dependNode.schema.depend);
        if (!dependNode) {
            break;
        }
        if (!dependNode.inherit) {
            // Disable inherit, but keep whatever data it had already
            node.inherit = false;
            node.inheritFixedDepend = true;
            break;
        }
    }

    var inheritFixed = node.inheritData == null || node.inheritFixedParent || node.inheritFixedDepend;
    if (node.inheritFixed !== inheritFixed) {
        node.inheritFixed = inheritFixed;
        changed = true;
    }

    // Set readOnly now that we have the final node.inherit value
    var readOnly = node.schema.readonly || this.options.readOnly || node.inherit;
    if (node.readOnly !== readOnly) {
        node.readOnly = readOnly;
        changed = true;
    }

    if (changed) {
        changedNodes = [ node ];
    }

    if (node.schema.type == 'object') {
        for (var propName in node.properties) {
            changedNodes = changedNodes.concat(this.setTreeNodeFlags(node.properties[propName]));
        }
    } else if (node.schema.type == 'array') {
        for (var i = 0; i < node.items.length; i++) {
            changedNodes = changedNodes.concat(this.setTreeNodeFlags(node.items[i]));
        }
    }

    return changedNodes;
};

accfg.Accfg.prototype.createTreeNodeInternal = function (node, override) {
    node.overrideAll = false;
    if (node.inheritData == null) {
        node.inherit = false;
    } else if (node.inheritFixedParent) {
        node.inherit = node.parent.inherit;
    } else if (override === false) {
        node.inherit = true;
    } else {
        node.inherit = false;
        node.overrideAll = true;
    }
    if (node.data == null) {
        node.data = this.configurationUtils.schemaDefault(node.schema, node.schema['default']);
    }
    node.label = node.schema.label || node.schema.title || node.name;
    if (node.schema.additionalProperties) {
        node.itemLabel = node.schema.additionalProperties.title || node.label;
    } else {
        node.itemLabel = node.label;
    }
    node.elements = [];
    node.dependents = [];
    node.dependentLabels = [];
    if (node.parent) {
        node.path = node.parent.path.concat([node.name]);
    } else {
        node.path = [];
    }
    if (node.schema.type === 'object') {
        this.createTreeObject(node, override);
    } else if (node.schema.type === 'array') {
        this.createTreeArray(node);
    }
    return node;
};

accfg.Accfg.prototype.createTreeObject = function (node, override) {
    var _inst = this;
    if (node.schema.properties == null) {
        node.schema.properties = {};
    }
    var props = node.schema.properties;

    var propInheritFixedParent;
    if (node.inheritFixedParent) {
        propInheritFixedParent = true;
    } else if (this.treeObjectIsAdditional(node)) {
        // properties of additional objects must be fixed
        propInheritFixedParent = true;
    } else {
        // node.inherit is meaningless if properties aren't fixed
        node.inherit = null;
        propInheritFixedParent = false;
    }

    var createProperty = function (propName, propSchema) {
        var propOverride = false;
        if (override === true || override === false) {
            propOverride = override;
        } else if (override != null && override.hasOwnProperty(propName)) {
            propOverride = override[propName];
        }
        return _inst.createTreeNodeInternal({
            parent: node,
            name: propName,
            schema: propSchema,
            data: node.data[propName],
            originalData: node.originalData != null ? node.originalData[propName] : null,
            inheritData: node.inheritData != null ? node.inheritData[propName] : null,
            inheritFixedParent: propInheritFixedParent
        }, propOverride);
    };

    var propName;
    var propSchema;
    node.properties = {};
    for (propName in props) {
        propSchema = props[propName];
        if (!propSchema.title && node.schema.title && node.schema.additionalProperties)
            propSchema.title = propName;
        node.properties[propName] = createProperty(propName, propSchema);
    }
    if (node.schema.additionalProperties) {
        for (propName in node.data) {
            if (!props.hasOwnProperty(propName)) {
                propSchema = jQuery.extend(true, {}, node.schema.additionalProperties);
                propSchema.title = propName;
                propSchema._deletable = true;
                node.properties[propName] = createProperty(propName, propSchema);
            }
        }
    }
};

accfg.Accfg.prototype.createTreeArray = function (node) {
    node.itemSchema = {
        title: node.schema.itemname, // backwards compat
        description: node.schema.description,
        _deletable: true
    };
    node.itemSchema = jQuery.extend(true, node.itemSchema, node.schema.items);

    node.items = [];
    for (var i = 0; i < node.data.length; i++) {
        node.items.push(this.createTreeNodeInternal({
            parent: node,
            name: i,
            schema: node.itemSchema,
            data: node.data[i],
            originalData: node.originalData != null ? node.originalData[i] : null,
            inheritData: node.inheritData != null ? node.inheritData[i] : null,
            inheritFixedParent: true
        }));
    }
};

accfg.Accfg.prototype.createTreeElement = function (parentElement, node) {
    var schema = node.schema;
    var label = node.label;
    if (schema.type == 'alias') {
        var aliasNode = node;
        node = this.treeLookup(aliasNode, aliasNode.schema.path);
        // Rendering must use the schema from node, but with title/description from aliasNode
        schema = jQuery.extend(true, {}, node.schema);
        schema = jQuery.extend(true, schema, aliasNode.schema);
        schema.type = node.schema.type;
    }
    var nodeElement = {
        parent: node,
        parentElement: parentElement,
        schema: schema,
        label: label
    };
    if (parentElement) {
        nodeElement.path = parentElement.path.concat([node.name]);
    } else {
        nodeElement.path = [];
    }
    if (node.schema.type == 'object' || node.schema.type == 'array') {
        nodeElement.expanded = node.expanded || this.expanded(nodeElement.path);
    }
    node.elements.push(nodeElement);
    return nodeElement;
};

accfg.Accfg.prototype.treeLookup = function (node, path) {
    var i;

    path = path.split('/');
    if (path.length < 1) {
        return null;
    }

    if (path[0] === '' || path[0] === '#') {
        node = this.documentTree;
    } else if (/^\d+$/.test(path[0])) {
        var count = parseInt(path[0]);
        for (i = 0; i < count; i++) {
            node = node.parent;
            if (node == null) {
                return null;
            }
        }
    } else {
        return null;
    }

    for (i = 1; i < path.length; i++) {
        node = node.properties[path[i]];
        if (node == null) {
            return null;
        }
    }
    return node;
};

accfg.Accfg.prototype.treeObjectIsAdditional = function (node) {
    if (!node.parent) {
        return false;
    }
    if (!node.parent.schema.properties) {
        return true;
    }
    return !node.parent.schema.properties.hasOwnProperty(node.name);
};


accfg.Accfg.prototype.viewable = function (path, viewableDefault) {
    if (this.options.view != null) {
        var i = 0;
        var vp = this.options.view;
        for (; i < path.length; i++) {
            if (vp[path[i]] == null) {
                if (vp["*"] == null) {
                    break;
                } else {
                    vp = vp["*"];
                }
            } else {
                vp = vp[path[i]];
            }
        }
        /* last node's values apply downwards */
        if (vp != null && vp._viewable != 'auto')
            return vp._viewable;
    }

    return viewableDefault ? true : false;
};


accfg.Accfg.prototype.expanded = function (path) {
    if (this.options.view == null)
        return !this.options.collapsedCollapsibles;

    var i = 0;
    var ep = this.options.view;
    for (; i < path.length; i++) {
        if (ep[path[i]] == null) {
            if (ep["*"] == null) {
                break;
            } else {
                ep = ep["*"];
            }
        } else {
            ep = ep[path[i]];
        }
    }

    /* last node's values apply downwards */
    if (ep != null)
        return ep._expanded;

    return !this.options.collapsedCollapsibles;
};

accfg.Accfg.prototype.setExpanded = function (nodeElement, expanded, fromAll) {
    if (nodeElement.collapser) {
        nodeElement.expanded = expanded;
        if (expanded) {
            nodeElement.container.show();
            nodeElement.collapser.removeClass('collapsed');
        } else {
            nodeElement.container.hide();
            nodeElement.collapser.addClass('collapsed');
        }
    }
};

accfg.Accfg.prototype.setExpandedAll = function (node, clickedNode, expand) {
    if (node == null) return;
    var _inst = this;
    for (var key in node.elements) {
        if (node.elements[key].field.attr('id').indexOf(clickedNode) != -1) {
            _inst.setExpanded(node.elements[key], expand, true);
        }
    }

    if (node.properties) {
        for (var key in node.properties) {
            _inst.setExpandedAll(node.properties[key], clickedNode, expand);
        };
    }
    if (node.items) {
        node.items.forEach(function (nodeItem) {
            _inst.setExpandedAll(nodeItem, clickedNode, expand);
        });
    }
    if (node.additionalProperties) {
        for (var key in node.additionalProperties) {
            _inst.setExpandedAll(node.additionalProperties[key], clickedNode, expand);
        }
    }
};

accfg.Accfg.prototype.format = function (name) {
    var schema = this.documentSchema.properties.schema;
    if (schema === undefined || schema.type !== 'object') {
        return null;
    }

    var ret = schema.formats;
    if (ret === undefined) {
        return null;
    }

    return ret[name];
};


accfg.Accfg.prototype.clearFirstLastClass = function (nodeElement, children) {
    var _inst = this;
    nodeElement.value.children().each(function () {
        if ($(this).css('display') != 'none') {
            var childElement = $(this).data('accfg-element');
            if (childElement.field.hasClass('field')) {
                childElement.field.removeClass('first last');
                children.push(childElement);
            } else if (childElement.schema.type === 'object' || childElement.schema.type == 'array') {
                _inst.clearFirstLastClass(childElement, children);
            }
        }
    });
};

accfg.Accfg.prototype.setFirstLastClass = function (nodeElement) {
    var children = [];
    this.clearFirstLastClass(nodeElement, children);
    if (children.length > 0) {
        children[0].field.addClass('first');
        children[children.length - 1].field.addClass('last');
    }
};

accfg.Accfg.prototype.renderObject = function (nodeElement) {
    var propName;
    var node = nodeElement.parent;
    var viewable = this.viewable(nodeElement.path, node.schema.title);
    var fieldId = this.pathToFieldId(nodeElement.path);
    nodeElement.value = $(document.createElement('ul'));
    if (viewable) {
        nodeElement.value.addClass('fieldvalue-object');
    } else {
        nodeElement.value.addClass('fieldvalue-hidden');
    }
    nodeElement.value.attr('id', 'fieldvalue-' + fieldId);
    // Render all the properties defined in the schema
    var propNode;
    var propElement;
    for (propName in node.schema.properties) {
        propNode = node.properties[propName];
        propElement = this.renderField(nodeElement, propNode);
        nodeElement.value.append(propElement.field);
    }
    var showEditBar = false;
    // Now check if the object has additional properties
    if (node.schema.additionalProperties) {
        for (propName in node.properties) {
            if (!node.schema.properties.hasOwnProperty(propName)) {
                propNode = node.properties[propName];
                propElement = this.renderField(nodeElement, propNode);
                nodeElement.value.append(propElement.field);
            }
        }
        if (this.viewable(nodeElement.path.concat(['*']), node.schema.additionalProperties.title)) {
            showEditBar = true;
        }
    }
    if (!jQuery.isEmptyObject(node.properties) || (!node.readOnly && viewable && showEditBar)) {
        this.setFirstLastClass(nodeElement);
        nodeElement.container.append(nodeElement.value);
    }
    // Toolbar if needed
    if (!node.readOnly && viewable && showEditBar) {
        var editBar = $(document.createElement('div')).addClass('edit-bar object');
        var inner = $(document.createElement('small'));
        var addBtn = $(document.createElement('button')).attr('type', 'button').addClass('btn field-add property-add').text(this.tr('Add'));
        inner.append(this.tr('Add ' + node.itemLabel + ': '));
        nodeElement.add = $(document.createElement('input')).attr({
            'type': 'text',
            'id': 'fieldadd-' + fieldId,
            'placeholder': this.tr(node.itemLabel)
        }).on('keypress',function(evt){
            // If enter is pressed click on the relevant 'Add' button
            if (evt.which == 13){
                addBtn.click();
            }
        });
        inner.append(nodeElement.add);
        addBtn.data('accfg-element', nodeElement);
        inner.append(' ').append(addBtn);
        editBar.append(inner);
        nodeElement.container.append(editBar);
    }
};


accfg.Accfg.prototype.renderEnumArrayField = function (
    node,
    fieldValueId,
    schema,
    valueData)
{
    // Renders field with enum property set.
    // The field will be rendered as dropdown.
    //TODO: If not exclusive, use combo box
    var fieldValueNode = null;
    var selectedValue = null;
    var hasSelected = false;
    // First, check if there's any data provided.
    // If so, check if the enum has the same value
    // If so, save the information
    if (typeof valueData === schema.type &&
            schema.enum.indexOf(valueData) >= 0) {
        selectedValue = valueData;
        hasSelected = true;
    }
    // If there's no data provided, or the data is not valid,
    // try to get selected value from the default.
    if (!hasSelected && typeof schema['default'] === schema.type &&
            schema.enum.indexOf(schema['default']) >= 0) {
        selectedValue = schema['default'];
        hasSelected = true;
    }
    if (schema && schema.enum) {
        if (schema.enum.length > 1) {
            var optN = null;
            fieldValueNode = $(document.createElement('select')).addClass('value-input form-control').attr({
                'id': fieldValueId
            });
            if (schema.optional || !hasSelected) {
                // Add the 'null' option if the field is not required
                fieldValueNode.append('<option value=""></option>');
            }
            for (var iev = 0; iev < schema.enum.length; iev++) {
                optN = $(document.createElement('option'));
                optN.text(schema.enum[iev]);
                // Select the value
                if (hasSelected && selectedValue == schema.enum[iev]) {
                    optN.attr('selected', 'selected');
                }
                fieldValueNode.append(optN);
            }
            if (node.readOnly) {
                fieldValueNode.attr('disabled', 'disabled');
            }
        } else {
            fieldValueNode = $(document.createElement('input')).attr('type', 'text').addClass('value-input form-control').attr({
                'id': fieldValueId,
                'value': schema.enum[0],
                'readonly': 'readonly'
            });
        }
    }
    return fieldValueNode;
};


accfg.Accfg.prototype.renderEnumObjectField = function (
    node,
    fieldValueId,
    schema,
    valueData)
{
    var selectedValue = null;
    var hasSelected = false;
    var fieldValueNode = null;

    // First, check if there's any data provided.
    // If so, check if the enum has the same value
    // If so, save the information
    if (typeof valueData === schema.type && schema.enum[valueData]) {
        selectedValue = valueData;
        hasSelected = true;
    }
    // If there's no data provided, or the data is not valid,
    // try to get selected value from the default.
    if (!hasSelected && typeof schema['default'] === schema.type &&
            schema.enum[schema['default']]) {
        selectedValue = schema['default'];
        hasSelected = true;
    }

    var optN = null;
    fieldValueNode = $(document.createElement('select')).addClass('value-input form-control').attr({
        'id': fieldValueId
    });
    if (schema.optional || !hasSelected) {
        // Add the 'null' option if the field is not required
        fieldValueNode.append($(document.createElement('option')).attr('value', ''));
    }
    for (var enumName in schema.enum) {
        optN = $(document.createElement('option')).attr('value', enumName);
        if (schema.enum[enumName].title) {
            optN.text(schema.enum[enumName].title);
        } else {
            optN.text(enumName);
        }
        if (hasSelected && selectedValue == enumName) {
            optN.attr('selected', 'selected');
        }
        fieldValueNode.append(optN);
    }
    if (node.readOnly) {
        fieldValueNode.attr('disabled', 'disabled');
    }
    return fieldValueNode;
};


accfg.Accfg.prototype.renderEnumField = function (
    node,
    fieldValueId,
    schema,
    valueData)
{
    if (Array.isArray(schema.enum)) {
        return this.renderEnumArrayField(node, fieldValueId, schema, valueData);
    } else if (typeof schema.enum == 'object') {
        return this.renderEnumObjectField(node, fieldValueId, schema, valueData);
    }
    return $(document.createElement('input')).attr('type', 'text').addClass('value-input form-control').attr({
        'value': 'unknown enum type ' + typeof schema.enum,
        'readonly': 'readonly'
    });
};


accfg.Accfg.prototype.renderRef = function (fieldValueNode) {
    var nodeElement = fieldValueNode.data('accfg-element');
    var node = nodeElement.parent;
    var fieldValueId = fieldValueNode.attr('id');
    var dataRef = fieldValueNode.attr('data-ref')
    var valueData = fieldValueNode.val();

    var schema = jQuery.extend(true, { enum: {} }, node.schema);
    if (Array.isArray(node.ref)) {
        node.ref.forEach(function (ref) {
            if (ref.single) {
                schema.enum['/' + ref.node.path.join('/')] = { "title": ref.node.label };
            } else {
                var refNode = ref.node;
                for (var propName in refNode.properties) {
                    var propNode = refNode.properties[propName];
                    // Refs not being inherited can only be to fixed objects or overridden
                    // objects, otherwise changes in the parent config could cause the
                    // config to become invalid.
                    if (node.inherit === true || propNode.inherit !== true || !propNode.schema._deletable) {
                        schema.enum['/' + propNode.path.join('/')] = { "title": refNode.itemLabel + ": " + propNode.label };
                    }
                }
            }
        });
    } else {
        for (var propName in node.ref.properties) {
            var propNode = node.ref.properties[propName];
            // Refs not being inherited can only be to fixed objects or overridden
            // objects, otherwise changes in the parent config could cause the
            // config to become invalid.
            if (node.inherit === true || propNode.inherit !== true || !propNode.schema._deletable) {
                schema.enum[propName] = { "title": propNode.label };
            }
        }
    }

    nodeElement.value = this.renderEnumField(node, fieldValueId, schema, valueData);
    nodeElement.value.attr('data-ref', dataRef);
    nodeElement.value.data('accfg-element', nodeElement);
    fieldValueNode.replaceWith(nodeElement.value);
};

accfg.Accfg.prototype.renderOneRef = function (parentNode, refNode) {
    var _inst = this;
    var ref = this.pathToFieldName(refNode.path);
    parentNode.find('[data-ref~="' + ref + '"]').each(function () {
        _inst.renderRef($(this));
    });
};

accfg.Accfg.prototype.renderAllRefs = function (parentNode) {
    var _inst = this;
    parentNode.find('[data-ref]').each(function () {
        _inst.renderRef($(this));
    });
};


accfg.Accfg.prototype.renderFieldValue = function (nodeElement)
{
    var node = nodeElement.parent;
    var schema = node.schema;
    var _inst = this;
    var fieldId = this.pathToFieldId(nodeElement.path);
    var fieldValueId = 'fieldvalue-' + fieldId;
    var showContainer = null;
    if (schema.type == 'string') {
        // String property
        if (schema.enum) {
            nodeElement.value = this.renderEnumField(node, fieldValueId, schema, node.data);
        } else {
            //TODO: Format
            if (schema.multiline) {
                nodeElement.value = $(document.createElement('textarea'));
            } else if (schema.private) {
                nodeElement.value = $(document.createElement('input')).attr('type', 'password').attr('autocomplete', 'off');
                var showButton = $(document.createElement('input')).attr('type', 'checkbox').change(function () {
                    nodeElement.value.prop('type', $(this).prop('checked') ? 'text' : 'password');
                });
                var showText = $(document.createElement('span')).text('Show');
                showContainer = $(document.createElement('label')).addClass('field-show').append(showButton).append(showText);
            } else {
                nodeElement.value = $(document.createElement('input')).attr('type', 'text');
            }
            nodeElement.value.addClass('value-input form-control').attr({
                'id': fieldValueId,
                'name': this.pathToFieldName(node.path)
            });
            if (typeof node.data == 'string') {
                nodeElement.value.val(node.data);
            } else if (node.data instanceof Array) {
                nodeElement.value.val(this.fieldPrivate);
            } else if (schema['default'])
                nodeElement.value.val(schema['default']);
            if (schema.title) {
                nodeElement.value.attr('title', schema.title);
            }
            if (!schema.optional && schema['default']) {
                //TODO: Check the type
                nodeElement.value.attr('placeholder', schema['default']);
            }
            if (schema.ref) {
                var ref;
                if (Array.isArray(schema.ref)) {
                    // schema.ref is an array of paths to objects.
                    // data-ref only needs to be the paths that can vary,
                    // which is those that end in a wildcard ('*').
                    if (node.elements.length === 1) {
                        node.ref = [];
                        for (var i = 0; i < schema.ref.length; i++) {
                            var single = true;
                            var path = schema.ref[i].split('/');
                            if (path[path.length - 1] === '*') {
                                path.pop();
                                single = false;
                            }
                            node.ref.push({
                                node: _inst.treeLookup(node, path.join('/')),
                                single: single
                            });
                        }
                    }
                    ref = [];
                    for (var i = 0; i < node.ref.length; i++) {
                        if (!node.ref[i].single) {
                            ref.push(_inst.pathToFieldName(node.ref[i].node.path));
                        }
                    }
                    ref = ref.join(' ');
                } else {
                    if (node.elements.length === 1) {
                        node.ref = this.treeLookup(node, schema.ref);
                    }
                    ref = this.pathToFieldName(node.ref.path);
                }
                nodeElement.value.attr('data-ref', ref);
            }
        }
        if (node.readOnly) {
            nodeElement.value.attr('readonly', 'readonly');
        }
        nodeElement.container.append(nodeElement.value);
        if (showContainer) {
            nodeElement.container.append(showContainer);
        }
    } else if (schema.type == 'number' || schema.type == 'integer') {
        // Numeric property (number or integer)
        if (schema.enum) {
            nodeElement.value = this.renderEnumField(node, fieldValueId, schema, node.data);
        } else {
            nodeElement.value = $(document.createElement('input')).attr('type', 'number').addClass('value-input form-control').attr({
                'id': fieldValueId
            });
            if (typeof node.data == "number") {
                nodeElement.value.val(node.data);
            } else if (typeof node.data == "string") {
                if (schema.type == 'integer') {
                    nodeElement.value.val(parseInt(node.data, 10));
                } else {
                    nodeElement.value.val(parseFloat(node.data));
                }
            } else if ('default' in schema) {
                nodeElement.value.val(schema['default']);
            }
            if (schema.title) {
                nodeElement.value.attr('title', schema.title);
            }
            if (schema.type == 'integer')
                nodeElement.value.attr('step', '1');
            else
                nodeElement.value.attr('step', 'any');
            if (schema['default']) {
                //TODO: Check the type
                nodeElement.value.attr('placeholder', schema['default']);
            }
        }
        if (node.readOnly) {
            nodeElement.value.attr('readonly', 'readonly');
        }
        nodeElement.container.append(nodeElement.value);
    } else if (schema.type == 'boolean') {
        // Boolean property
        //TODO: Check box (allow value replacements/mapping)
        nodeElement.value = $(document.createElement('input')).attr('type', 'checkbox').addClass('value-input').attr({
            'id': fieldValueId
        });
        var isTrue = false;
        if (node.data !== undefined && node.data !== null) {
            isTrue = (node.data === 'on' || node.data === 'true' ||
                    node.data === 'checked' || node.data === '1' ||
                    node.data === 1 || node.data === true);
        } else if ('default' in schema) {
            isTrue = schema['default'];
        }
        if (node.readOnly) {
            nodeElement.value.attr('disabled', 'disabled');
        }
        if (schema.title) {
            nodeElement.value.attr('title', schema.title);
        }
        nodeElement.value.prop('checked', isTrue);
        nodeElement.container.append(nodeElement.value);
    } else if (schema.type == 'object') {
        this.renderObject(nodeElement);
    } else if (schema.type == 'array') {
        nodeElement.value = $(document.createElement('ol')).addClass('fieldvalue-list').attr({
            'id': fieldValueId
        });
        nodeElement.value.sortable({
            axis: "y",
            tolerance: "pointer",
            start: function (ev, ui) {
                ui.placeholder.height(ui.item.height());
                $(this).attr('data-sort-start-index', ui.item.index());
            },
            stop: function (ev, ui) {
                _inst.onMoveListItem(this, $(this).attr('data-sort-start-index'), ui.item.index());
            }
        });
        nodeElement.value.data('accfg-element', nodeElement);
        node.lastIndex = node.items.length;
        for (var i = 0; i < node.items.length; i++) {
            var itemNode = node.items[i];
            var itemElement = this.renderField(nodeElement, itemNode);
            nodeElement.value.append(itemElement.field);
        }
        this.setFirstLastClass(nodeElement);
        nodeElement.container.append(nodeElement.value);

        if (!node.readOnly && this.viewable(nodeElement.path, schema.title)) {
            var editBar = $(document.createElement('div')).addClass('edit-bar array');
            var inner = $(document.createElement('small'));

            inner.append(this.tr("Add " + node.itemSchema.title + ": "));

            var addBtn = $(document.createElement('button')).attr('type', 'button').addClass('btn field-add item-add').text(this.tr("Add"));
            addBtn.data('accfg-element', nodeElement);
            inner.append(' ').append(addBtn);
            editBar.append(inner);
            nodeElement.container.append(editBar);
        }
    } else {
        nodeElement.container.
                append(this.tr("InternalError: Unsupported property type: ")).
                append($(document.createElement('tt')).text(schema.type));
    }
    nodeElement.value.data('accfg-element', nodeElement);
    if (schema.type != 'object' && schema.type != 'array') {
        nodeElement.errormsg = $(document.createElement('div')).addClass('errormsg').hide();
        nodeElement.container.append(nodeElement.errormsg);
    }
};

accfg.Accfg.prototype.renderFieldHelp = function (nodeElement)
{
    var node = nodeElement.parent;
    var schema = nodeElement.schema;
    if (!schema.description)
        return null;

    var content = '<p><b>' + schema.title + '</b>';
    if (schema.optional) {
        content = content + ' (optional)';
    }
    content = content + ': ' + schema.description + '</p>';
    if (typeof schema.enum == 'object') {
        var options = '';
        for (var enumName in schema.enum) {
            if (schema.enum[enumName].title && schema.enum[enumName].description) {
                options = options + '<li><b>' + schema.enum[enumName].title + '</b>: ' + schema.enum[enumName].description + '</li>';
            }
        }
        if (options !== '') {
            content = content + '<p><b>Options</b>:</p><ul>' + options + '</ul>';
        }
    }
    if (schema.format) {
        var format = this.format(schema.format);
        if (format && format.syntax) {
            content = content + '<p><b>Syntax</b>: ' + format.syntax + '</p>';
        }
    }
    if ('minimum' in schema) {
        content = content + '<p><b>Minimum</b>: ' + schema.minimum + '</p>';
    }
    if ('maximum' in schema) {
        content = content + '<p><b>Maximum</b>: ' + schema.maximum + '</p>';
    }
    if (schema.type === 'string' && 'default' in schema && !schema.private) {
        var def = schema['default'];
        if (typeof schema.enum == 'object') {
            if (typeof schema.enum[def] == 'object') {
                if ('title' in schema.enum[def]) {
                    def = schema.enum[def].title;
                }
            }
        } else if (schema.ref) {
            if (Array.isArray(schema.ref)) {
                var refNode = this.treeLookup(node, def);
                if (def in schema.ref) {
                    def = refNode.label;
                } else {
                    def = refNode.parent.label + ": " + refNode.label;
                }
            } else {
                var refNode = this.treeLookup(node, node.schema.ref);
                def = refNode.properties[def].label;
            }
        }
        content = content + '<p><b>Default</b>: ' + def + '</p>';
    } else if ((schema.type === 'number' || schema.type === 'integer') && 'default' in schema) {
        content = content + '<p><b>Default</b>: ' + schema['default'] + '</p>';
    }
    return content;
};

accfg.Accfg.prototype.renderFieldActionItem = function (nodeElement, label, className)
{
    var a = $(document.createElement('a')).attr('href', '#').text(this.tr(label)).data('accfg-element', nodeElement);
    if (className) {
        a.addClass(className);
    }
    return $(document.createElement('li')).append(a);
};

accfg.Accfg.prototype.renderFieldActions = function (nodeElement)
{
    if (!nodeElement.action) {
        return;
    }

    var node = nodeElement.parent;
    var actionMenu = nodeElement.action.find('.dropdown-menu');
    actionMenu.empty();

    var helpContent = this.renderFieldHelp(nodeElement);
    if (helpContent) {
        helpPopover = {
            content: helpContent,
            html: true,
            container: 'body',
            placement: 'right',
            trigger: 'hover'
        };
        actionMenu.append(this.renderFieldActionItem(nodeElement, 'Help').popover(helpPopover));
    }

    if (!node.schema.readonly && !this.options.readOnly) {
        if (node.inherit !== true) {
            if (node.originalData != null) {
                actionMenu.append(this.renderFieldActionItem(nodeElement, 'Set to original', 'field-setoriginal'));
            }

            actionMenu.append(this.renderFieldActionItem(nodeElement, 'Set to default', 'field-setdefault'));
        }
        if (!node.inheritFixed) {
            if (node.inherit !== false) {
                actionMenu.append(this.renderFieldActionItem(nodeElement, 'Override', 'field-override'));
            }
            if (node.inherit !== true) {
                actionMenu.append(this.renderFieldActionItem(nodeElement, 'Inherit', 'field-inherit'));
            }
        }
        if (node.inherit !== true && node.inheritFixed) {
            if (node.schema._deletable) {
                actionMenu.append(this.renderFieldActionItem(nodeElement, 'Delete', 'field-delete'));
            }
        }
    }

    if (nodeElement.schema.format == '2fa-secret') {
        // Menu item to generate a 128-bit base32 encoded string for Two-factor Authentication Secret key
        var _inst = this;
        actionMenu.append(this.renderFieldActionItem(nodeElement, 'Generate secret key').click(function() {
            var secret = '';
            var bits = 128;
            var bitsPerChar = 5;
            var charMap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
            var n = Math.floor(bits/bitsPerChar);
            for (var i = 0; i < n; i++) {
                secret += charMap[Math.floor(Math.random()*charMap.length)];
            }
            var leftover = bits - n*bitsPerChar;
            if (leftover) {
                secret += charMap[Math.floor(Math.random()*Math.pow(2, leftover))];
            }
            nodeElement.value.val(secret);
            _inst.validateDataNow();
        }));

        // A type (totp/hotp) is required to insert into the QR code
        var typeElement = this.treeLookup(node, '1/type');
        if (typeElement) {
            var userElement = this.treeLookup(node, '2');
            actionMenu.append(this.renderFieldActionItem(nodeElement, 'Show secret key QR code').click(function() {
                var secret = nodeElement.value.val();
                var type = typeElement.elements[0].value.val();
                var size = 300;

                // WebUI: <user>@<mac> issuer=AcceleratedConcepts <model>
                // AView: <user>@<configuration name> issuer=<organisation> <model>
                var desc = userElement ? userElement.name : 'user';
                var s = $('#index_mac').text() || $('#device_configuration_name').val() || $('#name').val();
                if (s) desc += '@' + s;
                var issuer = $('#select2-device_configuration_organization_id-container').text()
                        || $('#organization_id').text() || 'AcceleratedConcepts';
                s = $('#index_model').text() || $('#category').val() || $('#model').val();
                if (s) issuer += ' ' + s;

                var url = 'https://www.google.com/chart?chs=' + size + 'x' + size + '&chld=M|0&cht=qr&chl=otpauth://'
                        + type + '/' + desc + '%3Fsecret%3D' + secret + '%26issuer%3D' + issuer;
                var alt = 'If the QR code does not load, please record the secret key manually.';
                $('<div><img src="' + url + '" alt="' + alt + '"></div>').dialog({
                    title: 'Two-factor Authentication',
                    modal: false,
                    minWidth: size,
                    minHeight: size,
                    buttons: {
                        Close: function() {
                            $(this).dialog('close');
                        }
                    }
                }).parent()
                    .css('background', '#fefefe')
                    // Get rid of the close button in the titlebar that doesn't show properly with current icons/CSS
                    .find('.ui-dialog-titlebar-close').remove();
            }));
        }
    }

    if (actionMenu[0].childElementCount) {
        nodeElement.action.show();
    } else {
        nodeElement.action.hide();
    }
};

accfg.Accfg.prototype.renderField = function (parentElement, origNode)
{
    var _inst = this;
    var nodeElement = origNode != null ? this.createTreeElement(parentElement, origNode) : parentElement;
    var node = nodeElement.parent;
    var schema = nodeElement.schema;
    var label = nodeElement.label;
    var idPath = nodeElement.path;
    var fieldId = this.pathToFieldId(idPath);
    if (!this.viewable(idPath, schema.title)) {
        nodeElement.field = nodeElement.field || $(document.createElement('li')).attr({
            'id': 'field-' + fieldId
        });
        nodeElement.field.data('accfg-element', nodeElement);
        nodeElement.container = $(document.createElement('span'));
        nodeElement.field.append(nodeElement.container);
        this.renderFieldValue(nodeElement);
        if (schema.type != 'object') {
            nodeElement.field.addClass('field');
            nodeElement.field.hide();
        }
        if (schema.depend && node.elements.length === 1) {
            this.treeLookup(node, schema.depend).dependents.push(node);
        }
        return nodeElement;
    }
    nodeElement.field = nodeElement.field || $(document.createElement('li')).addClass('field').addClass(schema.type).attr({
        'id': 'field-' + fieldId
    });
    nodeElement.field.data('accfg-element', nodeElement);

    if (node.elements.length === 1 && schema.depend) {
        this.treeLookup(node, schema.depend).dependents.push(node);
    }
    var labelN = $(document.createElement('label')).addClass('field-name').attr('for', 'fieldvalue-' + fieldId);
    if (schema.type == 'object' || schema.type == 'array') {
        // Some special treatments for collapsible field
        nodeElement.collapser = $(document.createElement('div')).addClass('collapser');
        nodeElement.collapser.data('accfg-element', nodeElement);
        nodeElement.collapser.append(labelN);
        nodeElement.errormsg = $(document.createElement('div')).addClass('errormsg').hide();
        nodeElement.collapser.append(nodeElement.errormsg);
        nodeElement.field.append(nodeElement.collapser).addClass('collapsible');

        nodeElement.container = $(document.createElement('div')).addClass('collapsible-panel');
        nodeElement.field.append(nodeElement.container);

        if (!nodeElement.expanded) {
            nodeElement.container.hide();
            nodeElement.collapser.addClass('collapsed');
        }
    } else {
        nodeElement.field.addClass('field-single');
        nodeElement.field.append(labelN);
        nodeElement.container = $(document.createElement('span')).addClass('value');
        nodeElement.field.append(nodeElement.container);
    }

    nodeElement.action = $(document.createElement('span')).addClass('field-dropdown dropdown').
        append($(document.createElement('button')).attr('type', 'button').addClass('btn dropdown-toggle').attr('data-toggle', 'dropdown').append($(document.createElement('span')).addClass('caret'))).
        append($(document.createElement('ul')).addClass('dropdown-menu'));
    labelN.after(nodeElement.action);

    this.renderFieldActions(nodeElement);
    this.renderFieldValue(nodeElement);

    if (schema.titleRef) {
        // Changes to the titleRefs trigger updates to this node's label.
        var refs = typeof schema.titleRef === 'string' ? [ schema.titleRef ] : schema.titleRef;
        refs.forEach(function(ref) {
            var titleNode = _inst.treeLookup(node, ref);
            if (titleNode) {
                titleNode.dependentLabels.push(nodeElement);
            }
        });
    }

    nodeElement.labelElement = labelN;
    this.syncLabel(nodeElement);

    return nodeElement;
};

accfg.Accfg.prototype.syncLabel = function (nodeElement) {
    var _inst = this;
    var node = nodeElement.parent;
    var schema = nodeElement.schema;
    var label = nodeElement.label;

    if (schema.titleRef) {
        // Override the label with the first non-empty node contents.
        var refs = typeof schema.titleRef === 'string' ? [ schema.titleRef ] : schema.titleRef;
        for (var i in refs) {
            var titleNode = _inst.treeLookup(node, refs[i]);
            var titleVal;
            if (titleNode && (titleVal = titleNode.elements[0].value.val())) {
                label = titleVal;
                break;
            }
        }
    }

    nodeElement.labelElement.text(label);
}

accfg.Accfg.prototype.syncLabels = function (sourceNodeElement) {
    var _inst = this;
    var node = sourceNodeElement.parent;
    node.dependentLabels.forEach(function(nodeElement) {
        _inst.syncLabel(nodeElement);
    });
};

accfg.Accfg.prototype.syncAlias = function (sourceNodeElement) {
    var node = sourceNodeElement.parent;

    if (sourceNodeElement.value.is('input:checkbox')) {
        var checked = sourceNodeElement.value.prop('checked');
        node.elements.forEach(function (nodeElement) {
            nodeElement.value.prop('checked', checked);
        });
    } else {
        var val = sourceNodeElement.value.val();
        node.elements.forEach(function (nodeElement) {
            if (nodeElement !== sourceNodeElement && nodeElement.value.val() !== val) {
                nodeElement.value.val(val);
            }
        });
    }
};

accfg.Accfg.prototype.syncDepend = function (node) {
    var _inst = this;
    if (this.testDepend(node)) {
        node.elements.forEach(function (nodeElement) {
            if (_inst.viewable(nodeElement.path, nodeElement.schema.title || nodeElement.schema.type == "object")) {
                nodeElement.field.show();
                _inst.setFirstLastClass(nodeElement.parentElement);
            }
        });
    } else {
        node.elements.forEach(function (nodeElement) {
            _inst.setFirstLastClass(nodeElement.parentElement);
            nodeElement.field.hide();
        });
    }
};

accfg.Accfg.prototype.syncOneDepend = function (nodeElement) {
    var _inst = this;
    var node = nodeElement.parent;
    node.dependents.forEach(function (dependentNode) {
        _inst.syncOneDepend(dependentNode.elements[0]);
        _inst.syncDepend(dependentNode);
    });
};

accfg.Accfg.prototype.syncAllDepends = function (node) {
    var _inst = this;
    node.dependents.forEach(function (dependentNode) {
        _inst.syncDepend(dependentNode);
    });
    if (node.schema.type == 'object') {
        for (var propName in node.properties) {
            this.syncAllDepends(node.properties[propName]);
        }
    } else if (node.schema.type == 'array') {
        for (var i = 0; i < node.items.length; i++) {
            this.syncAllDepends(node.items[i]);
        }
    }
};

accfg.Accfg.prototype.onAddObjectProperty = function (handle) {
    var _inst = this;
    var handleNodeElement = handle.data('accfg-element');
    var node = handleNodeElement.parent;
    var propName = handleNodeElement.add.val();
    if (!propName) {
        //TODO: Nice [unobstrusive] error message
        alert("Property name must not be empty");
        return;
    }
    if (!propName.match(/^[a-z0-9_]+$/i)) {
        //TODO: Nice [unobstrusive] error message
        alert("Invalid property name");
        return;
    }
    if (propName in node.properties) {
        //TODO: Nice [unobstrusive] error message
        alert("Property name already exists");
        return;
    }
    handleNodeElement.add.val('');

    var propSchema = jQuery.extend(true, {}, node.schema.additionalProperties);
    propSchema.title = propName;
    propSchema._deletable = true;
    var propNode = this.createTreeNode({
        parent: node,
        name: propName,
        schema: propSchema
    });
    propNode.expanded = true;
    node.properties[propName] = propNode;
    node.elements.forEach(function (nodeElement) {
        var propElement = _inst.renderField(nodeElement, propNode);
        nodeElement.value.append(propElement.field);
        _inst.setFirstLastClass(nodeElement);
        propElement.field.hide();
        propElement.field.fadeIn('fast', function () {
            if (nodeElement === handleNodeElement) {
                propElement.field.find('input').first().focus();
            }
        });
        _inst.renderAllRefs(propElement.field);
    });
    this.renderOneRef(this.panelElement, node);
    _inst.syncAllDepends(propNode);
    this.validateDataNow();
};


accfg.Accfg.prototype.onAddListItem = function (handle) {
    var _inst = this;
    var handleNodeElement = handle.data('accfg-element');
    var node = handleNodeElement.parent;
    node.lastIndex++;
    var itemNode = this.createTreeNode({
        parent: node,
        name: node.lastIndex,
        schema: node.itemSchema
    });
    itemNode.expanded = true;
    node.items.push(itemNode);
    node.elements.forEach(function (nodeElement) {
        var itemElement = _inst.renderField(nodeElement, itemNode);
        nodeElement.value.append(itemElement.field);
        _inst.setFirstLastClass(nodeElement);
        itemElement.field.hide();
        itemElement.field.fadeIn('fast', function () {
            if (nodeElement === handleNodeElement) {
                itemElement.field.find('input').first().focus();
            }
        });
        _inst.renderAllRefs(itemElement.field);
    });
    _inst.syncAllDepends(itemNode);
    this.validateDataNow();
};


accfg.Accfg.prototype.onMoveListItem = function (handle, oldIndex, newIndex) {
    var _inst = this;
    var handleNodeElement = $(handle).data('accfg-element');
    var node = handleNodeElement.parent;
    var itemNode = node.items[oldIndex];
    node.items.splice(oldIndex, 1);
    node.items.splice(newIndex, 0, itemNode);
    node.elements.forEach(function (nodeElement) {
        if (nodeElement != handleNodeElement) {
            var oldChild = nodeElement.value.children()[oldIndex];
            var newChild = nodeElement.value.children()[newIndex];
            if (oldIndex < newIndex) {
                $(newChild).after(oldChild);
            } else {
                $(newChild).before(oldChild);
            }
        }
        _inst.setFirstLastClass(nodeElement);
    });
};


accfg.Accfg.prototype.onDeleteField = function (handle) {
    var propNode = handle.data('accfg-element').parent;
    var node = propNode.parent;
    if (node.schema.type == 'object') {
        for (var propName in node.properties) {
            if (node.properties[propName] === propNode) {
                delete node.properties[propName];
                break;
            }
        }
    } else if (node.schema.type == 'array') {
        for (var i = 0; i < node.items.length; i++) {
            if (node.items[i] === propNode) {
                node.items.splice(i, 1);
                break;
            }
        }
    }
    var _inst = this;
    propNode.elements.forEach(function (propElement) {
        propElement.field.fadeOut('fast', function () {
            $(this).remove();
            _inst.setFirstLastClass(propElement.parentElement);
        });
    });
    this.renderOneRef(_inst.panelElement, node);
    this.validateDataNow();
};

accfg.Accfg.prototype.setData = function (node, data) {
    var newNode = this.createTreeNode({
        parent: node.parent,
        name: node.name,
        schema: node.schema,
        data: data,
        originalData: node.originalData,
        inheritData: node.inheritData,
        inheritFixedParent: node.inheritFixedParent
    });
    if (node.schema.type === 'object') {
        node.properties = newNode.properties;
        for (propName in node.properties) {
            node.properties[propName].parent = node;
        }
    } else if (node.schema.type === 'array') {
        node.items = newNode.items;
        for (var i = 0; i < node.items.length; i++) {
            node.items[i].parent = node;
        }
    } else {
        node.data = data;
    }
};

accfg.Accfg.prototype.setInherit = function (node, inherit) {
    var _inst = this;

    var data;
    if (inherit) {
        data = node.inheritData;
    } else {
        data = this.buildProperty(node);
    }

    this.setData(node, data);
    node.overrideAll = !inherit;
    this.setInheritInternal(node, inherit);
    this.setTreeNodeFlags(node);
    this.renderUpdate(node);

    changedNodes = this.setTreeNodeFlags(this.documentTree);
    changedNodes.forEach(function (changedNode) {
        _inst.renderUpdate(changedNode);
    });

    if (node.schema.type === 'object') {
        this.renderAllRefs(this.panelElement);
    }
    this.validateDataNow();
};

accfg.Accfg.prototype.setInheritInternal = function (node, inherit) {
    if (node.inherit !== null) {
        node.inherit = inherit;
    }
    if (node.schema.type === 'object') {
        for (propName in node.properties) {
            this.setInheritInternal(node.properties[propName], inherit);
        }
    } else if (node.schema.type === 'array') {
        for (var i = 0; i < node.items.length; i++) {
            this.setInheritInternal(node.items[i], inherit);
        }
    } else if (node.schema.type === 'alias') {
        var aliasNode = this.treeLookup(node, node.schema.path);
        this.setInheritInternal(aliasNode, inherit);
    }
};

accfg.Accfg.prototype.renderUpdate = function (node) {
    var _inst = this;
    node.elements.forEach(function (nodeElement) {
        nodeElement.field.empty();
        _inst.renderField(nodeElement);
        _inst.renderAllRefs(nodeElement.field);
        _inst.syncLabels(nodeElement);
    });
    this.syncAllDepends(node);
};

accfg.Accfg.prototype.onSetDefaultField = function (handle) {
    var node = handle.data('accfg-element').parent;
    var val;
    // Prefer using the parent's version of the default value.
    // This lets "Set to default" on array items to set the value to the
    // array's default value not the array-item's default value.
    if (node.parent.schema && node.parent.schema.default
            && node.name in node.parent.schema.default) {
        val = node.parent.schema.default[node.name];
    } else {
        val = node.schema.default;
    }
    this.setData(node, this.configurationUtils.schemaDefault(node.schema, val));
    this.renderUpdate(node);
    this.validateDataNow();
};

accfg.Accfg.prototype.onSetOriginalField = function (handle) {
    var node = handle.data('accfg-element').parent;
    if (node.originalData != null) {
        this.setData(node, node.originalData);
        this.renderUpdate(node);
        this.validateDataNow();
    }
};

accfg.Accfg.prototype.onInheritField = function (handle) {
    var node = handle.data('accfg-element').parent;
    this.setInherit(node, true);
};

accfg.Accfg.prototype.onOverrideField = function (handle) {
    var node = handle.data('accfg-element').parent;
    this.setInherit(node, false);
};

accfg.Accfg.prototype._generateFieldId = function () {
    return 'f' + Math.floor(Math.random() * 1000000);
};

accfg.Accfg.prototype.pathToFieldId = function (path) {
    var id = [this.instanceId].concat(path).join('-');
    //TODO: Replace all other invalid characters for HTML element ID.
    id = id.replace(/\+/g, '_');
    return id;
};

accfg.Accfg.prototype.pathToFieldName = function (path) {
    return [this.instanceId].concat(path).join(this.fieldNamespaceSeparator);
};

accfg.Accfg.prototype.testDepend = function (node) {
    if (!node.schema.depend) {
        return true;
    }

    var dependNode = this.treeLookup(node, node.schema.depend);
    if (!this.testDepend(dependNode)) {
        return false;
    }
    var dependVal;
    if (dependNode.schema.type === 'boolean') {
        dependVal = dependNode.elements[0].value.prop('checked');
    } else {
        dependVal = dependNode.elements[0].value.val();
    }

    if (Array.isArray(node.schema.dependVal)) {
      for (var i = 0; i < node.schema.dependVal.length; i++) {
        if (dependVal === node.schema.dependVal[i]) {
          return true;
        }
      }
      return false;
    } else {
      return dependVal === node.schema.dependVal;
    }
};

accfg.Accfg.prototype.testEnable = function (node) {
    if (!node.schema.enable) {
        return true;
    }

    var enableNode = this.treeLookup(node, node.schema.enable);
    var enableVal = enableNode.elements[0].value.prop('checked');
    return enableVal;
};

accfg.Accfg.prototype.buildField = function (nodeElement) {
    var node = nodeElement.parent;
    var schema = node.schema;
    if (schema.type == 'boolean') {
        return nodeElement.value.prop('checked');
    }
    var data = nodeElement.value.val();
    if (data !== '') {
        if (schema.type == 'integer' || schema.type == 'number') {
            if (schema.type == 'integer') {
                return parseInt(data, 10);
            } else {
                return parseFloat(data);
            }
        } else if (schema.type == 'string') {
            if (data == this.fieldPrivate) {
                return node.data;
            } else {
                if (!this.validateOnly && schema.hash) {
                    if (!data.startsWith('$2a$')) {
                        data = this.bcrypt.hashpw(data, this.bcrypt.gensalt(5));
                    }
                }
                return data;
            }
        } else {
            return null;
        }
    } else {
        if (!schema.optional && this.testEnable(node)) {
            if ('default' in schema) {
                return schema['default'];
            } else {
                return null;
            }
        } else if (schema.type == 'string') {
            return '';
        } else {
            return null;
        }
    }
};

accfg.Accfg.prototype.buildProperty = function (node) {
    var schema = node.schema;

    if (schema.type == 'alias') {
        /* aliases point themselves at the real object, so don't return data.
         * We still need an error count though. */
        var aliasNode = this.treeLookup(node, schema.path);
        this.buildProperty(aliasNode);
        return null;
    }

    if (!this.testDepend(node)) {
        return this.configurationUtils.schemaDefault(schema, schema['default']);
    }

    if (schema.type == 'object') {
        return this.buildObject(node);
    } else if (schema.type == 'array') {
        return this.buildArray(node);
    } else {
        return this.buildField(node.elements[0]);
    }
};


accfg.Accfg.prototype.buildObject = function (node) {
    var data = {};
    for (var propName in node.properties) {
        var cRes = this.buildProperty(node.properties[propName]);
        if (cRes != null) {
            data[propName] = cRes;
        }
    }
    return data;
};


accfg.Accfg.prototype.buildArray = function (node) {
    var data = [];
    for (var i = 0; i < node.items.length; i++) {
        data.push(this.buildProperty(node.items[i]));
    }
    return data;
};

accfg.Accfg.prototype.buildOverride = function (node) {
    if (node.schema.type === 'object') {
        if (this.treeObjectIsAdditional(node)) {
            return !node.inherit;
        }
        if (node.inherit !== null) {
            return !node.inherit;
        }
        var override = {};
        var overrideAll = node.overrideAll;
        var propOverride;
        var propName;
        for (propName in node.properties) {
            propOverride = this.buildOverride(node.properties[propName]);
            if (propOverride === true) {
                override[propName] = propOverride;
            } else if (propOverride === false) {
                overrideAll = false;
            } else {
                override[propName] = propOverride;
                overrideAll = false;
            }
        }
        if (jQuery.isEmptyObject(override)) {
            return false;
        } else if (overrideAll) {
            return true;
        } else {
            return override;
        }
    } else if (node.schema.type === 'alias') {
        return false;
    } else {
        return !node.inherit;
    }
};

accfg.Accfg.prototype.getData = function () {
    var _inst = this;
    this.panelElement.find('.error').removeClass('error');
    this.panelElement.find('.errormsg').hide();
    var rc = {};
    rc.data = this.buildObject(this.documentTree);
    var validator = new Validator(this.documentSchema);
    rc.errorCount = validator.validate(rc.data, function (path, msg) {
        var node = _inst.documentTree;
        for (i = 0; node != null && i < path.length; i++) {
            if (node.schema.type == 'object') {
                node = node.properties[path[i]];
            } else if (node.schema.type == 'array') {
                node = node.items[path[i]];
            }
        }
        if (node != null) {
            _inst.setError(node);
            node.elements.forEach(function (nodeElement) {
                nodeElement.errormsg.html(msg).show();
            });
        }
    });
    rc.data = this.configurationUtils.removeDefaults(this.documentSchema, rc.data);
    if (this.options.parentData != null) {
        rc.data.schema.override = this.buildOverride(this.documentTree);
    }
    return rc;
};

accfg.Accfg.prototype.setError = function (node) {
    var _inst = this;
    if (node != null) {
        node.elements.forEach(function (nodeElement) {
            if (nodeElement.field != null) {
                nodeElement.field.addClass('error');
                nodeElement.value.addClass('error');
            }
            if (_inst.validateExpand || !_inst.validateOnly) {
                _inst.setExpanded(nodeElement, true);
            }
            _inst.setError(nodeElement.parent.parent);
        });
    }
};

accfg.Accfg.prototype.validateDataNow = function () {
    clearTimeout(this.validateTimer);
    this.validateTimer = null;
    this.validateOnly = true;
    this.getData();
    this.validateOnly = false;
    this.validateExpand = false;
};

accfg.Accfg.prototype.validateDataLater = function () {
    var _inst = this;
    clearTimeout(this.validateTimer);
    this.validateTimer = setTimeout(function () {
        _inst.validateDataNow();
    }, 500);
};


accfg.Accfg.prototype.tr = function (text) {
    // Translations go here
    return text;
};
