function initColumnVisibility(){
  var panel = $('#column-visibility');

  if ( panel.length > 0 ) {
    var tableAPI = $('.dataTable').dataTable().api();
    var columnVisibility = getColumnVisibility(tableAPI);
    initVisibilityToggles(columnVisibility, panel, tableAPI);
  }
}

function addVisibilityListeners(panel, tableAPI) {
  panel.find('.toggle-vis').on('ifChecked', function() {
    tableAPI.column($(this).attr('value') + ':name').visible(true);
  });

  panel.find('.toggle-vis').on('ifUnchecked', function() {
    tableAPI.column($(this).attr('value') + ':name').visible(false);
  });
}

function getColumnVisibility(tableAPI){
  var columnVisibility = {};
  tableAPI.columns().flatten().each(function(i) {
    var column = tableAPI.column(i);
    var name = $(column.header()).data('name');
    if (name) { columnVisibility[name] = column.visible(); }
  });
  return columnVisibility;
}

function initVisibilityToggles(columnVisibility, panel, tableAPI) {
  setVisibilityToggles(columnVisibility, panel);
  addVisibilityListeners(panel, tableAPI);
}

function setVisibilityToggles(columnVisibility, panel) {
  panel.find('.toggle-vis').each( function(i, e) {
    if (!columnVisibility[e.value]) { $(e).iCheck('uncheck'); }
  });
}
