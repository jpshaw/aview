$(document).ready(function() {
  setVsListener();
});

function setVsListener(){
  $(document).on('vs:search', function(e, query) {
    var queryFacets = query.facets;
    setMapWithSearch(queryFacets);
  });
}

function setMapWithSearch(queryFacets) {
  var vsFilter = buildFilter(queryFacets);
  $.ajax({
    type: "GET",
    url: "/maps.js",
    dataType: 'script',
    data: { filter: vsFilter }
  });
}

function buildFilter(queryFacets) {
  var vsFilter = {};
  $.each(queryFacets, function(i, facet){
    appendToFilter(vsFilter, facet);
  });
  return vsFilter;
}

function appendToFilter(vsFilter, facet) {
  var filterValue = getFilterValue(facet);
  if(facetIsMultiValue(facet)) {
    appendToValueArray(vsFilter, facet, filterValue);
  } else {
    vsFilter[facet.scope] = filterValue;
  }
}

// Gets the actual value of the option selected (e.g. the category id)
function getFilterValue(facet){
  var vsTargetWrapper = $('.vs-target-wrapper');
  var facetModels = vsTargetWrapper.data('facets');
  var filterValue = facetModelHasValues(facet, facetModels) ? lookupFacetValue(facet, facetModels) : facet.value;
  return filterValue;
}

function facetModelHasValues(facet, facetModels) {
  return facetModels[facet.name].facet_values !== '';
}

function lookupFacetValue(facet, facetModels) {
  return facetModels[facet.name].facet_values[facet.value];
}

function facetIsMultiValue(facet) {
  return facet.scope === 'categories' || facet.scope === 'models'
}

function appendToValueArray(vsFilter, facet, filterValue) {
  if(vsFilter.hasOwnProperty(facet.scope)){
    vsFilter[facet.scope].push(filterValue);
  } else {
    vsFilter[facet.scope] = [filterValue];
  }
}
