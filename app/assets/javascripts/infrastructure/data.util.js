AV.UTIL.DATA = {

  clone: function(data) {
    return JSON.parse(JSON.stringify(data));
  },

  parseBool: function(value) {
    if (value && typeof value === 'string') {
      if (value.toLowerCase() === 'true') return true;
      if (value.toLowerCase() === 'false') return false;
    }
    return value;
  }
};
