AV.UTIL.AJAX = {
  makeRequest: function(method, url, data, success, failure){
    var params = {
      type: method,
      url: url,
      contentType: "application/json",
      data: JSON.stringify(data)
    };
    var def = $.ajax(params)
      .done(success);
    if(failure){
      def.fail(failure);
    }
  },

  delete: function(url, data, callback, failure) {
    this.makeRequest("DELETE", url, data, callback, failure);
  },

  get: function(url, data, callback) {
    return $.getJSON(url, data, callback);
  },

  post: function(url, data, callback, failure) {
    this.makeRequest("POST", url, data, callback, failure);
  },

  patch: function(url, data, callback, failure) {
    this.makeRequest('PATCH', url, data, callback, failure);
  },

  put: function(url, data, callback, failure) {
    this.makeRequest("PUT", url, data, callback, failure);
  }
};
