(function repair_lodash_aliases_for_VisualSearch_Underscore_dependencies() {
  _.all = _.every;
  _.any = _.some;
  _.backflow = _.flowRight;
  _.callback = _.iteratee;
  _.collect = _.map;
  _.compose = _.flowRight;
  _.contains = _.includes;
  _.detect = _.find;
  _.foldl = _.reduce;
  _.foldr = _.reduceRight;
  _.include = _.includes;
  _.inject = _.reduce;
  _.methods = _.functions;
  _.select = _.filter;
  _.unique = _.uniq;
}());
