function extend(){
  for(var i=1; i<arguments.length; i++){
    for(var key in arguments[i]){
      if(arguments[i].hasOwnProperty(key)) { 
        if (typeof arguments[0][key] === 'object'
          && typeof arguments[i][key] === 'object')
          extend(arguments[0][key], arguments[i][key]);
        else
         arguments[0][key] = arguments[i][key];
       }
    }
  }
  return arguments[0];
}

function compileConfigurationSettings(data, parentData, documentSchema, firmwareVersion) {
  var migrator = new Migrator(documentSchema);
  var configurationUtils = new ConfigurationUtils();

  migrator.migrate(data);

  data = configurationUtils.schemaDefault(documentSchema, data);
  var override = null;
  if (parentData != null) {
    migrator.migrate(parentData);
    var inheritData = configurationUtils.schemaDefault(documentSchema, parentData);
    if (data.hasOwnProperty('schema')) {
      if (data.schema.hasOwnProperty('override')) {
        override = data.schema.override;
      }
    }
    data = configurationUtils.merge(documentSchema, inheritData, data, override);
  }
  data = configurationUtils.removeDefaults(documentSchema, data);
  if (!data.hasOwnProperty('firmware')) {
    data.firmware = {};
  }
  data.firmware.version = firmwareVersion;

  return JSON.stringify(data);
}

var Migrator = function(documentSchema) {
  this.migrate = function (data) {
    var schema = documentSchema.properties.schema;
    if (schema === undefined || schema.type !== 'object') {
      return;
    }

    var migrate = schema.migrate;
    var schemaVersion = null;
    if (Array.isArray(migrate)) {
      schemaVersion = migrate[migrate.length - 1].version;
    } else {
      for (schemaVersion in migrate);
    }

    if (!data.hasOwnProperty('schema')) {
      data.schema = {};
    }
    if (!data.schema.hasOwnProperty('version')) {
      data.schema.version = schemaVersion;
      return;
    }
    var documentVersion = data.schema.version;
    var override = data.schema.override;

    var found;
    if (Array.isArray(migrate)) {
      found = false;
      for (var i = 0; i < migrate.length; i++) {
        if (found) {
          migrateApply(data, override, migrate[i]);
        } else if (migrate[i].version === documentVersion) {
          found = true;
        }
      }
    } else {
      found = false;
      for (var version in migrate) {
        if (found) {
          migrateApply(data, override, migrate[version]);
        } else if (version === documentVersion) {
          found = true;
        }
      }
    }

    data.schema.version = schemaVersion;
  };

  function migrateApply(rootNode, override, migrate) {
    var action = migrate.action;
    if (action === 'rename') {
      migrateRename(rootNode, migrate);
      if (override != null) {
        migrateRename(override, migrate);
      }
    } else if (action === 'replace' || action == 'delete') {
      migrateDelete(rootNode, migrate);
      if (override != null) {
        migrateDelete(override, migrate);
      }
    } else if (action === 'default') {
      migrateDefault(rootNode, migrate);
    } else if (action === 'ref') {
      migrateRef(rootNode, migrate);
    } else if (action === 'function') {
      migrateFunction(rootNode, migrate);
    }
  }

  function migrateRename(rootNode, migrate) {
    var from = migrate.from.split('/');

    if (from[0] === '' || from[0] === '#') {
      /* The renamed field may have refs to (within) it.
       * Replace the old path in the refs with the new path. */
      for (var i in migrate.refs) {
        var ref = migrate.refs[i].split('/');
        if (ref[0] === '' || ref[0] === '#') {
          migrateRenameRef(rootNode, ref.slice(1), migrate.from, migrate.to);
        }
      }

      migrateRenameFrom(rootNode, rootNode, [], from.slice(1), migrate.to);
    }
  }

  function migrateRenameRef(node, path, fromPrefix, toPrefix) {
    var key;
    for (var i = 0; i < path.length - 1; i++) {
      if (path[i] === '*') {
        for (key in node) {
          migrateRenameRef(node[key], path.slice(i + 1), fromPrefix, toPrefix);
        }
        return;
      }
      node = node[path[i]];
      if (node === undefined)
        return;
    }

    if (path[i] === '*') {
      for (key in node) {
        migrateRenameRef(node, [ key ], fromPrefix, toPrefix)
      }
    } else if (node[path[i]].startsWith(fromPrefix)) {
      node[path[i]] = toPrefix + node[path[i]].slice(fromPrefix.length);
    }
  }


  function migrateRenameFrom(rootNode, node, path, from, to) {
    var key;
    for (var i = 0; i < from.length - 1; i++) {
      if (from[i] === '*') {
        path = path.concat(from.slice(0, i));
        for (key in node) {
          migrateRenameFrom(rootNode, node[key], path.concat([key]), from.slice(i + 1), to);
        }
        return;
      }
      node = node[from[i]];
      if (node === undefined)
        return;
    }

    var val = node[from[i]];
    if (val === undefined)
      return;

    delete node[from[i]];

    path = path.concat(from);
    path = resolvePath(path, to);
    migrateRenameTo(rootNode, val, path);
  }

  function resolvePath(base, path) {
    path = path.split('/');
    if (path.length < 1) {
      return null;
    }
    var result;
    if (path[0] === '' || path[0] === '#') {
      result = [];
    } else if (/^\d+$/.test(path[0])) {
      var count = parseInt(path[0]);
      result = base.slice(0, base.length - count);
    } else {
      return null;
    }
    return result.concat(path.slice(1));
  }

  function migrateRenameTo(node, val, to) {
    for (var i = 0; i < to.length - 1; i++) {
      var next = node[to[i]];
      var array = to[i+1].match(/<.*>/);

      if (next === undefined) {
        if (array) {
          next = [];
          node[to[i]] = next;
        } else {
          next = {};
          node[to[i]] = next;
        }
      }

      if (array) {
        if (to[i+1] === '<insert>') {
          next.splice(0, 0, undefined);
          to[i+1] = '0';
        } else if (to[i+1] === '<append>') {
          next.push(undefined);
          to[i+1] = '' + (next.length - 1);
        } else if (to[i+1] === '<first>') {
          to[i+1] = '0';
        } else if (to[i+1] === '<last>') {
          to[i+1] = '' + (next.length - 1);
        } else {
          to[i+1] = to[i+1].replace(/[<>]/g, '');
          if ( to[i+1] < 0 || to[i+1] > node[to[i]].length)
            return;
        }
      }

      node = next;
    }
    node[to[i]] = val;
  }

  function migrateDelete(rootNode, migrate) {
    var path = migrate.path.split('/');
    if (path[0] === '' || path[0] === '#') {
      migrateDeletePath(rootNode, path.slice(1));
    }
  }


  function migrateDeletePath(node, path) {
    var key;
    for (var i = 0; i < path.length - 1; i++) {
      if (path[i] === '*') {
        for (key in node) {
          migrateDeletePath(node[key], path.slice(i + 1));
        }
        return;
      }
      node = node[path[i]];
      if (node === undefined)
        return;
    }

    if (path[i] === '*') {
      for (key in node) {
        delete node[key];
      }
    } else {
      delete node[path[i]];
    }
  }


  function migrateDefault(rootNode, migrate) {
    var path = migrate.path.split('/');
    if (path[0] === '' || path[0] === '#') {
      migrateDefaultPath(rootNode, path.slice(1), migrate.from);
    }
  }


  function migrateDefaultPath(node, path, from) {
    var key;
    for (var i = 0; i < path.length - 1; i++) {
      if (path[i] === '*') {
        for (key in node) {
          migrateDefaultPath(node[key], path.slice(i + 1), from);
        }
        return;
      }
      var next = node[path[i]];
      if (next === undefined) {
        next = {};
        node[path[i]] = next;
      }
      node = next;
    }

    if (node[path[i]] === undefined)
      node[path[i]] = from;
  }


  function migrateRef(rootNode, migrate) {
    var path = migrate.path.split('/');
    if (path[0] === '' || path[0] === '#') {
      migrateRefPath(rootNode, path.slice(1), migrate.from);
    }
  }


  function migrateRefPath(node, path, from) {
    var key;
    for (var i = 0; i < path.length - 1; i++) {
      if (path[i] === '*') {
        for (key in node) {
          migrateRefPath(node[key], path.slice(i + 1), from);
        }
        return;
      }
      node = node[path[i]];
      if (node === undefined)
        return;
    }

    if (path[i] === '*') {
      for (key in node) {
        node[key] = from + '/' + node[key];
      }
    } else if (node[path[i]] !== undefined) {
      node[path[i]] = from + '/' + node[path[i]];
    }
  }

  function migrateFunction(rootNode, migrate) {
    function configGet(root, path, def) {
      var o = root;
      path = path.split('.');
      for (var i in path) {
        if (!(path[i] in o))
          return def;
        o = o[path[i]];
      }
      return o;
    }
    function configSet(root, path, value) {
      var o = root;
      path = path.split('.');
      var key = path.slice(-1);
      path = path.slice(0, -1);
      for (var i in path) {
        if (!(path[i] in o))
          o[path[i]] = {};
        o = o[path[i]];
      }
      o[key] = value;
    }

    /* Apply the migrate function to the configuration.
     * Replace the configuration with the one returned
     * by the function. */
    var f = eval(migrate.function);
    var newRootNode = f(extend({}, rootNode));
    for (var key in rootNode)
      delete rootNode[key];
    for (var key in newRootNode)
      rootNode[key] = newRootNode[key];
  }
};

var Validator = function(documentSchema) {
  var configurationUtils = new ConfigurationUtils();
  var documentData;
  var errorCount;
  var errorCb;

  this.validate = function (data, cb) {
    documentData = data;
    errorCb = cb;
    errorCount = 0;
    validateNode(documentSchema, data, []);
    return errorCount;
  }

  function error(path, message) {
    errorCount += 1;
    errorCb(path, message);
  }

  function validateNode(schema, node, path) {
    if (!checkDepend(schema, node, path)) {
      return;
    }

    if (schema.type === 'object') {
      validateObject(schema, node, path);
    } else if (schema.type === 'array') {
      validateArray(schema, node, path);
    } else if (schema.type === 'string') {
      validateString(schema, node, path);
    } else if (schema.type === 'integer') {
      validateInteger(schema, node, path);
    } else if (schema.type === 'number') {
      validateNumber(schema, node, path);
    } else if (schema.type === 'boolean') {
      validateBoolean(schema, node, path);
    } else if (schema.type === 'alias') {
      validateAlias(schema, node, path);
    } else {
      error(path, 'Invalid schema: type');
    }
  }

  function validateObject(schema, node, path) {
    if (node === null) {
      node = {};
    } else if (!configurationUtils.isPlainObject(node)) {
      error(path, 'Must be an object');
    }

    var properties = schema.properties || {};
    for (var child in properties) {
      path.push(child);
      if (child in node) {
        validateNode(properties[child], node[child], path);
      } else {
        validateNode(properties[child], null, path);
      }
      path.pop();
    }

    var additionalProperties = schema.additionalProperties;
    for (var child in node) {
      path.push(child);
      if (!(child in properties)) {
        if (additionalProperties) {
          if (!/^[a-z0-9_]+$/i.test(child)) {
            error(path, 'Invalid object key');
          }
          validateNode(additionalProperties, node[child], path);
        } else {
          error(path, 'Invalid object key');
        }
      }
      path.pop();
    }
  }

  function validateArray(schema, node, path) {
    if (!Array.isArray(node)) {
      error(path, 'Must be an array');
      return;
    }
    if (node.length === 0) {
      if (!schema.optional && checkEnable(schema, node, path)) {
        error(path, 'Required value');
      }
      return;
    }

    var items = schema.items;
    for (var i = 0; i < node.length; i++) {
      path.push(i);
      validateNode(items, node[i], path);
      path.pop();
    }
  }

  function validateString(schema, node, path) {
    if (node === null || node === '') {
      if (!schema.optional && checkEnable(schema, node, path)) {
        error(path, 'Required value');
      }
      return;
    }
    if (typeof node !== 'string') {
      if (schema.private && Array.isArray(node)) {
        // TODO: don't allow this for standalone validator
      } else {
        error(path, 'Must be a string');
      }
      return;
    }
    if (schema.minLength) {
      if (node.length < schema.minLength) {
        error(path, 'Must be a minimum of ' + schema.minLength + ' characters');  
        return;
      }
    }
    if (schema.maxLength) {
      if (node.length > schema.maxLength) {
        error(path, 'Must be a maximum of ' + schema.maxLength + ' characters');
        return;
      }
    }

    if (schema.enum) {
      if (Array.isArray(schema.enum)) {
        if (schema.enum.indexOf(node) == -1) {
          error(path, 'Must be an enum value');
        }
      } else {
        if (!(node in schema.enum)) {
          error(path, 'Must be an enum value');
        }
      }
    }
    checkRef(schema, node, path);
    checkFormat(schema, node, path);
  }

  function validateInteger(schema, node, path) {
    if (typeof node !== "number" || !isFinite(node) || Math.floor(node) !== node) {
      error(path, 'Must be an integer');
      return;
    }
    if ('minimum' in schema && node < schema.minimum) {
      error(path, 'Must not be less than minimum: ' + schema.minimum);
    }
    if ('maximum' in schema && node > schema.maximum) {
      error(path, 'Must not be greater than maximumvalue: ' + schema.maximum);
    }
  }

  function validateNumber(schema, node, path) {
    if (typeof node !== "number" || !isFinite(node)) {
      error(path, 'Must be a number');
      return;
    }
    if ('minimum' in schema && node < schema.minimum) {
      error(path, 'Must not be less than minimum: ' + schema.minimum);
    }
    if ('maximum' in schema && node > schema.maximum) {
      error(path, 'Must not be greater than maximum: ' + schema.maximum);
    }
  }

  function validateBoolean(schema, node, path) {
    if (node !== true && node !== false) {
      error(path, 'Must be a boolean');
    }
  }

  function validateAlias(schema, node, path) {
    if (node !== null) {
      error(path, 'Must be a null');
    }
  }

  function checkEnable(schema, node, path) {
    var enable = schema.enable;
    if (!enable) {
      return true;
    }
    var enablePath = concatPath(path, enable.split('/'));
    if (enablePath == null) {
      error(path, 'Invalid schema: enable');
      return true;
    }
    return lookupNode(enablePath) !== false;
  }

  function checkDepend(schema, node, path) {
    var depend = schema.depend;
    if (!depend) {
      return true;
    }
    var dependPath = concatPath(path, depend.split('/'));
    if (dependPath == null) {
      error(path, 'Invalid schema: depend');
      return true;
    }

    var dependSchema = lookupSchema(dependPath);
    if (dependSchema == null) {
      error(path, 'Invalid schema: depend');
      return true;
    }
    if (!checkDepend(dependSchema, null, dependPath)) {
      return false;
    }

    var dependVal = lookupNode(dependPath);
    if (dependVal == null) {
      error(path, 'Invalid schema: depend');
      return true;
    }
    if (Array.isArray(schema.dependVal)) {
      for (var i = 0; i < schema.dependVal.length; i++) {
        if (dependVal === schema.dependVal[i]) {
          return true;
        }
      }
      return false;
    } else {
      return dependVal === schema.dependVal;
    }
  }

  function checkRef(schema, node, path) {
    if (!schema.ref) {
      return;
    }

    if (Array.isArray(schema.ref)) {
      // schema.ref is an array of paths, node is a path
      nodePath = concatPath(path, node.split('/'));
      if (nodePath == null) {
        error(path, 'Invalid schema: ref');
        return;
      }

      for (var i = 0; i < schema.ref.length; i++) {
        var ref = schema.ref[i].split('/');
        if (ref[ref.length - 1] === '*') {
          var refPath = concatPath(path, ref.slice(0, -1));
          if (configurationUtils.isEqual(refPath, nodePath.slice(0, -1))) {
            var refNode = lookupNode(refPath);
            if (!configurationUtils.isPlainObject(refNode)) {
              error(path, 'Invalid schema: ref');
            } else {
              for (refVal in refNode) {
                if (refVal === nodePath[nodePath.length - 1]) {
                  return;
                }
              }
            }
          }
        } else {
          var refPath = concatPath(path, ref);
          if (configurationUtils.isEqual(refPath, nodePath)) {
            return;
          }
        }
      }
      error(path, 'Must be a ref value');
    } else {
      // schema.ref is a single path, node is a key
      var refPath = concatPath(path, schema.ref.split('/'));
      if (path == null) {
        error(path, 'Invalid schema: ref');
        return;
      }
      var ref = lookupNode(refPath);
      if (ref == null) {
        error(path, 'Invalid schema: ref');
        return;
      }
      if (!configurationUtils.isPlainObject(ref)) {
        error(path, 'Invalid schema: ref');
        return;
      }
      if (node in ref) {
        return;
      }
      error(path, 'Must be a ref value');
    }
  }

  function checkFormat(schema, node, path) {
    if (!schema.format) {
      return;
    }

    var format = documentSchema.properties.schema.formats[schema.format];
    if (!format) {
      error(path, 'Invalid schema: format');
      return;
    }

    if (format.pattern) {
      var re = new RegExp(format.pattern, format.flags || '');
      if (!re.test(node)) {
        error(path, 'Must match syntax: ' + format.syntax);
        return;
      }
    }
    if (format.validator) {
      try {
        var error_message=eval('(function(){'+format.validator+'}())');
        if(typeof(error_message) != "undefined")
        {
          error(path, error_message);
          return;
        }
      } catch(e) {
        error(path, "Error in validator.");
        return;
      }
    }

    if (schema.format == 'duration' || schema.format == 'duration-ms') {
      var duration = parseDuration(schema.format, node);
      if ('minimum' in schema && duration < parseDuration(schema.format, schema.minimum)) {
        error(path, 'Must not be less than minimum: ' + schema.minimum);
        return;
      }
      if ('maximum' in schema && duration > parseDuration(schema.format, schema.maximum)) {
        error(path, 'Must not be greater than maximum: ' + schema.maximum);
        return;
      }
    }
  }

  function parseDuration(format, duration) {
    var mult, sum = 0, m, re = /([0-9]+)(ms|[wdhms])/gi;
    if (format == 'duration') {
      mult = { w: 604800, d: 86400, h: 3600, m: 60, s: 1 };
    } else if (format == 'duration-ms') {
      mult = { w: 604800000, d: 86400000, h: 3600000, m: 60000, s: 1000, ms: 1 };
    } else {
      return null;
    }
    while (m = re.exec(duration)) {
      sum += m[1]*mult[m[2]];
    }
    return sum;
  }

  function lookupNode(path) {
    var node = documentData;
    for (i = 0; i < path.length; i++) {
      node = node[path[i]];
      if (node == null) {
        return null;
      }
    }
    return node;
  }

  function lookupSchema(path) {
    var schema = documentSchema;
    for (i = 0; i < path.length; i++) {
      if (schema.type === 'object') {
        if (path[i] in schema.properties) {
          schema = schema.properties[path[i]];
        } else if (schema.additionalProperties) {
          schema = schema.additionalProperties;
        } else {
          schema = null;
        }
      } else if (schema.type === 'array') {
        schema = schema.items;
      } else {
        schema = null;
      }
      if (schema == null) {
        return null;
      }
    }
    return schema;
  }

  function concatPath(basepath, path) {
    if (path.length < 1) {
        return null;
    }

    if (path[0] === '' || path[0] === '#') {
      return path.slice(1);
    } else if (/^\d+$/.test(path[0])) {
      var count = parseInt(path[0]);
      if (basepath.length < count) {
        return null;
      }
      return basepath.slice(0, -count).concat(path.slice(1));
    } else {
      return null;
    }
  }
}

var ConfigurationUtils = function() {
  this.schemaDefault = function (schema, def) {
    if (schema.type === 'object') {
      def = def || {};
      var props = schema.properties || {};
      var propName, propDef;
      for (propName in props) {
        propDef = def[propName];
        if (propDef === undefined)
          propDef = props[propName].default;
        propDef = this.schemaDefault(props[propName], propDef);
        if (propDef !== undefined)
          def[propName] = propDef;
      }
      if (schema.additionalProperties) {
        for (propName in def) {
          if (!props.hasOwnProperty(propName)) {
            propDef = def[propName];
            if (propDef === undefined)
              propDef = schema.additionalProperties.default;
            propDef = this.schemaDefault(schema.additionalProperties, propDef);
            if (propDef !== undefined)
              def[propName] = propDef;
          }
        }
      }
    } else if (schema.type === 'array') {
      def = def || [];
      for (var i = 0; i < def.length; i++) {
        def[i] = this.schemaDefault(schema.items, def[i]);
      }
    } else if (schema.type === 'string') {
      def = def || '';
    } else if (schema.type === 'integer') {
      def = def || 0;
    } else if (schema.type === 'number') {
      def = def || 0.0;
    } else if (schema.type === 'boolean') {
      def = def || false;
    }

    return def;
  };

  this.merge = function (schema, parent, child, override) {
    if (override === true) {
      return child;
    }
    if (override === false || override == null) {
      return parent;
    }
    if (schema.type !== 'object' || !this.isPlainObject(override)) {
      return parent;
    }

    var data = { };
    for (prop in schema.properties) {
      if (schema.properties[prop].type !== 'alias') {
        data[prop] = this.merge(schema.properties[prop], parent[prop], child[prop], override[prop]);
      }
    }
    if (schema.additionalProperties) {
      for (prop in parent) {
        if (!data.hasOwnProperty(prop)) {
          data[prop] = this.merge(schema.additionalProperties, parent[prop], child[prop], override[prop]);
        }
      }
      for (prop in child) {
        if (!data.hasOwnProperty(prop) && override[prop] === true) {
          data[prop] = child[prop];
        }
      }
    }
    return data;
  };

  this.removeDefaults = function (schema, data) {
    var ret;
    if (data instanceof Array) {
      ret = [];
      for (var i in data) {
        ret[i] = this.removeDefaults(schema.items, data[i]);
      }
      return ret;
    } else if (typeof data == 'object') {
      ret = {};
      for (var name in data) {
        if (schema.properties && schema.properties[name]) {
          if (!this.isDefault(schema.properties[name], data[name])) {
            ret[name] = this.removeDefaults(schema.properties[name], data[name]);
          }
        } else if (schema.additionalProperties) {
          ret[name] = this.removeDefaults(schema.additionalProperties, data[name]);
        }
      }
      return ret;
    } else {
      return data;
    }
  };

  this.isDefault = function (schema, data) {
    var def = schema['default'];
    if (def !== undefined) {
      return this.isEqual(data, this.schemaDefault(schema, def));
    }

    if (data instanceof Array) {
      return data.length === 0;
    } else if (typeof data == 'object') {
      for (var name in data) {
        if (schema.properties && schema.properties[name]) {
          if (!this.isDefault(schema.properties[name], data[name]))
            return false;
        } else {
          return false;
        }
      }
      return true;
    } else if (typeof data == 'string') {
      return data === '';
    } else if (typeof data == 'number') {
      return data === 0;
    } else if (typeof data == 'boolean') {
      return !data;
    } else {
      console.warn('type ' + typeof data + ' not implemented');
    }
  };

  this.isEqual = function (data1, data2) {
    if (data1 === data2)
      return true;
    if (data1 === null || data2 === null)
      return false;
    var t1 = typeof data1;
    var t2 = typeof data2;
    if (t1 != t2)
      return false;
    if (data1 instanceof Array) {
      if (!(data2 instanceof Array))
        return false;
      if (data1.length != data2.length)
        return false;
      for (var i = 0; i < data1.length; i++) {
        if (!this.isEqual(data1[i], data2[i]))
          return false;
      }
      return true;
    }
    if (t1 == 'object') {
      var name;
      for (name in data1) {
        if (data2[name] === undefined)
          return false;
        if (!this.isEqual(data1[name], data2[name]))
          return false;
      }
      for (name in data2) {
        if (data1[name] === undefined)
          return false;
      }
      return true;
    }
    return false;
  };

  // Best attempt at a no DOM version of JQuery.isPlainObject. The difference is that an instance of
  // a class will return true here where JQuery's returns false because the class instance has a constructor
  // This is okay for our current usage.
  this.isPlainObject = function( obj ) {
    return Object.prototype.toString.call( obj ) === "[object Object]";
  }
};
