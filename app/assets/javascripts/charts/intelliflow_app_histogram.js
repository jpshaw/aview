function intelliflowAppHistogram(options) {
  this.load = function () {
    $.getJSON(options.uri, {apps: options.topApplications, id: options.deviceId, range: options.range}, function (result) {
      drawHighChart({
        options: chartOptions(),
        target: $(options.target),
        response: result,
        type: 'line'
      });
    });
  };

  this.loadEmptyChart = function() {
    $(options.target).highcharts({
      credits: {enabled: false},
      title: {text: ''}
    });
  };

  this.exists = function () {
    return $(options.target + ' .highcharts-container').length > 0;
  };

  function chartOptions() {
    return {
      title: {text: 'Stacked Usage Over Time'},
      xAxis: {type: 'datetime'},
      yAxis: {
        labels: {
          formatter: function () {
            return formatTransferBytes(this.value)
          }
        },
        min: 0,
        title: {text: null}
      },
      tooltip: {
        formatter: function () {
          var tooltipString = '<span style="font-size: .9em">' + Highcharts.dateFormat('%A, %b %d, %H:%M', this.x) + '</span>';
          var totalBytes = 0;

          $.each(this.points, function (i, point) {
            tooltipString += '<br/>' + tooltipPointFormatter(point);
            totalBytes += point.y;
          });

          tooltipString += '<br/>' +
            '<span style="color: transparent;">\u25CF</span>' + '<b>' + ' Total: ' + formatTransferBytes(totalBytes) + '</b>';

          return tooltipString;
        }
      }
    };
  }

  function tooltipPointFormatter(point) {
    return "<span style=\"color: " + point.color + ";\">\u25CF</span> " + point.series.name + ": <b>" + formatTransferBytes(point.y) + "</b><br/>";
  }
}
