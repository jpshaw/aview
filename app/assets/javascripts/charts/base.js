function drawHighChart(config) {
  switch (config.type) {
    case 'areaspline':
      drawAreaSplineChart(config);
      break;
    case 'pie':
      drawPieChart(config);
      break;
    case 'column':
      drawColumnChart(config);
      break;
    case 'line':
      drawLineChart(config);
      break;
  }
}

function drawAreaSplineChart(config) {
  config.options = mergeChartOptions(config.options, defaultAreaSplineChartOptions(config));
  config.target.highcharts(processConfig(config.options));
}

function drawColumnChart(config) {
  config.options = mergeChartOptions(config.options, defaultColumnChartOptions(config));
  config.target.highcharts(processConfig(config.options));
}

function drawPieChart(config) {
  config.options = mergeChartOptions(config.options, defaultPieChartOptions(config));
  config.target.highcharts(processConfig(config.options));
}

function drawLineChart(config) {
  config.options = mergeChartOptions(config.options, defaultLineChartOptions(config));
  config.target.highcharts(processConfig(config.options));
}

function processConfig(options) {
  options = processSeries(options);
  options = options.drilldown ? processDrilldown(options) : options;
  return options;
}

function mergeChartOptions(options, defaultOptions) {
  return $.extend(true, defaultOptions, options);
}

function defaultAreaSplineChartOptions(config) {
  return {
    chart: { type: 'areaspline' },
    credits: {enabled: false},
    exporting: {enabled: true},
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: config.response ? config.response.series : {},
    tooltip: { shared: true }
  };
}

function defaultLineChartOptions(config) {
  return {
    chart: { type: 'line' },
    credits: {enabled: false},
    exporting: {enabled: true},
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: config.response ? config.response.series : {},
    tooltip: { shared: true }
  };
}

function defaultColumnChartOptions(config) {
  return {
    chart: { type: 'column' },
    credits: {enabled: false},
    exporting: {enabled: true},
    plotOptions: {
      series: {
        point: {
          events: {
            click: function () { handleHighChartClick(config.response.links, this) }
          }
        }
      }
    },
    series: config.response.series,
  };
}

function defaultPieChartOptions(config) {
  return {
    credits: {enabled: false},
    exporting: {enabled: true},
    plotOptions: {
      pie: {
        dataLabels: { enabled: false },
        showInLegend: true,
        startAngle: -180
      },
      series: {
        point: {
          events: {
            click: function () { handleHighChartClick(config.response.links, this) }
          }
        }
      }
    },
    series: [{
      center: ['50%', '50%'],
      data: config.response.series,
      innerSize: '60%',
      type: 'pie'
    }],
    title: {
      align: 'center',
      style: {fontSize: '14px'},
      verticalAlign: 'middle',
      y: -24
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '{point.name}: <b>{point.y}</b>'
    }
  };
}

function handleHighChartClick(links, point) {
  var link_key = links.link_key || 'name'
  if (links && links[point[link_key]]) {
    window.location.href = links[point[link_key]]
  }
}

function processDrilldown(options) {
  $.each(options.drilldown.series, function() {
    this.data = processPoints(this.data);
  });
  return options;
}

function processPoint(point) {
  var options = {};
  point.color ? setColor(point.color, options) : {};
  point = Object.keys(options).length ? $.extend(true, point, options) : point;
  return point;
}

function processPoints(points) {
  var processedPoints = [];
  $.each(points, function (index, point) {
    processedPoints.push(processPoint(point));
  });
  return processedPoints;
}

function processSeries(options) {
  $.each(options.series, function() {
    this.data = processPoints(this.data);
  });
  return options;
}

function setColor(color, options) {
  options.color = this.aviewColors[color] ;
}
