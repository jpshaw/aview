$(document).ready(function () {
  var target = $('.chart#deployed-status');
  if ( target.is(':visible') ) {
    initChart();
  }

  function initChart() {
    $.getJSON('/highcharts/deployed_status_chart', function (response) {
      drawHighChart({
        options: chartOptions(response),
        response: response,
        target: target,
        type: 'pie'
      });
    });
  }

  function chartOptions(response) {
    return {
      colors: [
        aviewColors.statusUp,
        aviewColors.statusUndeployed
      ],
      title: { text: response.total_count + '<br>Devices' }
    };
  }
});
