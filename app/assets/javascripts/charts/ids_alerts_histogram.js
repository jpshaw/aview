// eslint-disable-next-line no-unused-vars
function idsAlertsHistogram(options) {
  this.load = function () {
    $.getJSON(options.uri, { mac: options.deviceMAC }, function (result) {
      // eslint-disable-next-line no-undef
      drawHighChart({
        options: chartOptions(),
        target: $(options.target),
        type: "column",
        response: result
      });
    });
  };

  this.exists = function() {
    return $(options.target + " .highcharts-container").length > 0;
  };

  function chartOptions() {
    return {
      title: { text: "Alerts Over Time" },
      xAxis: { type: "datetime" },
      yAxis: { min: 0, title: { text: null } },
    };
  }
}
