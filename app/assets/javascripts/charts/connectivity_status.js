$(document).ready(function () {
  var target = $('.chart#connectivity-status');
  if (target.is(':visible')) {
    initChart();
  }

  function initChart() {
    $.getJSON('/highcharts/connectivity_status_chart', function (response) {
      drawHighChart({
        options: chartOptions(response),
        response: response,
        target: target,
        type: 'pie'
      });
    });
  }

  function chartOptions(response) {
    return {
      colors: [
        aviewColors.statusUp,
        aviewColors.statusDown,
        aviewColors.statusInactive
      ],
      series: [{name: 'Connectivity Status'}],
      title: {text: response.total_count + '<br>Devices'}
    }
  }
});
