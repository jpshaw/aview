$(document).ready(function () {
  var target = $('.chart#signal-strength');
  if (target.is(':visible')) {
    initChart();
  }

  function initChart() {
    $.getJSON('/highcharts/signal_strength_chart', function (response) {
      drawHighChart({
        options: chartOptions(response),
        response: response,
        target: target,
        type: 'column'
      });
    });
  }

  function chartOptions(response) {
    return {
      legend: { enabled: false },
      plotOptions: {
        column: {
          colorByPoint: true,
          colors: [
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp,
            aviewColors.statusUp
          ]
        }
      },
      title: {text: ''},
      xAxis: {
        categories: [
          '0% - 9%',
          '10% - 19%',
          '20% - 29%',
          '30% - 39%',
          '40% - 49%',
          '50% - 59%',
          '60% - 69%',
          '70% - 79%',
          '80% - 89%',
          '90% - 100%'
        ],
        title: {text: 'Signal Strength (%)'}
      },
      yAxis: {
        allowDecimals: false,
        title: {text: 'Devices'}
      }
    };
  }
});
