$(document).ready(function () {
  var target = $('.chart#network-type');
  if (target.is(':visible')) {
    initChart();
  }

  function initChart() {
    $.getJSON('/highcharts/network_type_chart', function (response) {
      drawHighChart({
        options: chartOptions(response),
        response: response,
        target: target,
        type: 'pie'
      });
    });
  }

  function chartOptions(response) {
    return {
      title: {text: response.total_count + '<br>Devices'}
    }
  }
});
