function intelliflowAppPieChart(options) {
  this.load = function (loadCompleteCallback, clickListener) {
    $.getJSON(options.uri, {id: options.deviceId, range: options.range}, function (result) {
      drawHighChart({
        options: chartOptions(clickListener),
        target: $(options.target),
        response: result,
        type: 'pie'
      });
      var topApplications = parseTopApplications(result.series);
      loadCompleteCallback(topApplications);
    });
  };

  this.exists = function () {
    return $(options.target + ' .highcharts-container').length > 0;
  };

  function parseTopApplications(series) {
    var topApplications = [];
    series.forEach(function (point) {
      topApplications.push(point.name);
    });
    return topApplications;
  }

  function chartOptions(clickListener) {
    return {
      title: {text: ''},
      plotOptions: {
        series: {
          point: {
            events: {
              click: function () {
                this.select();
                if(this.selected) {
                  clickListener(this.name);
                } else {
                  clickListener();
                }
              }
            }
          }
        }
      },
      tooltip: {
        formatter: function () {
          return this.key + ': <b>' + formatTransferBytes(this.y) + '</b>';
        }
      },
      legend: {
        align: 'left',
      }
    };
  }
}
