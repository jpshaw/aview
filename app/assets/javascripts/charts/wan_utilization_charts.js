function WanUtilizationChartConstructor(args) {
  this.load = function() {
    updateCharts();
    listenForRangeChange();
  };

  this.reflow = function() {
    setActiveChart();
  };

  function updateCharts(options) {
    if (!options) { options = {}; }
    showLoading();

    var jqxhr = getChartData(chartParams(options));
    jqxhr.done( function() {
      var response = jqxhr.responseJSON;
      removeOldCharts();
      if ( responseHasData(response) ) {
        $('.wan-utilization-charts').removeClass('hidden');
        drawCharts(response);
      }
      else { renderNoData(); }
      updateInterfaceSelector();
      setActiveChart();
    });
  }

  function chartParams(options) {
    var params = {};
    params.mac = args.mac;
    params.range = options.range || '24h';
    return params;
  }

  function getChartData(options) {
    return $.getJSON('/highcharts/wan_utilization', options);
  }

  function showLoading(){
    var chartElements = $('.chart.wan-utilization');
    if ( chartElements.length > 0 ) { chartElements.highcharts().hideNoData(); chartElements.highcharts().showLoading(); }
  }

  function removeOldCharts() { $('.chart-container.wan-utilization > *').remove(); }

  function responseHasData(response) { return response.length > 0; }

  function drawCharts(response) {
    $.each(response, function (i, deviceInterface) { drawInterface(deviceInterface); });
  }

  function drawInterface(deviceInterface) {
    $('.chart-container.wan-utilization').append("<div class='" + deviceInterface.name + "' data-interface='" + deviceInterface.name + "'></div>");

    var upload_data = [];
    var download_data = [];

    deviceInterface.data.forEach(function(series) {
      var data = { name: series.name, data: series.data }
      switch (series.name) {
        case 'upload':
        case 'upload_cos':
        case 'upload_hwm':
          upload_data.push(data);
          break;
        case 'download':
        case 'download_cos':
        case 'download_hwm':
          download_data.push(data);
          break;
      }
    })

    drawChart({ name: deviceInterface.name, data: upload_data, type: 'upload bandwidth' });
    drawChart({ name: deviceInterface.name, data: download_data, type: 'download bandwidth' });
  }

  function drawChart(deviceInterface) {
    $('.chart-container.wan-utilization .' + deviceInterface.name)
      .append("<div class='chart wan-utilization hidden " + deviceInterface.type + "'></div>");

    drawHighChart({
      options: chartOptions(deviceInterface),
      target: $(chartElementSelector(deviceInterface)),
      type: 'line'
    });
  }

  function chartOptions(deviceInterface) {
    var exportEnabled = deviceInterface ? deviceInterface.length > 0 : false;
    var series = deviceInterface ? deviceInterface.data : [];
    var titleText = deviceInterface ? deviceInterface.type.toTitleCase() : '';

    return {
      exporting: { enabled: exportEnabled },
      legend: { labelFormatter: function () { return legendLabel(this.name); }},
      plotOptions: { series: { stacking: null } },
      series: formatSeries(series),
      subtitle: {
        style: { fontSize: '12px' },
        text: subtitleText(deviceInterface)
      },
      title: {
        style: { fontSize: '14px' },
        text: titleText
      },
      tooltip: { formatter: function() { return tooltipFormatter(this); } },
      xAxis: { type: 'datetime' },
      yAxis: {
        min: 0,
        title: { text: '' },
        labels: { formatter: function() { return bytesFormat(this.value, true); } }
      }
    };
  }

  function bytesFormat(bytes, label) {
    if (bytes == 0) return '0';
    bytes = bytes * 1024;
    var s = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    var e = Math.floor(Math.log(bytes) / Math.log(1024));
    var value = ((bytes / Math.pow(1024, e)).toFixed(0));
    e = (e < 0) ? (-e) : e;
    if (label) value += ' ' + s[e];
    return value;
  }

  function formatSeries(series) {
    series.forEach(function(singleSeries) {
      singleSeries = formatSingleSeries(singleSeries);
    })
    return series;
  }

  function formatSingleSeries(singleSeries) {
    switch(singleSeries.name) {
      case 'upload':
      case 'download':
        singleSeries.color = "#434348";
        break;
      case 'upload_cos':
      case 'download_cos':
        singleSeries.color = "#fc6c85";
        break;
      case 'upload_hwm':
      case 'download_hwm':
        singleSeries.color = "#90ed7d";
        break;
    }

    return singleSeries;
  }

  function subtitleText(deviceInterface) {
    var hwmSeries;
    var usageSeries;

    if (deviceInterface === undefined) { return null }

    deviceInterface.data.forEach(function(series) {
      switch (series.name) {
        case 'upload_hwm':
          usageSeries = series.data;
          break;
        case 'upload_cos':
          hwmSeries = series.data;
          break;
        case 'download_hwm':
          usageSeries = series.data;
          break;
        case 'download_cos':
          hwmSeries = series.data;
          break;
      }
    });

    var size = deviceInterface.data[0].data.length;
    var usagesPerInterval = [];
    var usagePerInterval = 0;

    for (var i = 0; i < size; i++) {
      var denominator = 0;
      var numerator;

      if (usageSeries != undefined) { numerator = usageSeries[i].y; }
      if (hwmSeries != undefined) { denominator = hwmSeries[i].y; }

      if ( denominator === 0 ) { if ( numerator !== 0 ) { continue; } }
      else { usagesPerInterval.push(numerator / denominator); }
    }

    if (usagesPerInterval.length === 0) { usagePerInterval = '--'; }
    else {
      var max = usagesPerInterval[0];
      usagesPerInterval.forEach(function(item){
        max = item > max ? item : max;
      });
      usagePerInterval = (max * 100).toFixed(1) + "%";
    }

    return 'Max Percentage by Range: ' + usagePerInterval ;
  }

  function tooltipFormatter(tooltip) {
    var tooltipString = '<span style="font-size: .9em">' + Highcharts.dateFormat('%A, %b %d, %H:%M', tooltip.x) + '</span>';
    var percentUtilization = {};

    $.each(tooltip.points, function(i, point) {
      tooltipString += '<br/>' + tooltipPointFormatter(point);
      percentUtilization[point.series.name] = point.y;
    });

    percentUtilization = calculatePercentUtilization(percentUtilization);

    tooltipString += '<br/>' +
                     '<span style="color: transparent;">\u25CF</span>' + '<b>' + ' Max Percentage Used: ' + percentUtilization + '</b>';

    return tooltipString;
  }

  function calculatePercentUtilization(percentUtilization) {
    var usageString = '--'
    if ( !percentUtilization['upload_cos'] == 0 ) {
      usageString = ( percentUtilization['upload_hwm'] / percentUtilization['upload_cos'] * 100 ).toFixed(1);
      usageString += ' %'
    }
    else if ( !percentUtilization['download_cos'] == 0 ) {
      usageString = ( percentUtilization['download_hwm'] / percentUtilization['download_cos'] * 100 ).toFixed(1);
      usageString += ' %'
    }
    return usageString;
  }

  function tooltipPointFormatter(point) {
    return "<span style=\"color: " + point.color + ";\">\u25CF</span> " + legendLabel(point.series.name) + ": <b>" + point.y + " kb/sec</b><br/>";
  }

  function legendLabel(name) {
    var label;

    switch(name) {
      case 'download':
      case 'upload':
        label = 'Average Used'
        break;
      case 'download_cos':
      case 'upload_cos':
        label = 'High Water Mark'
        break;
      case 'download_hwm':
      case 'upload_hwm':
        label = 'Maximum Used'
        break;
    }

    return label;
  }

  function renderNoData(){
    $('.chart-container.wan-utilization').append("<div class='chart wan-utilization wan-utilization-chart-no-data' style='width: 100%'></div>");
    var target = $(".wan-utilization-chart-no-data");

    drawHighChart({
      options: chartOptions(),
      target: target,
      type: 'line'
    });
  }

  function updateInterfaceSelector() {
    var chartElements = $('.chart-container.wan-utilization > *');
    if ( chartElements.length > 1 ) {
      if ( !interfaceSelectorPresent() ) { drawInterfaceSelector(); }
      updateInterfaceSelectorOptions(chartElements);
      listenForInterfaceChange();
    }
    else { $('#interface-select').closest('.form-group').remove(); }
  }

  function interfaceSelectorPresent() { return $('#interface-select').length > 0; }

  function drawInterfaceSelector() {
    $('.wan-utilization-charts .form').prepend(
      '<div class="form-group">' +
      ' <label for="interface-select">Interface</label>' +
      ' <select class="form-control" id="interface-select"></select>' +
      '</div>'
    );
  }

  function updateInterfaceSelectorOptions(chartElements) {
    $('#interface-select option').remove();
    $.each(chartElements, function(i, chartElement) { addInterfaceSelectorOption($(chartElement).data('interface')); });
  }

  function addInterfaceSelectorOption(interfaceType) {
    $('#interface-select').append("<option " + "value=\"" + interfaceType + "\"" + ">" + interfaceType + "</option>");
  }

  function chartElementSelector(id) { return "." + id.name + " ." + id.type.replace(/\s/g, "."); }

  function listenForInterfaceChange() {
    $('#interface-select').change(function() {
      showSelectedChart();
    });
  }

  function showSelectedChart() {
    var target = $('#interface-select').val();
    target = $("." + target + " .chart");
    $('.chart.wan-utilization').addClass('hidden');
    showAndReflow(target);
  }

  function showAndReflow(target) {
    target = $(target);
    target.removeClass('hidden');
    if (target.length > 1) { $.each(target, function(index, element) { $(element).highcharts().reflow(); }); }
    else { target.highcharts().reflow(); }
  }

  function setActiveChart() {
    if ( interfaceSelectorPresent() ) { showSelectedChart(); }
    else { showAndReflow('.chart.wan-utilization'); }
  }

  function listenForRangeChange() {
    $('#wan-utilization-range').change( function() {
      var range = $(this).val();
      updateCharts({range: range});
    });
  }
}
