var AccConfigForm = function (dataElement, panelElement) {
      var isReadOnly    = dataElement.prop('tagName') == 'FORM' ? false : true;
      var accfgSession  = new accfg.Accfg(dataElement, panelElement);

      function _buildFormImpl(schema, data) {
          if (!schema) { return; }
          var parentConfigDescription = $('#parent_configuration_description').val();
          if(parentConfigDescription === undefined || parentConfigDescription === '')
              var parentData = null;
          else
              var parentData = JSON.parse(parentConfigDescription);

          accfgSession.render(schema, data, {
              dataURL: '',
              advMode: false,
              readOnly: isReadOnly,
              collapsedCollapsibles: true,
              renderFinished: function (element, details) {
                  dataElement.hide();
                  dataElement.fadeIn('fast');
              },
              parentData: parentData
          });
      }

      function buildForm() {
          panelElement.empty();

          var device_firmware_id = $('#device_configuration_device_firmware_id').val();

          if (device_firmware_id != '' && device_firmware_id != null) {
              $.get(
                '/device_firmwares/' + device_firmware_id + '/schema_file'
              )
              .done(function(data) {
                  if (data.status == 200) {
                    var schema      = JSON.parse(data.schema);
                    var description = JSON.parse($('#device_configuration_description').val() || '{}') || {};
                    _buildFormImpl(schema, description);
                  } else {
                    alert(data.message);
                  }
              });
          }

          return true;
      }

      $('#device_configuration_device_firmware_id').change(function() {
        buildForm();
      });

      // Listen to form submit
      dataElement.submit(function (evt) {
          var device_firmware_id = $('#device_configuration_device_firmware_id').val();

          if (!isReadOnly && device_firmware_id != null) {
            // Update firmware.version with value from drop down.
            $("input[name*='firmware.version']").first().val($('#device_configuration_device_firmware_id option:selected').text());

            var outData = accfgSession.getData();

            // Display alert and bail if firmware selected and errors in schema.
            if (device_firmware_id != '' && outData.errorCount > 0) {
              alert("Unable to continue. " + outData.errorCount + " errors were found in the form. Please correct any Settings errors highlighted in red before continuing.");
              return false;
            }
          }

          // Clear description if no firmware, otherwise build it up.
          if (device_firmware_id == '') {
            $('#device_configuration_description').empty();
          } else if(outData != null) {
            $('#device_configuration_description').val(JSON.stringify(outData.data, null, "  "));
          }

          panelElement.empty();
          return true;
      });

      buildForm();
};
