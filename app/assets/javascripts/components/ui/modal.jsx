class Modal extends React.Component {
  render() {
    return (
      <ReactBootstrap.Modal { ...this.props } className="vertical-centered">
        { this.props.children }
      </ReactBootstrap.Modal>
    );
  }
}
class ModalHeader extends React.Component {
  render() {
    return (
      <ReactBootstrap.Modal.Header { ...this.props }>
        { this.props.children }
      </ReactBootstrap.Modal.Header>
    );
  }
}
class ModalTitle extends React.Component {
  render() {
    return (
      <ReactBootstrap.Modal.Title { ...this.props }>
        { this.props.children }
      </ReactBootstrap.Modal.Title>
    );
  }
}
class ModalBody extends React.Component {
  render() {
    return (
      <ReactBootstrap.Modal.Body { ...this.props }>
        { this.props.children }
      </ReactBootstrap.Modal.Body>
    );
  }
}
class ModalFooter extends React.Component {
  render() {
    return (
      <ReactBootstrap.Modal.Footer { ...this.props }>
        { this.props.children }
      </ReactBootstrap.Modal.Footer>
    );
  }
}

Modal.Header = ModalHeader;
Modal.Title = ModalTitle;
Modal.Body = ModalBody;
Modal.Footer = ModalFooter;
