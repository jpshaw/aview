class ConfigurationsCascadedNetworkModal extends React.Component {
  constructor(props) {
    super(props);

    this.default_structure = this.props.model || {
      ip_address: null,
      subnet_mask: null,
      router_ip_address: null,
      description: null,
      local_lan_alias: false,
      internet_only_route: false,
      routing_metric: null,
      restrict_vpn_advertisement: false,
      history: null
    };

    this.state = {
      has_saved: false,
      is_processing: false,
      is_new: !this.props.model,
      errors: [],
      model: AV.UTIL.DATA.clone(this.default_structure)
    };

    this.renderErrors = this.renderErrors.bind(this);
    this.processingStart = this.processingStart.bind(this);
    this.processingStop = this.processingStop.bind(this);
    this.saveAction = this.saveAction.bind(this);
    this.closeAction = this.closeAction.bind(this);
    this.fieldChange = this.fieldChange.bind(this);
  }

  processingStart() {
    let new_state = this.state;
    new_state.is_processing = true;
    this.setState(new_state);
  }

  processingStop() {
    let new_state = this.state;
    new_state.is_processing = false;
    this.setState(new_state);
  }

  saveAction() {
    const success = () => {
      this.processingStop();
      this.closeAction();
    }

    const fail = (response) => {
      let new_state = this.state;
      new_state.errors = response.errors;
      this.setState(new_state);

      this.processingStop();
    }

    this.processingStart();
    this.props.save_action(this.state.model)
      .then((response) => {
        if(_.some(response.errors)) {
          fail(response);
          return;
        }
        success();
      });
  }

  closeAction() {
    let new_state = this.state;
    new_state.model = AV.UTIL.DATA.clone(this.default_structure);
    this.setState(new_state);

    this.props.close_action();
  }

  fieldChange(event, field) {
    let new_state = this.state;
    const value = event.target.value;
    new_state.model[field] = value;
    this.setState(new_state);
  }

  renderErrors(errors) {
    return (
      <div className="errors-container text-left">
        <h3>Please check the following issues:</h3>
        { errors.map((error, index) => {
          return (
            <div key={ index } dangerouslySetInnerHTML={{ __html: error }} />
          )
        })}
      </div>
    )
  }

  render() {
    const { show_modal } = this.props;
    const { has_saved, is_processing, is_new, model, errors } = this.state;

    return (
      <Modal
        show={ show_modal }
        onHide={ this.closeAction }
      >
        <Modal.Header>
          <Modal.Title>
            <div className="row">
              <div className="col-sm-7">
                Cascaded Network
              </div>
              <div className="col-sm-5">
                History
              </div>
            </div>
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="row">

            <div className="col-sm-7">
              <div className="row">
                <div className="col-sm-12">
                  <label>IP Address</label>
                  { is_new && (<input type="text" className="form-control" value={ model.ip_address || '' } onChange={ (event) => this.fieldChange(event, 'ip_address') } />)}
                  { !is_new && (<div className="form-control-static">{ model.ip_address || '' }</div>)}
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Subnet Mask/Prefix</label>
                  <input type="text" className="form-control" value={ model.subnet_mask || '' } onChange={ (event) => this.fieldChange(event, 'subnet_mask') } />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Router IP Address</label>
                  <input type="text" className="form-control" value={ model.router_ip_address || '' } onChange={ (event) => this.fieldChange(event, 'router_ip_address') } />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Description</label>
                  <input type="text" className="form-control text-uppercase" value={ model.description || '' } maxLength="30" onChange={ (event) => this.fieldChange(event, 'description') } />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Local Lan Alias</label>
                  <div>
                    <div className="radio-inline">
                      <input type="radio" name="local_lan_alias"
                        value={ true } defaultChecked={ AV.UTIL.DATA.parseBool(model.local_lan_alias) === true }
                        onChange={ (event) => this.fieldChange(event, 'local_lan_alias') } />
                      Yes
                    </div>
                    <div className="radio-inline">
                      <input type="radio" name="local_lan_alias"
                        value={ false } defaultChecked={ AV.UTIL.DATA.parseBool(model.local_lan_alias) === false }
                        onChange={ (event) => this.fieldChange(event, 'local_lan_alias') } />
                      No
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Internet Only Route</label>
                  <div>
                    <div className="radio-inline">
                      <input type="radio" name="internet_only_route"
                        value={ true } defaultChecked={ AV.UTIL.DATA.parseBool(model.internet_only_route) === true }
                        onChange={ (event) => this.fieldChange(event, 'internet_only_route') } />
                      Yes
                    </div>
                    <div className="radio-inline">
                      <input type="radio" name="internet_only_route"
                        value={ false } defaultChecked={ AV.UTIL.DATA.parseBool(model.internet_only_route) === false }
                        onChange={ (event) => this.fieldChange(event, 'internet_only_route') } />
                      No
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Routing Metric</label>
                  <input type="text" className="form-control" value={ model.routing_metric || '' } onChange={ (event) => this.fieldChange(event, 'routing_metric') } />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Restrict VPN Advertisement</label>
                  <div>
                    <div className="radio-inline">
                      <input type="radio" name="restrict_vpn_advertisement"
                        value={ true } defaultChecked={ AV.UTIL.DATA.parseBool(model.restrict_vpn_advertisement) === true }
                        onChange={ (event) => this.fieldChange(event, 'restrict_vpn_advertisement') } />
                      Yes
                    </div>
                    <div className="radio-inline">
                      <input type="radio" name="restrict_vpn_advertisement"
                        value={ false } defaultChecked={ AV.UTIL.DATA.parseBool(model.restrict_vpn_advertisement) === false }
                        onChange={ (event) => this.fieldChange(event, 'restrict_vpn_advertisement') } />
                      No
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-sm-5">
              <div className="row">
                <div className="col-sm-12">
                  <label>Created By</label>
                  <input type="text" className="form-control" value={ model.created_by } disabled />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Created On</label>
                  <input type="text" className="form-control" value={ model.created_on } disabled />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Updated By</label>
                  <input type="text" className="form-control" value={ model.updated_by } disabled />
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <label>Updated On</label>
                  <input type="text" className="form-control" value={ model.updated_on } disabled />
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>

        <Modal.Footer>

          { _.some(errors) && this.renderErrors(errors) }

          <div className="row">
            <div className="col-sm-12 text-right actions">
              { is_processing && (<span className="wait-for left">PROCESSING <img className="waiting-image" src={ this.props.waiting_url } /></span>)}
              <button className="btn btn-default" onClick={ this.closeAction } disabled={ is_processing }>Cancel</button>
              <button className="btn btn-primary" onClick={ this.saveAction } disabled={ is_processing }>Save</button>
            </div>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}
