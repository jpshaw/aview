class NetworkConfigurationTab extends React.Component {

  constructor(props) {
    super(props);

    this.settings = {
      fields_enabled: this.props.fields_enabled,
      urls: {
        spinner_image: this.props.spinner_url,
        get: this.props.fetch_url,
        save: this.props.save_url
      }
    };
    this.state = {
      has_saved: false,
      last_saved_on: null,
      page_is_processing: false,
      page_initialized: false,
      data: {
        original: {},
        current: {}
      }
    };

    this.updateField = this.updateField.bind(this);
    this.toggleCheckboxField = this.toggleCheckboxField.bind(this);
    this.setCheckboxField = this.setCheckboxField.bind(this);
    this.save = this.save.bind(this);
    this.cancel = this.cancel.bind(this);
    this.processingStart = this.processingStart.bind(this);
    this.processingStop = this.processingStop.bind(this);
  }

  componentWillMount() {
    this.processingStart();

    AV.UTIL.AJAX.get(this.settings.urls.get, { }, (data) => {
      let new_state = this.state;

      new_state.data.original = data;
      new_state.data.current = AV.UTIL.DATA.clone(data);
      new_state.page_initialized = true;

      this.setState(new_state);
      this.processingStop();
    });
  }

  processingStart() {
    let new_state = this.state;
    new_state.page_is_processing = true;
    this.setState(new_state);
  }

  processingStop() {
    let new_state = this.state;
    new_state.page_is_processing = false;
    this.setState(new_state);
  }

  save() {
    this.processingStart();
    AV.UTIL.AJAX.put(
      this.settings.urls.save,
      { network: this.state.data.current.network },
      (response) => {
        let new_state = this.state;

        const has_errors = response.errors && response.errors.length > 0;
        if(has_errors) {
          new_state.has_saved = false;
          new_state.last_saved_on = null;
          new_state.data.current.errors = response.errors;
          this.setState(new_state);
        } else {
          const saved_date_time = new Date();
          new_state.has_saved = true;
          new_state.last_saved_on = saved_date_time.toLocaleString();
          new_state.data.original = response;
          new_state.data.current = AV.UTIL.DATA.clone(response);
        }
        this.setState(new_state);

        this.processingStop();

      }
    );
  }

  cancel() {
    this.processingStart();
    var allowCancellation = confirm("Revert changes?");
    if(!allowCancellation) { this.processingStop(); return; }

    let new_state = this.state;
    new_state.data.current = AV.UTIL.DATA.clone(new_state.data.original);
    this.setState(new_state);
    this.processingStop();
  }

  updateField(event, property_path) {
    let new_state = this.state;
    let { current } = new_state.data.current;
    const path = `data.current.${ property_path }`;
    const value = event.target.value;

    _.set(new_state, path, value);

    this.setState(new_state);
  }

  toggleCheckboxField(property_path) {
    let new_state = this.state;
    let { current } = new_state.data.current;
    const path = `data.current.${ property_path }`;
    const prior_value = _.get(new_state, path);

    _.set(new_state, path, !prior_value);

    this.setState(new_state);
  }

  setCheckboxField(property_path, value) {
    let new_state = this.state;
    let { current } = new_state.data.current;
    const path = `data.current.${ property_path }`;

    _.set(new_state, path, value);

    this.setState(new_state);
  }

  renderErrors(errors) {
    return (
      <div className="row errors-container">
        <h3>Please check the following issues:</h3>
        { errors.map((error, index) => {
          return (
            <div key={ index } className="col-sm-12" dangerouslySetInnerHTML={{ __html: error }} />
          )
        })}
      </div>
    )
  }

  render() {
    const { page_is_processing, page_initialized, has_saved, last_saved_on } = this.state;
    const { network, errors } = this.state.data.current;

    if(!page_initialized) {
      return (
        <div>
          <h3>
            <img src={ this.settings.urls.spinner_image } />
            Loading Configuration Information
          </h3>
        </div>
      )
    }

    return (
      <fieldset disabled={ !this.settings.fields_enabled }>
        <div>
          <ConfigurationsHeaderRow title="Override/Device Network" secondary_title="Default/Inherited" />

          <ConfigurationsHeaderRow title="LAN" />

          <ConfigurationsSelectRow label="Ethernet Speed"
            property_path="network.lan.ethernet_speed"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />
          <ConfigurationsInputRow label="Address"
            property_path="network.lan.address"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />
          <ConfigurationsInputRow label="Subnet"
            property_path="network.lan.subnet"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />

          <ConfigurationsHeaderRow title="DHCP" />

          <ConfigurationsCheckboxRow label="Authoritative Server"
            property_path="network.dhcp.authoritative_server"
            property_obj={ this.state.data.current }
            field_set={ this.setCheckboxField }
            field_change={ this.toggleCheckboxField }
            inherit_change={ this.toggleCheckboxField }
            />
          <ConfigurationsInputRow label="Start Address"
            property_path="network.dhcp.start_address"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />
          <ConfigurationsInputRow label="End Address"
            property_path="network.dhcp.end_address"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />
          <ConfigurationsInputRow label="Default Route"
            property_path="network.dhcp.default_route"
            property_obj={ this.state.data.current }
            field_change={ this.updateField }
            inherit_change={ this.toggleCheckboxField }
          />

          { _.some(errors) && this.renderErrors(errors) }

          <div className="row text-right actions">
            <div className="col-sm-12">
              { has_saved && (<span className="saved-on-message">Last Saved On: { last_saved_on }</span>) }
              <button className="btn btn-default" onClick={ this.cancel } disabled={ page_is_processing }>Cancel</button>
              <button className="btn btn-primary" onClick={ this.save } disabled={ page_is_processing }>Save</button>
              { page_is_processing && (<img className='wait-for right' src={ this.settings.urls.spinner_image } />)}
              { has_saved && !page_is_processing && (<span className='saved-successful-message wait-for right'><i className="fa fa-check"></i></span>)}
            </div>
          </div>
        </div>
      </fieldset>
    );
  }
}
