class ConfigurationsInternetRoutesTab extends React.Component {

  constructor(props) {
    super(props);

    this.settings = {
      fields_enabled: this.props.fields_enabled,
      model_key: 'internet_routes',
      ModalElement: ConfigurationsInternetRouteModal,
      labels: {
        model: 'Internet Route',
        model_plural: 'Internet Routes'
      },
      table_columns: [
        { label: 'IP Address', data_field: 'ip_address', is_key: true },
        { label: 'Subnet Mask/Prefix', data_field: 'subnet_mask' },
        { label: 'Description', data_field: 'description' }
      ],
      urls: {
        spinner_image: this.props.spinner_url,
        get: this.props.fetch_url,
        save: this.props.save_url
      }
    };
  }

  render() {
    return (
      <ListTabBaseComponent settings={ this.settings } />
    );
  }
}
