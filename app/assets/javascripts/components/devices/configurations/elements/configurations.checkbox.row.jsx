class ConfigurationsCheckboxRow extends React.Component {
  constructor() {
    super();

    this.empty_property = {
      options: [],
      errors: []
    };
  }

  renderErrors(errors) {
    return (
      <div className="errors">
        { errors.map((error, index) => {
          return (
            <div key={ index }>{ error }</div>
          )
        })}
      </div>
    )
  }

  render() {
    const { label, property_path, property_obj } = this.props;
    const { field_change, field_set, inherit_change } = this.props;
    const property = _.get(property_obj, property_path) || this.empty_property;

    return (
      <div className="row">
        <div className="col-sm-6">
          <label>{ label }</label>
          <div className="form-group">
            { property.inherited && (
              <input type="checkbox" className="form-control" checked={ property.parent } disabled />
            )}
            { !property.inherited && (
              <input type="checkbox" className="form-control" checked={ property.individual } onChange={ () => field_change(`${ property_path }.individual`) } />
            )}
            { _.some(property.errors) && this.renderErrors(property.errors) }
          </div>
        </div>
        <div className="col-sm-1 parent">
          <label>Inherit</label>
          <div className="form-group">
            <input type="checkbox" className="form-control" checked={ property.inherited } onChange={
                () => { inherit_change(`${ property_path }.inherited`);
                        if (property.individual === null) { field_set(`${ property_path }.individual`, false) }
                      }
                    }
            />
          </div>
        </div>
        <div className="col-sm-5">
          <label>{ label }</label>
            <input type="checkbox" className="form-control" checked={ property.parent } disabled />
        </div>
      </div>
    );
  }
}
