class ConfigurationsHeaderRow extends React.Component {
  render() {
    const { title, secondary_title } = this.props;
    const title_2 = secondary_title || title;

    return (
      <div className="row">
        <div className="col-sm-6">
          <h2>{ title }</h2>
        </div>
        <div className="col-sm-6 parent">
          <h2>{ title_2 }</h2>
        </div>
      </div>
    );
  }
}
