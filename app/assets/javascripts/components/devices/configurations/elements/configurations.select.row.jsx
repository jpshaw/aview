class ConfigurationsSelectRow extends React.Component {
  constructor() {
    super();

    this.empty_property = {
      options: [],
      errors: []
    };
  }

  renderErrors(errors) {
    return (
      <div className="errors">
        { errors.map((error, index) => {
          return (
            <div key={ index }>{ error }</div>
          )
        })}
      </div>
    )
  }

  render() {
    const { label, property_path, property_obj } = this.props;
    const { field_change, inherit_change } = this.props;
    const property = _.get(property_obj, property_path) || this.empty_property;

    return (
      <div className="row">
        <div className="col-sm-6 form-group">
          <label>{ label }</label>
          { property.inherited && (
            <select className="form-control" value={ property.parent || '' } disabled>
              <option></option>
              {
                property.options.map((option) => {
                  return (
                    <option key={ `parent_${ option.label }_${ option.value }` } value={ option.value }>{ option.label }</option>
                  );
                })
              }
            </select>
          )}
          { !property.inherited && (
            <select className="form-control"
              value={ property.individual || '' }
              onChange={ (event) => { field_change(event, `${ property_path }.individual`); } }>
              <option></option>
              {
                property.options.map((option) => {
                  return (
                    <option key={ `individual_${ option.label }_${ option.value }` } value={ option.value }>{ option.label }</option>
                  );
                })
              }
            </select>
          )}
          { _.some(property.errors) && this.renderErrors(property.errors) }
        </div>
        <div className="col-sm-1 parent">
          <label>Inherit</label>
          <div className="form-group">
            <input type="checkbox" className="form-control" checked={ property.inherited } onChange={ () => inherit_change(`${ property_path }.inherited`) } />
          </div>
        </div>
        <div className="col-sm-5 form-group">
          <label>{ label }</label>
          <select className="form-control" value={ property.parent || '' } disabled>
            <option></option>
            {
              property.options.map((option) => {
                return (
                  <option key={ `parent_${ option.label }_${ option.value }` } value={ option.value }>{ option.label }</option>
                );
              })
            }
          </select>
        </div>
      </div>
    );
  }
}
