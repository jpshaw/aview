class ListTabBaseComponent extends React.Component {
  constructor(props) {
    super(props);

    this.settings = this.props.settings;

    this.state = {
      show_new_model_modal: false,
      show_edit_model_modal: false,
      show_inhertiable: false,
      page_initialized: false,
      is_processing: false,
      has_saved: false,
      last_saved_on: null,
      data: {
        original: {},
        current: {}
      },
      selected_model: null
    };

    this.processingStart = this.processingStart.bind(this);
    this.processingStop = this.processingStop.bind(this);
    this.toggleShowInheritableData = this.toggleShowInheritableData.bind(this);
    this.setAsOverridden = this.setAsOverridden.bind(this);
    this.setAsInherited = this.setAsInherited.bind(this);
    this.parseServerResponse = this.parseServerResponse.bind(this);
    this.setLocalStateFromServerResponse = this.setLocalStateFromServerResponse.bind(this);

    this.modelClose = this.modelClose.bind(this);
    this.modelNew = this.modelNew.bind(this);
    this.modelEdit = this.modelEdit.bind(this);
    this.modelCreate = this.modelCreate.bind(this);
    this.modelUpdate = this.modelUpdate.bind(this);
    this.modelDelete = this.modelDelete.bind(this);
    this.modelInherit = this.modelInherit.bind(this);
  }

  componentWillMount() {
    AV.UTIL.AJAX.get(this.settings.urls.get, {}, (response) => {
      let new_state = this.state;
      let data = this.parseServerResponse(response);
      this.setLocalStateFromServerResponse(data);
    });
  }

  parseServerResponse(response) {
    let data = {
      models: response[this.settings.model_key],
      parent_models: response[`parent_${ this.settings.model_key }`],
      inherited: response.inherited
    };
    data.models.forEach((route) => {
      route.ui_id = _.uniqueId();
    });
    return data;
  }

  setLocalStateFromServerResponse(response) {
    let new_state = this.state;
    new_state.data.original = response;
    new_state.data.current = AV.UTIL.DATA.clone(response);
    new_state.page_initialized = true;
    this.setState(new_state);
  }

  processingStart() {
    let new_state = this.state;
    new_state.is_processing = true;
    this.setState(new_state);
  }

  processingStop() {
    let new_state = this.state;
    new_state.is_processing = false;
    this.setState(new_state);
  }

  toggleShowInheritableData() {
    let new_state = this.state;
    new_state.show_inhertiable = !new_state.show_inhertiable;
    this.setState(new_state);
  }

  setAsOverridden() {
    let new_state = this.state;
    new_state.data.current.inherited = false;
    this.setState(new_state);
  }

  setAsInherited() {
    const allow_deletions = confirm('Inheriting will delete your overrides. Continue?');
    if(!allow_deletions) { return; }

    this.processingStart();

    this.modelInherit()
      .then(() => {
        this.processingStop();

        let new_state = this.state;
        const saved_date_time = new Date();
        new_state.data.current.inherited = true;
        new_state.has_saved = true;
        new_state.last_saved_on = saved_date_time.toLocaleString();
        this.setState(new_state);
      });
  }

  modelClose() {
    let new_state = this.state;
    new_state.show_new_model_modal = false;
    new_state.show_edit_model_modal = false;
    new_state.selected_model = null;
    this.setState(new_state);
  }

  modelNew() {
    let new_state = this.state;
    new_state.show_new_model_modal = true;
    new_state.show_edit_model_modal = false;
    new_state.selected_model = null;
    this.setState(new_state);
  }

  modelEdit(model) {
    let new_state = this.state;
    new_state.show_new_model_modal = false;
    new_state.show_edit_model_modal = true;
    new_state.selected_model = model;
    this.setState(new_state);
  }

  modelInherit() {
    return new Promise((resolve, reject) => {
      AV.UTIL.AJAX.delete(this.settings.urls.save.replace(/.json/, '') + "/destroy_all", {}, (response) => {
        let data = this.parseServerResponse(response);
        this.setLocalStateFromServerResponse(data);
        return resolve({ model: [] });
      });
    });
  }

  modelCreate(model) {
    return new Promise((resolve, reject) => {
      AV.UTIL.AJAX.post(this.settings.urls.save, model, (response) => {
        if (response.errors) { return resolve({ errors: response.errors }); }

        let new_state = this.state;
        let new_model = response;

        new_model.ui_id = _.uniqueId();
        new_state.data.current.models.push(new_model);
        this.setState(new_state);

        return resolve({ model: new_model });
      });
    });
  }

  modelUpdate(model) {
    return new Promise((resolve, reject) => {
      AV.UTIL.AJAX.put(this.settings.urls.save, model, (response) => {
        if (response.errors) { return resolve({ errors: response.errors }); }

        let new_model = response;
        let new_state = this.state;

        const origin_model = _.find(new_state.data.current.models, (m) => { return m.ui_id == model.ui_id; });
        _.assignIn(origin_model, new_model);
        this.setState(new_state);

        return resolve({ model: origin_model });
      });
    });
  }

  modelDelete(model) {
    let new_state = this.state;
    let m = _.find(new_state.data.current.models, (m) => { return m.ui_id == model.ui_id; });
    m.is_processing = true;
    this.setState(new_state);

    return new Promise((resolve, reject) => {
      AV.UTIL.AJAX.delete(this.settings.urls.save, model, (response) => {
        if (response.errors) { return resolve({ errors: response.errors }); }

        let new_state = this.state;
        _.remove(new_state.data.current.models, (m) => { return m.ui_id == model.ui_id; });
        this.setState(new_state);

        return resolve();
      });
    });
  }

  render() {
    const { page_initialized, show_inhertiable, is_processing, has_saved, last_saved_on } = this.state;
    const { models, parent_models, inherited } = this.state.data.current;

    if(!page_initialized) {
      return (
        <div>
          <h3>
            <img src={ this.settings.urls.spinner_image } />
            Loading { this.settings.labels.model_plural }
          </h3>
        </div>
      )
    }

    const default_filter = { type: 'TextFilter' };

    return (
      <div>
        <div className="row">
          <div className="col-sm-8">
            <h2>
              { this.settings.labels.model_plural + ' ' }
              { !inherited && (
                <button className="btn btn-success"
                  onClick={ this.modelNew }
                  disabled={ !this.settings.fields_enabled }
                  >Add New Entry</button>) }
            </h2>
          </div>
          <div className="col-sm-4 text-right actions">
            { has_saved && (<span className="saved-on-message">{ last_saved_on }</span>) }
            <div className="btn-group" role="group">
                <button type="button" className={ `btn ${ !inherited ? 'btn-success' : 'btn-default' }` }
                  onClick={ this.setAsOverridden }
                  disabled={ !this.settings.fields_enabled || is_processing }
                  >Override</button>
                <button type="button" className={ `btn ${ inherited ? 'btn-success' : 'btn-default' }` }
                  onClick={ this.setAsInherited }
                  disabled={ !this.settings.fields_enabled || is_processing }
                  >Inherit</button>
            </div>
            { is_processing && (<img className='wait-for right' src={ this.settings.urls.spinner_image } />)}
            { has_saved && !is_processing && (<span className='saved-successful-message wait-for right'><i className="fa fa-check"></i></span>)}
          </div>
        </div>

        { !inherited && (<div>
        <h3>Device Override</h3>
        <BootstrapTable data={ models } striped hover pagination>
          { this.settings.table_columns.map((c, i) => {
            return (
              <TableHeaderColumn key={ i } isKey={ !!c.is_key } dataField={ c.data_field } dataSort filter={{ type: 'TextFilter', placeholder: `Filter by ${ c.label }` }}>{ c.label }</TableHeaderColumn>
            )
          })}
          <TableHeaderColumn dataField='ui_id' columnClassName="table-action-header"
            dataFormat={ (cell, row) => {
              return (
                <span className="text-center">

                  { row.is_processing && (
                    <img className="waiting-image" src={ this.settings.urls.spinner_image } />
                  )}

                  { !row.is_processing && (
                  <span>
                    <button className="btn btn-xs btn-success"
                      onClick={ () => { this.modelEdit(row) } }
                      disabled={ !this.settings.fields_enabled }>
                      <i className="fa fa-pencil"></i>
                    </button> &nbsp;
                    <button className="btn btn-xs btn-danger"
                      onClick={ () => { this.modelDelete(row) } }
                      disabled={ !this.settings.fields_enabled }>
                      <i className="fa fa-trash" />
                    </button>
                  </span>
                  )}

                </span>
              )
            }}
            >
              &nbsp;
            </TableHeaderColumn>
        </BootstrapTable>
        </div>)}

        <br />

        <h3>Default/Inherited</h3>
        <BootstrapTable data={ parent_models } striped hover pagination>
          { this.settings.table_columns.map((c, i) => {
            return (
              <TableHeaderColumn key={ i } isKey={ !!c.is_key } dataField={ c.data_field } dataSort filter={{ type: 'TextFilter', placeholder: `Filter by ${ c.label }` }}>{ c.label }</TableHeaderColumn>
            )
          })}
        </BootstrapTable>

        { this.state.show_new_model_modal && (
          <this.settings.ModalElement
            waiting_url={ this.settings.urls.spinner_image }
            show_modal={ this.state.show_new_model_modal }
            close_action={ this.modelClose }
            save_action={ this.modelCreate }
          />
        )}
        { this.state.show_edit_model_modal && (
          <this.settings.ModalElement
            waiting_url={ this.settings.urls.spinner_image }
            show_modal={ this.state.show_edit_model_modal }
            close_action={ this.modelClose }
            save_action={ this.modelUpdate }
            model={ this.state.selected_model }
          />
        )}
      </div>
    );
  }
}
