module ApplicationHelper

  def table_header_tooltip(title)
    content_tag(:i, '',
      class: 'fa fa-question-circle',
      title: title,
      'data-toggle' => 'tooltip',
      'data-placement' => 'top'
    )
  end

  # Public: Creates a list of device configurations available to the user for use
  # in the main navigation.
  #
  # Returns an ActiveSupport::SafeBuffer containing the device configuration list for
  #   the user.
  def device_configuration_list
    list = ""
    content_tag :ul, class: 'nav nav-second-level' do
      current_user.device_categories.pluck(:name).sort.each do |category_name|
        case category_name.downcase
        when 'cellular', 'dial-to-ip', 'remote manager', 'ucpe'
          if current_user.has_ability?('Modify Device Configurations')
            category_config_path = "device_configurations/#{category_name.downcase.gsub(/-|\s/, '_')}"
            list += content_tag :li, :class => active_path(category_config_path) do
              link_to category_name, "/#{category_config_path}"
            end
          end
        when 'legacy cellular'
          if current_user.has_ability?('Modify Configurations')
            list += content_tag :li, :class => active_path('legacy_configurations/index') do
              link_to DeviceCategory::LEGACY_CELLULAR_CATEGORY, legacy_configurations_path
            end
          end
        end
      end

      list.html_safe
    end
  end

  # Public: Checks if the given hash matches the controller and page. If the
  # action is an update, the page is pulled from the params; Otherwise,
  # `action_name` is used.
  #
  # This method is useful for pages that have a form and want to highlight a nav
  # button, even during post actions like `update` and `create`. In order to do
  # this the form must have a field called `page` passed with the name of the
  # desired action.
  #
  # opts - The Hash containing controllers and actions to match.
  #
  # Examples
  #
  #   controller_name = 'users'
  #   action_name     = 'show'
  #
  #   is_page_active?(users: [:show])
  #   # => true
  #
  #   is_page_active?('users' => [:show])
  #   # => true
  #
  #   is_page_active?('users' => ['permissions'])
  #   # => false
  #
  # Returns true if any of the given paths match; otherwise false.
  def is_page_active?(pages, _params = {})
    pages ||= {}
    post_actions = ['update', 'create']

    # Stringify values and allow access to keys with indifference.
    pages = pages.reduce({}) do |h, (k, v)|
      h.merge(k => v.map(&:to_s))
    end.with_indifferent_access

    # Set the correct page, based on the action type. We use the `page` param
    # instead of the action because there may be multiple nav items that react
    # to `update`, which results in highlighting them all.
    page = post_actions.include?(action_name) ? params[:page] : action_name

    page_active = pages[controller_name].present? && pages[controller_name].include?(page)

    if _params.present?
      _params.each do |k, v|
        unless params[k] == v
          page_active = false
          break
        end
      end
    end

    page_active
  end

  # Public: Checks if the path is active, and, if so, will return 'active'.
  #
  # opts - The Hash containing controllers and actions to match.
  #
  # Returns 'active' if any of the page paths match; otherwise it will return
  #   an empty string.
  def active_page(pages, _params = {})
    is_page_active?(pages, _params) ? 'active' : ''
  end

  # Public: Creates an array of organization options for use in a select box.
  #
  # opts - The Hash of options.
  #        :prepend_all - Prepends 'All' to the top of the list.
  #        :for         - The manager to show organizations for.
  #
  # Examples
  #
  #   organizations_select_list
  #   #=> [['Managed Organization 1', 1], ['Managed Organization 2', 2]]
  #
  #   organizations_select_list(for: @user)
  #   #=> [['Managed Organization 2', 2]]
  #
  #   organizations_select_list(for: @user, prepend_all: true)
  #   #=> [['All', 0], ['Managed Organization 2', 2]]
  #
  # Returns an Array of organization options for the given manager, to use in a
  #   select tag.
  def organizations_select_list(opts = {})
    opts[:prepend_all] ||= false
    opts[:for]         ||= current_user.filter_organization || current_user

    # In order to nil out the filter organization for a user, we need to set
    # the `all` value to anything less than the initial value for the `id` field.
    all_organizations_flag = 0

    list = managed_orgs_list_for(opts[:for])
    list.prepend(['All', all_organizations_flag]) if opts[:prepend_all]
    list
  end

  # Public: Displays a logout button for the user if the feature is enabled in the
  # Devise.logout_enabled variable
  def logout_link
    return unless Devise.logout_enabled
    content_tag :li do
      link_to destroy_user_session_path, method: :delete, id: 'logout' do
        content_tag(:i, nil, class: 'fa fa-power-off') +
        content_tag(:span, 'Logout')
      end
    end
  end

  # Public: Creates footer text from the variables in Settings.footer.
  #
  # Examples
  #
  #   # When git SHA is not available.
  #   "&copy; 2013 AT&T - Version 1.0.0"
  #
  #   # When git SHA is available,
  #   "&copy; 2013 AT&T - Version 1.0.0 - d14680c9488129805f65d46e5d5c4fd1242a5efc"
  #
  # Returns the footer text.
  def footer_text
    footer = []
    footer << copyright_year
    footer << footer_version
    footer << git_sha_link if Rails.env.staging?
    footer.compact.join(" - ").html_safe
  end

  def brandable_footer
    if current_user
      Rails.cache.fetch("#{current_user.organization.id}_footer") do
        current_user.footer_text
      end
    else
      Settings.footer
    end
  end

  def copyright_year
    "&copy; #{Time.now.year}"
  end

  def footer_version
    "Version #{app_version}"
  end

  # Public: Returns the application version.
  def app_version
    @app_version ||= Aview.version
  end

  # Public: Returns the sha link if available, else nil.
  def git_sha_link
    return if Settings.git.sha.nil?
    git_commit_path = "#{Settings.git.repo}/commits/#{Settings.git.sha}"
    link_to Settings.git.sha, git_commit_path, target: '_blank'
  end

  def first_page?

    # Assuming no page param is first page
    return true if params[:page].nil?

    if "#{params[:page]}".to_i == 0
      true
    else
      false
    end

  end

  # Public: Calls a nested method on an object. This is for safely getting a
  # value from an object that may not have the methods given in the chain.
  #
  # object       - The Object to get the value from.
  # method_chain - The String of methods to call on the object.
  #
  # Returns the value of the methods on the object, if valid; otherwise it
  #   will return an empty string.
  def nested_value(object, method_chain)
    begin
      method_chain.split(".").inject(object) { |obj, met| obj.send(met) }
    rescue NoMethodError => e
      ""
    end
  end

  # Public: Check if current action is to edit.
  #
  # Returns true if the current action is edit, otherwise false.
  def edit?
    params[:action] == "edit"
  end

  def users_enabled?
    Settings.users && Settings.users.enabled == true
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def action?(*action)
    action.include?(params[:action])
  end

  # Public: Create link to an object's show page.
  #
  # link_text - The String of the text to use, or the Symbol name of the method
  #             to use on the object.
  # path      - The Symbol path to the show page for the object.
  # obj       - The Object to show.
  # link_opts - A Hash of link_to options.
  #
  # Returns a link to the objects show page if valid, otherwise an empty string.
  def link_to_object_show(text, path, obj, link_opts = {})
    return "" if obj.nil? || !obj.respond_to?(:id) || obj.id.nil?

    default_text = "View"

    text = case text
           when String
             text
           when Symbol
             begin
               obj.send(text)
             rescue NoMethodError
               default_text
             end
           else
             default_text
           end

    begin
      link_to(text, send(path, obj), link_opts)
    rescue NoMethodError
      ""
    end
  end

  # Checks params for a list of selected orgs

  def selected_models
    return params[:model_ids] if params[:model_ids]
  end

  def selected_category?(category_id)
    if params[:categories]
      params[:categories].include?("#{category_id}")
    else
      true # show by default
    end
  end

  def selected_model?(model_id)
    if params[:models]
      params[:models].include?("#{model_id}")
    else
      true # show by default
    end
  end

  def previous_and_next(count, limit)
    return if count.blank? || limit.blank?

    # Guaranteed a positive integer value
    page          = params[:page].to_i.abs
    previous_page = page - 1
    next_page     = page + 1

    retval = ""

    if page > 0
      retval << capture { link_to "&laquo; Previous".html_safe, params.merge(page: previous_page), { class: 'btn btn-white', remote: true } }
    end

    if count >= limit
      retval << capture { link_to "Next &raquo;".html_safe, params.merge(page: next_page), { class: 'btn btn-primary pull-right', remote: true } }
    end

    retval.html_safe
  end

  def popover_data(field)
    title, content = case field.to_s
    when "name"
      ["Configuration Name", "Enter a name to call this configuration."]
    when "organization_id"
      ["Organization", "The Organization that this configuration will belong to, only devices in this organization will be able to use this configuration"]
    when "parent_id"
      ["Parent Configuration", "The Parent Configuration that this configuration will inherit values from, any blank values will be filled in while anything with a value will be left alone"]
    when "firmware"
      ["Firmware", "List of firmware versions"]
    when "img_3g"
      ["3G Firmware", "List of 3G firmware versions"]
    when "img_4g"
      ["4G Firmware", "List of 4G firmware versions"]
    when "hostname"
      ["Hostname","The Hostname of the NetBridge used for DHCP."]
    when "img_dx"
      ["DX Firmware", "Fill in the DX firmware you wish to use"]
    when "config_server"
      ["Config Server","This is the IP address or domain name of the NB configuration server. Multiple values can be configured, separated by commas. <br /><br />Example: cfg=config.accns.com, 97.76.62.140, portal.accns.com"]
    when "admin_access"
      ["HTTP Admin Access", "Enables or disables the web user interface"]
    when "admin_username"
      ["Admin User ID", "The username used for logging into the web interface (if enabled)"]
    when "admin_password"
      ["Admin Password", "The password used for logging into the web interface (if enabled)"]
    when "dst_enabled"
      ["Daylight Savings Time", "Enable or disable daylight savings time"]
    when "tz_offset"
      ["Time Zone", "Indicates how many minutes away from GMT the device is located."]
    when "netreach_configuration_wds_mode"
      ["WDS Mode", "Indicates if WDS mode is enabled and if it is, what mode it will run in. <br /><br />#{t(:mac_addresses)} are optional or required based on the WDS Mode value.<br /><br />
                    If WDS Mode = blank, all three fields must be blank<br /><br />
                    If WDS Mode = Disabled, all three fields must be blank<br /><br />
                    If WDS Mode = Bridge, then at least one #{t(:mac_address)} is required<br /><br />
                    If WDS Mode = Repeater, then at least one #{t(:mac_address)} is required<br /><br />
                    If WDS Mode = Lazy, then no #{t(:mac_address)} can be entered"]
    when "netreach_configuration_wds_mac_1"
      ["#{t(:mac_address)} #1", "#{t(:mac_address)} must be 12 characters in length and only contains 0-9 and A-F characters."]
    when "netreach_configuration_wds_mac_2"
      ["#{t(:mac_address)} #2", "#{t(:mac_address)} must be 12 characters in length and only contains 0-9 and A-F characters."]
    when "netreach_configuration_wds_mac_3"
      ["#{t(:mac_address)} #3", "#{t(:mac_address)} must be 12 characters in length and only contains 0-9 and A-F characters."]
    when "log_enabled"
      ["Enable Syslog", "Enable/Disable Syslog"]
    when "log_ipv4"
      ["IPv4 Address", "Indicates the IP address to send syslogs to.<br /><br /> Note: Port 514 must be accessible for syslogs to be received."]
    when "netreach_configuration_network_mode"
      ["Network Mode", "Network mode is the type of wireless network to run - G/N/B+G/B+G+N"]
    when "netreach_configuration_country_code"
      ["Country Code", "This is the ISO country code where the device will be operating"]
    when "netreach_configuration_channel"
      ["Channel", "Indicates which channel to use for the device.  Valid values are 0-14. <br /><br /> 0 = auto-channel select"]
    when "netreach_configuration_channel_region"
      ["Channel Region", "Indicates the channel region to use for the device. Valid values are 0-6."]
    when "netreach_configuration_rf_power"
      ["RF Power", "Controls the signal strength for the device's SSIDs.  Range is from 25-100 as a percentage."]
    when "proxy_host"
      ["Proxy Host IP", "The IP address of the Proxy Host. Used for sending syslog over HTTP support"]
    when "proxy_port"
      ["Proxy Port", "The port of the Proxy Host.  Used for sending syslog over HTTP support"]
    when "ssid_disabled"
      ["Enable/Disable SSID", "Indicates whether the SSID of the device is enabled or disabled"]
    when "ssid"
      ["SSID Name", "The broadcasted name of the SSID"]
    when "ssid_advertise"
      ["Advertise SSID", "Indicates if the SSID should be advertised"]
    when "vlan_id"
      ["Vlan ID for SSID", "This is the VLAN number to be associated with the SSID. <br /><br />
                    This is an optional field.  The valid values are 1-4050"]
    when "netreach_configuration_ssid_sec"
      ["Security", "Indicates what encryption method to use"]
    when "netreach_configuration_ssid_wep_len"
      ["WEP Key Length", "This specifies the key length when using WEP security"]
    when "netreach_configuration_ssid_pass_t"
      ["Passprase Type", "The format of the passphrase.  This field is required when using any security other than 'None'. <br /><br />
                    Note:  Hexadecimal can only be selected when using WEP security"]
    when "netreach_configuration_ssid_wpa_c"
      ["WPA-WPA2 Cipher", "This specifies the key cipher to use when using WPA or WPA2 security"]
    when "netreach_configuration_ssid_pass"
      ["Passphrase", "This is the passphrase used to configure the connection to the SSID.<br /><br />
                    This field is required when using any security other than 'None'"]
    when "netreach_configuration_ssid_ipv4"
      ["Radius Server Address", "The IP address of the server used for WPA2 Enterprise (RADIUS) authentication"]
    when "netreach_configuration_ssid_port"
      ["Radius Server Port", "The port of the server used for WPA2 Enterprise (RADIUS) authentication<br /><br />
                    Valid values are 1-65535"]
    when "netreach_configuration_ssid_timeout"
      ["Radius Session Timeout", "The session timeout value for the server used for WPA2 Enterprise (RADIUS) authentication<br /><br />
                    Valid values are 10-9999 in seconds"]
    when "netreach_configuration_ssid_secret"
      ["Radius Shared Secret", "The shared scret value for the server used for WPA2 Enterprise (RADIUS) authentication<br /><br /> NOTE:  the minimum length is 5"]
    when "firmware_server"
      ["Firmware Server","Only one server IP or DNS can be specified in this value. If you want to set multiple lists, use the cfg value, which can be used to download config files and firmware files"]
    when "iui"
      ["Ignore Unsolicited Inbound","If enabled, the unit will enable a firewall that blocks any unsolicited traffic from the WAN (internet) side."]
    when "png"
      ["Ping Test List", "The unit will periodically ping a server on the WAN (internet) to ensure the link is operational; this configures which server it will ping. A space-delimited list of ip addresses or fqdns can be specified, and the unit will try cfg.ips_rc_home_ep followed by each one in this list until it gets a succesfull reply. At least one ip address should be given here in case DNS is broken, but the link is otherwise alive. To disable link checking, set this to 127.0.0.1. <br /><br />Default: 8.8.8.8 184.106.215.57 50.28.67.96"]
    when "rbt"
      ["Reboot nightly","Reboot nightly after scheduled maintenance operations."]
    when "remote_control"
      ["Enable Remote Control Tunnel","Enables or disables the remote control functionality of the NetBridge. Enabled by default"]
    when "sms_check_freq"
      ["SMS Check Frequency","The device will check its SMS messages periodically. This field allows you to specify the length of the period. The value is represented as NB where N is a numberical value and B is the time base. NB means once every N B. For instance, '4m' means the ping test will run once every 4 minutes. B can be m,h, or d for minutes, hours, days. N has to make sense (0-59 for minutes, 0-24 for hours). Set to 0 to disable. <br /><br />Default: 1m"]
    when "upload_server"
      ["Upload Server","IP address or fqdn of server that holds upload logs"]
    when "var"
      ["Variant","Variant setting of NetBridge A - All (default) M - Mobility mode S - SMX/Service Manager mode"]
    when "wdog"
      ["Enable Watchdog","If disabled, disables wdog timer in NetBridge. Default is enabled"]
    when "config_change_reboot"
      ["Reboot immediately when config changes.","Some config options require a reboot to apply changes. The reboot will result in possibly several minutes of downtime. Setting this option to 0 prevents rebooting for config chagnes; those changes will get applied the next time the unit reboots for other reasons. Setting this to 1 makes the NetBridge reboot immediately if that is needed to apply changes. Anything other than 0 or 1 keeps the default behavior. Default: Enabled"]
    when "sticky_apn"
      ["Keep Configured APN sticky", "If the NetBridge cannot connect using the configured APN, it will attempt to connect to a list of fallback APNs in order to get a connection and download a config. If sticky_apn is 0, the NetBridge will stay connected with the backup APN. If sticky_apn is 1, the NetBridge will try to reconnect using the configured APN. Default: 1 - reconnect using the configured APN"]
    when "xma"
      ["Modem APN","Modem APN. Overrides SMX or web configuration if and only if xmc or xmt values match operational environment."]
    when "xmc"
      ["Modem Carrier","Modem Carrier. Used to gate xma/xmu/xmp/xmi values. Supported values include 'at&t', 'sprint', and 'verizon'."]
    when "xmi"
      ["Modem Initialization (AT command string)","Modem Initialization (AT command string). Overrides SMX or web configuration if and only if xmc or xmt values match operational environment."]
    when "xmp"
      ["Modem Password", "Modem Password. Overrides SMX or web configuration if and only if xmc or xmt values match operational environment."]
    when "xmt"
      ["Modem Type", "Modem Type. Used to gate xma/xmu/xmp/xmi values. A typical value might be 'sw885' for Sierra Wireless 885."]
    when "xmu"
      ["Modem Userid", "Modem Userid. Overrides SMX or web configuration if and only if xmc or xmt values match operational environment."]
    when "link_check_freq"
      ["Link Check Frequency","The unit will periodically check to make sure the link the WAN (internet) link is operational; this configures how frequent those check occur. It does this by checking if any traffic has come inbound from the lan since the last check; and if it hasn't, it pings a server on the WAN (configured by cfg.png). Link check frequency (lcf) is represented as NB where N is a numberical value and B is the time base. NB means once every N B. For instance, '4m' means the ping test will run once every 4 minutes. B can be m,h, or d for minutes, hours, days. N has to make sense (0-59 for minutes, 0-24 for hours). Set to 0 to disable. Default: 4m"]
    when "link_timeout"
      ["Link Timeout", "The unit will periodically ping a server on the WAN (internet) to ensure the link is operational; this configures how long in seconds failed pings can occur before the link is considered dead. Default: 900"]
    when "traf_acct"
      ["Traffic Accounting Enable","Enables tracking the number of bytes sent over each link, also separating out syslog, http, and other types of traffic. Default: disabled"]
    when "proxy_arp"
      ["Proxy ARP","Allows downstream devices to reach hosts in the upstream network within the same subnet. Enabled by default."]
    when "ips_cust_dh_grp"
      ["IPSec Custom Diffie Helman Group","Diffie Hellman group for both IPsec phase 1 and phase 2. Required for static IP feature Default - 1"]
    when "ips_cust_home_ep"
      ["IPsec Custom Home Endpoint","The unit will attempt to establish an IPsec tunnel with this server. Default: none"]
    when "ips_cust_home_net"
      ["IPsec Custom Home Network","The unit will setup an IPsec policy for traffic between cfg.ips_cust_home_net and cfg.ips_cust_remote_net. The 'home' net is the network behind the IPsec server we are connecting to. Default: none"]
    when "ips_cust_keyid_tag"
      ["IPsec Custom Keyid Tag","The NetBridge will use this string as its keyid tag identifier in establishing an IPsec SA with ips_cust_home_ep"]
    when "ips_cust_psk"
      ["IPsec Private Shared Key, Ecnrypted","The unit will use this psk when establishing an IPsec SA with ips_cust_home_ep. The string is an rsa-encrypted (with the unit's public key) version of the actual psk it will use. Default: none"]
    when "ips_cust_remote_ep"
      ["IPsec Custom Remote Endpoint", "The unit will use this address to establish an IPsec SA with ips_cust_home_ep. Default: none"]
    when "ips_cust_remote_net"
      ["IPsec Custom Remote Network","The unit will setup an IPsec policy for traffic between cfg.ips_cust_home_net and cfg.ips_cust_remote_net. The 'remote' net is the network behind the unit. Default: none"]
    when "ips_cust_xauth_pass"
      ["IPsec XAUTH Password", "The XAUTH password for a custom IPsec setup."]
    when "ips_cust_xauth_user"
      ["IPsec XAUTH username", "The XAUTH username for a custom IPsec setup."]
    when "ips_keepalive"
      ["IPsec Keepalive","The time between keepalive messages sent to keep nat traversal working. See Racoon config for valid settings. Default: 45 sec"]
    when "tus"
      ["Tunnel Server IP address","Default - 184.106.213.137"]
    when "vpn_cust_type"
      ["Custom VPN Type","The type of connection for a custom VPN."]
    when "dhcp_lease_max"
      ["DHCP lease max","DHCP clients may request a specific duration for the lease. The default behavior is to honor that request. This sets the maximum allowed lease time, in minutes. If the setting is 10 and the client resquests a lease of 24 hours, the dhcp server will give a lease of 10 minutes. If the settings is 10 and the client requests a lease time of 7 minutes, the dhcp server will give a lease of 7 minutes. If the client does not include the lease time option in the request, the dhcp server will use the dhcp_lease_minutes setting."]
    when "dlm"
      ["DHCP lease minutes","DHCP lease minutes. The DHCP requery interval is half of this value. A value of auto typically means 1440 minutes (24 hours)."]
    when "dre"
      ["DHCP Range End","The unit will assign addresses from the range cfg.drs to cfg.dre. Default: passed through from carrier"]
    when "drn"
      ["DHCP Network","The unit will send this as the network address in a DHCP transaction. It also uses this to setup routing to the client boxes. Default: passed through from carrier, based on the given ip with /24 prefix"]
    when "drp"
      ["DHCP Prefix","The unit will use this to determine the netmask sent in a DHCP transaction. It also uses this to setup routing to the client boxes. Default: 24"]
    when "drs"
      ["DHCP Range Start","The unit will assign addresses from the range cfg.drs to cfg.dre. Default: passed through from carrier"]
    when "eth"
      ["Ethernet port auto-negotiation control","Ethernet port auto-negotiation control. If not auto, then full-duplex or half-duplex may be combined with 10 or 100 Mbps."]
    when "gip"
      ["Gateway IP","The unit will take this address as its own and set the dhcp option 'router' to this value."]
    when "gip_secondary"
      ["Secondary Gateway IP","The gateway IP will have a second default IP in the same subnet as the wanIPv4 address, except it's xxx.xxx.xxx.1 (Disabled by default)"]
    when "mtu"
      ["TCP/IP maximum transmission unit","TCP/IP maximum transmission unit. If specified, all interfaces will be configured to use the specified value, otherwise they will use their respective default settings."]
    when "ddns"
      ["Dynamic DNS Update URL","Most Dynamic DNS services can be updated by simply by fetching a resource at a URL. The NetBridge will get the page at this URL every time it connects to the network."]
    when "dnslist"
      ["DNS List","The unit applies the given list of DNS addresses (must be in CSV format). Any DNS value configured here overrides the DNS values obtained from a PPP connection"]
    when "rat"
      ["Radio access technology","Radio access technology. Can be used to force the WWAN link to use a specific network type.  auto - automatically connect to the highest available network.  4g - only connect on 4G networks (LTE, WiMAX, or HSPA+).  3g - only connect on 3G networks.  2g - only connect on 2G networks."]
    when "ippassthrough"
      ["IP Passthrough","Place modem into IP passthrough mode or into standard router mode. Currently applies to the 340U and UML295 modems. (Enabled by default)"]
    when "usb_adjust"
      ["USB Mode","blank (default) - Switches between from high speed to full speed USB mode depending on EMI error activity. If the device reboots, it will go back to high-speed mode. 0 - USB high-speed mode (480) only. 1 - USB full-speed mode (12) only."]
    when "ota_updates"
      ["OTA Updates","Controls over the air updates on the cellular modem. Currently applies to the 341U and 340U modems. 0 - disabled, 1 - enabled. Default: disabled"]
    when "ptf"
      ["Port Forward", "Applies to firmware versions prior to 1.391.91. For later versions use 'ptfr'. The unit can forward TCP and UDP ports to specific addresses specified in this list. Example: ptf=192.168.254.2:t-22,t-8081;192.168.254.3:t-22,t-8081 Where 192.168.254.2 is the IP address t-22 is port 22 TCP (for UDP, it would be u-22) Default: none ptf=192.168.1.71:t-1071 ptf=192.168.1.72:t-1072"]
    when "ptfr"
      ["Port Forwarding and Redirection","Applies to firmware versions after 1.391.91. For earlier versions use 'ptf'. This setting is used to forward incoming WAN traffic to specific hosts on the LAN. The syntax is as follows: t-PORT|u-PORT|p-PROTOCOL_NUMBER DESTINATION_IP[:DESTINATION_PORT],... Example: t-80 192.168.1.4:8000, u-500 192.168.1.1, p-50 192.168.1.1 The example sets redirects tcp port 80 to port 8000 on 192.168.1.4, forwards udp port 500 to 192.168.1.1, and forwards esp (ip protocol 50) to 192.168.1.1"]
    when "log"
      ["Default Syslog Address","Address of syslog collector. Used when no Primary and/or secondary syslog list is specified."]
    when "log_level"
      ["Log Level","The unit sends syslog information for diagnosing trouble. Currently, the only gated messages are ping statistics from the link checker. Set to >= 5 to see ping statistics. Default: none"]
    when "log_list"
      ["Primary Syslog Addresses List","Option can contain multiple IPs or domain names for syslogging (at least two values must be specified). If one is unreachable, the unit will try the next in the list. List of values must be separated by commas. This overrides the value set in the 'log' option"]
    when "log_list2"
      ["Secondary Syslog Addresses List","Option can contain multiple IPs or domain names for syslogging (at least two values must be specified). If one is unreachable, the unit will try the next in the list. List of values must be separated by commas."]
    when "syslog_http"
      ["Sylog over HTTP server","If the unit can only reach the general internet via HTTP proxy, it must send logs to a server. This can be an IP address or fqdn."]
    when "config_int_secs"
      ["Configuration Check Interval","Check for new configuration.  Value is measured in minutes.  Valid values are 10-535680"]
    when "wds_pass_phrase"
      ["WDS Passphrase","WPA Preshared Key for WDS network"]
    when "wds_encryption"
      ["WDS Security Type","The security type used by the WDS network.  Valid values are none (open), WPA2 (TKIP), or WPA2 (AES)"]
    when "wds_phy_mode"
      ["WDS Network Type","The WDS network type.  Valid values are 802.11b (CCK), 802.11g (OFDM), or 802.11n (HTMIX)"]
    when "netreach_configuration_remote_control"
      ["Remote Control Check Interval","NetReach will check for remote control flags within the listed interval. Valid values are 0-59 minutes. Setting to 0 disables this feature."]
    when "station_query"
      ["Station Query","Scan for nearby wireless APs (access points)"]
    when "anms_enabled"
      ["ANMS Configuration Enabled","Use ANMS Configuration profile"]
    when "client_monitor"
      ["Client Monitor","Monitor client devices connected to SSIDs"]
    when "upnp_enabled"
      ["UPNP Enabled","Toggle UPnP feature (0-disabled,1-enabled)"]
    when "wan_connection"
      ["WAN Connection Type","Configure WAN connection for DHCP or Static IP. If user selects STATIC, then they must provide a WAN IP address, Gateway IP address, and Netmask. If user selects DHCP, then no other WAN IP options should be selected/entered"]
    when "wan_ip_address"
      ["WAN IP Address","Static IP address for NetReach's WAN interface"]
    when "wan_gateway_ip"
      ["WAN Gateway IP","Gateway IP address for NetReach's WAN interface"]
    when "wan_netmask"
      ["WAN Netmask","Netmask for NetReach's WAN interface"]
    when "dns1"
      ["DNS 1","Primary DNS for NetReach's WAN interface"]
    when "dns2"
      ["DNS 2","Secondary DNS for NetReach's WAN interface"]
    when "conf_insecure_ssl"
      ["SSL Override"," Enables/disables SSL validation of the configuration server. Valid values are: 0 - enable SSL validation, 1 - disable SSL validation. Default - 0."]
    when "crt_url"
      ["Certificate URL","The 6200-FX uses a signed unique certificate to validate the file it gets when downloading its central configuration. This option specifies the URL the 6200-FX uses to send the certificate signing request to. (default URL is https://aview.accns.com:443/certificates/v2)."]
    when "dhcp_enable"
      ["DHCP Enabled","Controls the DHCP server on the device (0 - disabled, 1 - enabled; enabled by default)."]
    when "status_freq"
      ["Status Update Frequency","The unit will periodically send in a syslog message with updated status details (signal strength, data usage, etc.). This configuration option specifies how often the unit sends in a status update message. Link check frequency (lcf) is represented as NB where N is a numerical value and B is the time base. NB means N times per B. B can be m,h, or d for minutes, hours, days. N has to make sense (0-59 for minutes, 0-24 for hours). Set to 0 to disable. Default: 30m"]
    when "grace_period_duration"
      ["Grace Period Duration","Additional time, after a Status Update Frequency has not been
        received, that the system will wait to flag a device as being down. This value is represented
        as NB, where N is a numerical value and B is the time base. B can be w, d, h, m or s for
        weeks, days, hours, minutes or seconds. More than one value can be used (e.g.: 1w2h
        to wait for 1 week and 2 hours). Order is not important and time bases can be repeated, in
        which case they will be added during calculations (e.g.: 1d1d is the same as 2d)."]
    when "analytics_mode"
      ["Analytics Mode","Log information on other nearby wireless APs and unassociated client devices.  Note: the NetReach's SSIDs will be unavailable if Analytics Mode is enabled."]
    when "threshold_percentage"
      ["Threshold Percentage","Signal strength threshold percentage below which a Signal Strength Changed event will be generated"]
    when "support_contact_list"
      ["Support Contact List","A comma seperated list of email addresses to which feedback emails will be sent."]
    when "footer"
      ["Footer", "The footer can be either text or valid HTML. This field is optional."]
    else
      ["Title", "Content"]
    end
    {:title => title, :content => content}
  end

  def org_dropdown_selected
    retval = "Organization Filter"
    if params[:organization_id]
      if organization = Organization.select('name').find_by(id: params[:organization_id])
        if organization.name
          retval = "#{organization.name}"
        end
      end
    end
    retval
  end




  def filter_link(text, filters={}, klass = "")
    params_to_keep = [:range, :facility, :alert_type, :level, :organization_id, :device_type, :event_type]
    params_to_keep.each do |v|
      filters[v] ||= params[v] if params[v]
    end
    link_to text, filters, class: klass
  end

  # Public: Check if an explicit active path is set.
  #
  # Examples
  #
  #   get "/account"
  #   <%= active_path('account/index') %>
  #   # => "active"
  #
  # Return 'active' if path is currently set, otherwise returns an empty string.
  def active_path(path = "")
    current_path = controller.controller_name + "/" + controller.action_name
    path == current_path ? "active" : ""
  end

  # Sets nav active class for pages
  def active?(pages = [])
    is_active = false
    if pages.kind_of? String
      pages = ([] << pages)
    end
    if pages.kind_of? Array
      if pages.include? controller.controller_name
        is_active = true
      elsif pages.include? controller.action_name
        is_active = true
      elsif pages.include? params[:type]
        is_active = true
      end
    end
    return (is_active ? "active" : "")
  end

  def active_for_param(key, value)
    is_active = false
    case value
    when String
      if params[key] == value
        is_active = true
      end
    when Array
      value.each do |val|
        if params[key] == val
          is_active = true
          break
        end
      end
    end
    return (is_active ? "active" : "")
  end

  # Sorters
  def sortable(column, title = nil)

    title ||= column.titleize

    css_class = column == sort_column ? "sorted #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "desc" ? "asc" : "desc"
    link_to title, params.merge(:sort => column, :direction => direction), { :class => css_class }
  end

  def trunc(text = "", length = 15)
    truncate(text, :length => length)
  end

  def get_maint_exp
    AdminSettings.find_by(var: "maint_message_expiration")
  end

  def maint_message(skip_exp_check: false)
    exp_date = maint_expiration
    msg = AdminSettings.find_by(var: "maint_message")
    ignore_exp = exp_date.blank?
    return "" unless exp_date.present? || skip_exp_check || ignore_exp
    expired = skip_exp_check || ignore_exp || exp_date.to_i > Time.now.to_i ? false : true;
    msg.nil? || expired ? "" : msg.value.html_safe
  end

  def maint_expiration
    maint_exp = get_maint_exp
    maint_exp.blank? ? "" : maint_exp.value
  end

  def is_armt?
    Engine.name == "armt"
  end

  def organization_options
    orgs = current_user.accounts.map do |org|
      org.name
    end
    orgs.sort! {|a,b| a.downcase <=> b.downcase }
    orgs.to_json.html_safe
  end

  def timezones_for_js
    values = {}
    Timezone.all.each { |zone| values[zone.official_name] = ActiveSupport::TimeZone::MAPPING[zone.name] }
    values.to_json.html_safe
  end

  private

  def logo
    Rails.cache.fetch("#{current_user.organization.id}_logo") do
      current_user.logo_path
    end
  end

end
