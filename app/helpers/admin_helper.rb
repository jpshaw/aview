# Public: Helper methods for admin related views.
module AdminHelper

  # Public: Check if the current user is allowed to view the admin panel.
  #
  # Returns true if the user is permitted to view the admin panel;
  #   otherwise false.
  def can_view_admin_panel?
    AdminControllerPolicy.new(current_user, self).view?
  end
end
