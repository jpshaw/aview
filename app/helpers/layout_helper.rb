module LayoutHelper
  def current_organization_id
    current_user.filter_organization.try(:id)
  end

  def current_organization_name
    if current_user.filter_organization.present?
      current_user.filter_organization.name
    else
      'All Organizations'
    end
  end

  def filter_dropdown_class
    :active if current_user.filter_organization.present?
  end

  def flash_messages(exceptions = [])
    flash.to_hash.with_indifferent_access.except(*exceptions)
  end

  def hierarchy_state
    params[:exclude_hierarchy]
  end
end
