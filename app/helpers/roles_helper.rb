module RolesHelper
  # Public: Returns a list of roles for the given organization that can be used
  # in a `select` box.
  def roles_list_for(organization)
    return [] unless organization.present?
    organization.roles.order('roles.name asc').map { |r| [r.name, r.id] }
  end
end
