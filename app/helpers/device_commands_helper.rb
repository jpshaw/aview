module DeviceCommandsHelper
  def command_modal_for(command, name = nil)
    param_name = "#{name || command}".parameterize

    content_tag :div, id: "#{param_name}-command", class: 'modal inmodal form-horizontal', role: 'dialog',
        'tabindex' => '-1', 'aria-labelledby'=> "#{param_name}-command-label", 'aria-hidden' => 'true' do
      content_tag :div, class: 'modal-dialog' do
        content_tag :div, class: 'modal-content' do
          form_for :device, url: remote_device_path(@presenter.device), method: :get, html: {id: command} do |f|
            content_tag(:div, class: 'modal-header') do
              content_tag(:button, '&#215;'.html_safe, class: 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true') +
              content_tag(:h3, "Send #{name.try(:titleize)} Command", id: "#{param_name}-command-label", class: 'modal-title')
            end +

            content_tag(:div, class: 'modal-body') do
              yield
            end +

            content_tag(:div, class: 'modal-footer') do
              content_tag(:button, 'Close', class: 'btn btn-white', 'data-dismiss' => 'modal', 'aria-hidden' => 'true') +
              f.submit('Send Command', class: 'btn btn-primary command-submit-btn')
            end
          end
        end
      end
    end
  end
end
