module DevicesHelper
  extend Memoist

  # Public: Sets up a link_to for a device's IP Address if available.
  #
  # device - A Device Object.
  #
  # Returns a link_to string if host available, otherwise an empty string.
  def host_link(device)
    device.host.blank? ? "" : link_to(device.host, "https://#{device.host}")
  end

  def reverse_proxy_link(device)
    return 'N/A' unless Settings.reverse_proxy.enabled?

    link_to reverse_proxy_host(device),
      "https://#{reverse_proxy_host(device)}",
      target: '_blank'
  end

  def reverse_proxy_host(device)
    "#{device.mac}.#{Settings.reverse_proxy.domain}"
  end

  def terminal_link(device)
    return 'N/A' unless Settings.terminal.enabled?

    link_to "Terminal on Unit",
      terminal_device_path(device)
  end

  def network_tunnels(device)
    device.ipsec_networks
  end

  def lan_networks(device)
    device.networks.where(net_type: 'lan')
  end

  def lan_port_image(status)
    if status == 'Connected'
      "lan_port_up.png"
    else
      "lan_port_down.png"
    end
  end

  def vlan_details(network)
    details = ''
    return details unless network.present?
    details << "Status: #{network.status}\n"
    details << "Speed: #{network.speed}\n"
    details << "RX: #{Filesize.as_si(network.rx).pretty}\n"
    details << "TX: #{Filesize.as_si(network.tx).pretty}\n"
    network.ip_addrs.try(:each) do |vlan, ip_addr|
      details << "#{vlan.upcase}: #{ip_addr}\n"
    end
    details
  end

  def modem_identifier(modem)
    if modem.sim_slot.present?
      "SIM Slot #{modem.sim_slot}"
    else
      modem.model
    end
  end

  def progress_bar_for_status(status)
    status ? "progress-bar" : "progress-bar progress-bar-striped"
  end

  def device_configurations(device = @device, organization = @organization)
    return [] unless organization.present?
    configurations = [[t('configurations.no_configuration_option'), -1]]
    configurations += configurations(organization, device).map do |configuration|
      uses_models  = configuration.respond_to?(:device_model)
      has_model    = uses_models && configuration.device_model.present?
      models_match = has_model && configuration.device_model.id == device.series.id

      # Note: The `!uses_models` below regards the Legacy Cellular configurations,
      # which do not have models.
      (!uses_models || models_match) ? [configuration.name, configuration.id] : nil
    end.compact
  end

  def netbridge_configurations
    current_user.netbridge_configurations.preload(:organization)
  end

  def devices_policy
    Pundit.policy(current_user, Device)
  end
  memoize :devices_policy

  def can_modify_device_configuration?(device)
    key = "users/#{current_user.id}/abilities/change_configuration/#{device.id}"

    Rails.cache.fetch(key, expires_in: 30.minutes) do
      can?(current_user, :change_configuration?, device)
    end
  end

  # Public: Checks if the current user can modify devices.
  def can_modify_devices?
    devices_policy.modify?
  end
  memoize :can_modify_devices?

  def can_modify_device_hosts?
    devices_policy.modify_hosts?
  end
  memoize :can_modify_device_hosts?

  def can_modify_device_contacts?
    devices_policy.modify_contacts?
  end
  memoize :can_modify_device_contacts?

  def can_modify_device_comment?
    devices_policy.modify_comment?
  end
  memoize :can_modify_device_comment?

  def can_view_device_admin_panel?
    @policy.modify?
  end
  memoize :can_view_device_admin_panel?

  def can_send_commands?
    @policy.send_commands?
  end
  memoize :can_send_commands?

  # Return the text to show in the deploy button
  def deploy_text(device = @device)
    return unless device.present?
    device.deployed? ? 'Undeploy' : 'Deploy'
  end

  # Return the css class to use for the deploy button
  def deploy_class
    return unless @device.present?
    @device.deployed? ? 'danger' : 'primary'
  end

  def deploy_icon(device = @device)
    "<i class='fa fa-#{deploy_icon_name(device)}'></i>".html_safe
  end

  def deploy_icon_name(device = @device)
    device.deployed? ? 'pause' : 'play'
  end

  # Look for a device panel for the show page. If not found, return nil.
  def device_show(name, device = @device, opts = {})
    render partial: "/devices/#{device.class.to_s.underscore}/#{name}", locals: opts
  rescue ActionView::MissingTemplate => e
    logger.warn "Failed to load partial `#{name}`: #{e.message}"
    return nil
  end

  # Create a link for remote commands
  def command_link(title, device = @device, opts = {})
    # uClinux Devices that send json based commands need a device's user and
    # password, which is entered using a modal (with ID 'sendCommandModal').
    # Other devices don't need user and password, in which case a regular link
    # is issued instead.
    if device.is_platform?(:uc_linux) && device.remote_control_disabled?
      link_to title, '#sendCommandModal', data: {toggle: "modal"}.merge(opts), class: "send_command #{device.platform}"
    else
      link_to title, remote_device_path(device, remote_data: opts), class: "send_command"
    end
  end

  # Create a link for sms commands.  This bypasses the check for JSON commands
  # done in the `command_link` method since SMS commands aren't sent directly
  # to the device.
  def sms_command_link(title, device = @device, opts = {})
    link_to title, remote_device_path(device, remote_data: opts), class: "send_command"
  end

  # Create tr/td element for details table
  def table_data_row(opts = {})
    key   = content_tag :td, "#{opts[:key]}:", class: 'key'
    value = content_tag :td, (opts[:value] || 'N/A'), class: opts[:value_class]

    content_tag :tr do
      key + value
    end
  end

  # If the user doesn't have the ability to edit the device then show the basic page
  def show_basic_data?(policy = @policy)
    return true if policy.blank?
    !policy.edit?
  end

  def config_checked_at(device)
    if device.configuration_checked_at.present?
      "Last checked at #{timestamp_in_users_timezone(device.configuration_checked_at)}"
    end
  end

  def edit_group_configuration_path(configuration)
    configuration.is_a?(DeviceConfiguration) ?
      edit_device_configuration_path(configuration) :
      edit_legacy_configuration_path(configuration)
  end

  def device_input_class(klass, device = @device)
    if device && device.is_smx_device?
      klass + ' text-uppercase'
    else
      klass
    end
  end

  def device_input_maxlength(device, options = {})
    if device.is_smx_device? && options[:smx]
      options[:smx]
    else
      options[:default]
    end
  end

  private

  def configurations(organization, device)
    organization.__send__("#{device.object_type.underscore}_configurations".to_sym)
  rescue NoMethodError
    # No specific configuration association for the device type (DialToIp or RemoteManager). Use
    # device_configurations association.
    organization.device_configurations.where(record_type: DeviceConfiguration.record_types[:collection])
  rescue
    # Other error.
    []
  end

  def the_device_configuration_path(device_configuration)
    device_configuration.class == DeviceConfiguration ?
      device_configuration_path(device_configuration) :
      legacy_configuration_path(device_configuration)
  end
end
