module PaginationHelper

  def pagination_for(key, opts = {})
    key = "#{key}_pagination_limit".to_sym
    pagination_form key, current_user.send(key), opts
  end

  def pagination_form(field, limit, opts = {})
    opts[:limits] ||= pagination_limits

    content_tag :div do
      form_for :user, url: change_account_index_path, method: :patch do |f|
        ("Show " + f.select(field, options_for_select(opts[:limits], limit), {},
          class: 'select2-without-search form-trigger')).html_safe
      end
    end
  end

  def pagination_limits
    [10, 25, 50, 100]
  end

  def pagination_links_for(records)
    unless records.empty?
      content_tag :div, :class => "pagination alternate" do
        will_paginate records
      end
    end
  end
end
