module PolicyHelper
  def can_edit?(klass)
    return false if klass.nil?
    @can_edit ||= {}
    @can_edit[klass.to_s] ||= policy(klass).edit?
  end
end
