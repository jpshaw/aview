module CellularDevicesHelper

  def modem_name(device = @device)
    device.modem_name.present? ? device.modem_name : 'Unknown'
  end

end
