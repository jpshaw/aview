module GatewayHelper

  # Public: Checks if a given interface has a cell extender provisioned 
  # for the given device.
  #
  # interface - The String or Symbol interface name.
  # device    - The Device to check (default: @device).
  #
  # Returns true if the device has a value for extender on the given
  # interface; otherwise false.
  def has_cell_extender?(interface, device = @device)
    return false if device.details.blank?

    field = "#{interface}_cell_extender"
    return false unless device.details.respond_to?(field)

    device.details.send(field).present?
  end

  def has_tunnel_info?(device = @device)
    device.respond_to?(:tunnel_info)
  end

  def get_tunnel_class(device = @device, classes = {up: "status-up", warning: "status-notice", down: "status-down", default: ""})
    return " " if !has_tunnel_info?(device)
    tunnel_info = device.tunnel_info
    case tunnel_info[:severity_level]
      when 0
        classes[:up]
      when 1..5
        classes[:warning]
      when 6..10 
        classes[:down]
      else 
        classes[:default]
      end
  end

  def get_tunnel_text(device = @device)
    return " " if !has_tunnel_info?(device)
    tunnel_info = device.tunnel_info
    if !tunnel_info.empty?
      "#{tunnel_info[:tunnels_up]} / #{tunnel_info[:tunnels_configured]}"
    else
      device.tunnels_count
    end
  end
end
