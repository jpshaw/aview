module DeviceConfigurationsHelper

  def can_modify_device_configurations?
    @can_modify_device_configurations ||= Pundit.policy(current_user, DeviceConfiguration).modify?
  end


  def user_firmwares_selection_string
    return @user_firmwares_selection_string if @user_firmwares_selection_string

    @user_firmwares_selection_string = \
      if Pundit.policy(current_user, DeviceFirmware).assign_non_production?
        'sort_device_firmwares_reverse'
      else
        'sort_active_device_firmwares_reverse'
      end

    @user_firmwares_selection_string
  end

end
