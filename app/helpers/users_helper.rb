module UsersHelper

  def email_required?(user = @user)
    user ||= current_user
    user.__send__(:email_required?)
  end

end
