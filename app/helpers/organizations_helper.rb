module OrganizationsHelper

  # Public: Returns a list of managed organizations for the given manager
  # that can be used in a `select` box.
  def managed_orgs_list_for(manager)
    return [] unless manager.present?

    manager.head_manageable_organizations.reduce([]) do |managed_orgs_list, managed_org|
      managed_orgs_list += managed_org.sorted_hash_tree_organizations
    end.map {|o| [o.name, o.id]}
  end

  # Public: Checks if the current user can create organizations.
  def can_create_organizations?
    policy(Organization).create?
  end

  # Public: Checks if the current user can modify any organizations.
  def can_modify_organizations?
    policy(Organization).modify?
  end

  # Public: Checks if the current user can modify the given organization.
  def can_modify_organization?(organization = @organization)
    policy(organization).modify?
  end

  # Public: Checks if the given organization can be destroyed by the current user.
  def can_destroy_organization?(organization = @organization)
    policy(organization).destroy?
  end

  # Public: Checks if the current user can create users for the given organization.
  def can_modify_users_for_organization?(organization = @organization)
    policy(organization).modify_users?
  end

  # Public: Checks if the current user can create users for any organization.
  def can_modify_users_for_organizations?
    policy(Organization).modify_users?
  end

  #builds the organization tree markup
  def organization_tree(hash_tree)
    return if hash_tree.size == 0
    content_tag(:ol, class: 'dd-list') do
      hash_tree.sort_by { |k,v| k[:name] }.each do |customer, tree|
        concat(organization_hierarchy_row(customer, tree))
      end
    end
  end

  #builds a row for the organization tree markup
  def organization_hierarchy_row(customer, tree)
    content_tag(:li, { class: 'dd-item', id: customer.id, data: { id: "organization-#{customer.slug}" } }) do
      concat(content_tag(:div, { class: 'dd-handle' }) do
        concat(organization_hierarchy_link(customer))
        concat(content_tag(:div,
                           organization_action_dropdown(customer),
                           { class: 'pull-right hierarchy-unit-label-box' }))
      end)
      concat(organization_tree(tree))
    end
  end

  #builds a link for the organization tree markup
  def organization_hierarchy_link(customer)
    org_path=organization_path(customer)
    link_to(customer.name,
            org_path,
            class: "btn btn-link")
  end

  #builds the action menu for the organization tree markup
  def organization_action_dropdown(organization)
    content_tag(:div, :class => 'dropdown') do
      link_to(actions_menu_organization_path(organization),
              { class:'btn btn-mini dropdown-toggle hierarchy-btn-styling',
                'data-toggle' => 'dropdown',
                'data-org' => organization.id,
                remote: true }) do
        concat('Actions ')
        concat(content_tag(:span, '', :class => 'caret'))
      end +
      content_tag(:ul, class: "dropdown-menu pull-right") do
        content_tag(:li, class: 'text-center') do
          image_tag("basket-spinner.gif")
        end
      end
    end
  end

end
