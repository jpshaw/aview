module AccountHelper

  def notifications_disabled_alert(user = current_user)
    link    = link_to(t('notifications.disabled_href'), '#information', 'data-trigger' => 'tab').html_safe
    message = t('notifications.disabled_html', href: link)

    alert_warning message: message,
                    klass: 'notifications-disabled-warning',
                     hide: user.notify_enabled?
  end

  def current_user_from_params
    @current_user_from_params ||= begin
      if params[:account_id].to_i == current_user.id
        current_user
      else
        current_user.users.readonly(false).find_by(id: params[:account_id])
      end
    end
  end

  # Public: Print a widget box title with data.
  #
  # title - The String title of the widget box.
  # block - An optional block of additional elements to include in the header.
  #
  # Returns an html header element for widget boxes.
  def widget_title(title = "", &block)
    content_tag :div, :class => "widget-title" do
      content = content_tag "h5", :id => "settings-tab-title" do
        title
      end
      content += content_tag :div do
        if block_given?
          yield
        end
      end
    end
  end

  # Public: Print a form control group element. The default element type
  # is a text box. For other elements, pass a block containing the type.
  #
  # form - A Form object to print from.
  # name - The String name of the element.
  # key - The Symbol name of the field for the form object.
  # block - A block containing the element type to wrap.
  #
  # Examples
  #
  #   <%= control_group(f, "Email*", :email) %>
  #   # => <div class="control-group">
  #   # =>   <label class="control-label" for="user_email">Email*</label>
  #   # =>   <div class="controls">
  #   # =>     <input autocomplete="off" id="user_email" name="user[email]" size="30" type="text" value="test.user@accelerated.com" />
  #   # =>   </div>
  #   # => </div>
  #
  #   <%= control_group(f, "Maps Enabled", :maps_enabled) do %>
  #     <%= f.check_box :maps_enabled %>
  #   <% end %>
  #   # => <div class="control-group">
  #   # =>   <label class="control-label" for="user_maps_enabled">Maps Enabled</label>
  #   # =>   <div class="controls">
  #   # =>     <input name="user[maps_enabled]" type="hidden" value="0" />
  #   # =>     <input checked="checked" id="user_maps_enabled" name="user[maps_enabled]" type="checkbox" value="1" />
  #   # =>   </div>
  #   # => </div>
  #
  # Returns a control group element containing a label and form input for a field.
  def control_group(form, name, key, &block)
    content_tag :div, :class => "control-group" do
      content = form.label key, name, :class => "control-label"
      content += content_tag :div, :class => "controls" do
        if block_given?
          yield
        else
          form.text_field key, :autocomplete => "off"
        end
      end
    end
  end
end
