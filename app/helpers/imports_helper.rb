require 'will_paginate/array'

module ImportsHelper

  def self.import_file_exists?(import_file)
    File.exists?(import_file_path(import_file))
  end

  def self.import_file_path(import_file)
    File.expand_path(import_file.location) if import_file.present?
  end

  def import_file_exists?(import_file)
    ImportsHelper.import_file_exists?(import_file)
  end

  def import_file_path(import_file)
    ImportsHelper.import_file_path(import_file)
  end

  # Public: Checks if there are any old imports. An old import is defined as
  # being finished.
  #
  # Returns true if there are any old imports; otherwise false.
  def any_old_imports?
    Import.finished.any?
  end

  # Public: Gets a table of objects for an import. The table objects returned
  # are determined by the params :type and :view.
  #
  # Returns the result of a render for the table objects, if found; otherwise nil.
  def imported_items_table
    @records = import_records || []

    if @records.respond_to?(:paginate)
      @records = @records.paginate(:page     => @filter.page,
                                   :per_page => @filter.per_page)
    end

    if @import.the_type.configuration?
      render "/admin/imports/tables/#{@import.the_type}/#{params[:type]}"
    else
      render "/admin/imports/tables/#{params[:type]}/#{params[:view]}" rescue nil
    end
  end

  # Public: Gets object records for an import. The objects returned are
  # determined by the params :type and :view.
  #
  # import - The Import object containing the object ids.
  #
  # Returns a list of records for the object types, if found; otherwise nil.
  def import_records(import = @import)
    case params[:type]
    when "added"
      case params[:view]
      when "customers"
        Organization.where(:id => import.customer_ids)
      when "devices"
        Device.where(:id => import.device_ids)
      when "sites"
        Site.where(:id => import.site_ids)
      when "locations"
        Location.where(:id => import.location_ids)
      when "configurations"
        GatewayConfiguration.where(:id => import.configuration_ids)
      end
    when "skipped"
      case params[:view]
      when "devices"
        import.skipped_device_ids
      end
    end
  end

  # Public: Gets the label class for the import status.
  #
  # import - The Import object to get the status from.
  #
  # Returns a String with the label name for the import status.
  def import_status_label_class(import = @import)
    case import.status
    when "initializing"
      "info"
    when "running"
      "info"
    when "success"
      "success"
    else
      "important"
    end.prepend("label-")
  end
end
