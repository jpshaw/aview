module WidgetHelper

  def widget_box(opts = {}, &block)
    opts[:span] ||= "span12"
    content_tag :div, :class => "row-fluid" do
      content_tag :div, :class => opts[:span] do
        content_tag :div, :class => "widget-box" do
          yield if block_given?
        end
      end
    end
  end

  def widget_box_title(opts = {}, &block)
    content_tag :div, :class => "widget-title" do
      yield if block_given?
    end.concat content_tag(:div, nil, :class => "clearfix")
  end

  def widget_content(opts = {}, &block)
    content_tag :div, :class => "widget-content overflow-auto nopadding #{opts[:class]}" do
      yield if block_given?
    end
  end
end
