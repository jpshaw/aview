module NotificationSubscriptionsHelper

  def unsubscribe_confirmation_message
    case @subscription.try(:type)
    when NotificationSubscription::Types::DEVICE
      t('device.unsubscribe')
    when NotificationSubscription::Types::ORGANIZATION
      t('organization.unsubscribe', organization: @subscription.publisher.name)
    end
  end

  def organization_subscriptions_path(opts = {})
    subscriptions_path(opts.merge(publisher_type: NotificationSubscription::Types::ORGANIZATION))
  end

  def device_subscriptions_path(opts = {})
    subscriptions_path(opts.merge(publisher_type: NotificationSubscription::Types::DEVICE))
  end

  def subscriptions_path(opts = {})
    api_v2_account_notification_subscriptions_path(@user, opts)
  end

  def reset_subscriptions_path(opts = {})
    reset_api_v2_account_notification_subscriptions_path(@user, opts)
  end

  def publisher_link(subscription)
    publisher = subscription.publisher

    case subscription.publisher_type
    when NotificationSubscription::Types::DEVICE
      link_to publisher.mac, device_path(publisher)
    when NotificationSubscription::Types::ORGANIZATION
      link_to publisher.name, organization_path(publisher)
    else
      'N/A'
    end
  end
end
