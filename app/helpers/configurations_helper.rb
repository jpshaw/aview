module ConfigurationsHelper
  def can_modify_configurations?
    @can_modify_configurations ||= Pundit.policy(current_user, Configuration).modify?
  end

  def parent_configuration_name(config)
    if config.present?
      name = config.name

      if config.organization.present?
        name = "#{config.organization.name} #{name}"
      end

      name
    else
      'No Configuration'
    end
  end

  def grouped_configurations(configurations)
    configurations ||= []
    options          = []

    options << ['Select configuration ...', [['No Configuration', nil]]]

    configurations.group_by(&:organization).each do |organization, grouped_configs|
      options << [
        "Organization #{organization.name}",
        grouped_configs.map { |c| [c.name, c.id] }
      ]
    end

    options
  end

  def checkbox_values
    [["",""],["No",false],["Yes",true]]
  end

  def get_configuration_options_for_select2(device, organization)
    device_configurations(device, organization).map{ |option| { id: option[1], text: option[0] } }
  end
end
