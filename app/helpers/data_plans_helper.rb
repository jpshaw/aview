module DataPlansHelper
  def get_data_plan_options_for_select2
    data_plans = [{ id: ' ', text: t('data_plans.no_data_plan_option') }] #if the space is not in the ID value, this will not remove the data plan
    data_plans += DataPlan.for_organization(params[:organization_id]).map{ |plan| { id: plan.id, text: plan.name } }
    data_plans
  end
end