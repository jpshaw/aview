module EventsHelper

  def facility_dummy(facility = 0)
    case facility
      when 1
        "Trap"
      when 2
        "New Organization"
      when 3
        "New User"
      when 4
        "Delete User"
      else
        ""
    end
  end

  def type_dummy(type=0)
    case type
      when 1
        "VPN Gateway"
      when 2
        "User"
      else
        ""
    end
  end
end
