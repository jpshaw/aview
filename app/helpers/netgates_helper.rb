module NetgatesHelper

  def range_filter_day
    case params[:range]
      when "last_month"
        "Last Month"
      when "last_week"
        "Last Week"
      else
        "Last Day"
    end
  end

  def show?
    %w(details location update_location logs cos remote contacts).include? controller.action_name
  end

  def tunnel_state_code_reader(tunnel_code)
    case tunnel_code
    when "0"
      "Tunnel is Down"
    when "1"
      "Tunnel Coming Back Up"
    when "2"
      "Tunnel is Pending Dial Connection"
    when "3"
      "Tunnel is Up"
    when "4"
      "Tunnel is Coming Down"
    when "5"
      "Tunnel is Restarting"
    else
      ""
    end
  end


  def wan_config_code_reader(wan_code)
    case wan_code
    when "0"
      "Unknown"
    when "1"
      "Static IP"
    when "2"
      "DHCP"
    when "3"
      "PPPoE"
    when "4"
      "Dial"
    when "5"
      "ISDN"
    when "6"
      "PPPoA"
    when "7"
      "Cellular"
    when "8"
      "Cellular"
    else
      ""
    end
  end

  def endpoint_type_code_reader(endpoint_code)
    case endpoint_code
    when "0"
      "None"
    when "3"
      "Nortel"
    when "4"
      "SIG"
    when "5"
      "GIG"
    when "6"
      "SIG"
    when "9"
      "Cisco"
    when "20"
      "VIG"
    else
      ""
    end
  end

  def master_state_code_reader(state_code)
    case state_code
    when "0"
      "Not Running as Master"
    when "1"
      "In Master State"
    when "2"
      "Backup State"
    when "3"
      "Fault State"
    else
      ""
    end
  end

    def dial_backup_mode_reader(status)
    case status
    when "(0) 0:00:00.00"
      "No"
    when ""
      "No"
    when nil
      "No"
    else
      "Yes"
    end
  end

  def dial_backup_time_reader(backup_time)
    if backup_time == "(0) 0:00:00.00" or backup_time == "" or backup_time.nil?
      "N/A"
    elsif !backup_time.match(/\(\d+\)\s+\d+:\d+:\d+\.\d+/).nil? && backup_time != "(0) 0:00:00.00" && backup_time != ""
      backup_time.to_s.gsub(/\(\d+\)\s+/, "")
    else
      "N/A"
    end
  end

  def netgate_log_translator(statement)
    case statement
    when "sysUpTimeInstance"
      "Up Time"
    when "ngevNetGateMac"
      t(:mac_address)
    when "ngevTunConnectionId"
      "Tunnel Connection ID"
    when "ngtuTunMode"
      "Tunnel Mode"
    when "ngevConnectionType"
      "Connection Type"
    when "ngevAccount"
      "Account"
    when "ngevUserid"
      "User ID"
    when "ngtuEndpoint"
      "End Point IP Address"
    when "ngtuAuthType"
      "Authentication Type"
    when "ngtuAuthProt"
      "Authentication Port"
    when "ngtuEndpointType"
      "End Point Type"
    when "ngevTunConReason"
      "Tunnel Connection Reason"
    when "nghwWanConnectionMethod"
      "WAN Connection Method"
    when "ngtuTunInitiator"
      "Tunnel Initiator"
    when "ngtuIpsecIface"
      "IPsec Interface"
    when "ngevTunnelDateAndTime"
      "Tunnel Date and Time"
    when "ngevRequestorIp"
      "Requestor's IP Address"
    when "ngtuReasonCode"
      "Reason Code"
    when "ngifActiveWanIface"
      "Active WAN Interface"
    when "ngevActiveWanIp"
      "Active WAN IP Address"
    else
      statement.titleize
    end
  end

end
