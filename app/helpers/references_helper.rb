module ReferencesHelper

  def referred_from?(action)
    params[:reference].try(:to_sym) == action.to_sym
  end

end
