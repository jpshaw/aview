module AlertsHelper

  def alert_success(opts = {})
    alert_dismissable opts.merge(type: 'success')
  end

  def alert_warning(opts = {})
    alert_dismissable opts.merge(type: 'warning')
  end

  def alert_error(opts = {})
    alert_dismissable opts.merge(type: 'danger')
  end

  def alert_info(opts = {})
    alert_dismissable opts.merge(type: 'info')
  end

  def alert_dismissable(opts = {})
    opts[:type]    ||= 'success'
    opts[:message] ||= ''
    opts[:klass]   ||= ''
    opts[:hide]    ||= false

    render partial: 'shared/alert', locals: opts
  end

end