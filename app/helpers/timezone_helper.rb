# Public: Contains methods useful for dealing with timezones.
module TimezoneHelper

  # Public: Prints a time object in the users timezone.
  #
  # timestamp - The Time object to print.
  # user - The User object with a timezone to print the timestamp in - default
  #          is the current_user.
  #
  # Examples
  #
  #   timestamp_in_users_timezone(device.last_heartbeat_at)
  #   # => "Nov 25 2013 21:00:07 (EST)"
  #
  #   timestamp_in_users_timezone(device.last_heartbeat_at, User.first)
  #   # => "Nov 26 2013 11:05:27 (MDT)"
  #
  # Returns a string representing the time object in the user's timezone, if
  # valid; otherwise nil.
  def timestamp_in_users_timezone(timestamp, user = current_user)
    return if !timestamp.respond_to?(:in_time_zone) || user.nil?
    timestamp.in_time_zone(user.timezone.name).strftime("%b %d %Y %T (%Z)")
  end

  # Returns just the abbreviated current user timezone (e.g. EDT)
  def user_timezone(user = current_user)
    Time.now.in_time_zone(user.timezone.name).strftime("%Z")
  end
end
