module ExportHelper

  def export_this_page_only
    link_to "Current Page", which_path
  end

  def export_all_pages
    link_to "All Available Pages", which_path(true)
  end

  def export_all_search_results
    link_to "Export All", to_csv_devices_path(:export_button => params, :query => @query)
  end

  def export_all_site_results
    link_to "Export All", to_csv_organization_sites_path(@root_organization, :export_button => params, :site_id => @site.id)
  end

  private

  def which_path(ignore_page = false)
    path = case params[:controller]
    when "dashboard"
      ignore_page ? to_csv_devices_path(:export_button => params, :ignore_page => "1") : to_csv_devices_path(:export_button => params)
    when "devices"
      ignore_page ? to_csv_devices_path(:export_button => params, :ignore_page => "1") : to_csv_devices_path(:export_button => params)
    when "organizations"
      case params[:action]
      when "devices"
        ignore_page ? to_csv_organizations_path(:export_button => params, :ignore_page => "1") : to_csv_organizations_path(:export_button => params)
      end
    end
    path
  end

end
