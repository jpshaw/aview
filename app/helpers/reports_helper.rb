module ReportsHelper

  def report_type
    case params[:action]
    when "availability"
      "Availability Report"
    when "top_alert_devices"
      "Top Alert Devices Report"
    else
      "Report Generator"
    end
  end
end
