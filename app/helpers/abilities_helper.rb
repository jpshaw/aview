module AbilitiesHelper

  def can?(object, ability, subject)
    Pundit.policy(current_user, subject).send(ability)
  end

end
