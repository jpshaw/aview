class Certificate < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :cert, :approved, :signed, :exported, :device, :device_id

  belongs_to :device

  before_validation :generate_md5, if: 'self.cert_changed?'

  validates :cert,      presence: true
  validates :md5,       presence: true, uniqueness: true
  validates :device_id, presence: true

  def self.md5(cert)
    Digest::MD5.hexdigest(cert)
  end

  private

  def generate_md5
    self.md5 = self.cert.present? ? Certificate.md5(self.cert) : nil
  end

end
