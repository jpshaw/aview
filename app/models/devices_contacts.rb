class DevicesContacts < ActiveRecord::Base
  belongs_to :device
  belongs_to :contact

  validates :device_id,  presence: true, numericality: { only_integer: true }
  validates :contact_id, presence: true, numericality: { only_integer: true }
end
