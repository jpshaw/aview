class User < ActiveRecord::Base
  include Timezones
  include Pagination
  include PermissionsManager      # In concerns
  include OrganizationManageable  # In concerns
  include ApiAuthenticatable
  include Searchable

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :email, :password, :password_confirmation, :remember_me,
    :first_name, :last_name, :password_changed_at, :unique_session_id,
    :last_activity_at, :expired_at, :new_account_token, :organization_id,
    :notify_enabled, :filter_organization_id, :username,
    :account_name, :account_token, :maps_enabled, :restricted_by, :name,
    :cellular_pins_on_map, :clock_enabled

  belongs_to :organization
  belongs_to :filter_organization,        class_name: 'Organization'
  has_many   :notification_profiles,      class_name: 'NotificationProfile',      dependent: :destroy
  has_many   :notification_subscriptions, class_name: 'NotificationSubscription', dependent: :destroy
  has_many   :reports,                    class_name: 'DeviceReport',             dependent: :destroy
  has_many   :notifications,              class_name: 'Notification',             dependent: :destroy

  validates :first_name, length: { maximum: 50 }
  validates :last_name,  length: { maximum: 50 }
  validates :organization_id, presence: true
  validates :account_name, length: { maximum: 50 }, allow_blank: true
  validates :username,     length: { maximum: 50 }, allow_blank: true
  validate :notify_enabled_for_emails

  delegate :logo_path, :footer_text, to: :organization

  devise :database_authenticatable, :rememberable, :trackable,
    :validatable, :recoverable, :timeoutable

  has_sort_scope :name
  has_sort_scope :email
  has_sort_scope :last_sign_in_at

  has_search_scope :with_name_search, ->(search) { where("users.name LIKE ?", "%#{search}%") }
  has_search_scope :last_activity_since, ->(date) {
    if date.nil?
      where(last_activity_at: nil)
    else
      where('last_activity_at >= ?', date)
    end
  }

  has_search_scope :with_email_search, ->(search) { where("users.email LIKE ?", "%#{search}%") }
  has_search_scope :sort_by_timezone_name, ->(direction) {
    joins(:timezone)
      .order("timezones.off #{sort_direction direction}")
  }
  has_search_scope :with_timezone_id, ->(timezone_id) {
    joins(:timezone).where(timezones: { id: timezone_id })
  }

  has_search_scope :with_status, ->(status) {
    case status.to_sym
    when :pending
      where(sign_in_count: 0)
    when :active
      where('sign_in_count > 0 and last_sign_in_at >= ?', 30.days.ago)
    when :inactive
      where('sign_in_count > 0 and last_sign_in_at < ?', 30.days.ago)
    end
  }
  has_search_scope :notifiable, ->() { where(notify_enabled: true) }


  ## Authorized Objects

  # All unfiltered organizations available to the user. The is primarily used
  # to populate the global filter select box and the admin panel.
  def all_organizations(params = {})
    organizations(params.merge(all: true))
  end

  def organizations(params = {})
    OrganizationsFinder.new(self, params).execute
  end

  def accounts(params = {})
    organizations(params).leaves
  end


  def devices(params = {})
    DevicesFinder.new(self, params).execute
  end

  def device_events(params = {})
    DeviceEventsFinder.new(self, params).execute
  end

  def netbridge_configurations(params = {})
    NetbridgeConfigurationsFinder.new(self, params).execute
  end

  def device_configurations(params = {})
    DeviceConfigurationsFinder.new(self, params).execute
  end

  def sites(params = {})
    SitesFinder.new(self, params).execute
  end

  def users(params = {})
    UsersFinder.new(self, params).execute
  end

  def groups(params = {})
    GroupsFinder.new(self, params).execute
  end

  def tunnel_servers(params = {})
    TunnelServersFinder.new(self, params).execute
  end

  def tunnel_server_devices(params = {})
    TunnelServerDevicesFinder.new(self, params).execute
  end

  def device_categories
    @device_categories ||= DeviceCategoryFinder.new(self).execute
  end

  ##

  def full_name
    "#{first_name} #{last_name}".split.map(&:capitalize).join(' ')
  end

  def name_or_email
    name.blank? ? email : name
  end
  alias_method :full_name_or_email, :name_or_email

  def status
    if sign_in_count.zero?
      'Pending'
    elsif last_sign_in_at.present? && last_sign_in_at < 30.days.ago
      'Inactive'
    else
      'Active'
    end
  end

  def device_subscriptions
    self.notification_subscriptions.devices
  end

  def organization_subscriptions
    self.notification_subscriptions.organizations
  end

  def javascript_timezone
    ActiveSupport::TimeZone::MAPPING[self.timezone.name]
  end

  # Class methods
  def self.policy_class
    UserPolicy
  end

  protected

  def notify_enabled_for_emails
    return if email_required?
    if notify_enabled? && email.blank?
      errors.add(:notify_enabled, "can't be set without a valid Email")
    end
  end

end
