require 'friendly_id'
require 'site'

class Organization < ActiveRecord::Base

  BRANDABLE_ATTRIBUTES ||= ['logo', 'footer'].freeze

  mount_uploader :logo, LogoUploader

  extend FriendlyId
  friendly_id :name, use: :slugged

  include PermissionsManager      # In concerns
  include PermissionsManageable   # In concerns

  has_closure_tree dependent: :destroy

  belongs_to :customer_service

  has_one :admin_role, -> { where admin: true }, class_name: "Role", dependent: :destroy

  has_many :data_plans, dependent: :destroy
  has_many :devices, dependent: :destroy
  has_many :locations, dependent: :destroy
  has_many :sites, dependent: :destroy
  has_many :users, dependent: :destroy
  has_many :roles, dependent: :destroy
  has_many :gateway_configurations,   dependent: :destroy
  has_many :netreach_configurations,  dependent: :destroy
  has_many :netbridge_configurations, dependent: :destroy
  has_many :device_configurations,    dependent: :destroy
  has_many :notification_subscriptions, class_name: 'NotificationSubscription', as: :publisher, dependent: :destroy
  has_many :organizations_contacts, class_name: 'OrganizationsContacts'
  has_many :contacts, through: :organizations_contacts

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :slug, :import_name, :description, :location_desc,
    :label, :logo, :customer_service_id, :footer, :remove_logo, :parent_id,
    :gateway_cell_modem_collection_enabled, :gateway_vlan_collection_enabled

  validates :name,          presence:   true,
                            length:     { minimum: 2, maximum: 50, allow_blank: false },
                            uniqueness: { scope: :parent_id, case_sensitive: false }
  validates :slug,          presence:   true,
                            length:     { minimum: 2, maximum: 75, allow_blank: false },
                            uniqueness: { case_sensitive: false }
  validates :description,   length: { maximum: 250, allow_blank: true }
  validates :location_desc, length: { maximum: 100, allow_blank: true }
  validates :footer, length: { maximum: 1023, allow_blank: true }

  after_create :setup_associations

  before_save :check_name

  def self.policy_class
    ::OrganizationPolicy
  end


  # A head organization is the top organization in the list within a given organizational branch.
  # For example, given:
  #
  # Accelerated
  # |- Child 1
  # |  |- Grand Child 1
  # |  |  |- Great Grand Child 1
  # |  |- Grand Child 2
  # |     |- Great Grand Child 2
  # |- Child 2
  #     |- Grand Child 3
  #
  # 'Accelerated' is the head organization of all other organizations because they all stem from it.
  #
  # 'Child 1' is the head of all organizations in its sub-branch (Grand Child1, Great Grand Child 1,
  # Grand Child 2 and Great Grand Child 2).
  #
  # 'Grand Child 1' is the head of 'Great Grand Child 1', etc.
  #
  # Examples:
  #
  # If the list contains 'Child 1', 'Great Grand Child 2' and 'Grand Child 3',
  # only 'Child 1' and 'Grand Child 3' will be returned. 'Great Grand Child 2'
  # will not be returned because it is a descendant of 'Child 1', which is
  # also in the list.
  #
  # If the list contains 'Accelerated', only 'Accelerated' will be returned
  # because it is the head of all other organizations.
  def self.head_organizations_in(organizations)
    organizations = Array.wrap(organizations)
    heads         = []
    descendants   = []

    organizations.sort_by { |org| org.descendants.count }.reverse.each do |organization|
      unless descendants.include?(organization)
        heads     <<  organization
        remaining =   organizations - heads - descendants

        remaining.each do |rem_org|
          descendants << rem_org if organization.descendant_ids.include?(rem_org.id)
        end
      end
    end

    heads
  end

  def sorted_hash_tree_organizations(sort_by = :name)
    organizations = []
    process_tree_organizations(hash_tree, sort_by, organizations)
    organizations
  end

  def process_tree_organizations(current_tree, sort_by = :name, organizations = [])
    current_tree.sort_by { |k,v| k[sort_by] }.each do |org, tree|
      organizations << org
      process_tree_organizations(tree, sort_by, organizations)
    end

    organizations
  end

  # Returns devices that belong to this organization and any organization under it.
  def self_and_descendant_devices
    Device.joins(:organization)
          .joins('inner join organization_hierarchies on organizations.id = organization_hierarchies.descendant_id')
          .where(organization_hierarchies: { ancestor_id: id })
  end

  # All organizations managed by this organization
  def organizations
    Organization
      .joins('INNER JOIN `organization_hierarchies` ON `organizations`.`id` = `organization_hierarchies`.`descendant_id`')
      .where(organization_hierarchies: { ancestor_id: manageable_organization_ids })
      .uniq
  end

  # TODO: Invalidate cache when organization permissions are changed
  def manageable_organization_ids
    cache_key = "permissions/organizations/#{id}/manageable_organization_ids"

    Rails.cache.fetch(cache_key, expires_in: 30.minutes) do
      Permission
        .where(manager: self, manageable_type: self.class)
        .pluck(:manageable_id)
    end
  end

  def ancestor_id_list(include_self = true)
    include_self ? self_and_ancestor_ids : ancestor_ids
  end

  # Defaults
  def default_site
    sites.default
  end

  # Association methods
  def organization_count
    descendants.count
  end

  def setup_associations
    default_site
  end

  def self_managing_permission
    self.manageable_permissions.select { |mp| mp.manager == self }.first
  end

  def logo_path
    (org = organization_with(:logo)).present? ? org.logo.to_s : Settings.logo.image
  end


  def footer_text
    (org = organization_with(:footer)).present? ? org.footer : Settings.footer
  end

  def service_name
    self.customer_service.try(:name) || (self.parent.service_name if self.parent)
  end

  ### Device Settings
  # TODO: Look into encapsulating this in another class/module

  # TODO: Enable this if we should use hierarchy traversal.
  # def gateway_cell_modem_collection_enabled?
  #   if (organization = organization_with(:gateway_cell_modem_collection_enabled)).present?
  #     organization.gateway_cell_modem_collection_enabled
  #   else
  #     false
  #   end
  # end

  def self.find_by_import_name_or_name(name)
    where('upper(import_name) = upper(?)', name).first ||
      where('upper(name) = upper(?)', name).first
  end

  private

  def organization_with(attr)
    self_and_ancestors.where("#{attr} IS NOT NULL and #{attr} != ?", '').limit(1).first
  end

  # Fixes any odd spellings that may arise from the `titleize` used in the importers.
  def check_name
    self.name.gsub!(/at&t/i, 'AT&T') if self.name =~ /at&t/i
  end

end
