class Carrier < ActiveRecord::Base
  belongs_to :carrier_detail
  validates :name, presence: true, uniqueness: true

  before_save { name.downcase! }
  before_create :assign_matching_carrier_detail
  after_create :send_creation_notification

  attr_accessible :carrier_detail_id, :name

  def color
    carrier_detail.color if carrier_detail
  end

  def display_name
    carrier_detail ? carrier_detail.display_name : name
  end

  private

  def assign_matching_carrier_detail
    self.carrier_detail_id = CarrierDetail.all.detect{|carrier_detail| carrier_detail.regexp =~ name }.try(:id)
  end

  def send_creation_notification
    AdminMailer.new_carrier(self).deliver_now
  end
end
