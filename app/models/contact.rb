# Public: Holds information for individual contacts, used for support.
#
# A device can have multiple contacts, all available within the device's organization scope.
# The token is an attempt to prevent multiple contacts with the same identifiers from being used.
#
# name - The String name of the contact. Limit 100 characters.
# email - The String email of the contact. Limit 100 characters.
# label - Label used for identifying the contact. Integer
# phone - Phone number of the contact. Limit 50 characters.
#
class Contact < ActiveRecord::Base

  has_many :organizations_contacts, class_name: 'OrganizationsContacts'
  has_many :organizations, through: :organizations_contacts
  has_many :devices_contacts, class_name: 'DevicesContacts'
  has_many :devices, through: :devices_contacts

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :email, :phone, :label, :token

  validates :name, :presence => true, :length => { :minimum => 3, :maximum => 100 }
  validates :email, :presence => true, :length => { :maximum => 100 }
  validates_format_of :email, :with => /\A[^@]+@[^@]+\z/
  validates :phone, :length => { :maximum => 50 }
  validates :label, :presence => true
  validates :label, numericality: { only_integer: true }

  before_validation :set_default_label
  before_save :set_token

  scope :sorted_by_name, ->(direction = :asc) { order(name: direction) }

  def display_name
    "#{name}, #{label_name} Contact"
  end

  # Public: Return a human readable version of the current contact's label
  def label_name
    case label
    when ::ContactLabels::TECHNICAL
      "Technical"
    when ::ContactLabels::SALES
      "Sales"
    when ::ContactLabels::SITE
      "Site"
    else
      "General"
    end
  end

  # Public: Generate a unique token for the given email, used for lookup.
  #
  # email - The String email of the contact to tokenize.
  #
  # Examples
  #
  #   Contact.token("test.user@accelerated.com")
  #   # => '3e06fa3927cbdf4e9d93ba4541acce86'
  #
  # Returns a MD5 hash of the unique email
  def self.token(email)
    Digest::MD5.hexdigest(email.upcase)
  end

  # TODO: Remove tokens when contact management is added.
  def self.tokenize(attributes)
    attributes = attributes.with_indifferent_access

    Digest::MD5.hexdigest(
      [
        attributes[:name],
        attributes[:phone],
        attributes[:email]
      ].compact.join(' ').upcase
    )
  end

  def self.find_by_token_or_initialize(attributes)
    find_by_token(attributes) || new(attributes)
  end

  def self.find_by_token(attributes)
    find_by(token: tokenize(attributes))
  end

  private

  def set_token
    self.token ||= self.class.tokenize(attributes)
  end

  def set_default_label
    self.label ||= ::ContactLabels::GENERAL
  end
end
