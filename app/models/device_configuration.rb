class DeviceConfiguration < ActiveRecord::Base
  include Searchable

  class ConfigurationObject < Hashie::Mash
    disable_warnings
  end

  FIRMWARE_18_4_54 = Gem::Version.new('18.4.54')
  enum record_type: { collection: 0, individual: 1, system: 2 }

  has_paper_trail

  store :additional_settings, accessors: [:grace_period_duration, :grace_period_duration_locked, :device_firmware_locked]

  alias_attribute :settings, :description

  belongs_to :organization
  belongs_to :device_firmware
  has_many :children, class_name: 'DeviceConfiguration', foreign_key: 'parent_id', dependent: :destroy
  belongs_to :parent, class_name: 'DeviceConfiguration'

  before_save :set_heartbeat_fields

  #TODO:  Uncomment this has_many when all device types are moved from configurations to
  #       device_configurations.
  # has_many :devices, foreign_key: :configuration_id, dependent: :nullify

  validates :organization_id, presence: true

  validates(
    :name,
    presence:   true,
    uniqueness: {
      scope:          :organization_id,
      case_sensitive: false,
      message:        'already exists for the organization'
    }
  )

  validates :device_firmware_id, :settings, presence: true

  validate :valid_grace_period_duration?, if: lambda { |dc| dc.grace_period_duration.present? }

  validate :settings_is_valid_json

  validate :settings_do_not_violate_firmware_schema

  delegate :category, :device_model, to: :device_firmware, allow_nil: true

  has_search_scope :with_name_search, ->(search) { where("device_configurations.name LIKE ?", "%#{search}%") }
  has_search_scope :with_firmware_search, ->(search) {
    joins(:device_firmware)
      .where("device_firmwares.version LIKE ?", "%#{search}%")
  }
  has_search_scope :with_model_id, ->(model_id) {
    joins(:device_firmware)
      .where(device_firmwares: { device_model_id: model_id })
  }
  has_search_scope :with_category, ->(category) {
    joins(device_firmware: {device_model: :category})
      .where("category_id = ?", category)
  }
  has_search_scope :with_organization, ->(filter_hash) {
    if filter_hash[:include_hierarchy] == 'true'
      organization = Organization.find(filter_hash[:organization_id])
      where(organization_id: organization.self_and_descendant_ids )
    else
      where(organization_id: filter_hash[:organization_id] )
    end
  }
  has_search_scope :sort_by_name, ->(direction) { order("name #{sort_direction direction}") }
  has_search_scope :sort_by_organizations_name, ->(direction) {
    joins(:organization)
      .order("organizations.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_device_categories_name, ->(direction) {
    joins(device_firmware: {device_model: :category})
      .order("device_categories.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_device_models_name, ->(direction) {
    joins(device_firmware: :device_model)
      .order("device_models.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_device_firmware, ->(direction) {
    joins(:device_firmware)
      .order("device_firmwares.version #{sort_direction direction}")
  }
  has_search_scope :sort_by_updated_at, ->(direction) { order("updated_at #{sort_direction direction}") }

  def credentials
    OpenStruct.new(Rails.cache.read("#{Settings.device_configurations.credentials.cache_key_prefix}#{id}"))
  end

  def to_hash
    JSON.parse(compiled_settings).with_indifferent_access
  rescue JSON::ParserError
    {}
  end
  alias_method :to_h, :to_hash

  def remote_control_disabled?
    # The field is not explicitly set and enabled by default.
    fetch_value(:service, :remote_control, :enable) == false
  end

  # Fetch a value from the configuration hash by passing the key path.
  #
  # Example:
  #
  #   # Setup
  #   data = { schema: { version: '49' } }
  #   device.configuration.update_attribute(:description, data.to_json)
  #
  #   device.configuration.fetch_value(:schema, :version)
  #   # => '49'
  #
  #   device.configuration.fetch_value(:a, :b, :c)
  #   # => nil
  #
  def fetch_value(*keys)
    return unless compiled_settings.present?

    hash = self.to_hash
    hash.extend Hashie::Extensions::DeepFetch
    hash.deep_fetch(*keys)

  rescue Hashie::Extensions::DeepFetch::UndefinedPathError
    nil
  end

  #TODO:  Remove this method when all device types are moved from configurations to
  #       device_configurations.
  def devices
    Device.where(configuration_id: self.id, model_id: self.device_model.id)
  end


  def all_associated_devices
    configuration_ids = self.children.pluck(:id).push(self.id)
    Device.where(configuration_id: configuration_ids, model_id: self.device_model.id)
  end


  def status_freq
    frequency_from_configuration || frequency_from_schema
  end

  def parsed_settings
    JSON.parse(self.settings) if self.settings.present?
  rescue JSON::ParserError
    nil
  end

  def parsed_compiled_settings
    JSON.parse(self.compiled_settings) if self.compiled_settings.present?
  rescue JSON::ParserError
    nil
  end

  # TODO: this can be reimplemented when we have a proper firmware schema validator
  def validate_against_firmware_schema(settings)
    {}
  end

  private

  def settings_is_valid_json
    errors.add(:settings, 'is not valid JSON') unless parsed_settings
  end

  def settings_do_not_violate_firmware_schema
    return unless device_firmware.present? && device_firmware.schema_file.present?
    result = validate_against_firmware_schema(settings)
    result.each{ |validation_error| errors.add(:schema_validation, validation_error[:message]) }
  end

  def settings_mash
    ConfigurationObject.new(parsed_settings)
  end


  def schema_mash
    schema_file = device_firmware.schema_file.read
    schema_hash = JSON.parse(schema_file) if schema_file
  rescue JSON::ParserError => je
    # Nothing to do. We are OK with schema_hash being nil, which will happen if no schema_file or if
    # the parsing fails, the variable gets declared either way.
  ensure
    return ConfigurationObject.new(schema_hash)
  end


  def frequency_from_configuration
    mash_duration("settings_mash.#{Settings.device_configurations.json_path_for.status_freq}")
  end


  def frequency_from_schema
    mash_duration("schema_mash.#{Settings.firmware_schemas.json_path_for.default_status_freq}")
  end


  def mash_duration(mash_path)
    instance_eval(mash_path)
  rescue NoMethodError
    # Raises if the attribute does not exist in the Mash property chain (below 1st level).
    nil
  end


  def calculate_duration(value, attribute_name=nil)
    ::DurationCalculator.calculate(value)
  rescue => e
    errors.add(attribute_name, "is invalid. #{e.message}") if attribute_name
  end


  def valid_grace_period_duration?
    calculate_duration(self.grace_period_duration, :grace_period_duration)
  end

  def set_heartbeat_fields
    self.heartbeat_frequency = calculate_duration(status_freq)
    self.heartbeat_grace_period = calculate_duration(grace_period_duration)
  end

end
