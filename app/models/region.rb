# Public: Model for individual regions.
#
# code - The String code of the country - Ex: "US". Must be 2 characters.
# name - The String name of the country - Ex: "United States". Limit 100 characters.
class Region < ActiveRecord::Base

  belongs_to :country

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :iso_code, :name, :country_id

  validates :iso_code, :presence => true, :length => { :is => 2 },
    :uniqueness => { :case_sensitive => false }

  validates :name, :presence => true, :length => { :minimum => 2, :maximum => 100 },
    :uniqueness => { :case_sensitive => false }

  class << self

    # Public: Create an array of regions to use for a select dropdown box.
    # Returns an array of regions, leading with 'Select region/state'.
    def select_list(country_id)
      select("iso_code, name, country_id").order('name asc').load.to_a.map { |region|
        [region.name, region.iso_code] if region.country_id == country_id
      }.unshift(["Select State", ""])
    end

    def country_ids
      select(:country_id).distinct.map(&:country_id)
    end

    def name_for_code(code)
      where(iso_code: code).limit(1).pluck(:name).first
    end

    def code_for_name(name)
      where(name: name).limit(1).pluck(:iso_code).first
    end

  end

end
