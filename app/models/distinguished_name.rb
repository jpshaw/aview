# Represents an SSL X509 Distinguished Name entity as defined by RFC2253.
#
# Reference
#   http://ruby-doc.org/stdlib-2.4.1/libdoc/openssl/rdoc/OpenSSL/X509/Name.html
#   https://www.ietf.org/rfc/rfc2253.txt
#
# Common attributes:
#
#      Key     Attribute (X.520 keys)
#      ------------------------------
#      CN      CommonName
#      L       LocalityName
#      ST      StateOrProvinceName
#      O       OrganizationName
#      OU      OrganizationalUnitName
#      C       CountryName
#
class DistinguishedName

  attr_accessor :cn
  alias_method :common_name, :cn

  attr_accessor :l
  alias_method :locality, :l

  attr_accessor :st
  alias_method :state, :st

  attr_accessor :o
  alias_method :organization, :o

  attr_accessor :ou
  alias_method :organizational_unit, :ou

  attr_accessor :c
  alias_method :country, :c

  def initialize(attributes = {})
    attributes.each do |type, value|
      send("#{type.downcase}=", value) if respond_to?(type.downcase)
    end
  end

  def to_h
    instance_values.transform_keys!(&:upcase)
  end

  def to_s
    to_h.map { |type, value| "#{type}=#{value}" }.join(',')
  end

  # Checks if any of the given names match the common name.
  #
  # Note: Matches are case-insenitive.
  #
  # Examples
  #
  #   DistinguishedName.parse('CN=abc').common_name_matches?('abc')
  #   #=> true
  #
  #   DistinguishedName.parse('CN=ABC').common_name_matches?('abc')
  #   #=> true
  #
  #   DistinguishedName.parse('CN=abc').common_name_matches?('a', 'b', 'c')
  #   #=> false
  #
  def common_name_matches?(*names)
    common_name.present? && names.any? do |name|
      name.present? && common_name.casecmp(name) == 0
    end
  end

  # Regex copied from Ruby's OpenSSL::X509::Name parser: http://goo.gl/MS5vmZ
  DN_REGEX = /\s*([^\/,]+)\s*/

  # Parses a distinguished name string and returns an object initialized with
  # the parsed attributes. Both comma and forward-slash separated attributes
  # are valid.
  def self.parse(str)
    matches    = str.to_s.scan(DN_REGEX)
    pairs      = matches.collect { |i| i[0].split('=', 2) }
    attributes = Hash[pairs]

    self.new(attributes)
  end

end
