class Search

  DEVICE              = 'device'.freeze
  DEVICE_MODEM        = 'device_modem'.freeze
  SITE                = 'site'.freeze
  LOCATION            = 'location'.freeze
  COUNTRY             = 'country'.freeze
  CONTAINS            = 'contains'.freeze
  LIKE                = 'like'.freeze
  EQUALS              = '='.freeze
  DEFAULT_COLUMN      = 'iccid'.freeze
  DEFAULT_MATCH_TYPE  = CONTAINS.freeze
  UNDESIRABLE_CHARS   = ':'.freeze

  MATCH_TYPES     = {
    CONTAINS  => LIKE,
    'equals'  => EQUALS
  }.freeze

  MODELS_COLUMNS  = {
    DEVICE => {
      'mac'       => I18n.t(:mac),
      'host'      => I18n.t(:primary_ip),
      'firmware'  => 'Firmware',
      'serial'    => 'Serial'
    },
    DEVICE_MODEM => {
      'number'  => 'Phone Number',
      'iccid'   => 'ICCID',
      'imei'    => 'IMEI',
      'imsi'    => 'IMSI',
      'esn'     => 'ESN'
    },
    SITE => {
      'name' =>'Name'
    },
    LOCATION => {
      'city'        => 'City',
      'state'       => 'State',
      'state_abbr'  => 'State Abbreviation',
      'postal_code' => 'Postal Code'
    },
    COUNTRY => {
      'name'  => 'Name',
      'code'  => 'Code'
    }
  }.with_indifferent_access.freeze

  include ActiveModel::Validations

  validates :model,       inclusion:  { in: MODELS_COLUMNS.keys, message: 'is invalid' }, if: 'model.present?'
  validates :match_type,  inclusion:  { in: MATCH_TYPES.keys, message: 'is invalid' }
  validates :value,       presence:   { message: 'is necessary to run a search' }

  validate :valid_column?

  attr_reader :current_user

  def initialize(current_user, search_parameters = {})
    @current_user = current_user

    if search_parameters.is_a?(Hash)
      self.model        = search_parameters[:model]
      self.value        = search_parameters[:value].to_s.strip.delete(UNDESIRABLE_CHARS)
      self.column       = model.present? ? search_parameters[:column] : DEFAULT_COLUMN
      self.match_type   = search_parameters[:match_type] || DEFAULT_MATCH_TYPE
    end
  end


  def devices
    if valid?
      if model.present?
        all_device_query    ||
        site_query          ||
        location_query      ||
        country_query       ||
        device_modem_query
      else
        basic_query
      end
    else
      Device.limit(0)
    end
  end


  def description
    if valid?
      model_description + match_type_description
    else
      'Invalid search parameters'
    end
  end


  private

  attr_accessor :model, :column, :value, :match_type

  def valid_column?
    unless errors.keys.include?(:model)
      errors.add(:column, 'is invalid') if model.present? && !valid_model_column?
    end
  end


  def valid_model_column?
    MODELS_COLUMNS[model].keys.include?(column)
  end


  def all_device_query
    query_results(device_query, column) if model == DEVICE
  end


  def site_query
    query_results(device_query.joins(:site), "sites.#{column}") if model == SITE
  end


  def location_query
    query_results(device_query.joins(:location), "locations.#{column}") if model == LOCATION
  end


  def country_query
    query_results(device_query.joins(location: :country), "countries.#{column}") if model == COUNTRY
  end

  def query_results(relation, where_text)
    relation.where("#{where_text} #{matches} ?", searched_value)
  end


  def device_modem_query
    if model == DEVICE_MODEM
      device_query.where(
        device_modem_query_string,
        searched_value
      )
    end
  end


  def basic_query
    query_string = "upper(mac) like upper(?) or upper(sites.name) like upper(?) or serial like ? or #{device_modem_query_string}"
    device_query.joins(:site).where(query_string, searched_value, searched_value, searched_value, searched_value)
  end


  def device_modem_query_string
    "devices.id in (select device_id from device_modems where device_modems.#{column} #{matches} ?)"
  end

  def device_query
    current_user.devices
  end

  def matches
    MATCH_TYPES[match_type]
  end


  def searched_value
    like_matching? ? "%#{value}%" : value
  end


  # Returns true if matching (using 'like'), false otherwise (using '=').
  def like_matching?
    matches == LIKE
  end


  def model_description
    model.present? ? "#{model.titleize} #{MODELS_COLUMNS[model][column]} " : ''
  end


  def match_type_description
    "#{model.present? ? match_type : match_type.titleize} '#{value}'"
  end

end
