# TODO: Add formal support for scheduling device commands
class ScheduledDeviceCommand

  def self.all_for_current_interval(options = {})
    gateway_vlan_commands(options)
  end

  def self.gateway_vlan_commands(options = {})
    return to_enum(:gateway_vlan_commands, options) unless block_given?

    batch_size = options[:batch_size] || 1000

    Organization.where(gateway_vlan_collection_enabled: true).pluck(:id).each do |organization_id|
      Gateway.deployed.good.joins(configuration: :portal).preload(configuration: :portal).where(organization_id: organization_id).find_in_batches(batch_size: batch_size) do |devices|
        devices.each do |device|
          next unless device.supports_json_commands?
          yield(device_id: device.id, command: Gateway::FETCH_VLAN_CONFIG)
        end
      end
    end
  end

end