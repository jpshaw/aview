require 'devices/cellular'
require 'devices/dial_to_ip'
require 'devices/embedded_cellular'
require 'devices/gateway'
require 'devices/netbridge'
require 'devices/netreach'
require 'devices/remote_manager'
require 'devices/ucpe'
require 'devices/usb_cellular'

class DeviceCategory < ActiveRecord::Base

  ALL                        = 'All'
  GATEWAY_CATEGORY           = 'VPN Gateway'
  CELLULAR_CATEGORY          = 'Cellular'
  LEGACY_CELLULAR_CATEGORY   = 'Legacy Cellular'
  USB_CELLULAR_CATEGORY      = 'USB Cellular'
  EMBEDDED_CELLULAR_CATEGORY = 'Embedded Cellular'
  WIFI_CATEGORY              = 'WiFi'
  DIAL_TO_IP_CATEGORY        = 'Dial-to-IP'
  REMOTE_MANAGER_CATEGORY    = 'Remote Manager'
  UCPE_CATEGORY              = 'uCPE'

  CATEGORIES = {
    gateway: {
      name: GATEWAY_CATEGORY, class: Gateway, groups: true
    },
    cellular: {
      name: CELLULAR_CATEGORY, class: Cellular, groups: true
    },
    legacy_cellular: {
      name: LEGACY_CELLULAR_CATEGORY, class: Netbridge
    },
    usb_cellular: {
      name: USB_CELLULAR_CATEGORY, class: UsbCellular, groups: true
    },
    embedded_cellular: {
      name: EMBEDDED_CELLULAR_CATEGORY, class: EmbeddedCellular, groups: true
    },
    wifi: {
      name: WIFI_CATEGORY, class: Netreach
    },
    dial_to_ip: {
      name: DIAL_TO_IP_CATEGORY, class: DialToIp, groups: true
    },
    remote_manager: {
      name: REMOTE_MANAGER_CATEGORY, class: RemoteManager, groups: true
    },
    ucpe: {
      name: UCPE_CATEGORY, class: Ucpe, groups: true
    }
  }.freeze

  GROUP_CATEGORIES = CATEGORIES.select { |k, v| v[:groups] == true }
                               .map { |k,v| v[:class].to_s }.freeze

  has_many :models, class_name: "DeviceModel",      foreign_key: :category_id, :dependent => :destroy
  has_many :uuids,  class_name: "DeviceEventUUID",  foreign_key: :category_id, :dependent => :destroy
  has_many :devices, foreign_key: :category_id

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :description, :class_name

  validates :name, :presence => true, :uniqueness => true
  validates :description, :length => { :maximum => 50 }
  validates :class_name, :presence => true, :uniqueness => true

  scope :gateway,           -> { where( class_name: CATEGORIES[:gateway][:class]           ) }
  scope :cellular,          -> { where( class_name: CATEGORIES[:cellular][:class]          ) }
  scope :legacy_cellular,   -> { where( class_name: CATEGORIES[:legacy_cellular][:class]   ) }
  scope :usb_cellular,      -> { where( class_name: CATEGORIES[:usb_cellular][:class]      ) }
  scope :embedded_cellular, -> { where( class_name: CATEGORIES[:embedded_cellular][:class] ) }
  scope :wifi,              -> { where( class_name: CATEGORIES[:wifi][:class]              ) }
  scope :dial_to_ip,        -> { where( class_name: CATEGORIES[:dial_to_ip][:class]        ) }
  scope :remote_manager,    -> { where( class_name: CATEGORIES[:remote_manager][:class]    ) }
  scope :ucpe,              -> { where( class_name: CATEGORIES[:ucpe][:class]              ) }
  scope :for_groups,        -> { where( class_name: GROUP_CATEGORIES ) }

  after_initialize :default_values

  def self.name_from_mac(mac)
    # TODO: Delegate this to the device models when correct subclassing is
    # implemented and a common method for defining mac patterns is in place

    if Gateway.valid_mac?(mac)
      return GATEWAY_CATEGORY
    elsif Netbridge.valid_mac?(mac)
      return LEGACY_CELLULAR_CATEGORY
    elsif Netreach.valid_mac?(mac)
      return WIFI_CATEGORY

    # NOTE: There are no MAC ranges defined for uClinux devices yet

    else
      'Unknown'
    end
  end

  private

  # Internal: Sets default values for a DeviceCategory object.
  def default_values
    self.name ||= "N/A"
  end
end
