class GatewayPortalProfile < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :auth_password, :auth_user, :password, :ssl_password, :ssl_port

  validates :auth_password, :length => { :maximum => 16, :allow_blank => true }
  validates :auth_user, :length => { :maximum => 8, :allow_blank => true }
  validates :password, :length => { :maximum => 255, :allow_blank => true }
  validates :ssl_password, :length => { :maximum => 255, :allow_blank => true }
  validates :ssl_port, :numericality => { :only_integer => true, :allow_nil => true }

  after_initialize :init

  SUPPORT_USER = 'support'

  def init
    self.auth_user ||= SUPPORT_USER
  end
end
