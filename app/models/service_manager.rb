module ServiceManager

  GATEWAY_SYSTEM_NAME ||= 'SYSTEM-DEFAULTS'.freeze

  def self.enabled?
    Settings.service_manager_sync.enabled == true
  end

  def self.gateway_whitelist
    @gateway_whitelist ||= Settings.imports.gateway_whitelist || []
  end

end
