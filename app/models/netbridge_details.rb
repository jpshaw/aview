class NetbridgeDetails < ActiveRecord::Base

  belongs_to :device, :class_name => "Netbridge"

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :usb_hub, :usb_list, :client_ip, :eth_state, :eth_macs, :tunnel_status

  serialize :eth_macs

  validates :device_id, :presence => true, :numericality => { :only_integer => true, :allow_nil => true }
  validates :usb_hub, length: { maximum: 10 }, allow_blank: true
  validates :usb_list, length: { maximum: 100 }, allow_blank: true
  validates :eth_state, length: { maximum: 20 }, allow_blank: true
  validates :client_ip, length: { maximum: 20 }, allow_blank: true

  after_initialize :default_values

  # TODO: Calculate the client mac when the eth1 syslog is received instead of
  # on the fly, which is done any time the netbridge index page is loaded.
  def client_mac
    @client_mac ||= ::MacAddressCorrelationService.new(eth1_mac).execute
  end

  def eth1_mac
    self.eth_macs.try(:fetch, :eth1, nil)
  end

  private

  def default_values
    self.eth_macs ||= {}
  end

end
