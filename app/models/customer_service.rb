class CustomerService < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name

  has_many :organizations, dependent: :nullify


  def self.all_for_select
    order(:name).map { |cs| [cs.name, cs.id] }
  end

end
