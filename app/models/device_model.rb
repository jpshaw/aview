class DeviceModel < ActiveRecord::Base
  belongs_to :category, class_name: "DeviceCategory"

  has_many :configuration_schemas,  foreign_key: :model_id
  has_many :device_firmwares
  has_many :devices, foreign_key: :model_id

  delegate :name, to: :category, prefix: true, allow_nil: true

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :category_id, :description, :name, :class_name, :model_version, :code

  validates :category_id, :presence => true, :numericality => { :only_integer => true }
  validates :description, :length => { :maximum => 255, :allow_blank => true }
  validates :name, :presence => true, :length => { :maximum => 50 }, :uniqueness => { :case_sensitive => false, :scope => :category_id }
  validates :class_name, :length => { :maximum => 255, :allow_blank => true }
  validates :model_version, :length => { :maximum => 20, :allow_blank => true }
  validates :code, :numericality => { :only_integer => true, :allow_nil => true }

  after_initialize :default_values

  def cached_firmwares
    Rails.cache.fetch("#{firmwares_cache_key}", expires_in: 30.minutes) do
      device_firmwares.to_a.sort.reverse
    end
  end

  def firmwares_cache_key
    "#{cache_key}/firmwares"
  end

  class << self

    # Public: Returns the category model based off of passed object_type and model_version. If
    # the search comes up nil, it will do the search with the model name as "Default" since
    # every category has a "Default" model
    #
    # object_type - A string that is a device's object type
    # model_version  - A string that is a device's hw_version
    #
    # Examples
    #
    #   DeviceModel.lookup("Gateway","8200")
    #   # => <#DeviceModel> || <#DeviceModel :name => "Default"> || nil
    #
    # Returns a single model or nil
    def lookup(object_type, model_version)
      find_model(object_type, model_version) || default(object_type)
    end

    # Public: used by find_model method to create a DeviceModel Query that joins on the
    # device_categories table and searches for a device_category with the passed object_type.
    # If used by it's self, will return an array of models that have a category with the passed
    # object_type for the class_name
    #
    # object_type - A string that is a device's object type
    #
    # Examples
    #
    #   DeviceModel.for_category_class_name("Gateway")
    #   # => [<#DeviceModel>,<#DeviceModel>,<#DeviceModel>]
    #
    # Returns an array of models
    def for_category_class_name(object_type)
      joins(:category).where("device_categories.class_name = ?", object_type)
    end

    # Public: creates and executes a query that looks for single model object that matches the passed
    # variables. Find's a model that have a category with a class_name that is equal to the object_type
    # and a name equal to the model_version
    #
    # object_type - A string that is a device's object type
    # model_version  - A string that is a device's hw_version
    #
    # Examples
    #
    #   DeviceModel.find_model("Gateway","8200")
    #   # => <#DeviceModel> || nil
    #
    # Returns a single model or nil
    def find_model(object_type, model_version)
      for_category_class_name(object_type).find_by_model_version(model_version)
    end

    # Public: Returns the default model for a given category, used by the lookup method if it's
    # initial search comes up empty
    #
    # object_type - A string that is a device's object type
    #
    # Examples
    #
    #   DeviceModel.default("Gateway")
    #   # => <#DeviceModel :name => "Default"> || nil
    #
    # Returns a model object with the name as "Default" or nil
    def default(object_type)
      return nil unless object_type
      return nil unless DeviceCategory.find_by_class_name(object_type).present?
      find_model(object_type, object_type.constantize.default_model)
    end
  end

  private

  # Internal: Sets default values for a Category object.
  def default_values
    self.name ||= "N/A"
  end

  #Chained method in strings are not working in the group_method for a grouped_collection_select
  #I suspect this changed in Rails 4
  def sort_device_firmwares_reverse
    self.device_firmwares.sort.reverse
  end

  def sort_active_device_firmwares_reverse
    self.device_firmwares.active.sort.reverse
  end
end
