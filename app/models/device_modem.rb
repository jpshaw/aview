class DeviceModem < ActiveRecord::Base
  before_save :upcase_cnti

  MINIMUM_SIGNAL_STRENGTH_PERCENTAGE = 0.freeze

  LTE_NETWORKS = %w(LTE)
  FOUR_G_NETWORK = %w(LTE HSPA+ WIMAX HSPA-PLUS)
  THREE_G_NETWORK = %w(EV-DO UMTS HSDPA/HSUPA UTRAN HSDPA,HSUPA EVDO0 HSDPA)
  TWO_G_NETWORK = %w(IS-95 CDMA CDMA-1X CDMA-1XEVDO GSM GPRS EDGE 1XRTT)
  NETWORKS = FOUR_G_NETWORK + THREE_G_NETWORK + TWO_G_NETWORK

  belongs_to :device
  belongs_to :cellular_location
  has_many :speed_tests, class_name: 'SpeedTest', foreign_key: :device_modem_id, dependent: :destroy

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :apn, :carrier, :cid, :cnti, :ecio, :esn, :iccid, :imei, :lac,
    :maker, :model, :name, :number, :revision, :signal, :imsi, :rx,
    :tx, :rx_30, :tx_30, :device_id, :sms_sent_at, :rsrp, :rsrq, :rssi, :sinr, :snr,
    :usb_speed, :mcc, :mnc, :band, :sku, :bars, :active, :sim_slot

  validates :device_id, presence: true, numericality: { only_integer: true, allow_nil: true }
  validates :apn,       length: { maximum: 255, allow_blank: true }
  validates :carrier,   length: { maximum: 255, allow_blank: true }
  validates :cid,       length: { maximum: 20, allow_blank: true }
  validates :cnti,      length: { maximum: 15, allow_blank: true }
  validates :ecio,      length: { maximum: 10, allow_blank: true }
  validates :esn,       length: { maximum: 20, allow_blank: true }
  validates :iccid,     length: { maximum: 25, allow_blank: true }
  validates :imei,      length: { maximum: 25, allow_blank: true }
  validates :lac,       length: { maximum: 10, allow_blank: true }
  validates :maker,     length: { maximum: 255, allow_nil:   true }
  validates :mcc,       length: { maximum: 10, allow_blank: true }
  validates :mnc,       length: { maximum: 10, allow_blank: true }
  validates :model,     length: { maximum: 20, allow_nil:   true }
  validates :name,      length: { maximum: 255, allow_blank: true }
  validates :number,    length: { maximum: 20, allow_blank: true }
  validates :imsi,      length: { maximum: 25, allow_blank: true }
  validates :rsrp,      length: { maximum: 10, allow_blank: true }
  validates :rsrq,      length: { maximum: 10, allow_blank: true }
  validates :rssi,      length: { maximum: 10, allow_blank: true }
  validates :sinr,      length: { maximum: 10, allow_blank: true }
  validates :snr,       length: { maximum: 10, allow_blank: true }
  validates :usb_speed, length: { maximum: 10, allow_blank: true }
  validates :rx,        numericality: { only_integer: true, allow_nil: true }
  validates :tx,        numericality: { only_integer: true, allow_nil: true }
  validates :rx_30,     numericality: { only_integer: true, allow_nil: true }
  validates :tx_30,     numericality: { only_integer: true, allow_nil: true }
  validates :bars,      numericality: { only_integer: true, allow_nil: true }
  validates :sim_slot,  numericality: { only_integer: true, allow_nil: true }

  before_validation :adjust_signal
  before_save :check_carrier
  before_save :update_cell_location, if: :cell_location_changed?
  before_save :save_cellular_utilization, if: :utilization_changed?

  scope :with_deployed_device, ->() { joins(:device).merge(Device.with_deployment Devices::Deployment::DEPLOYED) }
  scope :by_active, ->() { order(active: :desc) }

  def self.carrier(carrier)
    case carrier
    when "at&t"     then "AT&T"
    when "bell.ca"  then "BellCA"
    when "mobinil"  then "Mobinil"
    when /rogers.*/ then "Rogers"
    when "sprint"   then "Sprint"
    when "telecom"  then "Telecom"
    when "telus"    then "Telus"
    when "t-mobile" then "T-Mobile"
    when "verizon"  then "Verizon"
    when "vodafone" then "Vodafone"
    else carrier
    end
  end

  def self.network_type(cnti)
    if FOUR_G_NETWORK.include? cnti.to_s.upcase
      '4G'
    elsif THREE_G_NETWORK.include? cnti.to_s.upcase
      '3G'
    elsif TWO_G_NETWORK.include? cnti.to_s.upcase
      '2G'
    else
      'unknown'
    end
  end

  def status
    self.active? ? "Active" : "Inactive"
  end

  def self.percent_to_signal(percent)
    ((percent * 62 / 100.0) - 112)
  end

  def self.signal_to_percent(signal)
    (((signal + 112) * 100) / 62).round
  end

  def lte?
    LTE_NETWORKS.include?(self.cnti.try(:upcase))
  end

  def rx_delta
    return 0 unless rx_changed?
    old_rx = rx_was.to_i
    new_rx = rx.to_i

    positive_delta(old_rx, new_rx)
  end

  def tx_delta
    return 0 unless tx_changed?
    old_tx = tx_was.to_i
    new_tx = tx.to_i

    positive_delta(old_tx, new_tx)
  end

  def positive_delta(old_data, new_data)
    if new_data < old_data
      new_data
    else
      new_data - old_data
    end
  end

  # Public: Used to adjust the signal if it's outside the range of -112 to -50.
  def adjust_signal
    if self.signal.present?
      self.signal = -50 if self.signal > -50
      self.signal = -112 if self.signal < -112
    end
  end

  # Public: If the signal value is available it will translate it into a
  # percentage string so that it is easier to read for the customer. Otherwise
  # it will return N/A.
  #
  # Examples
  #
  # @modem.signal = -80
  # @modem.signal_pct
  # # => "52%"
  #
  # @modem.signal = nil
  # @modem.signal_pct
  # # => "N/A"
  #
  # Returns a string.
  def signal_pct
    return "N/A" unless self.signal.present?
    "#{signal_percent_value}%"
  end

  # Public: If the signal value is available, return the dBm signal strength and
  # the calculated percentage.  Otherwise, return N/A.
  #
  # Example
  # @modem.signal = -80
  # @modem.signal_str
  # # => "-80 dbm (52%)"
  def signal_str
    return "N/A" unless self.signal.present?
    "#{self.signal} dBm (#{signal_pct})"
  end

  # Public land mobile network ID
  # http://en.wikipedia.org/wiki/Public_land_mobile_network
  def plmn
    return if mcc.blank? || mnc.blank?
    "#{mcc}#{mnc}"
  end

  # Public: Shows the cnti network type (such as LTE) along with the network
  # categoy (2G, 3G, or 4G).
  def cnti_details
    return if cnti.nil?
    "#{cnti} (#{::DeviceModem.network_type(cnti)})"
  end

  def utilization_changed?
    rx_changed? || tx_changed?
  end

  def can_receive_sms_commands?
    if jasper_api_enabled?
      iccid.present?
    else
      number.present?
    end
  end

  # Public: Checks to see if an sms has been sent to this modem within the
  # last 60 seconds.
  #
  # Returns true if it has received an sms in the last 60 seconds, otherwise
  # false.
  def sent_sms_recently?
    sms_sent_at.present? && (sms_sent_at + 60.seconds) > Time.now
  end

  # Public: Returns the amount of seconds left until an sms can be sent to
  # this modem.
  def time_left_til_sms_allowed
    60 - (Time.now - sms_sent_at).to_i
  end


  def signal_below_threshold?
    signal_percent_value < signal_strength_threshold
  end

  # Use a fancy-dancy image for the bars if we have one
  def signal_bars
    return bars unless bars.present? && bars.between?(1,5)
    ActionController::Base.helpers.image_tag("signal_bars#{bars}.png")
  end

  def title
    sim_slot.present? ? "SIM Slot #{sim_slot}" : model
  end

  def serializable_hash(options = {})
    serialized_hash = super(options.merge(
      except: [:id,
               :created_at,
               :updated_at,
               :device_id,
               :cellular_location_id,
               :band,
               :data_used,
               :number,
               :signal,
               :sms_sent_at],
      methods: [:signal_pct]))
    serialized_hash[:dbm] = signal
    serialized_hash[:last_speed_test] = speed_tests.last.formatted_results if speed_tests.any?
    serialized_hash[:phone_number] = number
    serialized_hash[:signal] = signal_str
    serialized_hash
  end

  private

  def jasper_api_enabled?
    @jasper_api_enabled ||= Settings.api_keys.jasper.enabled?
  end

  def signal_percent_value
    self.class.signal_to_percent(signal)
  end


  def signal_strength_threshold
    (AdminSettings.threshold_percentage || MINIMUM_SIGNAL_STRENGTH_PERCENTAGE).to_i
  end

  def check_carrier
    if carrier_changed?
      self.carrier = self.class.carrier(self.carrier)
    end
  end

  def cell_location_changed?
    lac_changed? || cid_changed? || mcc_changed? || mnc_changed?
  end

  def upcase_cnti
    cnti.upcase! if cnti.present?
  end

  def update_cell_location
    queue = TorqueBox.fetch('/queues/cellular_location')
    queue.publish({ id: self.id, lac: self.lac, cid: self.cid, mnc: self.mnc, mcc: self.mcc }) if queue
  end

  def save_cellular_utilization
    return if device.nil?

    begin
      Measurement::CellularUtilization.create(
              mac: device.mac,
  organization_id: device.organization_id,
          carrier: carrier,
        interface: device.primary_interface,
        data_plan: device.data_plan_id,
               rx: rx_delta,
               tx: tx_delta,
             time: Time.now
      )
    rescue MetricsService::Error => e
      Rails.logger.error e.message
    end
  end
end
