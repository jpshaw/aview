class SubscribedEvent < ActiveRecord::Base
  belongs_to :profile, :class_name => "NotificationProfile"
  belongs_to :uuid, :class_name => "DeviceEventUUID"

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :profile_id, :uuid_id, :frequency

  validates :profile_id, numericality: { only_integer: true, allow_nil: true }
  validates :uuid_id,    numericality: { only_integer: true, allow_nil: true }, uniqueness: { scope: :profile_id }
  validates :frequency,  numericality: { only_integer: true, allow_nil: true }

  # Public: Checks if the subscribed event should have notifications
  # delivered immediately.
  #
  # Returns true if it should be delivered immediately, otherwise false.
  def immediate?
    frequency == NotificationProfile::Frequencies::IMMEDIATELY
  end

  # Public: Checks if the subscribed event should never have notifications
  # delivered.
  #
  # Returns true if it shouldn't ever be delivered, otherwise false.
  def never?
    frequency == NotificationProfile::Frequencies::NEVER
  end
end
