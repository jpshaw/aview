class IDSAlert < ActiveRecord::Base
  scope :recent_for_host, ->(host) { where(host: host, alerted_at: 30.days.ago..Time.current) }
  scope :grouped_by_sig, -> do
    select('msg, sig_id, sig_rev, reference, COUNT(*) as count')
      .group(:sig_id, :sig_rev)
      .order('count desc')
  end

  def row_format
    [
      msg,
      "#{sig_rev}-#{sig_id}",
      reference,
      count
    ]
  end
end
