class Configuration < ActiveRecord::Base

  self.inheritance_column = :type

  has_closure_tree

  store :data, coder: JSON

  delegate :inherited_data, to: :parent, prefix: true, allow_nil: true
  delegate :name, to: :parent, prefix: true, allow_nil: true

  SYSTEM_NAME = 'SYSTEM-DEFAULTS'.freeze

  belongs_to :resource, polymorphic: true

  def inherited_data
    return data if parent.blank?
    data.deep_merge(parent.data) do |key, this_val, other_val|
      this_val.blank? ? other_val : this_val
    end
  end

  def self.system
    where(name: SYSTEM_NAME).first_or_create
  end

  protected

  def is_boolean?(field)
    field.in?([true, false])
  end

  # It is possible for a parent to set a boolean to true, and a child to set it
  # to false. We should honor the child's settings over the parent here.
  def inheritable_boolean(name)
    if self.respond_to?(name) && is_boolean?(send(name))
      send(name)
    elsif parent.present? && parent.respond_to?("#{name}?")
      parent.send("#{name}?")
    else
      # If an inheritable field defaults to true we'll need to create a constant
      # to use here. Let's keep it simple for now.
      false
    end
  end
end
