class CellularLocation < ActiveRecord::Base
  has_many :device_modems

  scope :find_by_modem, ->(modem) { where(lac: modem.lac, cid: modem.cid, mcc: modem.mcc, mnc: modem.mnc) }

  def has_valid_map_data?
    lat.present? && lon.present?
  end
end
