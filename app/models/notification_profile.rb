require 'constants_helper'

class NotificationProfile < ActiveRecord::Base
  belongs_to :user

  has_many :subscriptions, :class_name => "NotificationSubscription",
    :foreign_key => "profile_id", :dependent => :destroy

  has_many :subscribed_events, :foreign_key => "profile_id", :dependent => :destroy

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :user_id, :frequency, :subscribed_events_attributes

  accepts_nested_attributes_for :subscribed_events, allow_destroy: true

  validates :name, :presence => true, :length => { :maximum => 30 },
    :uniqueness => { :scope => :user_id, :case_sensitive => false }
  validates :user_id, :presence => true, :numericality => { :only_integer => true }

  scope :by_created_at, ->(direction = :desc) {
    order("created_at #{direction == :desc ? :desc : :asc}")
  }

  # Public: Attaches event uuids and their frequencies to the profile.
  #
  # uuids - A Hash of event uuid id's mapped to their frequencies (default: {}):
  #         :id - The Integer ID of the uuid to tag for the subscription.
  #         :freq - The Integer frequency value to set for the event uuid.
  #
  # Returns nothing.
  def set_uuids(uuids = {})
    return unless uuids.is_a?(Hash)

    # For each uuid, get the subscribed event and set the frequency.
    uuids.each do |key, value|
      uuid_id   = key.to_i
      frequency = value.to_i

      # Ensure the frequency is valid
      next unless Frequencies.valid?(frequency)

      # Ensure the UUID is valid
      uuid = DeviceEventUUID.find_by(id: uuid_id)
      next if uuid.nil?

      # Create subscribed event
      subscribed_event = subscribed_events.where(:uuid_id => uuid.id).first_or_create(:frequency => frequency)

      # Update existing event if frequencies don't match
      unless subscribed_event.frequency == frequency
        subscribed_event.frequency = frequency
      end

      subscribed_event.save if subscribed_event.changed?
    end
  end

  # Public: Creates and returns a hash that maps uuids to their frequencies.
  #
  # Returns a hash of uuid => frequency set for the profile.
  def subscribed_event_settings
    retval = {}
    subscribed_events.each do |event|
      retval[event.uuid_id] = event.frequency
    end
    retval
  end

  # Public: Contains the frequencies available to notification profiles.
  module Frequencies
    include ConstantsHelper

    NEVER            = 9999
    IMMEDIATELY      = 0
    EVERY_15_MINUTES = 15
    EVERY_30_MINUTES = 30
    EVERY_HOUR       = 60
    EVERY_2_HOURS    = 120
    EVERY_4_HOURS    = 240
    EVERY_6_HOURS    = 360
    EVERY_12_HOURS   = 720
    EVERY_24_HOURS   = 1440

    # Public: Checks if a frequency is valid.
    #
    # frequency - The Integer frequency to validate.
    #
    # Returns true if the frequency is valid, otherwise false.
    def self.valid?(frequency)
      constants_values_list.include?(frequency)
    end

    # Public: Creates and returns a list of <select> options containing notification profile frequencies.
    #
    # Examples
    #
    #   Frequencies.select_list
    #   # => [["Never", 9999], ["Immediately", 0], ["Every 15 Minutes", 15], ["Every 30 Minutes", 30], ["Every Hour", 60],
    #        ["Every 2 Hours", 120], ["Every 4 Hours", 240], ["Every 6 Hours", 360], ["Every 12 Hours", 720], ["Every 24 Hours", 1440]]
    #
    # Returns a list of <select> options containing notification profile frequencies.
    def self.select_list
      [
        ["Immediately",      IMMEDIATELY],
        ["Every 15 Minutes", EVERY_15_MINUTES],
        ["Every 30 Minutes", EVERY_30_MINUTES],
        ["Every Hour",       EVERY_HOUR],
        ["Every 2 Hours",    EVERY_2_HOURS],
        ["Every 4 Hours",    EVERY_4_HOURS],
        ["Every 6 Hours",    EVERY_6_HOURS],
        ["Every 12 Hours",   EVERY_12_HOURS],
        ["Every 24 Hours",   EVERY_24_HOURS]
      ]
    end
  end
end
