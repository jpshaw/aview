class TunnelServer < ActiveRecord::Base
  include Searchable

  has_and_belongs_to_many :devices
  has_many :ip_addresses, class_name: 'TunnelServerIpAddress', dependent: :destroy

  validates :name,
            presence: true,
            uniqueness: { case_sensitive: false,
                          scope: :location,
                          message: 'with location already exists' }

  has_search_scope :with_name_search, ->(search) { where("tunnel_servers.name LIKE ?", "%#{search}%") }
  has_search_scope :with_location_search, ->(search) { where("tunnel_servers.location LIKE ?", "%#{search}%") }

  has_sort_scope :name
  has_sort_scope :location

  def server_type
    'VIG'
  end

  def self.find_by_ip_address(ip_address)
    joins(:ip_addresses).where(tunnel_server_ip_addresses: { ip_address: ip_address }).first
  end

end