class DeviceNetwork < ActiveRecord::Base

  LAN = 'lan'

  belongs_to :device

  serialize :ip_addrs

  validates :device_id, numericality: { only_integer: true, allow_nil: true }, uniqueness: { scope: :network_iface }
  validates :index, numericality: { only_integer: true, allow_nil: true }
  validates :network_iface, length: { maximum: 20, allow_blank: true }
  validates :metric0, numericality: { only_integer: true, allow_nil: true }
  validates :mtu, numericality: { only_integer: true, allow_nil: true }
  validates :mtu_ipv6, numericality: { only_integer: true, allow_nil: true }
  validates :rx, numericality: { only_integer: true, allow_nil: true }
  validates :tx, numericality: { only_integer: true, allow_nil: true }


  scope :lan, -> { where(net_type: LAN) }

  # Returns a hash where the key is changed to just be the ip index.
  def ip_addresses
    return {} if ip_addrs.blank? || !ip_addrs.respond_to?(:inject)

    ip_addrs.inject({}) do |hash, (key, address)|
      if index = key.to_s.scan(/\d+$/).first
        hash[index] = address if address.present?
      end
      hash
    end
  end
end
