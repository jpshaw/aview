class DataPlan < ActiveRecord::Base

  belongs_to :organization
  has_many :devices

  validates :name,            presence: true,
                              length:   { maximum: 50, allow_blank: false },
                              uniqueness: {
                                scope:          :organization_id,
                                case_sensitive: false,
                                message:        'already exists for the organization'
                              }
  validates :cycle_type,      presence: true
  validates :cycle_start_day, presence: true
  validates :data_limit,      presence: true

  before_update :reset_usage, if: :cycle_start_day_changed?
  before_destroy :remove_from_devices

  enum cycle_type: [:monthly]

  scope :for_organization, ->(organization) { where(organization_id: organization) }

  DEFAULT_DATA_LIMIT_IN_MB = 5000.freeze
  BYTES_IN_A_MEGABYTE = 1_000_000.00.freeze
  DAY_INTERVAL = 86400.freeze

  def convert_data_limits_to_bytes
    self.data_limit = self.data_limit * BYTES_IN_A_MEGABYTE
    self.individual_data_limit = self.individual_data_limit * BYTES_IN_A_MEGABYTE if self.individual_data_limit
  end

  def convert_data_limits_to_megabytes
    self.data_limit = self.data_limit / BYTES_IN_A_MEGABYTE
    self.individual_data_limit = self.individual_data_limit / BYTES_IN_A_MEGABYTE if self.individual_data_limit
  end

  def data_limit_as_mb
    self.data_limit.to_i / BYTES_IN_A_MEGABYTE
  end

  def individual_data_limit_as_mb
    self.individual_data_limit.to_i / BYTES_IN_A_MEGABYTE
  end

  def exceeds_total_limit_for_first_time?(current_data_usage)
    previous_data_usage = data_used ? data_used : 0
    previous_data_usage < data_limit && current_data_usage > data_limit
  end

  def exceeds_individual_limit_for_first_time?(device, current_device_usage)
    return false unless individual_data_limit.present?

    if device.modem.present?
      previous_device_usage = device.modem.data_used.present? ? device.modem.data_used : 0
      previous_device_usage < individual_data_limit && current_device_usage > individual_data_limit
    else
      false
    end
  end

  def save_data_usage(current_data_usage)
    self.data_used = current_data_usage
    self.save!
  end

  def current_usage
    Measurement::CellularUtilization.data_plan_usage({ data_plan_id: id, start_time: current_period_start_time })
  end

  def usage_for(device)
    Measurement::CellularUtilization.data_plan_usage({ data_plan_id: id,
                                                       start_time: current_period_start_time,
                                                       mac: device.mac })
  end

  def current_period_start_time
    now = Time.now.utc
    month = cycle_start_day_is_last_month? ? 1.month.ago.month : now.month
    year = cycle_start_day_is_last_month? && month == 12 ? 1.year.ago.year : now.year
    day = cycle_start_day_is_last_month? && cycle_start_day > Time.days_in_month(month, year) ?
        1.month.ago.end_of_month.day : cycle_start_day
    start_time = DateTime.new(year, month, day).utc.to_i
    start_time = start_time - (start_time % DAY_INTERVAL)
    Time.at(start_time).to_datetime.utc
  end

  private

  def reset_usage
    self.data_used = 0
  end

  def remove_from_devices
    Device.with_data_plan(self).update_all(data_plan_id: nil)
  end

  def cycle_start_day_is_last_month?
    @start_day_last_month ||= Time.now.utc.day < cycle_start_day
  end
end
