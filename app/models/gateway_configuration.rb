class GatewayConfiguration < ActiveRecord::Base

  belongs_to :portal, class_name: "GatewayPortalProfile"
  belongs_to :snmp,   class_name: "SnmpProfile"
  belongs_to :organization

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :snmp_enabled, :snmp_id, :portal_id

  validates :snmp_enabled,  inclusion: { in: [true, false] }
  validates :snmp_id,       numericality: { only_integer: true, greater_than_or_equal_to: 1, allow_nil: true }
  validates :portal_id,     numericality: { only_integer: true, greater_than_or_equal_to: 1, allow_nil: true }

  DEFAULT_USER = 'support'.freeze

  def user
    DEFAULT_USER
  end

  def password
    self.try(:portal).try(:ssl_password)
  end

end
