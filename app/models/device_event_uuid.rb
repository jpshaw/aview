require 'event_uuids'

# Public: Unique event id's used for notifications.
class DeviceEventUUID < ActiveRecord::Base
  belongs_to :category, class_name: "DeviceCategory"
  has_many   :models, :through => :category
  has_many   :subscribed_events, foreign_key: :uuid_id

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :code, :category_id

  validates :name,        presence: true, length: { maximum: 255 }
  validates :category_id, presence: true, numericality: { only_integer: true }
  validates :code,        numericality: { only_integer: true, allow_nil: true }

  # Public: Finds a UUID object by matching a symbol in the UUIDS module.
  #
  # sym - The Symbol that represents the UUID in the UUIDS module.
  #
  # Returns the UUID object if found, otherwise nil.
  def self.find_by_symbol(sym)
    return unless sym.is_a?(Symbol)
    uuids = EventUUIDS.constants_hash
    if uuids.has_key?(sym)
      find_by_code(uuids[sym])
    end
  end
end
