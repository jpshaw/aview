# Public: Model for individual countries.
#
# code - The String code of the country - Ex: "US". Must be 2 characters.
# name - The String name of the country - Ex: "United States". Limit 100 characters.
class Country < ActiveRecord::Base

  has_many :locations
  has_many :regions, class_name: 'Region'

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :code, :name

  validates :code, :presence => true, :length => { :is => 2 },
    :uniqueness => { :case_sensitive => false }

  validates :name, :presence => true, :length => { :minimum => 2, :maximum => 100 },
    :uniqueness => { :case_sensitive => false }

  class << self

    # Public: Create an array of countries to use for a select dropdown box.
    #
    # Returns an array of countries, leading with 'Select country'.
    def select_list
      select("id, name").order('name asc').load.to_a.map { |country|
        [country.name, country.id]
      }.unshift(["Select country", 0])
    end

    def code_for_id(id)
      where(id: id).limit(1).pluck(:code).first
    end

    def code_for_name(name)
      where(name: name).limit(1).pluck(:code).first
    end

  end

end
