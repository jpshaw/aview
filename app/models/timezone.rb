class Timezone < ActiveRecord::Base
  self.table_name = "timezones"

  default_scope { order(id: :asc) }

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :official_name, :off

  validates :name,          :presence => true, :length => { :maximum => 50 }
  validates :official_name, :presence => true, :length => { :maximum => 100 }
  validates :off,           :presence => true, :length => { :maximum => 6 }

  # Public: Gets the default timezone (EST).
  def self.default
    where(:off => "-300").first
  end
end
