class OrganizationsContacts < ActiveRecord::Base
  belongs_to :organization
  belongs_to :contact

  validates :organization_id, presence: true, numericality: { only_integer: true }
  validates :contact_id,      presence: true, numericality: { only_integer: true }
end
