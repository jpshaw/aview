require 'openssl'

class CertificateAuthority

  def initialize(csr)
    @request = OpenSSL::X509::Request.new csr
  end


  def details
    @request
  end


  def common_name
    @request.subject.to_a.last[1].downcase
  end


  # Accepts a cert request and an optional CA cert and key (uses configured defaults).
  # The cert request, CA cert, and key can be strings or OpenSSL Request/Certificate/PKey objects.
  # Returns the signed certificate as a string.
  def sign_request(crt=nil, key=nil)
    crt ||= File.read(File.expand_path(Settings.certificates.ca_crt_path))
    key ||= File.read(File.expand_path(Settings.certificates.ca_key_path))

    # Convert textey-bits into an object - works even if it's already objecticated.
    csr = OpenSSL::X509::Request.new      @request
    crt = OpenSSL::X509::Certificate.new  crt
    key = OpenSSL::PKey::RSA.new          key

    client_cert             = OpenSSL::X509::Certificate.new
    client_cert.subject     = csr.subject
    client_cert.issuer      = crt.subject
    client_cert.public_key  = csr.public_key
    client_cert.not_before  = Time.now
    client_cert.not_after   = Time.now + Settings.certificates.years_valid.years
    client_cert.serial      = OpenSSL::BN.rand(
                                Settings.certificates.serial_bits,
                                Settings.certificates.serial_fill
                              )

    client_cert.sign(key, OpenSSL::Digest::SHA1.new)
    client_cert.to_s
  end

end
