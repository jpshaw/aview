class NetreachConfiguration < ActiveRecord::Base

  has_many :devices, :foreign_key => "configuration_id"

  belongs_to :organization

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :admin_access, :admin_password, :admin_username, :channel, :channel_region,
    :country_code, :firmware, :dst_enabled, :log_enabled, :log_ipv4, :name, :network_mode,
    :organization_id, :proxy_host, :proxy_port, :rf_power, :ssid_1, :ssid_1_advertise, :ssid_1_ipv4,
    :ssid_1_pass, :ssid_1_pass_t, :ssid_1_port, :ssid_1_sec, :ssid_1_secret, :ssid_1_timeout,
    :ssid_1_wep_len, :ssid_1_wpa_c, :ssid_2, :ssid_2_advertise, :ssid_2_ipv4, :ssid_2_pass,
    :ssid_2_pass_t, :ssid_2_port, :ssid_2_sec, :ssid_2_secret, :ssid_2_timeout, :ssid_2_wep_len,
    :ssid_2_wpa_c, :ssid_1_enabled, :ssid_2_enabled, :ssid_1_vlan_id, :ssid_2_vlan_id,
    :tz_offset, :wds_mac_1, :wds_mac_2, :wds_mac_3, :wds_mode, :config_int_secs, :wds_phy_mode,
    :wds_encryption, :wds_pass_phrase, :station_query, :anms_enabled, :client_monitor, :remote_control,
    :upnp_enabled, :wan_connection, :wan_ip_address, :wan_gateway_ip, :wan_netmask, :dns1, :dns2, :analytics_mode

  validates :name, :presence => true, :length => { :maximum => 50 },
    :uniqueness => { :case_sensitive => false, :scope => :organization_id }

  validates :organization_id, :presence => true,
    :numericality => { :only_integer => true, :greater_than_or_equal_to => 1, :allow_nil => true }

  validates :admin_access, :inclusion => {:in => [true, false]}
  validates :admin_password, :length => { :maximum => 50 }
  validates :admin_username, :length => { :maximum => 50 }
  validates :channel, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (0..14)}, :allow_nil => true
  validates :channel_region, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (0..6)}, :allow_nil => true
  validates :country_code, :length => { :maximum => 2 }
  validates :firmware, :length => { :maximum => 15 }
  validates :dst_enabled, :inclusion => {:in => [true, false]}
  validates :log_enabled, :inclusion => {:in => [true, false]}
  validates :log_ipv4, :length => { :maximum => 30 }
  validates :network_mode, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (1..6)}, :allow_nil => true
  validates :proxy_host, :length => { :maximum => 100 }
  validates :proxy_port, :length => { :maximum => 10 }
  validates :rf_power, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (1..100)}, :allow_nil => true
  validates :ssid_1, :length => { :maximum => 50 }
  validates :ssid_1_advertise, :inclusion => {:in => [true, false]}
  validates :ssid_1_ipv4, :length => { :maximum => 50 }
  validates :ssid_1_pass, :length => { :maximum => 50 }
  validates :ssid_1_pass_t, :length => { :maximum => 50 }
  validates :ssid_1_port, :length => { :maximum => 10 }
  validates :ssid_1_sec, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (0..8)}, :allow_nil => true
  validates :ssid_1_secret, :length => { :maximum => 50 }
  validates :ssid_1_timeout, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (10..9999)}, :allow_nil => true
  validates :ssid_1_wep_len, :inclusion => {:in => [1, 2]}, :allow_nil => true
  validates :ssid_1_wpa_c, :length => { :maximum => 50 }
  validates :ssid_2, :length => { :maximum => 50 }
  validates :ssid_2_advertise, :inclusion => {:in => [true, false]}
  validates :ssid_2_ipv4, :length => { :maximum => 50 }
  validates :ssid_2_pass, :length => { :maximum => 50 }
  validates :ssid_2_pass_t, :length => { :maximum => 50 }
  validates :ssid_2_port, :length => { :maximum => 10 }
  validates :ssid_2_sec, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (0..8)}, :allow_nil => true
  validates :ssid_2_secret, :length => { :maximum => 50 }
  validates :ssid_2_timeout, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (10..9999)}, :allow_nil => true
  validates :ssid_2_wep_len, :inclusion => {:in => [1, 2]}, :allow_nil => true
  validates :ssid_2_wpa_c, :length => { :maximum => 50 }
  validates :ssid_1_enabled, :inclusion => {:in => [true, false]}
  validates :ssid_2_enabled, :inclusion => {:in => [true, false]}
  validates :ssid_1_vlan_id, :length => { :maximum => 50 }
  validates :ssid_2_vlan_id, :length => { :maximum => 50 }
  validates :tz_offset, :length => { :maximum => 10 }
  validates :wds_mac_1, :length => { :maximum => 30 }
  validates :wds_mac_2, :length => { :maximum => 30 }
  validates :wds_mac_3, :length => { :maximum => 30 }
  validates :wds_mode, :inclusion => {:in => [0, 2, 3, 4]}, :allow_nil => true
  validates :config_int_secs, :numericality => { :only_integer => true}, :inclusion => {:in => ((720..525600).to_a << 0)}, :allow_nil => true
  validates :wds_phy_mode, :length => { :maximum => 10 }, :inclusion => {:in => ["HTMIX", "CCK", "OFDM"]}, :allow_nil => true
  validates :wds_encryption, :length => { :maximum => 10 }, :inclusion => {:in => ["NONE", "AES", "TKIP"]}, :allow_nil => true
  validates :wds_pass_phrase, :length => { :maximum => 50 }
  validates :station_query, :inclusion => {:in => [true, false]}
  validates :anms_enabled, :inclusion => {:in => [true, false]}
  validates :client_monitor, :inclusion => {:in => [true, false]}
  validates :remote_control, :numericality => { :only_integer => true, :allow_nil => true }, :inclusion => {:in => (0..60)}, :allow_nil => true
  validates :upnp_enabled, :inclusion => {:in => [true, false]}
  validates :wan_connection, :length => { :maximum => 10 }, :inclusion => {:in => ["DHCP", "STATIC"]}, :allow_nil => true
  validates :wan_ip_address, :length => { :maximum => 20 }
  validates :wan_gateway_ip, :length => { :maximum => 20 }
  validates :wan_netmask, :length => { :maximum => 20 }
  validates :dns1, :length => { :maximum => 20 }
  validates :dns2, :length => { :maximum => 20 }
  validates :analytics_mode, :inclusion => {:in => [true, false]}

  class << self

    def policy_class
      OrganizationPolicy
    end

    def default
      where(:name => "Default").first_or_create
    end

  end

end
