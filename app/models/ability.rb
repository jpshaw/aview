require 'ability_groups'

class Ability < ActiveRecord::Base

  has_many :role_abilities, dependent: :destroy
  has_many :roles, through: :role_abilities

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :description, :name, :group_id

  validates :name,        :presence => true, :uniqueness => true
  validates :group_id,    :presence => true

  MODIFY_GROUPS = 'Modify Groups'

end
