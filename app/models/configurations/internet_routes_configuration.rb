class InternetRoutesConfiguration < ::Configuration

  store :data, accessors: [ :routes ], coder: JSON

  after_initialize :init

  private

  def init
    self.routes ||= []
  end

end
