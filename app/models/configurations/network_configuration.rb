class NetworkConfiguration < ::Configuration

  store :data, accessors: [
    :dhcp,
    :lan,
    :intelliflow_enabled,
    :ids_enabled
  ], coder: JSON

  def intelliflow_enabled?
    inheritable_boolean(:intelliflow_enabled)
  end

  def ids_enabled?
    inheritable_boolean(:ids_enabled)
  end

end
