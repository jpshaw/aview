module Highcharts
  class IntelliflowAppUsageChart
    def initialize(attrs)
      @range = attrs[:range]
      @device_id = attrs[:device_mac]
    end

    def as_json(options = {})
      {
        series: data_usage_per_app.map { |k, v| { name: k, y: v } }
      }
    end

    def data_usage_per_app
      intelliflow_repository.data_usage_per_app({ device_mac: @device_id,
                                              from_date_time: intelliflow_repository.from_date_time(@range),
                                              to_date_time: Time.now.utc,
                                              sample_size: 10 })
    end

    def intelliflow_repository
      @intelliflow_repository ||= Intelliflow.new
    end
  end
end
