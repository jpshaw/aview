module Highcharts
  class CellularUtilizationChart
    def initialize(params)
      @mac = params[:id]
      @range = Measurement::CellularUtilization.range_from_allowed_ranges(params[:range])
    end

    def response
      response = {}
      response[:series_by_carrier] = series_by_carrier
      response
    end

    private

    def series_by_carrier
      result = []
      response = Measurement::CellularUtilization.points_by_carrier_for_mac(
        @mac,
        @range,
        Measurement::CellularUtilization.interval_by_range(@range)
      )
      response.each do |carrier_group|
        name = carrier_group["tags"]["carrier"]
        result << {
          carrier_name: name,
          carrier_series: carrier_series_objects(carrier_group),
          received_total: utilization_totals_received,
          transmitted_total: utilization_totals_transmitted
        }
      end
      result
    end

    def carrier_color(carrier_name)
      Carrier.find_by(name: carrier_name).try(:color) || '#7CB5EC'
    end

    def carrier_series_objects(carrier_group)
      [
        { name: 'Received', data: carrier_series_object_data(carrier_group, 'received'), color: carrier_color(carrier_group["tags"]["carrier"]) },
        { name: 'Transmitted', data: carrier_series_object_data(carrier_group, 'transmitted'), color: '#434348' }
      ]
    end

    def carrier_series_object_data(carrier_group, direction)
      total = 0
      object_data = carrier_group["values"].first(point_limit(@range)).map{|point| { x: point["time"], y: point[direction] }}
      object_data.each {|hash| total += hash[:y]}
      utilization_totals(direction, total)
      object_data
    end


    def utilization_totals_received(total = 0)
      @utilization_totals_received ||= total
    end

    def utilization_totals_transmitted(total = 0)
      @utilization_totals_transmitted ||= total
    end

    private

    def utilization_totals(direction, total)
      my_method = 'utilization_totals_' + direction
      self.send(my_method, total)
    end

    # TODO: Influx should be handling this
    def point_limit(range)
      Measurement::Base.limit_by_range(range)
    end
  end
end
