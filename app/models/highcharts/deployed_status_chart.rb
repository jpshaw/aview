module Highcharts
  class DeployedStatusChart

    DEPLOYED = 'Deployed'
    UNDEPLOYED = 'Undeployed'
    DEPLOYMENT_STATUSES = [DEPLOYED, UNDEPLOYED]

    def initialize(current_user)
      @current_user = current_user
    end

    def response
      response = {}
      if total_count > 0
        response[:links] = links
        response[:series] = series
        response[:total_count] = total_count
      else
        response[:series] = []
      end
      response
    end

    private

    def links
      links = {}
      DEPLOYMENT_STATUSES.each do |deployment|
        links[deployment] = ::Rails.application.routes.url_helpers.devices_path(with_deployment: "Device::Deployment::#{deployment.upcase}".constantize)
      end
      links
    end

    def series
      [{ name: DEPLOYED, y: deployed_count },{ name: UNDEPLOYED, y: undeployed_count }]
    end

    def deployed_count
      @deployed_count ||= @current_user.devices.deployed.count
    end

    def total_count
      @total_count ||= @current_user.devices.count
    end

    def undeployed_count
      @undeployed_count ||= total_count - deployed_count
    end
  end
end
