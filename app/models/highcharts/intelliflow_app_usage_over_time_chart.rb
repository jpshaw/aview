module Highcharts
  class IntelliflowAppUsageOverTimeChart
    def initialize(attrs)
      @range = attrs[:range]
      @device_id = attrs[:device_mac]
      @apps = attrs[:apps]
      @offset = Time.now.in_time_zone(attrs[:timezone]).formatted_offset
    end

    def as_json(options = {})
      {
        series: data
      }
    end

    def data
      @data ||= intelliflow_repository.data_usage_per_app_over_time({ device_mac: @device_id,
                                                                  from_date_time: intelliflow_repository.from_date_time(@range),
                                                                  to_date_time: Time.now.utc,
                                                                  interval: intelliflow_repository.interval(@range),
                                                                  apps: @apps,
                                                                  offset: @offset })
    end

    def intelliflow_repository
      @intelliflow_repository ||= Intelliflow.new
    end
  end
end
