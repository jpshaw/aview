module Highcharts
  class SignalStrengthChart
    SERIES_NAME = 'Devices'
    NUMBER_OF_COLUMNS = 10

    def initialize(current_user)
      @current_user = current_user
    end

    def response
      response = {}
      if signal_strength_counts_by_group.present?
        response[:links] = links
        response[:series] = series
      else
        response[:series] = [{data: []}]
      end
      response
    end

    private

    def links
      links = {}
      links[:link_key] = 'index'
      NUMBER_OF_COLUMNS.times do |index|
        links[index] = ::Rails.application.routes.url_helpers.devices_path(
          with_deployment: Devices::Deployment::DEPLOYED,
          with_signal_range: index
        )
      end
      links
    end

    def series
      [
        {
          name: SERIES_NAME,
          data: signal_strength_counts_by_group
        }
      ]
    end

    def signal_strength_counts_by_group
      @signal_strength_counts_by_group ||= DeviceBySignalStrengthRangeFinder.new(user: @current_user).execute
    end
  end
end
