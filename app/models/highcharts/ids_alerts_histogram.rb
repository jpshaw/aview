module Highcharts
  class IDSAlertsHistogram
    def initialize(params)
      @host = params[:mac]
      @alerts = get_alerts
    end

    def response
      result = []
      current_period = 30.days.ago.beginning_of_day
      while current_period <= Time.current.beginning_of_day
        next_period = current_period.advance(days: 1)
        alerts_for_current_period, self.alerts = partition_alerts(next_period)
        result << { x: format_time(current_period), y: alerts_for_current_period.count }
        current_period = next_period
      end

      { series: [ name: 'IDS Alerts', data: result ] }
    end

    private

    attr_accessor :alerts
    attr_reader :host

    def get_alerts
      IDSAlert.recent_for_host(host).order(:alerted_at).pluck(:alerted_at)
    end

    def format_time(time)
      time.to_i * 1000
    end

    def partition_alerts(next_period)
      alerts.partition{ |alert| alert < next_period }
    end

  end
end
