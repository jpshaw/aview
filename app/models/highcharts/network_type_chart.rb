module Highcharts
  class NetworkTypeChart

    FOUR_G = '4G'
    THREE_G = '3G'
    TWO_G = '2G'
    UNKNOWN = 'unknown'
    NETWORK_TYPES = [FOUR_G, THREE_G, TWO_G, UNKNOWN]

    def initialize(current_user)
      @current_user = current_user
    end

    def response
      response = {}
      if total_count > 0
        response[:links] = links
        response[:series] = series
        response[:total_count] = total_count
      else
        response[:series] = []
      end
      response
    end

    private

    def links
      {}.tap do |links|
        NETWORK_TYPES.each do |network|
          links[network] = ::Rails.application.routes.url_helpers.devices_path(
            with_deployment:Devices::Deployment::DEPLOYED,
            with_network: network
          )
        end
      end
    end

    def network_type_counts
      @network_type_counts ||= DeviceByNetworkTypeFinder.new(user: @current_user).execute
    end

    def series
      NETWORK_TYPES.map do |network|
        {
          color: network,
          name: network,
          y: network_type_counts[network]
        }
      end
    end

    def total_count
      network_type_counts.reduce(0){|sum, n| sum + n.last}
    end
  end
end
