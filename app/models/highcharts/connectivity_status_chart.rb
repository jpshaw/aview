module Highcharts
  class ConnectivityStatusChart
    attr_reader :current_user

    UP = 'Up'
    DOWN = 'Down'
    UNREACHABLE = 'Unreachable'
    STATES = [UP, DOWN, UNREACHABLE]

    def initialize(current_user)
      @current_user = current_user
    end

    def response
      response = {}
      if total_count > 0
        response[:links] = links
        response[:series] = series
        response[:total_count] = total_count
      else
        response[:series] = []
      end
      response
    end

    private

    def links
      {}.tap do |links|
        STATES.each do |status|
          links[status] = ::Rails.application.routes.url_helpers.devices_path(
            with_deployment: Devices::Deployment::DEPLOYED,
            with_status: "DeviceState::States::#{status.upcase}".constantize
          )
        end
      end
    end

    def series
      [
        { name: UP, y: up_count },
        { name: DOWN, y: down_count },
        { name: UNREACHABLE, y: unreachable_count }
      ]
    end

    def up_count
      @up_count ||= device_states.up.count
    end

    def down_count
      @down_count ||= device_states.down.count
    end

    def unreachable_count
      @unreachable_count ||= device_states.unreachable.count
    end

    def total_count
      @total_count ||= device_states.count
    end

    def device_states
      @device_states ||= DeviceState.where(device_id: @current_user.devices.deployed)
    end
  end
end
