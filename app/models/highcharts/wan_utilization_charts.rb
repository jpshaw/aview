module Highcharts
  class WanUtilizationCharts
    attr_reader :mac, :range
    ORDERED_SERIES_NAMES = %w(upload_cos upload_hwm upload download_cos download_hwm download)
    STATIC_MODE_SERIES = %w(upload download)
    HWM_SERIES = %w(upload_cos download_cos)
    BANDWIDTH_TYPE_STATIC = 2

    def initialize(params)
      @mac = params[:mac]
      @range = params[:range]
      @device = Device.find_by(mac: @mac)
    end

    def response
      build_response
    end

    private

    def bandwidth_test_result
      @bandwidth_test_result ||= begin
        results_by_interface = execute_bandwidth_test_result_query
        results_by_interface.each { |result| format_bandwidth_test_result(result) }
      end
    end


    def build_response
      bandwidth_promise = Concurrent::Promise.execute{ bandwidth_test_result }
      wan_promise       = Concurrent::Promise.execute{ wan_utilization }

      results     = bandwidth_promise.value || []
      wan_results = wan_promise.value       || []

      wan_results.map do |wan_utilization_for_interface|
        if target_interface = find_interface(results, wan_utilization_for_interface[:name])
          target_interface[:data] = target_interface[:data] + wan_utilization_for_interface[:data]
        else
          results << { name: wan_utilization_for_interface[:name], data: wan_utilization_for_interface[:data] }
        end
      end
      results.map{ |result| { name: result[:name], data: order_data(result[:data]) }}
    end

    def check_hwm_empty(data)
      HWM_SERIES.each do |key|
        series = find_interface(data, key)
        unless series[:data].select { |item| item[:y] > 0}.empty?
          return false
        end
      end
      return true
    end

    def supported_fields(data)
      unless new_wan_utilization_features_supported
        unless check_hwm_empty(data)
          ORDERED_SERIES_NAMES
        else
          STATIC_MODE_SERIES
        end
      else
        if bandwidth_static?(data)
          STATIC_MODE_SERIES
        else
          ORDERED_SERIES_NAMES
        end
      end
    end

    def bandwidth_static?(data)
      if bandwidth_type_results = find_interface(data, :bandwidth_type)
        bandwidth_test_result[:data].first[:y] == BANDWIDTH_TYPE_STATIC
      else
        false
      end
    end

    def new_wan_utilization_features_supported
      (bw_type_and_status_supported || backup_usage_supported)
    end

    def bw_type_and_status_supported 
      @device.supports_bw_type_and_status?
    end

    def backup_usage_supported
      @device.supports_usage_stats_in_backup?
    end

    def execute_bandwidth_test_result_query
      Measurement::BandwidthTestResult.range_by_interval_by_field(
        {
          mac: mac,
          range: range,
          interval: interval_from_range(range)
        }
      )
    end

    def execute_wan_utilization_query
      Measurement::WanUtilization.range_by_interval_by_field(
        {
          mac: mac,
          range: range,
          interval: interval_from_range(range)
        }
      )
    end

    def find_interface(results, interface)
      results.detect{ |result_for_inteface| result_for_inteface[:name] == interface }
    end

    def format_bandwidth_test_result(result)
      result[:data] = result[:data].map do |datum|
        case datum[:name].to_s
        when 'upload_average'
          { name: 'upload_hwm', data: datum[:data] }
        when 'download_average'
          { name: 'download_hwm', data: datum[:data] }
        end
      end
      result[:data].compact!
    end

    def format_wan_utilization_result(result)
      result[:data] = result[:data].map do |datum|
        case datum[:name].to_s
        when 'upload'
          { name: 'upload', data: datum[:data] }
        when 'upload_hwm'
          { name: 'upload_cos', data: datum[:data] }
        when 'download'
          { name: 'download', data: datum[:data] }
        when 'download_hwm'
          { name: 'download_cos', data: datum[:data] }
        end
      end
      result[:data].compact!
    end

    def interval_from_range(range)
      case range
      when '7d'
        '12h'
      when '30d'
        '1d'
      else
        '1h'
      end
    end

    def point_limit(range)
      Measurement::Base.limit_by_range(range)
    end

    def order_data(data)
      ordered_data = []
      supported_fields_for_interface = supported_fields(data)
      ORDERED_SERIES_NAMES.each do |key|
        series = find_interface(data, key)
        series[:data] = series[:data].first(point_limit(@range)) if series.present?
        ordered_data << series if series.present? && supported_fields_for_interface.include?(key)
      end
      ordered_data
    end

    def wan_utilization
      @wan_utilization ||= begin
        results_by_interface = execute_wan_utilization_query
        results_by_interface.each { |result| format_wan_utilization_result(result) }
      end
    end
  end
end
