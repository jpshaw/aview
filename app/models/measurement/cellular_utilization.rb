module Measurement
  class CellularUtilization < Measurement::Base

    # TODO: Add ActiveModel Validations (i.e. presence)
    def initialize(params)
      @mac = params[:mac].to_s.upcase
      @organization_id = params[:organization_id] || 0
      @carrier = carrier_name(params[:carrier])
      @data_plan = params[:data_plan]
      @interface = params[:interface]
      @rx = params[:rx] || 0
      @tx = params[:tx] || 0
      @time = params[:time] || Time.now
    end

    def save
      self.class.save_sample({
        tags: tags_from_attributes,
        timestamp: @time.utc.to_i,
        values: { transmitted: @tx, received: @rx }
      })
    end

    def tags_from_attributes
      tags = {}
      tags[:mac] = @mac
      tags[:organization_id] = @organization_id.to_s
      tags[:carrier] = @carrier
      tags[:data_plan] = @data_plan if @data_plan.present?
      tags[:interface] = @interface if @interface.present?
      tags
    end

    class << self
      def carriers_with_usage(mac, range)
        query({ legacy: { name: :carriers_with_usage, args: [mac, range] } })
      end

      def create(params)
        new(params).save
      end

      def counts_by_carrier(mac, range)
        query({ legacy: { name: :counts_by_carrier, args: [mac, range, measurement_name] } })
      end

      def data_in_range?(options)
        options[:range] = Measurement::Base::ALLOWED_RANGES.last[:value] unless options[:range]
        data_in_range(options).to_i > 0
      end

      def points_by_carrier_for_mac(mac, range = Measurement::Base::DEFAULT_RANGE, intervals = Measurement::Base::DEFAULT_INTERVAL, fill = '0')
        query({ legacy: { name: :points_by_carrier_for_mac, args: [mac, range, intervals, fill] } })
      end

      def utilization_totals_druid(mac, range = Measurement::Base::DEFAULT_RANGE)
        query({ legacy: { name: :utilization_totals_druid, args: [mac, range] } })
      end

      def utilization_totals(opts = {})
        query({ legacy: { name: :utilization_totals, args: [opts] } })
      end

      def daily_utilization_totals(opts = {})
        query({ legacy: { name: :daily_utilization_totals, args: [opts] } })
      end

      def utilization_totals_base_query(mac, range)
        query({ legacy: { name: :utilization_totals_base_query, args: [mac, range] } })
      end

      def interval_by_range(range)
        Measurement::Base::RANGE_HELPER[range][:interval] || Measurement::Base::DEFAULT_INTERVAL
      end

      def range_from_allowed_ranges(range)
        Measurement::Base::ALLOWED_RANGES.map{|e| e[:value]}.include?(range) ? range : Measurement::Base::DEFAULT_RANGE
      end

      def data_plan_usage(attrs)
        query({ legacy: { name: :data_plan_usage, args: [attrs] } })
      end

      private

      def data_in_range(options)
        query({ legacy: { name: :cellular_data_in_range, args: [options] } })
      end
    end

    private

    def carrier_name(original_carrier)
      carrier = Carrier.where(name: original_carrier).first_or_create
      carrier.persisted? ? carrier.display_name : 'Unknown'
    end
  end
end
