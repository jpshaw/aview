module Measurement
  class Base
    ALLOWED_RANGES ||= [
      {
        description: 'Last 24 Hours',
        value: '24h'
      },
      {
        description: 'Last 7 Days',
        value: '7d'
      },
      {
        description: 'Last 30 Days',
        value: '30d'
      }
    ].freeze
    RANGE_HELPER ||= {
      '24h' => { interval: '1h', limit: 24 },
      '7d' => { interval: '24h', limit: 7  },
      '30d' => { interval: '1d', limit: 30 }
    }.freeze
    DEFAULT_RANGE ||= '24h'.freeze
    DEFAULT_INTERVAL ||= '1h'.freeze

    class << self

      def client
        backend = Settings.services.metrics.backend
        @client ||= MetricsService::Client.new(client_opts(backend))
      end

      def client_opts(backend)
        settings = Settings.send(backend.to_sym) || {}
        settings.keys.map{|key| { key => settings[key] }}.reduce({}, :update)
        { backend: backend }.merge(settings).merge(zookeeper: Settings.zookeeper)
      end

      def reset_client
        @client = nil
        @database_name = nil
      end

      def measurement_name
        @measurement_name ||= "accns.#{Settings.services.metrics.env}.aview.events.metrics.#{to_s.split('::').last.underscore.dasherize}"
      end

      def limit_by_range(range)
        RANGE_HELPER[range][:limit]
      end

      def query(*args)
        client.try(:query, *args)
      end

      def save_sample(data, name = measurement_name)
        client.try(:save_sample, name, data)
      end
    end
  end
end
