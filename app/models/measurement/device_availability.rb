module Measurement
  class DeviceAvailability < Base
    def initialize(params)
      @timestamp = params[:timestamp] || Time.now.utc.to_i
      @dimensions = dimensions(params)
      @metrics = metrics(params)
    end

    # TODO: This should be part of the parent / mix-in
    def save
      self.class.save_sample(
        {
          timestamp: @timestamp.to_i,
          tags: @dimensions,
          values: @metrics
        }
      )
    end

    private

    def dimensions(params)
      {
        mac: params[:mac],
        organization_id: params[:organization_id],
        state: params[:state]
      }
    end

    def metrics(params)
      { count: params[:count] || 1 }
    end

    class << self
      #TODO: Refactor Measurement::Base to module and include create
      def create(params)
        new(params).save
      end

      def get(opts)
        query({ legacy: { name: :get_device_availability, args: [opts] } })
      end
    end
  end
end
