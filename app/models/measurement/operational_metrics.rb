module Measurement
  class OperationalMetrics < Base
    def initialize(params)
      @time = params[:event_time]
      @device_statuses = params[:device_statuses]
      @queues = params[:queues]
      @system = params[:system]
      @influx = params[:influx]
      @jboss = params[:jboss]
      @mysql = params[:mysql]
    end

    def save
      self.class.save_sample(
        {
          tags: nil,
          timestamp: @time,
          values: values_from_attributes
        }
      )
    end

    def values_from_attributes
      values = {}.with_indifferent_access
      @device_statuses.each{ |status| values.merge!(status) }
      @queues.each{ |queue| values.merge!(queue_to_metric(queue)) }
      @system.each{ |metric| values.merge!(metric) }
      @influx.each{ |metric| values.merge!(metric) } if @influx
      @jboss.each{ |metric| values.merge!(metric) } if @jboss
      @mysql.each{ |metric| values.merge!(metric) } if @mysql
      values
    end

    private

    def queue_to_metric(queue)
      { "#{queue[:name].split('/').last}_queue" => queue[:count] }
    end

    class << self
      def create(params)
        new(params).save
      end
    end
  end
end
