module Measurement
  class IntelliflowError < StandardError; end

  class Intelliflow < Measurement::Base
    class << self
      def data_usage_per_app(options)
        data_usage_per_app_query(options)
      end

      def data_usage_per_app_over_time(options)
        data_usage_per_app_over_time_query(options)
      end

      def data_usage_per_client(options)
        data_usage_per_client_query(options)
      end

      def data_usage_per_client_for_app(options)
        data_usage_per_client_for_app_query(options)
      end

      def total_client_usage(options)
        total_client_usage_query(options)
      end

      def total_client_usage_for_app(options)
        total_client_usage_for_app_query(options)
      end

      private

      def data_usage_per_app_query(options)
        query({ legacy: { name: :intelliflow_data_usage_per_app, args: [options] } })
      end

      def data_usage_per_app_over_time_query(options)
        query({ legacy: { name: :intelliflow_data_usage_per_app_over_time, args: [options] } })
      end

      def data_usage_per_client_query(options)
        query({ legacy: { name: :intelliflow_data_usage_per_client, args: [options] } })
      end

      def data_usage_per_client_for_app_query(options)
        query({ legacy: { name: :intelliflow_data_usage_per_client_for_app, args: [options] } })
      end

      def total_client_usage_query(options)
        query({ legacy: { name: :intelliflow_total_client_usage, args: [options] } })
      end

      def total_client_usage_for_app_query(options)
        query({ legacy: { name: :intelliflow_total_client_usage_for_app, args: [options] } })
      end
    end
  end
end
