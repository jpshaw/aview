module Measurement
  class WanUtilizationError < StandardError ; end

  class WanUtilization < Measurement::Base

    attr_reader :mac, :interface, :time, :upload_hwm, :upload,
      :download_hwm, :download, :organization_id

    def initialize(params)
      @mac          = params[:mac]
      @organization_id = params[:organization_id]
      @interface    = init_interface(params[:interface])
      @time         = params[:time] || Time.now.utc.to_i
      @upload_hwm   = params[:upload_hwm]
      @upload       = params[:upload]
      @download_hwm = params[:download_hwm]
      @download     = params[:download]
    end

    def save
      self.class.save_sample(
        {
          tags: tags_from_attributes,
          values: values_from_attributes,
          timestamp: time,
        }
      )
    end

    private

    def init_interface(interface)
      interface.to_s.upcase if interface
    end

    def tags_from_attributes
      {
        mac: mac,
        organization_id: organization_id.to_s,
        interface: interface,
      }
    end

    def values_from_attributes
      {
        upload_hwm: upload_hwm,
        upload: upload,
        download_hwm: download_hwm,
        download: download,
      }
    end

    class << self
      def create(params)
        new(params).save
      end

      def data_in_range?(options)
        raise WanUtilizationError, "#{t(:mac)} not specified" unless options[:mac]
        options[:range] = Measurement::Base::ALLOWED_RANGES.last[:value] unless options[:range]
        data_in_range(options).to_i > 0
      end

      def range_by_interval_by_field(options = {})
        raise WanUtilizationError, "#{t(:mac)} not specified" unless options[:mac]
        range_by_interface_and_interval_points(options) || []
      end

      private

      def data_in_range(options)
        query({ legacy: { name: :data_in_range, args: [options] } })
      end

      def range_by_interface_and_interval_points(options)
        query({ legacy: { name: :range_by_interface_and_interval_points, args: [options] } })
      end
    end
  end
end
