module Measurement
  class BandwidthTestResultError < StandardError ; end

  class BandwidthTestResult < Measurement::Base
    class << self
      def create(params)
        new(params).save
      end

      def range_by_interval_by_field(options = {})
        raise BandwidthTestResultError, "#{t(:mac)} not specified" unless options[:mac]
        options[:interval] = 'day' unless options[:interval].present?
        options[:range] = '24h' unless options[:range].present?
        range_by_interface_and_interval_points(options) || []
      end

      private


      def range_by_interface_and_interval_points(options)
        query({ legacy: { name: :btr_range_by_interface_and_interval_points, args: [options] } })
      end
    end

    attr_reader :mac, :interface, :upload_result, :download_result,
      :upload_hwm, :download_hwm, :time, :organization_id, :bandwidth_type, :bandwidth_test_status

    def initialize(params)
      @mac = params[:mac]
      @organization_id = params[:organization_id].to_s
      @interface = params[:interface].to_s.upcase
      @upload_result = params[:upload_result].to_i
      @download_result = params[:download_result].to_i
      @upload_hwm = params[:upload_hwm].to_i
      @download_hwm = params[:download_hwm].to_i
      @time = params[:time] || Time.now.utc.to_i
      @bandwidth_type = params[:bandwidth_type].to_i
      @bandwidth_test_status = params[:bandwidth_test_status].to_i
    end

    def save
      self.class.save_sample(
        {
          tags: tags,
          values: values,
          timestamp: time
        }
      )
    end

    private

    def tags
      {
        mac: mac,
        organization_id: organization_id,
        interface: interface
      }
    end

    def values
      {
        upload_result: upload_result,
        download_result: download_result,
        upload_hwm: upload_hwm,
        download_hwm: download_hwm,
        bandwidth_type: bandwidth_type,
        bandwidth_test_status: bandwidth_test_status
      }
    end
  end
end
