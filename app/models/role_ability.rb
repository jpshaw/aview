class RoleAbility < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :role, :ability

  belongs_to :role
  belongs_to :ability

  validates :role, :ability, presence: true
  validate :uniqueness_of_role_and_ability?

  private

  def uniqueness_of_role_and_ability?
    if RoleAbility.where(role_id: role, ability_id: ability).exists?
      self.errors.add :base, "Ability already exists for the role"
    end
  end

end
