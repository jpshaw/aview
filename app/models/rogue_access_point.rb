class RogueAccessPoint < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :ip, :mac, :ssid

  validates :device_id, numericality: { only_integer: true }
  validates :mac, presence: true, uniqueness: true
  validates :ip, presence: true
  validates :ssid, presence: true

  belongs_to :device, class_name: 'Netreach'

  def formatted_mac
    return mac.upcase if mac.include?(':')
    mac.chars.each_slice(2).map(&:join).join(':').upcase
  end

  # Parses the given syslog message into an array of RogueAccessPoint objects
  def self.from_syslog(message)
    access_points = message.strip.split('~').map do |group|
      group.split(',').inject({}) do |hash, pair|
        key, value = pair.split('=')
        hash[key.strip.downcase] = value
        hash
      end
    end

    access_points.map do |ap_hash|
      access_point = where(mac: ap_hash['mac']).first_or_create!(ap_hash)
      access_point.update_attributes(ap_hash)
      access_point
    end
  end
end
