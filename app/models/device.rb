require 'retryable_record'
require 'events'
require 'subscriptions'
require 'statusable'

# Public: Base class for all common device attributes and methods.
class Device < ActiveRecord::Base
  include DeviceIdentifiable
  include Statusable
  include Events
  include Subscriptions
  include StateMachine
  include Deployable
  include Monitorable
  include Alertable
  include Geocodable
  include DeviceAssociatable
  include ActsAsPlatform
  include Searchable

  CELLULAR_PRIMARY_INTERFACES = ['WWAN0', 'WWAN1', 'WWAN2', 'PPP0', 'PPP1']
  NET_TYPE_IPSEC = 'ipsec'.freeze

  devise :authenticatable

  self.inheritance_column = "object_type"

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :category_id, :configuration_id, :firmware,
    :host, :is_deployed, :last_heartbeat_at, :location_id, :mac, :mgmt_host,
    :model_id, :organization_id, :site_id, :hostname, :last_restarted_at,
    :probe_mask, :hw_version, :dns1, :dns2, :replaced_with_id, :object_type,
    :activated_at, :unreachable_at, :retry_count, :retried_at, :probed_at,
    :tunnels_count, :lifetime, :domain_name, :serial, :primary_wan_iface,
    :node_name, :configuration_associated_at, :configuration_checked_at,
    :data_plan_id, :firmware_rollback, :group_id, :dual_wan_enabled, :device_status,
    :comment

  alias_attribute :pppl, :host

  belongs_to :organization
  # `model` renamed to `series` to avoid conflicting with ActiveModel's model_name
  belongs_to :series,   class_name: "DeviceModel", foreign_key: :model_id
  belongs_to :category, class_name: "DeviceCategory"
  belongs_to :site
  belongs_to :data_plan
  belongs_to :group

  has_many   :certificates, dependent: :destroy
  has_many   :notification_subscriptions, class_name: "NotificationSubscription", as: :publisher, dependent: :destroy
  has_many   :devices_contacts, class_name: 'DevicesContacts'
  has_many   :contacts, through: :devices_contacts
  has_many   :networks, class_name: 'DeviceNetwork', foreign_key: :device_id, dependent: :destroy

  validates :organization_id, presence: true

  validates :firmware, length: { maximum: 50 }, allow_blank: true
  validates :host, length: { maximum: 45 }, allow_blank: true
  validates :mgmt_host, length: { maximum: 45 }, allow_blank: true
  validates :dns1, length: { maximum: 255 }, allow_blank: true
  validates :dns2, length: { maximum: 255 }, allow_blank: true
  validates :hostname, length: { maximum: 50 }, allow_blank: true
  validates :hw_version, length: { maximum: 20 }, allow_blank: true
  validates :object_type, presence: true, length: { maximum: 100 }
  validates :domain_name, { length: { maximum: 255 },
                            allow_blank: true }
  validates :primary_wan_iface, length: { maximum: 20 }
  validate :data_plan_organization
  validates :device_status, length: { minimum: 0, maximum: 99 }, allow_blank: true
  validates :comment, length: { maximum: 255 }, allow_blank: true

  delegate :name,         to: :category,      prefix: true, allow_nil: true
  delegate :name,         to: :series,        prefix: true, allow_nil: true
  delegate :name,         to: :site,          prefix: true, allow_nil: true
  delegate :name, :slug,  to: :organization,  prefix: true, allow_nil: true
  delegate :name,         to: :configuration, prefix: true, allow_nil: true
  delegate :data_used,    to: :modem,         prefix: false, allow_nil: true

  # Organization delegations
  delegate :service_name, to: :organization
  delegate :contacts, to: :organization, prefix: true, allow_nil: true

  before_save :check_site
  before_save :check_device_status
  before_save :check_configuration_association

  has_search_scope :with_type, ->(type) { where(object_type: type) }
  has_search_scope :with_categories, ->(categories) { where(category_id: categories) }
  has_search_scope :with_group,  ->(group) { where(group_id: group) }
  has_search_scope :with_models, ->(models) { where(model_id: models) }
  has_search_scope :with_site,   ->(site) { where(site_id: site) }
  has_search_scope :with_device_status, -> (device_status) { where(device_status: device_status) }
  has_search_scope :with_status, ->(status) {
    base_query =  where(devices: { is_deployed: true })
    case status.to_i
    when DeviceState::States::UP
      base_query.joins(:device_state).where(device_states: { state: DeviceState::States::UP })
    when DeviceState::States::DOWN
      base_query.joins(:device_state).where(device_states: { state: DeviceState::States::DOWN })
    when DeviceState::States::UNREACHABLE
      base_query.joins(:device_state).where(device_states: { state: [DeviceState::States::UNREACHABLE, nil] })
    end
  }
  has_search_scope :with_deployment, ->(deployment) {
    case deployment.to_i
    when Devices::Deployment::DEPLOYED
      where(devices: { is_deployed: true })
    when Devices::Deployment::UNDEPLOYED
      where(devices: { is_deployed: false })
    end
  }
  has_search_scope :with_mac, ->(mac) { where(mac: mac) }
  has_search_scope :with_mac_search, ->(search) { where("devices.mac LIKE ?", "%#{search}%") }
  has_search_scope :with_site_name_search, ->(search) { joins(:site).where("sites.name LIKE ?", "%#{search}%") }
  has_search_scope :with_firmware_search, ->(search) { where("devices.firmware LIKE ?", "%#{search}%") }
  has_search_scope :with_hostname_search, ->(search) { where("devices.hostname LIKE ?", "%#{search}%") }
  has_search_scope :with_modem, -> { joins('INNER JOIN device_modems ON devices.id = device_modems.device_id').uniq }
  has_search_scope :with_active_modem, -> { with_modem.where(device_modems: {active: true }) }
  has_search_scope :with_network, ->(network) { DeviceByNetworkTypeFinder.new(network: network).execute }
  has_search_scope :with_signal_range, ->(range) { DeviceBySignalStrengthRangeFinder.new(range: range).execute }
  has_search_scope :with_data_plan, ->(data_plan_id) { where(data_plan_id: data_plan_id) }
  has_search_scope :last_heartbeat_since, ->(seconds_ago) { where('last_heartbeat_at >= ?', seconds_ago.seconds.ago ) }
  has_search_scope :last_heartbeat_between, ->(start_date, end_date) { where(last_heartbeat_at: (start_date..end_date)) }
  has_search_scope :sort_by_device_status, -> (direction) { order("devices.is_deployed desc", "device_status #{sort_direction direction}") }
  has_search_scope :sort_by_device_states_state, ->(direction) {
    joins(:device_state)
      .order('devices.is_deployed desc')
      .order("device_states.state #{sort_direction direction}")
  }
  has_search_scope :sort_by_devices_mac, ->(direction) { order("mac #{sort_direction direction}") }
  has_search_scope :sort_by_devices_host, ->(direction) { order("host #{sort_direction direction}") }
  has_search_scope :sort_by_devices_hostname, ->(direction) { order("hostname #{sort_direction direction}") }
  has_search_scope :sort_by_device_categories_name, ->(direction) {
    joins(:category).order("device_categories.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_device_models_name, ->(direction) {
    joins(:series).order("device_models.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_organizations_name, ->(direction) {
    joins(:organization).order("organizations.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_sites_name, ->(direction) {
    joins(:site).order("sites.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_devices_firmware, ->(direction) { order("firmware #{sort_direction direction}") }
  has_search_scope :sort_by_countries_name, ->(direction) {
    joins('left outer join locations as locations_sort on locations_sort.id = devices.location_id')
      .joins('left outer join countries as countries_sort on countries_sort.id = locations_sort.country_id')
      .order("countries_sort.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_devices_last_heartbeat_at, ->(direction) { order("last_heartbeat_at #{sort_direction direction}") }
  has_search_scope :sort_by_device_modems_data_used, ->(direction) {
    joins('left outer join device_modems on device_modems.device_id = devices.id')
      .order("device_modems.data_used #{sort_direction direction}")
  }
  has_search_scope :sort_by_devices_activated_at, ->(direction) { order("activated_at #{sort_direction direction}") }

  alias :hw_model :series

  # Return the models for the current device
  def self.device_models
    DeviceModel.where(class_name: self.to_s)
  end

  def self.not_in_group(group_id)
    where(arel_table[:group_id].not_eq(group_id).or(arel_table[:group_id].eq(nil)))
  end

  # Public: The parameter representing the device.
  #
  # Returns the MAC Address of the device.
  def to_param
    mac
  end
  alias_method :name, :to_param

  def primary_interface
    if self.respond_to?(:details) && self.details.respond_to?(:active_wan_iface)
      self.details.active_wan_iface
    else
      self.primary_wan_iface
    end
  end

  def using_cellular_as_primary?
    return false unless has_modem?
    return true if is_platform?(:buildroot)
    CELLULAR_PRIMARY_INTERFACES.include?(primary_interface.to_s.upcase)
  end

  def can_send_commands?
    true
  end

  def send_command(opts = {})
    # Device subclasses should define this for their platform
    raise NotImplementedError.new("Device platform must implement `send_command`")
  end

  # Public: Enqueues a command to be run for the device. This will be
  # popped by the DeviceCommandProcessor and run on the device.
  #
  # command - The String name of the command to run for the device.
  #
  # Returns nothing.
  def queue_command(data = {})
    if queue = TorqueBox.fetch('/queues/device_commands')
      data[:device_id] = self.id
      queue.publish(data.symbolize_keys)
    end
  end

  def command_ip_addr
    if ::DeviceIpsecAddressLookupService.enabled?
      address = ::DeviceIpsecAddressLookupService.new(mac).execute
      return address if address.present?
    end

    mgmt_host.present? ? mgmt_host : host
  end

  # Public: Set the device to use an organization identified by the id given.
  # Changing a devices organization also changes the configuration to the new
  # organization default. If the organization changed, it sets the new site id as well.
  #
  # Returns nothing.
  def change_organization(new_organization_id)

    # Do nothing if id is nil, or already set.
    return if new_organization_id.nil? || (self[:organization_id] == new_organization_id)

    if new_organization = Organization.find_by(id: new_organization_id)

      # If current organization is nil, or the organization has changed, do the following
      if self.organization.nil? || (self.organization.id != new_organization.id)

        # Set the device site to the organization's default site
        self.site = new_organization.default_site

      end

      self.organization = new_organization
      self.save if self.changed?

    end

  end

  def status
    if has_state? && deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

  def ipsec_networks
    self.networks.where(net_type: NET_TYPE_IPSEC)
  end

  # Assocation routines
  #
  def tag_contact(contact)
    return if contact.nil?
    contacts << contact
  end

  def replaceable_attributes
    retval = self.attributes

    retval.delete("id")
    retval.delete("mac")
    retval.delete("created_at")
    retval.delete("updated_at")
    retval.delete("state")

    retval
  end

  def replaced?
    !self.replaced_with.nil?
  end

  def supports_ids?
    false
  end

  def supports_intelliflow?
    false
  end

  def supports_wan_test_command?
    false
  end

  def supports_wan_toggle_command?
    false
  end

  def has_connectivity_tests?
    false
  end

  # Public: Explicity declares the policy for a device model. This prevents
  # name clashing with the `model_name` method we have for the `DeviceModel`
  # association.
  def self.policy_class
    "#{model_name}Policy"
  end

  # Class methods
  class << self

    def find_id_by_mac(mac)
      where(mac: mac).limit(1).pluck(:id).first
    end

    def find_mac_by_id(id)
      where(id: id).limit(1).pluck(:mac).first
    end

    def probes_for_minute(opts = {})
      opts[:threshold] ||= 10.minutes.ago
      opts[:minute] || return
      minute_mask = 1 << (opts[:minute])

      where(is_deployed: true).joins(:device_state).includes(configuration: :snmp)
        .where("probe_mask & ? > 0", minute_mask)
        .where("(probed_at <= ? OR probed_at IS NULL)", opts[:threshold])
        .where("mgmt_host IS NOT NULL")
    end

    def probes_for_current_minute
      base_time = Time.now.utc
      probe_threshold = base_time - 10.minutes
      probes_for_minute(minute: base_time.min, threshold: probe_threshold)
    end
  end

  # Checks if the device supports a cellular modem.
  def cellular_modem_supported?
    self.respond_to?(:modem)
  end

  # Checks if the device has a modem associated with it.
  def has_modem?
    self.cellular_modem_supported? && self.modem.present?
  end

  def has_cellular_location?
    has_modem? && cellular_location.present? && cellular_location.has_valid_map_data?
  end

  def cellular_location
    @cellular_location ||= self.try(:modem).try(:cellular_location)
  end

  def has_power_outlets?
    respond_to?(:power_outlets) && power_outlets.count > 0
  end

  # Parse

  def code_from_snmp_value(value = "")
    value.match(/\d+/) do |code_match|
      "#{code_match.to_s}".to_i
    end
  end

  def configuration_class
    self.class.configuration_class
  end

  def self.configuration_class
    self.reflect_on_association(:configuration).try(:class_name).try(:constantize)
  end

  def has_legacy_configuration?
    configuration.present? && configuration_class == NetbridgeConfiguration
  end

  def has_gateway_configuration?
    configuration.present? && configuration_class == GatewayConfiguration
  end

  def is_configurable?
    !is_platform?(:gateway) && !is_platform?(:alpha)
  end

  def serializable_hash(options = nil)
    serialized_hash = super(options)
    if options[:custom]
      serialized_hash[:category] = category_name
      serialized_hash[:deployed] = is_deployed
      serialized_hash[:model] = series_name
      serialized_hash[:modem] = modem.serializable_hash if has_modem?
      serialized_hash[:serial] = serial if serial.present?
      serialized_hash[:site] = site_name
      serialized_hash[:status] = status_description
    end
    serialized_hash
  end

  protected

  # Protected: Create a bitmask of minutes for a frequency at a given offset.
  #
  # freq - interval at which to select minutes (default: 15)
  # offset - interval offset from 0 minutes (default: random);
  #   offset must be less than freq
  #
  # Examples:
  #
  #   mask_for_frequency_and_offset()
  #     #=> 576478345026355200
  #   mask_for_frequency(5, 1)
  #     #=> 297528130221121800
  #
  # Returns a bitmask of minutes.
  def mask_for_frequency_and_offset(freq = 15, offset = nil)
    occurrences = (1...60/freq)
    offset ||= Random.rand(freq)
    minutes = occurrences.reduce([offset]) do |minutes, _|
      minutes + [minutes.last + freq]
    end
    probe_mask = minutes.reduce(0) { |mask, minute| mask | 1 << minute }
  end
  alias :mask_for_frequency :mask_for_frequency_and_offset

  def set_probe_mask
    update_attribute(:probe_mask, mask_for_frequency)
  end

  def clear_probe_mask
    update_attribute(:probe_mask, 0)
  end

  # Internal: Ensures the device's site is owned by the same organization. If not,
  # it will set the site to the organization's default site. If for some reason
  # the organization is not present, it will set the site to nil.
  def check_site
    if site_id_changed? || organization_id_changed?
      if organization.present?
        unless organization.sites.find_by(id: site_id).present?
          self.site = organization.default_site
        end
      else
        self.site = nil
      end
    end
  end

  def check_device_status
    #keep status in sync w/ device status after each save
    curr_status = self.status
    if curr_status != self.device_status
      self.device_status =  curr_status
    end
  end

  def check_configuration_association
    if configuration_id_changed?
      self.configuration_associated_at = self.configuration_id.nil? ? nil : Time.now.utc
      self.configuration_checked_at = nil
    end
  end

  def data_plan_organization
    if data_plan && data_plan.organization_id != organization_id
      errors.add(:data_plan, 'must belong to the same organization as this device')
    end
  end

  def firmware_is_greater_or_equal_to(baseline_version)
    return false if firmware.blank? || baseline_version.blank?
    this_firmware_parts = firmware.split('.')
    baseline_parts = baseline_version.split('.')
    this_firmware_parts.each_with_index do |part, index|
      return true if part.to_i > baseline_parts[index].to_i
      return false if part.to_i < baseline_parts[index].to_i
    end
    return true
  end
end
