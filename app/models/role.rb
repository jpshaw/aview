class Role < ActiveRecord::Base

  belongs_to :organization

  has_many :permissions, dependent: :destroy
  has_many :role_abilities, dependent: :destroy
  has_many :abilities, through: :role_abilities

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :description, :name
  attr_protected :admin

  validates :name, presence: true, uniqueness: { case_sensitive: false, scope: :organization_id }
  validates :admin, :inclusion => { :in => [true, false] }
  validates :organization_id, :presence => true

  def admin?
    admin == true
  end

  def self.scoped_to_organization?
    true
  end

end
