class DeviceFirmware < ActiveRecord::Base

  include Comparable

  SEPARATOR = '~'.freeze

  mount_uploader :firmware_file,  FirmwareFileUploader
  mount_uploader :schema_file,    SchemaFileUploader

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :img_type, :version, :schema_version, :migration_versions, :production, :deprecated

  belongs_to :device_model

  has_many :device_configurations, foreign_key: :firmware_id

  delegate :category, to: :device_model

  validates(
    :version,
    presence: true,
    uniqueness: {
      scope:    :device_model_id,
      message:  'already exists for this device model'
    },
    format: { with: /\d+(\.\d+)+/, message: 'must be a dot (.) separated list of numeric values' }
  )

  scope :active, -> { where(production: true, deprecated: false) }
  scope :by_latest, -> { order('device_firmwares.version desc') }

  after_commit :flush_device_model_firmwares_cache

  # Returns a list of firmwares associated to devices in the given categories
  # that have been active within the last 30 days.
  def self.assigned_to_active_devices(category_ids: DeviceCategory.pluck(:id))
    # Note: doing joins(:device_configurations) is broken
    DeviceFirmware
      .joins('INNER JOIN device_configurations ON device_configurations.device_firmware_id = device_firmwares.id')
      .joins('INNER JOIN devices ON devices.configuration_id = device_configurations.id')
      .where(devices: { id: Device.active.where(category_id: category_ids) })
      .uniq
      .pluck(:version)
      .sort
  end

  def active?
    production? and not deprecated?
  end

  # Returns a firmware's compatible firmwares.
  #
  # Compatible firmwares include the current firmware and those firmwares that share the firmware's
  # model and have a schema version that:
  # A. Is the same or is newer than the firmware's, or
  # B. Have the schema version of the firmware's included in the migration versions value.
  #
  # If the passed user does not have the ability to assign non-production firmwares, in addition to
  # the above conditions only 'active' firmwares (see scope above) will be selected.
  def compatible_firmwares(user = nil)
    firmwares = all_compatible_firmwares
    firmwares = firmwares.active unless user.present? && Pundit.policy(user, ::DeviceFirmware).assign_non_production?
    firmwares << self unless firmwares.pluck(:id).include?(self.id)
    firmwares
  end

  def all_compatible_firmwares
    firmwares = self.device_model.device_firmwares.where(
      [
        "schema_version >= ? or migration_versions like ?",
        self.schema_version,
        "%#{SEPARATOR}#{self.schema_version.to_s}#{SEPARATOR}%"
      ]
    )
    firmwares << self unless firmwares.pluck(:id).include?(self.id)
    firmwares
  end

  def self_and_parents(include_beta = false)
    results = self.device_model.cached_firmwares

    results.select! { |firmware| firmware >= self }

    # Check if beta firmwares should be removed, but ensure that we don't
    # remove this firmware object if it is one.
    unless include_beta
      results.reject! { |firmware| firmware != self and not firmware.active? }
    end

    results
  end

  def <=>(other)
    if other.is_a?(self.class)
      Gem::Version.new(self.version) <=> Gem::Version.new(other.version)
    else
      super
    end
  end

  def schema
    JSON.parse(schema_file.read)
  end


  module Type
    NETREACH_IMG    = 1
    NETBRIDGE_IMG   = 2
    NETBRIDGE_IMG_2 = 3
    NETBRIDGE_IMG_3 = 4
  end

  class << self

    def netreach_img
      where(:img_type => Type::NETREACH_IMG)
    end

    def netbridge_img
      where(:img_type => Type::NETBRIDGE_IMG)
    end

    def netbridge_img_2
      where(:img_type => Type::NETBRIDGE_IMG_2)
    end

    def netbridge_img_3
      where(:img_type => Type::NETBRIDGE_IMG_3)
    end

    def netreach_img_list
      listify(netreach_img)
    end

    def netbridge_img_list
      listify(netbridge_img)
    end

    def netbridge_img_2_list
      listify(netbridge_img_2)
    end

    def netbridge_img_3_list
      listify(netbridge_img_3)
    end

    def listify(list)
      list.order("version desc").map &:version
    end

  end

  private

  def flush_device_model_firmwares_cache
    Rails.cache.delete(device_model.firmwares_cache_key) if device_model
  end

end
