require 'constants_helper'

class NotificationSubscription < ActiveRecord::Base

  # Public: The publisher types for subscriptions.
  module Types
    include ConstantsHelper
    DEVICE       = "Device"
    ORGANIZATION = "Organization"
  end

  belongs_to :user
  belongs_to :profile, :class_name => "NotificationProfile"
  belongs_to :publisher, :polymorphic => true
  has_many   :notifications

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :user_id, :profile_id, :publisher_id, :publisher_type

  validates :user_id, :presence => true, :numericality => { :only_integer => true }
  validates :profile_id, :presence => true, :numericality => { :only_integer => true }
  validates :publisher_id, :presence => true, :uniqueness => { :scope => [:publisher_type, :user_id] },
    :numericality => { :only_integer => true }
  validates :publisher_type, :presence => true

  scope :by_created_at, ->(direction = :desc) {
    order("created_at #{direction == :desc ? :desc : :asc}")
  }

  def notification_delivery_count_for_last_day
    notification_delivery_count_for_interval(:last_day)
  end

  def notification_delivery_count_for_last_week
    notification_delivery_count_for_interval(:last_week)
  end

  def notification_delivery_count_for_last_month
    notification_delivery_count_for_interval(:last_month)
  end

  # Public: Gets the publisher class.
  #
  # Returns the class of the publisher if found, otherwise nil.
  def publisher_class
    return if publisher_type.nil?
    publisher_type.constantize
  end

  # Public: Get the string name of the publisher.
  #
  # Returns a string containing the publisher's name.
  def publisher_name
    return if publisher.nil?
    publisher.name
  end

  # Public: Returns the subscription's publisher type.
  #
  # Examples
  #
  #   subscription.publisher_type = Types::ORGANIZATION
  #   subscription.type
  #   # => "Organization"
  #
  #   subscription.publisher_type = Types::DEVICE
  #   subscription.type
  #   # => "Device"
  #
  # Returns the string name of the subscription's publisher type.
  def type
    return if publisher_class.nil?
    publisher_class.to_s.split("::").last
  end

  # Public: Returns subscriptions that are available for notifications.
  #
  # Returns an activerecord relation for notifiable subscriptions.
  def self.notifiable
    joins(profile: :subscribed_events)
      .where("subscribed_events.frequency <> ?", NotificationProfile::Frequencies::NEVER)
      .uniq
  end

  # Public: Get subscriptions interested in devices.
  def self.devices
    where(:publisher_type => Types::DEVICE)
  end

  # Public: Get subscriptions interested in organizations.
  def self.organizations
    where(:publisher_type => Types::ORGANIZATION)
  end

  # Public: Get subscriptions interested in a specific device and uuid.
  #
  # device_id - The Integer ID of the device.
  # uuid_id - The Integer ID of the event UUID.
  #
  # Returns subscriptions for the device and uuid.
  def self.for_device_and_uuid(device_id, uuid_id)
    devices.for_publisher_and_uuid(device_id, uuid_id)
  end

  # Public: Get subscriptions interested in specific organizations and uuid.
  #
  # organization_id_list - An Array of Integer organization IDs.
  # uuid_id - The Integer ID of the event UUID.
  #
  # Returns subscriptions for the organizations and uuid.
  def self.for_organizations_and_uuid(organization_id_list, uuid_id)
    organizations.for_publisher_and_uuid(organization_id_list, uuid_id)
  end

  protected

  # Internal: Get subscriptions for a given publisher and uuid. It requires the
  # publisher_type to be set before calling this method.
  #
  # publisher_id - The Integer ID of the publisher object.
  # uuid_id - The Integer ID of the event UUID.
  #
  # Returns subscriptions for the publisher and uuid.
  def self.for_publisher_and_uuid(publisher_id, uuid_id)
    return [] unless all.where_values_hash.has_key?("publisher_type")

    joins(:profile => :subscribed_events)
      .where(:publisher_id => publisher_id)
      .where("subscribed_events.uuid_id = ?", uuid_id)
      .uniq
  end

  # Return the number of notifications delivered for the given interval.
  def notification_delivery_count_for_interval(interval)
    range = \
      case interval
      when :last_day
        24.hours.ago.utc..Time.now.utc
      when :last_week
        1.week.ago.utc.beginning_of_day..DateTime.now.utc.end_of_day
      when :last_month
        1.month.ago.utc.beginning_of_day..DateTime.now.utc.end_of_day
      else
        nil
      end

    if range.present?
      notifications.where(notifications: { delivered_at: range }).count
    else
      0
    end
  end
end
