class SignalStrength < ActiveRecord::Base

  private_class_method :new

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :threshold_percentage

  validates :threshold_percentage, inclusion: {in: 0..100, message: 'must be between 0 and 100'}

  before_create :enforce_singleton!

  def self.instance
    first
  end


  private

  def enforce_singleton!
    unless SignalStrength.count == 0
      errors.add :base, 'Signal strength settings already exist'
      false
    end
  end

end
