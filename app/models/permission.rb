class Permission < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :manager_id, :manager_type, :manageable_id, :manageable_type,
                  :role_id, :manager, :manageable, :role

  acts_as_tree dependent: :destroy

  # Manageables
  MANAGEABLE_TYPE_ORGANIZATION  = 'Organization'.freeze
  VALID_MANAGEABLE_TYPES        = [MANAGEABLE_TYPE_ORGANIZATION].freeze

  # Managers
  MANAGER_TYPE_ORGANIZATION     = 'Organization'.freeze
  MANAGER_TYPE_USER             = 'User'.freeze
  VALID_MANAGER_TYPES           = [MANAGER_TYPE_ORGANIZATION, MANAGER_TYPE_USER].freeze

  belongs_to :manageable, polymorphic: true
  belongs_to :manager,    polymorphic: true
  belongs_to :role

  validates :manageable, :manager, :role, presence: true
  validates :manageable_type, inclusion: {in: VALID_MANAGEABLE_TYPES, message: "is not valid"}
  validates :manager_type,    inclusion: {in: VALID_MANAGER_TYPES,    message: "is not valid"}

  validate :uniqueness_of_manageable_and_manager?,  if: :management_fields_change?
  validate :managed_by_manager_organization,        if: :is_manager_type_user?
  validate :managed_by_manager_organization,        if: :is_manageable_type_organization?

  delegate :abilities, to: :role

  scope :manageable_organizations, ->() { where(manageable_type: MANAGEABLE_TYPE_ORGANIZATION) }

  # An intersection of own and ancestors abilities.
  def allowed_abilities
    returned_abilities = abilities
    ancestors.each do |ancestor|
      returned_abilities &= ancestor.abilities
    end
    returned_abilities
  end

  def is_manager_type_user?
    self.manager_type == MANAGER_TYPE_USER
  end

  def is_manageable_type_organization?
    self.manageable_type == MANAGEABLE_TYPE_ORGANIZATION
  end

  def self.all_by_manager(manager)
    where(manager_id: manager, manager_type: manager.class.to_s)
  end

  # Public: Gets permissions for a given manager and/or manageable.
  #
  # opts - The Hash of options to search for.
  #        :manager    - The manager object to search for.
  #        :manageable - The manageable object to search for.
  #
  # Examples
  #
  #   Permission.for(manager: User.first)
  #   # => [#<Permission id: 2, manager_id: 1, manager_type: "User",
  #           manageable_id: 2, manageable_type: "Organization", role_id: 4, ...>]
  #
  # Returns an ActiveRecord Relation for the Permissions matching the given
  # manager and/or manageable.
  def self.for(opts = {})
    manager_and_or_manageable = {}

    if opts[:manager]
      manager_and_or_manageable[:manager_id]   = opts[:manager].id
      manager_and_or_manageable[:manager_type] = opts[:manager].class.to_s
    end

    if opts[:manageable]
      manager_and_or_manageable[:manageable_id]   = opts[:manageable].id
      manager_and_or_manageable[:manageable_type] = opts[:manageable].class.to_s
    end

    where(manager_and_or_manageable)
  end

  private

  def uniqueness_of_manageable_and_manager?
    if Permission.where(
      manageable_id:    self.manageable,
      manageable_type:  self.manageable.class.to_s,
      manager_id:       self.manager,
      manager_type:     self.manager.class.to_s
    ).exists?
      self.errors.add :base, "The #{self.manager.class.to_s.downcase} already manages the #{self.manageable.class.to_s.downcase} resource"
    end
  end

  # Internal: Checks if manageable is managed by the managers organization.
  def managed_by_manager_organization
    return if self.manager == self.manageable
    return unless self.manager.respond_to?(:organization)

    # Build an ActiveRecord relation for the manageable type and the manager.
    permissions = self.class.where(manageable_type: self.manageable_type)
                            .all_by_manager(manager.organization)

    # If the manageable type is an organization, we need to search up
    # its ancestor tree to find permissions for it.
    permissions = \
      if is_manageable_type_organization?
        # This will choose the nearest ancestor (or self) with the permission.
        permissions.joins(joins_organizations_hierarchy_sql)
                   .where(organization_hierarchies: { descendant_id: self.manageable })
                   .order('organization_hierarchies.generations asc')
      else
        permissions.where(manageable_id: self.manageable)
      end

    # Set the parent to the first permission, if found. If no permissions are
    # found then add an error for the validation.
    unless self.parent = permissions.first
      self.errors.add :base, "The user's organization must also manage this #{self.manageable.class.to_s.downcase}"
    end
  end

  def management_fields_change?
    self.manageable_id_changed?   ||
    self.manageable_type_changed? ||
    self.manager_id_changed?      ||
    self.manager_type_changed?
  end

  def joins_organizations_hierarchy_sql
    'INNER JOIN organization_hierarchies ON permissions.manageable_id = organization_hierarchies.ancestor_id'
  end
end
