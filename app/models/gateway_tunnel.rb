class GatewayTunnel < ActiveRecord::Base

  belongs_to :device, class_name: 'Gateway', counter_cache: :tunnels_count
  belongs_to :tunnel_server

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :auth_type, :duration, :endpoint, :endpoint_ipv6,
    :endpoint_type, :initiator, :ipsec_iface, :tunnel_mode, :probe_index,
    :agns_managed, :device_id, :connection_id, :connection_type, :started_at,
    :account, :user, :auth_protocol, :reason, :wan_conn_method, :tunnel_name,
    :tunnel_server_id

  alias_attribute :mode, :tunnel_mode
  alias_attribute :name, :tunnel_name

  validates :account, length: { maximum: 20, allow_blank: true }
  validates :agns_managed, inclusion: { in: [true, false] }, allow_nil: true
  validates :auth_protocol, numericality: { only_integer: true, allow_nil: true }
  validates :auth_type, numericality: { only_integer: true }, allow_nil: true
  validates :connection_id, length: { maximum: 50, allow_blank: true }
  validates :connection_type, numericality: { only_integer: true, allow_nil: true }
  validates :device_id, numericality: { only_integer: true, allow_nil: true }
  validates :duration, numericality: { only_integer: true }, allow_nil: true
  validates :endpoint, length: { maximum: 15, allow_blank: true }
  validates :endpoint_ipv6, length: { maximum: 50, allow_blank: true }
  validates :endpoint_type, numericality: { only_integer: true }, allow_nil: true
  validates :initiator, numericality: { only_integer: true }, allow_nil: true
  validates :ipsec_iface, length: { maximum: 10, allow_blank: true }
  validates :tunnel_mode, numericality: { only_integer: true }, allow_nil: true
  validates :reason, length: { maximum: 50, allow_blank: true }
  validates :user, length: { maximum: 20, allow_blank: true }
  validates :wan_conn_method, numericality: { only_integer: true, allow_nil: true }

  after_create :check_device_tunnels_count
  after_destroy :check_device_tunnels_count

  before_save :associate_with_tunnel_server, if: :connected_to_vig?

  scope :non_management, -> { where("tunnel_mode != ?", MANAGEMENT_MODE) }

  scope :by_interface, ->(order = :asc) {
    order("gateway_tunnels.ipsec_iface #{(order == :asc ? :asc : :desc)}")
  }

  # TODO: Replace these "IN (..)" queries with a search by tunnels direction
  # when support for getting the SNMP tunDirection is implemented.
  scope :outbound, -> { where(ipsec_iface: OUTBOUND_INTERFACES) }
  scope :inbound, -> { where(ipsec_iface: INBOUND_INTERFACES) }

  OUTBOUND_INTERFACE_RANGE = 0..5
  INBOUND_INTERFACE_RANGE = 6..31
  OUTBOUND_INTERFACES = OUTBOUND_INTERFACE_RANGE.map { |i| "ipsec#{i}" }.freeze
  INBOUND_INTERFACES = INBOUND_INTERFACE_RANGE.map { |i| "ipsec#{i}" }.freeze

  NOT_AVAILABLE = 'N/A'

  # TODO: Use GatewayTunnel::BLANK_ENDPOINTS to deduplicate the other
  # blank endpoint checks.
  BLANK_ENDPOINTS = [
    '0.0.0.0',
    '00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00'
  ].freeze

  # Endpoint Types
  ENDPOINT_TYPES = {
    0  => 'None',
    3  => 'Nortel',
    4  => 'Linux Frame SIG',
    5  => 'GIG',
    6  => 'Linux SIG',
    9  => 'VPN Gateway',
    20 => 'VIG'
  }.freeze

  ENDPOINT_VIG = ENDPOINT_TYPES.key('VIG')

  # TODO: Move the constants to the top
  scope :connected_to_vig, -> { where(endpoint_type: ENDPOINT_VIG) }

  # Tunnel Modes
  MODES = {
    0 => 'Unknown',
    1 => 'User Control',
    2 => 'User View Only',
    3 => 'Hidden',
    4 => 'Management',
    5 => 'WSS Internet'
  }.freeze

  MANAGEMENT_MODE = MODES.key('Management')

  # Tunnel Auth Types
  AUTH_TYPES = {
    0   => 'Internet Only',
    1   => 'Service Manager',
    2   => 'Customer Direct',
    3   => 'Branch Office',
    4   => 'Customer Direct SM',
    999 => 'No Tunnel Yet'
  }.freeze

  # Tunnel Auth Protocols
  AUTH_PROTOCOLS = {
    0 => 'None',
    1 => 'Customer Radius',
    2 => 'Customer Secure ID',
    3 => 'Customer Axent',
    4 => 'Customer Certificate',
    5 => 'Customer LDAP',
    6 => 'Customer Internal',
    7 => 'Branch Shared Secret',
    8 => 'AGNS Managed'
  }.freeze

  # Tunnel Initiators
  INITIATORS = {
    0 => 'Unknown',
    1 => 'User',
    2 => 'Auto Logon',
    3 => 'Persistent Logon',
    4 => 'VPN CTL'
  }.freeze

  # Tunnel Alive / Down Reason Codes
  REASON_CODES = {
    0   => 'Disconnect Button Pressed',
    226 => 'Tunnel Initiated Disconnect',
    227 => 'Missed Tunnel Heartbeats',
    229 => 'Tunnel Protocol Test Failure',
    258 => 'Eth1 Lost',
    259 => 'Eth1 Restored',
    503 => 'Power Lost',
    504 => 'Tunnel Hearbeat',
    506 => 'Device Inactive',
    507 => 'No Tunnels in Internet-only mode',
    508 => 'Tunnel Idle Timeout',
    509 => 'Dial Idle Timeout',
    510 => 'Max Session Timeout',
    553 => 'VRRP State Change',
    555 => 'Inbound Shutdown Message',
    562 => 'Inbound Lost Keepalives',
    563 => 'Inbound Idle Timeout',
    564 => 'Inbound IPSec SA delete message from client',
    565 => 'Inbound ISAKMP delete message from client',
    599 => 'No reason given'
  }.freeze

  REASON_TUNNEL_ALIVE = REASON_CODES.key('Tunnel Hearbeat')


  # Extracts the ipsec number from the interface.
  #
  # Examples
  #
  #   tunnel.ipsec_iface = 'ipsec3'
  #   tunnel.ipsec_index
  #   # => 3
  def ipsec_index
    ipsec_iface.to_s.scan(/\d+$/).first.to_i
  end

  def connected_to_vig?
    endpoint_type.to_i == ENDPOINT_VIG
  end

  def management?
    self.tunnel_mode == MANAGEMENT_MODE
  end

  def managed_by_agns
    self.agns_managed == true ? 'Yes' : 'No'
  end

  def duration_from_range(from, to)
    retval = 0
    unless from.nil? or to.nil?
      elapsed = from.to_i - to.to_i
      if elapsed > 0
        retval = elapsed * 100 # Convert to TimeTicks
      end
    end
    retval
  end

  def time_connected
    return unless self.started_at.present?

    t = Time.now.utc.to_i - self.started_at.to_i
    mm, ss = t.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)
    day = "#{dd > 0 ? "#{dd} Day#{dd > 1 ? 's' : ''}," : ""}"
    "%s %d Hrs, %d Min and %d Sec" % [day, hh, mm, ss]
  end

  def endpoint_ip_address
    [endpoint, endpoint_ipv6]
      .compact
      .reject { |e| BLANK_ENDPOINTS.include?(e) }.first
  end

  def endpoints_changed?
    endpoint_changed? || endpoint_ipv6_changed? || endpoint_type_changed?
  end

  def endpoint_name
    ENDPOINT_TYPES[self.endpoint_type] || NOT_AVAILABLE
  end

  def mode_name
    MODES[self.tunnel_mode] || NOT_AVAILABLE
  end

  def auth_type_name
    AUTH_TYPES[self.auth_type] || NOT_AVAILABLE
  end

  def auth_protocol_name
    AUTH_PROTOCOLS[self.auth_protocol] || NOT_AVAILABLE
  end

  def initiator_name
    INITIATORS[self.initiator] || NOT_AVAILABLE
  end

  def connection_type_name
    Gateway::CONNECTION_TYPES[self.connection_type] || NOT_AVAILABLE
  end

  def wan_conn_name
    Gateway::WAN_CONN_METHODS[self.wan_conn_method] || NOT_AVAILABLE
  end

  private

  def associate_with_tunnel_server
    if tunnel_server_id.nil? || endpoints_changed?
      tunnel_server_ip_address =
        TunnelServerIpAddress.find_by(ip_address: endpoint_ip_address)

      if tunnel_server_ip_address
        self.tunnel_server_id = tunnel_server_ip_address.tunnel_server_id
      end
    end
  end

  # Internal: Checks if the devices tunnel count is out of bounds. If so,
  # it will tell counter_cache to reset it for the device.
  def check_device_tunnels_count
    return if device.blank?

    count = device.tunnels_count
    if count >= Gateway::TUNNEL_LIMIT || count <= 0
      device.class.reset_counters(device.id, :tunnels)
    end
  end
end
