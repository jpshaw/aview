require 'event_types'
require 'event_levels'

class DeviceEvent < ActiveRecord::Base
  include Searchable

  belongs_to :device
  belongs_to :uuid, class_name: "DeviceEventUUID"
  has_many   :notifications, foreign_key: :event_id, dependent: :destroy

  serialize :raw_data, JSON

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :information, :level, :event_type, :raw_data,
    :uuid_id, :created_at, :severity
  alias_attribute :level, :severity

  validates :device_id,  presence: true, numericality: { only_integer: true }
  validates :level,      numericality: { only_integer: true, allow_nil: true }
  validates :event_type, numericality: { only_integer: true, allow_nil: true }
  validates :uuid_id,    numericality: { only_integer: true, allow_nil: true }

  after_commit :queue_syslog_event, on: :create, if: :sysloggable_event?

  EventTypes.each do |key, value|
    next if key == :all
    scope key.to_s.pluralize, -> { where(event_type: value) }
  end

  has_search_scope :with_mac, ->(mac) {
    joins(:device)
      .merge(Device.with_mac(mac))
  }
  has_search_scope :with_type, ->(type) { where(event_type: type) }
  has_search_scope :with_level, ->(level) { where(severity: level) }
  has_search_scope :with_device_categories, ->(categories) {
    joins(:device)
      .merge(Device.with_categories(categories))
  }
  has_search_scope :with_device_models, ->(models) {
    joins(:device)
      .merge(Device.with_models(models))
  }
  has_search_scope :with_device_organization, ->(search_hash) {
    joins(:device)
      .merge(Device.with_organization(search_hash))
  }
  has_search_scope :with_device_organization_id, ->(id) {
    joins(:device)
      .where(devices: { organization_id: id })
  }
  has_search_scope :created_since, ->(date) { where('device_events.created_at > ?', date)}

  DEFAULT_TYPE_CODE  = EventTypes::STATUS
  DEFAULT_TYPE_NAME  = "Status"

  DEFAULT_LEVEL_CODE = EventLevels::INFO
  DEFAULT_LEVEL_NAME = "Info"

  # Public: Gets the information string.
  def info
    self.information.nil? ? "" : self.information.gsub(/~/,' ')
  end

  # Public: Gets the string name of the event level.
  def lvl
    if key = ::EventLevels.constants_hash.key(self.level)
      I18n.t(key, default: DEFAULT_LEVEL_NAME)
    else
      DEFAULT_LEVEL_NAME
    end
  end

  # Public: Gets the string name of the event type.
  def type
    if key = ::EventTypes.constants_hash.key(self.event_type)
      I18n.t(key, default: DEFAULT_TYPE_NAME)
    else
      DEFAULT_TYPE_NAME
    end
  end

  def formatted_raw_data
    return '' unless self.raw_data.present?

    type = self.raw_data.keys.first
    data = self.raw_data[type]

    if data.is_a?(Hash)
      if syslog?
        return "#{type}: #{data.map { |k,v| "#{k}=#{v}" }.join('~')}"
      else
        return "#{type}: #{data.to_json}"
      end
    end

    return "#{type}: #{data}"
  end

  def syslog?
    self.device.class != Gateway
  end


  def port_number_from_info
    self.information =~ /^Serial Port (\d*):/
    $1
  end


  def sysloggable_event?
    Settings.aots.enabled? && EventUUIDS::SYSLOGGABLE.include?(self.uuid.try(:code))
  end


  # Public: Converts a symbol for the event type into the constant value (code)
  # defined for it in the Types module.
  #
  # Returns the code for the type if found, otherwise the default.
  def self.type_code_for_symbol(sym)
    types = EventTypes.constants_hash
    types.has_key?(sym) ? types[sym] : DEFAULT_TYPE_CODE
  end

  # Public: Converts a symbol for the event severity into the constant value (code)
  # defined for it in the Levels module.
  #
  # Returns the code for the level if found, otherwise the default.
  def self.level_code_for_symbol(sym)
    levels = EventLevels.constants_hash
    levels.has_key?(sym) ? levels[sym] : DEFAULT_LEVEL_CODE
  end

  private

  # TODO: Add syslog event producer and consumer
  def queue_syslog_event
    queue = TorqueBox.fetch('/queues/syslog_events')
    queue.publish(self.id) if queue
  end

end
