class DeviceReport < ActiveRecord::Base
  include Searchable

  REPORT_LIMIT = 10

  belongs_to :user

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :chart_data, :finished_at, :params, :results, :user_id

  serialize :chart_data
  serialize :params

  validates :user_id, :presence => true, :numericality => { :only_integer => true }
  validates :params, :presence => true

  after_create :remove_extras
  before_destroy :delete_report_file

  scope :unfinished, ->() { where(finished_at: nil) }

  FILE_RETRY_COUNT = 5
  FILE_RETRY_SLEEP = 1.second

  def devices
    if params[:device_mac]
      Device.with_mac(params[:device_mac])
    else
      user.devices(filter_params[:filter] || {})
    end
  end

  # Public: Set the results table.
  # The data is being manually serialized to get around adapter conflicts
  # for saving to a BLOB.
  #
  # table - The Array to save as the report results.
  #
  # Returns nothing.
  def table=(table)
    if table.nil? || !table.respond_to?(:to_yaml) || !table.is_a?(Array)
      self.results = [].to_yaml
    else
      save_results_to_file(table.to_yaml)
    end
  end

  def file_exists?
    File.exists?(results_file_name)
  end

  # Public: Get the table results array.
  #
  # Returns an array of results.
  def table
    FILE_RETRY_COUNT.times do
      if file_exists?
        break
      else
        sleep FILE_RETRY_SLEEP
      end
    end

    return deprecated_table unless file_exists?

    @table ||= YAML.load(File.read(results_file_name))
    if !@table.is_a?(Array)
      @table = []
    end
    @table
  end

  def deprecated_table
    return [] if self.results.nil?
    @table ||= YAML.load(self.results)
    if !@table.is_a?(Array)
      @table = []
    end
    @table
  end

  def pie_chart=(chart)
    self.chart_data ||= {}
    self.chart_data["pie"] = chart
  end

  def pie_chart
    if self.chart_data.nil?
      {}
    else
      self.chart_data["pie"]
    end
  end

  def trend_chart=(chart)
    self.chart_data ||= {}
    self.chart_data["trend"] = chart
  end

  def trend_chart
    if self.chart_data.nil?
      {}
    else
      self.chart_data["trend"]
    end
  end

  def csv_file_name
    "ID#{self.id}_#{report_type}_report-#{Time.now.iso8601}.csv"
  end

  def results_file_name
    "#{Settings.server.report_directory}/#{id}_#{report_type}_for_user_#{user_id}"
  end

  def is_done?
     self.finished_at.present?
  end

  def status
    if is_done?
      "Finished"
    else
      "Generating..."
    end
  end

  def organization
    organization_id = "#{filter_params[:filter][:organization_id]}".to_i
    if organization = Organization.find_by(id: organization_id)
      organization.name
    elsif params[:device_mac]
      Device.find_by(mac: params[:device_mac]).try(:organization_name) || 'N/A'
    else
      'N/A'
    end
  end

  def title
    case report_type
    when nil
      ''
    when 'vig'
      'VIG'
    else
      report_type.titleize
    end
  end

  def report_type
    self.params[:report_type]
  end

  def stats_headers
    self.params[:stats_headers]
  end

  def options_to_csv
    options = []
    options << ["Date: #{self.finished_at.to_s}"]

    params = filter_params

    # TODO: Fix scope
    organization_id = "#{params[:filter][:organization_id]}".to_i
    if organization = Organization.find_by(id: organization_id)
      options << ["Organization: #{organization.name}"]
    end

    hierarchy = "#{params[:filter][:include_hierarchy]}".to_i
    options << ["Hierarchy: #{(hierarchy == 1 ? "Yes" : "No")}"]

    # Deployment
    deployment = case "#{params[:filter][:deployment]}".to_i
    when 1
      "Deployed"
    when 2
      "Undeployed"
    else
      "All"
    end

    options << ["Deployment: #{deployment}"]

    # Ordered By
    ordered_by = case params[:sort]
    when "mac"
      t(:mac_address)
    else
      "Last Activity"
    end

    direction = case params[:direction]
    when "desc"
      "Descending"
    else
      "Ascending"
    end

    options << ["Ordered By: #{ordered_by} - #{direction}"]

    # Range
    range = case params[:filter][:range]
    when "last_week"
      "Last Week"
    when "last_month"
      "Last Month"
    else
      "Last Day"
    end

    options << ["Range: #{range}"]

    # Categories
    category_id_list = params[:filter][:categories]
    unless category_id_list.blank?
      options << ["Categories: "]
      category_id_list.each do |category_id|
        if category = DeviceCategory.find_by(id: category_id)
          options << ["    #{category.name}"]
        end
      end
    end

    # Models
    model_id_list = params[:filter][:models]
    unless model_id_list.blank?
      options << ["Models: "]
      model_id_list.each do |model_id|
        if model = DeviceModel.find_by(id: model_id)
          options << ["    #{model.name}"]
        end
      end
    end

    options
  end

  def self.remove_extras_for_user(user_id)
    user = User.find_by(id: user_id)
    return unless user.present?

    report_ids_to_keep = user.reports
                             .select('id')
                             .order('device_reports.id desc')
                             .limit(REPORT_LIMIT)
                             .map(&:id)

    user.reports.where('id not in (?)', report_ids_to_keep).destroy_all
  end

  def self.allowable_filters
    [ :for_user, :sort_by_created_at ]
  end

  protected

  def filter_params
    base = {}
    report = self.params.symbolize_keys

    if report[:categories]
      if report[:categories].is_a?(Hash)
        set_categories_legacy report
      else
        report[:categories].reject! { |c| c.empty? }
        report[:categories].map!(&:to_i)
      end
    end

    if report[:models]
      if(report[:models].is_a?(Hash))
        set_models_legacy report
      else
        report[:models].reject! { |m| m.empty? }
        report[:models].map!(&:to_i)
      end
    end

    if report[:sort]
      base[:sort] = report[:sort]
    end

    if report[:direction]
      base[:direction] = report[:direction]
    end

    base[:filter] = report
    base
  end

  def remove_extras
    if user.reports.count > REPORT_LIMIT
      self.class.remove_extras_for_user(self.user_id)
    end
  end

  def set_categories_legacy report
    report[:categories] = report[:categories].map do |key, value|
      unless value.to_i == 0
        value.to_i
      end
    end.compact
  end

  def set_models_legacy report
    report[:models] = report[:models].map do |key, value|
      unless value.to_i == 0
        value.to_i
      end
    end.compact
  end

  def save_results_to_file(results_to_save)
    File.atomic_write(results_file_name) do |f|
      f.write(results_to_save)
    end
    self.results_file = results_file_name
  end

  def delete_report_file
    File.delete(results_file_name) if File.exists?(results_file_name)
  end
end
