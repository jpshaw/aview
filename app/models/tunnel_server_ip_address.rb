require 'ipaddress'

class TunnelServerIpAddress < ActiveRecord::Base
  belongs_to :tunnel_server

  validates :ip_address,
            presence: true,
            uniqueness: true

  # Provide a common string format for tunnel server ip addresses.
  def self.formatted_string(input)
    address = IPAddress.parse(input.to_s)

    # The IPAddress gem does not have a common interface to format IPv4 and IPv6
    # addresses, hence the check below.
    address.ipv4? ? address.octets.join('.') : address.compressed
  end
end