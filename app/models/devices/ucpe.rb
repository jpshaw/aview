require 'device'

class Ucpe < Device
  include CellularModem
  include ConfigurationMonitorable

  SD_9400_MODEL = '9400-SD'.freeze  # deprecated model; variable is retained for backwards compatibility
  UA_9400_MODEL = '9400-UA'.freeze

  acts_as_platform :uc_linux

  set_default_model UA_9400_MODEL

  has_serial_range 9400010000000000...9400020000000000

  belongs_to :configuration, class_name: 'DeviceConfiguration'

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

end
