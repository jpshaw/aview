require 'device'

class RemoteManager < Device
  include CellularModem
  include ConfigurationMonitorable

  # Accelerated
  RM_5300_MODEL = '5300-RM'.freeze
  RM_5400_MODEL = '5400-RM'.freeze
  RM_5401_MODEL = '5401-RM'.freeze
  RM_5402_MODEL = '5402-RM'.freeze #deprecated

  # Digi
  CONNECT_IT_4_MODEL = 'ConnectIT4'.freeze

  acts_as_platform :uc_linux

  set_default_model RM_5400_MODEL

  has_serial_range 5300020000000000...5300030000000000, model_name: RM_5300_MODEL
  has_serial_range 5400010000000000...5400020000000000
  has_serial_range 5401010000000000...5401020000000000, model_name: RM_5401_MODEL
  has_serial_range 5402010000000000...5402020000000000, model_name: CONNECT_IT_4_MODEL

  belongs_to :configuration, class_name: 'DeviceConfiguration'

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

end
