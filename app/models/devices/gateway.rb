require 'device'
require 'gateway_details'
require 'chronic_duration'
require 'snmp_prober'
require 'gateway_portal_manager'
require 'ipaddr'

class Gateway < Device
  include ::CellularModem

  DEFAULT_STATUS_FREQ           = '15m'.freeze
  DEFAULT_GRACE_PERIOD_DURATION = '17m'.freeze

  GW_8100_MODEL = '8100'.freeze
  GW_8200_MODEL = '8200'.freeze
  GW_8300_MODEL = '8300'.freeze
  GW_U110_MODEL = 'U110'.freeze
  GW_U115_MODEL = 'U115'.freeze

  BACKUP_INTERFACE = 'ppp1'

  acts_as_platform :gateway

  set_default_model GW_8200_MODEL

  has_mac_range 0x002704200000..0x00270422AB97
  has_mac_range 0x00270422ADBC..0x002704238493
  has_mac_range 0x002704238494..0x002704438493
  has_mac_range 0x00D0CF000000..0x00D0CFFFFFFF

  has_serial_range 1614770000000000..1614779999999999
  has_serial_range 2614770000000000..2614779999999999
  has_serial_range 8300000000000000..8300019999999999, model_name: GW_8300_MODEL

  # TODO: Add the u110 model. See AV-2628
  has_serial_range 110000001291600..110009999999999, model_name: GW_U110_MODEL
  has_serial_pattern '011500[0-9]{10}', model_name: GW_U115_MODEL

  belongs_to :configuration, class_name: 'GatewayConfiguration', dependent: :destroy
  has_one :network_configuration, as: :resource, class_name: 'NetworkConfiguration', dependent: :destroy
  has_one :internet_routes_configuration, as: :resource, class_name: 'InternetRoutesConfiguration', dependent: :destroy
  has_one :cascaded_networks_configuration, as: :resource, class_name: 'CascadedNetworksConfiguration', dependent: :destroy
  has_many :tunnels, class_name: 'GatewayTunnel', foreign_key: :device_id, dependent: :destroy
  has_one :details, class_name: 'GatewayDetails', foreign_key: :device_id, dependent: :destroy
  has_and_belongs_to_many :tunnel_servers, foreign_key: :device_id
  has_many :dial_test_results, foreign_key: :device_id, dependent: :destroy

  accepts_nested_attributes_for :details, :configuration

  delegate :user, to: :configuration, allow_nil: true
  delegate :password, to: :configuration, allow_nil: true
  delegate :vrrp_state, to: :details, allow_nil: true
  delegate :group_name, to: :details, allow_nil: true
  delegate :dual_wan, to: :details, allow_nil: true
  delegate :tunnels_total_up, :tunnels_total_count, :has_tunnels_status?,
    :tunnels_status_is_ok?, to: :details, allow_nil: true
  delegate :blank?, :both_up?, :both_down?,
    :wan1_up_and_wan2_down?, :wan1_down_and_wan2_up?,
    to: :dual_wan, prefix: true, allow_nil: true

  scope :sort_by_device_tunnels_count, ->(direction) { order("tunnels_count #{sort_direction direction}") }
  scope :sort_by_active_connection_type, ->(direction) {
    includes(:details)
      .order("gateway_details.active_conn_type #{sort_direction direction}")
  }
  scope :sort_by_vrrp_state, ->(direction) {
    includes(:details)
      .order("gateway_details.vrrp_state #{sort_direction direction}")
  }

  after_deploy :set_probe_mask, :get_status
  after_undeploy :clear_probe_mask

  # Minimum firmware to support sending commands to devices.
  COMMANDS_FIRMWARE_MIN = '5.3.106'.freeze
  JSON_COMMANDS_FIRMWARE_MIN = '6.1.8'.freeze
  JSON_LEGACY_FIRMWARE_RANGE = ('5.6.200'..'5.9.999').freeze
  IDS_FIRMWARE_MIN = '6.5.0'.freeze
  INTELLIFLOW_FIRMWARE_MIN = '6.5.0'.freeze
  WAN_TOGGLE_FIRMWARE_MIN = '6.5.0'.freeze
  WAN_TEST_FIRMWARE_MIN = '6.5.2'.freeze
  FIRMWARE_ROLLBACK_FIRMWARE_MIN = '6.4'.freeze
  TUNNEL_STATS_FIRMWARE_MIN = '7.0.0'.freeze
  BACKUP_USAGE_STATS_FIRMWARE_MIN = '7.0.1'.freeze

  # Maximum number of tunnels the device can have.
  TUNNEL_LIMIT = 4

  # Maximum number of modems the device can have.
  MODEM_SLOT_LIMIT = 2

  # Default IP addresses for management IPSEC tunnels.
  DEFAULT_IPv4_HOST = "0.0.0.0"
  DEFAULT_IPv6_HOST = "::0"

  # Disconnect / Profile Not Found
  PROFILE_NOT_FOUND    = 0
  DISCONTINUED_DEVICE  = 1
  BANDWIDTH_NO_RESULTS = 2

  CONNECTION_TYPES = {
    0 => 'Broadband',
    1 => 'AGNS Dial',
    2 => 'Other ISP Dial'
  }.freeze

  # WAN Connection Methods
  WAN_CONN_METHODS = {
    0 => 'Unknown',
    1 => 'Static IP',
    2 => 'DHCP',
    3 => 'PPPoE',
    4 => 'Dial Primary',
    5 => 'ISDN Primary',
    6 => 'PPPoA',
    7 => 'Cellular Persistent',
    8 => 'Cellular Primary'
  }.freeze

  # Dial Test Fail Categories
  DIAL_PROBLEMS = {
    0 => 'Error',
    1 => 'Warning',
    2 => 'Message',
    3 => 'No Category'
  }.freeze

  DIAL_TEST_RESULT = {
    0 => 'Failure',
    1 => 'Success',
    2 => 'Not Needed'
  }

  DIAL_TEST_SOURCE = {
    0 => 'Web Interface',
    1 => 'Autotest',
    2 => 'ARMT'
  }

  # SSH Login Results
  LOGIN_RESULTS = {
    0 => 'Failed',
    1 => 'Successful'
  }.freeze

  # Commands
  FETCH_VLAN_CONFIG = 'vlanconfig'

  # Public: Checks if the gateway has a configuration_id and portal_id through its configuration
  #
  # Returns true if the device has a valid portal; otherwise false.
  def has_portal_config?
    configuration.present? && configuration.portal.present?
  end

  # Public: The number of seconds the device will have to receive a heartbeat
  # before it is considered down.
  def lifetime
    32.minutes.to_i
  end

  # Public: Checks to see if the gateway device can send commands.
  #
  # Returns true if the device can send commands; otherwise false.
  def can_send_commands?
    return false if mgmt_host.blank? || firmware.blank? || !has_portal_config?
    Versionomy.parse(firmware) >= Versionomy.parse(COMMANDS_FIRMWARE_MIN)
  rescue Versionomy::Errors::ParseError
    false
  end

  def can_receive_sms_commands?
    false
  end

  def supports_rollback_commands?
    supports_json_commands? &&
      Versionomy.parse(firmware) >= Versionomy.parse(FIRMWARE_ROLLBACK_FIRMWARE_MIN)
  rescue Versionomy::Errors::ParseError
    false
  end

  def supports_json_commands?
    return false if firmware.nil? || !has_portal_config?
    return true if JSON_LEGACY_FIRMWARE_RANGE.cover?(firmware)
    Versionomy.parse(firmware) >= Versionomy.parse(JSON_COMMANDS_FIRMWARE_MIN)
  rescue Versionomy::Errors::ParseError
    false
  end

  def connected_to_vig?
    self.tunnels.map(&:connected_to_vig?).any?
  end

  def supports_ids?
    firmware_is_greater_or_equal_to(IDS_FIRMWARE_MIN) && ids_enabled?
  end

  def ids_enabled?
    network_configuration.present? && network_configuration.ids_enabled?
  end

  def supports_intelliflow?
    firmware_is_greater_or_equal_to(INTELLIFLOW_FIRMWARE_MIN) && intelliflow_enabled?
  end

  def intelliflow_enabled?
    network_configuration.present? && network_configuration.intelliflow_enabled?
  end

  def supports_wan_test_command?
    firmware_is_greater_or_equal_to(WAN_TEST_FIRMWARE_MIN)
  end

  def supports_wan_toggle_command?
    [GW_8100_MODEL, GW_8200_MODEL].exclude?(hw_version) &&
      firmware_is_greater_or_equal_to(WAN_TOGGLE_FIRMWARE_MIN)
  end

  def has_connectivity_tests?
    self.dial_test_results.any?
  end

  def find_base_mac
    self.class.find_base_mac(mac)
  end

  # TODO: Set tunnel limit based on the number of configured tunnels.
  def tunnel_limit
    TUNNEL_LIMIT
  end

  def modem_slot_limit
    MODEM_SLOT_LIMIT
  end

  def status
    if has_state? and deployed?
      case self.device_state.state
      when Status::UP
        if dual_wan_enabled?
          if dual_wan_blank? # We don't know dual wan state
            Status::UP
          elsif dual_wan_both_up? # Green
            Status::DUAL_WAN_BOTH_UP
          elsif dual_wan_both_down? # Red
            Status::DUAL_WAN_BOTH_DOWN
          elsif dual_wan_wan1_up_and_wan2_down? # Orange
            Status::DUAL_WAN_WAN1_UP_WAN2_DOWN
          elsif dual_wan_wan1_down_and_wan2_up? # Blue
            Status::DUAL_WAN_WAN1_DOWN_WAN2_UP
          end
        elsif in_backup_mode?
          Status::BACKUP_MODE
        else
          Status::UP
        end
      when Status::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when Status::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

  # Public: Checks if a tunnel other than the management (ARMT) tunnel is down.
  # Currently, our customers are only using one non-management tunnel, so check
  # if that tunnel is down (i.e. there are no non-management tunnels).  Once we
  # are able to determine from the SMx data how many expected tunnels a device
  # should have, we'll need to account for that here by comparing the current
  # non-management tunnel count to the expected non-management tunnel count.
  #
  # Returns true if a non-management tunnel is down, otherwise false.
  def tunnel_down?
    tunnels.non_management.count.zero?
  end

  def supports_tunnel_info?
    Versionomy.parse(firmware) >= Versionomy.parse(TUNNEL_STATS_FIRMWARE_MIN)
  end

  def supports_usage_stats_in_backup?
    Versionomy.parse(firmware) >= Versionomy.parse(BACKUP_USAGE_STATS_FIRMWARE_MIN)
  end

  def supports_bw_type_and_status?
    #both of these features have minimum versions of 7.0
    supports_tunnel_info?
  end

  def tunnel_info
    return {} unless has_tunnels_status? && supports_tunnel_info?
    tunnels_configured = tunnels_total_count
    tunnels_up = tunnels_total_up
    severity_level = details.tunnels_status
    {tunnels_configured: tunnels_configured, tunnels_up: tunnels_up, severity_level: severity_level}
  end

  def in_backup_mode?
    self.try(:details).try(:backup_started_at).present?
  end

  def vrrp_master?
    self.try(:details).try(:vrrp_state) == 'Master'
  end

  def get_status
    queue_command({ command: 'status' })
  end

  def fetch_lan_status
    self.supports_json_commands? && queue_command({ command: FETCH_VLAN_CONFIG })
  end

  # Intenal: Gets the devices configuration for its web portal.
  #
  # Returns a Hash of configuration variables for the web portal.
  def portal_config
    {
      :host             => self.domain_name || self.mgmt_host,
      :admin_password   => self.configuration.portal.password,
      :support_password => self.configuration.portal.ssl_password,
      :port             => self.configuration.portal.ssl_port
    }
  end

  # Sanity check for the differences in length for Gateway DateAndTime objects,
  # which changed with the timezone fix in AV-2458.
  #
  # TODO: Remove once all systems have the latest logstash snmp input plugin.
  def self.time_from_epoch(epoch)
    case epoch.to_s.length
    when 16 # Old versions reported in microseconds
      Time.at(epoch.to_i / 1_000_000)
    when 10 # New version
      Time.at(epoch.to_i)
    else
      Time.now
    end
  end

  private

  def default_status_freq
    DEFAULT_STATUS_FREQ
  end


  def default_grace_period_duration
    DEFAULT_GRACE_PERIOD_DURATION
  end

  class << self
    def find_base_mac(mac)
      # We're searching our database for any Gateway device that has a MAC
      # address close to the specified MAC.  Once we have a small range of MACs,
      # look at each of them and find the one that fits within a known logical
      # range of MAC addresses for a VPN Gateway.
      #    (Gateway MAC) <= (syslogged MAC) < (Gateway MAC + 10)

      hex_mac = mac.hex
      ret = nil

      # converting the MAC to hex strips off leading zeroes. Since we know all
      # Gateway devices have MAC addresses that start with double zeroes, add
      #them back so we can perform the device query.
      Gateway
        .where("mac <= ?", "00#{(hex_mac + 10).to_s(16)}")
        .where("mac >= ?", "00#{(hex_mac - 10).to_s(16)}")
        .select(:id, :mac)  # id attribute is needed for find_each validation
        .find_each(batch_size: 100) do |device|
          if device.mac.hex <= hex_mac && hex_mac < (device.mac.hex + 10)
            ret = device.mac
            break
          end
      end
      return ret
    end
  end

end
