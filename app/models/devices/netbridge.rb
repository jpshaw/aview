require 'device'
require 'netbridge_details'

class Netbridge < Device
  include CellularModem
  include ConfigurationMonitorable

  SX_MODEL      = 'SX'.freeze
  FX_6200_MODEL = '6200-FX'.freeze

  acts_as_platform :buildroot

  set_default_model FX_6200_MODEL

  has_mac_range 0x002704000000..0x002704010000, model_name: SX_MODEL
  has_mac_range 0x002704010001..0x00270401FFFF

  has_serial_range 6200010000000000..6200019999999999

  belongs_to :configuration, class_name: 'NetbridgeConfiguration'
  has_many :power_outlets, foreign_key: :device_id, dependent: :destroy
  has_one :details, class_name: 'NetbridgeDetails', foreign_key: :device_id, dependent: :destroy

  delegate :usb_hub, :usb_list, :eth_macs, :client_ip, :client_mac, :eth_state,
    :tunnel_status, to: :details, allow_nil: true

  after_device_association :keep_latest_eth0_association

  # Minimum firmware to support sending commands to devices.
  COMMANDS_FIRMWARE_MIN = "2.162.51"

  # Public: Attempts to send a command to a netbridge device.
  def send_command(remote_data = {})
    AclCommand.new(self, remote_data).send!
  end

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

  private

  def keep_latest_eth0_association
    eth1_associations = self.device_associations_for_interface(:eth0)
    eth1_associations.shift
    eth1_associations.each(&:destroy)
  end
end
