require 'device'

class Cellular < Device
  include CellularModem
  include ConfigurationMonitorable

  # Accelerated
  CX_6300_MODEL = '6300-CX'.freeze
  LX_6300_MODEL = '6300-LX'.freeze
  MX_6330_MODEL = '6330-MX'.freeze
  MX_6335_MODEL = '6335-MX'.freeze
  SR_6350_MODEL = '6350-SR'.freeze
  SR_6355_MODEL = '6355-SR'.freeze
  DX_6310_MODEL = '6310-DX'.freeze

  # Digi
  WR14_MODEL = 'WR14'.freeze #deprecated
  IX14_MODEL = 'IX14'.freeze

  acts_as_platform :uc_linux

  set_default_model CX_6300_MODEL

  has_mac_range 0x002704020000..0x002704FFFFFF
  has_mac_range 0x0004F3000000..0x0004F3FFFFFF, model_name: IX14_MODEL

  has_serial_range 6300010000000000..6300019999999999
  has_serial_range 6300020000000000..6300029999999999, model_name: LX_6300_MODEL
  has_serial_range 6330010000000000..6330019999999999, model_name: MX_6330_MODEL
  has_serial_range 6335010000000000..6335019999999999, model_name: MX_6335_MODEL
  has_serial_range 6300030000000000..6300039999999999, model_name: SR_6350_MODEL
  has_serial_range 6350010000000000..6350019999999999, model_name: SR_6350_MODEL
  has_serial_range 6355010000000000..6355019999999999, model_name: SR_6355_MODEL
  has_serial_range 6310010000000000..6310019999999999, model_name: DX_6310_MODEL

  has_serial_pattern 'IX14-[0-9]{6}', model_name: IX14_MODEL

  belongs_to :configuration, class_name: 'DeviceConfiguration'

  INTELLIFLOW_FIRMWARE_MIN = '17.7.122'

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

  def supports_intelliflow?
    return firmware_is_greater_or_equal_to(INTELLIFLOW_FIRMWARE_MIN)
  end

end
