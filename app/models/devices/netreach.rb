require 'device'

class Netreach < Device

  # Minimum firmware to support sending commands to devices.
  COMMANDS_FIRMWARE_MIN = "1.552.41".freeze

  DEFAULT_STATUS_FREQ           = '10m'.freeze
  DEFAULT_GRACE_PERIOD_DURATION = '54m'.freeze

  NX_4200_MODEL = '4200-NX'.freeze

  acts_as_platform :alpha

  set_default_model NX_4200_MODEL

  has_mac_range 0x00C0CA000000..0x00C0CAFFFFFF

  belongs_to :configuration, class_name: 'NetreachConfiguration', dependent: :destroy
  has_many :rogue_access_points, foreign_key: 'device_id', dependent: :destroy

  after_device_association :keep_latest_wan_association

  # Public: The number of seconds the device will have to receive a heartbeat
  # before it is considered down.
  def lifetime
    64.minutes.to_i
  end

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

  def can_send_commands?
    false
  end

  private

  def default_status_freq
    DEFAULT_STATUS_FREQ
  end


  def default_grace_period_duration
    DEFAULT_GRACE_PERIOD_DURATION
  end

  def keep_latest_wan_association
    wan_associations = self.device_wan_associations
    wan_associations.shift
    wan_associations.each(&:destroy)
  end

end
