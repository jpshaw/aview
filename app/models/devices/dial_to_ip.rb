require 'device'

class DialToIp < Device
  include CellularModem
  include ConfigurationMonitorable

  DC_5300_MODEL = '5300-DC'.freeze
  DC_5301_MODEL = '5301-DC'.freeze

  CELLULAR_MODELS = [DC_5300_MODEL].freeze

  acts_as_platform :uc_linux

  set_default_model DC_5301_MODEL

  has_serial_range 5300010000000000...5300020000000000, model_name: DC_5300_MODEL
  has_serial_range 5300030000000000...5300040000000000, model_name: DC_5300_MODEL
  has_serial_range 5301010000000000...5301020000000000

  belongs_to :configuration, class_name: 'DeviceConfiguration'

  def cellular_modem_supported?
    CELLULAR_MODELS.include?(self.hw_version)
  end

end
