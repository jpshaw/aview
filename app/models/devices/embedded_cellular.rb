require 'device'

class EmbeddedCellular < Device
  include CellularModem
  include ConfigurationMonitorable

  CX_6300_MODEL = '6300-CX'.freeze

  acts_as_platform :uc_linux

  set_default_model CX_6300_MODEL

  # TODO: remove this model altogether.  It has been replaced by the Cellular model.
  #has_serial_range 6300010000000000...6300020000000000

  belongs_to :configuration, class_name: 'DeviceConfiguration'

  def state
    if has_state? and deployed?
      case self.device_state.state
      when DeviceState::States::UP
        Status::UP
      when DeviceState::States::UNREACHABLE
        if self.last_heartbeat_at.nil?
          Status::INACTIVE
        else
          Status::NOTICE
        end
      when DeviceState::States::DOWN
        Status::DOWN
      else
        Status::INACTIVE
      end
    else
      Status::UNDEPLOYED
    end
  end

end
