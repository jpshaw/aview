class Site < ActiveRecord::Base
  include Searchable

  belongs_to :organization

  has_many :devices

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :organization_id

  validates :name, :presence => true,
    :length => { :minimum => 1, :maximum => 80, :allow_blank => false },
    :uniqueness => { :scope => :organization_id, :case_sensitive => false }

  validates :organization_id, :presence => true, :numericality => { :only_integer => true }

  before_destroy :reset_devices_sites

  DEFAULT_NAME = "Default"

  after_initialize :default_values

  scope :with_device_counts, -> { select('sites.*, COUNT(devices.id) as device_count')
                                    .joins('LEFT OUTER JOIN devices ON devices.site_id = sites.id')
                                    .group('sites.id') }
  has_search_scope :with_names, ->(names) { where(name: names) }
  has_search_scope :sort_by_name, ->(direction) { order("name #{sort_direction direction}") }
  has_search_scope :with_name_search, ->(search) { where('sites.name LIKE ?', "%#{search}%") }
  has_search_scope :sort_by_device_count, ->(direction) { order("device_count #{sort_direction direction}") }

  # Public: Checks if the site is a default.
  #
  # Returns true if the site is a default, otherwise false.
  def is_default?
    name.casecmp(DEFAULT_NAME) == 0
  end

  # Public: Gets the policy class for sites.
  #
  # Returns the policy class.
  def self.policy_class
    SitePolicy
  end

  # Public: Gets a site for a name.
  #
  # name - The String name of the site to look up.
  #
  # Returns the site if the name is valid, otherwise nil.
  def self.for_name(name)
    return if name.blank?
    where(:name => sanitize(name)).first_or_create
  end

  # Public: Gets the default site, usually through an organization scope.
  #
  # Returns the default site for the scope.
  def self.default
    where(:name => DEFAULT_NAME).first_or_create
  end

  private

  # Internal: Sets the site's devices to their organizatin's default site.
  #
  # Returns nothing.
  def reset_devices_sites
    return if organization.blank? # Check if the organization has been removed
    devices.update_all(site_id: organization.default_site)
  end

  # Internal: Sets default values for a Site object.
  #
  # Returns nothing.
  def default_values
    self.name ||= DEFAULT_NAME
  end

  # Internal: Sanitizes the site name.
  #
  # name - The String name of the site to sanitize.
  #
  # Returns the sanitized site name.
  def self.sanitize(name)
    "#{name}".strip.squeeze(" ")
  end
end
