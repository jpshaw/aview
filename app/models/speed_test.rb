class SpeedTest < ActiveRecord::Base

  SPEED_TEST_HISTORY_COUNT = 5

  belongs_to :device_modem

  attr_accessible :device_modem_id, :rx_avg, :tx_avg, :rx_latency, :tx_latency, :throughput_units, :latency_units

  after_save :remove_old_speed_tests

  def formatted_results
    "UP #{tx_avg}#{throughput_units} / " \
      "DOWN #{rx_avg}#{throughput_units} / " \
      "latency #{latency_average}#{latency_units} / #{created_at}"
  end

  def latency_average
    (( tx_latency.to_i + rx_latency.to_i ) / 2).round(2)
  end

  private

  def remove_old_speed_tests
    return unless device_modem.speed_tests.any?
    device_modem.speed_tests.order('id desc').offset(SPEED_TEST_HISTORY_COUNT).destroy_all
  end
end
