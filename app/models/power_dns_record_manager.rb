module PowerDnsRecordManager

  DOMAIN_TYPES = [
    'NATIVE',
    'MASTER',
    'SLAVE',
    'SUPERSLAVE'
  ].freeze

  # Configuration defaults
  @config = {
              :adapter  => nil,
              :encoding => nil,
              :database => nil,
              :user     => nil,
              :pass     => nil,
              :host     => nil,
              :port     => nil
            }

  @record_defaults = {
                       :domain_id => 1,
                       :name      => nil,
                       :content   => nil,
                       :type      => 'A',
                       :ttl       => '240',
                       :prio      => nil
                     }

  # Configure through hash
  def self.settings(set, opts = {})
    opts.each { |k,v| set[k.to_sym] = v if set.keys.include? k.to_sym }
  end

  def self.configure(opts = {})
    settings @config, opts
  end

  def self.set_defaults(opts = {})
    settings @record_defaults, opts
  end

  def self.config
    @config
  end

  def self.record_defaults
    @record_defaults
  end

  class PowerDnsDb < ActiveRecord::Base
    self.abstract_class = true
  end

  class Record < PowerDnsDb

    NS_RECORD_COUNT = 5
    SOA_TYPE = 'SOA'
    NS_TYPE  = 'NS'

    self.inheritance_column = :_type_disabled
    self.table_name = "dns_records"

    belongs_to :domain, class_name: 'PowerDnsRecordManager::Domain'

    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :name, :content, :type, :ttl, :domain_id, :prio,
      :change_date, :record_type

    validates :name, uniqueness: { scope: :type, unless: lambda { |r| r.type == 'NS' } }
    validates :domain_id, :name, :type, :ttl, presence: true

    after_initialize :init

    scope :soa, -> { where(type: SOA_TYPE) }
    scope :ns,  -> { where(type: NS_TYPE) }

    def init
      self.ttl       ||= 30 # seconds
      self.domain_id ||= 1
    end

  end

  class Domain < PowerDnsDb
    self.inheritance_column = :_type_disabled
    self.table_name = "domains"
    has_many :records, dependent: :destroy

    # TODO: remove `attr_accessible` and use strong params
    attr_accessible :name, :type

    validates :name, presence: true, uniqueness: { case_sensitive: false }
  end
end
