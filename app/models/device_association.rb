# Represents a connection between two devices.
#
# At least one device (source) must exist in the devices table, and the other
# (target) can exist, or just provide its mac address. Since the Device model
# does not currently support "Unknown" devices, we opt to build a self-referential,
# bi-directional, one-to-many relationship between source and target devices.
#
# Information about the connection is stored in the association details hash,
# such as ip address and interface, and is prefixed for each device by
# `source_` and `target_`. To avoid duplication, we enforce that only
# one association record can exist between the source and target devices in
# *any* direciton. Due to this constraint we must look at both directions to
# find associations for a device.
#
# A device can be the target of an association if another device creates one
# between them first. Any details reported about it by a device will be
# stored with respect to the device's direction. If a target device indicates
# that the association exists on its "eth2" interface, the data key will be
# `target_interface` in the details hash.
#
class DeviceAssociation < ActiveRecord::Base

  belongs_to :source_device, class_name: 'Device', foreign_key: 'source_id'
  belongs_to :target_device, class_name: 'Device', foreign_key: 'target_id'

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :source_id, :source_mac, :target_id, :target_mac, :details

  store :details, accessors: [
    :source_interface, :source_type, :target_interface, :target_type,
    :ip_address, :lease_type
  ], coder: JSON

  # TODO: Remove old fields after all instances are migrated
  serialize :meta, HashWithIndifferentAccess

  validates_presence_of :source_id,  if: 'source_mac.blank?'
  validates_presence_of :source_mac, if: 'source_id.blank?'
  validates_presence_of :target_id,  if: 'target_mac.blank?'
  validates_presence_of :target_mac, if: 'target_id.blank?'

  validates_uniqueness_of :source_id, scope: :target_id,
    if: ->(association) { association.source_id.present? &&
                          association.target_id.present? }

  validates_uniqueness_of :target_id, scope: :source_id,
    if: ->(association) { association.target_id.present? &&
                          association.source_id.present? }

  validates_uniqueness_of :source_mac, scope: :target_id, case_sensitive: false,
    if: ->(association) { association.source_mac.present? &&
                          association.target_id.present? }

  validates_uniqueness_of :target_mac, scope: :source_id, case_sensitive: false,
    if: ->(association) { association.target_mac.present? &&
                          association.source_id.present? }

  validate :cannot_associate_with_self
  validate :inverse_uniqueness

  before_validation :set_missing_device_attributes

  scope :by_updated_at, ->(direction = :desc) {
    order("#{self.table_name}.updated_at #{direction == :desc ? :desc : :asc}")
  }

  DIRECTIONS = [:source, :target].freeze
  DIRECTION_DETAILS = [:interface, :type].freeze

  #
  # Instance methods
  #

  # Check if the given device, id or mac is inversed for the association.
  def is_inversed_for?(object)
    if object.is_a?(Device)
      target_id == object.id
    elsif object.is_a?(Numeric)
      target_id == object
    elsif object.is_a?(String)
      target_mac == object
    else
      raise ArgumentError.new('Object must be a device, id or mac')
    end
  end

  #
  # Class methods
  #

  class << self

    def by_device_id(device_id)
      table = arel_table

      outbound = table[:source_id].eq(device_id)
      inbound  = table[:target_id].eq(device_id)

      where(outbound.or(inbound))
    end

    def by_device_mac(device_mac)
      table = arel_table

      outbound = table[:source_mac].eq(device_mac)
      inbound  = table[:target_mac].eq(device_mac)

      where(outbound.or(inbound))
    end

    def by_source_id_and_target_id(source_id, target_id)
      table = arel_table

      outbound = table[:source_id].eq(source_id).and(table[:target_id].eq(target_id))
      inbound  = table[:target_id].eq(source_id).and(table[:source_id].eq(target_id))

      where(outbound.or(inbound))
    end

    def by_source_id_and_target_mac(device_id, device_mac)
      table = arel_table

      outbound = table[:source_id].eq(device_id).and(table[:target_mac].eq(device_mac))
      inbound  = table[:source_mac].eq(device_mac).and(table[:target_id].eq(device_id))

      where(outbound.or(inbound))
    end

    def by_source_mac_and_target_mac(source_mac, target_mac)
      table = arel_table

      outbound = table[:source_mac].eq(source_mac).and(table[:target_mac].eq(target_mac))
      inbound  = table[:source_mac].eq(target_mac).and(table[:target_mac].eq(source_mac))

      where(outbound.or(inbound))
    end

    # Find an association for a source and target.
    #
    # An association can exist between a source and target in four ways:
    #
    #   1) source_id <-> target_id
    #   3) source_id -> target_mac
    #   4) source_mac <- target_id
    #   5) source_mac <-> target_mac
    #
    def find_by_source_and_target(attributes)
      source_id  = attributes[:source_id].presence
      source_mac = attributes[:source_mac].presence
      target_id  = attributes[:target_id].presence
      target_mac = attributes[:target_mac].presence

      # 1) Check for a source device and target device that both exist
      if source_id && target_id
        by_source_id_and_target_id(source_id, target_id).take

      # 2) Check for a source device that exists but the target device might not
      elsif source_id && target_mac
        by_source_id_and_target_mac(source_id, target_mac).take

      # 3) # Check for a target device that exists but the source device might not
      elsif source_mac && target_id
        by_source_id_and_target_mac(target_id, source_mac).take

      # 4) Check for a source device and target device that both might not exist
      elsif source_mac && target_mac
        by_source_mac_and_target_mac(source_mac, target_mac).take
      end
    end

    def find_or_initialize_by(attributes)
      find_by_source_and_target(attributes) || new(attributes)
    end

    def find_or_create_by(attributes)
      find_by_source_and_target(attributes) || create(attributes)
    end
  end

  private

  def set_missing_device_attributes
    self.source_id  ||= Device.find_id_by_mac(source_mac) if source_mac
    self.source_mac ||= Device.find_mac_by_id(source_id)  if source_id
    self.target_id  ||= Device.find_id_by_mac(target_mac) if target_mac
    self.target_mac ||= Device.find_mac_by_id(target_id)  if target_id
  end

  def source_and_target_are_the_same?
    if source_id && target_id
      source_id == target_id
    elsif source_mac && target_mac
      source_mac.upcase == target_mac.upcase
    else
      false
    end
  end

  def cannot_associate_with_self
    if source_and_target_are_the_same?
      errors.add(:base, "A device cannot associate with itself")
    end
  end

  # Used to validate if an inverse already exists to prevent duplicates
  def has_inverse?
    if inverse = inverse_attributes.presence
      self.class.exists?(inverse)
    else
      false
    end
  end

  def inverse_attributes
    if source_id && target_id
      { source_id: target_id, target_id: source_id }
    elsif source_id && target_mac
      { source_mac: target_mac, target_id: source_id }
    elsif source_mac && target_id
      { source_id: target_id, target_mac: source_mac }
    elsif source_mac && target_mac
      { source_mac: target_mac, target_mac: source_mac }
    else
      {}
    end
  end

  def inverse_uniqueness
    errors.add(:base, "Association already exists") if has_inverse?
  end
end
