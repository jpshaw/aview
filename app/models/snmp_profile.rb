class SnmpProfile < ActiveRecord::Base

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :community, :version, :username, :auth_password, :priv_password, :auth_protocol, :priv_protocol, :security_level

  validates :community, :length => { :maximum => 50, :allow_blank => true }
  validates :version, :length => { :maximum => 2, :allow_blank => true }
  validates :username, :length => { :maximum => 50, :allow_blank => true }
  validates :auth_password, :length => { :maximum => 50, :allow_blank => true }
  validates :priv_password, :length => { :maximum => 50, :allow_blank => true }
  validates :auth_protocol, :length => { :maximum => 3, :allow_blank => true }
  validates :priv_protocol, :length => { :maximum => 3, :allow_blank => true }
  validates :security_level, :numericality => { :only_integer => true, :allow_nil => true }
end
