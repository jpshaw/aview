# TODO: This class can probably be removed through refactoring
class Intelliflow
  DEFAULT_SAMPLE_SIZE = 10.freeze
  APPLICATION_NAME_FIELD = 'ipfix.site'.freeze
  CLIENT_NAME_FIELD = 'ipfix.client'.freeze
  DEVICE_MAC_FIELD = 'ipfix.observationDomainName'.freeze
  TIMESTAMP_FIELD = '@timestamp'.freeze
  TOTAL_BYTES_FIELD = 'ipfix.octetDeltaCount'.freeze
  ALLOWED_RANGES = [
    {
      description: 'Last 24 Hours',
      value: '24h'
    },
    {
      description: 'Last 7 Days',
      value: '7d'
    }
  ].freeze
  DEFAULT_RANGE = '24h'.freeze

  def from_date_time(range)
    case range
      when '24h'
        1.day.ago
      when '7d'
        7.days.ago
      when '30d'
        30.days.ago
      else
        1.hour.ago
    end
  end

  def interval(range)
    if range == '24h'
      'hour'
    else
      'day'
    end
  end

  def data_usage_per_app(args)
    Measurement::Intelliflow.data_usage_per_app(args)
  end

  def data_usage_per_app_over_time(args)
    Measurement::Intelliflow.data_usage_per_app_over_time(args)
  end

  def data_usage_per_client(args)
    Measurement::Intelliflow.data_usage_per_client(args)
  end

  def data_usage_per_client_for_app(args)
    Measurement::Intelliflow.data_usage_per_client_for_app(args)
  end

  def total_client_usage(args)
    Measurement::Intelliflow.total_client_usage(args)
  end

  def total_client_usage_for_app(args)
    Measurement::Intelliflow.total_client_usage_for_app(args)
  end
end
