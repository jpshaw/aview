class AdminSettings < RailsSettings::CachedSettings

  THRESHOLD_PERCENTAGE_VALID_RANGE      = (0..100).freeze
  THRESHOLD_PERCENTAGE_CHARACTER_RANGE  = ('0'..'100').freeze

  SUPPORT_CONTACT_LIST_EMAIL_REGEX     = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :var


  def self.valid?
    @errors = []
    valid_threshold_percentage? && valid_support_contact_list?
  end

  def self.valid_support_contact_list?
    emails = "#{AdminSettings.support_contact_list}".split(",")
    return true if emails.length == 0

    valid_emails = emails.select { |email| email =~ SUPPORT_CONTACT_LIST_EMAIL_REGEX }

    if emails.length == valid_emails.length
      return true
    end

    errors << 'One or more of the submitted emails is invalid'

    AdminSettings.support_contact_list = ''

    false
  end

  def self.valid_threshold_percentage?
    return true if THRESHOLD_PERCENTAGE_VALID_RANGE.include? AdminSettings.threshold_percentage

    if THRESHOLD_PERCENTAGE_CHARACTER_RANGE.include? AdminSettings.threshold_percentage
      AdminSettings.threshold_percentage = AdminSettings.threshold_percentage.to_i
      return true
    end

    errors << 'Threshold percentage must be between 0 and 100'
    false
  end


  def self.errors
    @errors = [] unless @errors
    @errors.uniq!
    @errors
  end

end
