class PowerOutlet < ActiveRecord::Base
  extend Enumerize

  belongs_to :device

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :port, :the_state

  enumerize :the_state, in: { off: 0, on: 1 }, predicates: true, default: :off

  validates :device_id, presence: true

  def turn_on!
    update_attribute(:the_state, :on) unless on?
  end
  alias_method :turn_on, :turn_on!

  def turn_off!
    update_attribute(:the_state, :off) unless off?
  end
  alias_method :turn_off, :turn_off!
end
