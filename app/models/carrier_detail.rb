class CarrierDetail < ActiveRecord::Base
  has_many :carrier
  validates :display_name, presence: true, uniqueness: true

  attr_accessible :color, :display_name, :regexp, :tail

  def regexp
    regexp_str = read_attribute(:regexp)
    regexp_str ? Regexp.new(regexp_str, Regexp::EXTENDED | Regexp::IGNORECASE) : nil
  end
end
