class GatewayDetails < ActiveRecord::Base

  belongs_to :device, :class_name => "Gateway"

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :active_wan_iface, :active_conn_type,
    :wan_1_conn_type, :wan_1_circuit_type, :wan_1_cell_extender, :wan_2_cell_extender,
    :dial_mode, :dial_backup_enabled, :dial_backup_type, :dial_modem_type, :last_trap_at,
    :time_in_dial, :dial_started_at, :configuration_name, :backup_started_at, :vrrp_state,
    :wan_ipv4_state, :wan_ipv6_state, :tunnels_total_count, :tunnels_total_up,
    :tunnels_status

  alias_attribute :group_name, :configuration_name

  validates :device_id, :presence => true, :numericality => { :only_integer => true }
  validates :active_wan_iface, :length => { :maximum => 20 }
  validates :wan_1_conn_type, :numericality => { :only_integer => true, :allow_nil => true }
  validates :wan_1_cell_extender, :length => { :maximum => 2 }
  validates :wan_2_cell_extender, :length => { :maximum => 2 }
  validates :dial_mode, :length => { :maximum => 1 }
  validates :dial_backup_enabled, :inclusion => {:in => [true, false], :allow_nil => true }
  validates :dial_backup_type, :length => { :maximum => 1 }
  validates :dial_modem_type, :length => { :maximum => 1 }
  validates :time_in_dial, :numericality => { :only_integer => true, :allow_nil => true }

  before_save :reset_wan_data, if: :active_wan_iface_changed?
  before_save :backup_mode_event, if: :backup_started_at_changed?

  # Public: Returns human readable version of the primary connection method.
  #
  # wanConfigUnknown(0),
  # wanConfigStatic (1),
  # wanConfigDHCP (2),
  # wanConfigPPPoE (3),
  # wanConfigDialPri(4),
  # wanConfigISDNPri(5),
  # wanConfigPPPoA (6),
  # wanConfig3GPer (7),
  #  # => A persistent 'always on' cellular connection
  # wanConfig3GPri (8)
  #  # => Dial as primary connection, users have the ability to bring it down if they want
  # wanConfigStatic2      (9),  //secondary WAN addr is static
  # wanConfigDHCP2        (10), //secondary WAN addr via DHCP
  # wanConfigPPPoE2       (11), //secondary WAN addr via pppoe
  # wanConfigIPv6Static   (12),
  # wanConfigIPv6DHCP     (13),
  # wanConfigIPv6Stateless(14), //radvd
  # wanConfigOff          (15) turn off IPv6
  def primary_connection_type_name
    case self[:active_conn_type]
    when 1, "1"
      "Static"
    when 2, "2"
      "DHCP"
    when 3, "3"
      "PPPoE"
    when 4, "4"
      "Dial"
    when 5, "5"
      "ISDN"
    when 6, "6"
      "PPPoA"
    when 7, "7"
      "Cellular Persistent"
    when 8, "8"
      "Cellular Primary"
    when 9, "9"
      "Secondary WAN Static"
    when 10, "10", "A"
      "Secondary WAN DHCP"
    when 11, "11", "B"
      "Secondary WAN PPPoE"
    when 14, "14", "E"
      "Router Advertisement"
    when 15, "15", "F"
      "IPv6 Disabled"
    else
      "Unknown"
    end
  end

  # Public: Gets the cellular extender name for a given interface.
  #
  # interface - The String or Symbol interface name.
  #
  # Returns a String containing the interface's cellular extender name, if the
  #   interface field exists; otherwise nil.
  def cell_extender_name(interface)
    field = "#{interface}_cell_extender"
    return unless respond_to?(field)
    case send(field)
    when 'N'
      'Extender not in use'
    when '1'
      '6200-FX'
    when '2'
      '6300-CX/LX'
    else
      'N/A'
    end
  end

  ## Dual WAN

  def dual_wan
    @dual_wan ||= ::GatewayDualWANService.new(wan_states)
  end

  def wan_states
    [wan_ipv4_state, wan_ipv6_state]
  end

  # An integer value from zero to ten that indicates the importance of the total
  # number of tunnels up.
  #
  #   0  - There are no problems.
  #   10 - There is a severe issue. Possible example: the only persistent tunnel
  #        is down and is getting back a password error in the tunnel
  #        authentication.
  def tunnels_status_is_ok?
    has_tunnels_status? && tunnels_status.zero?
  end

  def has_tunnels_status?
    tunnels_status.present?
  end

  private

  def reset_wan_data
    self.backup_started_at = nil
  end

  def backup_mode_event
    from = backup_started_at_was
    to   = backup_started_at

    # Device has entered backup mode
    if from.nil?
      device.event info: 'Device backup mode detected', level: :notice

    # Device has exited backup mode
    elsif to.nil?
      device.event info: 'Device is no longer in backup mode', level: :notice
    end
  end
end
