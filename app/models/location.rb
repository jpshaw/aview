class Location < ActiveRecord::Base

  acts_as_geolocation

  has_many   :devices
  belongs_to :country
  belongs_to :organization

  before_validation :ensure_fields_are_truncated

  validates :postal_code, length: { maximum: 20 }, allow_blank: true

  # TODO: Use something like the `snail` gem for this

  def city_line
    "#{city} #{region}  #{postal_code}".strip
  end

  # TODO: Deprecate this method
  def ctry_code
    country_code || ''
  end

  private

  def ensure_fields_are_truncated
    self.postal_code = postal_code.truncate(20, omission: '') if postal_code.present?
  end
end
