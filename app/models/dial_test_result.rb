class DialTestResult < ActiveRecord::Base
  MAX_SAVED_PER_DEVICE = 10

  validates :device_id, presence: true, numericality: { only_integer: true }

  after_save :delete_excess

  private

  def delete_excess
    last_n_results = DialTestResult.where(device_id: self.device_id).order(id: :desc).limit(MAX_SAVED_PER_DEVICE)
    DialTestResult.where(device_id: self.device_id).destroy_all(['id NOT IN (?)', last_n_results.collect(&:id)])
  end
end
