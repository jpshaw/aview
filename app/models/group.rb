class Group < ActiveRecord::Base
  include Searchable

  belongs_to :organization
  belongs_to :device_model
  belongs_to :firmware, class_name: 'DeviceFirmware'
  has_many :devices, dependent: :nullify
  has_one :category, through: :device_model

  validates :name, presence: true, length: { minimum: 3, maximum: 255 },
            uniqueness: { scope: :organization_id, case_sensitive: false }
  validates :organization_id, presence: true
  validates :device_model_id, presence: true
  validates :firmware_id, presence: true

  has_search_scope :with_name_search, ->(search) { where('groups.name LIKE ?', "%#{search}%") }
  has_search_scope :with_categories, ->(categories) { joins(:device_model).where(device_models: { category_id: categories }) }
  has_search_scope :with_models, ->(models) { where(device_model_id: models) }
  has_search_scope :with_firmware_search, ->(search) {
    joins(:firmware).where("device_firmwares.version LIKE ?", "%#{search}%")
  }
  has_search_scope :sort_by_name, ->(direction) { order("name #{sort_direction direction}") }
  has_search_scope :sort_by_device_categories_name, ->(direction) {
    joins(:category).order("device_categories.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_device_models_name, ->(direction) {
    joins(:device_model).order("device_models.name #{sort_direction direction}")
  }
  has_search_scope :sort_by_firmware_name, ->(direction) {
    joins(:firmware).order("device_firmwares.version #{sort_direction direction}")
  }

  delegate :category_name, to: :device_model, prefix: false, allow_nil: true
  delegate :name,          to: :device_model, prefix: true, allow_nil: true
  delegate :device_firmwares, to: :device_model

  def title
    "#{category_name} #{device_model_name} #{name}"
  end

end
