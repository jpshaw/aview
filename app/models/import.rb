require 'smx_importer'
require 'device_importer'
require 'configuration_importer'
require 'organization_importer'
require 'imports/drivers/smx_cellular_importer'

# Public: Runs individual imports and tracks their information, including
# the type, what triggered it, the source, when it finished, and what objects
# were created or skipped from the import file.
#
# Note: This currently supports only one import type, smx. The plan is to add
# more types in the future, allowing for multiple import sources and types.
# Support for multiple data files is mostly complete for this model - the
# biggest updates would be to the import controller and views. We should also
# look at making errors like SmxParser::InvalidFileError available to all
# importer classes, instead of just SmxParser.
#
# Only one import for each type can be run at a time. For instance, if an
# import for smx is running, another import for smx must wait until the first
# one is finished.
#
# Examples
#
#    ##########################################################################
#    ### Single data file from settings
#
#    # Get the import data file info from settings
#    import_file = Import.files.first
#    # => #<Config::Options name="smx_netgate_list", type="smx", location="~/smx_netgate_list.tab", action="append">
#
#    # Create a new import from the data file info
#    import = Import.create(
#               the_action:   import_file.action,
#               the_type:     import_file.type,
#               import_file:  File.open(File.expand_path(file.location))
#             )
#    # => #<Import id: 30, the_trigger: "job", the_state: "initializing",
#           the_source_name: "settings", the_type: "smx", the_action: "append", log_path: nil,
#           object_ids: {}, finished_at: nil, created_at: "2014-09-02 12:29:45",
#           updated_at: "2014-09-02 12:29:45", import_file: "smx_netgate_list.tab">
#
#    # Enqueue the import to be processed in the background
#    import.enqueue!
#
#    # Run the import directly
#    import.run!
#
#    ##########################################################################
#    ### Run import on all data files defined in the settings
#
#    Import.enqueue_all_files!
#    # => Enqueues each import record to be processed.
#
class Import < ActiveRecord::Base
  extend Enumerize

  include ActionView::Helpers::NumberHelper
  include ::Kraken::Logger.file

  TYPES = [:accelerated, :smx, :configuration, :organization, :smx_cellular].freeze

  FILE_RETRY_COUNT = 10
  FILE_RETRY_SLEEP = 1.second

  class ImportRunError < StandardError; end

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :the_type, :the_action, :the_source_name, :the_state, :the_trigger,
                  :object_ids, :log_path, :created_at, :finished_at, :import_file

  serialize :object_ids, Hash

  default_scope { order("created_at desc") }

  # Public: The maximum number of import records to keep.
  RECORD_LIMIT = Settings.imports.record_limit || 25

  mount_uploader :import_file, ImportFileUploader

  after_create :purge_old_imports

  # Public: Gets finished imports.
  scope :finished, -> { without_the_state(:initializing, :running) }

  validates :the_type,        :length => { :maximum => 20 }, :allow_blank => true
  validates :the_action,      :length => { :maximum => 20 }, :allow_blank => true
  validates :the_source_name, :length => { :maximum => 20 }, :allow_blank => true
  validates :the_state,       :length => { :maximum => 20 }, :allow_blank => true
  validates :the_trigger,     :length => { :maximum => 20 }, :allow_blank => true

  # Uploaded file validations.
  validates :import_file,       presence: true
  validate  :import_file_size,  on: :create, if: 'import_file.present?'

  # The trigger is what created and started the import.
  #
  # job   - The background job in app/jobs/data_importer.rb (default).
  # admin - The admin panel in the web ui.
  enumerize :the_trigger,
            :in => [:job, :admin],
            :default => :job

  # The action describes how the import will be applied to the database.
  #
  # append - Will only add devices to the database.
  # sync   - Will remove devices from the database if they are not in the
  #          import file.
  # update - Will only update devices already in the database.
  enumerize :the_action,
            :in => [:append, :sync, :update],
            :default => :append

  # The type defines the kind of import file.
  #
  # accelerated - Accelerated Concepts.
  # smx - AT&T Service Manager (default).
  enumerize :the_type,
            :in => TYPES,
            :default => :smx

  # The source is where the import file physically came from.
  #
  # settings - The files currently defined in Settings.yml (default).
  # upload   - A file uploaded from the admin panel.
  enumerize :the_source_name,
            :in => [:settings, :upload],
            :default => :settings

  # The state is used by the state machine for defining the import status.
  #
  # initializing       - The import is initializing (default).
  # running            - The import is running.
  # success            - The import successfully finished.
  # another_is_running - The import failed because another import is running.
  # file not found     - The import failed because it could not find the
  #                      source file.
  # invalid file       - The import failed due to an invalid source file.
  # failure            - The import failed - see log for more information.
  enumerize :the_state,
            :in => [:initializing, :another_is_running, :running, :success,
                    :file_not_found, :invalid_file, :failure],
            :default => :initializing,
            :scope => true

  # Alias 'the' methods with common names. These aren't columns due to reserved
  # keywords in ActiveRecord and some RDBMSs.
  alias_method :status,      :the_state
  alias_method :trigger,     :the_trigger
  alias_method :action,      :the_action
  alias_method :type,        :the_type

  # Public: Adds the import id to a queue. The ImportProcessor will pop this
  # off and run the import.
  #
  # Returns nothing.
  def enqueue!
    if queue = TorqueBox.fetch('/queues/imports')
      logger.info "Import[#{id}] enqueued"
      queue.publish({ import_id: id })
    else
      logger.error "Failed to fetch imports queue for record: #{id}"
    end
  end

  # Public: Runs the import.
  #
  # All of the import states are triggered here.
  #
  # The logic for importing is only run once, when the state transitions to
  # 'running'. After that, calling 'run!' again will raise an error.
  #
  # If the import is successful, all of the objects created are saved to the
  # object_ids Hash. Any skipped devices are saved as well.
  #
  # Returns nothing.
  # Raises ImportRunError if the import has already run.
  def run!
    start_running!

    # It is possible that the import file is stored on another server. We should
    # wait a bit to see if it becomes avilable to the server the import
    # processor is running on.
    FILE_RETRY_COUNT.times do
      if file_not_found?
        sleep FILE_RETRY_SLEEP
      else
        break
      end
    end

    file_not_found! and return if file_not_found?

    begin
      processed = import_class.import_file(import_file.file.path)

      if the_type.configuration?
        self.configuration_ids  = processed[:added]
      elsif the_type.organization?
        self.customer_ids       = processed[:added]
      else
        self.device_ids         = processed[:devices][:added]
        self.skipped_device_ids = processed[:devices][:skipped]
        self.customer_ids       = processed[:customers][:added]
        self.site_ids           = processed[:sites][:added]
        self.location_ids       = processed[:locations][:added]
        self.configuration_ids  = processed[:configurations][:added]

        remove_empty_organizations
      end

      success!
    rescue SmxParser::InvalidFileError => e
      invalid_file!(e)
    rescue => e
      failure!(e)
    end
  end

  # Public: Checks if the import is finished.
  #
  # Returns true if the import is finished; otherwise false.
  def finished?
    self.class.finished_states.include?(import.state) &&
      self.finished_at.present?
  end

  # Public: Gets a detailed message for the import state.
  #
  # Returns a String message for the import state, if found; otherwise nil.
  def status_msg
    case status
    when "initializing"
      "The import is initializing."
    when "another_is_running"
      "Another import is running."
    when "running"
      "The import is currently running."
    when "success"
      "Successfully imported file."
    when "file_not_found"
      "File not found."
    when "invalid_file"
      "Invalid file. Please ensure that the source file is a valid tabfile."
    when "failure"
      "Import failed. Please check the server log for more information."
    end
  end

  # Public: Gets the import files defined in the settings.
  #
  # Returns an Array of ImportFile objects defined in the settings.
  def self.files
    Settings.data_files || []
  end

  # Public: Enqueues the import files defined in settings for processing.
  #
  # Returns nothing.
  def self.enqueue_all_files!
    files.each do |file|
      create!(
        the_action:   file.action,
        the_type:     file.type,
        log_path:     file.log_path,
        import_file:  File.open(File.expand_path(file.location))
      ).enqueue!
    end
  end

  private

  # Internal: Gets the current import state.
  #
  # Returns a MicroMachine object with the current import state.
  def import
    @import ||= begin
      fsm = MicroMachine.new(the_state || 'initializing')

      fsm.when(:run,                'initializing' => 'running')
      fsm.when(:file_not_found,     'running' => 'file_not_found')
      fsm.when(:another_is_running, 'running' => 'another_is_running')
      fsm.when(:invalid_file,       'running' => 'invalid_file')
      fsm.when(:success,            'running' => 'success')
      fsm.when(:failure,            'running' => 'failure')

      # Save the state after each transition.
      fsm.on(:any) { update_attributes :the_state => import.state }

      # Run `finish!` on all finished states.
      self.class.finished_states.each { |state| fsm.on(state) { finish! } }

      fsm
    end
  end

  # Internal: Gets the class for the import type. The class should contain all
  # of the logic for importing the particular type of file.
  #
  # Returns the import class, if found; otherwise nil.
  def import_class
    case type
    when "smx"
      ::ServiceManager::ImportService
    when "accelerated"
      DeviceImporter
    when 'configuration'
      ConfigurationImporter
    when 'organization'
      OrganizationImporter
    when 'smx_cellular'
      SmxCellularImporter
    end
  end

  # Internal: Changes the state to indicate that the import is running.
  #
  # Returns true if the state changed; otherwise false.
  def start_running!
    logger.info "Import[#{id}] has started running"
    raise ImportRunError, "Import has already run" unless import.trigger(:run)
  end

  # Internal: Changes the state to indicate a successful import.
  #
  # Returns true if the state changed; otherwise false.
  def success!
    logger.info "Import[#{id}] is successful"
    import.trigger(:success)
  end

  # Internal: Changes the state to indicate that another import is running.
  #
  # Returns true if the state changed; otherwise false.
  def another_is_running!
    logger.error "Import[#{id}] is already running"
    import.trigger(:another_is_running)
  end

  # Internal: Changes the state to indicate that the source file wasn't found.
  #
  # Returns true if the state changed; otherwise false.
  def file_not_found!
    logger.error "Import[#{id}] failed to find file: #{import_file.path}"
    import.trigger(:file_not_found)
  end

  # Internal: Changes the state to indicate that the source file is invalid.
  #
  # Returns true if the state changed; otherwise false.
  def invalid_file!(error = nil)
    message = "Import[#{id}] file is invalid"
    message << ": #{error.message}\n #{error.backtrace}" if error
    logger.error message
    import.trigger(:invalid_file)
  end

  # Internal: Changes the state to indicate that the import failed. This is a
  # generic failure for unrecognized exceptions thrown during the import.
  #
  # Returns true if the state changed; otherwise false.
  def failure!(error = nil)
    message = "Import[#{id}] failed"
    message << ": #{error.message}\n #{error.backtrace}" if error
    logger.error message
    import.trigger(:failure)
  end

  # Internal: Handles any actions that need to occur when an import is finished.
  #
  # Returns nothing.
  def finish!
    touch :finished_at
  end

  # Internal: Checks if the source file exists.
  #
  # Returns true if the file is not found and the state successfully
  #   transitions to 'file not found'; otherwise false.
  def file_not_found?
    !File.exists?(import_file.path)
  end

  # Internal: Overrides Rails's method_missing routine to dynamically get and
  # set object_ids. Instead of defining duplicate methods like 'device_ids' and
  # 'account_ids', this will match the '_ids' method and get/set it.
  #
  # If the method does not end in '_ids' or '_ids=' it will invoke the base
  # method_missing.
  #
  # meth  - The Symbol missing method name.
  # args  - The Array of arguments passed to the missing method (default: []).
  # block - The Block passed to the missing method (optional).
  #
  # Returns nothing.
  def method_missing(meth, *args, &block)
    if meth.to_s =~ /_ids=?\z/
      args.any? ? set_object_ids(meth, args) : get_object_ids(meth)
    else
      super
    end
  end

  # Internal: Gets a list of IDs for an object name.
  #
  # obj_name - The Symbol name of the object.
  #
  # Returns an array of Integer IDs for the object name.
  def get_object_ids(obj_name)
    object_ids.has_key?(obj_name) ? object_ids[obj_name] : []
  end

  # Internal: Sets values for an object name.
  #
  # obj_name - The Symbol name of the object to set.
  # args     - The Array of arguments to set for the object name.
  #
  # Returns nothing.
  def set_object_ids(obj_name, *args)
    name = obj_name.to_s.gsub(/=\z/,'').to_sym
    object_ids[name] = args.first.flatten
  end

  # Internal: Trims the number of imports to RECORD_LIMIT. This is a callback
  # method that is called after each import is created.
  def purge_old_imports
    count = self.class.count
    if count > RECORD_LIMIT
      difference = count - RECORD_LIMIT
      self.class.reorder('created_at asc').limit(difference).destroy_all
    end
  end

  # Internal: If enabled, this will remove any organizations under root that
  # don't have any devices.
  def remove_empty_organizations
    return unless Settings.imports.remove_empty_organizations == true
    return unless root_organization.present?
    root_organization.descendants.find_each(batch_size: 100) do |organization|
      organization.destroy if organization.self_and_descendant_devices.count.zero?
    end
  end

  def root_organization
    @root_organization ||= Organization.roots.order('id asc').first
  end

  # Internal: Gets a list of finished states.
  #
  # Returns an Array of Strings containing the states considered finished.
  def self.finished_states
    states = the_state.values
    states.delete "initializing"
    states.delete "running"
    states
  end

  def import_file_size
    if import_file.file.size > ImportFileUploader::FILE_SIZE_LIMIT
      errors.add(
        :import_file,
        "is over the size limit of #{number_to_human_size(ImportFileUploader::FILE_SIZE_LIMIT, precision: 2)}."
      )
    end
  end
end
