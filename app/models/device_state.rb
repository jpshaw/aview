require 'statusable'

class DeviceState < ActiveRecord::Base
  include Statusable

  # TODO: Combine these into single module
  States = Status

  belongs_to :device

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :lock_version, :state, :device_id

  validates :lock_version, :numericality => { :only_integer => true, :allow_nil => true }
  validates :state, :numericality => { :only_integer => true, :allow_nil => true }
  validates :device_id, :numericality => { :only_integer => true, :allow_nil => true }

  before_create :set_initial_state

  scope :up,          ->() { where(state: States::UP) }
  scope :down,        ->() { where(state: States::DOWN) }
  scope :unreachable, ->() { where(state: States::UNREACHABLE) }

  def up?
    state === States::UP
  end

  # TODO: Deprecate and remove `unreachable`.
  def unreachable?
    state === States::UNREACHABLE
  end

  def down?
    state === States::DOWN
  end

  # Internal: State machine accessor used to change the device state.
  def state_machine
    @state_machine ||= begin
      status = MicroMachine.new( state || States::UNREACHABLE )
      status.when(:heartbeat,     States::DOWN => States::UP, States::UNREACHABLE => States::UP)
      status.when(:reboot,        States::UP => States::UNREACHABLE)
      status.when(:shutdown,      States::UP => States::UNREACHABLE)
      status.when(:timeout,       States::UP => States::UNREACHABLE)
      status.when(:recovery_fail, States::UP => States::DOWN, States::UNREACHABLE => States::DOWN)
      status.when(:down,          States::UP => States::DOWN, States::UNREACHABLE => States::DOWN)
      status.on(:any)                 { update_attributes :state => state_machine.state }
      status.on(States::DOWN)         { device.alert_down }
      status.on(States::UP)           { device.alert_up }
      status.on(States::UNREACHABLE)  { device.alert_unreachable }
      status
    end
  end

  def set_initial_state
    state_machine
    assign_attributes :state => state_machine.state
  end

  #TODO: Delete once the new filter toolbar is implemented across the site
  def self.select_list
    [
      ["All",         0                  ],
      ["Up",          States::UP         ],
      ["Alert",       States::DOWN       ],
      ["Unreachable", States::UNREACHABLE]
    ]
  end

  def self.facet_values
    {
      'Up' => States::UP,
      'Unreachable' => States::UNREACHABLE,
      'Down' => States::DOWN
    }
  end

end
