class Notification < ActiveRecord::Base
  include Searchable

  belongs_to :event, class_name: 'DeviceEvent' # TODO: Remove in 3.13
  belongs_to :notification_subscription
  belongs_to :user
  belongs_to :device
  belongs_to :uuid, class_name: 'DeviceEventUUID'

  serialize :data, JSON

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :device_id, :uuid_id, :severity, :event_type, :message,
    :data, :delivered_at, :event_id, :user_id, :notification_subscription_id,
    :created_at, :updated_at

  validates :notification_subscription_id, presence: true, numericality: { only_integer: true }
  validates :user_id, presence: true, numericality: { only_integer: true }

  delegate :profile, to: :notification_subscription, allow_nil: true

  after_commit :deliver_if_immediate!, on: :create

  has_search_scope :with_event_type,  ->(type)  { where(event_type: type) }
  has_search_scope :with_event_level, ->(level) { where(severity: level) }
  has_search_scope :delivered_since, ->(date) { where('delivered_at >= ?', date) }
  has_search_scope :with_device_organization, ->(filter_hash) {
    joins(:device).merge(Device.with_organization(filter_hash))
  }
  has_search_scope :with_device_categories, ->(categories) {
    joins(:device).merge(Device.with_categories(categories))
  }
  has_search_scope :with_device_models, ->(models) {
    joins(:device).merge(Device.with_models(models))
  }
  has_search_scope :with_device_organization_id, ->(id) {
    joins(:device).where(devices: { organization_id: id })
  }
  has_search_scope :with_device_mac, ->(mac) {
    joins(:device).merge(Device.with_mac(mac))
  }
  has_search_scope :sort_by_delivered_at, ->(direction) { order("delivered_at #{sort_direction direction}") }

  def lvl
    if key = ::EventLevels.constants_hash.key(severity)
      I18n.t(key, default: ::DeviceEvent::DEFAULT_LEVEL_NAME)
    else
      ::DeviceEvent::DEFAULT_LEVEL_NAME
    end
  end

  def type
    if key = EventTypes.constants_hash.key(event_type)
      I18n.t(key, default: ::DeviceEvent::DEFAULT_TYPE_NAME)
    else
      ::DeviceEvent::DEFAULT_TYPE_NAME
    end
  end

  # Public: Delivers a single notification according to the user's settings.
  #
  # Examples
  #
  #   Notification.first.deliver!
  #   # => email is sent to the user
  #
  # Returns nothing.
  def deliver!
    if queue = TorqueBox.fetch("/queues/immediate_notifications")
      queue.publish(self.id)
    end
  end

  # Public: Sets the notification delivered_at timestamp to now.
  def delivered!
    touch :delivered_at unless delivered?
  end

  # Public: Check if a notification has been delivered yet by checking the timestamp.
  #
  # Returns true if the notification has been delivered, otherwise false.
  def delivered?
    !delivered_at.nil?
  end

  # Public: Returns available notifications that users are interested in receiving.
  #
  # Returns an activerecord relation for notifiable records.
  def self.notifiable
    joins(notification_subscription: [profile: [:subscribed_events]])
      .where('subscribed_events.frequency <> ?', NotificationProfile::Frequencies::NEVER)
      .where(delivered_at: nil)
      .uniq
  end

  def self.bulk_notifiable
    subscribed_events = SubscribedEvent.arel_table
    immediately       = NotificationProfile::Frequencies::IMMEDIATELY
    never             = NotificationProfile::Frequencies::NEVER

    joins(uuid: :subscribed_events)
      .where(
        subscribed_events[:frequency]
          .gt(immediately).and(subscribed_events[:frequency].lt(never))
      )
      .where(delivered_at: nil)
  end

  # Public: Retrieve a list of notifications scheduled to be sent for
  # a particular minute of the day, grouped by their subscriptions.
  #
  # minute - An Integer minute of the day, between 0 and 1425 (12:00AM to 11:45PM).
  #
  # Examples
  #
  #   # Get notifications to be sent at 12:15 AM
  #   Notification.subscriptions_for_minute_of_day(15)
  #   # => {#<NotificationSubscription id: 18, ...> => [#<Notification id: 14,
  #         event_id: 696 ...>, #<Notification id: 15, event_id: 699, ...>]}
  #
  # Returns an array of notifications to be sent, grouped by their subscription.
  def self.available_for_minute_of_day(minute)
    # Minute must be an integer between 0 and 1425 (12:00AM to 11:45PM).
    return [] if !minute.is_a?(Integer) || minute < 0 || minute > 1425

    bulk_notifiable
      .joins(
        'INNER JOIN `notification_subscriptions` ' \
        'ON `notification_subscriptions`.`id` = `notifications`.`notification_subscription_id` ' \
        'AND `subscribed_events`.`profile_id` = `notification_subscriptions`.`profile_id`')
      .where('MOD(?, `subscribed_events`.`frequency`) = 0', minute)
      .group_by(&:notification_subscription)
  end

  private

  # Internal: Check whether the user wants this notification delivered immediately.
  def deliver_if_immediate!
    return unless profile.present?

    subscribed_event = profile.subscribed_events.find_by(uuid_id: uuid_id)

    if subscribed_event && subscribed_event.immediate?
      deliver!
    end
  end
end
