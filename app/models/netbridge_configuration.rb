class NetbridgeConfiguration < ActiveRecord::Base
  include Searchable

  ATTRIBUTES_KEYS = [:data, :cfg, :config_change_reboot, :firmware_server, :iui,
    :png, :rbt, :remote_control, :sms_check_freq, :upload_server, :var, :wdog,
    :sticky_apn, :xma, :xmc, :xmi, :xmp, :xmt, :xmu, :link_check_freq,
    :link_timeout, :traf_acct, :dhcp_lease_max, :dlm, :dre, :drn, :drp, :drs,
    :eth, :gip, :gip_secondary, :mtu, :ddns , :dnslist, :rat, :ptf, :ptfr,
    :ips_cust_dh_grp, :ips_cust_home_ep, :ips_cust_home_net,
    :ips_cust_keyid_tag, :ips_cust_psk, :ips_cust_remote_ep,
    :ips_cust_remote_net, :ips_cust_xauth_pass, :ips_cust_xauth_user,
    :ips_keepalive, :tus, :vpn_cust_type, :log, :log_level, :log_list,
    :syslog_http, :img, :img2, :img3, :hostname, :ippassthrough, :proxy_arp,
    :usb_adjust, :ota_updates, :log_list2, :conf_insecure_ssl, :crt_url,
    :dhcp_enable, :status_freq].freeze
  ADDITIONAL_SETTINGS_KEYS  = [:grace_period_duration].freeze
  CONFIG_KEYS               = (ATTRIBUTES_KEYS + ADDITIONAL_SETTINGS_KEYS).freeze

  LOCKED_BY_DEFAULT = true
  LOCKED_VALUE      = '1'.freeze
  UNLOCKED_VALUE    = '0'.freeze
  GET_LOCK_REGEX    = /_locked(\??)\z/ # Matches *_locked and *_locked? methods.
  SET_LOCK_REGEX    = /_locked=\z/     # Matches *_locked=... methods.

  DEFAULT_CONFIG_ENDPOINT = "#{Settings.subdomains.configurations}.accns.com".freeze

  DEFAULT_STATUS_FREQ = '30m'.freeze

  store :additional_settings, accessors: [:grace_period_duration]

  has_many :devices, :foreign_key => "configuration_id", :class_name => 'Netbridge'

  belongs_to :organization

  # TODO: remove `attr_accessible` and use strong params
  attr_accessible :name, :organization_id, :parent_id, :import_name, :key_meta
  attr_accessible *CONFIG_KEYS

  serialize :key_meta, ActiveSupport::HashWithIndifferentAccess

  # This callback must run before the acts_as_tree callbacks
  before_destroy :error_if_children
  before_destroy :error_if_devices

  before_save :set_heartbeat_fields

  after_initialize :defaults

  acts_as_tree hierarchy_table_name: 'netbridge_config_hierarchies'
  has_paper_trail

  validates :name, :presence => true, :length => { :maximum => 50 },
    :uniqueness => { :case_sensitive => false, :scope => :organization_id }

  validates :organization_id, :presence => true,
    :numericality => { :only_integer => true, :greater_than_or_equal_to => 1, :allow_nil => true }

  validate :valid_grace_period_duration?, unless: lambda { |nc| nc.grace_period_duration_locked? }

  has_search_scope :with_name_search, ->(search) { where("netbridge_configurations.name LIKE ?", "%#{search}%") }
  has_search_scope :sort_by_name, ->(direction) { order("name #{sort_direction direction}") }

  # Public: Applies the given attributes to the configuration.
  def apply_attributes(attributes, locks = {})
    attributes ||= {}
    locks      ||= {}

    # Reset key metadata. This prevents stale keys from accruing over time.
    key_meta = ActiveSupport::HashWithIndifferentAccess.new

    CONFIG_KEYS.each do |key|

      # Ensure we have a hash for the key.
      key_meta[key] ||= {}

      # If we have no explicit lock for the key, check if there is a value,
      # and if so, set to unlocked. Otherwise, set to the default.
      if [LOCKED_VALUE, UNLOCKED_VALUE].exclude?(locks["#{key}_locked"])
        locks["#{key}_locked"] = \
          if attributes[key].nil?
            LOCKED_BY_DEFAULT ? LOCKED_VALUE : UNLOCKED_VALUE
          else
            UNLOCKED_VALUE
          end
      end

      # If the key is locked, add metadata and set the value to nil.
      if locks["#{key}_locked"] == LOCKED_VALUE
        key_meta[key]['locked'] = true
        attributes[key]         = nil


      # If the key is not locked, add metadata and leave value alone.
      elsif locks["#{key}_locked"] == UNLOCKED_VALUE
        key_meta[key]['locked'] = false
      end
    end

    update_attributes attributes.merge(key_meta: key_meta)
  end

  # Public: Check if a given key is locked.
  def key_locked?(key)
    send("#{key}_locked?") == true
  end

  # Public: Determines the "final" configuration values and presents them
  # as key-value pairs separated by newlines.
  def to_keyval
    config_values = config_values_with_inherited

    # Ensure we always give a configuration endpoint to the device.
    config_values['cfg'] ||= DEFAULT_CONFIG_ENDPOINT

    config_values.reduce([]) do |keyvals, (key, value)|
      value.nil? ? keyvals : keyvals << "#{translate_key(key)}=#{value_for_device(value)}"
    end.join("\n") << "\n"
  end

  def firmware
    # depending on model, return correct image
    return img unless img.nil?
    return img2 unless img2.nil?
    return img3 unless img3.nil?

    #default
  end

  def error_if_children
    return true if self.leaf?
    self.errors[:base] << "This configuration has dependant configurations."
    false
  end

  def error_if_devices
    return true if self.devices.none?
    self.errors[:base] << "This configuration has dependant devices."
    false
  end

  # Public: Build hash of values that may be inherited from ancestors.
  #
  # Examples
  #
  #   values_from_ancestors
  #     #=> { "xma" => "cingular", "xmp" => "default" }
  #
  # Returns a hash of values from the configuration inheritance chain.
  def values_from_ancestors
    @values_from_ancestors ||= ancestors.reverse.reduce({}) do |hash, ancestor|
      hash.merge(ancestor.config_values.reject { |_, v| v.nil? })
    end
  end

  # Public: Builds a hash of config values that are unlocked and valid.
  def config_values
    attributes_config_values.merge(additional_settings_config_values)
  end

  # Public: Build hash of the final config values, including this config and all
  #   inherited values.
  #
  # Examples
  #
  #   values_from_ancestors
  #     #=> { "xma" => "cingular", "xmp" => "default" }
  #   config_values
  #     #=> { "xmu" => "user", "xmp" => "password" }
  #
  #   config_values_with_inherited
  #     #=> { "xma" => "cingular", "xmu" => "user", "xmp" => "password" }
  #
  # Returns a hash of values representing the compiled configuration.
  def config_values_with_inherited
    @config_values_with_inherited ||= values_from_ancestors.merge(config_values)
  end

  # Public: Takes the hash built by config_values_with_inherited and processes
  # the data for use in the form. It will remove any blanks. It will change
  # instances of true to "Yes" and false to "No". This allows the data to match
  # the same terminiology that we have in the form.
  #
  # Returns a hash of values representing a cleaned up compiled configuration.
  def config_values_with_inherited_for_form
    retval = config_values_with_inherited
    retval.each do |key,value|
      if value == true
        retval[key] = "Yes"
      elsif value == false
        retval[key] = "No"
      elsif value.blank?
        retval.delete(key)
      end
    end
    retval
  end

  # Public: The policy class for the configuration object.
  def policy_class
    "#{self.class.model_name}Policy"
  end

  def self.default
    where(:name => "Default").first_or_create
  end

  def self.find_by_device_mac(mac)
    joins(:devices).where(devices: { mac: mac }).first
  end

  protected

  # Internal: Defines dynamic methods to match for the configuration keys,
  # if found, otherwise will raise the NoMethodError with the super class.
  def method_missing(meth, *args, &block)

    # Get method name as string to match regexes.
    method_name = meth.to_s

    if method_name =~ GET_LOCK_REGEX

      # Get the key from the method. For instance, if `cfg_locked` is given,
      # it will get the key as `cfg`.
      key = method_name.gsub(GET_LOCK_REGEX, '')

      # If we have a meta entry for the key, check the locked value.
      if key_meta[key]
        return key_meta[key]['locked'] == true

      # Otherwise return the default lock value.
      else
        return LOCKED_BY_DEFAULT
      end

    elsif method_name =~ SET_LOCK_REGEX

      key = method_name.gsub(SET_LOCK_REGEX, '')

      # Ensure we at least have a hash for the key.
      key_meta[key] ||= {}

      # Set locked to true or false based on the given value.
      key_meta[key]['locked'] = args.first == true

    else
      super
    end
  end

  # Internal: Converts the given value into what a device is expecting.
  def value_for_device(value)
    case value
    when true  then '1'
    when false then '0'
    else value
    end
  end

  # Internal: Converts the given key into what a device is expecting.
  def translate_key(key)
    case key
    when 'dhcp_enable' then 'dhcp.enable'
    when 'crt_url' then 'crt.url'
    when 'conf_insecure_ssl' then 'conf.insecure_ssl'
    else key
    end
  end


  private

  def valid_grace_period_duration?
    calculate_duration(self.grace_period_duration, :grace_period_duration)
  end


  def calculate_duration(value, attribute_name=nil)
    ::DurationCalculator.calculate(value)
  rescue => e
    errors.add(attribute_name, "is invalid. #{e.message}") if attribute_name
  end


  def attributes_config_values
    configuration_values(attributes, ATTRIBUTES_KEYS)
  end


  def additional_settings_config_values
    configuration_values(additional_settings, ADDITIONAL_SETTINGS_KEYS)
  end


  def configuration_values(attrs, config_keys)
    retval = {}

    attrs.each do |key, value|
      next unless config_keys.include? key.to_sym
      next if key_locked?(key)
      retval[key] = value
    end

    retval
  end

  def defaults
    # NOTE: This excplicitly checks if the key_meta is set to a String, which
    # is what Oracle will do by default when saving a configuration record. This
    # is a known issue with the oracle enhanced adapter, fixed in a version
    # supported by Rails 4.2+:
    #   https://github.com/rsim/oracle-enhanced/issues/549
    case self.key_meta
    when String
      self.key_meta = eval(self.key_meta).with_indifferent_access
    else
      self.key_meta ||= ActiveSupport::HashWithIndifferentAccess.new
    end
  end

  def set_heartbeat_fields
    frequency = self.status_freq || config_values_with_inherited['status_freq'] || DEFAULT_STATUS_FREQ
    grace_period = self.grace_period_duration || config_values_with_inherited[:grace_period_duration]

    self.heartbeat_frequency = calculate_duration(frequency)
    self.heartbeat_grace_period = calculate_duration(grace_period)
  end

end
