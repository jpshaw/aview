class NotificationPresenter < EventsIndexPresenter
  def columns
    super
    sort_column = @columns.detect{ |column| column[:sort_scope] == :sort_by_created_at }
    sort_column[:sort_scope] = :sort_by_delivered_at if sort_column
    @columns
  end

  def title
    :notifications
  end

  def facets
    @facets =
        {
            organization: {
              facet_values: organizations_map,
              scope: :with_device_organization_id
            },
            category: {
                facet_values: DeviceCategory. # TODO: Rails 4 -> DeviceCategory.select(:id, :name).map{...}
                    find_by_sql('select id, name from device_categories').
                    map { |e| { e.name => e.id } }.
                    reduce(:merge),
                scope: :with_device_categories,
            },
            model: {
                facet_values: DeviceModel.
                    find_by_sql('select id, name from device_models').
                    map { |e| { e.name => e.id } }.
                    reduce(:merge),
                scope: :with_device_models,
            },
            type: {
                facet_values: Hash[*EventTypes.select_list.flatten],
                scope: :with_event_type
            },
            level: {
                facet_values: Hash[*EventLevels.select_list.flatten],
                scope: :with_event_level
            },
            range: {
                facet_values: { "Last Day" => 1.day.ago, "Last Week" => 1.week.ago, "Last Month" => 1.month.ago },
                scope: :delivered_since
            }
        }
  end
end
