module Presenters
  module Reports
    class Availability < Presenters::Reports::New
      def report
        @report ||= AvailabilityReport.new
      end

      def title
        :availability_form
      end

      def device_categories
        @categories ||= DeviceCategory.order('device_categories.name asc')
      end

      def has_device_category_selection?
        true
      end
    end
  end
end