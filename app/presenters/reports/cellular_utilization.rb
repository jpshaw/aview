module Presenters
  module Reports
    class CellularUtilization < Presenters::Reports::New
      def report
        @report ||= CellularUtilizationReport.new
      end

      def title
        :cellular_utilization_form
      end
    end
  end
end
