module Presenters
  module Reports
    class CellularNetworkType < Presenters::Reports::New
      def report
        @report ||= CellularNetworkTypeReport.new
      end

      def title
        :cellular_network_type_form
      end
    end
  end
end