module Presenters
  module Reports
    class CellularSignalStrength < Presenters::Reports::New
      def report
        @report ||= CellularSignalStrengthReport.new
      end

      def title
        :cellular_signal_strength_form
      end
    end
  end
end