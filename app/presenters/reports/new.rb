module Presenters
  module Reports
    class New < Presenters::Base
      def device_categories_for_select
        device_categories.collect { |c| [c.name, c.id] }
      end

      def device_models_for_select
        device_categories.map do |category|
          [category.name, category.models.map { |model| [model.name, model.id] }]
        end
      end

      def device_categories
        @categories ||= DeviceCategory.where(name: [DeviceCategory::LEGACY_CELLULAR_CATEGORY,
                                                    DeviceCategory::CELLULAR_CATEGORY,
                                                    DeviceCategory::DIAL_TO_IP_CATEGORY,
                                                    DeviceCategory::UCPE_CATEGORY,
                                                    DeviceCategory::REMOTE_MANAGER_CATEGORY,
                                                    DeviceCategory::GATEWAY_CATEGORY])
      end

      def ranges
        [
            ['Last Day',    'last_day'],
            ['Last Week',   'last_week'],
            ['Last Month',  'last_month']
        ]
      end

      def report_count user
        user.reports.count
      end

      def has_device_category_selection?
        false
      end

      def has_device_model_selection?
        true
      end

      def has_time_span_selection?
        true
      end
    end
  end
end
