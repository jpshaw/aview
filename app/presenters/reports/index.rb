module Presenters
  module Reports
    class Index < Presenters::Base
      def columns
        @columns =
            [
                {
                    name: :id,
                    table_label: 'Report ID',
                    class: 'text-center'
                },
                {
                    name: :type,
                    table_label: 'Type',
                    class: 'text-center'
                },
                {
                    name: :status,
                    table_label: 'Status',
                    class: 'text-center'
                },
                {
                    name: :organization,
                    table_label: 'Organization',
                    class: 'text-center'
                },
                {
                    name: :generated_in,
                    table_label: 'Generated In',
                    class: 'text-center'
                },
                {
                    class: 'sortable-primary text-center',
                    direction: 'desc',
                    name: :created_at,
                    sort_scope: :sort_by_created_at,
                    table_label: 'Created At'
                },
                {
                    name: :download,
                    table_label: 'Download',
                    class: 'text-center'
                },
                {
                    name: :delete,
                    table_label: 'Delete',
                    class: 'text-center'
                }
            ]
      end

      def column_data(column)
        result = {}
        result[:direction] = column[:direction] if column[:direction].present? # Sorting Direction
        result[:name] = column[:name] # Used by DataTable to identify column
        result[:sort_scope] = column[:sort_scope] # Used by DataTable to send sorting scope of column
        result
      end

      def klass
        :device_report
      end

      def title
        :generated_reports
      end
    end
  end
end