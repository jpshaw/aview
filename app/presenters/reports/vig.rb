module Presenters
  module Reports
    class Vig < Presenters::Reports::New
      def report
        @report ||= VigReport.new
      end

      def title
        :vig_form
      end

      def device_categories
        @categories ||= DeviceCategory.gateway
      end

      def device_models
        DeviceModel.includes(:category).where(:device_categories => { :id => device_categories.map(&:id) })
      end

      def has_device_category_selection?
        false
      end

      def has_device_model_selection?
        false
      end

      def has_time_span_selection?
        false
      end
    end
  end
end
