class DeviceFirmwaresPresenter
  def initialize(device_firmwares)
    @firmwares = device_firmwares
  end

  def rows
    @firmwares.map do |firmware|
      [
          firmware.version,
          firmware.created_at,
          Device.joins(:series).where(firmware: firmware.version).where("device_models.id = #{firmware.device_model_id}").count
      ]
    end
  end

  def headers
    %w(Version Created\ At Device\ Count)
  end
end