class DeviceAssociationPresenter < Presenters::Base

  attr_reader :association, :current_device, :associated_device, :associated_mac,
    :associated_interface, :associated_type

  delegate :device_path, :device_device_association_path, :time_ago_in_words, :t,
    to: :view

  DATA_UNKNOWN = '-'

  def initialize(view_context, current_device, association)
    super(view_context)

    @association = association
    @current_device = current_device
    @is_inversed = association.is_inversed_for?(@current_device)

    if @is_inversed
      @associated_device    = association.source_device
      @associated_mac       = association.source_mac
      @associated_interface = association.target_interface
      @associated_type      = association.target_type
    else
      @associated_device    = association.target_device
      @associated_mac       = association.target_mac
      @associated_interface = association.source_interface
      @associated_type      = association.source_type
    end
  end

  def device_link
    if associated_device
      link_to(associated_device.mac, device_path(associated_device))
    else
      associated_mac
    end
  end

  def device_model
    associated_device.try(:series_name) || DATA_UNKNOWN
  end

  def interface
    associated_interface || DATA_UNKNOWN
  end

  def type
    type = associated_type.try(:upcase) || DATA_UNKNOWN
    type += " (#{association.lease_type})" if association.lease_type.present?
    type
  end

  def ip_address
    association.ip_address || DATA_UNKNOWN
  end

  def connection_time
    time_ago_in_words(association.created_at)
  end

  def delete_button
    link_to('<i class="fa fa-chain-broken"> </i>'.html_safe,
      device_device_association_path(current_device, association, format: :js),
      data: { confirm: t('device_associations.confirm_deletion') },
      class: 'btn btn-warning btn-xs table-button-remote',
      method: :delete,
      remote: true
    )
  end

end
