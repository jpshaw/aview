class EventsIndexPresenter < Presenters::Base
  include DataTableable

  def columns
    @columns ||=
      [
        # {
        #    class: 'searchable', # This instructs DataTable that the column should be searchable
        #    name: :filter_scope, # This is the scope that should be used for searching, if not searching it is arbitrary.
        #    sort_scope: :sort_scope # This is the scope that should be used to determine sort / order
        #    table_label: 'Category', # The label that should be applied to the column in the table header.
        #    visibility_label: 'Special Category' # This is an optional attribute dictating the label for column visbility.
        # }
        {
          name: :mac,
          table_label: I18n.t(:mac)
        },
        {
          class: 'searchable',
          name: :categories,
          table_label: 'Category'
        },
        {
          class: 'searchable',
          name: :models,
          table_label: 'Model'
        },
        {
          name: :organization,
          table_label: 'Organization'
        },
        {
          name: :site,
          table_label: 'Site',
        },
        {
          class: 'searchable',
          name: :type,
          table_label: 'Type'
        },
        {
          class: 'searchable',
          name: :level,
          table_label: 'Level'
        },
        {
          name: :information,
          table_label: 'Information',
        },
        {
          class: 'sortable-primary',
          direction: 'desc',
          sort_scope: :sort_by_created_at,
          name: :created_at,
          table_label: 'Created'
        }
      ]
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets =
      {
        organization: {
          facet_values: organizations_map,
          scope: :with_device_organization_id
        },
        category: {
          facet_values: DeviceCategory. # TODO: Rails 4 -> DeviceCategory.select(:id, :name).map{...}
            find_by_sql('select id, name from device_categories').
            map { |e| { e.name => e.id } }.
            reduce(:merge),
          scope: :with_device_categories,
        },
        model: {
          facet_values: DeviceModel.
            find_by_sql('select id, name from device_models').
            map { |e| { e.name => e.id } }.
            reduce(:merge),
          scope: :with_device_models,
        },
        type: {
          facet_values: Hash[*EventTypes.select_list.flatten],
          scope: :with_type
        },
        level: {
          facet_values: Hash[*EventLevels.select_list.flatten],
          scope: :with_level
        },
        range: {
          facet_values: { "Last Day" => 1.day.ago, "Last Week" => 1.week.ago, "Last Month" => 1.month.ago },
          scope: :created_since
        }
      }
  end

  def title
    :device_events
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end
end
