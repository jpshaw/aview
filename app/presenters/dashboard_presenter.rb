class DashboardPresenter < Presenters::Base
  include DataTableable
  include DeviceActionable

  def columns
    @columns =
      [
        {
          class: 'searchable sortable center-children',
          name: :with_device_status,
          sort_scope: :sort_by_device_status,
          table_label: I18n.t(:device_status),
          visibility_label:  I18n.t(:device_status)
        },
        {
          class: 'searchable sortable',
          name: :with_mac,
          sort_scope: :sort_by_devices_mac,
          table_label: I18n.t(:mac)
        },
        {
          class: 'sortable',
          name: :host,
          sort_scope: :sort_by_devices_host,
          table_label: I18n.t(:primary_ip)
        },
        {
          name: :mgmt_host,
          table_label: 'Management IP'
        },
        {
          name: :hostname,
          table_label: 'Hostname'
        },
        {
          class: 'searchable sortable',
          name: :with_categories,
          sort_scope: :sort_by_device_categories_name,
          table_label: 'Category',
        },
        {
          class: 'searchable sortable',
          name: :with_models,
          sort_scope: :sort_by_device_models_name,
          table_label: 'Model'
        },
        {
          class: 'sortable',
          name: :with_organization,
          sort_scope: :sort_by_organizations_name,
          table_label: 'Organization',
        },
        {
          class: 'sortable',
          name: :site,
          sort_scope: :sort_by_sites_name,
          table_label: 'Site'
        },
        {
          class: 'sortable',
          name: :firmware,
          sort_scope: :sort_by_devices_firmware,
          table_label: 'Firmware'
        },
        {
          name: :city,
          table_label: 'City'
        },
        {
          class: 'sortable',
          name: :country,
          sort_scope: :sort_by_countries_name,
          table_label: 'Country'
        },
        {
          class: 'sortable init-hidden',
          direction: 'desc',
          name: :activated_at,
          sort_scope: :sort_by_devices_activated_at,
          table_label: "Activation Date (#{view_context.user_timezone})"
        },
        {
          class: 'sortable sortable-primary',
          direction: 'desc',
          name: :last_heartbeat,
          sort_scope: :sort_by_devices_last_heartbeat_at,
          table_label: "Last Heartbeat (#{view_context.user_timezone})"
        }
      ]
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets =
      {
        category: {
          facet_values: DeviceCategory. # TODO: Rails 4 -> DeviceCategory.select(:id, :name).map{...}
          find_by_sql('select id, name from device_categories').
            map { |e| { e.name => e.id } }.
            reduce(:merge),
          scope: :with_categories,
        },
        deployment: {
          facet_values: ::Devices::Deployment.facet_values,
          scope: :with_deployment,
          select_type: :single
        },
        model: {
          facet_values: DeviceModel.
            find_by_sql('select id, name from device_models').
            map { |e| { e.name => e.id } }.
            reduce(:merge),
          scope: :with_models,
        },
        status: {
          facet_values: DeviceState.facet_values,
          scope: :with_device_status,
          select_type: :single
        }
      }
  end

  def title
    :alert_devices
  end

  def skip_translate?
    false
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end
end
