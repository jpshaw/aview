class LegacyCellularDeviceConfigurationsPresenter < Presenters::Base
  include DataTableable

  def columns
    @columns ||=
      [
        {
            class: 'sortable',
            name: :with_name,
            sort_scope: :sort_by_name,
            table_label: 'Name'
        },
        {
            class: 'sortable text-center',
            name: :with_organization,
            sort_scope: :sort_by_organizations_name,
            table_label: 'Organization'
        },
        {
            class: 'sortable sortable-primary text-center',
            direction: 'desc',
            name: :sort_by_updated_at,
            sort_scope: :sort_by_updated_at,
            table_label: 'Updated'
        },
        {
            class: 'text-center',
            name: :edit,
            table_label: 'Edit'
        },
        {
            class: 'text-center',
            name: :delete,
            table_label: 'Delete'
        }
      ]
  end

  def title
    :legacy_cellular_configurations
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end

  def facets
    @facets ||=
      {
        name: {
          facet_values: '',
          scope: :with_name_search
        },
        organization: {
          facet_values: organizations_map,
          scope: :with_organization_id
        }
      }
  end

end