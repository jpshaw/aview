class MapPresenter < Presenters::Base
  def facets
    @facets ||=
      {
        organization: {
          facet_values: organizations_map,
          scope: :organization_id,
          select_type: :single
        },
        category: {
          facet_values: categories,
          scope: :categories,
        },
        deployment: {
          facet_values: Devices::Deployment.facet_values,
          scope: :deployment,
          select_type: :single
        },
        model: {
          facet_values: models,
          scope: :models,
        },
        (I18n.t(:device_status_facet).to_sym) =>  {
          facet_values: DeviceState.facet_values,
          scope: :status,
          select_type: :single
        }
      }
  end

  private

  def categories
    @categories ||= DeviceCategory.select(:id, :name).
      map { |e| { e.name => e.id } }.
      reduce(:merge)
  end

  def models
    @models ||= DeviceModel.select(:id, :name).
      map { |e| { e.name => e.id } }.
      reduce(:merge)
  end
end
