class UsersPresenter < Presenters::Base

  delegate :policy, to: :view

  TIMEZONE_OFFSET_REGEX  = /(?<offset>gmt .*(0|5))/i

  #
  # class: 'searchable', # This instructs DataTable that the column should be searchable
  # name: :filter_scope, # This is the scope that should be used for searching, if not searching it is arbitrary.
  # sort_scope: :sort_scope # This is the scope that should be used to determine sort / order
  # table_label: 'Category', # The label that should be applied to the column in the table header.
  # visibility_label: 'Special Category' # This is an optional attribute dictating the label for column visbility.
  #
  def columns
    @columns =
      [
        {
          class: 'searchable sortable sortable-primary',
          name: :name,
          sort_scope: :sort_by_name,
          table_label: 'Name'
        },
        {
          class: 'searchable sortable text-center',
          name: :email,
          sort_scope: :sort_by_email,
          table_label: 'Email'
        },
        {
          class: 'searchable sortable text-center',
          name: :organization,
          sort_scope: :sort_by_organizations_name,
          table_label: 'Organization'
        },
        {
          class: 'searchable sortable text-center',
          name: :timezone,
          sort_scope: :sort_by_timezone_name,
          table_label: 'Time Zone'
        },
        {
          class: 'sortable text-center',
          name: :last_login,
          sort_scope: :sort_by_last_sign_in_at,
          table_label: "Last Login (#{view_context.user_timezone})"
        },
        {
          class: 'searchable sortable text-center',
          name: :status,
          sort_scope: :sort_by_last_sign_in_at,
          table_label: 'Status'
        }
      ]

      if policy(Organization).modify_users?
        @columns << { name: :edit, table_label: 'Edit', class: 'text-center' }
        @columns << { name: :delete, table_label: 'Delete', class: 'text-center' }
      end
    @columns
  end

  def column_data(column)
    result = {}
    result[:direction] = column[:direction] if column[:direction].present? # Sorting Direction
    result[:name] = column[:name] # Used by DataTable to identify column
    result[:sort_scope] = column[:sort_scope] # Used by DataTable to send sorting scope of column
    result
  end


  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets ||=
      {
        name: {
          facet_values: '',
          scope: :with_name_search
        },
        email: {
          facet_values: '',
          scope: :with_email_search
        },
        organization: {
          facet_values: organizations_map,
          scope: :with_organization_id,
        },
        timezone: {
          facet_values: timezones_map,
          scope: :with_timezone_id
        },
        status: {
          facet_values: statuses_map,
          scope: :with_status
        }
      }
  end

  # TODO: Allow custom inputs + selected values for email search
  def email_domains
    @email_domains ||= current_user.users.select(:email).where.not(email: nil).map { |u| u.email.to_s.split('@').last }.uniq.map { |domain| { domain => domain } }.reduce(:merge)
  end

  def timezones_map
    @timezones_map ||= begin
      Timezone.select(:id, :official_name).map do |timezone|
        if match = timezone.official_name.match(TIMEZONE_OFFSET_REGEX)
          { match[:offset] => timezone.id }
        end
      end.reduce(:merge)
    end
  end

  def statuses_map
    @statuses_map ||= [:pending, :active, :inactive].map { |status| { status.capitalize => status } }.reduce(:merge)
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end

  def klass
    :user
  end

  def title
    :users
  end
end
