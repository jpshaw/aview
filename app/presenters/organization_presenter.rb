class OrganizationPresenter < Presenters::Base
  delegate :can_destroy_organization?, to: :view
  attr_reader :organization, :subscription

  def initialize(view_context, attrs)
    super(view_context)
    @organization = attrs[:organization]
    @subscription = attrs[:subscription]
  end

  def show_new_organization?
    view_enabled?(:new_organization)
  end

  def show_logo?
    view_enabled?(:logo)
  end

  def show_roles?
    view_enabled?(:roles)
  end

  def show_permissions?
    view_enabled?(:permissions)
  end

  def show_parent?
    view_enabled?(:edit_parent)
  end

  def show_edit_name?
    view_enabled?(:edit_name)
  end

  def show_edit_service_names?
    Settings.service_names.enabled?
  end

  def show_delete?
    can_destroy_organization?(organization) && view_enabled?(:delete_organization)
  end

  def subscribe_action_id
    "execute-subscribe-#{organization.id}"
  end

  private

  def view_enabled?(name)
    Settings.views.organization.try(name) == true
  end
end