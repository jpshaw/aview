class OrganizationDevicesPresenter < Presenters::Devices::Base
  def initialize(view_context, attrs)
    super(view_context)
    @organization = attrs[:organization]
    extend_facets
  end

  def extend_facets
    facets['include hierarchy'] = {
        facet_values: {yes: 'true', no: 'false'},
        select_type: :single,
        scope: :include_hierarchy
    }
  end

  def title
    "#{@organization.name} Devices"
  end

  def reference_id
    @organization.id
  end

  def class_name
    'organization_devices'.freeze
  end

  def skip_translate?
    true
  end

  def usable_organizations
    @usable_organizations ||= @organization.organizations
  end
end
