class DevicePresenter < DeviceShowPresenter
  delegate :options_from_collection_for_select,
    :can_modify_device_configuration?, to: :view_context

  def show_organization_setting?
    can_modify_device?
  end

  def show_site_setting?
    policy.modify_site?
  end

  def show_hostname_setting?
    Settings.views.device.hostname == true && can_modify_device?
  end

  def show_service_name_setting?
    service_name.present? && can_modify_device?
  end

  def can_modify_device?
    return @can_modify_device unless @can_modify_device.nil?
    @can_modify_device = policy.modify?
    @can_modify_device
  end

  def associated_devices?
    device.device_associations.any?
  end

  def configuration?
    @configuration ||= policy.change_configuration? && device.is_configurable?
  end

  def contact_types
    @contact_types ||= ContactLabels.select_list
  end

  def contacts
    @contact ||= device.contacts
  end

  def countries
    @countries ||= Country.select_list
  end

  def regions
    @regions = Region.select_list(us_country_id)
  end

  def country_ids_with_select
    @countries_with_select ||= Region.country_ids
  end

  def us_country_id
    # Cheap out here since we only support a single country (US) with region pull-down
    @us_country_id ||= country_ids_with_select.first
  end

  def location_region_select_id
    "location_region_select"
  end
  def location_region_text_id
    "location_region_text"
  end

  def gateway_tunnels?
    device.respond_to?(:tunnels) && device.tunnels.present?
  end

  def show_wan_utilization_chart?
    device.is_a? Gateway
  end

  def location
    return @location if defined? @location
    @location = device.location
  end

  def location_has_data?
    location.present? && location.address.present?
  end

  def modem?
    device.cellular_modem_supported? && device.modem.present?
  end

  def networks?
    device.respond_to?(:networks) && device.networks.present?
  end

  def organization
    device.organization
  end

  def rogue_access_points?
    device.respond_to?(:rogue_access_points) && device.rogue_access_points.present?
  end

  def service_name
    @service_name ||= device.organization.service_name
  end

  def settings?
    can_modify_device_configuration?(device)
  end

  def show_hostname?
    Settings.views.device.hostname == true
  end

  def tabs
    tabs = []
    tabs << :device_details
    tabs << :configuration if show_configuration_tab?
    tabs << :configuration if show_gateway_configuration_tab?
    tabs << :rogue_access_points if rogue_access_points?
    tabs << :settings if settings?
    tabs << :associated_devices
    tabs << :cellular_details if modem?
    tabs << :tunnels if tunnels?
    tabs << :networks if networks?
    tabs << :location if current_user.maps_enabled?
    tabs << :contacts
    tabs << :intelliflow if intelliflow?
    tabs << :connectivity_tests if show_connectivity_tests?
    tabs
  end

  def tunnels?
    gateway_tunnels? || uclinux_tunnels?
  end

  def uclinux_tunnels?
    device.respond_to?(:networks) && view_context.network_tunnels(device).present?
  end

  def device_has_location?
    device.has_location?
  end

  def device_has_valid_cellular_location?
    cellular_location && cellular_location.has_valid_map_data?
  end

  def cellular_location
    if device.has_modem?
      @cellular_location ||= device.modem.try(:cellular_location)
    end
  end

  def cellular_latitude
    device_has_valid_cellular_location? ? cellular_location.lat : nil
  end

  def cellular_longitude
    device_has_valid_cellular_location? ? cellular_location.lon : nil
  end

  def device_location_latitude
    device.try(:location).try(:latitude)
  end

  def device_location_longitude
    device.try(:location).try(:longitude)
  end

  def data_plans_for_select
    DataPlan.for_organization(device.organization.id).map{ |plan| [plan.name, plan.id] }
  end

  def data_plan?
    device.cellular_modem_supported?
  end

  def create_data_plan?
    can_modify_organization?(organization)
  end

  def intelliflow?
    device.supports_intelliflow? && Settings.views.device.intelliflow
  end

  def ids?
    device.supports_ids?
  end

  def show_configuration_tab?
    configuration? && individual_configuration_edit?
  end

  def show_connectivity_tests?
    device.has_connectivity_tests?
  end

  def show_gateway_configuration_tab?
    ::ServiceManager.enabled? && device.is_smx_device?
  end

  def group_configuration
    @group_configuration ||= begin
      if device.configuration.present? && !device.configuration.respond_to?(:collection?)
        device.configuration
      elsif device.configuration.present? && device.configuration.collection?
        device.configuration
      elsif device.configuration.present? && device.configuration.individual?
        device.configuration.parent
      end
    end
  end

  def device_configuration_settings
    device.configuration.settings if device.configuration.present?
  end

  def device_configuration_compiled_settings
    device.configuration.compiled_settings if device.configuration.present?
  end

  def group_configuration_is_not_viewable?
    group_configuration.present? && device.organization_id != group_configuration.organization_id
  end

  def firmware_options
    options_from_collection_for_select(compatible_configuration_firmwares,
                                       'id',
                                       'version',
                                       device.configuration.device_firmware_id) if device.configuration.present?
  end

  def compatible_configuration_firmwares
    if device.configuration.present? && device.configuration.device_firmware.present?
      device.configuration.device_firmware.compatible_firmwares(current_user).sort.reverse
    else
      []
    end
  end

  def compatible_parent_configuration_firmware_ids
    if device.configuration.present? && device.configuration.parent.present?
      device.configuration.parent.device_firmware.compatible_firmwares(current_user).pluck(:id)
    else
      []
    end
  end

  def device_configuration_grace_period_duration
    device.configuration.grace_period_duration if device.configuration.present?
  end

  def firmware_inherited?
    return false if device.configuration.nil?
    (device.configuration.parent.present? && !device.configuration.device_firmware_locked) ||
      (device.configuration.parent.nil? && !device.configuration.individual?)
  end

  def grace_period_duration_inherited?
    return false if device.configuration.nil?
    (device.configuration.parent.present? && !device.configuration.grace_period_duration_locked) ||
      (device.configuration.parent.nil? && !device.configuration.individual?)
  end

  def device_configuration_parent_disconnected?
    device.configuration.present? && device.configuration.parent_disconnect
  end

  def parent_settings
    return group_configuration.compiled_settings.to_json if group_configuration.present?
    '{}'
  end

  private

  def individual_configuration_edit?
    Settings.views.device.configuration_inheritance && device.is_platform?(:uc_linux)
  end
end
