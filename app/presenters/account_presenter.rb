class AccountPresenter < Presenters::Base
  def show_password_change?
    Settings.views.account.change_password
  end

  def show_api_access?
    Settings.user_api.enabled?
  end
end
