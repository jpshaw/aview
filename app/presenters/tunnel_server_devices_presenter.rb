class TunnelServerDevicesPresenter < Presenters::Devices::Base

  def title
    "#{tunnel_server.name} Devices"
  end

  def tunnel_server
    @tunnel_server ||= current_user.tunnel_servers.find(params[:id])
  end

  def skip_translate?
    true
  end

  def reference_id
    tunnel_server.try(:id)
  end

end
