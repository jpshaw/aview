class GroupDevicesPresenter < Presenters::Base
  include DataTableable

  def title
    'Group Devices'
  end

  def devices_online
    group.devices.good.count
  end

  def devices_offline
    group.devices.alert.count
  end

  def devices_deployed
    group.devices.deployed.count
  end

  def devices_undeployed
    group.devices.undeployed.count
  end

  def devices_activated
    group.devices.where.not(activated_at: nil).count
  end

  def devices_total
    group.devices.count
  end

  def group
    @group ||= Group.find(params[:id])
  end

  def facets
    @facets ||= {
      name: {
        facet_values: '',
        scope: :with_name_search
      },
      organization: {
        facet_values: organizations_map,
        scope: :with_organization_id
      },
      category: {
        facet_values: categories,
        scope: :with_categories,
      },
      model: {
        facet_values: models,
        scope: :with_models,
      },
      firmware: {
        facet_values: '',
        scope: :with_firmware_search
      }
    }
  end

  def columns
    @columns ||= [
      {
        class: 'text-center th-dark',
        name: :checkbox,
        table_label: check_box_tag("device_ids[]", nil, false, class: 'device-select-all'),
        width: "5%"
      },
      {
        class: 'text-center th-dark',
        name: :with_device_status,
        table_label: 'Status',
        visibility_label: 'Status'
      },
      {
        class: 'searchable sortable',
        name: :with_mac,
        sort_scope: :sort_by_devices_mac,
        table_label: I18n.t(:mac)
      },
      {
        class: 'sortable',
        name: :firmware,
        sort_scope: :sort_by_firmware,
        table_label: 'Firmware'
      },
      {
        class: 'sortable',
        name: :host,
        sort_scope: :sort_by_devices_host,
        table_label: I18n.t(:primary_ip)
      },
      {
        class: 'sortable',
        name: :site,
        sort_scope: :sort_by_sites_name,
        table_label: 'Site'
      },
      {
        class: 'searchable sortable',
        name: :serial,
        sort_scope: :sort_by_devices_serial,
        table_label: 'Serial'
      },
      {
        class: 'sortable sortable-primary',
        direction: 'desc',
        name: :last_heartbeat,
        sort_scope: :sort_by_devices_last_heartbeat_at,
        table_label: "Last Heartbeat (#{view_context.user_timezone})"
      }
    ]
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  private

  def categories
    @categories ||= DeviceCategory.select(:id, :name).map { |e| { e.name => e.id } }.reduce(:merge)
  end

  def models
    @models ||= DeviceModel.select(:id, :name).map { |e| { e.name => e.id } }.reduce(:merge)
  end
end
