class SiteDevicesPresenter < Presenters::Devices::Base
  def initialize(view_context, site)
    super(view_context)
    @site = site
  end

  def reference_id
    @site.id
  end

  def title
    "#{@site.name} Devices"
  end

  def skip_translate?
    true
  end
end
