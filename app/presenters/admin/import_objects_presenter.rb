class Admin::ImportObjectsPresenter < Admin::ImportPresenter

  def objects
    @objects ||= get_objects
  end

  private

  def get_objects
    case view.params[:type]
    when 'device_ids'
      import_objects(:device_ids).map { |mac| link_to(mac, "/devices/#{mac}") }
    when 'skipped_device_ids'
      import_objects(:skipped_device_ids)
    else
      []
    end
  end

end