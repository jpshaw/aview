class Admin::ImportShowPresenter < Admin::ImportPresenter

  def table_header_columns
    OBJECTS.keys.map do |key|
      key.to_s.titleize
    end
  end

  def object_counts
    OBJECTS.map do |name, opts|
      count = import_objects(opts[:key]).count

      if opts[:link] && count > 0
        import_objects_link(count, opts[:key])
      else
        0
      end
    end
  end

  private

  def import_objects_link(name, type)
    link_to(
      name,
      objects_admin_import_path(import, format: :js, type: type),
      remote: true,
       class: 'single-button-remote'
    )
  end

end