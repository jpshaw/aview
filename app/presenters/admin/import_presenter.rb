class Admin::ImportPresenter < Presenters::Base

  delegate :link_to, :objects_admin_import_path, to: :view

  OBJECTS = {
    organizations: {
      key: :customer_ids
    },
    devices: {
      key: :device_ids,
      link: true
    },
    sites: {
      key: :site_ids
    },
    locations: {
      key: :location_ids
    },
    configurations: {
      key: :configuration_ids },
    skipped: {
      key: :skipped_device_ids,
      link: true
    },
  }.with_indifferent_access.freeze

  attr_reader :import

  def initialize(view_context, import)
    @view_context = view_context
    @import       = import
  end

  def import_objects(name)
    import_object_ids.fetch(name, []).compact
  end

  def import_object_ids
    @import_object_ids ||= import.object_ids.with_indifferent_access
  end

end