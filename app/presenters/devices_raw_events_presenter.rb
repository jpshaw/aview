class DevicesRawEventsPresenter < DevicesEventsPresenter
  attr_reader :device, :events, :view_context

  def initialize(view_context, device, events)
    @view_context = view_context
    @device = device
    @events = events
  end

  def most_recent_event_id
    events.first.id if events.present?
  end

  def current_pagination_limit
    current_user.device_events_pagination_limit
  end
end
