module Presenters
  module Devices
    #TODO: Use super for columns once ordering is independent of the listing below
    class Cellular < Presenters::Devices::Netbridge
      def klass
        :cellular
      end

      def title
        :cellular_devices
      end

      def device_models
        @device_models ||= ::Cellular.device_models
      end
    end
  end
end
