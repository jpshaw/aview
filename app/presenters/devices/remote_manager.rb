module Presenters
  module Devices
    #TODO: Use super for columns once ordering is independent of the listing below
    class RemoteManager < Presenters::Devices::Base
      def columns
        @columns =
          [
            {
              class: 'searchable sortable center-children',
              name: :with_device_status,
              sort_scope: :sort_by_device_status,
              table_label: I18n.t(:device_status),
              visibility_label: I18n.t(:device_status)
            },
            {
              class: 'searchable sortable',
              name: :with_mac,
              sort_scope: :sort_by_devices_mac,
              table_label: I18n.t(:mac)
            },
            {
              class: 'init-hidden center-children',
              name: :comment,
              table_label: 'Comment',
              visibility_label: 'Comment'
            },
            {
              class: 'init-hidden',
              name: :serial,
              table_label: 'Serial Number'
            },
            {
              class: 'sortable',
              name: :host,
              sort_scope: :sort_by_devices_host,
              table_label: I18n.t(:primary_ip)
            },
            {
              class: 'init-hidden',
              name: :mgmt_host,
              table_label: 'Management IP'
            },
            {
              class: "#{hostname_class}",
              name: :hostname,
              table_label: 'Hostname'
            },
            {
              class: 'searchable sortable',
              name: :with_models,
              sort_scope: :sort_by_device_models_name,
              table_label: 'Model'
            },
            {
              class: 'sortable',
              name: :with_organization,
              sort_scope: :sort_by_organizations_name,
              table_label: 'Organization',
            },
            {
              class: 'sortable',
              name: :site,
              sort_scope: :sort_by_sites_name,
              table_label: 'Site'
            },
            {
              class: 'sortable',
              name: :firmware,
              sort_scope: :sort_by_devices_firmware,
              table_label: 'Firmware'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :phone_number,
              #sort_scope: :sort_by_,
              table_label: 'Phone Number'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :iccid,
              #sort_scope: :sort_by_,
              table_label: 'ICCID'
            },
            {
              name: :city,
              table_label: 'City'
            },
            {
              class: 'sortable',
              name: :country,
              sort_scope: :sort_by_countries_name,
              table_label: 'Country'
            },
            {
              class: 'sortable init-hidden',
              direction: 'desc',
              name: :activated_at,
              sort_scope: :sort_by_devices_activated_at,
              table_label: "Activation Date (#{view_context.user_timezone})"
            },
            {
              class: 'sortable sortable-primary',
              direction: 'desc',
              name: :last_heartbeat,
              sort_scope: :sort_by_devices_last_heartbeat_at,
              table_label: "Last Heartbeat (#{view_context.user_timezone})"
            }
          ]
      end

      def facets
        super.except(:category)
      end

      def klass
        :remote_manager
      end

      def title
        :remote_manager_devices
      end

      def device_models
        @device_models ||= ::RemoteManager.device_models
      end
    end
  end
end
