module Presenters
  module Devices
    class Base < Presenters::Base
      include DataTableable
      include DeviceActionable

      def columns
        @columns =
          [
            # {
            #    class: 'searchable', # This instructs DataTable that the column should be searchable
            #    name: :filter_scope, # This is the scope that should be used for searching, if not searching it is arbitrary.
            #    sort_scope: :sort_scope # This is the scope that should be used to determine sort / order
            #    table_label: 'Category', # The label that should be applied to the column in the table header.
            #    visibility_label: 'Special Category' # This is an optional attribute dictating the label for column visbility.
            # }
            {
              class: 'searchable sortable center-children center-children',
              name: :with_device_status,
              sort_scope: :sort_by_device_status,
              table_label: I18n.t(:device_status),
              visibility_label:  I18n.t(:device_status)
            },
            {
              class: 'searchable sortable',
              name: :with_mac,
              sort_scope: :sort_by_devices_mac,
              table_label: I18n.t(:mac)
            },
            {
              class: 'init-hidden center-children',
              name: :comment,
              table_label: 'Comment',
              visibility_label: 'Comment'
            },
            {
              class: 'init-hidden',
              name: :serial,
              table_label: 'Serial Number'
            },
            {
              class: 'sortable',
              name: :host,
              sort_scope: :sort_by_devices_host,
              table_label: I18n.t(:primary_ip)
            },
            {
              class: 'init-hidden',
              name: :mgmt_host,
              table_label: 'Management IP'
            },
            {
              class: "#{hostname_class}",
              name: :hostname,
              table_label: 'Hostname'
            },
            {
              class: 'searchable sortable',
              name: :with_models,
              sort_scope: :sort_by_device_models_name,
              table_label: 'Model'
            },
            {
              class: 'sortable',
              name: :with_organization,
              sort_scope: :sort_by_organizations_name,
              table_label: 'Organization',
            },
            {
              class: 'sortable',
              name: :site,
              sort_scope: :sort_by_sites_name,
              table_label: 'Site'
            },
            {
              class: 'sortable',
              name: :firmware,
              sort_scope: :sort_by_devices_firmware,
              table_label: 'Firmware'
            },
            {
              name: :city,
              table_label: 'City'
            },
            {
              class: 'sortable',
              name: :country,
              sort_scope: :sort_by_countries_name,
              table_label: 'Country'
            },
            {
              class: 'sortable init-hidden',
              direction: 'desc',
              name: :activated_at,
              sort_scope: :sort_by_devices_activated_at,
              table_label: "Activation Date (#{view_context.user_timezone})"
            },
            {
              class: 'sortable sortable-primary',
              direction: 'desc',
              name: :last_heartbeat,
              sort_scope: :sort_by_devices_last_heartbeat_at,
              table_label: "Last Heartbeat (#{view_context.user_timezone})"
            }
          ]
      end

      def facets
        @facets ||=
          {
            mac: {
              facet_values: '',
              scope: :with_mac_search
            },
            organization: {
              facet_values: organizations_map,
              scope: :with_organization_id,
            },
            category: {
              facet_values: DeviceCategory. # TODO: Rails 4 -> DeviceCategory.select(:id, :name).map{...}
              find_by_sql('select id, name from device_categories').
                map { |e| { e.name => e.id } }.
                reduce(:merge),
              scope: :with_categories,
            },
            deployment: {
              facet_values: ::Devices::Deployment.facet_values,
              scope: :with_deployment,
              select_type: :single
            },
            model: {
              facet_values: device_models_map,
              scope: :with_models,
            },
            (I18n.t(:device_status_facet).to_sym) => {
              facet_values: DeviceState.facet_values,
              scope: :with_status,
              select_type: :single
            },
            site: {
              facet_values: '',
              scope: :with_site_name_search
            },
            firmware: {
              facet_values: '',
              scope: :with_firmware_search
            },
            hostname: {
              facet_values: '',
              scope: :with_hostname_search
            }
          }
      end

      def device_models
        @device_models ||= ::DeviceModel.all
      end

      def device_models_map
        @device_models_map ||= device_models.select(:id, :name).map { |m| { m.name => m.id } }.reduce(:merge)
      end

      def klass
        :device
      end

      def title
        :devices
      end

      def skip_translate?
        false
      end

      def visibility_label(column)
        return column[:visibility_label] || column[:table_label]
      end

      def class_name
        @class_name ||= self.class.name.split('::').last.underscore.parameterize('_')
      end

      def data_table_class
        @data_table_class ||= class_name == :base ? :base : class_name.gsub('_presenter', '') + '_data_table'
      end

      def reference_id
        nil
      end

      def show_hostname?
        Settings.views.device.hostname == true
      end

      def hostname_class
        @hostname_class ||= show_hostname? ? '' : 'init-hidden'
      end
    end
  end
end
