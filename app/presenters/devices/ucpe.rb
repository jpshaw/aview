module Presenters
  module Devices
    class Ucpe < Presenters::Devices::Base
      def facets
        super.except(:category)
      end

      def klass
        :ucpe
      end

      def title
        "#{DeviceCategory::UCPE_CATEGORY} Devices"
      end

      def skip_translate?
        true
      end

      def device_models
        @device_models ||= ::Ucpe.device_models
      end
    end
  end
end
