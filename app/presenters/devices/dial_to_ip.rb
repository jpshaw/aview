module Presenters
  module Devices
    class DialToIp < Presenters::Devices::Base
      def facets
        super.except(:category)
      end

      def klass
        :dialToIp
      end

      def title
        "#{DeviceCategory::DIAL_TO_IP_CATEGORY} Devices"
      end

      def skip_translate?
        true
      end

      def device_models
        @device_models ||= ::DialToIp.device_models
      end

      def show_move_to_data_plan_action?
        false
      end
    end
  end
end
