module Presenters
  module Devices
    class Netreach < Presenters::Devices::Base
      def facets
        super.except(:category)
      end

      def klass
        :netreach
      end

      def title
        :wifi_devices
      end

      def device_models
        @device_models ||= ::Netreach.device_models
      end

      def show_move_to_data_plan_action?
        false
      end
    end
  end
end
