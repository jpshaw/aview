module Presenters
  module Devices
    #TODO: Use super for columns once ordering is independent of the listing below
    class Netbridge < Presenters::Devices::Base
      def columns
        @columns =
          [
            {
              class: 'searchable sortable center-children',
              name: :with_device_status,
              sort_scope: :sort_by_device_status,
              table_label: I18n.t(:device_status),
              visibility_label: I18n.t(:device_status)
            },
            {
              class: 'searchable sortable',
              name: :with_mac,
              sort_scope: :sort_by_devices_mac,
              table_label: I18n.t(:mac)
            },
            {
              class: 'init-hidden center-children',
              name: :comment,
              table_label: 'Comment',
              visibility_label: 'Comment'
            },
            {
              class: 'init-hidden',
              name: :serial,
              table_label: 'Serial Number'
            },
            {
              class: 'sortable',
              name: :host,
              sort_scope: :sort_by_devices_host,
              table_label: I18n.t(:primary_ip)
            },
            {
              class: 'init-hidden',
              name: :mgmt_host,
              table_label: 'Management IP'
            },
            {
              class: "#{hostname_class}",
              name: :hostname,
              table_label: 'Hostname'
            },
            {
              class: 'searchable sortable',
              name: :with_models,
              sort_scope: :sort_by_device_models_name,
              table_label: 'Model'
            },
            {
              class: 'sortable',
              name: :with_organization,
              sort_scope: :sort_by_organizations_name,
              table_label: 'Organization',
            },
            {
              class: 'sortable',
              name: :site,
              sort_scope: :sort_by_sites_name,
              table_label: 'Site'
            },
            {
              class: 'sortable',
              name: :firmware,
              sort_scope: :sort_by_devices_firmware,
              table_label: 'Firmware'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :carrier,
              #sort_scope: :sort_by_,
              table_label: 'Carrier'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :dbm,
              #sort_scope: :sort_by_,
              table_label: 'dBm'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :signal,
              #sort_scope: :sort_by_,
              table_label: 'Signal'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :network,
              #sort_scope: :sort_by_,
              table_label: 'Network'
            },
            {
              #class: 'sortable', TODO: Dependent on AV-1673
              name: :phone_number,
              #sort_scope: :sort_by_,
              table_label: 'Phone Number'
            },
            {
              name: :city,
              table_label: 'City'
            },
            {
              class: 'sortable',
              name: :country,
              sort_scope: :sort_by_countries_name,
              table_label: 'Country'
            },
            {
              class: 'sortable init-hidden',
              direction: 'desc',
              name: :activated_at,
              sort_scope: :sort_by_devices_activated_at,
              table_label: "Activation Date (#{view_context.user_timezone})"
            },
            {
              class: 'sortable sortable-primary',
              direction: 'desc',
              name: :last_heartbeat,
              sort_scope: :sort_by_devices_last_heartbeat_at,
              table_label: "Last Heartbeat (#{view_context.user_timezone})"
            },
            {
              class: 'init-hidden',
              name: :configuration,
              table_label: "Configuration"
            },
            {
              class: 'init-hidden',
              name: :client_mac,
              table_label: "Client #{I18n.t(:mac)}"
            },
            {
              class: 'init-hidden',
              name: :last_restart,
              table_label: 'Last Restart'
            },
            {
              class: 'init-hidden',
              name: :apn,
              table_label: 'APN'
            },
            {
              class: 'init-hidden',
              name: :band,
              table_label: 'Band'
            },
            {
              class: 'init-hidden',
              name: :cid,
              table_label: 'CID'
            },
            {
              class: 'init-hidden',
              name: :ecio,
              table_label: 'Ec/Io'
            },
            {
              class: 'init-hidden',
              name: :esn,
              table_label: 'ESN'
            },
            {
              name: :iccid,
              table_label: 'ICCID'
            },
            {
              class: 'init-hidden',
              name: :imei,
              table_label: 'IMEI'
            },
            {
              class: 'init-hidden',
              name: :imsi,
              table_label: 'IMSI'
            },
            {
              class: 'init-hidden',
              name: :lac,
              table_label: 'LAC'
            },
            {
              class: 'init-hidden',
              name: :mcc,
              table_label: 'MCC'
            },
            {
              class: 'init-hidden',
              name: :mnc,
              table_label: 'MNC'
            },
            {
              class: 'init-hidden',
              name: :modem,
              table_label: 'Modem'
            },
            {
              class: 'init-hidden',
              name: :rsrp,
              table_label: 'RSRP'
            },
            {
              class: 'init-hidden',
              name: :rsrq,
              table_label: 'RSRQ'
            },
            {
              class: 'init-hidden',
              name: :rssi,
              table_label: 'RSSI'
            },
            {
              class: 'init-hidden',
              name: :sku,
              table_label: 'SKU'
            },
            {
              class: 'init-hidden',
              name: :sinr,
              table_label: 'SiNR'
            },
            {
              class: 'init-hidden',
              name: :snr,
              table_label: 'SNR'
            },
            {
              class: 'init-hidden',
              name: :usb_speed,
              table_label: 'USB Speed'
            },
            {
              class: 'init-hidden',
              name: :version,
              table_label: 'Version'
            }
          ]
      end

      def facets
        super.except(:category)
      end

      def klass
        :netbridge
      end

      def title
        :legacy_cellular_devices
      end

      def device_models
        @device_models ||= ::Netbridge.device_models
      end
    end
  end
end
