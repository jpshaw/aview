class SearchesIndexPresenter < Presenters::Base
  include DataTableable

  delegate :h, :pluralize, to: :view_context

  def initialize(view_context, search)
    @search = search
    super(view_context)
  end

  def columns
    @columns =
      [
        {
          class: 'sortable',
          name: :with_device_status,
          sort_scope: :sort_by_device_status,
          table_label: '',
          visibility_label: 'Status'
        },
        {
          class: 'sortable',
          name: :with_mac,
          sort_scope: :sort_by_devices_mac,
          table_label: I18n.t(:mac)
        },
        {
          class: 'sortable',
          name: :host,
          sort_scope: :sort_by_devices_host,
          table_label: I18n.t(:primary_ip)
        },
        {
          name: :hostname,
          table_label: 'Hostname'
        },
        {
          class: 'sortable',
          name: :with_categories,
          sort_scope: :sort_by_device_categories_name,
          table_label: 'Category',
        },
        {
          class: 'sortable',
          name: :with_models,
          sort_scope: :sort_by_device_models_name,
          table_label: 'Model'
        },
        {
          class: 'sortable',
          name: :with_organization,
          sort_scope: :sort_by_organizations_name,
          table_label: 'Organization',
        },
        {
          class: 'sortable',
          name: :site,
          sort_scope: :sort_by_sites_name,
          table_label: 'Site'
        },
        {
          class: 'sortable',
          name: :firmware,
          sort_scope: :sort_by_devices_firmware,
          table_label: 'Firmware'
        },
        {
          name: :city,
          table_label: 'City'
        },
        {
          class: 'sortable',
          name: :country,
          sort_scope: :sort_by_countries_name,
          table_label: 'Country'
        },
        {
          class: 'sortable init-hidden',
          direction: 'desc',
          name: :activated_at,
          sort_scope: :sort_by_devices_activated_at,
          table_label: "Activation Date (#{view_context.user_timezone})"
        },
        {
          class: 'sortable sortable-primary',
          direction: 'desc',
          name: :last_heartbeat,
          sort_scope: :sort_by_devices_last_heartbeat_at,
          table_label: "Last Heartbeat (#{view_context.user_timezone})"
        }
      ]
  end
end
