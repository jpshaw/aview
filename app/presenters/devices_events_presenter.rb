class DevicesEventsPresenter < DeviceShowPresenter
  def column_data(column)
    result = {}
    result[:direction] = column[:direction] if column[:direction].present? # Sorting Direction
    result[:name] = column[:name] # Used by DataTable to identify column
    result[:sort_scope] = column[:sort_scope] # Used by DataTable to send sorting scope of column
    result[:width] = column[:width] if column[:width].present?
    result
  end

  def columns
    @columns =
      [
        # {
        #    class: 'searchable', # This instructs DataTable that the column should be searchable
        #    name: :filter_scope, # This is the scope that should be used for searching, if not searching it is arbitrary.
        #    sort_scope: :sort_scope # This is the scope that should be used to determine sort / order
        #    table_label: 'Category', # The label that should be applied to the column in the table header.
        #    visibility_label: 'Special Category' # This is an optional attribute dictating the label for column visbility.
        # }
        {
          class: 'sortable-primary',
          direction: 'desc',
          sort_scope: :sort_by_created_at,
          name: :created_at,
          table_label: 'Created'
        },
        {
          class: 'searchable',
          name: :type,
          table_label: 'Type'
        },
        {
          class: 'searchable',
          name: :level,
          table_label: 'Level'
        },
        {
          name: :information,
          table_label: 'Information',
          width: '70%'
        },
      ]
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets =
      {
        type: {
          facet_values: Hash[*EventTypes.select_list.flatten],
          scope: :with_type
        },
        level: {
          facet_values: Hash[*EventLevels.select_list.flatten],
          scope: :with_level
        },
        range: {
          facet_values: { "Last Day" => 1.day.ago, "Last Week" => 1.week.ago, "Last Month" => 1.month.ago },
          scope: :created_since
        }
      }
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end
end
