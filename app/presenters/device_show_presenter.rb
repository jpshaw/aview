class DeviceShowPresenter < Presenters::Base
  attr_reader :view_context, :device
  delegate :current_organization, :edit_organization_path,
    :can_modify_organization?, :can_modify_device_contacts?,
    :timestamp_in_users_timezone, :link_to, :site_path, to: :view_context

  def initialize(view_context, device)
    @view_context = view_context
    @device = device
  end

  def can_add_contact?
    !device.is_smx_device? || device.contacts.empty?
  end

  def can_change_location?
    if device.is_smx_device? && !ServiceManager.enabled?
      false
    else
      policy.change_location?
    end
  end

  def policy
    @policy ||= Pundit.policy(current_user, device)
  end

  def show_commands?
    policy.send_commands? && @device.can_send_commands?
  end

  def show_cellular_reports?
    device.cellular_modem_supported?
  end

  def profiles
    @profiles ||= current_user.notification_profiles
  end

  def sms?
    policy.send_commands? && device.has_modem? && device.can_receive_sms_commands?
  end

  def notification_subscription
    @notification_subscription ||= current_user.notification_subscriptions.build
  end

  def subscription
    @subscription ||= current_user.notification_subscriptions.devices.where(publisher_id: device.id).first
  end

  def subtitle
    "#{device.category_name} #{device.series_name}"
  end

  def header_title
    "#{title} #{site_title}".html_safe
  end

  def title
    "#{I18n.t(:mac)}: #{device.mac.upcase}"
  end

  def site_title
    device.site.present? ? " / Site ID: #{site_link}" : ""
  end

  def site_link
    link_to(device.site_name, site_path(device.site), id: 'device-site-title-link')
  end

  def show_vig_report?
    device.category_name == DeviceCategory::GATEWAY_CATEGORY
  end

  def over_report_limit?
    current_user.reports.size >= DeviceReport::REPORT_LIMIT
  end

  def show_individual_data_limit?
    device.data_plan.individual_data_limit.present?
  end

  def configuration_checked_at
    return 'N/A' unless device.configuration_checked_at.present?
    if device.is_smx_device?
      device.configuration_checked_at.strftime("%b %d %Y %T")
    else
      timestamp_in_users_timezone(device.configuration_checked_at)
    end
  end

  def data_plan
    if device.data_plan.blank?
      'N/A'
    elsif can_modify_organization?(device.organization)
      link_to device.data_plan.name, edit_organization_path(device.organization, anchor: 'data-plans')
    else
      device.data_plan.name
    end
  end

  def data_plan_individual_limit
    return 'N/A' unless device.data_plan.individual_data_limit
    Filesize.as_si(device.data_plan.individual_data_limit).pretty
  end

  def data_used
    Filesize.as_si(device.data_used).pretty
  end

  def data_usage_highlight
    return '' unless device.data_plan.individual_data_limit.present? && device.data_used.present?
    'highlight-alert' if device.data_used > device.data_plan.individual_data_limit
  end

  def subscribe_action_id
    "execute-subscribe-#{device.id}"
  end

  def show_wan_test_command?
    device.supports_wan_test_command?
  end

  def show_wan_toggle_command?
    device.supports_wan_toggle_command?
  end
end
