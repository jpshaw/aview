class SitesPresenter < Presenters::Base
  include DataTableable

  def columns
    @columns =
      [
        # {
        #    class: 'searchable', # This instructs DataTable that the column should be searchable
        #    name: :filter_scope, # This is the scope that should be used for searching, if not searching it is arbitrary.
        #    sort_scope: :sort_scope # This is the scope that should be used to determine sort / order
        #    table_label: 'Category', # The label that should be applied to the column in the table header.
        #    visibility_label: 'Special Category' # This is an optional attribute dictating the label for column visbility.
        # }
        {
          class: 'sortable sortable-primary',
          name: :with_names,
          sort_scope: :sort_by_name,
          table_label: 'Name'
        },
        {
          class: 'searchable sortable text-center',
          name: :with_organization,
          sort_scope: :sort_by_organizations_name,
          table_label: 'Organization'
        },
        {
          class: 'sortable text-center',
          name: :site_devices_count,
          sort_scope: :sort_by_device_count,
          table_label: 'Devices'
        }
      ]

      if view_context.can_edit?(Site)
        @columns << {
          class: 'sortable text-center',
          name: :updated_at,
          sort_scope: :sort_by_updated_at,
          table_label: "Updated (#{view_context.user_timezone})"
        }
      end

      @columns << {
        class: 'sortable text-center',
        name: :created_at,
        sort_scope: :sort_by_created_at,
        table_label: "Created (#{view_context.user_timezone})"
      }

      if view_context.can_edit?(Site)
        @columns << { name: :edit,
                      table_label: 'Edit',
                      class: 'text-center'
                    }
        @columns << {
                      name: :delete,
                      table_label: 'Delete',
                      class: 'text-center'
                    }
      end
    @columns
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets ||=
      {
        name: {
          facet_values: '',
          scope: :with_name_search,
        },
        organization: {
          facet_values: organizations_map,
          scope: :with_organization_id
        }
      }
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end

  def klass
    :site
  end

  def title
    :sites
  end
end
