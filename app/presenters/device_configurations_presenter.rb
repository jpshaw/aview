class DeviceConfigurationsPresenter < Presenters::Base
  include DataTableable

  SKIP_TRANSLATE_CATEGORIES = [ DeviceCategory::UCPE_CATEGORY,
                                DeviceCategory::DIAL_TO_IP_CATEGORY ]

  def initialize(view_context, category)
    @category = category
    super(view_context)
  end

  def category_id
    @category.id
  end

  def columns
    @columns ||=
        [
            {
                class: 'sortable',
                name: :with_name,
                sort_scope: :sort_by_name,
                table_label: 'Name'
            },
            {
                class: 'sortable text-center',
                name: :with_organization,
                sort_scope: :sort_by_organizations_name,
                table_label: 'Organization'
            },
            {
                class: 'sortable text-center',
                name: :with_device_category,
                sort_scope: :sort_by_device_categories_name,
                table_label: 'Category'
            },
            {
                class: 'sortable text-center',
                name: :with_device_model,
                sort_scope: :sort_by_device_models_name,
                table_label: 'Model'
            },
            {
                class: 'sortable text-center',
                name: :firmware_version,
                sort_scope: :sort_by_device_firmware,
                table_label: 'Firmware Version'
            },
            {
                class: 'sortable sortable-primary text-center',
                direction: 'desc',
                name: :sort_by_updated_at,
                sort_scope: :sort_by_updated_at,
                table_label: 'Updated'
            },
            {
                class: 'text-center',
                name: :updated_by,
                table_label: 'Updated By'
            },
            {
                class: 'text-center',
                name: :edit,
                table_label: 'Edit'
            },
            {
                class: 'text-center',
                name: :delete,
                table_label: 'Delete'
            }
        ]
  end

  def title
    @category.name + ' Configurations'
  end

  def skip_translate?
    SKIP_TRANSLATE_CATEGORIES.include?(@category.name)
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def facets
    @facets ||=
      {
        name: {
          facet_values: '',
          scope: :with_name_search
        },
        organization: {
          facet_values: organizations_map,
          scope: :with_organization_id
        },
        model: {
          facet_values: models_map,
          scope: :with_model_id
        },
        firmware: {
          facet_values: '',
          scope: :with_firmware_search
        }
      }
  end

  def models_map
    @models_map ||= @category.models.select(:id, :name).map { |m| { m.name => m.id } }.reduce(:merge)
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end
end
