class DataPlanShowPresenter < Presenters::Devices::Base
  def initialize(view_context, data_plan)
    super(view_context)
    @data_plan = data_plan
  end

  def columns
    @columns = super << {
          class: 'sortable sortable-primary',
          direction: 'desc',
          name: :data_used,
          sort_scope: :sort_by_device_modems_data_used,
          table_label: 'Data Used'
    }
  end

  def title
    "#{@data_plan.name} Devices"
  end

  def class_name
    'data_plan_devices'.freeze
  end

  def reference_id
    @data_plan.id
  end

  def data_plans_for_select
    DataPlan.for_organization(@data_plan.organization).map{ |plan| [plan.name, plan.id] }
  end

  def show_organization_selection?
    false
  end

  def skip_translate?
    true
  end
end
