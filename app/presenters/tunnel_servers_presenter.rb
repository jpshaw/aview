class TunnelServersPresenter < Presenters::Base

  def skip_translate?
    false
  end

  def columns
    @columns ||= [
      {
        class: 'searchable sortable sortable-primary',
        name: :name,
        sort_scope: :sort_by_name,
        table_label: 'Name'
      },
      {
        class: 'text-center',
        name: :device_count,
        table_label: 'Devices'
      },
      {
        class: 'text-center',
        name: :ip_addresses,
        table_label: 'IP Addresses'
      },
      {
        class: 'text-center',
        name: :type,
        table_label: 'Type'
      },
      {
        class: 'searchable sortable text-center',
        name: :location,
        sort_scope: :sort_by_location,
        table_label: 'Location'
      },
    ]
  end

  def column_data(column)
    result = {}
    result[:direction] = column[:direction] if column[:direction].present? # Sorting Direction
    result[:name] = column[:name] # Used by DataTable to identify column
    result[:sort_scope] = column[:sort_scope] # Used by DataTable to send sorting scope of column
    result
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  def visibility_label(column)
    return column[:visibility_label] || column[:table_label]
  end

  def facets
    @facets ||=
      {
        name: {
          facet_values: '',
          scope: :with_name_search
        },
        location: {
          facet_values: '',
          scope: :with_location_search
        },
      }
  end

  def klass
    :tunnel_server
  end

  def title
    :tunnel_servers
  end
end
