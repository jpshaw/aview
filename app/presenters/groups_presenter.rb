class GroupsPresenter < Presenters::Base
  include DataTableable

  def title
    'Groups'
  end

  def facets
    @facets ||= {
      name: {
        facet_values: '',
        scope: :with_name_search
      },
      organization: {
        facet_values: organizations_map,
        scope: :with_organization_id
      },
      category: {
        facet_values: categories,
        scope: :with_categories,
      },
      model: {
        facet_values: models,
        scope: :with_models,
      },
      firmware: {
        facet_values: '',
        scope: :with_firmware_search
      }
    }
  end

  def columns
    @columns ||= [
      {
        class: 'text-center th-dark',
        name: :checkbox,
        table_label: check_box_tag("group_ids[]", nil, false, class: 'group-select-all'),
        width: "5%"
      },
      {
        class: 'searchable sortable',
        name: :name,
        direction: 'asc',
        sort_scope: :sort_by_name,
        table_label: 'Name'
      },
      {
        class: 'searchable sortable',
        name: :organization,
        sort_scope: :sort_by_organizations_name,
        table_label: 'Organization'
      },
      {
        class: 'searchable sortable text-center',
        name: :category,
        sort_scope: :sort_by_device_categories_name,
        table_label: 'Category'
      },
      {
        class: 'searchable sortable text-center',
        name: :model,
        sort_scope: :sort_by_device_models_name,
        table_label: 'Model'
      },
      {
        class: 'searchable sortable text-center',
        name: :firmware,
        sort_scope: :sort_by_firmware_name,
        table_label: 'Firmware'
      },
      {
        class: 'th-dark text-center',
        name: :device_count,
        table_label: 'Devices'
      },
      {
        class: 'th-dark text-center',
        name: :online_devices,
        table_label: 'Online'
      }
    ]
  end

  def config_options
    {
      column_visibility: {
        breakpoint: columns.size - ( columns.size / 2 ),
        size: columns.size
      }
    }
  end

  private

  def categories
    @categories ||= DeviceCategory.for_groups.select(:id, :name).map { |e| { e.name => e.id } }.reduce(:merge)
  end

  def models
    @models ||= DeviceModel.where(category_id: categories.values).select(:id, :name).map { |e| { e.name => e.id } }.reduce(:merge)
  end
end
