class AdminControllerPolicy < ApplicationPolicy
  def view?
    admin_panel_enabled? &&
      root_user? &&
      user_has_ability?('View Admin Panel')
  end

  private

  def admin_panel_enabled?
    Settings.admin.present? && Settings.admin.enabled == true
  end
end
