class DeviceEventPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.device_events
    end
  end

end
