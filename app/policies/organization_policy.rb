class OrganizationPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      user.permitted_resources_of_type(self.scope)
    end
  end

  def modify?
    user_has_ability? 'Modify Organizations'
  end
  alias_method :new?,    :modify?
  alias_method :create?, :modify?
  alias_method :edit?,   :modify?

  def destroy?
    modify? && !current_user_organization? && !record.root?
  end

  def modify_users?
    return false unless Settings.users.enabled == true
    user_has_ability? 'Modify Users'
  end
  alias_method :create_users?, :modify_users?

  def current_user_organization?
    user.organization.eql?(record)
  end
end
