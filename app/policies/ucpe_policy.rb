class UcpePolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :ucpe)
    end
  end

end
