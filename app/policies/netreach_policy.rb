class NetreachPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :netreach)
    end
  end

end
