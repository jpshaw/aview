class DeviceModelPolicy < ApplicationPolicy
  def index?
    root_user?
  end
end