class GatewayPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :gateway)
    end
  end

  def change_organization?
    false
  end

end
