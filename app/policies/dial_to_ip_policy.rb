class DialToIpPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :dial_to_ip)
    end
  end

end
