class NetbridgePolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :netbridge)
    end
  end

end
