class DeviceFirmwarePolicy < ApplicationPolicy
  def index?
    root_user?
  end

  def assign_non_production?
    user.has_ability?('Assign Non-Production Firmwares')
  end

end
