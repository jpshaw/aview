class EmbeddedCellularPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :embedded_cellular)
    end
  end

end
