class GroupPolicy < ApplicationPolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.groups
    end
  end

  def modify?
    return @modify unless @modify.nil?
    @modify = has_ability_from_organization?(Ability::MODIFY_GROUPS)
  end

end
