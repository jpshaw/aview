class NetbridgeConfigurationPolicy < ConfigurationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      user.netbridge_configurations
    end
  end

end
