class ConfigurationPolicy < ApplicationPolicy

  def modify?
    has_ability_from_organization?('Modify Configurations')
  end

  def view_history?
    has_ability_from_organization?('View Configuration History')
  end

  alias_method :copy?, :modify?

end
