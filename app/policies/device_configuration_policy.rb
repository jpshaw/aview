class DeviceConfigurationPolicy < ApplicationPolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.device_configurations
    end
  end

  def modify?
    has_ability_from_organization?('Modify Device Configurations')
  end

  alias_method :show?,                  :modify?
  alias_method :new?,                   :modify?
  alias_method :create?,                :modify?
  alias_method :edit?,                  :modify?
  alias_method :update?,                :modify?
  alias_method :destroy?,               :modify?
  alias_method :edit_linked_devices?,   :modify?
  alias_method :update_linked_devices?, :modify?
  alias_method :copy?,                  :modify?

end
