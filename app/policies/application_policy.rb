class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user   = user
    @record = record
  end

  # Public: Checks if the user has the given ability on the record. If the record
  # is an ActiveRecord object, it will check if the user has the ability on it -
  # otherwise, it will check if the user has the ability for any of their
  # manageable organizations.
  #
  # Examples
  #
  #   # The `user` has the ability on `@organization`.
  #   policy = Pundit.policy(user, @organization)
  #   policy.user_has_ability?('Modify Organization')
  #   => true
  #
  #   policy = Pundit.policy(user, Organization)
  #   policy.user_has_ability?('Modify Organization')
  #   => true
  #
  #   # The `user` does not have the ability on any organization.
  #   policy = Pundit.policy(user, @organization)
  #   policy.user_has_ability?('Modify Organization')
  #   => false
  #
  #   policy = Pundit.policy(user, Organization)
  #   policy.user_has_ability?('Modify Organization')
  #   => false
  #
  # Returns true if the user has the given ability on the record, or has the
  # ability on any records with the same type; otherwise false.
  def user_has_ability?(ability)
    if record.is_a? ActiveRecord::Base
      user.has_ability_on?(record, to: ability)
    else
      user.has_ability_for_any_organization_to?(ability)
    end
  end

  def has_ability_from_organization?(ability)
    if record.respond_to? :organization
      user.has_ability_on?(record.organization, to: ability)
    else
      user.has_ability_for_any_organization_to?(ability)
    end
  end

  def index?
    false
  end

  def show?
    scope.where(:id => record.id).exists?
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def root_user?
    user.all_organizations.roots.exists?
  end

  ### Engine policy hooks

  def engine_policy_exists?(policy)
    engine_policy_class_defined? && engine_policy_class.method_defined?(policy)
  end

  def engine_policy(policy)
    engine_policy_class.new(user).send(policy)
  end

  def engine_policy_class
    engine_policy_class_name.constantize
  end

  def engine_policy_class_name
    'EnginePolicy'
  end

  def engine_policy_class_defined?
    Object.const_defined?(engine_policy_class_name)
  end

  private

  # Checks if the policy is defined for the current engine. If so, it will
  # always call the engine policy. If not, the given block will be called as the
  # default policy. If the policy is not found for the engine and no block is
  # given, then an exception will be thrown.
  def try_engine_policy(policy, &block)
    if engine_policy_exists?(policy)
      engine_policy(policy)
    else
      raise 'A block must be given for default policy values' unless block_given?
      block.call
    end
  end
end
