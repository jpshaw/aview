class UsbCellularPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :usb_cellular)
    end
  end

end
