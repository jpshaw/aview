class Admin::ImportsControllerPolicy < ApplicationPolicy
  def view?
    Settings.imports.present? && Settings.imports.enabled == true
  end
end
