class RemoteManagerPolicy < DevicePolicy

  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices(type: :remote_manager)
    end
  end

end
