class UserPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      user.users
    end
  end

  def modify?
    has_ability_from_organization?('Modify Users')
  end
  alias_method :show?,    :modify?
  alias_method :new?,     :modify?
  alias_method :create?,  :modify?
  alias_method :edit?,    :modify?
  alias_method :update?,  :modify?
  alias_method :destroy?, :modify?
end
