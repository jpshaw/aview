class SitePolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      user.sites
    end
  end

  def modify?
    has_ability_from_organization?('Modify Sites')
  end

  alias_method :edit?,   :modify?
  alias_method :update?, :modify?
  alias_method :delete?, :modify?
end
