class DevicePolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      user.devices
    end
  end

  def modify?
    return @modify unless @modify.nil?
    @modify = has_ability_from_organization?('Modify Devices')
    @modify
  end

  def modify_site?
    try_engine_policy(:modify_device_site?) { modify? }
  end

  def modify_comment?
    try_engine_policy(:modify_device_comment?) { modify? }
  end

  alias :edit?            :modify?
  alias :remote?          :modify?

  def change_location?
    modify? || has_ability_from_organization?('Modify Device Location')
  end

  def replace?
    has_ability_from_organization?('Replace Devices')
  end

  def import?
    has_ability_from_organization?('Import Devices')
  end

  def change_organization?
    try_engine_policy(:modify_device_organization?) { modify? }
  end

  def send_commands?
    has_ability_from_organization?('Send Device Commands')
  end

  # TODO: Combine configuration abilities into one.
  def change_configuration?
    has_ability_from_organization?('Modify Configurations') ||
      has_ability_from_organization?('Modify Device Configurations')
  end

  def deploy?
    has_ability_from_organization?('Deploy Devices')
  end

  def modify_hosts?
    try_engine_policy(:modify_device_hosts?) { modify? }
  end

  def modify_contacts?
    try_engine_policy(:modify_device_contacts?) { modify? }
  end
end
