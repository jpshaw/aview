class AddModifyFooterAbility < SeedMigration::Migration

  def up
    ability     = Ability.create(name: 'Modify Footer', description: 'modify footer')
    admin_role  = Organization.root.admin_role
    admin_role.abilities << ability unless admin_role.abilities.include?(ability)
  end

  def down
    Ability.find_by_name('Modify Footer').destroy
  end

end
