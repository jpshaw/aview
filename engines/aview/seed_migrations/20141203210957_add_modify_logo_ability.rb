class AddModifyLogoAbility < SeedMigration::Migration

  def up
    ability     = Ability.create(name: 'Modify Logo', description: 'modify logo')
    admin_role  = Organization.root.admin_role
    admin_role.abilities << ability unless admin_role.abilities.include?(ability)
  end

  def down
    Ability.find_by_name('Modify Logo').destroy
  end

end
