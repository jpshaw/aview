class AddViewAdminPanelAbilityToRootOrganizationAdminRole < SeedMigration::Migration

  def up
    if ability = Ability.find_by_name('View Admin Panel')
      admin_role = Organization.roots.first.admin_role
      admin_role.abilities << ability unless admin_role.abilities.include?(ability)
    end
  end

  def down
    if ability = Ability.find_by_name('View Admin Panel')
      admin_role = Organization.roots.first.admin_role
      admin_role.role_abilities.where(ability_id: ability.id).destroy_all
    end
  end

end
