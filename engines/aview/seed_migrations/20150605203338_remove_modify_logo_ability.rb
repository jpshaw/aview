class RemoveModifyLogoAbility < SeedMigration::Migration

  def up
    Ability.find_by_name('Modify Logo').destroy
  end

  def down
    ability     = Ability.create(name: 'Modify Logo', description: 'modify logo')
    admin_role  = Organization.root.admin_role
    admin_role.abilities << ability unless admin_role.abilities.include?(ability)
  end

end
