class RemoveModifyFooterAbility < SeedMigration::Migration

  def up
    Ability.find_by_name('Modify Footer').destroy
  end

  def down
    ability     = Ability.create(name: 'Modify Footer', description: 'modify footer')
    admin_role  = Organization.root.admin_role
    admin_role.abilities << ability unless admin_role.abilities.include?(ability)
  end

end
