class AddAdminPanelToRootAdmin < SeedMigration::Migration
  def up
    root = Organization.roots.order('id asc').first
    role = root.admin_role

    ability = Ability.find_by_name 'View Admin Panel'

    if ability.present?
      role.abilities << ability unless role.abilities.include?(ability)
    end
  end

  def down
    ability = Ability.find_by_name 'View Admin Panel'
    if ability.present?
      role.abilities.delete(ability) if role.abilities.include?(ability)
    end
  end
end
