require 'rails_helper'

describe PermissionsController do
  before do
    @current_user = FactoryBot.create(:user)
    @root_org     = @current_user.organization

    @organization_ability = FactoryBot.create(:organization_ability)
    @user_ability         = FactoryBot.create(:user_ability)
    @device_ability       = FactoryBot.create(:device_ability)

    @role = @root_org.admin_role
    @role.abilities << @organization_ability
    @role.abilities << @user_ability
    @role.abilities << @device_ability

    Permission.create(manager: @current_user, manageable: @root_org, role: @role)

    @manager_org = FactoryBot.create(:random_regular_organization)
    @root_org.add_child(@manager_org)

    @manager_user = FactoryBot.create(:user, organization: @manager_org)
    Permission.create(manager: @manager_user, manageable: @manager_org, role: @manager_org.admin_role)

    @manageable = FactoryBot.create(:random_regular_organization)
    @root_org.add_child(@manageable)

    @params = {}

    sign_in @current_user
  end

  shared_examples_for 'permission :create' do
    context 'with valid attributes' do
      it 'creates a new permission' do
        expect {
          xhr :post, :create, @params
        }.to change(Permission, :count).by(1)
      end

      it 'renders the :create view' do
        xhr :post, :create, @params
        expect(response).to render_template :create
      end
    end

    context 'with invalid attributes' do
      before do
        @params[:permission] = {
          manageable_type: nil,
          manageable_id:   nil,
          role_id:         nil
        }
      end

      it 'does not save the new permission' do
        expect {
          xhr :post, :create, @params
        }.to_not change(Permission, :count)
      end

      it 'renders the :create view' do
        xhr :post, :create, @params
        expect(response).to render_template :create
      end
    end
  end

  def set_manager_to_organization
    @manager                  = @manager_org
    @params[:organization_id] = @manager.slug
  end

  def set_manager_to_user
    @manager          = @manager_user
    @params[:user_id] = @manager.id
  end

  describe 'POST #create' do
    context 'when the manager is an organization' do
      before do
        set_manager_to_organization
        @params[:permission] = {
          manageable_type: @manageable.class.to_s,
          manageable_id:   @manageable.id,
          role_id:         @manageable.admin_role.id
        }
      end
      it_behaves_like 'permission :create'
    end

    context 'when the manager is a user' do
      before do
        set_manager_to_user
        @params[:permission] = {
          manageable_type: @manageable.class.to_s,
          manageable_id:   @manageable.id,
          role_id:         @manager.organization.admin_role.id
        }

        # User's org must have a permission on the manageable.
        Permission.create(
          manager:    @manager.organization,
          manageable: @manageable,
          role:       @manageable.admin_role
        )
      end
      it_behaves_like 'permission :create'
    end
  end


end
