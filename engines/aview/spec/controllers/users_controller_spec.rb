require 'rails_helper'

describe UsersController do
  before :each do
    @user          = FactoryBot.create(:user)
    @organization  = @user.organization
    @role          = @organization.admin_role

    @user.manager_permissions.create do |p|
      p.manageable = @organization
      p.role       = @role
    end
    sign_in @user
  end

  describe 'users controller' do
    before do
      Settings.users = Config::Options.new(:enabled => true)
    end

    after do
      Settings.reload!
    end

    describe 'GET #index' do
      it_behaves_like 'an unauthorized action', :get, :index

      context 'when the current user is authorized to view users' do
        before do
          @role.abilities << FactoryBot.create(:user_ability)
        end

        it 'has a 200 status code' do
          get :index
          expect(response.status).to eq(200)
        end

        it 'assigns @presenter' do
          get :index
          expect(assigns(:presenter)).to be_an_instance_of UsersPresenter
        end

        it 'renders the :index view' do
          get :index
          expect(response).to render_template :index
        end
      end
    end

  end
end
