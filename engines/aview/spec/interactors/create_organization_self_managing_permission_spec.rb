require 'rails_helper'

describe CreateOrganizationSelfManagingPermission do

  let(:organization)  { FactoryBot.create(:random_regular_organization) }
  let(:context)       { {organization: organization} }

  before :each do
    # We remove the self managing permission if it exists in case the factory created it.
    organization
    organization.self_managing_permission.destroy if organization.self_managing_permission
    organization.reload
  end


  describe "#call" do

    it "adds an error to the organization when it fails" do
      # We remove the admin role if it exists in case the factory created it to make the so the test fails.
      organization.admin_role.destroy if organization.admin_role
      organization.reload

      expect(organization.errors).to be_empty
      result = CreateOrganizationSelfManagingPermission.call(context)
      expect(organization.errors).not_to be_empty
    end

    it "creates self managing permission for the organization when it succeeds" do
      # We stub the organization's admin role so it succeeds.
      allow(organization).to receive(:admin_role).and_return(FactoryBot.build(:admin_role))
      expect(organization.self_managing_permission).not_to be_present
      result = CreateOrganizationSelfManagingPermission.call(context)
      expect(organization.self_managing_permission).to be_present
    end

  end


  describe "#rollback" do

    it "destroys the organization's self managing permission" do
      permission = Permission.new
      allow(organization).to receive(:self_managing_permission).and_return(permission)
      expect(permission).to receive(:destroy)
      CreateOrganizationSelfManagingPermission.new(context).rollback
    end

  end

end
