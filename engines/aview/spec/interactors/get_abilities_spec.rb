require 'rails_helper'

describe GetAbilities do

  subject { GetAbilities }

  context 'when nothing is given' do
    it 'returns an empty array' do
      result = subject.call({})
      expect(result.abilities).to eq []
    end

    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when valid attributes are given' do
    before do
      # Create the abilities we want to use
      @organization_ability   = FactoryBot.create(:organization_ability)
      @user_ability           = FactoryBot.create(:user_ability)
      @device_ability         = FactoryBot.create(:device_ability)
      @configuration_ability  = FactoryBot.create(:configuration_ability)
      @import_devices_ability = FactoryBot.create(:import_devices_ability)

      # The root organization that will hold the current user.
      @root_organization = FactoryBot.create(:random_regular_organization)

      # The current user trying to get the abilities.
      @current_user = FactoryBot.create(:user, organization: @root_organization)

      # The root organization's role.
      @root_role = @root_organization.admin_role

      @root_role.abilities << @organization_ability
      @root_role.abilities << @user_ability
      @root_role.abilities << @device_ability
      @root_role.abilities << @configuration_ability
      @root_role.abilities << @import_devices_ability

      # Add a permission for the current user on it's organization with all abilities.
      Permission.create(
        manager:    @current_user,
        manageable: @root_organization,
        role:       @root_role
        )

      # Create sibling organizations that are children of root.
      @organization_1 = FactoryBot.create(:random_regular_organization)
      @organization_2 = FactoryBot.create(:random_regular_organization)

      @org_1_role_1 = @organization_1.admin_role
      @org_1_role_1.abilities << @user_ability
      @org_1_role_1.abilities << @device_ability

      @org_2_role_1 = @organization_2.admin_role
      @org_2_role_1.abilities << @device_ability
      @org_2_role_1.abilities << @configuration_ability

      @root_organization.add_child(@organization_1)
      @root_organization.add_child(@organization_2)
    end

    context 'when the manager is an organization' do
      before do
        # Set the manager and manageable for clarity.
        @manager    = @organization_1
        @manageable = @organization_2

        @manager_role_1    = @org_1_role_1
        @manageable_role_1 = @org_2_role_1

        @params = {
          manager_id:      @manager.id,
          manager_type:    @manager.class.to_s,
          manageable_id:   @manageable.id,
          manageable_type: @manageable.class.to_s
        }

        # Hash used when calling `call` on the interactor.
        @attributes = {
          params: @params,
          user:   @current_user
        }
      end

      context 'when a permission is not given' do
        it 'succeeds when given a manageable role' do
          @params.merge!(role_id: @manageable_role_1.id)
          expect(subject.call(@attributes)).to be_a_success
        end

        # The manageable should dicate what abilities the manager can have on it.
        it 'fails when given a manager role' do
          @params.merge!(role_id: @manager_role_1.id)
          result = subject.call(@attributes)
          expect(result).to be_a_failure
          expect(result.message).to match(/unable to find role/i)
        end

        it 'uses the abilities from the manageable role' do
          @params.merge!(role_id: @manageable_role_1.id)
          result = subject.call(@attributes)
          expect(result.abilities.sort).to eq @manageable_role_1.abilities.sort
        end
      end

      context 'when given a permission' do
        before do
          @permission = Permission.create(
            manager:    @manager,
            manageable: @manageable,
            role:       @manageable_role_1
          )
          expect(@permission).to be_persisted
          @params.merge!(id: @permission.id)
        end

        context 'when updating the permission`s role' do
          before do
            @manageable_role_2 = @manageable.roles.create name: 'Test'
            @manageable_role_2.abilities << @device_ability
          end

          it 'succeeds when given a manageable role' do
            @params.merge!(role_id: @manageable_role_2.id)
            expect(subject.call(@attributes)).to be_a_success
          end

          it 'fails when given a manager role' do
            @params.merge!(role_id: @manager_role_1.id)
            result = subject.call(@attributes)
            expect(result).to be_a_failure
            expect(result.message).to match(/unable to find role/i)
          end

          it 'uses the allowed abilites for the permission with the given role' do
            @params.merge!(role_id: @manageable_role_2.id)
            result = subject.call(@attributes)
            expect(result.abilities.sort).to eq @manageable_role_2.abilities.sort
          end
        end
      end
    end

    context 'when the manager is a user' do
      before do
        @manager    = FactoryBot.create(:user, organization: @organization_1)
        @manageable = @organization_2

        @manager_org_role_1 = @org_1_role_1
        @manageable_role_1  = @org_2_role_1

        # Create permission for user's organization on the test manageable organization
        Permission.create(
          manager:    @organization_1,
          manageable: @organization_2,
          role:       @manageable_role_1
        )

        @params = {
          manager_id:      @manager.id,
          manager_type:    @manager.class.to_s,
          manageable_id:   @manageable.id,
          manageable_type: @manageable.class.to_s
        }

        @attributes = {
          params: @params,
          user:   @current_user
        }
      end

      context 'when a permission is not given' do
        it 'succeeds using a role that belongs to the user`s organization' do
          @params.merge!(role_id: @manager_org_role_1.id)
          expect(subject.call(@attributes)).to be_a_success
        end

        it 'fails when given a manageable role' do
          @params.merge!(role_id: @manageable_role_1.id)
          result = subject.call(@attributes)
          expect(result).to be_a_failure
          expect(result.message).to match(/unable to find role/i)
        end

        it 'allows only the abilities given to user`s organization' do
          abilities = @manager_org_role_1.abilities
          abilities &= @manager.organization.abilities_for(@manageable)

          @params.merge!(role_id: @manager_org_role_1.id)
          result = subject.call(@attributes)
          expect(result.abilities.sort).to eq abilities.sort
        end
      end

      context 'when a permission is given' do
        before do
          @permission = Permission.create(
            manager:    @manager,
            manageable: @manageable,
            role:       @manager_org_role_1
          )
          expect(@permission).to be_persisted
          @params.merge!(id: @permission.id)
        end

        context 'when updating the permission`s role' do
          before do
            @manager_org_role_2 = @manager.organization.roles.create name: 'Test'
            @manager_org_role_2.abilities << @configuration_ability
          end

          it 'succeeds when using a role that belongs to the user`s organization' do
            @params.merge!(role_id: @manager_org_role_2.id)
            expect(subject.call(@attributes)).to be_a_success
          end

          it 'fails when given a manageable role' do
            @params.merge!(role_id: @manageable_role_1.id)
            result = subject.call(@attributes)
            expect(result).to be_a_failure
            expect(result.message).to match(/unable to find role/i)
          end

          it 'gets the allowed abilites from the permission with the new role' do
            @permission.role = @manager_org_role_2
            abilities        = @permission.allowed_abilities
            @params.merge!(role_id: @manager_org_role_2.id)
            result = subject.call(@attributes)
            expect(result.abilities.sort).to eq abilities.sort
          end
        end
      end
    end
  end
end
