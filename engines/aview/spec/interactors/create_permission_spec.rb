require 'rails_helper'

describe UpdatePermission do

  subject { UpdatePermission }

  context 'when nothing is given' do
    it 'fails' do
      result = subject.call({})
      expect(result).to be_a_failure
    end
  end

  context 'when attributes are given' do
    before do
      @manager    = FactoryBot.create(:random_regular_organization)
      @manageable = FactoryBot.create(:random_regular_organization)

      @permission = Permission.create(
        manager:    @manager,
        manageable: @manageable,
        role:       @manageable.admin_role
      )

      @params = {
        role_id: @manageable.admin_role.id
      }

      @attributes = {
        params:     @params,
        permission: @permission
      }
    end

    context 'when the permission manager is set' do
      it 'succeeds' do
        result = subject.call(@attributes)
        expect(result).to be_a_success
      end
    end

    context 'when the permission manager is not set' do
      before do
        @permission.manager = nil
      end

      it 'fails' do
        result = subject.call(@attributes)
        expect(result).to be_a_failure
        expect(result.message).to match(/unable to find manager/i)
      end
    end

    context 'when the permission manageable is set' do
      it 'succeeds' do
        result = subject.call(@attributes)
        expect(result).to be_a_success
      end
    end

    context 'when the permission manageable is not set' do
      before do
        @permission.manageable = nil
      end

      it 'fails' do
        result = subject.call(@attributes)
        expect(result).to be_a_failure
        expect(result.message).to match(/unable to find manageable/i)
      end
    end

    context 'when the manager is an organization' do
      context 'when the role belongs to the manageable' do
        before do
          @role             = @manageable.roles.create name: 'Test'
          @params[:role_id] = @role.id
          expect(@role).to be_persisted
        end

        it 'succeeds' do
          result = subject.call(@attributes)
          expect(result).to be_a_success
        end
      end

      context 'when the role does not belong to the manageable' do
        before do
          @role             = @manager.roles.create name: 'Test'
          @params[:role_id] = @role.id
          expect(@role).to be_persisted
        end

        it 'fails' do
          result = subject.call(@attributes)
          expect(result).to be_a_failure
          expect(result.message).to match(/unable to find role/i)
        end
      end
    end

    context 'when the manager is a user' do
      before do
        @manager_org = @manager
        @manager     = FactoryBot.create(:user, organization: @manager_org)
        @permission  = Permission.create(
          manager:    @manager,
          manageable: @manageable,
          role:       @manager_org.admin_role
        )
        @attributes[:permission] = @permission
        expect(@permission).to be_persisted
      end

      context 'when the role belongs to the user`s organization' do
        before do
          @role             = @manager_org.roles.create name: 'Test'
          @params[:role_id] = @role.id
          expect(@role).to be_persisted
        end

        it 'succeeds' do
          result = subject.call(@attributes)
          expect(result).to be_a_success
        end
      end

      context 'when the role does not belong to the user`s organization' do
        before do
          @role             = @manageable.roles.create name: 'Test'
          @params[:role_id] = @role.id
          expect(@role).to be_persisted
        end

        it 'fails' do
          result = subject.call(@attributes)
          expect(result).to be_a_failure
          expect(result.message).to match(/unable to find role/i)
        end
      end
    end
  end
end
