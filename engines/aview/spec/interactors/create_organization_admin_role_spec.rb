require 'rails_helper'

describe CreateOrganizationAdminRole do

  let(:organization)  { FactoryBot.create(:random_regular_organization) }
  let(:context)       { {organization: organization} }

  before :each do
    # We remove the admin role for the organization if it exists in case the factory created it.
    organization
    organization.admin_role.destroy if organization.admin_role
    organization.reload
  end


  describe "#call" do

    it "adds an error to the organization when it fails" do
      allow(organization).to receive(:save).and_return(false)
      expect(organization.errors).to be_empty
      result = CreateOrganizationAdminRole.call(context)
      expect(organization.errors).not_to be_empty
    end

    it "creates an admin role for the organization when it succeeds" do
      expect(organization.admin_role).not_to be_present
      result = CreateOrganizationAdminRole.call(context)
      expect(organization.admin_role).to be_present
    end

  end


  describe "#rollback" do

    it "destroys the organization's admin_role" do
      role = Role.new
      allow(organization).to receive(:admin_role).and_return(role)
      expect(role).to receive(:destroy)
      CreateOrganizationAdminRole.new(context).rollback
    end

  end

end
