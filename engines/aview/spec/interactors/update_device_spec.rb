require 'rails_helper'

describe UpdateDevice do
  subject { UpdateDevice }

  let(:organization)  { FactoryBot.create(:random_regular_organization) }
  let(:old_site_name) { 'Test Site 01' }
  let(:site)          { FactoryBot.create(:site, organization: organization, name: old_site_name) }
  let(:device)        { FactoryBot.create(:netbridge_device, organization: organization, site: site) }
  let(:user)          { FactoryBot.create(:user, organization: organization) }
  let(:configuration) { FactoryBot.create(:netbridge_configuration, organization: organization) }
  let(:new_org)       { FactoryBot.create(:random_regular_organization) }
  let(:new_site_name) { 'Test Site 02' }
  let(:params)        { { organization_id: new_org.id,
                         configuration_id: configuration.id } }
  let(:attributes)    { { device: device, params: params, user: user } }

  context 'when the user has the ability to modify devices' do
    before do
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Devices').and_return(true)

      # Need to stub these as well since we are being explicit with the method
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Configurations').and_return(false)
      allow(user).to receive(:has_ability_on?).with(organization, to: 'Modify Device Configurations').and_return(false)
      allow_any_instance_of(DevicePolicy).to receive(:change_organization?).and_return(true)
    end

    it 'updates the organization' do
      result = subject.call(attributes)
      device.reload
      expect(device.organization).to eq new_org
    end

    context 'when the organization is changed but not the site' do
      before do
        params[:site_name] = old_site_name
      end

      it 'creates a new site for the organization' do
        expect {
          subject.call(attributes)
        }.to change { organization.sites.count }.by(1)
      end

      it 'keeps the old site name' do
        result = subject.call(attributes)
        device.reload
        expect(device.site_name).to eq old_site_name
      end
    end

    context 'when the organization and site are changed' do
      before do
        params[:site_name] = new_site_name
      end

      it 'creates a new site for the organization' do
        expect {
          subject.call(attributes)
        }.to change { organization.sites.count }.by(1)
      end

      it 'uses the new site name' do
        result = subject.call(attributes)
        device.reload
        expect(device.site_name).to eq new_site_name
      end
    end
  end
end
