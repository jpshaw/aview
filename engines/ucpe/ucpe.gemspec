lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name        = 'ucpe'
  s.version     = '1.0'
  s.authors     = ["Accelerated Concepts"]
  s.email       = ["aview@accelerated.com"]
  s.homepage    = "https://ucpe.accns.com"
  s.summary     = "An engine used to run ucpe"
  s.description = "An engine used to run ucpe"

  s.files = Dir['aview.gemspec', 'torquebox.yml', 'app/**/*', 'config/**/*', 'lib/**/*']

  s.add_dependency 'rails', '~> 4.0'
end
