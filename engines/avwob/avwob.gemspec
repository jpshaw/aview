lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name        = 'avwob'
  s.version     = '1.0'
  s.authors     = ["Accelerated Concepts"]
  s.email       = ["aview@accelerated.com"]
  s.homepage    = "https://avwob.accns.com"
  s.summary     = "An engine used to run avwob"
  s.description = "An engine used to run avwob"

  s.files = Dir['avwob.gemspec', 'config/**/*', 'lib/**/*']

  s.add_dependency 'rails', '~> 4.0'
end
