class Admin::SyslogServersController < AdminController

  before_filter :authorize_aots_syslogs!
  before_filter :find_syslog_server, only: [:edit, :update, :destroy, :test]
  before_filter :set_view_path, only: [:new, :edit]

  def index
    render json: SyslogServersDataTable.new(view_context)
  end

  def new
    @syslog_server = SyslogServer.new
  end

  def create
    @syslog_server = SyslogServer.create(params[:syslog_server])
  end

  def edit
  end

  def update
    @syslog_server.update_attributes(params[:syslog_server])
  end

  def destroy
    @syslog_server.destroy
  end

  def test
    @syslog_server.test_connection
  end

  private

  def authorize_aots_syslogs!
    redirect_to admin_path unless Settings.aots.enabled?
  end

  def find_syslog_server
    @syslog_server = SyslogServer.find_by(id: params[:id])
  end

  def set_view_path
    prepend_view_path "#{Rails.root}/engines/avwob/app/views/admin/syslog_servers"
  end
end
