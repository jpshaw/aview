require 'syslog_event'

class SyslogServer < ActiveRecord::Base

  UP   = 'up'
  DOWN = 'down'

  attr_accessible :name, :uri, :server_state

  validates :name,  presence: true, uniqueness: true
  validates :uri,   presence: true, uniqueness: true

  validate  :uri_format, if: 'uri.present?'

  def up!
    state_machine.trigger(:up)
  end

  def down!
    state_machine.trigger(:down)
  end

  def up?
    state_machine.state == UP
  end

  def down?
    state_machine.state == DOWN
  end

  def client
    @client ||= SyslogEvent::Client.new(self.uri)
  end

  def test_connection
    client.test_syslog
    up!
  rescue Errno::ECONNREFUSED
    down!
  end

  private

  def state_machine
    @state_machine ||= begin
      fsm = MicroMachine.new(server_state || UP)
      fsm.when(:up,   DOWN => UP)
      fsm.when(:down, UP   => DOWN)

      fsm.on(:any)  { update_attribute(:server_state, state_machine.state) }
      fsm.on(UP)    { send_up_notification }
      fsm.on(DOWN)  { send_down_notification }

      fsm
    end
  end

  def send_up_notification
    SyslogEvent.logger.info "Server '#{name}' with URI '#{uri}' is up"
    SyslogServerStatusMailer.server_up(self).deliver_now
  end

  def send_down_notification
    SyslogEvent.logger.error "Server '#{name}' with URI '#{uri}' is down"
    SyslogServerStatusMailer.server_down(self).deliver_now
  end

  def uri_format
    URI::parse("tcp://#{uri}")
  rescue
    errors.add(:uri, 'has an invalid format')
  end

end
