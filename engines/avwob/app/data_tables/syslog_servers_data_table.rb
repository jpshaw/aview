class SyslogServersDataTable < DataTables::Base

  delegate :test_admin_syslog_server_path, :edit_admin_syslog_server_path,
    :admin_syslog_server_path, to: :view

  def as_json(options = {})
    {
      draw:            params[:draw].to_i,
      recordsTotal:    get_raw_records.count(:all),
      recordsFiltered: get_raw_records.count(:all),
      data:            data
    }
  end

  private

  def data
    get_raw_records.map do |record|
      data_row(record)
    end
  end

  def data_row(record)
    {
      id: record.id,
      name: record.name,
      uri: record.uri,
      status: server_status(record),
      test: test_link(record),
      edit: edit_link(record),
      delete: delete_link(record),
    }
  end

  def get_raw_records
    @imports ||= begin
      SyslogServer
        .order(:name)
        .paginate(
          page:     params[:page],
          per_page: current_user.syslog_servers_pagination_limit
        )
    end
  end

  def server_status(record)
    if record.up?
      '<span class="text-info"><i class="fa fa-play fa-rotate-270"></i> Up</span>'
    else
      '<span class="text-danger"><i class="fa fa-play fa-rotate-90"></i> Down</span>'
    end
  end

  def test_link(record)
    link_to(
      '<i class="fa fa-cog"> </i> Test'.html_safe,
      test_admin_syslog_server_path(record, format: :js),
      class:  'btn btn-success btn-xs table-button-remote',
      title:  'Send Test Syslog',
      method: :patch,
      remote: true
    )
  end

  def edit_link(record)
    link_to(
      '<i class="fa fa-edit"> </i> Edit'.html_safe,
      edit_admin_syslog_server_path(record, format: :js),
      class: 'btn btn-white btn-xs table-button-remote',
      title: 'Edit Syslog Server',
      remote: true
    )
  end

  def delete_link(record)
    link_to(
      '<i class="fa fa-trash"></i> Remove'.html_safe,
      admin_syslog_server_path(record),
      class: 'btn btn-danger btn-xs table-button-remote',
      title: 'Remove Syslog Server',
      method: :delete,
      remote: true,
      data: { confirm: 'Are you sure you want to remove this syslog server?' }
    )
  end

end