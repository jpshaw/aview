require 'syslog_event'

class SyslogEventProcessor < TorqueBox::Messaging::MessageProcessor

  def on_message(device_event_id)
    SyslogEvent.process(device_event_id)
  end

end
