class SyslogServerStatusMailer < ActionMailer::Base
  default from: Settings.email_addresses.alerts

  def server_down(server)
    @message = "Server '#{server.name}' with URI '#{server.uri}' is DOWN"
    mail to: Settings.support_email.address, subject: @message
  end

  def server_up(server)
    @message = "Server '#{server.name}' with URI '#{server.uri}' is UP"
    mail to: Settings.support_email.address, subject: @message
  end
end
