require 'rails_helper'


describe SyslogServer do

  let(:valid_uri)   { 'localhost:80'  }
  let(:invalid_uri) { '\\\INVALID_URI' }
  let(:server)      { SyslogServer.create!(name: 'Test', uri: '127.0.0.1:10514') }
  let(:logger)      { SyslogEvent.logger }

  describe '#up!' do
    context 'when called on a new syslog server record' do
      before do
        expect(server.server_state).to be_blank
      end

      it 'sets the default state to `up`' do
        server.up!
        expect(server.up?).to be true
      end
    end

    context 'when the server is already up' do
      before do
        server.up!
      end

      it 'does not generate a notification' do
        expect(server).to_not receive(:send_up_notification)
        server.up!
      end

      it 'does not create a log' do
        expect(logger).to_not receive(:info)
        server.up!
      end
    end

    context 'when the server is down' do
      before do
        server.down!
      end

      it 'sets the state to `up`' do
        expect {
          server.up!
        }.to change { server.up? }.from(false).to(true)
      end

      it 'creates a log indicating the server is up' do
        expect(logger).to receive(:info).with(/up/)
        server.up!
      end

      it 'generates an `up` notification' do
        expect(server).to receive(:send_up_notification)
        server.up!
      end
    end
  end

  describe '#down!' do
    context 'when the server is up' do
      before do
        server.up!
      end

      it 'sets the state to `down`' do
        expect {
          server.down!
        }.to change { server.down? }.from(false).to(true)
      end

      it 'creates a log indicating the server is down' do
        expect(logger).to receive(:error).with(/down/)
        server.down!
      end

      it 'generates a `down` notification' do
        expect(server).to receive(:send_down_notification)
        server.down!
      end
    end

    context 'when the server is already down' do
      before do
        server.down!
      end

      it 'does not generate a notification' do
        expect(server).to_not receive(:send_down_notification)
        server.down!
      end

      it 'does not create a log' do
        expect(logger).to_not receive(:error)
        server.down!
      end
    end
  end

  describe '#test_connection' do
    let(:client) { double('client') }

    context 'when the uri can be reached' do
      before do
        allow(client).to receive(:test_syslog).and_return(true)
        allow(server).to receive(:client).and_return(client)
      end

      it 'triggers the up! method' do
        expect(server).to receive(:up!)
        server.test_connection
      end
    end

    context 'when the uri is unreachable' do
      before do
        allow(client).to receive(:test_syslog).and_raise(Errno::ECONNREFUSED)
        allow(server).to receive(:client).and_return(client)
      end

      it 'triggers the down! method' do
        expect(server).to receive(:down!)
        server.test_connection
      end
    end
  end

  describe "Validations" do

    describe 'uri format' do

      context 'without uri' do

        it 'is not validated' do
          expect(subject).not_to receive(:uri_format)
          subject.valid?
        end

      end


      context 'with uri' do

        it 'is validated against the URI class parser' do
          subject.uri = valid_uri
          expect(URI).to receive(:parse)
          subject.valid?

          subject.uri = invalid_uri
          expect(URI).to receive(:parse)
          subject.valid?
        end

        it 'generates an error if invalid' do
          subject.name = 'test'
          subject.uri = invalid_uri
          subject.valid?
          expect(subject.errors.keys).to include(:uri)
        end

        it 'does not generate an error if valid' do
          subject.name = 'test'
          subject.uri = valid_uri
          subject.valid?
          expect(subject.errors.keys).not_to include(:uri)
        end

      end

    end

  end

end
