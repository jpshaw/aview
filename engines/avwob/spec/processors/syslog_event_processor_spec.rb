require 'rails_helper'

describe SyslogEventProcessor do

  let(:device) { FactoryBot.create(:remote_manager_device) }
  let(:event)  { device.events.create }

  describe '#on_message' do

    context 'when given a device event id' do
      it 'processes the event' do
        expect(SyslogEvent).to receive(:process).with(event.id)
        subject.on_message(event.id)
      end
    end

  end

end
