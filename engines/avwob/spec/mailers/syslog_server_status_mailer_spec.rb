require 'rails_helper'

describe SyslogServerStatusMailer do

  subject { SyslogServerStatusMailer }

  let(:server) { SyslogServer.create(name: 'Testing', uri: '127.0.0.1:10514') }

  after do
    subject.deliveries.clear
  end

  describe '.server_down' do
    context 'when given a syslog server' do
      it 'creates an email notification indicating the server is down' do
        subject.server_down(server).deliver_now
        expect(subject.deliveries).to_not be_empty
        message = subject.deliveries.first.subject
        expect(message).to include(server.name)
        expect(message).to include(server.uri)
        expect(message).to include('DOWN')
      end
    end
  end

  describe '.server_up' do
    context 'when given a syslog server' do
      it 'creates an email notification indicating the server is up' do
        subject.server_up(server).deliver_now
        expect(subject.deliveries).to_not be_empty
        message = subject.deliveries.first.subject
        expect(message).to include(server.name)
        expect(message).to include(server.uri)
        expect(message).to include('UP')
      end
    end
  end

end
