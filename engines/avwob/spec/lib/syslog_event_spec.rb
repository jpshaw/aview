require 'rails_helper'

describe SyslogEvent do

  subject { SyslogEvent }

  let(:device) { FactoryBot.create(:remote_manager_device) }
  let(:event)  { device.events.create }
  let(:logger) { subject.logger }

  describe '.process' do
    context 'when given an event id' do
      before do
        allow_any_instance_of(SyslogEvent::Message).to receive(:valid?).and_return(true)
      end

      it 'creates a new message instance' do
        expect(SyslogEvent::Message).to receive(:new)
        subject.process(event.id)
      end

      it 'uses the dispatcher to send the message' do
        expect_any_instance_of(SyslogEvent::Dispatcher).to receive(:send)
        subject.process(event.id)
      end
    end

    context 'when an exception is thrown while processing the event' do
      before do
        allow_any_instance_of(SyslogEvent::Dispatcher).to receive(:send).and_raise(StandardError)
      end

      it 'logs an error' do
        expect(logger).to receive(:error).with(/Failed to process event/)
        subject.process(event.id)
      end
    end
  end

end