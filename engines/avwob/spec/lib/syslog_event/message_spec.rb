require 'rails_helper'

describe SyslogEvent::Message do

  subject { SyslogEvent::Message }

  let(:device) { FactoryBot.create(:remote_manager_device) }
  let(:event)  { device.events.create }

  describe '.new' do
    context 'when given an event that exists' do
      context 'when given a valid syslog event' do
        before do
          allow_any_instance_of(subject).to receive(:valid?).and_return(true)
        end

        it 'does not raise an exception' do
          expect { subject.new(event.id) }.to_not raise_error
        end
      end

      context 'when given an invalid syslog event' do
        before do
          allow_any_instance_of(subject).to receive(:valid?).and_return(false)
        end

        it 'raises an `invalid syslog` exception' do
          expect {
            subject.new(event.id)
          }.to raise_error(SyslogEvent::Message::InvalidSyslogEventError)
        end
      end
    end

    context 'when given an event that does not exist' do
      it 'raises a `record not found` exception' do
        expect { subject.new(0) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'instance variables' do
    before do
      allow_any_instance_of(subject).to receive(:valid?).and_return(true)
      allow_any_instance_of(subject).to receive(:event_code).and_return(EventUUIDS::REMOTEMANAGER_DOWN)
    end

    let(:message) { subject.new(event.id) }
    let(:text)    { 'Modem is unreachable' }
    let(:id)      { 'Modem_Down_Notify' }
    let(:args)    { {iccid: nil, imei: nil, service_name: nil, hostname: nil} }

    describe '#text' do
      it 'returns the notification text' do
        expect(message.text).to eq text
      end
    end

    describe '#id' do
      it 'returns the notification id' do
        expect(message.id).to eq id
      end
    end

    describe '#args' do
      it 'returns the notification arguments' do
        expect(message.args).to eq args
      end
    end
  end

end