require 'rails_helper'

describe SyslogEvent::Dispatcher do

  subject { SyslogEvent::Dispatcher }

  before do
    allow_any_instance_of(SyslogEvent::Message).to receive(:valid?).and_return(true)
    SyslogServer.create!(name: 'Test', uri: '127.0.0.1:10514')
  end

  let(:device)     { FactoryBot.create(:remote_manager_device) }
  let(:event)      { device.events.create }
  let(:message)    { SyslogEvent::Message.new(event.id) }
  let(:dispatcher) { subject.new }
  let(:logger)     { dispatcher.__send__(:logger) }

  describe '#send' do
    context 'when given nil' do
      it 'raises an `invalid message` exception' do
        expect {
          dispatcher.send(nil)
        }.to raise_error(SyslogEvent::Dispatcher::InvalidMessageError)
      end
    end

    context 'when given a message' do
      it 'does not raise an exception' do
        expect { dispatcher.send(message) }.to_not raise_error
      end

      context 'when the message is successfully sent' do
        before do
          allow_any_instance_of(SyslogEvent::Client).to receive(:send).and_return(true)
        end

        it 'returns true' do
          expect(dispatcher.send(message)).to be true
        end

        it 'logs that the message was sent' do
          expect(logger).to receive(:info).with(/Successfully sent event/)
          dispatcher.send(message)
        end

        it 'tags the server as being up' do
          expect_any_instance_of(SyslogServer).to receive(:up!)
          dispatcher.send(message)
        end
      end

      context 'when the message is not sent' do
        before do
          allow_any_instance_of(SyslogEvent::Client).to receive(:send).and_return(false)
        end

        it 'returns false' do
          expect(dispatcher.send(message)).to be false
        end

        it 'logs a warning that the message was not sent' do
          expect(logger).to receive(:warn).with(/Unable to send event/)
          dispatcher.send(message)
        end
      end

      context 'when the connection is refused' do
        before do
          allow_any_instance_of(SyslogEvent::Client).to receive(:send).and_raise(Errno::ECONNREFUSED)
          allow_any_instance_of(SyslogServer).to receive(:down!).and_return(nil)
        end

        it 'tags the server as being down' do
          expect_any_instance_of(SyslogServer).to receive(:down!)
          dispatcher.send(message)
        end

        it 'logs an error about the connection refusal' do
          expect(logger).to receive(:error).with(/Connection refused/)
          dispatcher.send(message)
        end
      end

      context 'when an unexpected exception is raised' do
        before do
          allow_any_instance_of(SyslogEvent::Client).to receive(:send).and_raise(StandardError)
        end

        it 'logs an error about the unexpected error' do
          expect(logger).to receive(:error).with(/An unexpected error occured/)
          dispatcher.send(message)
        end
      end
    end
  end

end