require 'rails_helper'
require 'syslog_event'

describe SyslogEvent::Client do

  subject { SyslogEvent::Client }

  let(:logger)       { SyslogRuby::Logger.new }
  let(:uri)          { '127.0.0.1:10514' }
  let(:client)       { subject.new(uri) }
  let(:message_text) { 'TEST_MESSAGE_TEXT' }
  let(:message_id)   { 'TEST_MESSAGE_ID' }
  let(:args)         {
    {
      iccid:        'TEST_ICIID',
      imei:         'TEST_IMEI',
      service_name: 'TEST_SERVICE NAME',
      hostname:     'TEST_HOST_NAME'
    }
  }

  before do
    allow(logger).to receive(:opened?).and_return(true)
    allow(logger).to receive(:info)
  end

  describe '#send' do
    it 'instantiates a SyslogRuby::Logger' do
      expect(SyslogRuby::Logger).to receive(:new).and_return(logger)
      client.send(message_text, message_id, args)
    end

    it 'logs a message' do
      allow(SyslogRuby::Logger).to receive(:new).and_return(logger)
      expect(logger).to receive(:info)
      client.send(message_text, message_id, args)
    end
  end

  describe '#test_syslog' do
    it 'logs the message' do
      allow(SyslogRuby::Logger).to receive(:new).and_return(logger)
      expect(logger).to receive(:info)
      client.test_syslog
    end
  end
end