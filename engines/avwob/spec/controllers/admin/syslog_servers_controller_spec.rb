require 'rails_helper'

describe Admin::SyslogServersController do

  let!(:server) { SyslogServer.create!(name: 'Test', uri: '127.0.0.1:10514') }

  before do
    # Create user with admin access
    @user = FactoryBot.create(:user_with_org_permission)
    sign_in @user

    # Enable admin panel
    Settings.admin = Config::Options.new(enabled: true)
    allow_any_instance_of(AdminControllerPolicy).to receive(:view?).and_return(true)

    # Enable Syslog Servers for AOTS
    Settings.aots = Config::Options.new(enabled?: true)
  end

  after do
    Settings.reload!
  end

  describe 'GET index' do
    it 'has a 200 status code' do
      xhr :get, :index
      expect(response.status).to eq(200)
    end

    it 'returns a data table json object for syslog servers' do
      xhr :get, :index
      data_table = JSON.parse(response.body)
      expect(data_table['recordsTotal']).to eq 1
      record = data_table['data'].first
      expect(record['name']).to eq server.name
    end
  end

  describe 'GET new' do
    it 'assigns a new Syslog Server to @syslog_server' do
      xhr :get, :new
      expect(assigns(:syslog_server)).to be_a SyslogServer
    end

    it 'renders the new template' do
      xhr :get, :new
      expect(response).to render_template 'admin/syslog_servers/new'
    end

    it 'has a 200 status code' do
      xhr :get, :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do
    context 'with valid attributes' do
      let(:valid_attributes) { { syslog_server: { name: 'Test 1', uri: 'test.local' } } }

      it 'creates a new syslog server' do
        expect {
          xhr :post, :create, valid_attributes
        }.to change(SyslogServer, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_attributes) { { syslog_server: { name: '', uri: '' } } }

      it 'does not create a new syslog server' do
        expect {
          xhr :post, :create, invalid_attributes
        }.to_not change(SyslogServer, :count)
      end
    end
  end

  describe 'GET edit' do
    it 'assigns the requested syslog server to @syslog_server' do
      xhr :get, :edit, id: server.id
      expect(assigns(:syslog_server)).to eq server
    end

    it 'renders the edit template' do
      xhr :get, :edit, id: server.id
      expect(response).to render_template 'admin/syslog_servers/edit'
    end
  end

  describe 'PATCH update' do
    context 'with valid attributes' do
      let(:new_name) { 'New Name Test' }
      let(:valid_attributes) { { id: server.id, syslog_server: { name: new_name } } }

      it 'located the requested @syslog_server' do
        xhr :patch, :update, valid_attributes
        expect(assigns(:syslog_server)).to eq server
      end

      it 'changes the syslog server attributes' do
        expect {
          xhr :patch, :update, valid_attributes
        }.to change { server.reload.name }.to(new_name)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_attributes) { { id: server.id, syslog_server: { name: '' } } }

      it 'located the requested @syslog_server' do
        xhr :patch, :update, invalid_attributes
        expect(assigns(:syslog_server)).to eq server
      end

      it 'does not change the syslog server attributes' do
        expect {
          xhr :patch, :update, invalid_attributes
        }.to_not change { server.reload.name }
      end
    end
  end

  describe 'DELETE destroy' do
    it 'deletes the syslog server' do
      expect {
        xhr :delete, :destroy, id: server.id
      }.to change(SyslogServer, :count).by(-1)
    end
  end

end
