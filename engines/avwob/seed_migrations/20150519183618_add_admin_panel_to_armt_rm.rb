class AddAdminPanelToArmtRm < SeedMigration::Migration
  def up
    root = Organization.roots.order('id asc').first
    role = root.admin_role

    ability = Ability.find_by_name 'View Admin Panel'

    if ability.present?
      role.abilities << ability unless role.abilities.include?(ability)
    end
  end

  def down
    if ability = Ability.find_by_name('View Admin Panel')
      admin_role = Organization.roots.first.admin_role
      admin_role.role_abilities.where(ability_id: ability.id).destroy_all
    end
  end
end
