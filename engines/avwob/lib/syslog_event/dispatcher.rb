module SyslogEvent

  # Handles sending a syslog event message to any of the syslog servers available.
  class Dispatcher
    include ::Kraken::Logger.file

    class InvalidMessageError < StandardError; end

    attr_reader :servers

    def initialize
      @servers = SyslogServer.all.to_a
    end

    def send(message)
      validate(message)

      servers.each do |server|
        begin
          client = server.client

          if client.send(message.text, message.id, message.args)
            log_message_success(message, server)
            server.up!
            return true
          else
            log_message_warning(message, server)
          end

        rescue Errno::ECONNREFUSED
          log_connection_refused(message, server)
          server.down!
        rescue => e
          log_unexpected_error(message, server, e)
        end
      end

      false
    end

    private

    def validate(message)
      unless message.is_a?(SyslogEvent::Message)
        raise InvalidMessageError.new("Invalid message type given: '#{message.class}' - must be a SyslogEvent::Message")
      end
    end

    def log_message_success(message, server)
      logger.info "Successfully sent event ID #{message.event.id} to " \
                  "#{server_message(server)}."
    end

    def log_message_warning(message, server)
      logger.warn "Unable to send event ID #{message.event.id} to " \
                  "#{server_message(server)}."
    end

    def log_connection_refused(message, server)
      logger.error "Connection refused for event ID #{message.event.id} to " \
                   "#{server_message(server)}."
    end

    def log_unexpected_error(message, server, error)
      logger.error "An unexpected error occured when sending event ID #{message.event.id} to " \
                   "#{server_message(server)}. " \
                   "Error: #{error.message}. " \
                   "Backtrace: #{error.backtrace[0,3].join("\n")}\n"
    end

    def server_message(server)
      "server '#{server.name}' with URI '#{server.uri}'"
    end

  end

end
