module SyslogEvent

  # This class wraps a device event and provides the common interfaces needed to
  # create a syslog notification.
  class Message

    class InvalidSyslogEventError < StandardError; end

    attr_reader :event, :device, :modem

    def initialize(event_id)
      @event  = DeviceEvent.find(event_id)
      @device = @event.device
      @modem  = @device.modem

      unless valid?
        raise InvalidSyslogEventError.new("Not a valid syslog type event: #{event.id}")
      end
    end

    def text
      @text ||= notification[:text]
    end

    def id
      @id ||= notification[:id]
    end

    def args
      @args ||= begin
        {
          iccid:        modem.try(:iccid),
          imei:         modem.try(:imei),
          service_name: device.service_name,
          hostname:     device_hostname
        }
      end
    end

    private

    def event_code
      @event_code ||= event.uuid.try(:code)
    end

    def notification
      @notification ||= notifications.fetch(event_code, {})
    end

    def notifications
      @notifications ||= {
        EventUUIDS::REMOTEMANAGER_DOWN => {
          text: 'Modem is unreachable',
          id:   'Modem_Down_Notify'
        },
        EventUUIDS::REMOTEMANAGER_PORT_DOWN => {
          text: "#{event.port_number_from_info} connected at Host #{device.host} is down",
          id:   'Port_Down_Notify'
        },
        EventUUIDS::REMOTEMANAGER_SIGNAL_CHANGED => {
          text: "Signal strength is below the threshold of #{signal_threshold}%",
          id:   'Low_Signal_Notify'
        },
        EventUUIDS::REMOTEMANAGER_ETHERNET_DISCONNECTED => {
          text: "#{event.info}",
          id:   'Eth_Down_Notify'
        },
        EventUUIDS::CELLULAR_DOWN => {
          text: 'Modem is unreachable',
          id:   'Modem_Down_Notify'
        },
        EventUUIDS::CELLULAR_SIGNAL_CHANGED => {
          text: "Signal strength is below the threshold of #{signal_threshold}%",
          id:   'Low_Signal_Notify'
        },
        EventUUIDS::CELLULAR_ETHERNET_DISCONNECTED => {
          text: "#{event.info}",
          id:   'Eth_Down_Notify'
        },
      }
    end

    def device_hostname
      if device.hostname.present?
        device.hostname
      elsif device.configuration.present?
        device.configuration.fetch_value(:system, :name)
      end
    end

    def signal_threshold
      AdminSettings.threshold_percentage
    end

    def valid?
      EventUUIDS::SYSLOGGABLE.include?(event.uuid.try(:code))
    end

  end

end
