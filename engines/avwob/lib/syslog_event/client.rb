require 'syslog_ruby'

module SyslogEvent

  # Handles connecting to a syslog endpoint and sending messages to it.
  class Client

    DEFAULT_LOG_PRIORITY  = 132.freeze
    DEFAULT_LOG_LEVEL     = 'Warning'.freeze
    NIL_VALUE             = 'NILVALUE'.freeze

    attr_reader :uri

    def initialize(uri)
      @uri = uri
    end

    # Sends a TCP Syslog to the URI
    def send(message_text, message_id, args = {})
      logger = syslogger(args)

      # A TCPSocket is used by the logger and it could timeout. If we don't
      # check if the socket is open we might incur in a double timeout, which
      # could be quite a long time.
      if logger.opened?
        logger.info(
          "#{DEFAULT_LOG_LEVEL}|#{message_text}|#{args[:iccid]}|#{args[:imei]}|#{args[:service_name]}",
          message_id: message_id
        )
      end
    end

    # Sends a test syslog
    def test_syslog
      args = {
        iccid:        'TEST_ICCID',
        imei:         'TEST_IMEI',
        service_name: 'TEST_SERVICE NAME',
        hostname:     'TEST_HOST_NAME'
      }

      send 'TEST_MESSAGE_TEXT', 'TEST_MESSAGE_ID', args
    end

    private

    def syslogger(args)
      SyslogRuby::Logger.new(
        'ruby',
        :LOCAL3,
        {
          uri:        "tcp://#{uri}",
          version:    1,
          hostname:   args[:hostname],
          separator:  :dash,
          priority:   DEFAULT_LOG_PRIORITY,
          app_name:   NIL_VALUE,
          proc_id:    NIL_VALUE
        }
      )
    end
  end

end