require 'syslog_event/message'
require 'syslog_event/dispatcher'
require 'syslog_event/client'

module SyslogEvent
  include ::Kraken::Logger.file

  def self.process(event_id)
    message    = SyslogEvent::Message.new(event_id)
    dispatcher = SyslogEvent::Dispatcher.new

    dispatcher.send(message)
  rescue => e
    logger.error "Failed to process event ID #{event_id}. " \
                 "Error: #{e.message}. " \
                 "Backtrace: #{e.backtrace[0,3].join("\n")} \n"
  end

end
