require 'active_support/concern'

module AVWOB
  module GTACAuthenticatable
    extend ActiveSupport::Concern

    AUTH_LEVEL = 'Accelecon/Auth-Level'

    included do
      attr_accessible :role_id
      alias_attribute :auth_level, :role_id

      devise :radius_authenticatable

      before_radius_authorization :assign_permission_role, :assign_organization
      after_radius_authorization :persist_permission_changes
    end

    def email_required?
      false
    end

    def password_required?
      false
    end

    def role
      Role.find_by_auth_level(self.auth_level)
    end

    def permission
      @permission ||= self.manager_permissions.first || self.manager_permissions.build
    end

    private

    def assign_organization
      root_organization          = Organization.roots.first
      self.permission.manageable = root_organization
      self.organization          = root_organization
    end

    def assign_permission_role
      # TODO: Add 'to_i' support for radiustars `Radiustar::Packet::Attribute`
      self.auth_level      = "#{self.radius_attributes[AUTH_LEVEL]}".to_i
      self.permission.role = self.role
    end

    def persist_permission_changes
      self.permission.save if self.permission.changed?
    end

  end

end