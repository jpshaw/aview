require 'rails'
require 'devise-radius-authenticatable'
require 'avwob/gtac_roleable'
require 'avwob/gtac_admin_role'
require 'avwob/gtac_permissable'
require 'avwob/gtac_authenticatable'

module AVWOB
  class Engine < ::Rails::Engine
    config.before_initialize do

      # Autoload paths for AT&T TorqueBox Services
      initializer 'torquebox.services.autoload', before: :set_autoload_paths do |app|
        processors_path = File.expand_path('../../../app/processors', __FILE__)
        app.config.autoload_paths << processors_path
      end
    end

    config.after_initialize do
      User.send(:include, AVWOB::GTACAuthenticatable)
      unless Rails.env.test?
        Role.send(        :include, AVWOB::GTACRoleable   )
        Organization.send(:include, AVWOB::GTACAdminRole  )
        Permission.send(  :include, AVWOB::GTACPermissable)
      end
    end
  end
end
