require 'active_support/concern'

module AVWOB
  module GTACRoleable
    extend ActiveSupport::Concern

    # GTAC Auth levels
    ADMIN             = 1
    READ_WRITE        = 2
    READ_WRITE_NO_ORG = 3
    READ_ONLY         = 4

    ROLES = {
      ADMIN             => 'AVWOB ADMIN',
      READ_WRITE        => 'AVWOB READ/WRITE',
      READ_WRITE_NO_ORG => 'AVWOB READ/WRITE (No Modify ORG)',
      READ_ONLY         => 'AVWOB READ ONLY',
    }.freeze

    def self.scoped_to_organization?
      false
    end

    module ClassMethods

      def find_by_auth_level(auth_level)
        find_by_name ROLES[auth_level.try(:to_i)]
      end

      def admin
        find_by_auth_level ADMIN
      end

      def read_write
        find_by_auth_level READ_WRITE
      end

      def read_write_no_org
        find_by_auth_level READ_WRITE_NO_ORG
      end

      def read_only
        find_by_auth_level READ_ONLY
      end

    end
  end
end