require 'active_support/concern'

module AVWOB
  module GTACPermissable
    extend ActiveSupport::Concern

    # Don't validate permission ownership
    def managed_by_manager_organization
    end

  end
end