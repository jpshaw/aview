AviewWebapp::Application.routes.draw do

  namespace :admin do
    resources :syslog_servers, except: [:show] do
      member do
        patch :test
      end
    end
  end

end
