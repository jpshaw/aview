# Public: An abstract queue class that hides the synchonous request/response implementation.
#
# Examples
#
#   CrossAccountQuery.accounts_for_user("ATAP")
#   # => ["AARON", "BTAP", "CDGRN"]
class CrossAccountQuery

  def self.accounts_for_user(user = "")
    logger   = TorqueBox::Logger.new(self.class)
    queue    = TorqueBox.fetch('/queues/xaccount_requests')
    accounts = []

    begin
      accounts = queue.publish_and_receive(user, :timeout => 5000)
    rescue => e
      logger.error "Cross account query failed with message: #{e.message}"
    end

    accounts
  end
end
