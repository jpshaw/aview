namespace :armt do
  namespace :ppg do

    PPG_CONTACTS_FILE = "#{ENV['HOME']}/ppg_contact_data.csv"

    desc "Associates PPG contacts to VPN Gateway devices"
    task add_contacts: :environment do

      # column headers in the file are:
      #   ACCOUNT_ID
      #   MAC
      #   CONTACT_NAME
      #   CONTACT NUMBER
      CSV.foreach(PPG_CONTACTS_FILE, headers: true) do |row|
        contact_details = { label: ContactLabels::SITE,
                            name:  row['CONTACT_NAME'],
                            phone: row['CONTACT_NUMBER'],
                            email: "#{row['CONTACT_NAME']}@ppg.com"
                          }

        device_mac = row['MAC']

        unless contact_details[:name].present? && contact_details[:phone].present? && device_mac.present?
          puts "Row missing required details: (#{contact_details[:name]}) (#{contact_details[:phone]}) (#{device_mac})"
          next
        end

        gateway = Gateway.find_by(mac: device_mac)

        unless gateway.present?
          puts "Device not found with given MAC (#{device_mac})"
          next
        end

        unless contact = Contact.where(contact_details).first
          contact = gateway.contacts.create(contact_details)
        end

        if contact.blank? || contact.errors.any?
          puts "Error creating (#{contact_details[:name]}) contact: #{contact.errors.full_messages}"
          next
        end
        
        if gateway.contacts.pluck(:id).include?(contact.id)
          puts "Contact #{contact_details[:name]} already associated with VPN Gateway #{gateway.mac}"
        else
          gateway.contacts << contact
          puts "Associated contact #{contact_details[:name]} with VPN Gateway #{gateway.mac}"
        end

      end

    end
  end
end
