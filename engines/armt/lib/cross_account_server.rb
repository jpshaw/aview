# Public: An abstract model used to connect to the AT&T cross acount server
# and run a stored procedure.
class CrossAccountServer < ActiveRecord::Base
  self.abstract_class = true
end
