require 'devise'

module Devise
  module Strategies
    # Public: Authenticate users with AT&T's SMX Service.
    class SmxAuthenticatable < Authenticatable

      def authenticate!

        # authentication_hash doesn't include the password
        auth_params = authentication_hash
        auth_params[:password] = password

        # Pass an empty user to the authenticator. If successful then look for
        # the user or create it.
        auth_user = mapping.to.new

        # Defined in Devise::Strategies::Authenticatable. It takes a block that
        # must return a boolean value.
        #
        # If the block returns true the user will be logged in.
        # If the block returns false the user will be redirected to the login page.
        if validate(auth_user) { auth_user.smx_authentication(auth_params) }

          # Get user and set authorizations.
          user = User.for_smx_auth(auth_level: auth_user.role_id,
                                      account: auth_params[:account_name],
                                     username: auth_params[:username])

          user.set_authorizations

          # I have no idea why this works, but it prevents `401 unauthorized` for
          # first-time users.
          # TODO: Understand why this isn't working correctly and fix.
          user = mapping.to.find_by(id: user.id)

          Rails.logger.info "Successfully authenticated user: #{user.username}"
          Rails.logger.info "#{user.username} organization permissions: #{managed_organization_names_for_user(user)}"

          success!(user)
        end
      end

      # Returns a string of organization names the user has permissions for.
      def managed_organization_names_for_user(user)
        Permission.for(manager: user)
                  .where(manageable_type: Permission::MANAGEABLE_TYPE_ORGANIZATION)
                  .map(&:manageable)
                  .map(&:name)
                  .join(', ')
      end
    end
  end
end
