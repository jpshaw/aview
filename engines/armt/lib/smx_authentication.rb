require 'radiustar'

# Builds and runs an authentication request with AT&T's SMX via RADIUS. It uses
# radiustar, a pure ruby RADIUS library modified to work with JRuby.
class SmxAuthentication

  # RADIUS
  ACCESS_ACCEPT = 'Access-Accept'
  ACCESS_REJECT = 'Access-Reject'
  AUTH_LEVEL    = 'Accelecon/Auth-Level'
  AUTH_TIMEOUT  = 5 # seconds
  USER_OPTS     = { 'NAS-Identifier' => 'ANMS' }.freeze

  # Auth levels
  NO_AUTH_LEVEL       = 0
  ATT_CONTROLLER      = 1
  CUSTOMER_CONTROLLER = 2
  CUSTOMER_VIEWER     = 3
  ATT_VIEWER          = 4

  # Logging
  LOG_NAME     = 'smx_auth.log'
  LOG_DAYS     = 5
  LOG_MAX_SIZE = 10.megabytes
  LOG_FORMAT   = '%Y-%m-%d %H:%M:%S'

  attr_reader :username, :account, :password, :secret, :servers, :dict_path,
    :timeout, :success, :valid, :auth_level

  def self.perform(opts = {})
    new(opts).send(:perform)
  end

  def success?
    success == true
  end

  def valid?
    valid == true
  end

  protected

  attr_writer :auth_level, :success

  class InvalidDictionaryPathError < StandardError; end

  def initialize(opts = {})
    @username   = opts[:username]
    @account    = opts[:account_name]
    @password   = opts[:password]
    @secret     = opts[:auth_secret]     || Settings.auth.secret
    @servers    = opts[:servers]         || Settings.auth.servers
    @dict_path  = opts[:dictionary_path] || Settings.auth.dictionary_path
    @timeout    = opts[:timeout]         || AUTH_TIMEOUT
    @success    = false
    @valid      = false
    @auth_level = nil
  end
  private_class_method :new

  def dictionary
    @dictionary ||= Radiustar::Dictionary::new(dict_path)
  end

  def perform
    begin
      self.validate!
      self.run
    rescue => e
      log_exception(e)
    end

    self
  end

  def validate!
    raise ArgumentError.new('Username cannot be blank')         if username.blank?
    raise ArgumentError.new('Account cannot be blank')          if account.blank?
    raise ArgumentError.new('Password cannot be blank')         if password.blank?
    raise ArgumentError.new('Auth Secret cannot be blank')      if secret.blank?
    raise ArgumentError.new('No RADIUS servers are configured') if servers.blank?

    unless File.directory?(dict_path)
      message = "Failed to find RADIUS dictionary path: #{dict_path}"
      raise InvalidDictionaryPathError.new(message)
    end

    @valid = true
  end

  def run
    return unless valid?

    # SMX users are formatted as ACCOUNT.USER
    smx_user = "#{account}.#{username}".upcase
    smx_pass = "#{password}"

    logger.info "Attempting to authenticate user #{smx_user}"

    # Randomly cycle through each server
    servers.shuffle.each do |server|
      begin
        # Create a new RADIUS request.
        request = Radiustar::Request.new(server, { dict: dictionary, reply_timeout: timeout })

        # Run the RADIUS request, storing the response.
        response = request.authenticate(smx_user, smx_pass, secret, USER_OPTS)

        case response[:code]
        when ACCESS_ACCEPT

          self.auth_level = response[AUTH_LEVEL].to_i

          if auth_level != NO_AUTH_LEVEL
            self.success = true
            logger.info "Successfully authenticated user: #{smx_user}, " \
                        "auth level: #{auth_level}"
          else
            logger.error "Authenticated user #{smx_user} does not have an " \
                         "auth level. Please set one in SMX."
          end

        else
          logger.error "SMX Authentication failed for user: #{smx_user}, " \
                       "server: #{server}, response: #{response}"
        end

        # Don't try to authenticate to any more servers if we were able to
        # interact with one without any exceptions.
        break

      rescue Errno::ECONNREFUSED
        logger.error "Unable to reach SMX server: #{server}"
      rescue => e
        log_exception(e)
      end
    end
  end

  def log_exception(e)
    logger.error("#{e.class} - #{e.message}\n#{e.backtrace[0,5].join("\n")}\n")
  end

  def logger
    self.class.logger
  end

  def self.logger
    @logger ||= begin
      logger = ::Logger.new(log_path, LOG_DAYS, LOG_MAX_SIZE)
      logger.datetime_format = LOG_FORMAT
      logger.formatter = proc do |severity, datetime, progname, msg|
        "#{datetime} [#{severity}]: #{msg}\n"
      end
      logger
    end
  end

  def self.log_path
    @log_path ||= Rails.root.join('log', LOG_NAME).to_s
  end

end
