class Role < ActiveRecord::Base
  CONTROLLER = 'Controller'.freeze
  VIEWER     = 'Viewer'.freeze

  def self.scoped_to_organization?
    false
  end

  def self.controller
    where(name: CONTROLLER).first_or_create do |role|
      role.organization = Organization.root
    end
  end

  def self.viewer
    where(name: VIEWER).first_or_create do |role|
      role.organization = Organization.root
    end
  end
end
