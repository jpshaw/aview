class Organization < ActiveRecord::Base
  def admin_role
    Role.controller
  end

  class << self
    def find_or_create_by_name!(name)
      find_by_import_name_or_name(name) || create_by_name!(name)
    end

    private

    def create_by_name!(name)
      organization = Organization.create!(
        name:        name,
        import_name: name,
        parent_id:   root.id
      )

      Permission.create!(
        manager:    organization,
        manageable: organization,
        role:       organization.admin_role
      )

      organization
    end
  end
end
