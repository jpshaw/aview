class EnginePolicy

  attr_reader :user

  def initialize(user)
    @user = user
  end

  def user_is_controller?
    return @user_is_controller unless @user_is_controller.nil?
    @user_is_controller = user.respond_to?(:controller?) && user.controller?
    @user_is_controller
  end

  alias_method :modify_device_hosts?,        :user_is_controller?
  alias_method :modify_device_contacts?,     :user_is_controller?
  alias_method :modify_device_site?,         :user_is_controller?
  alias_method :modify_device_organization?, :user_is_controller?
  alias_method :modify_device_comment?,      :user_is_controller?

end
