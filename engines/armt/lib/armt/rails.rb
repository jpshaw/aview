require 'rails'
require 'devise/strategies/smx_authenticatable'
require 'armt/auth'

module ARMT
  class Engine < ::Rails::Engine

    # Autoload paths for AT&T TorqueBox Services
    initializer 'torquebox.services.autoload', before: :set_autoload_paths do |app|
      services_path = File.expand_path('../../../app/services', __FILE__)
      app.config.autoload_paths << services_path
    end

    initializer 'anms.user.patch' do
      config.after_initialize do
        User.send(:include, ARMT::Auth)
      end
    end

    config.after_initialize do
      require 'armt/role'
      require 'armt/organization'
      require 'armt/permission'
      require 'armt/closure_tree'
      require 'armt/engine_policy'
    end
  end
end
