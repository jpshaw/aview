class Permission < ActiveRecord::Base

  unless Rails.env.test?
    # Stop this validation from running
    def managed_by_manager_organization

    end
  end
end
