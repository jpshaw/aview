module ARMT
  module Auth
    extend ActiveSupport::Concern

    require 'smx_authentication'
    require 'cross_account_query'

    ATT_AUTH_LEVELS      = [SmxAuthentication::ATT_CONTROLLER,
                            SmxAuthentication::ATT_VIEWER].freeze
    CUSTOMER_AUTH_LEVELS = [SmxAuthentication::CUSTOMER_CONTROLLER,
                            SmxAuthentication::CUSTOMER_VIEWER].freeze
    CONTROLLERS          = [SmxAuthentication::ATT_CONTROLLER,
                            SmxAuthentication::CUSTOMER_CONTROLLER].freeze
    VIEWERS              = [SmxAuthentication::ATT_VIEWER,
                            SmxAuthentication::CUSTOMER_VIEWER].freeze

    AUTH_LEVELS          = (ATT_AUTH_LEVELS + CUSTOMER_AUTH_LEVELS).freeze

    included do
      # TODO: remove `attr_accessible` and use strong params
      attr_accessible :role_id
    end

    def email_required?
      false
    end

    def password_required?
      false
    end

    def controller?
      CONTROLLERS.include?(self.role_id)
    end

    def viewer?
      VIEWERS.include?(self.role_id)
    end

    def att_user?
      ATT_AUTH_LEVELS.include?(self.role_id)
    end

    def customer_user?
      CUSTOMER_AUTH_LEVELS.include?(self.role_id)
    end

    def role
      case self.role_id
      when SmxAuthentication::ATT_CONTROLLER, SmxAuthentication::CUSTOMER_CONTROLLER
        Role.controller
      when SmxAuthentication::ATT_VIEWER, SmxAuthentication::CUSTOMER_VIEWER
        Role.viewer
      end
    end

    def smx_authentication(params)
      authenticator = SmxAuthentication.perform(params)

      if authenticator.success?
        self.role_id = authenticator.auth_level
        true
      else
        false
      end
    end

    def after_database_authentication
      set_authorizations
    end
    alias_method :after_authentication, :after_database_authentication

    def set_authorizations
      # Track permission ids. It is possible that a user's privileges may change,
      # so we will need to update accordingly.
      permission_ids = []

      # Set up the user's permission on it's own organization.
      permission = permission_for_organization(organization)
      permission_ids << permission.id if permission and permission.id

      # If the user is a customer, get any additional organizations and
      # add permissions for them.
      if customer_user?
        # Get a list of account names the customer is authorized to manage.
        account_names = smx_accounts_for_account(account_name)

        # Create permissions for each organization the user can manage.
        account_names.each do |account_name|
          organization = Organization.find_by_import_name_or_name(account_name)

          next unless organization.present?

          permission = permission_for_organization(organization)
          permission_ids << permission.id if permission and permission.id
        end
      end

      # Remove any permissions the user is not authorized for.
      if permission_ids.any?
        Permission.for(manager: self).where.not(id: permission_ids).destroy_all
      end
    end

    class_methods do
      def valid_auth_level?(level)
        AUTH_LEVELS.include?(level.to_i)
      end

      def for_smx_auth(params = {})
        auth_level   = params[:auth_level].to_i
        account      = params[:account].to_s.upcase
        username     = params[:username].to_s.upcase
        principal_id = "#{account}.#{username}"

        # Set the user's organization to root if they are from AT&T
        organization = \
          if ATT_AUTH_LEVELS.include?(auth_level)
            Organization.root
          else
            Organization.find_or_create_by_name!(account)
          end

        user = User.find_by(username: principal_id)

        if user.nil?
          user              = User.new
          user.organization = organization
          user.username     = principal_id
          user.account_name = account
        end

        # Check if the user's organization has changed.
        unless user.organization == organization
          user.organization = organization
        end

        user.role_id = auth_level
        user.save
        user
      end
    end

    private

    # Find or create a permission for the user and organization, and return it.
    # Nil is returned if a race condition occurs and is not resolved after the
    # retries.
    def permission_for_organization(organization)
      # Retry permission fetch up to 3 times
      retries ||= 3

      permission = Permission.for(manager: self, manageable: organization).first_or_initialize

      # Roles for a user might change, ensure the permission is in sync
      permission.role = self.role
      permission.save
      permission

    rescue ActiveRecord::RecordNotUnique
      retry unless (retries -= 1).zero?
    end

    # Given a user's account name, this will return a list of account names
    # that are authorized to be managed by it.
    def smx_accounts_for_account(account_name)
      list = []

      begin
        accounts = CrossAccountQuery.accounts_for_user(account_name)

        if accounts.present?
          list += accounts
        end
      rescue => e
        Rails.logger.error "Cross account query failed for account: " \
                             "#{account_name}, error: #{e.message}"
      end

      list.flatten
    end
  end
end
