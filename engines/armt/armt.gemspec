lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name        = 'armt'
  s.version     = '1.0'
  s.authors     = ["Accelerated Concepts"]
  s.email       = ["aview@accelerated.com"]
  s.homepage    = "https://armt.att.com"
  s.summary     = "An engine used to run armt"
  s.description = "An engine used to run armt"

  s.files = Dir['armt.gemspec', 'torquebox.yml', 'app/**/*', 'config/**/*', 'lib/**/*']

  s.require_paths = %w(lib)

  s.add_dependency 'rails', '~> 4.0'
  s.add_runtime_dependency 'ipaddr_extensions', '~> 1.0.0'
end
