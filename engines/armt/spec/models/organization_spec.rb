require 'rails_helper'

describe Organization do

  describe '.find_or_create_by_name!' do
    subject { described_class }

    let!(:root)  { create(:organization, name: 'root') }
    let(:name)   { 'test' }
    let(:result) { subject.find_or_create_by_name!(name) }

    context 'when the organization exists' do
      let!(:organization) { create(:organization, name: name) }

      it 'finds the organization' do
        expect(result).to eq organization
      end
    end

    context 'when the organization does not exist' do
      it 'creates the organization' do
        expect { result }.to change { Organization.count }.by(1)
      end

      it 'sets the organization parent to root' do
        expect(result.parent).to eq root
      end
    end
  end

end
