require 'rails_helper'

describe User do
  subject { create(:user) }

  describe 'validates notify_enabled_for_emails' do
    context 'when email is not set and notify enabled is true' do
      let(:user) { build(:user, email: nil, notify_enabled: true) }

      it 'is not valid' do
        expect(user).to_not be_valid
      end

      it 'has an error when saved' do
        user.save
        expect(user.errors.first).to eq ([:notify_enabled, "can't be set without a valid Email"])
      end
    end

    context 'when email is set' do
      let(:user) { build(:user, email: 'test@example.com', notify_enabled: true) }

      it 'is valid' do
        expect(user).to be_valid
      end
    end
  end

  describe '#after_database_authentication' do
    it 'calls set_authorizations' do
      expect(subject).to receive(:set_authorizations)
      subject.after_database_authentication
    end
  end

  describe '#set_authorizations' do
    subject { user.set_authorizations }

    describe "sets the user's permissions on its own organization" do
      context 'when permission exists' do
        let!(:user) { create(:user, role_id: role.id, organization: organization) }
        let(:organization) { create(:organization) }
        let(:role) { create(:role, organization_id: organization.id) }

        context 'and role is the same' do
          before do
            stub_const('SmxAuthentication::CUSTOMER_VIEWER', role.id)
            stub_const('ARMT::Auth::CUSTOMER_AUTH_LEVELS', [role.id])
            expect(CrossAccountQuery).to receive(:accounts_for_user)
            create(:permission, manager: user, manageable: organization, role: role)
          end

          it 'permission is not updated' do
            expect{ subject }.not_to change{ Permission.for(manager: user, manageable: user.organization).first }
          end
        end

        context 'and role is different' do
          let!(:user) { create(:user, role_id: role.id, organization: organization) }
          let(:organization) { create(:organization) }
          let(:role) { create(:viewer_role, organization_id: organization.id) }
          let!(:permission) { create(:permission, manager: user, manageable: organization, role: create(:role, name: 'NOT VIEWER')) }

          before do
            stub_const('SmxAuthentication::CUSTOMER_VIEWER', role.id)
            stub_const('ARMT::Auth::CUSTOMER_AUTH_LEVELS', [role.id])
            expect(CrossAccountQuery).to receive(:accounts_for_user)
          end

          it 'updates permission to new role' do
            expect(permission.role).not_to eq role
            subject
            permission.reload
            expect(permission.role).to eq role
          end
        end
      end

      context 'when permission does not exist' do
        let(:user) { create(:user, role_id: role.id, organization: organization) }
        let(:organization) { create(:organization) }
        let(:role) { create(:role, organization_id: organization.id) }

        before do
          stub_const('SmxAuthentication::CUSTOMER_VIEWER', role.id)
          stub_const('ARMT::Auth::CUSTOMER_AUTH_LEVELS', [role.id])
          expect(CrossAccountQuery).to receive(:accounts_for_user)
        end

        it 'creates permission' do
          expect{ subject }.to change{ Permission.for(manager: user, manageable: organization).first.present? }
        end
      end
    end

    context 'if user is a customer user' do
      let!(:user) { create(:user, role_id: role.id, organization: organization) }
      let(:organization) { create(:organization) }
      let(:role) { create(:role) }
      let(:organizations) { [].tap{|result| 3.times{ result << create(:organization) }} }
      let(:account_names) { organizations.map(&:name) }

      before do
        stub_const('SmxAuthentication::CUSTOMER_VIEWER', role.id)
        stub_const('ARMT::Auth::CUSTOMER_AUTH_LEVELS', [role.id])
        expect(CrossAccountQuery).to receive(:accounts_for_user).and_return(account_names)
        organizations.each do |org|
          organization.children << org
        end
      end

      it 'creates permissions for each organization the user can manage' do
        subject
        organizations.each do |organization|
          expect(Permission.for(manager: user, manageable: organization).first).to be
        end
      end
    end

    context 'cleanup' do
      let!(:user) { create(:user, role_id: role.id, organization: organization) }
      let(:old_organization) { create(:organization) }
      let(:organization) { create(:organization) }
      let(:role) { create(:role) }

      before do
        stub_const('SmxAuthentication::CUSTOMER_VIEWER', role.id)
        stub_const('ARMT::Auth::CUSTOMER_AUTH_LEVELS', [role.id])
        expect(CrossAccountQuery).to receive(:accounts_for_user)
        build(:permission, manager: user, manageable: old_organization).save(validate: false)
      end

      it 'deletes any permissions no longer granted' do
        expect{ subject }.to change{ Permission.for(manager: user, manageable: old_organization).first }
      end
    end
  end

  describe '#permission_for_organization' do
    let(:organization) { create(:organization) }
    let(:user)         { create(:user, organization_id: organization.id) }
    let(:role)         { create(:role, organization_id: organization.id) }
    let(:user_role)    { create(:role, organization_id: organization.id) }
    let(:result)       { user.send(:permission_for_organization, organization) }

    before do
      allow(user).to receive(:role).and_return(user_role)
    end

    context 'when the permission exists' do
      let!(:permission) {
        create(:permission, manager: user, manageable: organization, role: role)
      }

      it 'does not create a new permission' do
        expect { result }.to_not change { Permission.count }
      end

      it 'returns the permission' do
        expect(result).to eq permission
      end

      it 'changes the permission role to the user role' do
        expect { result }.to change { permission.reload.role }.to user_role
      end
    end

    context 'when the permission does not exist' do
      before do
        expect(user.manageable_organizations).to_not include(organization)
      end

      it 'creates a permission' do
        expect { result }.to change { Permission.count }.by(1)
      end

      it 'returns a permission' do
        expect(result).to be_present
      end

      it 'sets the permission role to the user role' do
        expect(result.role).to eq user_role
      end
    end

    context 'when a race condition occurs' do
      let(:permission)       { double('permission') }
      let(:permission_query) { class_double('Permission') }

      before do
        allow(Permission).to receive(:for).and_return(permission_query)
        allow(permission_query).to receive(:first_or_initialize).and_return(permission)
        allow(permission).to receive(:role=).and_return(nil)
      end

      context 'when the exception is only thrown once' do
        before do
          @retry_count = 0
          allow(permission).to receive(:save) do
            @retry_count += 1
            raise ActiveRecord::RecordNotUnique.new('error') if @retry_count == 1
          end
        end

        it 'calls the code twice' do
          expect { result }.to change { @retry_count }.to(2)
        end

        it 'returns the permission' do
          expect(result).to eq permission
        end
      end

      context 'when the exception is always thrown' do
        before do
          @retry_count = 0
          allow(permission).to receive(:save) do
            @retry_count += 1
            raise ActiveRecord::RecordNotUnique.new('error')
          end
        end

        it 'calls the code three times' do
          expect { result }.to change { @retry_count }.from(0).to(3)
        end

        it 'returns nil' do
          expect(result).to be nil
        end
      end
    end
  end
end
