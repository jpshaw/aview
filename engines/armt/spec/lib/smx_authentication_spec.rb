require 'rails_helper'

describe SmxAuthentication do

  subject { SmxAuthentication }

  describe '.perform' do
    context 'when given an empty hash' do
      it 'is not successful' do
        expect(subject.perform({}).success?).to be false
      end

      it 'is invalid' do
        expect(subject.perform({}).valid?).to be false
      end

      it 'logs an error' do
        expect(subject.logger).to receive(:error)
        subject.perform({})
      end
    end

    context 'when given valid data' do
      let(:data) {
        {
          username:     'user',
          account_name: 'account',
          password:     'password',
          auth_secret:  'secret',
          servers:      ['127.0.0.1']
        }
      }

      it 'logs the auth attempt for the user' do
        expect(subject.logger).to receive(:info).with('Attempting to authenticate user ACCOUNT.USER')
        subject.perform(data)
      end

      it 'is valid' do
        expect(subject.perform(data).valid?).to be true
      end

      context 'when the server is unreachable' do
        it 'logs an error indicating it cannot reach the server' do
          expect(subject.logger).to receive(:error).with('Unable to reach SMX server: 127.0.0.1')
          subject.perform(data)
        end
      end

      context 'when the server is reachable' do
        context 'when the user authentication is successful' do
          let(:response) {
            {
              code: SmxAuthentication::ACCESS_ACCEPT,
              SmxAuthentication::AUTH_LEVEL => SmxAuthentication::ATT_CONTROLLER
            }
          }

          before do
            allow_any_instance_of(Radiustar::Request).to receive(:authenticate).and_return(response)
          end

          it 'is successful' do
            expect(subject.perform(data)).to be_a_success
          end

          it 'sets the auth level' do
            expect(subject.perform(data).auth_level).to eq SmxAuthentication::ATT_CONTROLLER
          end

          # This tests smx users that don't have an auth level set.
          context 'when the auth level is blank' do
            before do
              response[SmxAuthentication::AUTH_LEVEL] = nil
            end

            it 'is not successful' do
              expect(subject.perform(data).success?).to be false
            end

            it 'sets the auth level to `no auth`' do
              expect(subject.perform(data).auth_level).to eq SmxAuthentication::NO_AUTH_LEVEL
            end

            it 'logs an error indicating the user does not have an auth level' do
              expect(subject.logger).to receive(:error).with(/does not have an auth level/)
              subject.perform(data)
            end
          end
        end

        context 'when the user authentication fails' do
          let(:response) {
            {
              code: SmxAuthentication::ACCESS_REJECT
            }
          }

          before do
            allow_any_instance_of(Radiustar::Request).to receive(:authenticate).and_return(response)
          end

          it 'is not successful' do
            expect(subject.perform(data).success?).to be false
          end

          it 'does not set the auth level' do
            expect(subject.perform(data).auth_level).to be nil
          end
        end
      end
    end
  end
end
