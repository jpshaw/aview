require 'cross_account_server'

# Public: A service that synchronously processes AT&T cross account requests.
#
# Examples
#
#   queue = TorqueBox.fetch('/queues/xaccount_requests')
#   queue.publish_and_receive("ATAP", :timeout => 5000)
#   # => ["AARON", "BTAP", "CDGRN"]
class CrossAccountService

  def initialize(opts = {})
    @logger    = TorqueBox::Logger.new(self.class)
    @queue     = TorqueBox.fetch('/queues/xaccount_requests')
    @db_user   = Settings.xaccount.user
    @db_pass   = Settings.xaccount.password
    @db_port   = Settings.xaccount.port
    @servers   = Settings.xaccount.servers
    @connected = false
  end

  # Public: Starts the cross account service.
  def start
    @logger.info "Starting AT&T Cross Account service ..."
    @done = false
    Thread.new { run }
  end

  # Public: Stops the cross account service.
  def stop
    @logger.info "Stopping AT&T Cross Account service ..."
    @done = true
  end

  protected

  # Internal: The thread that runs the cross account query service. Requests are polled,
  # and if one is present, a response is sent back synchronously.
  def run
    until @done
      begin
        @queue.receive_and_publish(:timeout => 1000) do |user|
          accounts_query_for_user(user)
        end
      rescue => e
        @logger.error "Receive failed with message: #{e.message}"
      end
    end
  end

  # Internal: Check if a connection is active with the cross account server.
  def connected?
    @connected && CrossAccountServer.connection.active?
  end

  # Internal: Runs a cross account query for a user.
  #
  # Examples
  #
  #   accounts_query_for_user("ATAP")
  #   # => ["AARON", "BTAP", "CDGRN"]
  #
  # Returns an array of authorized accounts for the user.
  def accounts_query_for_user(user = "")
    accounts = []

    # Ensure a connection is available.
    setup_connection

    # Only run query if a connection is established.
    if connected?

      # Run the cross account stored procedure on the AT&T server.
      if authorized_accounts = CrossAccountServer.connection.select_all("sp_ControlledAccounts #{user}")
        authorized_accounts.each do |account|
          accounts << account["Account"]
        end
      end
    end

    accounts
  end

  # Internal: Setup a connection to an AT&T cross account server. We establish the connection
  # here so we can spin the server selection.
  def setup_connection
    server_count = @servers.count

    # Randomize server order
    if server_count > 1
      @servers.shuffle!
    end

    # Server index
    i = 0

    begin
      CrossAccountServer.establish_connection(
        :adapter   => 'mssql',
        :host      => @servers[i],
        :port      => @db_port,
        :username  => @db_user,
        :password  => @db_pass
      )
      @connected = true
    rescue => e
      i += 1
      retry if i < server_count
      @logger.error "Cross account database connection failed with message: #{e.message}"
      @connected = false
    end
  end

end
