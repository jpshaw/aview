class AddNetbridgeFirmwares < SeedMigration::Migration
  def up
    versions_3g = %w{1.542.01 1.472.92}
    versions_4g = %w{2.310.71 2.301.71 2.291.51 2.281.51 2.280.21 2.271.41 2.262.11
                     2.252.41 2.251.61 2.242.71 2.232.01 2.231.31 2.162.51}

    versions_3g.each do |version|
      DeviceFirmware.netbridge_img.where(version: version).first_or_create
    end

    versions_4g.each do |version|
      DeviceFirmware.netbridge_img_2.where(version: version).first_or_create
    end
  end

  def down
    DeviceFirmware.destroy_all
  end
end
