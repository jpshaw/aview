class EnsureRootPermissionIsSet < SeedMigration::Migration
  def up
    root = Organization.root

    # Check if root has permission on self
    permission = Permission.for(manager: root, manageable: root).first

    if permission.blank?
      Permission.create!(
        manager:    root,
        manageable: root,
        role:       root.admin_role
      )
    end
  end

  def down

  end
end
