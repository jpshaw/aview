class AddDeviceConfigurationAbilities < SeedMigration::Migration
  def up
    role      = Role.find_by_name 'Controller'
    abilities = []

    abilities << Ability.where(name: 'Modify Configurations')
                        .first_or_create!(description: 'create, delete, modify details of, device configurations',
                                             group_id: AbilityGroups::DEFAULT)

    abilities << Ability.where(name: 'Modify Device Configurations')
                        .first_or_create!(description: 'create, delete, modify details of, device configurations',
                                             group_id: AbilityGroups::DEFAULT)

    abilities.each do |ability|
      role.abilities << ability unless role.abilities.include?(ability)
    end
  end

  def down
    role      = Role.find_by_name 'Controller'
    abilities = ['Modify Configurations', 'Modify Device Configurations']

    Ability.where(name: abilities).each do |ability|
      role.abilities.delete(ability) if role.abilities.include?(ability)
    end
  end
end
