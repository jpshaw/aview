class RemoveOldAbilities < SeedMigration::Migration
    def up
      abilities = [
        'Create Boundary Organizations',
        'View Gateway Devices',
        'Replace Devices'
      ]
      Ability.where(name: abilities).destroy_all
    end

    def down
      Ability.create(name: 'Create Boundary Organizations')
      Ability.create(name: 'View Gateway Devices')
    end
end
