class AddCorrectRolesForArmt < SeedMigration::Migration
  def up
    Role.destroy_all
    root       = Organization.roots.order('id asc').first
    controller = Role.create(name: 'Controller', organization_id: root.id)
    viewer     = Role.create(name: 'Viewer',     organization_id: root.id)

    # Add abilities to controller
    abilities = [
      'Deploy Devices',
      'Send Device Commands',
      'View Admin Panel'
    ]

    Ability.where(name: abilities).each do |ability|
      controller.abilities << ability
    end
  end

  def down
    Role.destroy_all
    Role.create({
      name: 'Admin',
      description: 'can do everything with in their scope',
      admin: true }, without_protection: true)
    Role.create({
      name: 'Viewer',
      description: 'can view everything with in their scope',
      admin: false }, without_protection: true)
  end
end
