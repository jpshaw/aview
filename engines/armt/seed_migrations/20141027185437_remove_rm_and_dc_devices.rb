class RemoveRmAndDcDevices < SeedMigration::Migration
  def up
    device_categories = [RemoteManager, DialToIp]
    DeviceCategory.where(class_name: device_categories).destroy_all
  end

  def down
  end
end
