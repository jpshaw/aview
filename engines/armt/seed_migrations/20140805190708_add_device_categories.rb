class AddDeviceCategories < SeedMigration::Migration
  def up
    categories = {
      DeviceCategory::WIFI_CATEGORY => {
        :description => "NetReach device category",
        :class_name => "Netreach",
        :models => [
          { :name => "Default", :class_name => "Netreach", :model_version => "Default" },
          { :name => "1000", :class_name => "Netreach", :model_version => "1000" }
        ],
        :events => [
          { :code => EventUUIDS::NETREACH_UP,           :name => "Device recovered" },
          { :code => EventUUIDS::NETREACH_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::NETREACH_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::NETREACH_REBOOTED,     :name => "Device rebooted" },
          { :code => EventUUIDS::NETREACH_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::NETREACH_IP_CHANGED,   :name => "Device ip changed" }
        ]
      },
      DeviceCategory::USB_CELLULAR_CATEGORY => {
        :description => "NetBridge device category",
        :class_name => "Netbridge",
        :models => [
          { :name => "Default", :class_name => "Netbridge", :model_version => "Default" },
          { :name => "SX", :class_name => "Netbridge", :model_version => "SX" },
          { :name => "6200-FX", :class_name => "Netbridge", :model_version => "6200-FX" }
        ],
        :events => [
          { :code => EventUUIDS::NETBRIDGE_UP,           :name => "Device recovered" },
          { :code => EventUUIDS::NETBRIDGE_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::NETBRIDGE_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::NETBRIDGE_REBOOTED,     :name => "Device rebooted" },
          { :code => EventUUIDS::NETBRIDGE_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::NETBRIDGE_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::NETBRIDGE_CNTI_CHANGED, :name => "Device network type changed" }
        ]
      },
      DeviceCategory::DIAL_TO_IP_CATEGORY => {
        :description => "Dial-to-IP device category",
        :class_name => "DialToIp",
        :models => [
          { :name => "Default", :class_name => "DialToIp", :model_version => "Default" },
          { :name => "5300-DC", :class_name => "DialToIp", :model_version => "5300-DC" }
        ],
        :events => [
          { :code => EventUUIDS::DIALTOIP_UP,           :name => "Device recovered" },
          { :code => EventUUIDS::DIALTOIP_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::DIALTOIP_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::DIALTOIP_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::DIALTOIP_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::DIALTOIP_CNTI_CHANGED, :name => "Device cellular network type changed" },
          { :code => EventUUIDS::DIALTOIP_WAN_CHANGED,  :name => "Device WAN interface changed" }
        ]
      },
      DeviceCategory::REMOTE_MANAGER_CATEGORY => {
        description: "Remote Manager device category",
        class_name: "RemoteManager",
        models: [
          { name: "Default", class_name: "RemoteManager", model_version: "Default" },
          { name: "5300-RM", class_name: "RemoteManager", model_version: "5300-RM" }
        ],
        :events => [
          { :code => EventUUIDS::REMOTEMANAGER_UP,           :name => "Device recovered" },
          { :code => EventUUIDS::REMOTEMANAGER_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::REMOTEMANAGER_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::REMOTEMANAGER_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::REMOTEMANAGER_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::REMOTEMANAGER_CNTI_CHANGED, :name => "Device cellular network type changed" },
          { :code => EventUUIDS::REMOTEMANAGER_WAN_CHANGED,  :name => "Device WAN interface changed" }
        ]
      }
    }

    categories.each do |category_name, category_data|
      category = DeviceCategory.where(name: category_name)
                               .first_or_create(description: category_data[:description],
                                                 class_name: category_data[:class_name])

      # Model data
      category_data[:models].each do |model_data|

        # Check if model already exists for another category
        model = DeviceModel.where(name: model_data[:name],
                            class_name: model_data[:class_name])
                           .first_or_create(model_version: model_data[:model_version],
                                           category_id: category.id)

        # Assign to category
        model.category = category

        # Save if changed
        model.save if model.changed?
      end

      # Event UUIDs
      category_data[:events].each do |event|
        uuid = DeviceEventUUID.where(code: event[:code])
                              .first_or_create(name: event[:name],
                                        category_id: category.id)

        uuid.name = event[:name]
        uuid.category = category

        uuid.save if uuid.changed?
      end
    end
  end

  def down
    categories = [
      DeviceCategory::WIFI_CATEGORY,
      DeviceCategory::USB_CELLULAR_CATEGORY,
      DeviceCategory::DIAL_TO_IP_CATEGORY,
      DeviceCategory::REMOTE_MANAGER_CATEGORY
    ]
    DeviceCategory.destroy_all(name: categories)
  end
end
