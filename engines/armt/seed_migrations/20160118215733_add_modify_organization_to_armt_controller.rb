class AddModifyOrganizationToArmtController < SeedMigration::Migration

  # TODO: Refactor abilities so we don't have to use strings
  MODIFY_ORGANIZATIONS = 'Modify Organizations'

  def up
    Role.controller.abilities << ability unless Role.controller.abilities.include?(ability)
  end

  def down
    Role.controller.abilities.delete(ability) if Role.controller.abilities.include?(ability)
  end

  protected

  def ability
    Ability.find_by_name MODIFY_ORGANIZATIONS
  end
end
