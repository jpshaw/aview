class AddModifyDeviceLocationAbilityToArmt < SeedMigration::Migration
  ABILITY_NAME        = 'Modify Device Location'.freeze
  ABILITY_DESCRIPTION = 'modify a device location'.freeze

  def up
    Ability.create(name: ABILITY_NAME, description: ABILITY_DESCRIPTION)
  end

  def down
    Ability.destroy_all(name: ABILITY_NAME)
  end
end
