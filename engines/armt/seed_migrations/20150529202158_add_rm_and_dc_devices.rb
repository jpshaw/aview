class AddRmAndDcDevices < SeedMigration::Migration
  def up
    categories = {
      DeviceCategory::DIAL_TO_IP_CATEGORY => {
        :description => "Dial-to-IP device category",
        :class_name => "DialToIp",
        :models => [
          { :name => "Default", :class_name => "DialToIp", :model_version => "Default" },
          { :name => "5300-DC", :class_name => "DialToIp", :model_version => "5300-DC" }
        ],
        :events => [
          { :code => EventUUIDS::DIALTOIP_UP,           :name => "Device recovered" },
          { :code => EventUUIDS::DIALTOIP_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::DIALTOIP_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::DIALTOIP_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::DIALTOIP_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::DIALTOIP_CNTI_CHANGED, :name => "Device cellular network type changed" },
          { :code => EventUUIDS::DIALTOIP_WAN_CHANGED,  :name => "Device WAN interface changed" }
        ]
      },
      DeviceCategory::REMOTE_MANAGER_CATEGORY => {
        description: "Remote Manager device category",
        class_name: "RemoteManager",
        models: [
          { name: "Default", class_name: "RemoteManager", model_version: "Default" },
          { name: "5300-RM", class_name: "RemoteManager", model_version: "5300-RM" }
        ],
        :events => [
          { :code => EventUUIDS::REMOTEMANAGER_UP,             :name => "Device recovered" },
          { :code => EventUUIDS::REMOTEMANAGER_DOWN,           :name => "Device down" },
          { :code => EventUUIDS::REMOTEMANAGER_FW_CHANGED,     :name => "Device firmware changed" },
          { :code => EventUUIDS::REMOTEMANAGER_UNREACHABLE,    :name => "Device unreachable" },
          { :code => EventUUIDS::REMOTEMANAGER_IP_CHANGED,     :name => "Device primary ip changed" },
          { :code => EventUUIDS::REMOTEMANAGER_CNTI_CHANGED,   :name => "Device cellular network type changed" },
          { :code => EventUUIDS::REMOTEMANAGER_WAN_CHANGED,    :name => "Device WAN interface changed" },
          { :code => EventUUIDS::REMOTEMANAGER_SIGNAL_CHANGED, :name => "Device signal strength changed" },
          { :code => EventUUIDS::REMOTEMANAGER_PORT_DOWN,      :name => "Device port down" }
        ]
      }
    }

    categories.each do |category_name, category_data|
      category = DeviceCategory.where(name: category_name)
                               .first_or_create(description: category_data[:description],
                                                 class_name: category_data[:class_name])

      # Model data
      category_data[:models].each do |model_data|

        # Check if model already exists for another category
        model = DeviceModel.where(name: model_data[:name],
                            class_name: model_data[:class_name])
                           .first_or_create(model_version: model_data[:model_version],
                                           category_id: category.id)

        # Assign to category
        model.category = category

        # Save if changed
        model.save if model.changed?
      end

      # Event UUIDs
      category_data[:events].each do |event|
        uuid = DeviceEventUUID.where(code: event[:code])
                              .first_or_create(name: event[:name],
                                        category_id: category.id)

        uuid.name = event[:name]
        uuid.category = category

        uuid.save if uuid.changed?
      end
    end
  end

  def down
    categories = [
      DeviceCategory::DIAL_TO_IP_CATEGORY,
      DeviceCategory::REMOTE_MANAGER_CATEGORY
    ]
    DeviceCategory.destroy_all(name: categories)
  end
end
