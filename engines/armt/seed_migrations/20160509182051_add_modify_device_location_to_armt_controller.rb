class AddModifyDeviceLocationToArmtController < SeedMigration::Migration
  # TODO: Refactor abilities so we don't have to use strings
  MODIFY_DEVICE_LOCATION = 'Modify Device Location'

  def up
    Role.controller.abilities << ability unless Role.controller.abilities.include?(ability)
  end

  def down
    Role.controller.abilities.delete(ability) if Role.controller.abilities.include?(ability)
  end

  protected

  def ability
    @ability ||= Ability.find_by_name MODIFY_DEVICE_LOCATION
  end
end
