# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181023003043) do
  create_table "abilities", force: :cascade do |t|
    t.datetime "created_at"
    t.string "description", limit: 255
    t.integer "group_id", limit: 4, default: 0
    t.string "name", limit: 255
    t.datetime "updated_at"
  end

  create_table "abilities_roles", id: false, force: :cascade do |t|
    t.integer "ability_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "abilities_roles", ["ability_id", "role_id"], name: "idx_ablt_rls_ablt_rl", unique: true, using: :btree

  create_table "carrier_details", force: :cascade do |t|
    t.string "color", limit: 255
    t.datetime "created_at", null: false
    t.string "display_name", limit: 255, null: false
    t.string "regexp", limit: 255
    t.string "tail", limit: 255
    t.datetime "updated_at", null: false
  end

  add_index "carrier_details", ["display_name"], name: "index_carrier_details_on_display_name", unique: true, using: :btree

  create_table "carriers", force: :cascade do |t|
    t.integer "carrier_detail_id", limit: 4
    t.datetime "created_at", null: false
    t.string "name", limit: 255, null: false
    t.datetime "updated_at", null: false
  end

  add_index "carriers", ["name"], name: "index_carriers_on_name", unique: true, using: :btree

  create_table "cellular_locations", force: :cascade do |t|
    t.decimal "accuracy", precision: 10
    t.integer "cid", limit: 8
    t.datetime "created_at", null: false
    t.integer "lac", limit: 8
    t.float "lat", limit: 24
    t.float "lon", limit: 24
    t.integer "mcc", limit: 4
    t.integer "mnc", limit: 4
    t.string "radio", limit: 255
    t.datetime "updated_at", null: false
  end

  add_index "cellular_locations", ["cid"], name: "index_cellular_locations_on_cid", using: :btree
  add_index "cellular_locations", ["lac"], name: "index_cellular_locations_on_lac", using: :btree
  add_index "cellular_locations", ["mcc"], name: "index_cellular_locations_on_mcc", using: :btree
  add_index "cellular_locations", ["mnc"], name: "index_cellular_locations_on_mnc", using: :btree

  create_table "certificates", force: :cascade do |t|
    t.boolean "approved", default: false
    t.text "cert", limit: 65535
    t.integer "device_id", limit: 4
    t.boolean "exported"
    t.string "md5", limit: 255
    t.boolean "signed", default: false
  end

  add_index "certificates", ["device_id"], name: "idx_cert_dev_id", using: :btree
  add_index "certificates", ["md5"], name: "idx_cert_md5", unique: true, using: :btree

  create_table "configuration_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations", limit: 4, null: false
  end

  add_index "configuration_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "configuration_anc_desc_idx", unique: true, using: :btree
  add_index "configuration_hierarchies", ["descendant_id"], name: "configuration_desc_idx", using: :btree

  create_table "configurations", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.binary "data", limit: 16777215
    t.string "name", limit: 255
    t.integer "parent_id", limit: 4
    t.integer "resource_id", limit: 4
    t.string "resource_type", limit: 255
    t.string "type", limit: 255
    t.datetime "updated_at", null: false
  end

  add_index "configurations", ["name"], name: "index_configurations_on_name", using: :btree
  add_index "configurations", ["parent_id"], name: "index_configurations_on_parent_id", using: :btree
  add_index "configurations", ["resource_type", "resource_id"], name: "index_configurations_on_resource_type_and_resource_id", using: :btree
  add_index "configurations", ["type"], name: "index_configurations_on_type", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.datetime "created_at"
    t.string "email", limit: 255
    t.integer "label", limit: 4
    t.string "name", limit: 255
    t.string "phone", limit: 255
    t.string "token", limit: 255, null: false
    t.datetime "updated_at"
  end

  create_table "countries", force: :cascade do |t|
    t.string "code", limit: 2
    t.string "name", limit: 100
  end

  create_table "customer_services", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "data_plans", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.integer "cycle_start_day", limit: 4
    t.integer "cycle_type", limit: 4
    t.integer "data_limit", limit: 8
    t.integer "data_used", limit: 8
    t.integer "individual_data_limit", limit: 8
    t.string "name", limit: 50
    t.boolean "notify_enabled", default: false
    t.integer "organization_id", limit: 4
    t.datetime "updated_at", null: false
  end

  add_index "data_plans", ["organization_id"], name: "index_data_plans_on_organization_id", using: :btree

  create_table "device_associations", force: :cascade do |t|
    t.integer "associated_device_id", limit: 4
    t.string "associated_device_mac", limit: 255
    t.datetime "created_at"
    t.text "details", limit: 65535
    t.integer "device_id", limit: 4
    t.text "meta", limit: 65535
    t.integer "source_id", limit: 4
    t.string "source_mac", limit: 255
    t.integer "target_id", limit: 4
    t.string "target_mac", limit: 255
    t.datetime "updated_at"
  end

  add_index "device_associations", ["associated_device_id"], name: "idx_dev_asoc_dev_id", using: :btree
  add_index "device_associations", ["associated_device_mac"], name: "idx_dev_asoc_dev_mac", using: :btree
  add_index "device_associations", ["device_id"], name: "idx_dev_asoc", using: :btree
  add_index "device_associations", ["source_id"], name: "index_device_associations_on_source_id", using: :btree
  add_index "device_associations", ["source_mac"], name: "index_device_associations_on_source_mac", using: :btree
  add_index "device_associations", ["target_id"], name: "index_device_associations_on_target_id", using: :btree
  add_index "device_associations", ["target_mac"], name: "index_device_associations_on_target_mac", using: :btree

  create_table "device_categories", force: :cascade do |t|
    t.string "class_name", limit: 255
    t.integer "code", limit: 4
    t.datetime "created_at"
    t.string "description", limit: 255
    t.string "name", limit: 50
    t.datetime "updated_at"
  end

  add_index "device_categories", ["class_name"], name: "idx_dvc_cat_cls_nm", unique: true, using: :btree

  create_table "device_configurations", force: :cascade do |t|
    t.text "additional_settings", limit: 65535
    t.text "compiled_settings", limit: 65535
    t.datetime "created_at"
    t.text "description", limit: 65535
    t.integer "device_firmware_id", limit: 4
    t.integer "heartbeat_frequency", limit: 4
    t.integer "heartbeat_grace_period", limit: 4
    t.string "name", limit: 255
    t.integer "organization_id", limit: 4
    t.boolean "parent_disconnect", default: false
    t.integer "parent_id", limit: 4
    t.integer "record_type", limit: 4, default: 0
    t.datetime "updated_at"
  end

  add_index "device_configurations", ["device_firmware_id"], name: "idx_dev_conf_firm_id", using: :btree
  add_index "device_configurations", ["organization_id"], name: "idx_dev_conf_org_id", using: :btree
  add_index "device_configurations", ["parent_id"], name: "index_device_configurations_on_parent_id", using: :btree
  add_index "device_configurations", ["record_type"], name: "index_device_configurations_on_record_type", using: :btree

  create_table "device_event_uuids", force: :cascade do |t|
    t.integer "category_id", limit: 4
    t.integer "code", limit: 4
    t.string "name", limit: 255
  end

  add_index "device_event_uuids", ["category_id"], name: "idx_dev_ev_uuid_cat_id", using: :btree

  create_table "device_events", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.integer "event_type", limit: 4
    t.text "information", limit: 65535
    t.text "raw_data", limit: 65535
    t.integer "severity", limit: 4
    t.datetime "updated_at"
    t.integer "uuid_id", limit: 4
  end

  add_index "device_events", ["created_at"], name: "idx_dvc_evnt_crtd_at", using: :btree
  add_index "device_events", ["device_id", "created_at"], name: "idx_dev_evnts_dev_id_crt_at", using: :btree
  add_index "device_events", ["device_id"], name: "idx_dvc_evnt_dvc_id", using: :btree
  add_index "device_events", ["event_type"], name: "idx_dvc_evnt_evnt_typ", using: :btree
  add_index "device_events", ["severity"], name: "idx_dvc_evnt_lvl", using: :btree
  add_index "device_events", ["uuid_id"], name: "idx_dev_ev_uuid_id", using: :btree

  create_table "device_firmwares", force: :cascade do |t|
    t.datetime "created_at"
    t.boolean "deprecated", default: false
    t.integer "device_model_id", limit: 4
    t.string "firmware_file", limit: 255
    t.integer "img_type", limit: 4
    t.text "migration_versions", limit: 65535
    t.boolean "production", default: false
    t.string "schema_file", limit: 255
    t.integer "schema_version", limit: 4
    t.datetime "updated_at"
    t.string "version", limit: 20
  end

  add_index "device_firmwares", ["device_model_id"], name: "idx_dev_firm_mod_id", using: :btree

  create_table "device_models", force: :cascade do |t|
    t.integer "category_id", limit: 4
    t.string "class_name", limit: 255
    t.integer "code", limit: 4
    t.datetime "created_at"
    t.string "description", limit: 255
    t.string "model_version", limit: 20
    t.string "name", limit: 50
    t.datetime "updated_at"
  end

  add_index "device_models", ["class_name", "model_version"], name: "idx_dvc_mdl_cls_mdl", unique: true, using: :btree

  create_table "device_modems", force: :cascade do |t|
    t.boolean "active"
    t.string "apn", limit: 255
    t.string "band", limit: 255
    t.integer "bars", limit: 4
    t.string "carrier", limit: 255
    t.integer "cellular_location_id", limit: 4
    t.string "cid", limit: 20
    t.string "cnti", limit: 15
    t.datetime "created_at"
    t.integer "data_used", limit: 8
    t.integer "device_id", limit: 4
    t.string "ecio", limit: 10
    t.string "esn", limit: 20
    t.string "iccid", limit: 25
    t.string "imei", limit: 25
    t.string "imsi", limit: 25
    t.string "lac", limit: 10
    t.string "maker", limit: 255
    t.string "mcc", limit: 10
    t.string "mnc", limit: 10
    t.string "model", limit: 20
    t.string "name", limit: 255
    t.string "number", limit: 20
    t.string "revision", limit: 255
    t.string "rsrp", limit: 10
    t.string "rsrq", limit: 10
    t.string "rssi", limit: 10
    t.integer "rx", limit: 8
    t.integer "rx_30", limit: 8
    t.integer "signal", limit: 1
    t.integer "sim_slot", limit: 4
    t.string "sinr", limit: 10
    t.string "sku", limit: 255
    t.datetime "sms_sent_at"
    t.string "snr", limit: 10
    t.integer "tx", limit: 8
    t.integer "tx_30", limit: 8
    t.datetime "updated_at"
    t.string "usb_speed", limit: 10
  end

  add_index "device_modems", ["cellular_location_id"], name: "index_device_modems_on_cellular_location_id", using: :btree
  add_index "device_modems", ["device_id"], name: "idx_nb_mdm_dev_id", using: :btree

  create_table "device_networks", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.string "gw0", limit: 255
    t.string "gw1", limit: 255
    t.integer "index", limit: 4
    t.text "ip_addrs", limit: 65535
    t.integer "metric0", limit: 4
    t.string "mtu", limit: 255
    t.string "mtu_ipv6", limit: 255
    t.string "net_type", limit: 255
    t.string "network_iface", limit: 20
    t.string "priority", limit: 255
    t.integer "rx", limit: 8
    t.string "speed", limit: 255
    t.string "status", limit: 255
    t.integer "tx", limit: 8
    t.datetime "updated_at"
  end

  add_index "device_networks", ["device_id", "network_iface"], name: "index_device_networks_on_device_id_and_network_iface", unique: true, using: :btree

  create_table "device_reports", force: :cascade do |t|
    t.text "chart_data", limit: 65535
    t.datetime "created_at"
    t.datetime "finished_at"
    t.text "params", limit: 65535
    t.binary "results", limit: 16777215
    t.string "results_file", limit: 255
    t.datetime "updated_at"
    t.integer "user_id", limit: 4
  end

  create_table "device_states", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.integer "lock_version", limit: 4
    t.integer "state", limit: 4
    t.datetime "updated_at"
  end

  add_index "device_states", ["device_id"], name: "idx_dvc_stt_dvc_id", unique: true, using: :btree
  add_index "device_states", ["state"], name: "idx_dvc_stt_stt", using: :btree

  create_table "devices", force: :cascade do |t|
    t.datetime "activated_at"
    t.integer "category_id", limit: 4
    t.string "comment", limit: 255
    t.datetime "configuration_associated_at"
    t.datetime "configuration_checked_at"
    t.integer "configuration_id", limit: 4
    t.datetime "created_at"
    t.integer "data_plan_id", limit: 4
    t.integer "device_status", limit: 4
    t.string "dns1", limit: 255
    t.string "dns2", limit: 255
    t.string "domain_name", limit: 255
    t.boolean "dual_wan_enabled", default: false
    t.string "firmware", limit: 50
    t.string "firmware_rollback", limit: 255
    t.integer "group_id", limit: 4
    t.string "host", limit: 45
    t.string "hostname", limit: 255
    t.string "hw_version", limit: 20
    t.boolean "is_deployed", default: false
    t.datetime "last_heartbeat_at"
    t.datetime "last_restarted_at"
    t.integer "lifetime", limit: 4, default: 600, null: false
    t.integer "location_id", limit: 4
    t.string "mac", limit: 12
    t.string "mgmt_host", limit: 45
    t.integer "model_id", limit: 4
    t.string "node_name", limit: 255
    t.string "object_type", limit: 255
    t.integer "organization_id", limit: 4
    t.string "primary_wan_iface", limit: 20
    t.integer "probe_mask", limit: 8, default: 0
    t.datetime "probed_at"
    t.integer "replaced_with_id", limit: 4
    t.datetime "retried_at"
    t.integer "retry_count", limit: 4, default: 0
    t.string "serial", limit: 16
    t.integer "site_id", limit: 4
    t.integer "tunnels_count", limit: 4, default: 0
    t.datetime "unreachable_at"
    t.datetime "updated_at"
  end

  add_index "devices", ["category_id"], name: "idx_dvc_cat_id", using: :btree
  add_index "devices", ["configuration_id"], name: "index_devices_on_configuration_id", using: :btree
  add_index "devices", ["data_plan_id"], name: "index_devices_on_data_plan_id", using: :btree
  add_index "devices", ["device_status"], name: "index_devices_on_device_status", using: :btree
  add_index "devices", ["dual_wan_enabled"], name: "index_devices_on_dual_wan_enabled", using: :btree
  add_index "devices", ["group_id"], name: "index_devices_on_group_id", using: :btree
  add_index "devices", ["is_deployed"], name: "idx_dvc_is_dpld", using: :btree
  add_index "devices", ["location_id"], name: "idx_dvc_lctn_id", using: :btree
  add_index "devices", ["mac"], name: "idx_dvc_mac", unique: true, using: :btree
  add_index "devices", ["model_id"], name: "idx_dvc_mdl_id", using: :btree
  add_index "devices", ["organization_id"], name: "idx_dvc_org_id", using: :btree
  add_index "devices", ["primary_wan_iface"], name: "idx_dvc_wan_iface", using: :btree
  add_index "devices", ["serial"], name: "idx_dvc_srl", using: :btree
  add_index "devices", ["site_id"], name: "idx_dvc_st_id", using: :btree

  create_table "devices_contacts", force: :cascade do |t|
    t.integer "contact_id", limit: 4
    t.integer "device_id", limit: 4
  end

  add_index "devices_contacts", ["device_id", "contact_id"], name: "idx_dvc_con_dvc_con", unique: true, using: :btree

  create_table "devices_tunnel_servers", id: false, force: :cascade do |t|
    t.integer "device_id", limit: 4, null: false
    t.integer "tunnel_server_id", limit: 4, null: false
  end

  add_index "devices_tunnel_servers", ["device_id", "tunnel_server_id"], name: "index_devices_tunnel_servers_on_device_id_and_tunnel_server_id", using: :btree
  add_index "devices_tunnel_servers", ["tunnel_server_id", "device_id"], name: "index_devices_tunnel_servers_on_tunnel_server_id_and_device_id", using: :btree

  create_table "dial_test_results", force: :cascade do |t|
    t.integer "category", limit: 4
    t.datetime "created_at", null: false
    t.integer "device_id", limit: 4
    t.string "reason", limit: 255
    t.integer "result", limit: 4
    t.integer "source", limit: 4
    t.datetime "updated_at", null: false
  end

  add_index "dial_test_results", ["device_id"], name: "index_dial_test_results_on_device_id", using: :btree

  create_table "dns_records", force: :cascade do |t|
    t.integer "change_date", limit: 4
    t.string "content", limit: 255
    t.integer "domain_id", limit: 4
    t.string "name", limit: 255
    t.integer "prio", limit: 4
    t.string "record_type", limit: 255
    t.integer "ttl", limit: 4
    t.string "type", limit: 255
  end

  add_index "dns_records", ["domain_id"], name: "idx_dns_domain_id", using: :btree
  add_index "dns_records", ["name", "type"], name: "idx_dns_name_type", using: :btree

  create_table "domains", force: :cascade do |t|
    t.string "account", limit: 40
    t.integer "last_check", limit: 4
    t.string "master", limit: 128
    t.string "name", limit: 255, null: false
    t.integer "notified_serial", limit: 4
    t.string "type", limit: 6, null: false
  end

  add_index "domains", ["name"], name: "idx_pdns_name", unique: true, using: :btree

  create_table "gateway_configurations", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "organization_id", limit: 4
    t.integer "portal_id", limit: 4
    t.boolean "snmp_enabled", default: false
    t.integer "snmp_id", limit: 4
    t.datetime "updated_at"
  end

  add_index "gateway_configurations", ["organization_id"], name: "idx_gat_conf_org_id", using: :btree
  add_index "gateway_configurations", ["portal_id"], name: "index_gateway_configurations_on_portal_id", using: :btree
  add_index "gateway_configurations", ["snmp_id"], name: "index_gateway_configurations_on_snmp_id", using: :btree

  create_table "gateway_details", force: :cascade do |t|
    t.string "active_conn_type", limit: 255
    t.string "active_wan_iface", limit: 255
    t.string "active_wan_ip", limit: 255
    t.datetime "backup_started_at"
    t.string "configuration_name", limit: 255
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.boolean "dial_backup_enabled", default: false
    t.string "dial_backup_type", limit: 255
    t.string "dial_mode", limit: 255
    t.string "dial_modem_type", limit: 255
    t.datetime "dial_started_at"
    t.datetime "last_trap_at"
    t.integer "time_in_dial", limit: 4, default: 0
    t.integer "tunnels_status", limit: 4
    t.integer "tunnels_total_count", limit: 4
    t.integer "tunnels_total_up", limit: 4
    t.datetime "updated_at"
    t.string "vrrp_state", limit: 255
    t.string "wan_1_cell_extender", limit: 2
    t.string "wan_1_circuit_type", limit: 255
    t.integer "wan_1_conn_type", limit: 4
    t.string "wan_2_cell_extender", limit: 2
    t.integer "wan_ipv4_state", limit: 4
    t.integer "wan_ipv6_state", limit: 4
  end

  add_index "gateway_details", ["device_id"], name: "idx_gtw_dtl_dvc_id", using: :btree

  create_table "gateway_portal_profiles", force: :cascade do |t|
    t.string "auth_password", limit: 16
    t.string "auth_user", limit: 8, default: "support"
    t.datetime "created_at"
    t.string "password", limit: 255
    t.string "ssl_password", limit: 255
    t.integer "ssl_port", limit: 4, default: 443
    t.datetime "updated_at"
  end

  create_table "gateway_tunnels", force: :cascade do |t|
    t.string "account", limit: 20
    t.boolean "agns_managed", default: false
    t.integer "auth_protocol", limit: 4
    t.integer "auth_type", limit: 4
    t.string "connection_id", limit: 50
    t.integer "connection_type", limit: 4
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.integer "duration", limit: 4
    t.string "endpoint", limit: 15
    t.string "endpoint_ipv6", limit: 50
    t.integer "endpoint_type", limit: 4
    t.integer "initiator", limit: 4
    t.string "ipsec_iface", limit: 10
    t.string "reason", limit: 50
    t.datetime "started_at"
    t.integer "tunnel_mode", limit: 4
    t.string "tunnel_name", limit: 255, default: "0"
    t.integer "tunnel_server_id", limit: 4
    t.datetime "updated_at"
    t.string "user", limit: 20
    t.integer "wan_conn_method", limit: 4
  end

  add_index "gateway_tunnels", ["device_id"], name: "idx_gtw_tnl_dvc_id", using: :btree
  add_index "gateway_tunnels", ["tunnel_server_id"], name: "index_gateway_tunnels_on_tunnel_server_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.integer "device_model_id", limit: 4
    t.integer "firmware_id", limit: 4
    t.string "name", limit: 255
    t.integer "organization_id", limit: 4
    t.datetime "updated_at", null: false
    t.string "uuid", limit: 255
  end

  add_index "groups", ["device_model_id"], name: "index_groups_on_device_model_id", using: :btree
  add_index "groups", ["firmware_id"], name: "index_groups_on_firmware_id", using: :btree
  add_index "groups", ["organization_id"], name: "index_groups_on_organization_id", using: :btree

  create_table "ids_alerts", force: :cascade do |t|
    t.datetime "alerted_at"
    t.text "classification", limit: 65535
    t.string "dst", limit: 255
    t.integer "dstport", limit: 4
    t.string "host", limit: 255
    t.string "interface", limit: 255
    t.text "msg", limit: 65535
    t.integer "priority", limit: 4
    t.string "proto", limit: 255
    t.text "reference", limit: 16777215
    t.integer "sig_gen", limit: 4
    t.integer "sig_id", limit: 4
    t.integer "sig_rev", limit: 4
    t.string "src", limit: 255
    t.integer "srcport", limit: 4
  end

  add_index "ids_alerts", ["alerted_at"], name: "index_ids_alerts_on_alerted_at", using: :btree
  add_index "ids_alerts", ["host"], name: "index_ids_alerts_on_host", using: :btree
  add_index "ids_alerts", ["sig_id"], name: "index_ids_alerts_on_sig_id", using: :btree
  add_index "ids_alerts", ["sig_rev"], name: "index_ids_alerts_on_sig_rev", using: :btree

  create_table "imports", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "finished_at"
    t.string "import_file", limit: 255
    t.string "log_path", limit: 255
    t.text "object_ids", limit: 16777215
    t.string "the_action", limit: 20
    t.string "the_source_name", limit: 20
    t.string "the_state", limit: 20
    t.string "the_trigger", limit: 20
    t.string "the_type", limit: 20
    t.datetime "updated_at"
  end

  create_table "locations", force: :cascade do |t|
    t.string "address_1", limit: 150, default: ""
    t.string "address_2", limit: 50, default: ""
    t.string "address_checksum", limit: 32
    t.string "city", limit: 100, default: ""
    t.string "coordinates_checksum", limit: 32
    t.integer "country_id", limit: 4
    t.datetime "created_at"
    t.float "f_latitude", limit: 24
    t.float "f_longitude", limit: 24
    t.string "formatted_address", limit: 255
    t.string "geocode_state", limit: 255
    t.string "input_token", limit: 32
    t.boolean "is_verified", default: false
    t.decimal "latitude", precision: 10, scale: 8
    t.decimal "longitude", precision: 11, scale: 8
    t.integer "organization_id", limit: 4
    t.string "postal_code", limit: 20
    t.string "state", limit: 255
    t.string "state_abbr", limit: 255, default: ""
    t.datetime "updated_at"
    t.string "zip", limit: 10, default: ""
  end

  add_index "locations", ["address_checksum"], name: "index_locations_on_address_checksum", using: :btree
  add_index "locations", ["coordinates_checksum"], name: "index_locations_on_coordinates_checksum", using: :btree
  add_index "locations", ["country_id"], name: "idx_lctn_ctry_id", using: :btree
  add_index "locations", ["input_token", "organization_id"], name: "idx_lctn_npt_tkn", unique: true, using: :btree

  create_table "netbridge_config_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations", limit: 4, null: false
  end

  add_index "netbridge_config_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "idx_nb_cfg_anc_desc", unique: true, using: :btree
  add_index "netbridge_config_hierarchies", ["descendant_id"], name: "idx_nb_cfg_desc_idx", using: :btree

  create_table "netbridge_configurations", force: :cascade do |t|
    t.text "additional_settings", limit: 65535
    t.string "cfg", limit: 255
    t.integer "conf_insecure_ssl", limit: 4
    t.boolean "config_change_reboot"
    t.datetime "created_at"
    t.string "crt_url", limit: 255
    t.text "data", limit: 65535
    t.string "ddns", limit: 255
    t.integer "dhcp_enable", limit: 4
    t.integer "dhcp_lease_max", limit: 4
    t.string "dlm", limit: 255
    t.string "dnslist", limit: 255
    t.string "dre", limit: 255
    t.string "drn", limit: 255
    t.string "drp", limit: 255
    t.string "drs", limit: 255
    t.string "eth", limit: 255
    t.string "firmware_server", limit: 255
    t.string "gip", limit: 255
    t.boolean "gip_secondary"
    t.integer "heartbeat_frequency", limit: 4
    t.integer "heartbeat_grace_period", limit: 4
    t.string "hostname", limit: 50
    t.string "img", limit: 255
    t.string "img2", limit: 255
    t.string "img3", limit: 255
    t.string "import_name", limit: 255
    t.boolean "ippassthrough"
    t.string "ippassthrough_old", limit: 10
    t.string "ips_cust_dh_grp", limit: 255
    t.string "ips_cust_home_ep", limit: 255
    t.string "ips_cust_home_net", limit: 255
    t.string "ips_cust_keyid_tag", limit: 255
    t.text "ips_cust_psk", limit: 65535
    t.string "ips_cust_remote_ep", limit: 255
    t.string "ips_cust_remote_net", limit: 255
    t.string "ips_cust_xauth_pass", limit: 255
    t.string "ips_cust_xauth_user", limit: 255
    t.string "ips_keepalive", limit: 255
    t.boolean "iui"
    t.binary "key_meta", limit: 65535
    t.string "link_check_freq", limit: 255
    t.integer "link_timeout", limit: 4
    t.string "log", limit: 255
    t.string "log_level", limit: 255
    t.string "log_list", limit: 255
    t.string "log_list2", limit: 255
    t.string "mtu", limit: 255
    t.string "name", limit: 50
    t.integer "organization_id", limit: 4
    t.integer "ota_updates", limit: 4
    t.integer "parent_id", limit: 4
    t.string "png", limit: 255
    t.string "proxy_arp", limit: 10
    t.string "ptf", limit: 255
    t.string "ptfr", limit: 255
    t.string "rat", limit: 255
    t.boolean "rbt"
    t.boolean "remote_control"
    t.string "sms_check_freq", limit: 255
    t.string "status_freq", limit: 255
    t.boolean "sticky_apn"
    t.string "syslog_http", limit: 255
    t.boolean "traf_acct"
    t.string "tus", limit: 255
    t.datetime "updated_at"
    t.string "upload_server", limit: 255
    t.string "usb_adjust", limit: 10
    t.string "var", limit: 255
    t.string "vpn_cust_type", limit: 255
    t.boolean "wdog"
    t.string "xma", limit: 255
    t.string "xmc", limit: 255
    t.string "xmi", limit: 255
    t.string "xmp", limit: 255
    t.string "xmt", limit: 255
    t.string "xmu", limit: 255
  end

  add_index "netbridge_configurations", ["organization_id"], name: "idx_nbg_cnf_org_id", using: :btree

  create_table "netbridge_details", force: :cascade do |t|
    t.string "client_ip", limit: 20
    t.datetime "created_at"
    t.integer "device_id", limit: 4
    t.text "eth_macs", limit: 65535
    t.string "eth_state", limit: 20
    t.string "tunnel_status", limit: 255
    t.datetime "updated_at"
    t.string "usb_hub", limit: 255
    t.string "usb_list", limit: 255
  end

  create_table "netreach_configurations", force: :cascade do |t|
    t.boolean "admin_access", default: true
    t.string "admin_password", limit: 50, default: "w1f1adm1npw"
    t.string "admin_username", limit: 50, default: "wifiadmin"
    t.boolean "analytics_mode", default: false
    t.boolean "anms_enabled", default: true
    t.integer "channel", limit: 4, default: 11
    t.integer "channel_region", limit: 4, default: 5
    t.boolean "client_monitor", default: false
    t.integer "config_int_secs", limit: 4, default: 1440
    t.string "country_code", limit: 2, default: "US"
    t.datetime "created_at"
    t.string "dns1", limit: 20
    t.string "dns2", limit: 20
    t.boolean "dst_enabled", default: true
    t.string "firmware", limit: 15
    t.boolean "log_enabled", default: true
    t.string "log_ipv4", limit: 30
    t.string "name", limit: 50
    t.integer "network_mode", limit: 4, default: 6
    t.integer "organization_id", limit: 4
    t.string "proxy_host", limit: 100
    t.string "proxy_port", limit: 10
    t.integer "remote_control", limit: 4, default: 10
    t.integer "rf_power", limit: 4, default: 100
    t.string "ssid_1", limit: 50
    t.boolean "ssid_1_advertise", default: true
    t.boolean "ssid_1_enabled", default: true
    t.string "ssid_1_ipv4", limit: 50
    t.string "ssid_1_pass", limit: 50, default: "attn3tgat3w1f2"
    t.string "ssid_1_pass_t", limit: 50, default: "C"
    t.string "ssid_1_port", limit: 10, default: "01812"
    t.integer "ssid_1_sec", limit: 4, default: 4
    t.string "ssid_1_secret", limit: 50
    t.integer "ssid_1_timeout", limit: 4, default: 3600
    t.string "ssid_1_vlan_id", limit: 50
    t.integer "ssid_1_wep_len", limit: 4, default: 2
    t.string "ssid_1_wpa_c", limit: 50, default: "A"
    t.string "ssid_2", limit: 50
    t.boolean "ssid_2_advertise", default: true
    t.boolean "ssid_2_enabled", default: true
    t.string "ssid_2_ipv4", limit: 50
    t.string "ssid_2_pass", limit: 50, default: "attn3tgat3w1f2"
    t.string "ssid_2_pass_t", limit: 50, default: "C"
    t.string "ssid_2_port", limit: 10, default: "01812"
    t.integer "ssid_2_sec", limit: 4, default: 4
    t.string "ssid_2_secret", limit: 50
    t.integer "ssid_2_timeout", limit: 4, default: 3600
    t.string "ssid_2_vlan_id", limit: 50
    t.integer "ssid_2_wep_len", limit: 4, default: 2
    t.string "ssid_2_wpa_c", limit: 50, default: "A"
    t.boolean "station_query", default: false
    t.string "tz_offset", limit: 10, default: "-300"
    t.datetime "updated_at"
    t.boolean "upnp_enabled", default: false
    t.string "wan_connection", limit: 10
    t.string "wan_gateway_ip", limit: 20
    t.string "wan_ip_address", limit: 20
    t.string "wan_netmask", limit: 20
    t.string "wds_encryption", limit: 10, default: "NONE"
    t.string "wds_mac_1", limit: 30
    t.string "wds_mac_2", limit: 30
    t.string "wds_mac_3", limit: 30
    t.integer "wds_mode", limit: 4, default: 0
    t.string "wds_pass_phrase", limit: 50
    t.string "wds_phy_mode", limit: 10, default: "HTMIX"
  end

  add_index "netreach_configurations", ["organization_id"], name: "idx_nrch_cnf_org_id", using: :btree

  create_table "notification_profiles", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "frequency", limit: 4
    t.string "name", limit: 30
    t.datetime "updated_at"
    t.integer "user_id", limit: 4
  end

  add_index "notification_profiles", ["frequency"], name: "idx_not_prof_freq", using: :btree
  add_index "notification_profiles", ["user_id"], name: "idx_not_prof_user_id", using: :btree

  create_table "notification_subscriptions", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "profile_id", limit: 4
    t.integer "publisher_id", limit: 4
    t.string "publisher_type", limit: 100
    t.datetime "updated_at"
    t.integer "user_id", limit: 4
  end

  add_index "notification_subscriptions", ["profile_id"], name: "idx_not_sub_prof_id", using: :btree
  add_index "notification_subscriptions", ["publisher_id"], name: "idx_not_sub_pub_id", using: :btree
  add_index "notification_subscriptions", ["publisher_type"], name: "idx_not_sub_pub_type", using: :btree
  add_index "notification_subscriptions", ["user_id"], name: "idx_not_sub_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.datetime "created_at"
    t.text "data", limit: 65535
    t.datetime "delivered_at"
    t.integer "device_id", limit: 4
    t.integer "event_id", limit: 4
    t.integer "event_type", limit: 4
    t.text "message", limit: 65535
    t.integer "notification_subscription_id", limit: 4
    t.integer "severity", limit: 4
    t.datetime "updated_at"
    t.integer "user_id", limit: 4
    t.integer "uuid_id", limit: 4
  end

  add_index "notifications", ["delivered_at"], name: "idx_not_del_at", using: :btree
  add_index "notifications", ["device_id"], name: "index_notifications_on_device_id", using: :btree
  add_index "notifications", ["event_id"], name: "idx_not_ev_id", using: :btree
  add_index "notifications", ["event_type"], name: "index_notifications_on_event_type", using: :btree
  add_index "notifications", ["notification_subscription_id"], name: "idx_not_sub_id", using: :btree
  add_index "notifications", ["severity"], name: "index_notifications_on_severity", using: :btree
  add_index "notifications", ["user_id"], name: "idx_not_user_id", using: :btree
  add_index "notifications", ["uuid_id"], name: "index_notifications_on_uuid_id", using: :btree

  create_table "organization_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations", limit: 4, null: false
  end

  add_index "organization_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "org_anc_desc_udx", unique: true, using: :btree
  add_index "organization_hierarchies", ["descendant_id"], name: "org_desc_idx", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "customer_service_id", limit: 4
    t.string "description", limit: 255
    t.string "footer", limit: 1023
    t.boolean "gateway_cell_modem_collection_enabled", default: false
    t.boolean "gateway_vlan_collection_enabled", default: false
    t.string "import_name", limit: 255
    t.integer "label", limit: 4
    t.string "location_desc", limit: 100
    t.string "logo", limit: 255
    t.string "name", limit: 50
    t.integer "owner_id", limit: 4
    t.integer "parent_id", limit: 4
    t.string "slug", limit: 75
    t.datetime "updated_at"
    t.string "uri_key", limit: 255
  end

  add_index "organizations", ["customer_service_id"], name: "idx_org_cust_srvc_id", using: :btree
  add_index "organizations", ["name", "owner_id"], name: "idx_org_nme_onr_id", unique: true, using: :btree
  add_index "organizations", ["slug"], name: "idx_org_slg", unique: true, using: :btree

  create_table "organizations_contacts", id: false, force: :cascade do |t|
    t.integer "contact_id", limit: 4
    t.integer "organization_id", limit: 4
  end

  add_index "organizations_contacts", ["organization_id", "contact_id"], name: "idx_org_con_org_con", unique: true, using: :btree

  create_table "organizations_locations", id: false, force: :cascade do |t|
    t.integer "location_id", limit: 4
    t.integer "organization_id", limit: 4
  end

  add_index "organizations_locations", ["organization_id", "location_id"], name: "org_lctn_org_lctn", unique: true, using: :btree

  create_table "permission_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id", limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations", limit: 4, null: false
  end

  add_index "permission_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "permission_anc_desc_udx", unique: true, using: :btree
  add_index "permission_hierarchies", ["descendant_id"], name: "permission_desc_idx", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "manageable_id", limit: 4
    t.string "manageable_type", limit: 255
    t.integer "manager_id", limit: 4
    t.string "manager_type", limit: 255
    t.integer "parent_id", limit: 4
    t.integer "role_id", limit: 4
    t.datetime "updated_at"
  end

  add_index "permissions", ["manageable_id"], name: "idx_perm_mgble_id", using: :btree
  add_index "permissions", ["manager_id", "manager_type", "manageable_id", "manageable_type"], name: "idx_perm_mgr_mgble", unique: true, using: :btree
  add_index "permissions", ["manager_id"], name: "idx_perm_mgr_id", using: :btree
  add_index "permissions", ["parent_id"], name: "idx_perm_parent_id", using: :btree
  add_index "permissions", ["role_id"], name: "idx_perm_role_id", using: :btree

  create_table "power_outlets", force: :cascade do |t|
    t.integer "device_id", limit: 4
    t.integer "port", limit: 4
    t.integer "the_state", limit: 4
  end

  add_index "power_outlets", ["device_id"], name: "idx_dev_id", using: :btree

  create_table "rake_task_migrations", force: :cascade do |t|
    t.datetime "migrated_on"
    t.integer "runtime", limit: 4
    t.string "version", limit: 255
  end

  create_table "records", force: :cascade do |t|
    t.integer "change_date", limit: 4
    t.string "content", limit: 255
    t.integer "domain_id", limit: 4
    t.string "name", limit: 255
    t.integer "prio", limit: 4
    t.string "record_type", limit: 255
    t.integer "ttl", limit: 4
    t.string "type", limit: 10
  end

  add_index "records", ["domain_id"], name: "idx_pdns_domain_id", using: :btree
  add_index "records", ["name", "type"], name: "idx_pdns_name_type", using: :btree

  create_table "regions", force: :cascade do |t|
    t.integer "country_id", limit: 4
    t.string "iso_code", limit: 255
    t.string "name", limit: 255
  end

  add_index "regions", ["country_id"], name: "index_regions_on_country_id", using: :btree

  create_table "rogue_access_points", force: :cascade do |t|
    t.integer "device_id", limit: 4
    t.string "ip", limit: 255
    t.string "mac", limit: 255
    t.string "ssid", limit: 255
  end

  add_index "rogue_access_points", ["device_id"], name: "idx_rogue_ap_dev_id", using: :btree

  create_table "role_abilities", force: :cascade do |t|
    t.integer "ability_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "role_abilities", ["ability_id"], name: "idx_rol_abi_abi_id", using: :btree
  add_index "role_abilities", ["role_id", "ability_id"], name: "idx_rol_abi_role_id_abi_id", unique: true, using: :btree
  add_index "role_abilities", ["role_id"], name: "idx_rol_abi_role_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.boolean "admin", default: false
    t.datetime "created_at"
    t.string "description", limit: 255
    t.string "name", limit: 255
    t.integer "organization_id", limit: 4
    t.datetime "updated_at"
  end

  add_index "roles", ["organization_id"], name: "idx_rol_org_id", using: :btree

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "role_id", limit: 4
    t.integer "user_id", limit: 4
  end

  add_index "roles_users", ["role_id", "user_id"], name: "idx_rls_usr_rl_usr", unique: true, using: :btree

  create_table "seed_migration_data_migrations", force: :cascade do |t|
    t.datetime "migrated_on"
    t.integer "runtime", limit: 4
    t.string "version", limit: 255
  end

  create_table "settings", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "thing_id", limit: 4
    t.string "thing_type", limit: 30
    t.datetime "updated_at"
    t.text "value", limit: 65535
    t.string "var", limit: 255, null: false
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "idx_stngs_typ_id_var", unique: true, using: :btree

  create_table "signal_strengths", force: :cascade do |t|
    t.integer "threshold_percentage", limit: 4
  end

  create_table "sites", force: :cascade do |t|
    t.datetime "created_at"
    t.string "name", limit: 80
    t.integer "organization_id", limit: 4
    t.datetime "updated_at"
  end

  add_index "sites", ["name", "organization_id"], name: "idx_sts_nm_org_id", unique: true, using: :btree
  add_index "sites", ["organization_id"], name: "idx_sts_org_id", using: :btree

  create_table "snmp_profiles", force: :cascade do |t|
    t.string "auth_password", limit: 50
    t.string "auth_protocol", limit: 3
    t.string "community", limit: 50
    t.datetime "created_at"
    t.string "priv_password", limit: 50
    t.string "priv_protocol", limit: 3
    t.integer "security_level", limit: 4, default: 3
    t.datetime "updated_at"
    t.string "username", limit: 50
    t.string "version", limit: 2
  end

  create_table "speed_tests", force: :cascade do |t|
    t.datetime "created_at"
    t.integer "device_modem_id", limit: 4
    t.string "latency_units", limit: 255
    t.float "rx_avg", limit: 24
    t.float "rx_latency", limit: 24
    t.string "throughput_units", limit: 255
    t.float "tx_avg", limit: 24
    t.float "tx_latency", limit: 24
    t.datetime "updated_at"
  end

  add_index "speed_tests", ["device_modem_id"], name: "index_speed_tests_on_device_modem_id", using: :btree

  create_table "subscribed_events", force: :cascade do |t|
    t.integer "frequency", limit: 4
    t.integer "profile_id", limit: 4
    t.integer "uuid_id", limit: 4
  end

  add_index "subscribed_events", ["frequency"], name: "idx_sub_ev_freq", using: :btree
  add_index "subscribed_events", ["profile_id"], name: "idx_sub_ev_prof_id", using: :btree
  add_index "subscribed_events", ["uuid_id"], name: "idx_sub_ev_uuid_id", using: :btree

  create_table "supermasters", id: false, force: :cascade do |t|
    t.string "account", limit: 40
    t.string "ip", limit: 64, null: false
    t.string "nameserver", limit: 255, null: false
  end

  create_table "syslog_servers", force: :cascade do |t|
    t.string "name", limit: 255
    t.string "server_state", limit: 255
    t.string "uri", limit: 255
  end

  create_table "timezones", force: :cascade do |t|
    t.string "name", limit: 50
    t.string "off", limit: 6
    t.string "official_name", limit: 100
  end

  create_table "tunnel_server_ip_addresses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "ip_address", limit: 255, null: false
    t.integer "tunnel_server_id", limit: 4
    t.datetime "updated_at", null: false
  end

  add_index "tunnel_server_ip_addresses", ["tunnel_server_id"], name: "index_tunnel_server_ip_addresses_on_tunnel_server_id", using: :btree

  create_table "tunnel_servers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.string "location", limit: 255
    t.string "name", limit: 255
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.boolean "can_admin_configurations", default: false
    t.boolean "can_admin_current_organization", default: false
    t.boolean "can_admin_devices", default: false
    t.boolean "can_admin_organizations", default: false
    t.boolean "can_export_device_reports", default: false
    t.boolean "can_manage_configurations", default: false
    t.boolean "can_manage_devices", default: false
    t.boolean "can_manage_organizations", default: false
    t.boolean "can_view_devices", default: false
    t.boolean "can_view_organizations", default: false
    t.datetime "created_at"
    t.boolean "is_admin", default: false
    t.string "name", limit: 50, default: ""
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "account_name", limit: 50
    t.string "account_token", limit: 32
    t.string "authentication_token", limit: 255
    t.boolean "cellular_pins_on_map", default: true
    t.boolean "clock_enabled", default: false
    t.datetime "created_at"
    t.datetime "current_sign_in_at"
    t.string "current_sign_in_ip", limit: 20
    t.string "email", limit: 100, default: ""
    t.string "encrypted_password", limit: 150, default: ""
    t.datetime "expired_at"
    t.string "filter", limit: 20
    t.integer "filter_organization_id", limit: 4
    t.string "first_name", limit: 50
    t.integer "group_id", limit: 4
    t.datetime "last_activity_at"
    t.string "last_name", limit: 50
    t.datetime "last_sign_in_at"
    t.string "last_sign_in_ip", limit: 20
    t.boolean "maps_enabled", default: true
    t.string "name", limit: 255
    t.string "netbridge_table_preference", limit: 255
    t.string "netgate_table_preference", limit: 255
    t.string "netreach_table_preference", limit: 255
    t.string "new_account_token", limit: 16
    t.boolean "notify_enabled", default: false
    t.integer "organization_id", limit: 4
    t.text "pagination_limit", limit: 65535
    t.text "pagination_limits", limit: 65535
    t.datetime "password_changed_at"
    t.datetime "remember_created_at"
    t.datetime "reset_password_sent_at"
    t.string "reset_password_token", limit: 150
    t.integer "restricted_by", limit: 4, default: 1
    t.integer "role_id", limit: 4
    t.integer "sign_in_count", limit: 4, default: 0
    t.integer "timezone_id", limit: 4
    t.string "unique_session_id", limit: 20
    t.datetime "updated_at"
    t.string "username", limit: 50
  end

  add_index "users", ["account_token"], name: "idx_usr_acnt_tkn", unique: true, using: :btree
  add_index "users", ["authentication_token"], name: "idx_users_auth_tkn", using: :btree
  add_index "users", ["expired_at"], name: "idx_usr_expd_at", using: :btree
  add_index "users", ["last_activity_at"], name: "idx_usr_lst_acty_at", using: :btree
  add_index "users", ["organization_id"], name: "idx_usr_org_id", using: :btree
  add_index "users", ["password_changed_at"], name: "idx_usr_pwd_chg_at", using: :btree
  add_index "users", ["reset_password_token"], name: "idx_usr_rst_pwd_tkn", unique: true, using: :btree
  add_index "users", ["role_id"], name: "idx_usr_rl_id", using: :btree
  add_index "users", ["timezone_id"], name: "idx_usr_tmzn_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.datetime "created_at"
    t.string "event", limit: 255, null: false
    t.integer "item_id", limit: 4, null: false
    t.string "item_type", limit: 255, null: false
    t.text "object", limit: 4294967295
    t.string "whodunnit", limit: 255
  end

  add_index "versions", ["item_type", "item_id"], name: "idx_vers_type_id", using: :btree

  add_foreign_key "data_plans", "organizations"
  add_foreign_key "device_modems", "cellular_locations"
  add_foreign_key "devices", "data_plans"
end
