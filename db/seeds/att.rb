module Seed

  def self.root_organization
    {
      :name => "AT&T",
      :label => 0
    }
  end

  def self.categories_and_models
    {
      DeviceCategory::GATEWAY_CATEGORY => {
        :description => "VPN Gateway device category",
        :class_name => "Gateway",
        :models => [
          { :name => "Default", :class_name => "Gateway", :model_version => "Default" },
          { :name => "7100", :class_name => "Gateway", :model_version => "7100" },
          { :name => "8100", :class_name => "Gateway", :model_version => "8100" },
          { :name => "8200", :class_name => "Gateway", :model_version => "8200" }
        ],
        :events => [
          { :code => EventUUIDS::GATEWAY_UP,          :name => "Device up" },
          { :code => EventUUIDS::GATEWAY_DOWN,        :name => "Device down" },
          { :code => EventUUIDS::GATEWAY_FW_CHANGED,  :name => "Device firmware changed" },
          { :code => EventUUIDS::GATEWAY_REBOOTED,    :name => "Device rebooted" },
          { :code => EventUUIDS::GATEWAY_IP_CHANGED,  :name => "Device primary ip changed" },
          { :code => EventUUIDS::GATEWAY_UNREACHABLE, :name => "Device unreachable" },
          { :code => EventUUIDS::GATEWAY_DBU_FAILED,  :name => "Backup WAN autotest failed" },
          { :code => EventUUIDS::GATEWAY_TUN_DOWN,    :name => "Device tunnel down" },
          { :code => EventUUIDS::GATEWAY_TUN_UP,      :name => "Device tunnel up" },
          { :code => EventUUIDS::GATEWAY_WAN_CHANGE,  :name => "Device WAN interface changed" }
        ]
      }
    }
  end

  def self.abilities
    {
      # Default Abilities
      "Modify Sites"                    => { :description => "create, delete, modify details of sites", :group_id => AbilityGroups::DEFAULT },
      "Modify Devices"                  => { :description => "create, delete, modify details of, change parent organization of, remote-control, (un)deploy, replace devices", :group_id => AbilityGroups::DEFAULT },
      "Modify Configurations"           => { :description => "create, delete, modify details of, device configurations", :group_id => AbilityGroups::DEFAULT },
      "Modify Organizations"            => { :description => "create, delete, modify details of organizations", :group_id => AbilityGroups::DEFAULT },

      # Admin Abilities
      "Replace Devices"                 => { :description => "replace device with another device manually", :group_id => AbilityGroups::ADMIN }
    }
  end

  def self.roles
    {
      "Admin" => {
        :description => "can do everything with in their scope",
        :admin => true
      },
      "Viewer" => {
        :description => "can view everything with in their scope",
        :admin => false
      }
    }
  end
end
