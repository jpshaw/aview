module Seed

  def self.root_organization
    {
      :name => "Accelerated",
      :import_name => "Accelerated",
      :label => 0
    }
  end

  def self.categories_and_models
    {
      DeviceCategory::GATEWAY_CATEGORY => {
        :description => "VPN Gateway device category",
        :class_name => "Gateway",
        :models => [
          { :name => "Default", :class_name => "Gateway", :model_version => "Default" },
          { :name => "7100", :class_name => "Gateway", :model_version => "7100" },
          { :name => "8100", :class_name => "Gateway", :model_version => "8100" },
          { :name => "8200", :class_name => "Gateway", :model_version => "8200" }
        ],
        :events => [
          { :code => EventUUIDS::GATEWAY_UP,          :name => "Device up" },
          { :code => EventUUIDS::GATEWAY_DOWN,        :name => "Device down" },
          { :code => EventUUIDS::GATEWAY_FW_CHANGED,  :name => "Device firmware changed" },
          { :code => EventUUIDS::GATEWAY_REBOOTED,    :name => "Device rebooted" },
          { :code => EventUUIDS::GATEWAY_IP_CHANGED,  :name => "Device primary ip changed" },
          { :code => EventUUIDS::GATEWAY_UNREACHABLE, :name => "Device unreachable" },
          { :code => EventUUIDS::GATEWAY_DBU_FAILED,  :name => "Backup WAN autotest failed" },
          { :code => EventUUIDS::GATEWAY_TUN_DOWN,    :name => "Device tunnel down" },
          { :code => EventUUIDS::GATEWAY_TUN_UP,      :name => "Device tunnel up" },
          { :code => EventUUIDS::GATEWAY_WAN_CHANGE,  :name => "Device WAN interface changed" }
        ]
      },
      DeviceCategory::WIFI_CATEGORY => {
        :description => "NetReach device category",
        :class_name => "Netreach",
        :models => [
          { :name => "Default", :class_name => "Netreach", :model_version => "Default" },
          { :name => "1000", :class_name => "Netreach", :model_version => "1000" }
        ],
        :events => [
          { :code => EventUUIDS::NETREACH_UP,           :name => "Device up" },
          { :code => EventUUIDS::NETREACH_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::NETREACH_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::NETREACH_REBOOTED,     :name => "Device rebooted" },
          { :code => EventUUIDS::NETREACH_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::NETREACH_IP_CHANGED,   :name => "Device ip changed" }
        ]
      },
      DeviceCategory::USB_CELLULAR_CATEGORY => {
        :description => "NetBridge device category",
        :class_name => "Netbridge",
        :models => [
          { :name => "Default", :class_name => "Netbridge", :model_version => "Default" },
          { :name => "SX", :class_name => "Netbridge", :model_version => "SX" },
          { :name => "6200-FX", :class_name => "Netbridge", :model_version => "6200-FX" }
        ],
        :events => [
          { :code => EventUUIDS::NETBRIDGE_UP,           :name => "Device up" },
          { :code => EventUUIDS::NETBRIDGE_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::NETBRIDGE_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::NETBRIDGE_REBOOTED,     :name => "Device rebooted" },
          { :code => EventUUIDS::NETBRIDGE_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::NETBRIDGE_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::NETBRIDGE_CNTI_CHANGED, :name => "Device network type changed" }
        ]
      },
      DeviceCategory::DIAL_TO_IP_CATEGORY => {
        :description => "Dial-to-IP device category",
        :class_name => "DialToIp",
        :models => [
          { :name => "Default", :class_name => "DialToIp", :model_version => "Default" },
          { :name => "5300-DC", :class_name => "DialToIp", :model_version => "5300-DC" }
        ],
        :events => [
          { :code => EventUUIDS::DIALTOIP_UP,           :name => "Device up" },
          { :code => EventUUIDS::DIALTOIP_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::DIALTOIP_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::DIALTOIP_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::DIALTOIP_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::DIALTOIP_CNTI_CHANGED, :name => "Device cellular network type changed" },
          { :code => EventUUIDS::DIALTOIP_WAN_CHANGED,  :name => "Device WAN interface changed" }
        ]
      },
      DeviceCategory::REMOTE_MANAGER_CATEGORY => {
        description: "Remote Manager device category",
        class_name: "RemoteManager",
        models: [
          { name: "Default", class_name: "RemoteManager", model_version: "Default" },
          { name: "5300-RM", class_name: "RemoteManager", model_version: "5300-RM" }
        ],
        :events => [
          { :code => EventUUIDS::REMOTEMANAGER_UP,           :name => "Device up" },
          { :code => EventUUIDS::REMOTEMANAGER_DOWN,         :name => "Device down" },
          { :code => EventUUIDS::REMOTEMANAGER_FW_CHANGED,   :name => "Device firmware changed" },
          { :code => EventUUIDS::REMOTEMANAGER_UNREACHABLE,  :name => "Device unreachable" },
          { :code => EventUUIDS::REMOTEMANAGER_IP_CHANGED,   :name => "Device primary ip changed" },
          { :code => EventUUIDS::REMOTEMANAGER_CNTI_CHANGED, :name => "Device cellular network type changed" },
          { :code => EventUUIDS::REMOTEMANAGER_WAN_CHANGED,  :name => "Device WAN interface changed" }
        ]
      }
    }
  end

  def self.abilities
    {
      # Default Abilities
      "Modify Sites"                    => { :description => "create, delete, modify details of sites", :group_id => AbilityGroups::DEFAULT },
      "Modify Devices"                  => { :description => "create, delete, modify details of, change parent organization of, remote-control, (un)deploy, replace devices", :group_id => AbilityGroups::DEFAULT },
      "Modify Configurations"           => { :description => "create, delete, modify details of, device configurations", :group_id => AbilityGroups::DEFAULT },
      "Modify Device Configurations"    => { :description => "create, delete, modify details of, device configurations", :group_id => AbilityGroups::DEFAULT },
      "Modify Organizations"            => { :description => "create, delete, modify details of organizations", :group_id => AbilityGroups::DEFAULT },
      "Modify Users"                    => { :description => "create, delete, modify details of users", :group_id => AbilityGroups::DEFAULT },

      # Admin Abilities
      "Replace Devices"                 => { :description => "replace device with another device manually", :group_id => AbilityGroups::ADMIN },
      "Import Devices"                  => { :description => "import devices manually", :group_id => AbilityGroups::ADMIN },

      "Manage Accounts"                 => { description: "can manage accounts", group_id: AbilityGroups::DEVICE },

      # Feature Testing
      "View Configuration History"                 => { description: "can view configuration history", group_id: AbilityGroups::FEATURE },
    }
  end

  def self.roles
    {
      "Admin" => {
        :description => "can do everything with in their scope",
        :admin => true
      },
      "Viewer" => {
        :description => "can view everything with in their scope",
        :admin => false
      }
    }
  end
end
