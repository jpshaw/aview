class Add5301ModelToDialtoipDevice < SeedMigration::Migration
    def up
      device_category = DeviceCategory.where(class_name: "DialToIp").first
      DeviceModel.create(category_id: device_category.id, class_name: device_category.class_name, model_version: "5301-DC", name: "5301-DC")
    end

    def down
      DeviceModel.where(name: "5301-DC").destroy_all
    end
end
