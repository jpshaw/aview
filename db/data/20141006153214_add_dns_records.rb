class AddDnsRecords < SeedMigration::Migration
  def up
    domain = PowerDnsRecordManager::Domain.first

    soa_record = {
      domain_id:   domain.id,
      name:        Settings.powerdns.domain.name,
      ttl:         86400,
      prio:        nil,
      change_date: nil,
      record_type: 'SOA',
      content:     '127.0.0.1'
    }

    ns_record = {
      domain_id:   domain.id,
      name:        Settings.powerdns.domain.name,
      type:        'NS',
      ttl:         86400,
      prio:        nil,
      change_date: nil,
      record_type: 'NS'
    }

    # Ensure we have an SOA record
    PowerDnsRecordManager::Record.where(type: 'SOA').first_or_create!(soa_record)

    # Ensure we have NS records
    ns_record_count = PowerDnsRecordManager::Record.where(type: 'NS').count

    if ns_record_count.zero?
      PowerDnsRecordManager::Record::NS_RECORD_COUNT.times do
        PowerDnsRecordManager::Record.create!(ns_record)
      end
    end
  end

  def down
    PowerDnsRecordManager::Record.where(type: 'SOA').destroy_all
    PowerDnsRecordManager::Record.where(type: 'NS').destroy_all
  end
end
