class Add8300GatewayModel < SeedMigration::Migration
  def up
    device_category = DeviceCategory.gateway.first
    if device_category.present?
      device_category.models
                     .where(model_version: '8300',
                                  name: '8300',
                            class_name: device_category.class_name)
                     .first_or_create!
    end
  end

  def down

  end
end
