class AddSupportContactListToAdminSettings < SeedMigration::Migration
  def up
    AdminSettings.support_contact_list = ""
  end

  def down
    AdminSettings.destroy :support_contact_list
  end
end
