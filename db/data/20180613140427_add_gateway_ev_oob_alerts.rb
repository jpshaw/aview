class AddGatewayEvOobAlerts < SeedMigration::Migration
  NOTIFICATIONS = {
    EventUUIDS::GATEWAY_OOB_CABLES_MISSING => 'OOB Cable(s) Missing',
    EventUUIDS::GATEWAY_OOB_CABLES_PRESENT => 'OOB Cable(s) All Present'
  }

  def up
    NOTIFICATIONS.each do |code, name|
      category.uuids.where(code: code, name: name).first_or_create
    end
  end

  def down
    category.uuids.where(code: NOTIFICATIONS.keys).destroy_all
  end

  def category
    DeviceCategory.gateway.first
  end
end
