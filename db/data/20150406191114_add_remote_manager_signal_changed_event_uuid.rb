class AddRemoteManagerSignalChangedEventUuid < SeedMigration::Migration

  def up
    if category = DeviceCategory.remote_manager.first
      DeviceEventUUID.create! do |event|
        event.category = category
        event.code     = EventUUIDS::REMOTEMANAGER_SIGNAL_CHANGED
        event.name     = 'Device signal strength changed'
      end
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::REMOTEMANAGER_SIGNAL_CHANGED).destroy_all
  end

end
