class Add6355SrModel < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first

    category.models.create!(
      name: Cellular::SR_6355_MODEL,
      model_version: Cellular::SR_6355_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: Cellular::SR_6355_MODEL).destroy_all
  end
end
