class UpdateSignalNotificationDescription < SeedMigration::Migration

  OLD_NAME = 'Device signal strength changed'
  NEW_NAME = 'Device signal strength fell below a nominal threshold'

  def up
    DeviceEventUUID.where(name: OLD_NAME).update_all(name: NEW_NAME)
  end

  def down
    DeviceEventUUID.where(name: NEW_NAME).update_all(name: OLD_NAME)
  end
end
