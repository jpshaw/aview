class AddAbilityToViewAdminPanel < SeedMigration::Migration
    def up
      Ability.create(name: 'View Admin Panel', description: 'view the admin panel')
    end

    def down
      Ability.destroy_all(name: 'View Admin Panel')
    end
end
