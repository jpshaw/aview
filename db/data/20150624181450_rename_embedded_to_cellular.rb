class RenameEmbeddedToCellular < SeedMigration::Migration
  def up
    category = DeviceCategory.where(name: 'Embedded Cellular').first

    if category.present?
      # Rename category
      category.update_attributes(
        name:       'Cellular',
        description: 'Cellular device category',
        class_name:  Cellular.to_s
      )

      # Change CX model
      category.models.where(class_name: 'EmbeddedCellular').update_all(class_name: 'Cellular')

      # Add LX model
      category.models.where(name: Cellular::LX_6300_MODEL).first_or_create!(
        model_version: Cellular::LX_6300_MODEL,
        class_name:    Cellular.to_s
      )

      # Update device models
      Device.where(object_type: 'EmbeddedCellular').update_all(object_type: 'Cellular')
    end
  end

  def down
    category = DeviceCategory.where(name: 'Cellular').first

    if category.present?
      category.update_attributes(
        name:       'Embedded Cellular',
        desciption: 'Embedded Cellular device category',
        class_name:  EmbeddedCellular.to_s
      )

      category.models.where(class_name: 'Cellular').update_all(class_name: 'EmbeddedCellular')
      category.models.where(name: Cellular::LX_6300_MODEL).destroy_all
      Device.where(object_type: 'Cellular').update_all(object_type: 'EmbeddedCellular')
    end
  end
end
