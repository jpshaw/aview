class AddNewDeviceAbilities < SeedMigration::Migration
  def up
    Ability.create(name: 'Deploy Devices', description: 'deploy devices')
    Ability.create(name: 'Send Device Commands', description: 'send commands to devices')
  end

  def down
    Ability.destroy_all(name: ['Deploy Devices', 'Send Device Commands'])
  end
end
