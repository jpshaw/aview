#!/bin/env ruby
# encoding: utf-8

require 'timezone'
require 'country'
require 'organization'
require 'organization_creator'
require 'device'
require 'device_event_uuid'
require 'device_category'
require 'device_model'
require 'notification_profile'
require 'ability'
require 'role'
require 'power_dns_record_manager'

class Initialize < SeedMigration::Migration
  def up
    # Set path to seeds dir
    seeds_dir = Rails.root.join('db', 'seeds')

    # Syncs the Country List
    puts "Syncing countries to database"
    open("#{seeds_dir}/countries.txt") do |countries|
      countries.read.each_line do |country|
        code, name = country.chomp.split("|")
        Country.where(:name => name, :code => code).first_or_create
      end
    end

    # Syncs the timezone list
    puts "Syncing timezones to database"
    open("#{seeds_dir}/timezones.txt") do |timezones|
      timezones.read.each_line do |timezone|
        name, official_name, offset = timezone.chomp.split("|")
        Timezone.where(:name => name, :official_name => official_name, :off => offset).first_or_create
      end
    end

    # Load seed files
    if Settings.seeds
      Settings.seeds.each do |seed_name|
        seed_file = File.join(seeds_dir, seed_name)
        require seed_file
      end
    end

    # Organization
    puts "Syncing Root Organization to DB"
    if @organization =  Organization.where(
                          :name   => Seed.root_organization[:name],
                          :label  => Seed.root_organization[:label]
                        ).first
      # If the organization exists make sure it has an admin role and a self managing permission.
      CreateOrganizationAdminRole.call(organization: @organization) unless @organization.admin_role.present?
      CreateOrganizationSelfManagingPermission.call(organization: @organization) unless @organization.self_managing_permission.present?
    else
      # If the organization does not exist create it with the OrganizationCreator interactor.
      # The interactor will create all support objects the organization needs.
      result = OrganizationCreator.call(params: Seed.root_organization)

      if result.success?
        @organization = result.organization
      else
        puts "Error creating the root organization. Errors: #{result.organization.errors.full_messages}"
      end
    end

    # Device Categories, Models, and Events
    puts "Syncing Categories and Models to DB"
    Seed.categories_and_models.each do |category_name, category_data|
      puts "Syncing category: #{category_name}"
      category = DeviceCategory.where(:name => category_name)
        .first_or_create(:description => category_data[:description], :class_name => category_data[:class_name])

      # Models
      category_data[:models].each do |model_data|
        puts "Syncing model: #{model_data[:name]}"
        category.models.where(:name => model_data[:name], :class_name => model_data[:class_name])
          .first_or_create(:model_version => model_data[:model_version])
      end

      # Event uuids - ensure the category id is set correctly for these.
      unless category_data[:events].blank?
        category_data[:events].each do |event|
          puts "Syncing event: #{event[:name]}"
          uuid = DeviceEventUUID.where(:code => event[:code]).first_or_initialize(:name => event[:name])
          uuid.name = event[:name]
          uuid.category = category
          uuid.save!
        end
      end
    end

    # Delete Unneeded Events
    puts "Checking to make sure there are no extra unneeded event types..."
    @database_events = DeviceEventUUID.all
    @events = []
    Seed.categories_and_models.each_value { |h| @events << h[:events] }
    @events.flatten!
    @events.delete_if {|event| event == nil}
    unless @database_events.count == @events.count
      event_codes = @events.map { |event| event[:code] }
      @database_events.each do |event|
        unless event_codes.include? event.code
          puts "Event '#{event.name}' Should Not Be In DB; Deleting..."
          event.destroy
        end
      end
    end

    # User Abilities
    puts "Syncing Abilities to DB"
    Seed.abilities.each do |ability_name, ability_data|
      puts "Syncing ability: #{ability_name}"
      ability = Ability.where(:name => ability_name).first_or_create(:description => ability_data[:description], :group_id => ability_data[:group_id])
      ability.update_attributes(:description => ability_data[:description], :group_id => ability_data[:group_id])
    end

    # Removed Unneeded Abilities
    puts "Checking to make sure there are no extra unneeded abilities..."
    @database_abilities = Ability.all
    unless @database_abilities.count == Seed.abilities.count
      @database_abilities.each do |ability|
        unless Seed.abilities.keys.include? ability.name
          puts "Ability '#{ability.name}' Should Not Be In DB; Deleting..."
          ability.destroy
        end
      end
    end

    # Roles
    puts "Syncing roles ..."
    Seed.roles.each do |role_name, role_data|
      puts "Syncing role: #{role_name}"
      role = Role.where(:name => role_name, :admin => role_data[:admin]).first_or_create(:description => role_data[:description])
      role.update_attributes(:description => role_data[:description])

      # If the role is the admin role, it will add all the abilities to the role
      if role_data[:admin]
        Ability.all.each do |ability|
          next if role.abilities.include? ability
          role.abilities << ability
        end
      else
        # If it isn't an admin role, it must be a viewer and it will remove any abilities it may have since it isn't supposed to have any
        role.abilities.clear
      end
    end


    # Sync Power DNS
    puts "Syncing NATIVE Power DNS"
    domain = PowerDnsRecordManager::Domain.where(:name => Settings.powerdns.domain.name,
                                                :type => "NATIVE")
                                          .first_or_create

    # Localhost
    puts "Syncing A record for localhost Power DNS"
    local = PowerDnsRecordManager::Record.where(:name => "localhost.#{Settings.powerdns.domain.name}",
                                                :type => "A")
                                        .first_or_create

    local.update_attributes(:content => "127.0.0.1",
                            :domain_id => domain.id,
                            :ttl => 120)

    # SOA
    puts "Syncing SOA Power DNS"
    soa = PowerDnsRecordManager::Record.where(:name => "#{Settings.powerdns.domain.name}",
                                              :type => "SOA")
                                      .first_or_create

    soa.update_attributes(:content => "#{Settings.powerdns.domain.hostmaster}",
                          :domain_id => domain.id,
                          :ttl => Settings.powerdns.domain.ttl)

    # Nameservers
    puts "Syncing NS Power DNS"
    Settings.powerdns.domain.nameservers.each do |nameserver|
      ns = PowerDnsRecordManager::Record.where(:name => nameserver.name,
                                              :type => "NS")
                                        .first_or_create

      ns.update_attributes(:domain_id => domain.id,
                          :ttl => nameserver.ttl || 86400)
    end
  end

  def down
    Country.destroy_all
    Timezone.destroy_all
    Organization.destroy_all
    DeviceCategory.destroy_all
    DeviceEventUUID.destroy_all
    Ability.destroy_all
    Role.destroy_all
    Permission.destroy_all
    DeviceModel.destroy_all
    NotificationProfile.destroy_all
    RoleAbility.destroy_all
    PowerDnsRecordManager::Domain.destroy_all
    PowerDnsRecordManager::Record.destroy_all
  end
end
