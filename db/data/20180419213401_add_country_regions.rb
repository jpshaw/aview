require 'region'

class AddCountryRegions < SeedMigration::Migration
  def up
    seeds_dir = Rails.root.join('db', 'seeds')
    country = Country.find_by_code('US')

    puts "Syncing US regions to database"
    open("#{seeds_dir}/us-regions.txt") do |regions|
      regions.read.each_line do |region|
        name, code = region.chomp.split("|")
        Region.where(:country_id => country, :iso_code => code, :name => name).first_or_create
      end
    end
  end

  def down
    Region.destroy_all
  end

end
