class AddReplaceWr14WithIx14Model < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first
    wr14_model = category.models.where(name: Cellular::WR14_MODEL).first

    ix14_model = category.models.create!(
      name: Cellular::IX14_MODEL,
      model_version: Cellular::IX14_MODEL,
      class_name: category.class_name
    )

    wr14_model.devices.find_each do |device|
      device.hw_version = Cellular::WR14_MODEL
      device.category_id = category.id
      device.model_id = ix14_model.id
      device.save!
    end

    wr14_model.destroy
  end

  def down
    category = DeviceCategory.cellular.first
    ix14_model = category.models.where(name: Cellular::IX14_MODEL).first

    wr14_model = category.models.create!(
      name: Cellular::WR14_MODEL,
      model_version: Cellular::WR14_MODEL,
      class_name: category.class_name
    )

    ix14_model.devices.find_each do |device|
      device.hw_version = Cellular::WR14_MODEL
      device.category_id = category.id
      device.model_id = wr14_model.id
      device.save!
    end

    ix14_model.destroy
  end
end
