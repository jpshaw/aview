class ReplaceRecoveredWithUpInDeviceEventUuidName < SeedMigration::Migration

  def up
    DeviceEventUUID.where(name: 'Device recovered').update_all(name: 'Device up')
  end

  def down
    DeviceEventUUID.where(name: 'Device up').update_all(name: 'Device recovered')
  end

end
