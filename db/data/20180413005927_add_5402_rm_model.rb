class Add5402RmModel < SeedMigration::Migration
  def up
    category = DeviceCategory.remote_manager.first

    category.models.create!(
      name: RemoteManager::RM_5402_MODEL,
      model_version: RemoteManager::RM_5402_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: RemoteManager::RM_5402_MODEL).destroy_all
  end
end
