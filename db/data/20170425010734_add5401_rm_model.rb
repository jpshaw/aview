class Add5401RmModel < SeedMigration::Migration
  def up
    category = DeviceCategory.remote_manager.first

    category.models.create!(
      name: RemoteManager::RM_5401_MODEL,
      model_version: RemoteManager::RM_5401_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: RemoteManager::RM_5401_MODEL).destroy_all
  end
end
