class AddNetbridgeSignalChangedEventUuid < SeedMigration::Migration
  def up
    if category = DeviceCategory.where(name: 'USB Cellular').first
      DeviceEventUUID.create! do |event|
        event.category = category
        event.code     = EventUUIDS::NETBRIDGE_SIGNAL_CHANGED
        event.name     = 'Device signal strength changed'
      end
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::NETBRIDGE_SIGNAL_CHANGED).destroy_all
  end
end
