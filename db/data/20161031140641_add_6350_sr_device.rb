class Add6350SrDevice < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first

    category.models.create!(
      name: Cellular::SR_6350_MODEL,
      model_version: Cellular::SR_6350_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: Cellular::SR_6350_MODEL).destroy_all
  end
end
