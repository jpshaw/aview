class RenameUcpeSdModelToUa < SeedMigration::Migration
  def up
    model = category.models.first_or_create!(
      name:          Ucpe::SD_9400_MODEL,
      model_version: Ucpe::SD_9400_MODEL,
      class_name:    Ucpe.to_s
    )
    model.update_attributes(name: Ucpe::UA_9400_MODEL, model_version: Ucpe::UA_9400_MODEL)
  end

  def down
    model = category.models.first_or_create!(
      name:          Ucpe::UA_9400_MODEL,
      model_version: Ucpe::UA_9400_MODEL,
      class_name:    Ucpe.to_s
    )
    model.update_attributes(name: Ucpe::SD_9400_MODEL, model_version: Ucpe::SD_9400_MODEL)
  end

  def category
    @category ||= DeviceCategory.find_by(class_name: Ucpe.to_s)
  end
end
