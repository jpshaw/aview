class RemoveTunnelUpAndTunnelDownNotifications < SeedMigration::Migration
  def up
    DeviceEventUUID.where(code: EventUUIDS::GATEWAY_TUN_DOWN).destroy_all
    DeviceEventUUID.where(code: EventUUIDS::GATEWAY_TUN_UP).destroy_all
  end

  def down
    gateway_category = DeviceCategory.gateway.first
    return unless gateway_category.present?

    DeviceEventUUID.create! do |event|
      event.category = gateway_category
      event.code     = EventUUIDS::GATEWAY_TUN_DOWN
      event.name     = 'Device tunnel down'
    end

    DeviceEventUUID.create! do |event|
      event.category = gateway_category
      event.code     = EventUUIDS::GATEWAY_TUN_UP
      event.name     = 'Device tunnel up'
    end
  end
end
