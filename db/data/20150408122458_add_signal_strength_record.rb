class AddSignalStrengthRecord < SeedMigration::Migration

  def up
    SignalStrength.create! threshold_percentage: 0
  end

  def down
    SignalStrength.destroy_all
  end

end
