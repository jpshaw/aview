class AddDeviceCommandAbilityToRoles < SeedMigration::Migration
  def up
    ability = Ability.find_by_name('Send Device Commands')

    # Find roles with the ability to `Modify Devices` and add the new ability
    Role.joins(:abilities).where(abilities: { name: 'Modify Devices' }).each do |role|
      role.abilities << ability unless role.abilities.include?(ability)
    end
  end

  def down
    RoleAbility.joins(:ability)
               .where(abilities: { name: 'Send Device Commands' })
               .destroy_all
  end
end
