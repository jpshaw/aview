class AddDbuThresholdNotificationToGatewayDevice < SeedMigration::Migration
    def up
      if category = DeviceCategory.gateway.first
        DeviceEventUUID.create! do |event|
          event.category = category
          event.code     = EventUUIDS::GATEWAY_DBU_THRESHOLD
          event.name     = 'Backup WAN Threshold reached'
        end
      end
    end

    def down
      DeviceEventUUID.where(code: EventUUIDS::GATEWAY_DBU_THRESHOLD).destroy_all
    end
end
