class ChangeCellularToLegacyCellular < SeedMigration::Migration
  def up
    DeviceCategory.where(name: 'Cellular').update_all(name: 'Legacy Cellular')
  end

  def down
    DeviceCategory.where(name: 'Legacy Cellular').update_all(name: 'Cellular')
  end
end
