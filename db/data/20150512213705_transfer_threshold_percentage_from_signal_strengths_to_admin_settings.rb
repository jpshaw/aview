class TransferThresholdPercentageFromSignalStrengthsToAdminSettings < SeedMigration::Migration

  def up
    signal_strength = SignalStrength.instance

    if signal_strength
      AdminSettings.threshold_percentage = signal_strength.threshold_percentage
    end
  end

end
