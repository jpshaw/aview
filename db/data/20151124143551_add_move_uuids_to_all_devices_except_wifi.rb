class AddMoveUuidsToAllDevicesExceptWifi < SeedMigration::Migration
  def up
    [:gateway, :legacy_cellular, :cellular, :ucpe, :remote_manager, :dial_to_ip].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_name = "#{category.class_name}_CELL_LOCATION_CHANGED".upcase
      event_code = "EventUUIDS::#{uuid_name}".constantize

      event = {
          code: event_code,
          name: 'Device cellular location changed'
      }

      category.uuids.where(event).first_or_create!
    end
  end

  def down
    [:gateway, :legacy_cellular, :cellular, :ucpe, :remote_manager, :dial_to_ip].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_name = "#{category.class_name}_CELL_LOCATION_CHANGED".upcase
      event_code = "EventUUIDS::#{uuid_name}".constantize

      category.uuids.where(code: event_code).destroy_all
    end
  end
end
