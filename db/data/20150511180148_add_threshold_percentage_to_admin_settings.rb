class AddThresholdPercentageToAdminSettings < SeedMigration::Migration

  def up
    AdminSettings.threshold_percentage = 0
  end

  def down
    AdminSettings.destroy :threshold_percentage
  end

end
