class RenameGatewayCategory < SeedMigration::Migration

  OLD_GATEWAY_NAME = 'Gateway'
  NEW_GATEWAY_NAME = 'VPN Gateway'

  def up
    DeviceCategory.where(name: OLD_GATEWAY_NAME).update_all(name: NEW_GATEWAY_NAME)
  end

  def down
    DeviceCategory.where(name: NEW_GATEWAY_NAME).update_all(name: OLD_GATEWAY_NAME)
  end
end
