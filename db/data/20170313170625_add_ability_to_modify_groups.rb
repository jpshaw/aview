class AddAbilityToModifyGroups < SeedMigration::Migration
  def up
    Ability.create(name: Ability::MODIFY_GROUPS)
  end

  def down
    Ability.destroy_all(name: Ability::MODIFY_GROUPS)
  end
end
