class AddEmbeddedCellularSignalChangedEventUuid < SeedMigration::Migration

  def up
    if category = DeviceCategory.embedded_cellular.first
      DeviceEventUUID.create! do |event|
        event.category = category
        event.code     = EventUUIDS::EMBEDDEDCELLULAR_SIGNAL_CHANGED
        event.name     = 'Device signal strength changed'
      end
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::EMBEDDEDCELLULAR_SIGNAL_CHANGED).destroy_all
  end

end
