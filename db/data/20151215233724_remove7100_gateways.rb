class Remove7100Gateways < SeedMigration::Migration

  def up
    category      = DeviceCategory.gateway.first
    default_model = DeviceModel.default(category.class_name)
    model_7100    = category.models.find_by_name '7100'

    # Mode existing 7100 devices to the default gateway category
    model_7100.devices.update_all(model_id: default_model.id)
    model_7100.destroy
  end

  def down
    category = DeviceCategory.gateway.first
    category.models.create(name: '7100', model_version: '7100', class_name: category.class_name,)
  end
end
