class UpdateOfficialNameOfPlus1300AndPlus1400Timezones < SeedMigration::Migration
  def up
    plus_1300_tz = Timezone.where(off: 780).first
    plus_1400_tz = Timezone.where(off: 840).first
    return unless plus_1300_tz.present? and plus_1400_tz.present?

    plus_1300_tz.update_attributes(name: 'Tonga',      official_name: "(GMT +13:00) Nuku'alofa, Tonga")
    plus_1400_tz.update_attributes(name: 'Kiritimati', official_name: '(GMT +14:00) Kiritimati, Kiribati')
  end

  def down
    plus_1300_tz = Timezone.where(off: 780).first
    plus_1400_tz = Timezone.where(off: 840).first
    return unless plus_1300_tz.present? and plus_1400_tz.present?

    plus_1300_tz.update_attributes(name: "Nuku'alofa",    official_name: '(GMT +13:00) Eniwetok, Kwajalein')
    plus_1400_tz.update_attributes(name: 'Tokelau Is.',   official_name: '(GMT +14:00) Eniwetok, Kwajalein')
  end
end
