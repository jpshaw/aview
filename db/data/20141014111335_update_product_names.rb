class UpdateProductNames < SeedMigration::Migration
  def up
    # Remote Manager
    rm_category = DeviceCategory.find_by_name('Remote Manager')
    rm_model = rm_category.models.find_by_name '5300-RM'
    rm_model.update_attributes(name: '5400-RM', model_version: '5400-RM')

    # Netreach
    nr_category = DeviceCategory.find_by_name('WiFi')
    nr_model = nr_category.models.find_by_name '1000'
    nr_model.update_attributes(name: '4200-NX', model_version: '4200-NX')
  end

  def down
    # Remote Manager
    rm_category = DeviceCategory.find_by_name('Remote Manager')
    rm_model = rm_category.models.find_by_name '5400-RM'
    rm_model.update_attributes(name: '5300-RM', model_version: '5300-RM')

    # Netreach
    nr_category = DeviceCategory.find_by_name('WiFi')
    nr_model = nr_category.models.find_by_name '4200-NX'
    nr_model.update_attributes(name: '1000', model_version: '1000')
  end
end
