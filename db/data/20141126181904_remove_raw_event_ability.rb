class RemoveRawEventAbility < SeedMigration::Migration
  def up
    Ability.destroy_all(name: 'View Raw Events')
  end

  def down

  end
end
