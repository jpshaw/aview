class ResetGatewayVrrpStatus < SeedMigration::Migration
  def up
    vrrp_devices.find_each { |device| device.update(device_status: nil) }
  end

  def down
    # Don't do anything since vrrp state no longer exists in gateway model
  end

  private

  def vrrp_devices
    Gateway.includes(:details).where.not(gateway_details: { vrrp_state: nil })
  end
end
