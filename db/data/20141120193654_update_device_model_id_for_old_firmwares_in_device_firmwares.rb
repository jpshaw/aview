class UpdateDeviceModelIdForOldFirmwaresInDeviceFirmwares < SeedMigration::Migration

  def up
    DeviceFirmware
      .where(img_type: DeviceFirmware::Type::NETBRIDGE_IMG)
      .update_all(device_model_id: DeviceModel.find_by_name('SX').id)

    DeviceFirmware
      .where(img_type: DeviceFirmware::Type::NETBRIDGE_IMG_2)
      .update_all(device_model_id: DeviceModel.find_by_name('6200-FX').id)
  end

  def down
    DeviceFirmware
      .where('img_type is not null')
      .update_all(device_model_id: nil)
  end

end
