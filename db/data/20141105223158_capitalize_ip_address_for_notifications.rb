class CapitalizeIpAddressForNotifications < SeedMigration::Migration
  def up
    DeviceEventUUID.where("name like '% ip %'").each do |uuid|
      uuid.name = uuid.name.gsub(' ip ', ' IP ')
      uuid.save!
    end
  end

  def down

  end
end
