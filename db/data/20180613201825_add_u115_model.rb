class AddU115Model < SeedMigration::Migration
  def up
    category = DeviceCategory.gateway.first

    category.models.create!(
      name: Gateway::GW_U115_MODEL,
      model_version: Gateway::GW_U115_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: Gateway::GW_U115_MODEL).destroy_all
  end
end
