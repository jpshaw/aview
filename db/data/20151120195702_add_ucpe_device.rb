class AddUcpeDevice < SeedMigration::Migration
  def up
    category = DeviceCategory.ucpe.first_or_create!(
      name:         DeviceCategory::CATEGORIES[:ucpe][:name],
      description: 'uCPE device category',
      class_name:   Ucpe.to_s
    )

    category.models.create!(
      name:          Ucpe::SD_9400_MODEL,
      model_version: Ucpe::SD_9400_MODEL,
      class_name:    Ucpe.to_s
    )

    events = [
      { code: EventUUIDS::UCPE_UP,             name: 'Device recovered' },
      { code: EventUUIDS::UCPE_DOWN,           name: 'Device down' },
      { code: EventUUIDS::UCPE_FW_CHANGED,     name: 'Device firmware changed' },
      { code: EventUUIDS::UCPE_UNREACHABLE,    name: 'Device unreachable' },
      { code: EventUUIDS::UCPE_IP_CHANGED,     name: 'Device primary ip changed' },
      { code: EventUUIDS::UCPE_REBOOTED,       name: 'Device rebooted' },
      { code: EventUUIDS::UCPE_WAN_CHANGED,    name: 'Device WAN interface changed' }
    ]

    events.each { |event| category.uuids.where(event).first_or_create! }
  end

  def down
    DeviceCategory.ucpe.destroy_all
  end
end
