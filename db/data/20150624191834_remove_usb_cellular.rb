class RemoveUsbCellular < SeedMigration::Migration
  def up
    usb_category      = DeviceCategory.where(name: 'USB Cellular').first
    cellular_category = DeviceCategory.where(name: 'Cellular').first

    if usb_category.present? && cellular_category.present?
      lx_model = cellular_category.models.where(name: Cellular::LX_6300_MODEL).first

      usb_category.devices.update_all(
        category_id: cellular_category.id,
        model_id:    lx_model.id,
        object_type: 'Cellular'
      )

      usb_category.destroy
    end
  end

  def down
    usb_category = DeviceCategory.where(name: 'USB Cellular').first_or_create(
      description: 'USB Cellular device category',
      class_name:  'UsbCellular'
    )
    cellular_category = DeviceCategory.where(name: 'Cellular').first

    if usb_category.present? && cellular_category.present?
      lx_model = usb_category.models.where(name: Cellular::LX_6300_MODEL).first

      cellular_category.devices.update_all(
        category_id: usb_category.id,
        model_id:    lx_model.id,
        object_type: 'UsbCellular'
      )
    end
  end
end
