class RenameCellularToUsbCellular < SeedMigration::Migration
  def up
    DeviceCategory.where(name: 'Cellular').update_all(name: 'USB Cellular')
  end

  def down
    DeviceCategory.where(name: 'USB Cellular').update_all(name: 'Cellular')
  end
end
