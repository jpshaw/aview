class AddRebootUuuidsToUcLinuxDevices < SeedMigration::Migration
  def up
    [:remote_manager, :dial_to_ip, :embedded_cellular, :usb_cellular].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_name = "#{category.class_name}_REBOOTED".upcase
      event_code = "EventUUIDS::#{uuid_name}".constantize

      event = {
        code: event_code,
        name: 'Device rebooted'
      }

      category.uuids.where(event).first_or_create!
    end
  end

  def down
    [:remote_manager, :dial_to_ip, :embedded_cellular, :usb_cellular].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_name = "#{category.class_name}_REBOOTED".upcase
      event_code = "EventUUIDS::#{uuid_name}".constantize

      category.uuids.where(code: event_code).destroy_all
    end
  end
end
