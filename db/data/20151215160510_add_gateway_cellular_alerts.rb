class AddGatewayCellularAlerts < SeedMigration::Migration
  def up
    category = DeviceCategory.gateway.first
    return unless category.present?

    DeviceEventUUID.create! do |event|
      event.category = category
      event.code     = EventUUIDS::GATEWAY_SIGNAL_CHANGED
      event.name     = 'Device signal strength fell below a nominal threshold'
    end

    DeviceEventUUID.create! do |event|
      event.category = category
      event.code     = EventUUIDS::GATEWAY_CNTI_CHANGED
      event.name     = 'Device cellular network type changed'
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::GATEWAY_SIGNAL_CHANGED).destroy_all
    DeviceEventUUID.where(code: EventUUIDS::GATEWAY_CNTI_CHANGED).destroy_all
  end
end
