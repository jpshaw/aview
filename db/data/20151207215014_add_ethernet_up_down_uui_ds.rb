class AddEthernetUpDownUuiDs < SeedMigration::Migration
  def up
    [:cellular, :remote_manager].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_connected_name     = "#{category.class_name}_ETHERNET_CONNECTED".upcase
      uuid_disconnected_name  = "#{category.class_name}_ETHERNET_DISCONNECTED".upcase

      event_connected_code    = "EventUUIDS::#{uuid_connected_name}".constantize
      event_disconnected_code = "EventUUIDS::#{uuid_disconnected_name}".constantize

      category.uuids.where(code: event_connected_code,
                           name: 'Device ethernet port connected').first_or_create!
      category.uuids.where(code: event_disconnected_code,
                           name: 'Device ethernet port disconnected').first_or_create!
    end
  end

  def down
    [:cellular, :remote_manager].each do |category_name|
      category = DeviceCategory.__send__(category_name).first
      next unless category.present?

      uuid_connected_name     = "#{category.class_name}_ETHERNET_CONNECTED".upcase
      uuid_disconnected_name  = "#{category.class_name}_ETHERNET_DISCONNECTED".upcase

      event_connected_code    = "EventUUIDS::#{uuid_connected_name}".constantize
      event_disconnected_code = "EventUUIDS::#{uuid_disconnected_name}".constantize

      category.uuids.where(code: [event_connected_code, event_disconnected_code]).destroy_all
    end
  end
end
