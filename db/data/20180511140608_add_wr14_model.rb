class AddWr14Model < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first

    category.models.create!(
      name: Cellular::WR14_MODEL,
      model_version: Cellular::WR14_MODEL,
      class_name: category.class_name
    )

  end

  def down
    DeviceModel.where(name: Cellular::WR14_MODEL).destroy_all
  end
end
