class Add633xMxModels < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first

    category.models.create!(
      name: Cellular::MX_6330_MODEL,
      model_version: Cellular::MX_6330_MODEL,
      class_name: category.class_name
    )

    category.models.create!(
      name: Cellular::MX_6335_MODEL,
      model_version: Cellular::MX_6335_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: Cellular::MX_6330_MODEL).destroy_all
    DeviceModel.where(name: Cellular::MX_6335_MODEL).destroy_all
  end
end
