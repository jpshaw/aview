class AddAbilityToAssignNonProductionFirmwaresToRootAdminRole < SeedMigration::Migration

  ABILITY_NAME = 'Assign Non-Production Firmwares'.freeze

  def up
    ability     = Ability.find_by_name(ABILITY_NAME)
    root_admin  = Organization.root.admin_role
    root_admin.abilities << ability unless root_admin.abilities.include?(ability)
  end

  def down
    RoleAbility.joins(:ability)
               .where(abilities: { name: ABILITY_NAME })
               .destroy_all
  end

end
