class Replace5402RmWithConnectIt4Model < SeedMigration::Migration
  def up
    category = DeviceCategory.remote_manager.first
    rm5402_model = category.models.where(name: RemoteManager::RM_5402_MODEL).first

    it4_model = category.models.create!(
      name: RemoteManager::CONNECT_IT_4_MODEL,
      model_version: RemoteManager::CONNECT_IT_4_MODEL,
      class_name: category.class_name
    )

    rm5402_model.devices.find_each do |device|
      device.hw_version = RemoteManager::CONNECT_IT_4_MODEL
      device.category_id = category.id
      device.model_id = it4_model.id
      device.save!
    end

    rm5402_model.destroy
  end

  def down
    category = DeviceCategory.remote_manager.first
    it4_model = category.models.where(name: RemoteManager::CONNECT_IT_4_MODEL).first

    rm5402_model = category.models.create!(
      name: RemoteManager::RM_5402_MODEL,
      model_version: RemoteManager::RM_5402_MODEL,
      class_name: category.class_name
    )

    it4_model.devices.find_each do |device|
      device.hw_version = RemoteManager::RM_5402_MODEL
      device.category_id = category.id
      device.model_id = rm5402_model.id
      device.save!
    end

    it4_model.destroy
  end
end
