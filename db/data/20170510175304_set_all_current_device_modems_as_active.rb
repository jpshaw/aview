class SetAllCurrentDeviceModemsAsActive < SeedMigration::Migration
  def up
    DeviceModem.where(active: nil).update_all(active: true)
  end

  def down
    DeviceModem.update_all(active: nil)
  end
end
