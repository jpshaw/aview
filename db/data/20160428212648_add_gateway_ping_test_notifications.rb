class AddGatewayPingTestNotifications < SeedMigration::Migration

  NOTIFICATIONS = {
    EventUUIDS::GATEWAY_PING_TEST_SUCCEEDED => 'Ping test succeeded',
    EventUUIDS::GATEWAY_PING_TEST_FAILED => 'Ping test failed'
  }

  def up
    NOTIFICATIONS.each do |code, name|
      category.uuids.where(code: code, name: name).first_or_create
    end
  end

  def down
    category.uuids.where(code: NOTIFICATIONS.keys).destroy_all
  end

  def category
    DeviceCategory.gateway.first
  end

end