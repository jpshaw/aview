class RemoveDefaultDeviceModels < SeedMigration::Migration
  def up
    DeviceCategory.all.each do |category|
      old_default_model = category.models.find_by_name('Default')
      new_default_model = DeviceModel.find_by_name(category.class_name.constantize.default_model)

      next unless old_default_model.present?

      if old_default_model.devices.any? && new_default_model.present?
        # Move existing default devices to the new "default" model.
        puts "Moving #{old_default_model.devices.count} devices from default model to #{new_default_model.name} model."
        old_default_model.devices.update_all(model_id: new_default_model.id)
      end

      old_default_model.destroy
      puts "Removed default model #{old_default_model.id} for #{category.name} category."
    end
  end

  def down
    DeviceCategory.all.each do |category|
      category.models.create(name: 'Default', model_version: 'Default', class_name: category.class_name)
    end
  end
end
