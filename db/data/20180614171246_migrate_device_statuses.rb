class MigrateDeviceStatuses < SeedMigration::Migration

  LegacyStatuses = {
    Devices::LegacyStatus::UP => Devices::Status::UP,
    Devices::LegacyStatus::UNREACHABLE => Devices::Status::UNREACHABLE,
    Devices::LegacyStatus::DOWN => Devices::Status::DOWN,
    Devices::LegacyStatus::UNDEPLOYED => Devices::Status::UNDEPLOYED,
    Devices::LegacyStatus::INACTIVE => Devices::Status::INACTIVE,
    Devices::LegacyStatus::TUNNEL_DOWN => Devices::Status::TUNNEL_DOWN,
    Devices::LegacyStatus::BACKUP_MODE => Devices::Status::BACKUP_MODE,
    Devices::LegacyStatus::VRRP_MASTER => Devices::Status::VRRP_MASTER,
    Devices::LegacyStatus::DUAL_WAN_BOTH_UP => Devices::Status::DUAL_WAN_BOTH_UP,
    Devices::LegacyStatus::DUAL_WAN_BOTH_DOWN => Devices::Status::DUAL_WAN_BOTH_DOWN,
    Devices::LegacyStatus::DUAL_WAN_WAN1_UP_WAN2_DOWN => Devices::Status::DUAL_WAN_WAN1_UP_WAN2_DOWN,
    Devices::LegacyStatus::DUAL_WAN_WAN1_DOWN_WAN2_UP => Devices::Status::DUAL_WAN_WAN1_DOWN_WAN2_UP,
  }.freeze

  def up
    Device.joins(:device_state).find_each do |device|
      if current_state = device.device_state.state
        new_state = LegacyStatuses[current_state] || Devices::Status::UNREACHABLE
        device.device_state.update(state: new_state)
      end
    end
  end

  def down
    Device.joins(:device_state).find_each do |device|
      if current_state = device.device_state.state
        new_state = LegacyStatuses.key(current_state) || Devices::LegacyStatus::UNREACHABLE
        device.device_state.update(state: new_state)
      end
    end
  end
end
