class AddUsbCellularDevice < SeedMigration::Migration
  def up
    category = DeviceCategory.usb_cellular.first_or_create!(
      name:         DeviceCategory::CATEGORIES[:usb_cellular][:name],
      description: 'USB Cellular device category',
      class_name:   UsbCellular.to_s
    )

    category.models.create!(
      name:          UsbCellular::LX_6300_MODEL,
      model_version: UsbCellular::LX_6300_MODEL,
      class_name:    UsbCellular.to_s
    )

    events = [
      { code: EventUUIDS::USBCELLULAR_UP,             name: 'Device recovered' },
      { code: EventUUIDS::USBCELLULAR_DOWN,           name: 'Device down' },
      { code: EventUUIDS::USBCELLULAR_FW_CHANGED,     name: 'Device firmware changed' },
      { code: EventUUIDS::USBCELLULAR_UNREACHABLE,    name: 'Device unreachable' },
      { code: EventUUIDS::USBCELLULAR_IP_CHANGED,     name: 'Device primary ip changed' },
      { code: EventUUIDS::USBCELLULAR_CNTI_CHANGED,   name: 'Device cellular network type changed' },
      { code: EventUUIDS::USBCELLULAR_WAN_CHANGED,    name: 'Device WAN interface changed' },
      { code: EventUUIDS::USBCELLULAR_SIGNAL_CHANGED, name: 'Device signal strength changed' }
    ]

    events.each { |event| category.uuids.where(event).first_or_create! }
  end

  def down
    DeviceCategory.usb_cellular.destroy_all
  end
end
