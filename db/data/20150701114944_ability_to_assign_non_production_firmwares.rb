class AbilityToAssignNonProductionFirmwares < SeedMigration::Migration

  ABILITY_NAME        = 'Assign Non-Production Firmwares'.freeze
  ABILITY_DESCRIPTION = 'assign non-production firmwares to a device'.freeze

  def up
    Ability.create(name: ABILITY_NAME, description: ABILITY_DESCRIPTION)
  end

  def down
    Ability.destroy_all(name: ABILITY_NAME)
  end

end
