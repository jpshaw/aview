class AddRawEventAbility < SeedMigration::Migration
  def up
    ability = Ability.create(name: 'View Raw Events', description: 'view raw device events')
    root    = Organization.root
    role    = root.admin_role
    role.abilities << ability unless role.abilities.include?(ability)
  end

  def down
    Ability.destroy_all(name: 'View Raw Events')
  end
end
