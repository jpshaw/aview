class AddRogueAccessPointNotificationToNetreachDevice < SeedMigration::Migration
  def up
    if category = DeviceCategory.wifi.first
      DeviceEventUUID.create! do |event|
        event.category = category
        event.code     = EventUUIDS::NETREACH_ROGUE_ACCESS_POINT
        event.name     = 'Device detected rogue Access Point'
      end
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::NETREACH_ROGUE_ACCESS_POINT).destroy_all
  end
end
