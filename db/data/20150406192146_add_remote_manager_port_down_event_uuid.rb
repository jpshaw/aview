class AddRemoteManagerPortDownEventUuid < SeedMigration::Migration

  def up
    if category = DeviceCategory.remote_manager.first
      DeviceEventUUID.create! do |event|
        event.category = category
        event.code     = EventUUIDS::REMOTEMANAGER_PORT_DOWN
        event.name     = 'Device port down'
      end
    end
  end

  def down
    DeviceEventUUID.where(code: EventUUIDS::REMOTEMANAGER_PORT_DOWN).destroy_all
  end

end
