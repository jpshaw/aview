class Add631xModels < SeedMigration::Migration
  def up
    category = DeviceCategory.cellular.first

    category.models.create!(
      name: Cellular::DX_6310_MODEL,
      model_version: Cellular::DX_6310_MODEL,
      class_name: category.class_name
    )
  end

  def down
    DeviceModel.where(name: Cellular::DX_6310_MODEL).destroy_all
  end
end
