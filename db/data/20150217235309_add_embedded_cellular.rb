class AddEmbeddedCellular < SeedMigration::Migration
  def up
    category = DeviceCategory.embedded_cellular.first_or_create!(
      name:         DeviceCategory::CATEGORIES[:embedded_cellular][:name],
      description: 'Embedded Cellular device category',
      class_name:   EmbeddedCellular.to_s
    )

    # Base model
    category.models.create!(
      name:          EmbeddedCellular::CX_6300_MODEL,
      model_version: EmbeddedCellular::CX_6300_MODEL,
      class_name:    EmbeddedCellular.to_s
    )

    events = [
      { code: EventUUIDS::EMBEDDEDCELLULAR_UP,           name: 'Device recovered' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_DOWN,         name: 'Device down' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_FW_CHANGED,   name: 'Device firmware changed' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_UNREACHABLE,  name: 'Device unreachable' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_IP_CHANGED,   name: 'Device primary ip changed' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_CNTI_CHANGED, name: 'Device cellular network type changed' },
      { code: EventUUIDS::EMBEDDEDCELLULAR_WAN_CHANGED,  name: 'Device WAN interface changed' }
    ]

    events.each { |event| category.uuids.where(event).first_or_create! }
  end

  def down
    DeviceCategory.embedded_cellular.destroy_all
  end
end
