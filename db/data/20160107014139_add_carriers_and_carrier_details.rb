class AddCarriersAndCarrierDetails < SeedMigration::Migration
  def up
    carrier_service = CarrierInformationService.new
    carrier_service.load_records_from_file
    carrier_service.seed
  end

  def down
    CarrierDetail.destroy_all
    Carrier.destroy_all
  end

  private

  def records
    @records ||= YAML.load(File.read('db/seeds/carrier_info.yml'))
  end
end
