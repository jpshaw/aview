class AddDeployAbilityToDeviceModifiers < SeedMigration::Migration
  def up
    ability = Ability.find_by_name('Deploy Devices')

    # Find roles with the ability to `Modify Devices` and add the new ability
    Role.joins(:abilities).where(abilities: { name: 'Modify Devices' }).each do |role|
      role.abilities << ability unless role.abilities.include?(ability)
    end
  end

  def down
    RoleAbility.joins(:ability)
               .where(abilities: { name: 'Deploy Devices' })
               .destroy_all
  end
end
