class AddIndexesToNotifications < ActiveRecord::Migration
  def change
    add_index :notifications, :event_id, name: 'idx_not_ev_id'
    add_index :notifications, :notification_subscription_id, name: 'idx_not_sub_id'
  end
end
