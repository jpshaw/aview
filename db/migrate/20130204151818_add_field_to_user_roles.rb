class AddFieldToUserRoles < ActiveRecord::Migration
  def change
    add_column :user_roles, :can_admin_configurations, :boolean, :default => false
  end
end
