class RemoveCountryFieldFromLocations < ActiveRecord::Migration
  def up
    remove_column :locations, :country
  end

  def down
    add_column :locations, :country, :string, :limit => 50, :default => ""
  end
end
