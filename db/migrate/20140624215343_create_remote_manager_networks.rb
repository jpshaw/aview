class CreateRemoteManagerNetworks < ActiveRecord::Migration
  def change
    create_table :remote_manager_networks do |t|
      t.integer :device_id
      t.integer :index
      t.integer :metric0
      t.integer :rx
      t.integer :tx

      t.string :gw0, :length => 45
      t.string :network_iface, :length => 10
      t.string :ip0, :length => 45
      t.string :ip1, :length => 45
      t.string :mtu, :length => 10
      t.string :priority, :length => 15
      t.string :speed, :length => 20
      t.string :status, :length => 20
      t.string :net_type, :length => 15

      t.timestamps
    end
  end
end
