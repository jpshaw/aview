class ChangeObjectTypeForNotificationSubscriptions < ActiveRecord::Migration
  def up
    change_column :notification_subscriptions, :object_type, :integer
  end

  def down
    change_column :notification_subscriptions, :object_type, :string
  end
end
