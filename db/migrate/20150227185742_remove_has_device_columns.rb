class RemoveHasDeviceColumns < ActiveRecord::Migration
  def up
    remove_column :organizations, :has_netgates
    remove_column :organizations, :has_netreaches
    remove_column :organizations, :has_netbridges
    remove_column :organizations, :has_opticells
  end

  def down
    add_column :organizations, :has_netgates, :boolean, default: false
    add_column :organizations, :has_netreaches, :boolean, default: false
    add_column :organizations, :has_netbridges, :boolean, default: false
    add_column :organizations, :has_opticells, :boolean, default: false
  end
end
