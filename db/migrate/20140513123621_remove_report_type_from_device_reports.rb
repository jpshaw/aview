class RemoveReportTypeFromDeviceReports < ActiveRecord::Migration

  def up
    remove_column :device_reports, :report_type
  end

  def down
    add_column :device_reports, :report_type, :string
  end

end
