class CreateDeviceImports < ActiveRecord::Migration
  def change
    create_table :device_imports do |t|
      t.integer :user_id
      t.string :user_email
      t.integer :user_organization_id
      t.integer :organization_id
      t.integer :customer_id
      t.text :device_ids
      t.text :failed_macs
      t.integer :device_count

      t.timestamps
    end
  end
end
