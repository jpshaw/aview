class FixIndexForOrganizationsContacts < ActiveRecord::Migration
  def up
    remove_index :organizations_contacts, :name => :idx_org_con_org_id
    remove_index :organizations_contacts, :name => :idx_org_con_con_id
    add_index :organizations_contacts, [:organization_id, :contact_id], :unique => true, :name => 'idx_org_con_org_con'
  end

  def down
    add_index :organizations_contacts, :organization_id, :name => 'idx_org_con_org_id'
    add_index :organizations_contacts, :contact_id, :name => 'idx_org_con_con_id'
    remove_index :organizations_contacts, :name => 'idx_org_con_org_con'
  end
end
