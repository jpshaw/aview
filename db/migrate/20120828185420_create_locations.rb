class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :address_1, :limit => 150, :default => ""
      t.string :address_2, :limit => 50, :default => ""
      t.string :city, :limit => 100, :default => ""
      t.string :state, :limit => 10, :default => ""
      t.string :zip, :limit => 10, :default => ""
      t.string :country, :limit => 50, :default => ""
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
