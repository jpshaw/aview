class AddSignedToCertificates < ActiveRecord::Migration

  def change
    add_column :certificates, :signed, :boolean, default: false
  end

end
