class ChangeLocationCoordinateTypesToDecimal < ActiveRecord::Migration
  def change
    rename_column :locations, :latitude, :f_latitude
    rename_column :locations, :longitude, :f_longitude

    # From http://stackoverflow.com/a/12504340/667165
    #   Latitudes range from -90 to +90 (degrees), so DECIMAL(10, 8) is ok for
    #   that, but longitudes range from -180 to +180 (degrees) so you need
    #   DECIMAL(11, 8)
    add_column :locations, :latitude, :decimal, precision: 10, scale: 8
    add_column :locations, :longitude, :decimal, precision: 11, scale: 8
  end
end
