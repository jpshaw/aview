class AddConfigurationAssociatedAtToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :configuration_associated_at, :datetime
  end
end
