class AddRxTxToNetbridgeModems < ActiveRecord::Migration
  def change
    add_column :netbridge_modems, :rx, :integer, :length => 8
    add_column :netbridge_modems, :tx, :integer, :length => 8
    add_column :netbridge_modems, :rx_30, :integer, :length => 8
    add_column :netbridge_modems, :tx_30, :integer, :length => 8
  end
end
