class RenameModeColumnToTunnelMode < ActiveRecord::Migration
  def change
    rename_column :gateway_tunnels, :mode, :tunnel_mode
  end
end
