class RemoveAccountIdFromDevices < ActiveRecord::Migration
  def change
    remove_column :devices, :account_id
  end
end
