class AddMissingFieldsFromFirmwareManagementToDeviceFirmwares < ActiveRecord::Migration

  def change
    add_column :device_firmwares, :schema_version,      :integer
    add_column :device_firmwares, :migration_versions,  :text
    add_column :device_firmwares, :production,          :boolean, default: false
    add_column :device_firmwares, :deprecated,          :boolean, default: false
  end

end
