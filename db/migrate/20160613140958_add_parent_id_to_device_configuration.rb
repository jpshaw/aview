class AddParentIdToDeviceConfiguration < ActiveRecord::Migration
  def change
    add_reference :device_configurations, :parent, index: true
  end
end
