class AddMccMncLacCidIndexToCellularLocationTable < ActiveRecord::Migration
  def change
    add_index :cellular_locations, :mcc
    add_index :cellular_locations, :mnc
    add_index :cellular_locations, :lac
    add_index :cellular_locations, :cid
  end
end
