class CreateJoinTableDeviceTunnelServer < ActiveRecord::Migration
  def change
    create_join_table :devices, :tunnel_servers do |t|
      t.index [:device_id, :tunnel_server_id]
      t.index [:tunnel_server_id, :device_id]
    end
  end
end
