class ChangeSignalStrenghtsThresholdPercentageToInteger < ActiveRecord::Migration

  def up
    change_column :signal_strengths, :threshold_percentage, :integer
  end

  def down
    change_column :signal_strengths, :threshold_percentage, :decimal, precision: 5, scale: 2
  end

end
