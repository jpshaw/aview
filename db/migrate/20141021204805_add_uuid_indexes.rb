class AddUuidIndexes < ActiveRecord::Migration
  def change
    add_index :device_events, :uuid_id, name: 'idx_dev_ev_uuid_id'
    add_index :subscribed_events, :uuid_id, name: 'idx_sub_ev_uuid_id'
  end
end
