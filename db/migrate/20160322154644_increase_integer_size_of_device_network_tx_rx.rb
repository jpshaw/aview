class IncreaseIntegerSizeOfDeviceNetworkTxRx < ActiveRecord::Migration
  def up
    change_column :device_networks, :tx, :integer, limit: 8
    change_column :device_networks, :rx, :integer, limit: 8
  end

  def down
    change_column :device_networks, :tx, :integer, limit: 4
    change_column :device_networks, :rx, :integer, limit: 4
  end
end
