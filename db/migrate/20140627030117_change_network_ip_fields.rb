class ChangeNetworkIpFields < ActiveRecord::Migration
  def up
    remove_column :device_networks, :ip0
    remove_column :device_networks, :ip1
    add_column :device_networks, :ip_addrs, :text
  end
  def down
    add_column :device_networks, :ip0, :string, :length => 45
    add_column :device_networks, :ip1, :string, :length => 45
    remove_column :device_networks, :ip_addrs
  end
end
