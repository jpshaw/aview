class RemoveDevicesCountFromSites < ActiveRecord::Migration

  def up
    remove_column :sites, :devices_count
  end

  def down
    add_column :sites, :devices_count, :integer, default: 0
  end

end
