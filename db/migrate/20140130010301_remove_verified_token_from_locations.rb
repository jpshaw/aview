class RemoveVerifiedTokenFromLocations < ActiveRecord::Migration
  def up
    remove_column :locations, :verified_token
  end

  def down
    add_column :locations, :verified_token, :string, :limit => 32
  end
end
