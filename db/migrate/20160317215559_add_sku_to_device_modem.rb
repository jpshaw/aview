class AddSkuToDeviceModem < ActiveRecord::Migration
  def change
    add_column :device_modems, :sku, :string
  end
end
