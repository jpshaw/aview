class CreateRoleAbilities < ActiveRecord::Migration
  def change
    create_table :role_abilities do |t|
      t.references :role
      t.references :ability
    end
    add_index :role_abilities, :role_id, name: "idx_rol_abi_role_id"
    add_index :role_abilities, :ability_id, name: "idx_rol_abi_abi_id"
    add_index :role_abilities, [:role_id, :ability_id], unique: true, name: "idx_rol_abi_role_id_abi_id"
  end
end
