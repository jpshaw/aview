class CreateConfigurations < ActiveRecord::Migration
  def change
    create_table :configurations do |t|
      t.string :name, index: true
      t.references :organization, index: true, foreign_key: true
      t.integer :config_type, null: false, index: true, default: 0
      t.integer :parent_id, index: true
      t.binary :data, limit: 1.megabyte
      t.timestamps null: false
    end
  end
end
