class AddVrrpStateToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :vrrp_state, :string
  end
end
