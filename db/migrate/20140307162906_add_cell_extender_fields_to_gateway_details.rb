class AddCellExtenderFieldsToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :wan_1_cell_extender, :string, :limit => 2
    add_column :gateway_details, :wan_2_cell_extender, :string, :limit => 2
  end
end
