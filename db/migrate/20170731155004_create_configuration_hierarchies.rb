class CreateConfigurationHierarchies < ActiveRecord::Migration
  def change
    create_table :configuration_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :configuration_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "configuration_anc_desc_idx"

    add_index :configuration_hierarchies, [:descendant_id],
      name: "configuration_desc_idx"
  end
end
