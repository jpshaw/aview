class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :mac, :limit => 12
      t.string :host, :limit => 45
      t.string :mgmt_host, :limit => 45
      t.integer :organization_id
      t.integer :location_id
      t.integer :configuration_id
      t.integer :account_id
      t.integer :model_id
      t.integer :category_id
      t.integer :site_id
      t.boolean :is_deployed, :default => false
      t.boolean :is_down, :default => false
      t.string :firmware, :limit => 50
      t.datetime :last_heartbeat_at
      t.timestamps
    end
  end
end
