class AddSubscribedEvents < ActiveRecord::Migration
  def change
    create_table :subscribed_events do |t|
      t.integer :profile_id
      t.integer :uuid_id
      t.integer :frequency
    end
  end
end
