class AddTunnelNameToGatewayTunnel < ActiveRecord::Migration
  def change
    add_column :gateway_tunnels, :tunnel_name, :string, default: false
  end
end
