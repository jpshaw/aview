class AddDeviceIndexToNetbridgeModems < ActiveRecord::Migration
  def change
    add_index :netbridge_modems, :device_id, name: 'idx_nb_mdm_dev_id'
  end
end
