class CreateDialTestResults < ActiveRecord::Migration
  def change
    create_table :dial_test_results do |t|
      t.integer :device_id
      t.integer :result
      t.string :reason
      t.integer :category
      t.integer :source
      t.timestamps null: false
    end

    add_index :dial_test_results, :device_id
  end
end
