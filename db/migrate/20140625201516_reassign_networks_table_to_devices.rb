class ReassignNetworksTableToDevices < ActiveRecord::Migration
  def change
    rename_table :remote_manager_networks, :device_networks
  end
end
