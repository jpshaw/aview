class AddFieldsToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :input_token, :string, :limit => 32
    add_column :locations, :verified_token, :string, :limit => 32
    add_column :locations, :is_verified, :boolean, :default => false
    add_column :locations, :formatted_address, :string

    add_index :locations, :input_token, :unique => true, :name => 'idx_lctn_npt_tkn'
    add_index :locations, :verified_token, :unique => true, :name => 'idx_lctn_vrfd_tkn'
  end
end
