require 'device'
require 'devices/gateway'

class MoveDeviceStateFromDevicesToDeviceState < ActiveRecord::Migration
  def up
    Device.find_each do | device |
      device.create_device_state :state => device.device_state
    end
    remove_column :devices, :device_state
  end

  def down
    #this will not copy the data back from the device_states table
    add_column :devices, :device_state
    drop_table :device_states
  end
end
