class AddConfigurationCheckedAtToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :configuration_checked_at, :datetime
  end
end
