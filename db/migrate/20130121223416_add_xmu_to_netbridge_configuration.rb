class AddXmuToNetbridgeConfiguration < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :xmu, :string
  end
end
