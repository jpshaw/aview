
class CreateGatewayPortalProfiles < ActiveRecord::Migration
  def change

    opts = ''
    if ActiveRecord::Base.configurations[Rails.env]['adapter'] == 'mysql2'
      opts = 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin'
    end

    create_table :gateway_portal_profiles, :options => opts do |t|
      t.string :password, :length => 50
      t.string :ssl_password, :length => 50
      t.integer :ssl_port, :default => 443
      t.string :auth_user, :length => 50
      t.string :auth_password, :length => 50
      t.timestamps
    end

  end
end
