class CreateHardwareModels < ActiveRecord::Migration
  def change
    create_table :device_models do |t|
      t.integer :category_id
      t.string :name, :limit => 50
      t.string :description
      t.timestamps
    end
  end
end
