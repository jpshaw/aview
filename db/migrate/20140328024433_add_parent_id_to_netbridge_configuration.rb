class AddParentIdToNetbridgeConfiguration < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :parent_id, :integer
  end
end
