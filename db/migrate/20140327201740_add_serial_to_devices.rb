class AddSerialToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :serial, :string, limit: 16
    add_index  :devices, :serial, :name => "idx_dvc_srl"
  end
end
