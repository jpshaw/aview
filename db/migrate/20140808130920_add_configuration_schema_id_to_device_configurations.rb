class AddConfigurationSchemaIdToDeviceConfigurations < ActiveRecord::Migration

  def change
    add_column :device_configurations, :configuration_schema_id, :integer
    add_index  :device_configurations, :configuration_schema_id, name: "idx_dev_cfg_cfg_sch_id"
  end

end
