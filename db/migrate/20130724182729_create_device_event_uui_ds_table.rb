class CreateDeviceEventUuiDsTable < ActiveRecord::Migration
  def change
    create_table :device_event_uuids do |t|
      t.string :name, :limit => 50
      t.integer :code
      t.integer :category_id
    end
  end
end
