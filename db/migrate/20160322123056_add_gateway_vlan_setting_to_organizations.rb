class AddGatewayVlanSettingToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :gateway_vlan_collection_enabled, :boolean, default: false
  end
end
