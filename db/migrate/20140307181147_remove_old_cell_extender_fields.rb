class RemoveOldCellExtenderFields < ActiveRecord::Migration
  def up
    remove_column :gateway_details, :wan_1_cell_enabled
    remove_column :gateway_details, :wan_2_cell_enabled
  end

  def down
    add_column :gateway_details, :wan_1_cell_enabled, :boolean, :default => false
    add_column :gateway_details, :wan_2_cell_enabled, :boolean, :default => false
  end
end
