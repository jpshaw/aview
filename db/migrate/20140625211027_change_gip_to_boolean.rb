require 'netbridge_configuration'

class ChangeGipToBoolean < ActiveRecord::Migration
  def up
    rename_column :netbridge_configurations, :gip_secondary, :gip_secondary_old
    add_column :netbridge_configurations, :gip_secondary, :boolean

    NetbridgeConfiguration.find_each(batch_size: 500) do |config|
      next unless config.gip_secondary_old.present?

      value = case config.gip_secondary_old
      when '1', 'true'
        true
      when '0', 'false'
        false
      else
        nil
      end

      config.update_attributes(gip_secondary: value)
    end

    remove_column :netbridge_configurations, :gip_secondary_old
  end

  def down
    rename_column :netbridge_configurations, :gip_secondary, :gip_secondary_old
    add_column :netbridge_configurations, :gip_secondary, :string

    NetbridgeConfiguration.find_each(batch_size: 500) do |config|
      next unless config.gip_secondary_old.present?

      value = case config.gip_secondary_old
      when true
        '1'
      when false
        '0'
      else
        nil
      end

      config.update_attributes(gip_secondary: value)
    end

    remove_column :netbridge_configurations, :gip_secondary_old
  end
end
