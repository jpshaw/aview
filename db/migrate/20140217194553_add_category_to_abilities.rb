class AddCategoryToAbilities < ActiveRecord::Migration
  def change
    add_column  :abilities, :category, :string
    add_index   :abilities, :category, name: "idx_abi_cat"
  end
end
