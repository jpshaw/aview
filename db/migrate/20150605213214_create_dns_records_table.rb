class CreateDnsRecordsTable < ActiveRecord::Migration
  def change
    create_table :dns_records, force: true do |t|
      t.integer :domain_id
      t.string  :name
      t.string  :type
      t.string  :content
      t.integer :ttl
      t.integer :prio
      t.integer :change_date
      t.string  :record_type
    end

    add_index :dns_records, :domain_id, name: 'idx_dns_domain_id'
    add_index :dns_records, [:name, :type], name: 'idx_dns_name_type'
  end
end
