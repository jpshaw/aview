class RemoveCategoryFromAbilities < ActiveRecord::Migration

  def up
    remove_column :abilities, :category
  end

  def down
    add_column :abilities, :category, :string
  end

end
