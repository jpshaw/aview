class AddIndexesToDeviceConfigurations < ActiveRecord::Migration
  def change
    add_index :netbridge_configurations, :organization_id, :name => 'idx_nbg_cnf_org_id'
    add_index :netreach_configurations, :organization_id, :name => 'idx_nrch_cnf_org_id'
  end
end
