class AddConfigurationsToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :station_query, :boolean, :default => false
    add_column :netreach_configurations, :anms_enabled, :boolean, :default => true
    add_column :netreach_configurations, :client_monitor, :boolean, :default => false
    add_column :netreach_configurations, :remote_control, :integer, :default => 10
    add_column :netreach_configurations, :upnp_enabled, :boolean, :default => false
    add_column :netreach_configurations, :wan_connection, :string, :limit => 10
    add_column :netreach_configurations, :wan_ip_address, :string, :limit => 20
    add_column :netreach_configurations, :wan_gateway_ip, :string, :limit => 20
    add_column :netreach_configurations, :wan_netmask, :string, :limit => 20
    add_column :netreach_configurations, :dns1, :string, :limit => 20
    add_column :netreach_configurations, :dns2, :string, :limit => 20
  end
end
