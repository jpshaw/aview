class CreateSyslogServers < ActiveRecord::Migration

  def change
    create_table :syslog_servers do |t|
      t.string :name
      t.string :uri
    end
  end

end
