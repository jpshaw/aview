class IncreaseOrganizationFooterLength < ActiveRecord::Migration
  def change
  	change_column :organizations, :footer, :string, :limit => 1023
  end
end
