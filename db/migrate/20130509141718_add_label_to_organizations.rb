class AddLabelToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :label, :integer
  end
end
