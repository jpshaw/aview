class AddIdsAlert < ActiveRecord::Migration
  def change
    create_table :ids_alerts do |t|
      t.integer :sig_gen
      t.integer :sig_id, index: true
      t.integer :sig_rev, index: true
      t.text :msg
      t.string :proto
      t.string :src
      t.integer :srcport
      t.string :dst
      t.integer :dstport
      t.text :classification
      t.integer :priority
      t.string :host, index: true
      t.string :interface
      t.text :reference, limit: 16.megabytes - 1 # mediumtext
      t.datetime :alerted_at, index: true
    end
  end
end
