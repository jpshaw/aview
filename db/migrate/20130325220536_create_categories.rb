class CreateCategories < ActiveRecord::Migration
  def change
    create_table :device_categories do |t|
      t.string :name, :limit => 50
      t.string :description
      t.timestamps
    end
  end
end
