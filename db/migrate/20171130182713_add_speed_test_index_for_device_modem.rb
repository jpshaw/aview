class AddSpeedTestIndexForDeviceModem < ActiveRecord::Migration
  def change
    add_index :speed_tests, :device_modem_id
  end
end
