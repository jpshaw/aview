class AddAnalyticsModeToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :analytics_mode, :boolean, :default => false
  end
end
