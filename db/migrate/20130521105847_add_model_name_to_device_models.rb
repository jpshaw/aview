class AddModelNameToDeviceModels < ActiveRecord::Migration
  def change
    add_column :device_models, :model_name, :string, :limit => 20
  end
end
