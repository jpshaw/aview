class AddLocationDescToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :location_desc, :string, :limit => 100
  end
end
