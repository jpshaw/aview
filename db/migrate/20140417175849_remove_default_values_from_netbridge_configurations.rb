class RemoveDefaultValuesFromNetbridgeConfigurations < ActiveRecord::Migration
  def up
    change_column_default :netbridge_configurations , :config_change_reboot , nil
    change_column_default :netbridge_configurations , :iui                  , nil
    change_column_default :netbridge_configurations , :rbt                  , nil
    change_column_default :netbridge_configurations , :remote_control       , nil
    change_column_default :netbridge_configurations , :sms_check_freq       , nil
    change_column_default :netbridge_configurations , :var                  , nil
    change_column_default :netbridge_configurations , :wdog                 , nil
    change_column_default :netbridge_configurations , :sticky_apn           , nil
    change_column_default :netbridge_configurations , :link_check_freq      , nil
    change_column_default :netbridge_configurations , :link_timeout         , nil
    change_column_default :netbridge_configurations , :traf_acct            , nil

  end
  def down
    change_column_default :netbridge_configurations , :config_change_reboot , false
    change_column_default :netbridge_configurations , :iui                  , false
    change_column_default :netbridge_configurations , :rbt                  , true
    change_column_default :netbridge_configurations , :remote_control       , false
    change_column_default :netbridge_configurations , :sms_check_freq       , "1m"
    change_column_default :netbridge_configurations , :var                  , "A"
    change_column_default :netbridge_configurations , :wdog                 , true
    change_column_default :netbridge_configurations , :sticky_apn           , true
    change_column_default :netbridge_configurations , :link_check_freq      , "4m"
    change_column_default :netbridge_configurations , :link_timeout         , 900
    change_column_default :netbridge_configurations , :traf_acct            , false
  end
end
