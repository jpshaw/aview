class AddMd5ToCertificates < ActiveRecord::Migration

  def change
    add_column  :certificates, :md5, :string
    add_index   :certificates, :md5, unique: true, name: 'idx_cert_md5'
  end

end
