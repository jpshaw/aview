class RefactorDeviceAssociationFields < ActiveRecord::Migration
  def up
    change_table :device_associations do |t|
      t.integer :source_id
      t.string :source_mac
      t.integer :target_id
      t.string :target_mac
      t.text :details

      t.index :source_id
      t.index :source_mac
      t.index :target_id
      t.index :target_mac
    end
  end

  def down
    change_table :device_associations do |t|
      t.remove :source_id, :source_mac, :target_id, :target_mac, :details
    end
  end
end
