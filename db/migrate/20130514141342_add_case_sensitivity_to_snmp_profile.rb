class AddCaseSensitivityToSnmpProfile < ActiveRecord::Migration
  def change
    if ActiveRecord::Base.configurations[Rails.env]['adapter'] == 'mysql2'
      execute %{ALTER TABLE snmp_profiles MODIFY `community` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin;}
      execute %{ALTER TABLE snmp_profiles MODIFY `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin;}
      execute %{ALTER TABLE snmp_profiles MODIFY `auth_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin;}
      execute %{ALTER TABLE snmp_profiles MODIFY `priv_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin;}
    end
  end
end
