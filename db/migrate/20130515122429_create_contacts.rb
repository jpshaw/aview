class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name, :length => 50
      t.string :phone, :length => 20
      t.string :email, :length => 50
      t.integer :label
      t.timestamps
    end
  end
end
