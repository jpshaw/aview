class CreateNetbridgeDetails < ActiveRecord::Migration
  def change
    create_table :netbridge_details do |t|
      t.integer :device_id
      t.string :usb_hub, :length => 10
      t.string :usb_list, :length => 100 

      t.timestamps
    end
  end
end
