class RemovePaperclipColumnsFromImports < ActiveRecord::Migration

  def up
    remove_column :imports, :source_file_name
    remove_column :imports, :source_content_type
    remove_column :imports, :source_file_size
    remove_column :imports, :source_updated_at
    remove_column :imports, :source_fingerprint
  end

  def down
    add_column :imports, :source_file_name,     :string
    add_column :imports, :source_content_type,  :string
    add_column :imports, :source_file_size,     :string
    add_column :imports, :source_updated_at,    :string
    add_column :imports, :source_fingerprint,   :string
  end

end
