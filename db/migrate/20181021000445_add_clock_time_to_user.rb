class AddClockTimeToUser < ActiveRecord::Migration
  def change
  	add_column :users, :clock_enabled, :boolean, :default => false
  end
end
