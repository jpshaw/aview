class AddIndexToNetbridgeModems < ActiveRecord::Migration
  def change
    add_index :netbridge_modems, :netbridge_id, :name => 'idx_nbg_mdm_nbg_id'
  end
end
