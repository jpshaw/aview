class AddDateIndexToEvents < ActiveRecord::Migration
  def change
    add_index :device_events, :created_at, :name => 'idx_dvc_evnt_crtd_at'
  end
end
