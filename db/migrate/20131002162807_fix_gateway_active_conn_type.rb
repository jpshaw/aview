class FixGatewayActiveConnType < ActiveRecord::Migration
  def up
    change_column :gateway_details, :active_conn_type, :string
  end
  def down
    change_column :gateway_details, :active_conn_type, :integer
  end
end
