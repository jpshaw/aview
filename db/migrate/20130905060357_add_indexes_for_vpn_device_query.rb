class AddIndexesForVpnDeviceQuery < ActiveRecord::Migration
  def up
    add_index :devices, :location_id, :name => 'idx_dvc_lctn_id'
    add_index :gateway_details, :device_id, :name => 'idx_gtw_dtl_dvc_id'
  end

  def down
    remove_index :devices, :name => 'idx_dvc_lctn_id'
    remove_index :gateway_details, :name => 'idx_gtw_dtl_dvc_id'
  end
end
