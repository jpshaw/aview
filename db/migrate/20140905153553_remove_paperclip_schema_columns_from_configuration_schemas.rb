class RemovePaperclipSchemaColumnsFromConfigurationSchemas < ActiveRecord::Migration

  def up
    remove_column :configuration_schemas, :schema_content_type
    remove_column :configuration_schemas, :schema_file_name
    remove_column :configuration_schemas, :schema_file_size
    remove_column :configuration_schemas, :schema_updated_at
  end

  def down
    add_column :configuration_schemas, :schema_content_type,  :string
    add_column :configuration_schemas, :schema_file_name,     :string
    add_column :configuration_schemas, :schema_file_size,     :integer
    add_column :configuration_schemas, :schema_updated_at,    :datetime
  end

end
