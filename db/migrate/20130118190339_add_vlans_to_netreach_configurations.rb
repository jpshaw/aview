class AddVlansToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :ssid_1_vlan_id, :string, :limit => 50
    add_column :netreach_configurations, :ssid_2_vlan_id, :string, :limit => 50
  end
end
