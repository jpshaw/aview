class CreateCellularLocations < ActiveRecord::Migration
  def change
    create_table :cellular_locations do |t|
      t.string :radio, :length => 20
      t.integer :mcc
      t.integer :mnc
      t.integer :lac
      t.integer :cid
      t.float :lat
      t.float :lon
      t.decimal :accuracy

      t.timestamps null: false
    end
  end
end
