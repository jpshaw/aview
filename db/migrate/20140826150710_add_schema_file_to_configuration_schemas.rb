class AddSchemaFileToConfigurationSchemas < ActiveRecord::Migration

  def change
    add_column :configuration_schemas, :schema_file, :string
  end

end
