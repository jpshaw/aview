class RemoveCfgDefaultFromNetbridgeConfigurations < ActiveRecord::Migration
  def up
    change_column_default :netbridge_configurations, :cfg, nil
  end

  def down
    change_column_default :netbridge_configurations, :cfg, 'anms-config.accns.com'
  end
end
