class AddDeviceIdToNetbridgeModems < ActiveRecord::Migration
  def change
    add_column :netbridge_modems, :device_id, :integer
  end
end
