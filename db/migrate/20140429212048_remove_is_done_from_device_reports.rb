class RemoveIsDoneFromDeviceReports < ActiveRecord::Migration

  def up
    remove_column :device_reports, :is_done
  end

  def down
    add_column :device_reports, :is_done, :boolean
  end

end
