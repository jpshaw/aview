class AddCountryIdIndexToRegions < ActiveRecord::Migration
  def change
    add_index :regions, :country_id
  end
end
