class AddCustomerServiceIdToOrganizations < ActiveRecord::Migration

  def change
    add_column :organizations, :customer_service_id, :integer
    add_index  :organizations, :customer_service_id, name: 'idx_org_cust_srvc_id'
  end

end
