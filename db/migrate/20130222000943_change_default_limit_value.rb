class ChangeDefaultLimitValue < ActiveRecord::Migration
  def up
    change_column :users, :pagination_limit, :integer, :default => 100
  end

  def down
    change_column :users, :pagination_limit, :integer, :default => 1000
  end
end
