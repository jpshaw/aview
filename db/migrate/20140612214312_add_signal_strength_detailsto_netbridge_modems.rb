class AddSignalStrengthDetailstoNetbridgeModems < ActiveRecord::Migration
  def change
    add_column :netbridge_modems, :rsrp, :string, limit: 10
    add_column :netbridge_modems, :rsrq, :string, limit: 10
    add_column :netbridge_modems, :rssi, :string, limit: 10
    add_column :netbridge_modems, :sinr, :string, limit: 10
    add_column :netbridge_modems, :snr, :string, limit: 10
    add_column :netbridge_modems, :usb_speed, :string, limit: 10
  end
end
