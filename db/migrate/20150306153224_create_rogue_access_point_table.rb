class CreateRogueAccessPointTable < ActiveRecord::Migration
  def change
    create_table :rogue_access_points do |t|
      t.integer :device_id
      t.string  :ip
      t.string  :mac
      t.string  :ssid
    end

    add_index :rogue_access_points, :device_id, name: 'idx_rogue_ap_dev_id'
  end
end
