class AddTokenToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :token, :string, :length => 32, :null => false
  end
end
