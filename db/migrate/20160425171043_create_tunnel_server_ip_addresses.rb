class CreateTunnelServerIpAddresses < ActiveRecord::Migration
  def change
    create_table :tunnel_server_ip_addresses do |t|
      t.string :ip_address, null: false
      t.references :tunnel_server, index: true

      t.timestamps null: false
    end
  end
end
