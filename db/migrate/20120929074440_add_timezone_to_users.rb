class AddTimezoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :timezone, :string, :length => 6, :default => "-04:00"
  end
end
