class AddEventFieldsToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :device_id, :integer
    add_column :notifications, :uuid_id, :integer
    add_column :notifications, :severity, :integer
    add_column :notifications, :event_type, :integer
    add_column :notifications, :message, :text
    add_column :notifications, :data, :text

    add_index :notifications, :device_id
    add_index :notifications, :uuid_id
    add_index :notifications, :severity
    add_index :notifications, :event_type
  end
end
