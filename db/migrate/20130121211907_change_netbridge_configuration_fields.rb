class ChangeNetbridgeConfigurationFields < ActiveRecord::Migration

  def self.up
    remove_column :netbridge_configurations, :file_type
    remove_column :netbridge_configurations, :mac
  end

  def self.down
  end

end
