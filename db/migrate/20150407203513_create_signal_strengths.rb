class CreateSignalStrengths < ActiveRecord::Migration

  def change
    create_table :signal_strengths, force: true do |t|
      t.decimal :threshold_percentage, precision: 5, scale: 2
    end
  end

end
