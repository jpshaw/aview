class AddOrganizationsContactsTable < ActiveRecord::Migration
  def change
    create_table :organizations_contacts, :id => false do |t|
      t.integer :organization_id
      t.integer :contact_id
    end
    add_index :organizations_contacts, :organization_id, :name => 'idx_org_con_org_id'
    add_index :organizations_contacts, :contact_id, :name => 'idx_org_con_con_id'
  end
end
