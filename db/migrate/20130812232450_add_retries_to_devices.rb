class AddRetriesToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :retry_count, :integer, :default => 0
    add_column :devices, :retried_at, :datetime
  end
end
