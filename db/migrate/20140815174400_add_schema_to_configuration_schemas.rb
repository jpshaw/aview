class AddSchemaToConfigurationSchemas < ActiveRecord::Migration

  def change
    add_column :configuration_schemas, :schema_file_name,     :string
    add_column :configuration_schemas, :schema_file_size,     :integer
    add_column :configuration_schemas, :schema_content_type,  :string
    add_column :configuration_schemas, :schema_updated_at,    :datetime
  end

end
