class AddCodeColumns < ActiveRecord::Migration
  def change
    add_column :device_categories, :code, :integer
    add_column :device_models, :code, :integer
  end
end
