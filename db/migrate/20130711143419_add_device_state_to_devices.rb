class AddDeviceStateToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :device_state, :integer, :default => 2
  end
end
