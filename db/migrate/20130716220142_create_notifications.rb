class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer   :event_id
      t.integer   :notification_subscription_id
      t.datetime  :delivered_at
    end
  end
end
