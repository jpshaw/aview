class AddFirmwareRollbackToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :firmware_rollback, :string
  end
end
