class RemoveOldNotificationProfileFields < ActiveRecord::Migration
  def up
    [:gw_up_down, :publisher_type, :devices].each do |column|
      if ActiveRecord::Base.connection.column_exists?(:notification_profiles, column)
        remove_column :notification_profiles, column
      end
    end
  end

  def down
    add_column :notification_profiles, :gw_up_down, :integer
    add_column :notification_profiles, :publisher_type, :integer
    add_column :notification_profiles, :devices, :text
  end
end
