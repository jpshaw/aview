class AddTunnelStatusColumnsToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :tunnels_total_count, :integer
    add_column :gateway_details, :tunnels_total_up, :integer
    add_column :gateway_details, :tunnels_status, :integer
  end
end
