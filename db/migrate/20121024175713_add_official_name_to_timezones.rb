class AddOfficialNameToTimezones < ActiveRecord::Migration
  def change
    add_column :timezones, :official_name, :string, :limit => 100
  end
end
