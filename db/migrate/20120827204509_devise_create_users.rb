class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :email,              :null => false, :default => "", :limit => 100
      t.string :encrypted_password, :null => false, :default => "", :limit => 150

      ## Recoverable
      t.string   :reset_password_token, :limit => 150
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip, :limit => 20
      t.string   :last_sign_in_ip, :limit => 20

      ## Alerts
      t.string :emailable, :limit => 20

      ## Organizations
      t.integer :organization_id

      ## Roles
      t.integer :role_id

      ## Filters
      t.string :filter, :limit => 20

      ## Table View Custimizations
      t.string :netbridge_table_preference
      t.string :netgate_table_preference
      t.string :netreach_table_preference

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      ## Token authenticatable
      # t.string :authentication_token


      t.timestamps
    end

    add_index :users, :email,                :unique => true, :name => 'idx_usr_email'
    add_index :users, :reset_password_token, :unique => true, :name => 'idx_usr_rst_pwd_tkn'
    # add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true
    # add_index :users, :authentication_token, :unique => true
  end
end
