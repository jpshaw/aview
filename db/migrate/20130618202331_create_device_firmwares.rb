class CreateDeviceFirmwares < ActiveRecord::Migration
  def change
    create_table :device_firmwares do |t|
      t.string :version, :limit => 20
      t.integer :img_type
      t.timestamps
    end
    add_index :device_firmwares, [:version, :img_type], :unique => true, :name => 'idx_dvc_frmw_v_img_typ'
  end
end
