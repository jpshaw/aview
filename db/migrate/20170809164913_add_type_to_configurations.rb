class AddTypeToConfigurations < ActiveRecord::Migration
  def change
    add_column :configurations, :type, :string
    add_index :configurations, :type
  end
end
