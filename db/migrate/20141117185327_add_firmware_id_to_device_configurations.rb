class AddFirmwareIdToDeviceConfigurations < ActiveRecord::Migration

  def change
    add_column  :device_configurations, :firmware_id, :integer
    add_index   :device_configurations, :firmware_id, name: 'idx_dev_conf_firm_id'
  end

end
