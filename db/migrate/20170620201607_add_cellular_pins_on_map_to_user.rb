class AddCellularPinsOnMapToUser < ActiveRecord::Migration
  def change
    add_column :users, :cellular_pins_on_map, :boolean, :default => true
  end
end
