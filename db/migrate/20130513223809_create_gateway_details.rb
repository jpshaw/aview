class CreateGatewayDetails < ActiveRecord::Migration
  def change
    create_table :gateway_details do |t|
      t.integer :device_id
      t.string :active_wan_iface, :length => 20
      t.string :active_wan_ip, :length => 50
      t.integer :active_conn_type
      t.integer :wan_1_conn_type
      t.integer :wan_1_circuit_type
      t.boolean :wan_1_cell_enabled, :default => false
      t.boolean :wan_2_cell_enabled, :default => false
      t.string :dial_mode, :length => 1
      t.boolean :dial_backup_enabled, :default => false
      t.string :dial_backup_type, :length => 1
      t.string :dial_modem_type, :length => 1
      t.datetime :last_trap_at

      t.timestamps
    end
  end
end
