class CreateGatewayTunnels < ActiveRecord::Migration
  def change
    create_table :gateway_tunnels do |t|
      t.integer :device_id
      t.integer :endpoint_type
      t.integer :mode
      t.integer :duration
      t.integer :initiator
      t.integer :connection_type
      t.integer :auth_type
      t.integer :auth_protocol
      t.integer :wan_conn_method

      t.string :ipsec_iface, :limit => 10
      t.string :endpoint, :limit => 15
      t.string :endpoint_ipv6, :limit => 50
      t.string :connection_id, :limit => 50
      t.string :account, :limit => 20
      t.string :user, :limit => 20
      t.string :reason, :limit => 50

      t.boolean :agns_managed, :default => false
      t.boolean :is_up, :default => false

      t.datetime :started_at
      t.timestamps
    end

    add_index :gateway_tunnels, :connection_id, :unique => true, :name => 'idx_gtw_tnl_conn_id'
  end
end
