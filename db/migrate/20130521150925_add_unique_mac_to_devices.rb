class AddUniqueMacToDevices < ActiveRecord::Migration
  def change
    add_index :devices, :mac, :unique => true, :name => 'idx_dvc_mac'
  end
end
