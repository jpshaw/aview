class ChangeStateLengthForLocations < ActiveRecord::Migration
  def up
    rename_column :locations, :state, :state_abbr
    add_column :locations, :state, :string, :length => 100
  end

  def down
    remove_column :locations, :state
    rename_column :locations, :state_abbr, :state
  end
end
