class AddOtaUpdateOptionToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :ota_updates, :integer
  end
end
