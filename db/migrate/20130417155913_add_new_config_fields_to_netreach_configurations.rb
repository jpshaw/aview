class AddNewConfigFieldsToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :config_int_secs, :integer, :default => 1440
    add_column :netreach_configurations, :wds_pass_phrase, :string, :limit => 50
    add_column :netreach_configurations, :wds_encryption, :string, :limit => 10, :default => "NONE"
    add_column :netreach_configurations, :wds_phy_mode, :string, :limit => 10, :default => "HTMIX"
  end
end
