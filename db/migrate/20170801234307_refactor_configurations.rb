class RefactorConfigurations < ActiveRecord::Migration
  def up
    remove_foreign_key :configurations, :organizations
    remove_reference :configurations, :organization, index: true
    remove_column :configurations, :config_type
  end

  def down
    add_column :configurations, :config_type, :integer
    add_reference :configurations, :organization, index: true
    add_foreign_key :configurations, :organizations
  end
end
