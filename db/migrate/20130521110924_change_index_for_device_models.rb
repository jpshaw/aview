class ChangeIndexForDeviceModels < ActiveRecord::Migration
  def up
    remove_index :device_models, :name => 'idx_dvc_mdl_cls_nm'
    add_index :device_models, ["class_name", "model_name"], :unique => true, :name => 'idx_dvc_mdl_cls_mdl'
  end

  def down
    add_index :device_models, :class_name, :unique => true, :name => 'idx_dvc_mdl_cls_nm'
    remove_index :device_models, :name => 'idx_dvc_mdl_cls_mdl'
  end
end
