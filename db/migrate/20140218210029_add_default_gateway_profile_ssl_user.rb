require 'gateway_portal_profile'

class AddDefaultGatewayProfileSslUser < ActiveRecord::Migration
  def up
    rename_column :gateway_portal_profiles, :auth_user, :old_auth_user

    # Max length for Auth User ID is 8 characters.
    add_column :gateway_portal_profiles, :auth_user, :string, :limit => 8, :default => "support"

    GatewayPortalProfile.reset_column_information

    GatewayPortalProfile.find_each do |p|
      next if p.old_auth_user.blank?
      # Using update attributes because we want the validation to run.
      p.update_attributes :auth_user => p.old_auth_user
    end

    remove_column :gateway_portal_profiles, :old_auth_user
  end

  def down
    if column_exists?(:gateway_portal_profiles, :old_auth_user)
      remove_column :gateway_portal_profiles, :auth_user
      rename_column :gateway_portal_profiles, :old_auth_user, :auth_user
    end
  end
end
