class AddNotificationIdToDeviceEvents < ActiveRecord::Migration
  def change
    add_column :device_events, :notification_id, :integer
  end
end
