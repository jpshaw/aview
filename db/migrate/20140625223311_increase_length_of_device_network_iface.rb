class IncreaseLengthOfDeviceNetworkIface < ActiveRecord::Migration
  def up
    change_column :device_networks, :network_iface, :string, limit: 20
  end

  def down
    change_column :device_networks, :network_iface, :string, limit: 10
  end
end
