class AddIndexScopeToAccounts < ActiveRecord::Migration
  def up
    # We can't simply run `remove_index :accounts, :name` here, since
    # the index name differs between databases.
    if index_exists?(:accounts, :name, :name => "idx_acnt_nm")
      remove_index :accounts, :name => "idx_acnt_nm"
    elsif index_exists?(:accounts, :name, :name => "index_accounts_on_name")
      remove_index :accounts, :name => "index_accounts_on_name"
    end
    add_index :accounts, [:name, :organization_id], :unique => true, :name => 'idx_acnt_nm'
  end

  def down
    if index_exists?(:accounts, :name, :name => "idx_acnt_nm")
      remove_index :accounts, :name => "idx_acnt_nm"
    elsif index_exists?(:accounts, :name, :name => "index_accounts_on_name")
      remove_index :accounts, :name => "index_accounts_on_name"
    end
    add_index :accounts, :name, :unique => true, :name => 'idx_acnt_nm'
  end
end
