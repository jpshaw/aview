class AddProbedAtToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :probed_at, :datetime
  end
end
