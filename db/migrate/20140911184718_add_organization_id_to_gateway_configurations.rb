class AddOrganizationIdToGatewayConfigurations < ActiveRecord::Migration

  def change
    add_column  :gateway_configurations, :organization_id, :integer
    add_index   :gateway_configurations, :organization_id, name: 'idx_gat_conf_org_id'
  end

end
