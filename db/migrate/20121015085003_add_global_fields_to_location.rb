class AddGlobalFieldsToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :postal_code, :string, :limit => 20
    add_column :locations, :country_id, :integer
  end
end
