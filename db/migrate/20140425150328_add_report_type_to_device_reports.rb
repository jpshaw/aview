class AddReportTypeToDeviceReports < ActiveRecord::Migration

  def change
    add_column :device_reports, :report_type, :string
  end

end
