class AddSmsToNetbridgeModems < ActiveRecord::Migration
  def change
    add_column :netbridge_modems, :sms_sent_at, :timestamp
  end
end
