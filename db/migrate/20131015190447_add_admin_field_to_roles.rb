class AddAdminFieldToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :admin, :boolean, :default => false
  end
end
