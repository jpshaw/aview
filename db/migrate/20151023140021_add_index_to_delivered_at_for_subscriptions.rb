class AddIndexToDeliveredAtForSubscriptions < ActiveRecord::Migration
  def change
    add_index :notifications, :delivered_at, name: 'idx_not_del_at'
  end
end
