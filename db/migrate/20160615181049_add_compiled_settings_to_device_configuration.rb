class AddCompiledSettingsToDeviceConfiguration < ActiveRecord::Migration
  def change
    add_column :device_configurations, :compiled_settings, :text
    add_column :device_configurations, :parent_disconnect, :boolean, default: false
  end
end
