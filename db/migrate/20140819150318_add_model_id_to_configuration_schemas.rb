class AddModelIdToConfigurationSchemas < ActiveRecord::Migration

  def change
    add_column :configuration_schemas, :model_id, :integer
    add_index  :configuration_schemas, :model_id, name: 'idx_conf_sch_model_id'
  end

end
