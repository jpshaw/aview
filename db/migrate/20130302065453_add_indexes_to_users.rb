class AddIndexesToUsers < ActiveRecord::Migration
  def change
    add_index :users, :organization_id, :name => 'idx_usr_org_id'
    add_index :users, :role_id, :name => 'idx_usr_rl_id'
    add_index :users, :timezone_id, :name => 'idx_usr_tmzn_id'
  end
end
