class AddSecurityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :password_changed_at, :datetime
    add_column :users, :unique_session_id, :string, :limit => 20
    add_column :users, :last_activity_at, :datetime
    add_column :users, :expired_at, :datetime
    add_index :users, :password_changed_at, :name => 'idx_usr_pwd_chg_at'
    add_index :users, :last_activity_at, :name => 'idx_usr_lst_acty_at'
    add_index :users, :expired_at, :name => 'idx_usr_expd_at'
  end
end
