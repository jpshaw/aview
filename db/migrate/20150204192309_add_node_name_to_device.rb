class AddNodeNameToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :node_name, :string
  end
end
