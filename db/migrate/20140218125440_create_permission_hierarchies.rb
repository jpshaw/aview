class CreatePermissionHierarchies < ActiveRecord::Migration

  def change
    create_table :permission_hierarchies, id: false do |t|
      t.integer  :ancestor_id, :null => false   # ID of the parent/grandparent/great-grandparent/... permission
      t.integer  :descendant_id, :null => false # ID of the target permission
      t.integer  :generations, :null => false   # Number of generations between the ancestor and the descendant. Parent/child = 1, for example.
    end

    # For "all progeny of permission" and leaf selects:
    add_index :permission_hierarchies, [:ancestor_id, :descendant_id, :generations], :unique => true, :name => "permission_anc_desc_udx"

    # For "all ancestors of permission" selects,
    add_index :permission_hierarchies, [:descendant_id], :name => "permission_desc_idx"
  end

end
