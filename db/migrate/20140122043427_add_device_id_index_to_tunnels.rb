class AddDeviceIdIndexToTunnels < ActiveRecord::Migration
  def change
    add_index :gateway_tunnels, :device_id, :name => 'idx_gtw_tnl_dvc_id'
  end
end
