class AddImportNameToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :import_name, :string
  end
end
