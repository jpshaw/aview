class AddOffToTimezones < ActiveRecord::Migration
  def change
    add_column :timezones, :off, :string, :limit => 6
  end
end
