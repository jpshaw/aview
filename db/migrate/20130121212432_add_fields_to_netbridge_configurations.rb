class AddFieldsToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :cfg, :string
    add_column :netbridge_configurations, :config_change_reboot, :boolean, :default => false
    add_column :netbridge_configurations, :firmware_server, :string
    add_column :netbridge_configurations, :iui, :boolean, :default => false
    add_column :netbridge_configurations, :png, :string
    add_column :netbridge_configurations, :rbt, :boolean, :default => true
    add_column :netbridge_configurations, :remote_control, :boolean, :default => false
    add_column :netbridge_configurations, :sms_check_freq, :string, :default => "1m"
    add_column :netbridge_configurations, :upload_server, :string
    add_column :netbridge_configurations, :var, :string, :default => "A"
    add_column :netbridge_configurations, :wdog, :boolean, :default => true

    add_column :netbridge_configurations, :sticky_apn, :boolean, :default => true
    add_column :netbridge_configurations, :xma, :string
    add_column :netbridge_configurations, :xmc, :string
    add_column :netbridge_configurations, :xmi, :string
    add_column :netbridge_configurations, :xmp, :string
    add_column :netbridge_configurations, :xmt, :string

    add_column :netbridge_configurations, :link_check_freq, :string, :default => "4m"
    add_column :netbridge_configurations, :link_timeout, :integer, :default => 900
    add_column :netbridge_configurations, :traf_acct, :boolean, :default => false

    ### DHCP Options ###
    add_column :netbridge_configurations, :dhcp_lease_max, :integer
    add_column :netbridge_configurations, :dlm, :string
    add_column :netbridge_configurations, :dre, :string
    add_column :netbridge_configurations, :drn, :string
    add_column :netbridge_configurations, :drp, :string
    add_column :netbridge_configurations, :drs, :string
    add_column :netbridge_configurations, :eth, :string
    add_column :netbridge_configurations, :gip, :string
    add_column :netbridge_configurations, :gip_secondary, :string
    add_column :netbridge_configurations, :mtu, :string

    ### DNS Options ###
    add_column :netbridge_configurations, :ddns, :string
    add_column :netbridge_configurations, :dnslist, :string

    ### Modem Management ###
    add_column :netbridge_configurations, :rat, :string

    ### Port Forwarding ###
    add_column :netbridge_configurations, :ptf, :string
    add_column :netbridge_configurations, :ptfr, :string

    ### Static IP ###
    add_column :netbridge_configurations, :ips_cust_dh_grp, :string
    add_column :netbridge_configurations, :ips_cust_home_ep, :string
    add_column :netbridge_configurations, :ips_cust_home_net, :string
    add_column :netbridge_configurations, :ips_cust_keyid_tag, :string
    add_column :netbridge_configurations, :ips_cust_psk, :string
    add_column :netbridge_configurations, :ips_cust_remote_ep, :string
    add_column :netbridge_configurations, :ips_cust_remote_net, :string
    add_column :netbridge_configurations, :ips_cust_xauth_pass, :string
    add_column :netbridge_configurations, :ips_cust_xauth_user, :string
    add_column :netbridge_configurations, :ips_keepalive, :string
    add_column :netbridge_configurations, :tus, :string
    add_column :netbridge_configurations, :vpn_cust_type, :string

    ### Syslog Options ###
    add_column :netbridge_configurations, :log, :string
    add_column :netbridge_configurations, :log_level, :string
    add_column :netbridge_configurations, :log_list, :string
    add_column :netbridge_configurations, :syslog_http, :string

  end
end
