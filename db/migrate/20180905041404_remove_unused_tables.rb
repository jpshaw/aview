class RemoveUnusedTables < ActiveRecord::Migration
  def change
    drop_table :accounts
    drop_table :account_sites
    drop_table :users_accounts
  end
end
