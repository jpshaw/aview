class AddCommentFieldToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :comment, :string
  end
end
