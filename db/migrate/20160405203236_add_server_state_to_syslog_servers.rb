class AddServerStateToSyslogServers < ActiveRecord::Migration
  def change
    add_column :syslog_servers, :server_state, :string
  end
end
