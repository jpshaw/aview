class AddFirmwareToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :firmware, :string, :limit => 15
  end
end
