class AddSiteIdIndexToDevices < ActiveRecord::Migration
  def change
    add_index :devices, :site_id, :name => 'idx_dvc_st_id'
  end
end
