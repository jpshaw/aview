class AddNotifyEnabledToUsers < ActiveRecord::Migration
  def change
    change_column_default :users, :notify_enabled, true
  end
end
