class CreateImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.string :the_trigger,     :limit => 20
      t.string :the_state,       :limit => 20
      t.string :the_source_name, :limit => 20
      t.string :the_type,        :limit => 20
      t.string :the_action,      :limit => 20

      t.string    :source_file_name
      t.integer   :source_file_size
      t.string    :source_content_type
      t.datetime  :source_updated_at
      t.string    :source_fingerprint, :limit => 32

      t.string :log_path

      # Limiting the text size here to 15 MB, which is the maximum size of
      # a yaml file with a hash that has 20 keys (each with 20 chars), with each
      # key having an array of 50,000 integer ids 10 digits long. This also
      # includes a skipped_device array of 50,000, with each entry having
      # 12 characters.
      t.text :object_ids, :limit => 15.megabytes

      t.datetime :finished_at
      t.timestamps
    end
  end
end
