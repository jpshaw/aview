class CreateTunnelServers < ActiveRecord::Migration
  def change
    create_table :tunnel_servers do |t|
      t.string :name
      t.string :location

      t.timestamps null: false
    end
  end
end
