class AddOrganizationIdToLocationTokenUniqueness < ActiveRecord::Migration
  def up
    # We can't simply run `remove_index :locations, :input_token` here, since
    # the token name differs between databases.
    if index_exists?(:locations, :input_token, :name => "idx_lctn_npt_tkn")
      remove_index :locations, :name => "idx_lctn_npt_tkn"
    elsif index_exists?(:locations, :input_token, :name => "index_locations_on_input_token")
      remove_index :locations, :name => "index_locations_on_input_token"
    end
    add_index :locations, [:input_token, :organization_id], :unique => true, :name => 'idx_lctn_npt_tkn'
  end

  def down
    if index_exists?(:locations, :input_token, :name => "idx_lctn_npt_tkn")
      remove_index :locations, :name => "idx_lctn_npt_tkn"
    elsif index_exists?(:locations, :input_token, :name => "index_locations_on_input_token")
      remove_index :locations, :name => "index_locations_on_input_token"
    end
    add_index :locations, :input_token, :unique => true, :name => 'idx_lctn_npt_tkn'
  end
end
