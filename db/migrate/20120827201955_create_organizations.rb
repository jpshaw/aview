class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name, :limit => 50
      t.boolean :has_netgates,   :default => false
      t.boolean :has_netreaches, :default => false
      t.boolean :has_netbridges, :default => false
      t.boolean :has_opticells,  :default => false
      t.timestamps
    end
  end
end
