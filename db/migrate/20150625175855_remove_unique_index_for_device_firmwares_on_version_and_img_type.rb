class RemoveUniqueIndexForDeviceFirmwaresOnVersionAndImgType < ActiveRecord::Migration

  def up
    if index_exists?(:device_firmwares, [:version, :img_type], name: 'idx_dvc_frmw_v_img_typ')
      remove_index :device_firmwares, name: 'idx_dvc_frmw_v_img_typ'
    elsif index_exists?(:device_firmwares, [:version, :img_type], name: 'index_device_firmwares_on_version_and_img_type')
      remove_index :device_firmwares, name: 'index_device_firmwares_on_version_and_img_type'
    end
  end

  def down
    add_index :device_firmwares, [:version, :img_type], unique: true, name: 'idx_dvc_frmw_v_img_typ'
  end

end
