class AddIndexesToNotificationTables < ActiveRecord::Migration
  def change
    add_index :device_event_uuids,         :category_id,    name: 'idx_dev_ev_uuid_cat_id'
    add_index :notification_profiles,      :user_id,        name: 'idx_not_prof_user_id'
    add_index :notification_subscriptions, :user_id,        name: 'idx_not_sub_user_id'
    add_index :notification_subscriptions, :publisher_id,   name: 'idx_not_sub_pub_id'
    add_index :notification_subscriptions, :publisher_type, name: 'idx_not_sub_pub_type'
    add_index :notification_subscriptions, :profile_id,     name: 'idx_not_sub_prof_id'
    add_index :notifications,              :user_id,        name: 'idx_not_user_id'
    add_index :subscribed_events,          :profile_id,     name: 'idx_sub_ev_prof_id'
    add_index :subscribed_events,          :frequency,      name: 'idx_sub_ev_freq'
  end
end
