class AddNameToNotificationProfiles < ActiveRecord::Migration
  def change
    add_column :notification_profiles, :name, :string, :limit => 30
  end
end
