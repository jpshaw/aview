class UpdateMakerLengthOfDeviceModems < ActiveRecord::Migration
  def up
    change_column :device_modems, :maker, :string, limit: 50
  end

  def down
    change_column :device_modems, :maker, :string, limit: 20
  end
end
