class AddFlagsToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :flags, :text
  end
end
