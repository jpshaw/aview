class CreateAccounts < ActiveRecord::Migration
  def change

    opts = ''
    if ActiveRecord::Base.configurations[Rails.env]['adapter'] == 'mysql2'
      opts = 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin'
    end

    create_table :accounts, :options => opts do |t|
      t.string :name, :limit => 50
      t.integer :organization_id
      t.timestamps
    end
  end
end
