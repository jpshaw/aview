require 'notification_subscription'

class TypeChangeForPublisherTypeInNotificationSubscriptions < ActiveRecord::Migration
  def up
    rename_column :notification_subscriptions, :publisher_type, :publisher_type_old
    add_column :notification_subscriptions, :publisher_type, :string, :limit => 100

    NotificationSubscription.all.each do |subscription|
      case subscription.publisher_type_old
      when 0
        subscription.update_attribute(:publisher_type, "Device")
      when 1
        subscription.update_attribute(:publisher_type, "Organization")
      end

      # Remove subscription if it's publisher dosen't exist anymore
      subscription.destroy if subscription.publisher.nil?
    end

    remove_column :notification_subscriptions, :publisher_type_old
  end

  def down
    rename_column :notification_subscriptions, :publisher_type, :publisher_type_old
    add_column :notification_subscriptions, :publisher_type, :integer

    NotificationSubscription.all.each do |subscription|
      case subscription.publisher_type_old
      when "Device"
        subscription.update_attribute(:publisher_type, 0)
      when "Organization"
        subscription.update_attribute(:publisher_type, 1)
      end
    end

    remove_column :notification_subscriptions, :publisher_type_old
  end
end
