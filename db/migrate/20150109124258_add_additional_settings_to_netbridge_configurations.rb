class AddAdditionalSettingsToNetbridgeConfigurations < ActiveRecord::Migration

  def change
    add_column :netbridge_configurations, :additional_settings, :text
  end

end
