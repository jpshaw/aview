class AddKeyMetaColumnToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    # If each key has 10 data points, each with 10 characters, with a maximum
    # of 50 keys, then the space required comes to about 15 kilobytes.
    add_column :netbridge_configurations, :key_meta, :binary, limit: 15.kilobytes
  end
end
