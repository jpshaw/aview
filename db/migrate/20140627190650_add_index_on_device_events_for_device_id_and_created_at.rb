class AddIndexOnDeviceEventsForDeviceIdAndCreatedAt < ActiveRecord::Migration

  def change
    add_index :device_events, [:device_id, :created_at], name: 'idx_dev_evnts_dev_id_crt_at'
  end

end
