class Add236191NetbridgeConfigurationOptions < ActiveRecord::Migration
  def change 
    add_column :netbridge_configurations, :conf_insecure_ssl, :integer
    add_column :netbridge_configurations, :crt_url, :string
    add_column :netbridge_configurations, :dhcp_enable, :integer
    add_column :netbridge_configurations, :status_freq, :string
  end
end
