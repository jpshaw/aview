class AddPolymorphicResourceToConfigurations < ActiveRecord::Migration
  def change
    add_reference :configurations, :resource, polymorphic: true, index: true
  end
end
