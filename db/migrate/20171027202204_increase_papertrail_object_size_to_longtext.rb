class IncreasePapertrailObjectSizeToLongtext < ActiveRecord::Migration

  # https://github.com/airblade/paper_trail/commit/4ba85e2548af43aa5ea09311cb56932a9153d99f
  # The largest text column available in all supported RDBMS is
  # 1024^3 - 1 bytes, roughly one gibibyte.  We specify a size
  # so that MySQL will use `longtext` instead of `text`.  Otherwise,
  # when serializing very large objects, `text` might not be big enough.
  TEXT_BYTES = 1_073_741_823
  ORIG_TEXT_BYTES = 65535

  def up
     change_column :versions, :object, :text, limit: TEXT_BYTES
  end

  def down
     change_column :versions, :object, :text, limit: ORIG_TEXT_BYTES
  end
end
