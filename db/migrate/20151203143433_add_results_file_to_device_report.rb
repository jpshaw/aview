class AddResultsFileToDeviceReport < ActiveRecord::Migration
  def change
    add_column :device_reports, :results_file, :string
  end
end
