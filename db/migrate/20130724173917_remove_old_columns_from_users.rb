class RemoveOldColumnsFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :notify_nr_up
    remove_column :users, :notify_nr_down
    remove_column :users, :notify_nr_config
    remove_column :users, :notify_nr_fw
  end

  def down
    add_column :users, :notify_nr_up, :boolean, :default => false
    add_column :users, :notify_nr_down, :boolean, :default => false
    add_column :users, :notify_nr_config, :boolean, :default => false
    add_column :users, :notify_nr_fw, :boolean, :default => false
  end
end
