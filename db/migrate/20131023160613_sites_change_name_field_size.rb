class SitesChangeNameFieldSize < ActiveRecord::Migration
  def up
    change_column :sites, :name, :string, :limit => 80
  end
  def down
    Site.all.each do | site |
      site.name = site.name.truncate 20
      site.save
    end
    change_column :sites, :name, :string, :limit => 20
  end
end
