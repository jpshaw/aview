class ChangeColumnNameForNotificationSubscriptions < ActiveRecord::Migration
  def change
    change_table :notification_subscriptions do |t|
      t.rename :object_id, :subscribee_id
      t.rename :object_type, :subscribee_type
    end
  end
end
