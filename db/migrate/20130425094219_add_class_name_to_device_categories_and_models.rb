class AddClassNameToDeviceCategoriesAndModels < ActiveRecord::Migration
  def change
    add_column :device_categories, :class_name, :string, :length => 50
    add_column :device_models, :class_name, :string, :length => 50
    add_index :device_categories, :class_name, :unique => true, :name => 'idx_dvc_cat_cls_nm'
    add_index :device_models, :class_name, :unique => true, :name => 'idx_dvc_mdl_cls_nm'
  end
end
