class AddIndexToAccountName < ActiveRecord::Migration
  def change
    add_index :accounts, :name, :unique => true, :name => 'idx_acnt_nm'
  end
end
