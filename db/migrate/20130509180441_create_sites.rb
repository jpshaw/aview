class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name, :limit => 20
      t.integer :organization_id

      t.timestamps
    end
    add_index :sites, [:name, :organization_id], :unique => true, :name => 'idx_sts_nm_org_id'
  end
end
