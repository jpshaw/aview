class DevicesSiteIdToInteger < ActiveRecord::Migration
  def up
    change_table :devices do |t|
      t.change :site_id, :integer
    end
  end

  def down
    change_table :devices do |t|
      t.change :site_id, :string, :limit => 15
    end
  end
end
