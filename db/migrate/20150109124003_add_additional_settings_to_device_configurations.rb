class AddAdditionalSettingsToDeviceConfigurations < ActiveRecord::Migration

  def change
    add_column :device_configurations, :additional_settings, :text
  end

end
