class AddDualWanEnabledToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :dual_wan_enabled, :boolean, default: false
    add_index :devices, :dual_wan_enabled
  end
end
