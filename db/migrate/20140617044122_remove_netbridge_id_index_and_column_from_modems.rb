class RemoveNetbridgeIdIndexAndColumnFromModems < ActiveRecord::Migration
  def up
    if index_exists?(:netbridge_modems, :netbridge_id)
      remove_index :netbridge_modems, :netbridge_id
    elsif index_exists?(:netbridge_modems, :netbridge_id, name: 'idx_nbg_mdm_nbg_id')
      remove_index :netbridge_modems, name: 'idx_nbg_mdm_nbg_id'
    end
    remove_column :netbridge_modems, :netbridge_id
  end

  def down
    add_column :netbridge_modems, :netbridge_id, :integer
    add_index :netbridge_modems, :netbridge_id, name: 'idx_nbg_mdm_nbg_id'
  end
end
