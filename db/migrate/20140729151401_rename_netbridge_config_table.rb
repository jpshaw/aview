class RenameNetbridgeConfigTable < ActiveRecord::Migration
  def change
    unless ActiveRecord::Base.connection.table_exists?('netbridge_config_hierarchies')
      rename_table :netbridge_configuration_hierarchies, :netbridge_config_hierarchies
    end
  end
end
