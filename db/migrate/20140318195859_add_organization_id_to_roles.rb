class AddOrganizationIdToRoles < ActiveRecord::Migration

  def change
    add_column  :roles, :organization_id, :integer
    add_index   :roles, :organization_id, :name => "idx_rol_org_id"
  end

end
