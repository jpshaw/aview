class RemoveEventColumns < ActiveRecord::Migration
  def up
    [:category_id, :model_id, :organization_id, :device_type, :name, :notification_id].each do |column|
      if ActiveRecord::Base.connection.index_exists?(:device_events, column)
        remove_index :device_events, column
      end

      if ActiveRecord::Base.connection.column_exists?(:device_events, column)
        remove_column :device_events, column
      end
    end
  end

  def down
    add_column :device_events, :category_id, :integer
    add_index  :device_events, :category_id, :name => 'idx_dvc_evnt_cat_id'
    add_column :device_events, :model_id, :integer
    add_index  :device_events, :model_id, :name => 'idx_dvc_evnt_mdl_id'
    add_column :device_events, :organization_id, :integer
    add_index  :device_events, :organization_id, :name => 'idx_dvc_evnt_org_id'
    add_column :device_events, :device_type, :integer
    add_index  :device_events, :device_type, :name => 'idx_dvc_evnt_dvc_typ'
    add_column :device_events, :name, :string
    add_column :device_events, :notification_id, :integer
  end
end
