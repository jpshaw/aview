class CreateNetbridgeConfigurations < ActiveRecord::Migration
  def change
    create_table :netbridge_configurations do |t|
      t.string :name, :limit => 50
      t.string :file_type
      t.string :mac, :limit => 12
      t.text :data
      t.timestamps
    end
    add_index :netbridge_configurations, :mac, :unique => true, :name => 'idx_nbg_cnf_mac'
  end
end
