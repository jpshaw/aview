class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.integer :country_id
      t.string :iso_code
      t.string :name
    end
  end
end
