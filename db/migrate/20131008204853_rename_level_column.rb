class RenameLevelColumn < ActiveRecord::Migration
  def change
    rename_column :device_events, :level, :severity
  end
end
