class AddTypeToDeviceConfigurations < ActiveRecord::Migration
  def change
    add_column :device_configurations, :record_type, :integer, default: 0 #default is group
    add_index :device_configurations, :record_type
  end
end
