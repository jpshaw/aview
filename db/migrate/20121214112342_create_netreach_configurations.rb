class CreateNetreachConfigurations < ActiveRecord::Migration
  def change
    create_table :netreach_configurations do |t|
      t.string :name, :limit => 50
      t.integer :organization_id
      t.boolean :admin_access, :default => false
      t.string :admin_username, :limit => 50
      t.string :admin_password, :limit => 50
      t.boolean :log_enabled, :default => true
      t.string :log_ipv4, :limit => 30
      t.boolean :dst_enabled, :default => true
      t.string :tz_offset, :limit => 10
      t.integer :wds_mode
      t.string :wds_mac_1, :limit => 30
      t.string :wds_mac_2, :limit => 30
      t.string :wds_mac_3, :limit => 30
      t.integer :network_mode
      t.string :country_code, :limit => 2
      t.string :channel, :limit => 4
      t.string :channel_region, :limit => 5
      t.integer :rf_power
      t.string :ssid_1, :limit => 50
      t.boolean :ssid_1_advertise, :default => true
      t.integer :ssid_1_sec
      t.integer :ssid_1_wep_len
      t.string :ssid_1_pass_t, :limit => 50
      t.string :ssid_1_pass, :limit => 50
      t.string :ssid_1_wpa_c, :limit => 50
      t.string :ssid_1_ipv4, :limit => 50
      t.string :ssid_1_port, :limit => 10
      t.integer :ssid_1_timeout
      t.string :ssid_1_secret, :limit => 50
      t.string :ssid_2, :limit => 50
      t.boolean :ssid_2_advertise, :default => true
      t.integer :ssid_2_sec
      t.integer :ssid_2_wep_len
      t.string :ssid_2_pass_t, :limit => 50
      t.string :ssid_2_pass, :limit => 50
      t.string :ssid_2_wpa_c, :limit => 50
      t.string :ssid_2_ipv4, :limit => 50
      t.string :ssid_2_port, :limit => 10
      t.integer :ssid_2_timeout
      t.string :ssid_2_secret, :limit => 50

      t.timestamps
    end
  end
end
