class PowerdnsRecordManager < ActiveRecord::Migration
  def change
   create_table "domains", :force => true do |t|
      t.string  "name",                           :null => false
      t.string  "master",          :limit => 128
      t.integer "last_check"
      t.string  "type",            :limit => 6,   :null => false
      t.integer "notified_serial"
      t.string  "account",         :limit => 40
    end

    add_index "domains", ["name"], :name => "idx_pdns_name", :unique => true

    create_table "records", :force => true do |t|
      t.integer "domain_id"
      t.string  "name"
      t.string  "type",        :limit => 10
      t.text    "content"
      t.integer "ttl"
      t.integer "prio"
      t.integer "change_date"
      t.string  "record_type"
    end

    add_index "records", ["domain_id"], :name => "idx_pdns_domain_id"
    add_index "records", ["name", "type"], :name => "idx_pdns_name_type"

    create_table "supermasters", :id => false, :force => true do |t|
      t.string "ip",         :limit => 64, :null => false
      t.string "nameserver",               :null => false
      t.string "account",    :limit => 40
    end
  end
end
