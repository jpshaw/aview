class RemoveOldLogoColumnFromOrganizations < ActiveRecord::Migration
  def change
    remove_column :organizations, :logo
    remove_column :organizations, :avatar_file_name
    remove_column :organizations, :avatar_content_type
    remove_column :organizations, :avatar_file_size
    remove_column :organizations, :avatar_updated_at
  end
end
