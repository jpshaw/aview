class AddEnableDisableFieldsToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :ssid_1_disabled, :boolean, :default => false
    add_column :netreach_configurations, :ssid_2_disabled, :boolean, :default => false
  end
end
