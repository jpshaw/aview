class CreateDeviceAssociations < ActiveRecord::Migration
  def change
    create_table :device_associations do |t|
      t.integer  :device_id
      t.integer  :associated_device_id
      t.string   :associated_device_mac
      t.text     :meta
      t.timestamps
    end
    add_index :device_associations, :device_id, name: 'idx_dev_asoc'
    add_index :device_associations, :associated_device_id, name: 'idx_dev_asoc_dev_id'
    add_index :device_associations, :associated_device_mac, name: 'idx_dev_asoc_dev_mac'
  end
end
