class AddUniquenessForSlugAndNameToOrganizations < ActiveRecord::Migration
  def change
    add_index :organizations, [:name, :owner_id], :unique => true, :name => 'idx_org_nme_onr_id'
    add_index :organizations, :slug, :unique => true, :name => 'idx_org_slg'
  end
end
