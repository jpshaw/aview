class CreateAccountSites < ActiveRecord::Migration
  def change
    create_table :account_sites do |t|
      t.references :account
      t.references :site

      t.timestamps
    end
    add_index :account_sites, :account_id, name: "idx_acc_sit_acc_id"
    add_index :account_sites, :site_id, name: "idx_acc_sit_sit_id"
    add_index :account_sites, [:account_id, :site_id], unique: true, name: "idx_acc_sit_acc_id_sit_id"
  end
end
