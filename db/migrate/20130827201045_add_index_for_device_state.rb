class AddIndexForDeviceState < ActiveRecord::Migration
  def change
    add_index :device_states, :state, :name => 'idx_dvc_stt_stt'
  end
end
