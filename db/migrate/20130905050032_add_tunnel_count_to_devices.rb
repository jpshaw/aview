require 'devices/gateway'

class AddTunnelCountToDevices < ActiveRecord::Migration
  def up
    add_column :devices, :tunnels_count, :integer, :default => 0

    Gateway.reset_column_information
    Gateway.find_each do |p|
      Gateway.update_counters p.id, :tunnels_count => p.tunnels.length
    end
  end

  def down
    remove_column :devices, :tunnels_count
  end
end
