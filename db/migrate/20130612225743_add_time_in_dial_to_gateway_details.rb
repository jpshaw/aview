class AddTimeInDialToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :time_in_dial, :integer, :default => 0
  end
end
