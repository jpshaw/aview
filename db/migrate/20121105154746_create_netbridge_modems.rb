class CreateNetbridgeModems < ActiveRecord::Migration
  def change
    create_table :netbridge_modems do |t|
      t.integer :netbridge_id
      t.string :name,     :limit => 50
      t.string :carrier,  :limit => 20
      t.integer :signal,  :limit => 1
      t.string :number,   :limit => 20
      t.string :imei,     :limit => 25
      t.string :imsi,     :limit => 25
      t.string :esn,      :limit => 20
      t.string :iccid,    :limit => 25
      t.string :revision, :limit => 50
      t.string :apn,      :limit => 25
      t.string :ecio,     :limit => 6
      t.string :cnti,     :limit => 15
      t.string :cid,      :limit => 20
      t.string :lac,      :limit => 10
      t.timestamps
    end
  end
end
