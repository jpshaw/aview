class AddFirmwareFileToDeviceFirmwares < ActiveRecord::Migration

  def change
    add_column :device_firmwares, :firmware_file, :string
  end

end
