class FixGatewayDetailsWanCircuit < ActiveRecord::Migration
  def change
    change_column :gateway_details, :wan_1_circuit_type, :string
  end
end
