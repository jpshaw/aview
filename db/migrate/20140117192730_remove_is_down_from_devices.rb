class RemoveIsDownFromDevices < ActiveRecord::Migration
  def up
    remove_column :devices, :is_down
  end

  def down
    add_column :devices, :is_down, :boolean, :default => false
  end
end
