class DropTableConfigurationSchemas < ActiveRecord::Migration

  def up
    drop_table :configuration_schemas
  end

  def down
    create_table :configuration_schemas, force: true do |t|
      t.string  "name"
      t.string  "version"
      t.integer "model_id"
      t.string  "schema_file"
    end

    add_index :configuration_schemas, :model_id, name: 'idx_conf_sch_model_id'
  end

end
