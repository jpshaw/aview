require 'netbridge_configuration'

class ChangeIpsCustPskToText < ActiveRecord::Migration
  def up
    rename_column :netbridge_configurations, :ips_cust_psk, :ips_cust_psk_old
    add_column :netbridge_configurations, :ips_cust_psk, :text

    NetbridgeConfiguration.find_each(batch_size: 500) do |config|
      if config.ips_cust_psk_old.present?
        config.update_attributes(ips_cust_psk: config.ips_cust_psk_old)
      end
    end

    remove_column :netbridge_configurations, :ips_cust_psk_old
  end

  def down
    rename_column :netbridge_configurations, :ips_cust_psk, :ips_cust_psk_old
    add_column :netbridge_configurations, :ips_cust_psk, :string

    NetbridgeConfiguration.find_each(batch_size: 500) do |config|
      if config.ips_cust_psk_old.present?
        config.update_attributes(ips_cust_psk: config.ips_cust_psk_old)
      end
    end

    remove_column :netbridge_configurations, :ips_cust_psk_old
  end
end
