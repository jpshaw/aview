class AddSlugToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :slug, :string, :limit => 75
  end
end
