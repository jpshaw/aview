class AddLogoToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :logo, :string, :limit => 30
  end
end
