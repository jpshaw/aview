class RenameDeviceModelColumn < ActiveRecord::Migration
  def change
    rename_column :device_models, :model_name, :model_version
  end
end
