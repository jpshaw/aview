class CreateDeviceReports < ActiveRecord::Migration
  def change
    create_table :device_reports do |t|
      t.text :chart_data
      t.binary :results, :limit => 10.megabyte
      t.boolean :is_done, :default => false
      t.integer :user_id
      t.text :params
      t.datetime :finished_at

      t.timestamps
    end
  end
end
