class AddOrganizationIdToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :organization_id, :integer
  end
end
