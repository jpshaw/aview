class CreateConfigurationSchemas < ActiveRecord::Migration

  def change
    create_table :configuration_schemas, force: true do |t|
      t.string :name
      t.string :version
      t.string :json_location
    end
  end

end
