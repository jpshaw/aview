class AddImportNameToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :import_name, :string
  end
end
