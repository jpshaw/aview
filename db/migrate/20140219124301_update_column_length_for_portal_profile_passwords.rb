class UpdateColumnLengthForPortalProfilePasswords < ActiveRecord::Migration
  def up
    rename_column :gateway_portal_profiles, :auth_password, :old_auth_password

    # Max length for Auth Password is 16 characters.
    add_column :gateway_portal_profiles, :auth_password, :string, :limit => 16

    GatewayPortalProfile.reset_column_information

    GatewayPortalProfile.find_each do |p|
      next if p.old_auth_password.blank?
      # Using update attributes because we want the validation to run.
      p.update_attributes :auth_password => p.old_auth_password
    end

    remove_column :gateway_portal_profiles, :old_auth_password
  end

  def down
    if column_exists?(:gateway_portal_profiles, :old_auth_password)
      remove_column :gateway_portal_profiles, :auth_password
      rename_column :gateway_portal_profiles, :old_auth_password, :auth_user
    end
  end
end
