class AddCellularLocationToDeviceModem < ActiveRecord::Migration
  def change
    add_reference :device_modems, :cellular_location, index: true, foreign_key: true
  end
end
