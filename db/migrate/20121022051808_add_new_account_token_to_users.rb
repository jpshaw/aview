class AddNewAccountTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :new_account_token, :string, :limit => 16
  end
end
