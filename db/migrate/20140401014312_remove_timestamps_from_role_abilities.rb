class RemoveTimestampsFromRoleAbilities < ActiveRecord::Migration
  def change
    if column_exists?(:role_abilities, :created_at) && column_exists?(:role_abilities, :updated_at)
      change_table :role_abilities do |t|
        t.remove_timestamps
      end
    end
  end
end
