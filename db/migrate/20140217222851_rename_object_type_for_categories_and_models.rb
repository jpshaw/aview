require 'device_category'
require 'device_model'

class RenameObjectTypeForCategoriesAndModels < ActiveRecord::Migration
  def up
    DeviceCategory.where(class_name: 'Anms::Devices::Gateway').update_all(class_name: 'Gateway')
    DeviceCategory.where(class_name: 'Anms::Devices::Netbridge').update_all(class_name: 'Netbridge')
    DeviceCategory.where(class_name: 'Anms::Devices::Netreach').update_all(class_name: 'Netreach')

    DeviceModel.where(class_name: 'Anms::Devices::Gateway').update_all(class_name: 'Gateway')
    DeviceModel.where(class_name: 'Anms::Devices::Netbridge').update_all(class_name: 'Netbridge')
    DeviceModel.where(class_name: 'Anms::Devices::Netreach').update_all(class_name: 'Netreach')
  end

  def down
    DeviceCategory.where(class_name: 'Gateway').update_all(class_name: 'Anms::Devices::Gateway')
    DeviceCategory.where(class_name: 'Netbridge').update_all(class_name: 'Anms::Devices::Netbridge')
    DeviceCategory.where(class_name: 'Netreach').update_all(class_name: 'Anms::Devices::Netreach')

    DeviceModel.where(class_name: 'Gateway').update_all(class_name: 'Anms::Devices::Gateway')
    DeviceModel.where(class_name: 'Netbridge').update_all(class_name: 'Anms::Devices::Netbridge')
    DeviceModel.where(class_name: 'Netreach').update_all(class_name: 'Anms::Devices::Netreach')
  end
end
