class AddMccMncToDeviceModems < ActiveRecord::Migration
  def change
    add_column :device_modems, :mcc, :string, limit: 10
    add_column :device_modems, :mnc, :string, limit: 10
  end
end
