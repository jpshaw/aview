class CreateSpeedTest < ActiveRecord::Migration
  def change
    create_table :speed_tests do |t|
      t.integer  :device_modem_id
      t.float    :rx_avg
      t.float    :tx_avg
      t.float    :rx_latency
      t.float    :tx_latency
      t.string   :throughput_units
      t.string   :latency_units
      t.timestamps
    end
  end
end
