class AddDevicesContactsTable < ActiveRecord::Migration
  def change
    create_table :devices_contacts, :id => false do |t|
      t.integer :device_id
      t.integer :contact_id
    end
    add_index :devices_contacts, [:device_id, :contact_id], :unique => true, :name => 'idx_dvc_con_dvc_con'
  end
end
