class AddProxyToNetreachConfigurations < ActiveRecord::Migration
  def change
    add_column :netreach_configurations, :proxy_host, :string, :limit => 100
    add_column :netreach_configurations, :proxy_port, :string, :limit => 10
  end
end
