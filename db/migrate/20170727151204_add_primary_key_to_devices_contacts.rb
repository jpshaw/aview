class AddPrimaryKeyToDevicesContacts < ActiveRecord::Migration
  def change
    add_column :devices_contacts, :id, :primary_key
  end
end
