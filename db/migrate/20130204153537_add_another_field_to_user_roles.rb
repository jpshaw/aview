class AddAnotherFieldToUserRoles < ActiveRecord::Migration
  def change
    add_column :user_roles, :can_manage_configurations, :boolean, :default => false
  end
end
