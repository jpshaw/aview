class IncreaseIntegerSizeOfDeviceModemTxRx < ActiveRecord::Migration
  def up
    change_column :device_modems, :tx, :integer, limit: 8
    change_column :device_modems, :rx, :integer, limit: 8
  end

  def down
    change_column :device_modems, :tx, :integer, limit: 4
    change_column :device_modems, :rx, :integer, limit: 4
  end
end
