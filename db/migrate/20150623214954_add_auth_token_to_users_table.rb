class AddAuthTokenToUsersTable < ActiveRecord::Migration
  def change
    add_column :users, :authentication_token, :string
    add_index :users, :authentication_token, name: 'idx_users_auth_tkn'
  end
end
