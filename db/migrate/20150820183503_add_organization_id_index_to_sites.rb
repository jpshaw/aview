class AddOrganizationIdIndexToSites < ActiveRecord::Migration
  def change
    add_index :sites, :organization_id, name: 'idx_sts_org_id'
  end
end
