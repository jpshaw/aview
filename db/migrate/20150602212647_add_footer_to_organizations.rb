class AddFooterToOrganizations < ActiveRecord::Migration

  def change
    add_column :organizations, :footer, :string
  end

end
