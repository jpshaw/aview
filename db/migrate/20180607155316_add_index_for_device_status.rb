class AddIndexForDeviceStatus < ActiveRecord::Migration
  def change
    add_index :devices, :device_status
  end
end
