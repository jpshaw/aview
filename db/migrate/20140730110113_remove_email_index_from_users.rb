class RemoveEmailIndexFromUsers < ActiveRecord::Migration
  def up
    if index_exists?(:users, :email)
      remove_index :users, :email
    elsif index_exists?(:users, :email, name: 'idx_usr_email')
      remove_index :users, name: :idx_usr_email
    end
  end

  def down
    add_index :users, :email, unique: true, name: 'idx_usr_email'
  end
end
