class RemoveFlagsColumnFromDevices < ActiveRecord::Migration
  def up
    remove_column :devices, :flags
  end

  def down
    add_column :devices, :flags, :text
  end
end
