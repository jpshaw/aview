class CreateCarriers < ActiveRecord::Migration
  def change
    create_table :carrier_details do |t|
      t.string :color
      t.string :display_name, null: false
      t.string :regexp
      t.string :tail

      t.timestamps null: false
    end

    create_table :carriers do |t|
      t.references :carrier_detail
      t.string :name, null: false

      t.timestamps null: false
    end

    add_index :carriers, :name, unique: true
    add_index :carrier_details, :display_name, unique: true
  end
end
