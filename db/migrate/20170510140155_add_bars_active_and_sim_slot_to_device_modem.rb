class AddBarsActiveAndSimSlotToDeviceModem < ActiveRecord::Migration
  def change
    add_column :device_modems, :bars, :integer
    add_column :device_modems, :active, :boolean
    add_column :device_modems, :sim_slot, :integer
  end
end
