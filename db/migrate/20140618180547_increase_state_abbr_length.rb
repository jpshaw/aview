class IncreaseStateAbbrLength < ActiveRecord::Migration
  def up
    change_column :locations, :state_abbr, :string
  end

  def down
    change_column :locations, :state_abbr, :string, limit: 10
  end
end
