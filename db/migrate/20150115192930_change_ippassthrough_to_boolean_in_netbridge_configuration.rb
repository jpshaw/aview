class ChangeIppassthroughToBooleanInNetbridgeConfiguration < ActiveRecord::Migration
  def up
    rename_column :netbridge_configurations, :ippassthrough, :ippassthrough_old
    add_column    :netbridge_configurations, :ippassthrough, :boolean
  end

  def down
    remove_column :netbridge_configurations, :ippassthrough
    rename_column :netbridge_configurations, :ippassthrough_old, :ippassthrough
  end
end
