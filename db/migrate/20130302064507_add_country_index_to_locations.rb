class AddCountryIndexToLocations < ActiveRecord::Migration
  def change
    add_index :locations, :country_id, :name => 'idx_lctn_ctry_id'
  end
end
