class CreateDataPlans < ActiveRecord::Migration
  def change
    create_table :data_plans do |t|
      t.string :name, limit: 50
      t.integer :cycle_type
      t.integer :cycle_start_day
      t.integer :data_limit, limit: 8
      t.integer :individual_data_limit, limit: 8, default: nil
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
