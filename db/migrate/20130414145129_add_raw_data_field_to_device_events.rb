class AddRawDataFieldToDeviceEvents < ActiveRecord::Migration
  def change
    add_column :device_events, :raw_data, :text
  end
end
