class AddDataUsedToDataPlans < ActiveRecord::Migration
  def change
    add_column :data_plans, :data_used, :integer, limit: 8
  end
end
