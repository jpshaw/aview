class AddNotifyEnabledToDataPlans < ActiveRecord::Migration
  def change
    add_column :data_plans, :notify_enabled, :boolean, default: false
  end
end
