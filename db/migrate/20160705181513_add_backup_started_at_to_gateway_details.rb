class AddBackupStartedAtToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :backup_started_at, :datetime
  end
end
