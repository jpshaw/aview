class AddIndexesToUsersAccountsTable < ActiveRecord::Migration
  def change
    add_index :users_accounts, :user_id, :name => 'idx_usr_acnt_usr_id'
    add_index :users_accounts, :account_id, :name => 'idx_usr_acnt_acnt_id'
  end
end
