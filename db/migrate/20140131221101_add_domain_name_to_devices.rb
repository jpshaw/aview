class AddDomainNameToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :domain_name, :string, :length => 255
  end
end
