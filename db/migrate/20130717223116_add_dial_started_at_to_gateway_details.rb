class AddDialStartedAtToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :dial_started_at, :datetime
  end
end
