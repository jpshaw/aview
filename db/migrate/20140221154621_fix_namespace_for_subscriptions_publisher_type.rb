require 'notification_subscription'

class FixNamespaceForSubscriptionsPublisherType < ActiveRecord::Migration
  def up
    NotificationSubscription.where(publisher_type: 'Anms::Devices::Device')
                            .update_all(publisher_type: 'Device')

    NotificationSubscription.where(publisher_type: 'Anms::Organization')
                            .update_all(publisher_type: 'Organization')
  end

  def down
    NotificationSubscription.where(publisher_type: 'Device')
                            .update_all(publisher_type: 'Anms::Devices::Device')

    NotificationSubscription.where(publisher_type: 'Organization')
                            .update_all(publisher_type: 'Anms::Organization')
  end
end
