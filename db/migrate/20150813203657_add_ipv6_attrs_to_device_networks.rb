class AddIpv6AttrsToDeviceNetworks < ActiveRecord::Migration
  def change
    add_column :device_networks, :gw1, :string
    add_column :device_networks, :mtu_ipv6, :string
  end
end
