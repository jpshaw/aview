class AddSmxFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string, :limit => 50
    add_column :users, :account_token, :string, :limit => 32
    add_index :users, :account_token, :unique => true, :name => 'idx_usr_acnt_tkn'
  end
end
