class AddDeploymentIndexToDevices < ActiveRecord::Migration
  def change
    add_index :devices, :is_deployed, :name => 'idx_dvc_is_dpld'
  end
end
