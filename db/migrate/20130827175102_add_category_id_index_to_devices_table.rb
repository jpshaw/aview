class AddCategoryIdIndexToDevicesTable < ActiveRecord::Migration
  def change
    add_index :devices, :category_id, :name => 'idx_dvc_cat_id'
  end
end
