class RemoveStatusColumnAndAddDeviceStateColumnToDevices < ActiveRecord::Migration
  def change
    remove_column :devices, :status
    add_column :devices, :device_status, :integer
  end
end
