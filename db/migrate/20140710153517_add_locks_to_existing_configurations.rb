require 'netbridge_configuration'

class AddLocksToExistingConfigurations < ActiveRecord::Migration
  def up
    NetbridgeConfiguration.find_each(batch_size: 100) do |config|
      NetbridgeConfiguration::CONFIG_KEYS.each do |key|
        config.key_meta[key] ||= {}

        if !config[key].nil? && config[key] != ''
          config.key_meta[key]['locked'] = false
        else
          config.key_meta[key]['locked'] = true
          config[key] = nil
        end
      end

      config.save
    end
  end

  def down
    NetbridgeConfiguration.find_each(batch_size: 100) do |config|
      config.update_attributes(key_meta: nil)
    end
  end
end
