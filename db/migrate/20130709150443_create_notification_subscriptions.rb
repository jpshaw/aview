class CreateNotificationSubscriptions < ActiveRecord::Migration
  def change
    create_table :notification_subscriptions do |t|
      t.integer :user_id
      t.integer :object_id
      t.string :object_type, :limit => 100
      t.integer :profile_id
      t.timestamps
    end
  end
end
