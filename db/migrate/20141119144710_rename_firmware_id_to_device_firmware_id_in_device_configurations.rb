class RenameFirmwareIdToDeviceFirmwareIdInDeviceConfigurations < ActiveRecord::Migration

  def change
    rename_column :device_configurations, :firmware_id, :device_firmware_id
  end

end
