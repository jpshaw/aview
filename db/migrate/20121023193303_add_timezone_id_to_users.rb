class AddTimezoneIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :timezone_id, :integer
  end
end
