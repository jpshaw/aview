class RemoveLengthLimitOnDeviceModemRevision < ActiveRecord::Migration
  def up
    change_column :device_modems, :revision, :string
  end

  def down
    change_column :device_modems, :revision, :string, limit: 50
  end
end
