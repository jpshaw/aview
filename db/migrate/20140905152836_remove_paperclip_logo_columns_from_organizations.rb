class RemovePaperclipLogoColumnsFromOrganizations < ActiveRecord::Migration

  def up
    remove_column :organizations, :logo_content_type
    remove_column :organizations, :logo_file_name
    remove_column :organizations, :logo_file_size
    remove_column :organizations, :logo_updated_at
  end

  def down
    add_column :organizations, :logo_content_type,  :string
    add_column :organizations, :logo_file_name,     :string
    add_column :organizations, :logo_file_size,     :integer
    add_column :organizations, :logo_updated_at,    :datetime
  end

end
