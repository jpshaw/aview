
class AddRestrictedByToUsers < ActiveRecord::Migration
  def change
    add_column :users, :restricted_by, :integer, :default => 1
  end
end
