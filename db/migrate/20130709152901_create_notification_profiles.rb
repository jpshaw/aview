class CreateNotificationProfiles < ActiveRecord::Migration
  def change
    create_table :notification_profiles do |t|
      t.integer :user_id
      t.integer :object_type
      t.text :devices
      t.integer :gw_up_down
      t.timestamps
    end
  end
end
