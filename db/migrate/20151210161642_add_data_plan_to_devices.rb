class AddDataPlanToDevices < ActiveRecord::Migration
  def change
    add_reference :devices, :data_plan, index: true, foreign_key: true
  end
end
