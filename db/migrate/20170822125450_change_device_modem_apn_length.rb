class ChangeDeviceModemApnLength < ActiveRecord::Migration
  def up
    change_column :device_modems, :apn, :string, limit: 255
    change_column :devices, :dns1, :string, limit: 255
    change_column :devices, :dns2, :string, limit: 255
  end

  def down
    change_column :device_modems, :apn, :string, limit: 25
    change_column :devices, :dns1, :string, limit: 20
    change_column :devices, :dns2, :string, limit: 20
  end
end
