class RenameIfaceColumnofDevices < ActiveRecord::Migration
  def change
    rename_column :devices, :active_wan_iface, :primary_wan_iface
  end
end
