# Sorry for the 'Again' in the name but 'AddLogoToOrganizations' was already taken by another migration.
class AddAgainLogoToOrganizations < ActiveRecord::Migration

  def change
    add_column :organizations, :logo, :string
  end

end
