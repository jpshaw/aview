class AddTunnelStatusToNetbridgeDetails < ActiveRecord::Migration
  def change
    add_column :netbridge_details, :tunnel_status, :string
  end
end
