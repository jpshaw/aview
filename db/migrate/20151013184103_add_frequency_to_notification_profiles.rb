class AddFrequencyToNotificationProfiles < ActiveRecord::Migration
  def change
    add_column :notification_profiles, :frequency, :integer
    add_index :notification_profiles,  :frequency, name: 'idx_not_prof_freq'
  end
end
