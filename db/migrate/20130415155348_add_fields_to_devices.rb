class AddFieldsToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :hw_version, :string, :limit => 20
    add_column :devices, :probe_mask, :integer, :limit => 8, :default => 0
    add_column :devices, :dns1, :string, :limit => 20
    add_column :devices, :dns2, :string, :limit => 20
  end
end
