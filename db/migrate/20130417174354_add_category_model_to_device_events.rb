class AddCategoryModelToDeviceEvents < ActiveRecord::Migration
  def change
    add_column :device_events, :category_id, :integer
    add_column :device_events, :model_id, :integer
    add_index :device_events, :category_id, :name => 'idx_dvc_evnt_cat_id'
    add_index :device_events, :model_id, :name => 'idx_dvc_evnt_mdl_id'
  end
end
