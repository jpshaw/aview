class AddHeartbeatColumnsToConfigurations < ActiveRecord::Migration
  def change
    add_column :device_configurations, :heartbeat_frequency, :integer
    add_column :device_configurations, :heartbeat_grace_period, :integer
    add_column :netbridge_configurations, :heartbeat_frequency, :integer
    add_column :netbridge_configurations, :heartbeat_grace_period, :integer
  end
end
