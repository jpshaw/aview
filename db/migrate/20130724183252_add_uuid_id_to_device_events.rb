
class AddUuidIdToDeviceEvents < ActiveRecord::Migration
  def change
    add_column :device_events, :uuid_id, :integer
  end
end
