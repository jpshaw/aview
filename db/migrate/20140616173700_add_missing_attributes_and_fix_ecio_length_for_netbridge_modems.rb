class AddMissingAttributesAndFixEcioLengthForNetbridgeModems < ActiveRecord::Migration
  def up
    add_column :netbridge_modems, :maker, :string, limit: 20
    add_column :netbridge_modems, :model, :string, limit: 20
    change_column :netbridge_modems, :ecio, :string, limit: 10
  end

  def down
    remove_column :netbridge_modems, :maker
    remove_column :netbridge_modems, :model
    change_column :netbridge_modems, :ecio, :string, limit: 6
  end
end
