class CreatePowerOutlets < ActiveRecord::Migration
  def change
    create_table :power_outlets do |t|
      t.integer :device_id
      t.integer :port
      t.integer :the_state
    end
    add_index :power_outlets, :device_id, name: 'idx_dev_id'
  end
end
