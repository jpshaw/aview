class AddGeocodeStateToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :geocode_state, :string
  end
end
