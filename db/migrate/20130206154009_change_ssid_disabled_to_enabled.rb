class ChangeSsidDisabledToEnabled < ActiveRecord::Migration
  def up
    rename_column :netreach_configurations, :ssid_1_disabled, :ssid_1_enabled
    rename_column :netreach_configurations, :ssid_2_disabled, :ssid_2_enabled
    change_column :netreach_configurations, :ssid_1_enabled, :boolean, :default => true
    change_column :netreach_configurations, :ssid_2_enabled, :boolean, :default => true
  end

  def down
    rename_column :netreach_configurations, :ssid_1_enabled, :ssid_1_disabled
    rename_column :netreach_configurations, :ssid_2_enabled, :ssid_1_disabled
    change_column :netreach_configurations, :ssid_1_disabled, :boolean, :default => false
    change_column :netreach_configurations, :ssid_2_disabled, :boolean, :default => false
  end
end
