class AddGroupIdToAbilities < ActiveRecord::Migration
  def change
    add_column :abilities, :group_id, :integer, :default => 0
  end
end
