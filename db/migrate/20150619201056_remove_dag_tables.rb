class RemoveDagTables < ActiveRecord::Migration
  def up
    drop_table :links
  end

  def down
    create_table :links do |t|
      t.integer :ancestor_id
      t.integer :descendant_id
      t.boolean :direct
    end
    add_index :links, :ancestor_id, name: 'idx_lnks_anstr_id'
    add_index :links, :descendant_id, name: 'idx_lnks_dcndt_id'
  end
end
