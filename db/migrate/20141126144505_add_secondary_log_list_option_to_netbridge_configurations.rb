class AddSecondaryLogListOptionToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :log_list2, :string
  end
end
