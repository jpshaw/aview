class RemoveNullConstraintsFromUsersTable < ActiveRecord::Migration
  def up
    # Let these columns be NULL
    change_column_null :users, :email, true
    change_column_null :users, :encrypted_password, true
  end

  def down
    # Prevent these columns from being NULL
    change_column_null :users, :email, false
    change_column_null :users, :encrypted_password, false
  end
end
