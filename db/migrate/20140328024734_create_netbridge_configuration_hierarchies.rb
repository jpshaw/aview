class CreateNetbridgeConfigurationHierarchies < ActiveRecord::Migration
  def change
    create_table :netbridge_config_hierarchies, :id => false do |t|
      # ID of the parent/grandparent/great-grandparent/... configuration
      t.integer  :ancestor_id, :null => false
      # ID of the target configuration
      t.integer  :descendant_id, :null => false
      # Number of generations between the ancestor and the descendant.
      # Parent/child = 1, for example.
      t.integer  :generations, :null => false
    end

    # For "all progeny of…" and leaf selects:
    add_index :netbridge_config_hierarchies, [:ancestor_id, :descendant_id, :generations],
      :unique => true, :name => "idx_nb_cfg_anc_desc"

    # For "all ancestors of…" selects,
    add_index :netbridge_config_hierarchies, [:descendant_id],
      :name => "idx_nb_cfg_desc_idx"
  end
end
