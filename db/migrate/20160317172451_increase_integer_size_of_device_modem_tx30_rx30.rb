class IncreaseIntegerSizeOfDeviceModemTx30Rx30 < ActiveRecord::Migration
  def up
    change_column :device_modems, :tx_30, :integer, limit: 8
    change_column :device_modems, :rx_30, :integer, limit: 8
  end

  def down
    change_column :device_modems, :tx_30, :integer, limit: 4
    change_column :device_modems, :rx_30, :integer, limit: 4
  end
end
