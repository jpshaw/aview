class AddUnreachableAtToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :unreachable_at, :datetime
  end
end
