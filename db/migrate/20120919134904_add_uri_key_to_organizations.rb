class AddUriKeyToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :uri_key, :string, :length => 50
  end
end
