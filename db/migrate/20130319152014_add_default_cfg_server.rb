class AddDefaultCfgServer < ActiveRecord::Migration
  def up
    change_column :netbridge_configurations, :cfg, :string, :default => "anms-config.accns.com"
  end

  def down
     raise ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
