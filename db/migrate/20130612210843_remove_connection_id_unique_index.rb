class RemoveConnectionIdUniqueIndex < ActiveRecord::Migration
  def up
    remove_index :gateway_tunnels, :name => :idx_gtw_tnl_conn_id
  end

  def down
    add_index :gateway_tunnels, :connection_id, :unique => true, :name => 'idx_gtw_tnl_conn_id'
  end
end
