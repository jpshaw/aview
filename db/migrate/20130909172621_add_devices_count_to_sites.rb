require 'site'

class AddDevicesCountToSites < ActiveRecord::Migration
  def up
    add_column :sites, :devices_count, :integer, :default => 0

    Site.reset_column_information
    Site.find_each do |p|
      Site.update_counters p.id, :devices_count => p.devices.length
    end
  end

  def down
    remove_column :sites, :devices_count
  end
end
