class AddAccountNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :account_name, :string, :limit => 50
  end
end
