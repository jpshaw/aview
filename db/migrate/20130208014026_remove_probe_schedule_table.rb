class RemoveProbeScheduleTable < ActiveRecord::Migration
  def up
    drop_table :probe_schedules
  end

  def down
  end
end
