require 'device'

class RemoveNamespaceFromDeviceObjectTypes < ActiveRecord::Migration
  def up
    Device.where(object_type: 'Anms::Devices::Gateway').update_all(object_type: 'Gateway')
    Device.where(object_type: 'Anms::Devices::Netbridge').update_all(object_type: 'Netbridge')
    Device.where(object_type: 'Anms::Devices::Netreach').update_all(object_type: 'Netreach')
  end

  def down
    Device.where(object_type: 'Gateway').update_all(object_type: 'Anms::Devices::Gateway')
    Device.where(object_type: 'Netbridge').update_all(object_type: 'Anms::Devices::Netbridge')
    Device.where(object_type: 'Netreach').update_all(object_type: 'Anms::Devices::Netreach')
  end
end
