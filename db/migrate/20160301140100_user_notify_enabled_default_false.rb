class UserNotifyEnabledDefaultFalse < ActiveRecord::Migration
  def change
    change_column_default :users, :notify_enabled, false
  end
end
