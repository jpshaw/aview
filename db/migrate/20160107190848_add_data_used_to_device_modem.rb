class AddDataUsedToDeviceModem < ActiveRecord::Migration
  def change
    add_column :device_modems, :data_used, :integer, limit: 8
  end
end
