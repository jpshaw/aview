class AddGatewayConfigurationIndexes < ActiveRecord::Migration
  def change
    add_index :devices, :configuration_id
    add_index :gateway_configurations, :portal_id
    add_index :gateway_configurations, :snmp_id
  end
end
