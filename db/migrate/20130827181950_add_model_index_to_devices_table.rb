class AddModelIndexToDevicesTable < ActiveRecord::Migration
  def change
    add_index :devices, :model_id, :name => 'idx_dvc_mdl_id'
  end
end
