class AddLifetimeToDevice < ActiveRecord::Migration
  def change
    # default lifetime: 10 minutes (60*10)
    add_column :devices, :lifetime, :integer, { null: false, default: 60*10 }
  end
end
