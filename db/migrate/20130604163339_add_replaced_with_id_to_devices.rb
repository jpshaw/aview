
class AddReplacedWithIdToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :replaced_with_id, :integer, :default => nil
  end
end
