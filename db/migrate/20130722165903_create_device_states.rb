class CreateDeviceStates < ActiveRecord::Migration
  def change
    create_table :device_states do |t|
      t.integer :device_id
      t.integer :state
      t.integer :lock_version

      t.timestamps
    end
  end
end
