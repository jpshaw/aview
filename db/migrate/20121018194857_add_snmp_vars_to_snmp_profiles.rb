class AddSnmpVarsToSnmpProfiles < ActiveRecord::Migration
  def change
    add_column :snmp_profiles, :auth_protocol, :string, :limit => 3
    add_column :snmp_profiles, :priv_protocol, :string, :limit => 3
    add_column :snmp_profiles, :security_level, :integer, :default => 3
  end
end
