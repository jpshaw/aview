class AddOrganizationsLocationsTable < ActiveRecord::Migration
  def change
    create_table :organizations_locations, :id => false do |t|
      t.integer :organization_id
      t.integer :location_id
    end
    add_index :organizations_locations, [:organization_id, :location_id], :unique => true, :name => 'org_lctn_org_lctn'
  end
end
