class ChangeContentColumnForRecords < ActiveRecord::Migration
  def up
    remove_column :records, :content
    add_column :records, :content, :string
  end

  def down
    remove_column :records, :content
    add_column :records, :content, :text
  end
end
