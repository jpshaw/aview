class AddIndexesToDeviceEventsTable < ActiveRecord::Migration
  def change
    add_index :device_events, :organization_id, :name => 'idx_dvc_evnt_org_id'
    add_index :device_events, :device_id, :name => 'idx_dvc_evnt_dvc_id'
    add_index :device_events, :device_type, :name => 'idx_dvc_evnt_dvc_typ'
    add_index :device_events, :event_type, :name => 'idx_dvc_evnt_evnt_typ'
    add_index :device_events, :level, :name => 'idx_dvc_evnt_lvl'
  end
end
