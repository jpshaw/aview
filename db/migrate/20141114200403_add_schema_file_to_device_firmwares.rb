class AddSchemaFileToDeviceFirmwares < ActiveRecord::Migration

  def change
    add_column :device_firmwares, :schema_file, :string
  end

end
