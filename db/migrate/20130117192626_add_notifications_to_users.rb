class AddNotificationsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notify_enabled, :boolean, :default => false
    add_column :users, :notify_nr_up, :boolean, :default => false
    add_column :users, :notify_nr_down, :boolean, :default => false
    add_column :users, :notify_nr_config, :boolean, :default => false
    add_column :users, :notify_nr_fw, :boolean, :default => false
  end
end
