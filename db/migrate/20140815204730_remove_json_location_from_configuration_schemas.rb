class RemoveJsonLocationFromConfigurationSchemas < ActiveRecord::Migration

  def up
    remove_column :configuration_schemas, :json_location
  end

  def down
    add_column :configuration_schemas, :json_location, :string
  end

end
