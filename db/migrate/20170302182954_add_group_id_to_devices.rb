class AddGroupIdToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :group_id, :integer
    add_index :devices, :group_id
  end
end
