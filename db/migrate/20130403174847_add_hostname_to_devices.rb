class AddHostnameToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :hostname, :string
  end
end
