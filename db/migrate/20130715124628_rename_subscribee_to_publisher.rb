class RenameSubscribeeToPublisher < ActiveRecord::Migration
  def up
    rename_column :notification_profiles, :subscribee_type, :publisher_type
    rename_column :notification_subscriptions, :subscribee_id, :publisher_id
    rename_column :notification_subscriptions, :subscribee_type, :publisher_type
  end

  def down
    rename_column :notification_profiles, :publisher_type, :subscribee_type
    rename_column :notification_subscriptions, :publisher_id, :subscribee_id
    rename_column :notification_subscriptions, :publisher_type, :subscribee_type
  end
end
