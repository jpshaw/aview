class AddActivatedAtFieldToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :activated_at, :datetime
  end
end
