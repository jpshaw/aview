class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :ancestor_id
      t.string  :ancestor_type, :limit => 50
      t.integer :descendant_id
      t.string  :descendant_type, :limit => 50
      t.boolean :direct
      t.integer :count
    end
    add_index :links, :ancestor_id, :name => 'idx_lnks_anstr_id'
    add_index :links, :descendant_id, :name => 'idx_lnks_dcndt_id'
    add_index :links, :ancestor_type, :name => 'idx_lnks_anstr_typ'
    add_index :links, :descendant_type, :name => 'idx_lnks_dcndt_typ'
  end
end
