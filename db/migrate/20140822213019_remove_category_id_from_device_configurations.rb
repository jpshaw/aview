class RemoveCategoryIdFromDeviceConfigurations < ActiveRecord::Migration

  def up
    if index_exists?(:device_configurations, [:organization_id, :category_id], name: 'idx_dev_conf_org_id_cat_id')
      remove_index :device_configurations, name: 'idx_dev_conf_org_id_cat_id'
    end
    remove_column :device_configurations, :category_id
    add_index :device_configurations, :organization_id, name: 'idx_dev_conf_org_id'
  end

  def down
    add_column    :device_configurations, :category_id, :integer
    add_index     :device_configurations, :category_id, name: 'idx_dev_conf_cat_id'
    remove_index  :device_configurations, name: 'idx_dev_conf_org_id'
    add_index     :device_configurations, [:organization_id, :category_id], name: 'idx_dev_conf_org_id_cat_id'
  end

end
