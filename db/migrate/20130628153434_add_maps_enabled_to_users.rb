class AddMapsEnabledToUsers < ActiveRecord::Migration
  def change
    add_column :users, :maps_enabled, :boolean, :default => true
  end
end
