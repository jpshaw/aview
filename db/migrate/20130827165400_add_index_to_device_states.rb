class AddIndexToDeviceStates < ActiveRecord::Migration
  def change
    add_index :device_states, :device_id, :unique => true, :name => 'idx_dvc_stt_dvc_id'
  end
end
