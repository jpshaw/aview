class AddModelIdToDeviceFirmwares < ActiveRecord::Migration

  def change
    add_column  :device_firmwares, :model_id, :integer
    add_index   :device_firmwares, :model_id, name: 'idx_dev_firm_mod_id'
  end

end
