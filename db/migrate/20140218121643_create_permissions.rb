class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.references :manager,    polymorphic: true
      t.references :manageable, polymorphic: true
      t.references :role
      t.references :parent

      t.timestamps
    end
    add_index :permissions, :manager_id,    name: "idx_perm_mgr_id"
    add_index :permissions, :manageable_id, name: "idx_perm_mgble_id"
    add_index :permissions, :role_id,       name: "idx_perm_role_id"
    add_index :permissions, :parent_id,     name: "idx_perm_parent_id"
    add_index(
      :permissions,
      [:manager_id, :manager_type, :manageable_id, :manageable_type],
      unique: true,
      name: "idx_perm_mgr_mgble"
    )
  end
end
