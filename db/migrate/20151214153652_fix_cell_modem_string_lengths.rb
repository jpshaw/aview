class FixCellModemStringLengths < ActiveRecord::Migration
  def change
    change_column :device_modems, :name, :string, limit: nil
    change_column :device_modems, :carrier, :string, limit: nil
    change_column :device_modems, :maker, :string, limit: nil
  end
end
