class AddLastRestartToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :last_restarted_at, :datetime
  end
end
