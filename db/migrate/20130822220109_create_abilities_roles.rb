class CreateAbilitiesRoles < ActiveRecord::Migration
  def change
    create_table :abilities_roles, :id => false do |t|
      t.references :ability, :role
    end

    add_index :abilities_roles, [:ability_id, :role_id],
      name: "idx_ablt_rls_ablt_rl",
      unique: true
  end
end
