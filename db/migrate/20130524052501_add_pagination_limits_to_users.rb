class AddPaginationLimitsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pagination_limits, :text
  end
end
