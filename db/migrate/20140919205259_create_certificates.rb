class CreateCertificates < ActiveRecord::Migration

  def change
    create_table :certificates, force: true do |t|
      t.text        :cert
      t.boolean     :approved, default: false
      t.boolean     :exported
      t.references  :device
    end

    add_index :certificates, :device_id, name: 'idx_cert_dev_id'
  end

end
