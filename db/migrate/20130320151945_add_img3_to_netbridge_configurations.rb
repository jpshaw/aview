class AddImg3ToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :img3, :string
  end
end
