class RenameNetbridgeModemsToDeviceModems < ActiveRecord::Migration
  def change
    rename_table :netbridge_modems, :device_modems
  end
end
