class AddEthAttrsToNetbridgeDetails < ActiveRecord::Migration
  def change
    add_column :netbridge_details, :client_ip, :string, limit: 20
    add_column :netbridge_details, :eth_state, :string, limit: 20
    add_column :netbridge_details, :eth_macs, :text
  end
end
