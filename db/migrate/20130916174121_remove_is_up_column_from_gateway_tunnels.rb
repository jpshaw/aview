class RemoveIsUpColumnFromGatewayTunnels < ActiveRecord::Migration
  def up
    remove_column :gateway_tunnels, :is_up
  end

  def down
    add_column :gateway_tunnels, :is_up, :boolean, :default => false
  end
end
