class AddTunnelServerIdToGatewayTunnels < ActiveRecord::Migration
  def change
    add_column :gateway_tunnels, :tunnel_server_id, :integer
    add_index :gateway_tunnels, :tunnel_server_id
  end
end
