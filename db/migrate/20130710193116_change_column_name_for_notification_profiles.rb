class ChangeColumnNameForNotificationProfiles < ActiveRecord::Migration
  def change
    change_table :notification_profiles do |t|
      t.rename :object_type, :subscribee_type
    end
  end
end
