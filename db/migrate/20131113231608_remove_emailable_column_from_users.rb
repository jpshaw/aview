class RemoveEmailableColumnFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :emailable
  end
end
