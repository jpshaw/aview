class AddConfigurationNameToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :configuration_name, :string
  end
end
