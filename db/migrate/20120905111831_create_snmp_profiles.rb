class CreateSnmpProfiles < ActiveRecord::Migration
  def change
    create_table :snmp_profiles do |t|
      t.string :version, :limit => 2
      t.string :community, :limit => 50
      t.string :username, :limit => 50
      t.string :auth_password, :limit => 50
      t.string :priv_password, :limit => 50
      t.timestamps
    end
  end
end
