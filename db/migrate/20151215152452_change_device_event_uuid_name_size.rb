class ChangeDeviceEventUuidNameSize < ActiveRecord::Migration
  def change
    change_column :device_event_uuids, :name, :string, limit: nil
  end
end
