class RemovePolymorphicColumnsFromOrganizations < ActiveRecord::Migration
  def up
    remove_index :links, :name => 'idx_lnks_anstr_typ'
    remove_index :links, :name => 'idx_lnks_dcndt_typ'
    remove_column :links, :ancestor_type
    remove_column :links, :descendant_type
  end

  def down
    add_column :links, :ancestor_type, :string, :limit => 50
    add_column :links, :descendant_type, :string, :limit => 50
    add_index :links, :ancestor_type, :name => 'idx_lnks_anstr_typ'
    add_index :links, :descendant_type, :name => 'idx_lnks_dcndt_typ'
  end
end
