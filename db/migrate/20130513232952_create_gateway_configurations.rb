class CreateGatewayConfigurations < ActiveRecord::Migration
  def change
    create_table :gateway_configurations do |t|
      t.boolean :snmp_enabled, :default => false
      t.integer :snmp_id
      t.integer :portal_id
      t.timestamps
    end
  end
end
