class ChangePaginationLimitToTextForUsers < ActiveRecord::Migration
  def self.up
    if ActiveRecord::Base.connection.column_exists?(:users, :pagination_limit)
      remove_column :users, :pagination_limit
    end
    add_column :users, :pagination_limit, :text, :default => nil, :limit => 3000
  end
  def self.down
    if ActiveRecord::Base.connection.column_exists?(:users, :pagination_limit)
      remove_column :users, :pagination_limit
    end
    add_column :users, :pagination_limit, :integer, :default => 100
  end
end
