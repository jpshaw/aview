class CreateDeviceConfigurations < ActiveRecord::Migration

  def change
    create_table :device_configurations do |t|
      t.string      :name
      t.text        :description
      t.references  :organization
      t.references  :category

      t.timestamps
    end

    add_index :device_configurations, [:organization_id, :category_id], name: 'idx_dev_conf_org_id_cat_id'
    add_index :device_configurations, :category_id,                     name: 'idx_dev_conf_cat_id'
  end

end
