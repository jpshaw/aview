class IncreaseIntegerSizeOfCellularUtilizationCidAndLac < ActiveRecord::Migration
  def up
    change_column :cellular_locations, :cid, :integer, limit: 8
    change_column :cellular_locations, :lac, :integer, limit: 8
  end

  def down
    change_column :cellular_locations, :cid, :integer, limit: 4
    change_column :cellular_locations, :lac, :integer, limit: 4
  end
end
