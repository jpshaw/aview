class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :organization_id
      t.integer :device_model_id
      t.integer :firmware_id
      t.string :uuid
      t.timestamps null: false
    end

    add_index :groups, :organization_id
    add_index :groups, :device_model_id
    add_index :groups, :firmware_id
  end
end
