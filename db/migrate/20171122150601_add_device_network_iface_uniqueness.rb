class AddDeviceNetworkIfaceUniqueness < ActiveRecord::Migration
  def change
    add_index :device_networks, [:device_id, :network_iface], unique: true
  end
end
