class AddActiveWanIfaceToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :active_wan_iface, :string, limit: 20
    add_index  :devices, :active_wan_iface, :name => "idx_dvc_wan_iface"
  end
end
