class AddGatewayCellModemSetting < ActiveRecord::Migration
  def change
    add_column :organizations, :gateway_cell_modem_collection_enabled, :boolean, default: false
  end
end
