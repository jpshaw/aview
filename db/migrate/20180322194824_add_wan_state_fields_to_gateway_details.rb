class AddWanStateFieldsToGatewayDetails < ActiveRecord::Migration
  def change
    add_column :gateway_details, :wan_ipv4_state, :integer
    add_column :gateway_details, :wan_ipv6_state, :integer
  end
end
