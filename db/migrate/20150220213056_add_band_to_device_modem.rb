class AddBandToDeviceModem < ActiveRecord::Migration
  def change
    add_column :device_modems, :band, :string
  end
end
