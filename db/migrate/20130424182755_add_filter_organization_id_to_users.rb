class AddFilterOrganizationIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :filter_organization_id, :integer
  end
end
