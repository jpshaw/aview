class CreateProbeSchedules < ActiveRecord::Migration
  def change
    create_table :probe_schedules do |t|
      t.integer :netgate_id
      t.integer :hour_mask, :limit => 8
      t.integer :minute_mask, :limit => 8
      t.string :hours, :limit => 100
      t.string :minutes, :limit => 100
      t.integer :probe_type
      t.timestamps
    end
  end
end
