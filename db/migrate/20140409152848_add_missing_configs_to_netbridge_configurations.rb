class AddMissingConfigsToNetbridgeConfigurations < ActiveRecord::Migration
  def change
    add_column :netbridge_configurations, :hostname, :string, limit: 50
    add_column :netbridge_configurations, :ippassthrough, :string, limit: 10
    add_column :netbridge_configurations, :proxy_arp, :string, limit: 10
    add_column :netbridge_configurations, :usb_adjust, :string, limit: 10
  end
end
