class RenameModelIdToDeviceModelIdInDeviceFirmwares < ActiveRecord::Migration

  def change
    rename_column :device_firmwares, :model_id, :device_model_id
  end

end
