class AddDefaultValuesToNetreachConfigurations < ActiveRecord::Migration
  def up
    change_column :netreach_configurations, :admin_access, :boolean, :default => true
    change_column :netreach_configurations, :admin_username, :string, :default => "wifiadmin", :limit => 50
    change_column :netreach_configurations, :admin_password, :string, :default => "w1f1adm1npw", :limit => 50
    change_column :netreach_configurations, :tz_offset, :string, :default => "-300", :limit => 10
    change_column :netreach_configurations, :wds_mode, :integer, :default => 0
    change_column :netreach_configurations, :network_mode, :integer, :default => 6
    change_column :netreach_configurations, :country_code, :string, :default => "US", :limit => 2
    change_column :netreach_configurations, :channel, :integer, :default => 11
    change_column :netreach_configurations, :channel_region, :integer, :default => 5
    change_column :netreach_configurations, :rf_power, :integer, :default => 100
    change_column :netreach_configurations, :ssid_1_sec, :integer, :default => 4
    change_column :netreach_configurations, :ssid_1_wep_len, :integer, :default => 2
    change_column :netreach_configurations, :ssid_1_pass_t, :string, :default => "C", :limit => 50
    change_column :netreach_configurations, :ssid_1_pass, :string, :default => "attn3tgat3w1f2", :limit => 50
    change_column :netreach_configurations, :ssid_1_wpa_c, :string, :default => "A", :limit => 50
    change_column :netreach_configurations, :ssid_1_port, :string, :default => "01812", :limit => 10
    change_column :netreach_configurations, :ssid_1_timeout, :integer, :default => 3600
    change_column :netreach_configurations, :ssid_2_sec, :integer, :default => 4
    change_column :netreach_configurations, :ssid_2_wep_len, :integer, :default => 2
    change_column :netreach_configurations, :ssid_2_pass_t, :string, :default => "C", :limit => 50
    change_column :netreach_configurations, :ssid_2_pass, :string, :default => "attn3tgat3w1f2", :limit => 50
    change_column :netreach_configurations, :ssid_2_wpa_c, :string, :default => "A", :limit => 50
    change_column :netreach_configurations, :ssid_2_port, :string, :default => "01812", :limit => 10
    change_column :netreach_configurations, :ssid_2_timeout, :integer, :default => 3600
  end

  def down
    change_column :netreach_configurations, :admin_access, :boolean, :default => false
    change_column :netreach_configurations, :admin_username, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :admin_password, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :tz_offset, :string, :default => nil, :limit => 10
    change_column :netreach_configurations, :wds_mode, :integer, :default => nil
    change_column :netreach_configurations, :network_mode, :integer, :default => nil
    change_column :netreach_configurations, :country_code, :string, :default => nil, :limit => 2
    change_column :netreach_configurations, :channel, :string, :default => nil, :limit => 4
    change_column :netreach_configurations, :channel_region, :string, :default => nil, :limit => 5
    change_column :netreach_configurations, :rf_power, :integer, :default => nil
    change_column :netreach_configurations, :ssid_1_sec, :integer, :default => nil
    change_column :netreach_configurations, :ssid_1_wep_len, :integer, :default => nil
    change_column :netreach_configurations, :ssid_1_pass_t, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_1_pass, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_1_wpa_c, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_1_port, :string, :default => nil, :limit => 10
    change_column :netreach_configurations, :ssid_1_timeout, :integer, :default => nil
    change_column :netreach_configurations, :ssid_2_sec, :integer, :default => nil
    change_column :netreach_configurations, :ssid_2_wep_len, :integer, :default => nil
    change_column :netreach_configurations, :ssid_2_pass_t, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_2_pass, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_2_wpa_c, :string, :default => nil, :limit => 50
    change_column :netreach_configurations, :ssid_2_port, :string, :default => nil, :limit => 10
    change_column :netreach_configurations, :ssid_2_timeout, :integer, :default => nil
  end
end
