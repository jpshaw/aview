class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|
      t.string :name, :default => "", :limit => 50
      t.boolean :is_admin, :default => false
      t.boolean :can_admin_current_organization, :default => false
      t.boolean :can_admin_devices, :default => false
      t.boolean :can_admin_organizations, :default => false
      t.boolean :can_export_device_reports, :default => false
      t.boolean :can_manage_devices, :default => false
      t.boolean :can_manage_organizations, :default => false
      t.boolean :can_view_devices, :default => false
      t.boolean :can_view_organizations, :default => false

      t.timestamps
    end
  end
end
