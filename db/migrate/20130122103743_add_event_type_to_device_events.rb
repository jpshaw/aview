class AddEventTypeToDeviceEvents < ActiveRecord::Migration
  def change
    add_column :device_events, :event_type, :integer
  end
end
