class RemoveTestRunsTable < ActiveRecord::Migration
  def up
    drop_table :test_runs
  end

  def down
    create_table :test_runs do |t|
      t.boolean  :active,             default: true
      t.integer  :organization_count, default: 0
      t.integer  :device_count,       default: 0
      t.integer  :organization_id
      t.datetime :ended_at
      t.timestamps
    end
  end
end
