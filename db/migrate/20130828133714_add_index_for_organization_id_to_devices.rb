class AddIndexForOrganizationIdToDevices < ActiveRecord::Migration
  def change
     add_index :devices, :organization_id, :name => 'idx_dvc_org_id'
     add_index :devices, :account_id, :name => 'idx_dvc_acnt_id'
  end
end
