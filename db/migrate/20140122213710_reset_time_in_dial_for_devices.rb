class ResetTimeInDialForDevices < ActiveRecord::Migration
  def change
    devices = Gateway.joins(:details)
                     .where("gateway_details.time_in_dial > 0")

    devices.each do |device|
      device.details.time_in_dial = 0
      device.details.save
    end
  end
end
