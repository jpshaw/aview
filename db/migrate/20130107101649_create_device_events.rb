class CreateDeviceEvents < ActiveRecord::Migration
  def change
    create_table :device_events do |t|
      t.integer :organization_id
      t.integer :device_type
      t.integer :device_id
      t.integer :level
      t.string :name
      t.text :information

      t.timestamps
    end
  end
end
