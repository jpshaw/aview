class AddChecksumsToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :address_checksum, :string, limit: 32, index: true
    add_column :locations, :coordinates_checksum, :string, limit: 32, index: true

    add_index :locations, :address_checksum
    add_index :locations, :coordinates_checksum
  end
end
