# Accelerated View

[![Code Climate](https://codeclimate.com/repos/54ecd3286956802eee00a030/badges/6583836069bf7a192304/gpa.svg)](https://codeclimate.com/repos/54ecd3286956802eee00a030/feed)
[![Test Coverage](https://codeclimate.com/repos/54ecd3286956802eee00a030/badges/6583836069bf7a192304/coverage.svg)](https://codeclimate.com/repos/54ecd3286956802eee00a030/feed)

**Accelerated View** is a collection of services for monitoring and reporting network devices.

[Contributing](doc/contributing.md)  
[Setup](doc/setup.md)  
[Bower](doc/bower.md) - for installing front-end assets  
[Engines](doc/engines.md)  
[TorqueBox](doc/torquebox.md)  
[Testing](doc/testing.md)  
[Building](doc/building.md)  
[Deploying](doc/deploying.md)  
[Packaging](doc/packaging.md)  
[Operations](doc/operations.md)  
[Documentation](doc/documentation.md)
[Service Manager Replication](doc/service_manager_replication.md)