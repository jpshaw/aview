#!/usr/bin/env ruby

$stdout.sync = true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), 'lib'))

require 'rubygems'
require 'bundler/setup'
require 'smx_replication'

SMX::Replication::Server.run
