lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'kraken/logger/version'

Gem::Specification.new do |spec|
  spec.name          = 'kraken-logger'
  spec.version       = Kraken::Logger::VERSION
  spec.authors       = ['Michael Zaccari']
  spec.email         = ['michael.zaccari@accelerated.com']

  spec.summary       = ''
  spec.description   = ''
  spec.homepage      = 'https://bitbucket.org/accelecon/aview'

  # Prevent pushing this gem to rubygems.org.
  spec.metadata['allowed_push_host'] = 'http://rubygems.accns.com/'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '>= 4.2.0'
  spec.add_dependency 'logging', '>= 2.2.0'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
