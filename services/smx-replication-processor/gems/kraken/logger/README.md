# Kraken::Logger

Provides a common interface for logging to a console or file from the web application or a service.

## Installation

Add this line to your Gemfile:

```ruby
gem 'kraken-logger'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install kraken-logger

## Usage

### Console

```ruby
class Hello
  include Kraken::Logger.console
end

module Foo
  class Bar
    include Kraken::Logger.console
  end
end

module Baz
  include Kraken::Logger.console
end

Hello.logger.info 'class'
Hello.new.logger.info 'instance'
Foo::Bar.logger.info 'nested class'
Foo::Bar.new.logger.info 'nested instance'
Baz.logger.info 'module'
```

Output:

```
[2018-03-02T00:41:00] INFO  Hello: class
[2018-03-02T00:41:00] INFO  Hello: instance
[2018-03-02T00:41:00] INFO  Foo::Bar: nested class
[2018-03-02T00:41:00] INFO  Foo::Bar: nested instance
[2018-03-02T00:41:00] INFO  Baz: module
```

### File

```ruby
class Hello
  include Kraken::Logger.file
end

module Foo
  class Bar
    include Kraken::Logger.file
  end
end

module Baz
  include Kraken::Logger.file
end

Hello.logger.info 'class'
Hello.new.logger.info 'instance'
Foo::Bar.logger.info 'nested class'
Foo::Bar.new.logger.info 'nested instance'
Baz.logger.info 'module'
```

In `Kraken::Logger.base_dir/hello.log`:

```
[2018-03-02T00:45:35] INFO  Hello: class
[2018-03-02T00:45:35] INFO  Hello: instance
```

In `Kraken::Logger.base_dir/foo/bar.log`:

```
[2018-03-02T00:45:35] INFO  Foo::Bar: nested class
[2018-03-02T00:45:35] INFO  Foo::Bar: nested instance
```

In `Kraken::Logger.base_dir/baz.log`:

```
[2018-03-02T00:45:35] INFO  Baz: module
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.accns.com](https://rubygems.accns.com).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/accelecon/aview.
