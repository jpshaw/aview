require 'kraken/logger/core_ext'
require 'kraken/logger/logging'
require 'kraken/logger/console'
require 'kraken/logger/file'
require 'kraken/logger/version'

module Kraken
  module Logger
    extend self

    mattr_accessor(:loggers)  { ::Set.new }
    mattr_accessor(:base_dir) { '/tmp' }
    mattr_reader(:verbose)    { false }

    def configure(&block)
      yield self
    end

    def console
      ::Kraken::Logger::ConsoleMixin
    end

    def file
      ::Kraken::Logger::FileMixin
    end

    def register(logger)
      loggers << logger
    end

    def verbose=(verbose)
      class_variable_set('@@verbose', (verbose == true))
      loggers.each { |logger| logger.level = level }
    end

    def level
      verbose ? :debug : :info
    end
  end
end

# Toggle verbosity with USR1 signal
# Signal.trap('USR1') do
#   Thread.new do
#     ::Kraken::Logger.verbose = !::Kraken::Logger.verbose
#   end
# end
