module Kraken
  module Logger
    module File
      extend self

      # The maximum allowed size (in bytes) of a log file before it is rolled.
      mattr_accessor(:size)  { 26214400 } # 25 megabytes

      # The number of rolled log files to keep.
      mattr_accessor(:keep)  { 5 }

      # The Layout that will be used by the appender. The Basic layout will be
      # used if none is given.
      mattr_accessor(:layout) do
        ::Logging.layouts.pattern(
               pattern: '[%d] %-5l %c: %m\n',
          color_scheme: 'bright',
             backtrace: true
        )
      end

      mattr_reader(:appenders) { ::Hash.new }

      def [](name)
        logger = ::Logging.logger[name]
        logger.appenders = fetch_appender(name)
        logger.level = ::Kraken::Logger.level
        ::Kraken::Logger.register(logger)
        logger
      end

      private

      def fetch_appender(name)
        file_path = path_for_name(name)

        # TODO: Add mutex
        appenders[file_path] ||= begin
          ::Logging.appenders.rolling_file(file_path, size: size,
                                                      keep: keep,
                                                    layout: layout)
        end
      end

      def path_for_name(name)
        path = "#{::Kraken::Logger.base_dir}/#{name.to_s.underscore}.log"
        dir  = ::File.dirname(path)
        # Ensure path exists
        ::FileUtils.mkdir_p(dir) unless ::File.directory?(dir)
        path
      end
    end

    module FileMixin
      def self.included(base)
        base.class_eval do
          extend ClassMethods
          delegate :logger, to: :class
        end
      end

      module ClassMethods
        def logger
          @logger ||= ::Kraken::Logger::File[self]
        end
      end
    end
  end
end
