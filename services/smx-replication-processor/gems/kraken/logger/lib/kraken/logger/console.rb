module Kraken
  module Logger
    module Console
      extend self

      def [](name)
        logger = ::Logging.logger[name]
        logger.add_appenders 'stdout'
        logger.level = ::Kraken::Logger.level
        ::Kraken::Logger.register(logger)
        logger
      end
    end

    module ConsoleMixin
      def self.included(base)
        base.class_eval do
          extend ClassMethods
          delegate :logger, to: :class
        end
      end

      module ClassMethods
        def logger
          @logger ||= ::Kraken::Logger::Console[self]
        end
      end
    end
  end
end
