require 'logging'

## Colors
::Logging.color_scheme('bright',
  levels: {
    debug: :magenta,
     info: :green,
     warn: :yellow,
    error: :red,
    fatal: [:white, :on_red]
  },
     date: :blue,
   logger: :cyan,
  message: :white
)

## Appenders
::Logging.appenders.stdout('stdout',
  layout: ::Logging.layouts.pattern(
         pattern: '[%d] %-5l %c: %m\n',
    color_scheme: 'bright'
  )
)
