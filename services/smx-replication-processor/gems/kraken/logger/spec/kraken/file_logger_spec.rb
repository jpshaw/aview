RSpec.describe 'File Logger' do
  context 'class' do
    subject { Class.new { include Kraken::Logger.file } }

    it 'adds an instance logger' do
      expect(subject.new.logger).to be_present
    end

    it 'adds a class logger' do
      expect(subject.logger).to be_present
    end
  end

  context 'module' do
    subject { Module.new { include Kraken::Logger.file } }

    it 'adds a module logger' do
      expect(subject.logger).to be_present
    end
  end

  context 'file path' do
    it 'creates directories if needed' do
      module Foo
        class Bar
          include Kraken::Logger.file
        end
      end

      path = "#{Kraken::Logger.base_dir}/foo/bar.log"

      expect(File.exist?(path)).to be false
      Foo::Bar.new.logger.info 'Hello World'
      expect(File.exist?(path)).to be true
    end
  end
end
