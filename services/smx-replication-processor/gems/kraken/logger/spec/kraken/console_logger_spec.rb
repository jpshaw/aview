RSpec.describe 'Console Logger' do
  context 'class' do
    subject { Class.new { include Kraken::Logger.console } }

    it 'adds an instance logger' do
      expect(subject.new.logger).to be_present
    end

    it 'adds a class logger' do
      expect(subject.logger).to be_present
    end
  end

  context 'module' do
    subject { Module.new { include Kraken::Logger.console } }

    it 'adds a module logger' do
      expect(subject.logger).to be_present
    end
  end
end
