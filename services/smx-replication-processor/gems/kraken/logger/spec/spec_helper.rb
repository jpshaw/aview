require 'bundler/setup'
require 'kraken/logger'
require 'tmpdir'

def tmp_dir
  @tmp_dir ||= ::Dir.mktmpdir
end

Kraken::Logger.configure do |config|
  config.base_dir = tmp_dir
end

RSpec.configure do |config|
  config.example_status_persistence_file_path = '.rspec_status'
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.after(:suite) do
    ::FileUtils.remove_entry tmp_dir
  end
end
