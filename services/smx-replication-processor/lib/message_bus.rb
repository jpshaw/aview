require 'active_support/notifications'
require 'kafka'
require 'kafka/datadog'

# TODO: Replace this with Kraken::MessageBus gem when published
module MessageBus
  extend self
  include Kraken::Logger.console

  @producers = []

  def produce(data, topic:)
    producer.produce(serialize(data), topic: topic)
  rescue Kafka::BufferOverflow
    logger.warn 'Message bus buffer overflow'
    sleep 1
    retry
  end
  alias_method :publish, :produce

  def producer
    @producer ||= begin
      producer = client.async_producer(
        delivery_threshold: 10,
        delivery_interval: 5,
      )
      @producers << producer
      producer
    end
  end

  def client(brokers = ENV['KAFKA_BROKERS'])
    raise ArgumentError.new('Message bus brokers is empty') if brokers.blank?
    @client ||= Kafka.new(seed_brokers: brokers, logger: logger)
  end

  def shutdown
    @producers.each(&:shutdown)
  end

  private

  def serialize(data)
    case data
    when String
      data
    when Hash, Array
      data.to_json
    else
      data.to_s
    end
  end
end

%w(INT TERM).each do |signal|
  trap signal do
    MessageBus.shutdown
  end
end
