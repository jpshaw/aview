require 'json'
require 'memoist'
require 'active_support/core_ext/hash/keys'
require 'active_support/core_ext/object/blank'
require 'kraken/logger'
require 'message_bus'
require 'smx/replication/types'
require 'smx/replication/server'

module SMX
  module Replication
    extend self

    InvalidDataTypeError = Class.new(StandardError)

    def name_for_type(type)
      case type
      when *gateway_types
        :gateway
      when *internet_route_types
        :internet_route
      when *cascaded_network_types
        :cascaded_network
      else
        raise InvalidDataTypeError.new(type)
      end
    end

    def action_for_type(type)
      case type
      when *create_types
        :create
      when *update_types
        :update
      when *delete_types
        :delete
      else
        raise InvalidDataTypeError.new(type)
      end
    end

    def topic_for_type(type)
      case type
      when *gateway_types
        'aview.smx.inbound'
      when *internet_route_types
        'aview.smx.internet-routes'
      when *cascaded_network_types
        'aview.smx.cascaded-networks'
      else
        raise InvalidDataTypeError.new(type)
      end
    end

    def subscribed_to_type?(type)
      subscribed_types.include?(type)
    end

    def is_gateway_type?(type)
      gateway_types.include?(type)
    end

    def is_internet_route_type?(type)
      internet_route_types.include?(type)
    end

    def is_cascaded_network_type?(type)
      cascaded_network_types.include?(type)
    end

    def is_create_type?(type)
      create_types.include?(type)
    end

    def is_update_type?(type)
      update_types.include?(type)
    end

    def is_delete_type?(type)
      delete_types.include?(type)
    end

    def gateway_types
      Types::GATEWAY
    end

    def internet_route_types
      Types::INTERNET_ROUTE
    end

    def cascaded_network_types
      Types::CASCADED_NETWORK
    end

    def create_types
      Types::CREATE
    end

    def update_types
      Types::UPDATE
    end

    def delete_types
      Types::DELETE
    end

    def subscribed_types
      Types::SUBSCRIBED
    end
  end
end
