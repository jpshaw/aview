require 'smx/replication/attributes'

module SMX
  module Replication
    module Parser
      extend self

      def parse(request)
        data       = request.replication_data.to_s.dup
        type       = request.data_type
        correlator = request.header.correlator

        results = {
          data_type: type,
          correlator: correlator
        }

        Attributes.for_type(type).each do |attribute|
          results[attribute.name] = attribute.value_from_data(data)
        end

        results
      end
    end
  end
end
