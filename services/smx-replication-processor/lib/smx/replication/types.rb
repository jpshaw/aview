module SMX
  module Replication
    module Types
      UPDATE_GATEWAY = '558'.freeze
      UPDATE_GATEWAY_VIA_QUERY = '677'.freeze
      UPDATE_GATEWAY_VIA_LAST_ACCESS = '562'.freeze
      CREATE_INTERNET_ROUTE = '347'.freeze
      UPDATE_INTERNET_ROUTE = '660'.freeze
      DELETE_INTERNET_ROUTE = '859'.freeze
      CREATE_CASCADED_NETWORK = '301'.freeze
      UPDATE_CASCADED_NETWORK = '593'.freeze
      DELETE_CASCADED_NETWORK = '812'.freeze

      GATEWAY = [
        UPDATE_GATEWAY
      ].freeze

      INTERNET_ROUTE = [
        CREATE_INTERNET_ROUTE,
        UPDATE_INTERNET_ROUTE,
        DELETE_INTERNET_ROUTE
      ].freeze

      CASCADED_NETWORK = [
        CREATE_CASCADED_NETWORK,
        UPDATE_CASCADED_NETWORK,
        DELETE_CASCADED_NETWORK
      ].freeze

      CREATE = [
        CREATE_INTERNET_ROUTE,
        CREATE_CASCADED_NETWORK
      ].freeze

      UPDATE = [
        UPDATE_GATEWAY,
        UPDATE_INTERNET_ROUTE,
        UPDATE_CASCADED_NETWORK
      ].freeze

      DELETE = [
        DELETE_INTERNET_ROUTE,
        DELETE_CASCADED_NETWORK
      ].freeze

      SUBSCRIBED = GATEWAY + INTERNET_ROUTE + CASCADED_NETWORK
    end
  end
end
