require 'bindata'

module SMX
  module Replication
    class Header < BinData::Record
      endian :big

      uint16 :total_length
      uint16 :msg_length
      uint16 :outer_env_header
      uint16 :common_header_len
      uint16 :common_header_marker
      uint8 :pu_version_lt_len
      uint8 :pu_version_lt_marker
      uint8 :version
      uint8 :eye_catcher_lt_len, value: -> { eye_catcher.length + 2 }
      uint8 :eye_catcher_lt_marker
      string :eye_catcher, read_length: -> { eye_catcher_lt_len - 2 }
      uint8 :correlator_lt_len
      uint8 :correlator_lt_marker
      uint32 :correlator
    end

    class Request < BinData::Record
      extend Memoist

      endian :big

      header :header # SMX::Replication::Header
      uint16 :control_request_header_len
      uint16 :control_request_header_marker
      uint8 :confirm_processing_len
      uint8 :confirm_processing_marker
      uint8 :confirmation
      uint8 :app_name_len, value: -> { app_name.length + 2}
      uint8 :app_name_marker
      string :app_name, read_length: -> { app_name_len - 2 }
      uint8 :request_type_len
      uint8 :request_type_marker
      uint16 :request_type
      uint16 :request_data_header_len
      uint16 :request_data_header_marker
      uint16 :parm_llid_len
      uint16 :parm_llid_marker
      uint8 :entry_name_len, value: -> { entry_name.length - 2}
      uint8 :entry_name_marker
      string :entry_name, read_length: -> { entry_name_len - 2 }
      uint8 :entry_data_size_len
      uint8 :entry_data_size_marker
      uint32 :data_size
      uint16 :replication_data_llid_len
      uint16 :replication_data_llid_marker
      string :replication_data_type, read_length: 3
      uint8 :comma
      # subtract the 3 replication data type characters (e.g. 558, 720) and the comma
      string :replication_data, read_length: -> { data_size - 4 }
      string :garbage

      def data_type
        replication_data_type
      end

      def entry_info
        entry_code, entry_id, entry_server = entry_name.to_s.split(';')

        {
          code: entry_code,
          id: entry_id,
          server: entry_server
        }
      end
      memoize :entry_info

      def replication_info
        {
          type: data_type,
          server: entry_info[:server],
          correlator: header.correlator,
          entry: "#{entry_info[:code]}/#{entry_info[:id]}"
        }
      end
      memoize :replication_info
    end

    class Response < BinData::Record
      endian :big

      header :header # SMX::Replication::Header
      uint16 :control_response_header_len, value: 0x0020
      uint16 :control_response_header_marker, value: 0xCC01
      uint8 :response_rc_count_lt_len, value: 0x04
      uint8 :response_rc_count_lt_marker, value: 0x02
      uint16 :rc_count, value: 0x0002
      uint8 :rc_lt_len, value: 0x0A
      uint8 :rc_lt_marker, value: 0x03
      uint32 :status_code, value: 0x00000000
      uint32 :return_code, value: 0x00000000
      uint8 :response_type_lt_len, value: 0x04
      uint8 :response_type_lt_marker, value: 0x0E
      uint16 :replication_response, value: 0xAA91
    end
  end
end
