module SMX
  module Replication
    class Record
      include Kraken::Logger.console

      ARMT_ID      = 'ACCELRTD.ACCELRTD'.freeze
      ARMT_ENABLED = 'armtEnabled'.freeze
      DEVICE_ID    = 'deviceId'.freeze
      IS_MODEL     = 'modelInd'.freeze
      CREATOR      = 'creator'.freeze
      UPDATER      = 'updater'.freeze
      TRUE         = 'Y'.freeze
      FALSE        = 'N'.freeze

      attr_reader :data, :type, :correlator, :device_id, :creator, :updater, :action

      def initialize(data)
        @data       = data
        @type       = data[:data_type]
        @correlator = data[:correlator]
        @device_id  = data[DEVICE_ID]
        @creator    = data[CREATOR].to_s.upcase
        @updater    = data[UPDATER].to_s.upcase
        @action     = SMX::Replication.action_for_type(type).to_s.upcase
      end

      def valid?
        if SMX::Replication.is_gateway_type?(type)
          !armt_updater?
        else
          !created_or_updated_by_armt?
        end
      end

      def to_h
        { meta: { action: action }, data: data }
      end

      def to_json
        to_h.to_json
      end

      def is_model?
        data[IS_MODEL] == TRUE
      end

      def armt_enabled?
        data[ARMT_ENABLED] == TRUE
      end

      def created_or_updated_by_armt?
        created_by_armt? || updated_by_armt?
      end

      def created_by_armt?
        SMX::Replication.is_create_type?(type) && armt_creator?
      end

      def updated_by_armt?
        SMX::Replication.is_update_type?(type) && armt_updater?
      end

      def armt_creator?
        creator == ARMT_ID
      end

      def armt_updater?
        updater == ARMT_ID
      end
    end
  end
end
