require 'socket'
require 'smx/replication/worker'

module SMX
  module Replication
    class Server
      include Kraken::Logger.console

      def self.run(*args)
        new(*args).run
      end

      def initialize
        @host   = ENV['SOCKET_HOST'] || '0.0.0.0'
        @port   = (ENV['SOCKET_PORT'] || 36000).to_i
        @socket = ::TCPServer.new(@host, @port)
      end

      def run
        logger.info "Running service in #{RUBY_DESCRIPTION}"
        logger.info "Listening on #{host}:#{port}"
        register_socket_signal_handler

        loop do
          Thread.start(socket.accept) do |client|
            Worker.run(client)
          end
        end

      rescue Errno::ECONNRESET
        logger.error 'Connection reset'
      rescue EOFError, IOError
        logger.error 'Socket closed'
      rescue Errno::EBADF
        logger.error 'Socket terminated'
      rescue => error
        logger.error error
      ensure
        ::MessageBus.shutdown
      end

      private

      attr_reader :socket, :host, :port

      # Prevent the service from locking if a signal is sent before a connection
      # is opened to the socket.
      def register_socket_signal_handler
        %w(INT TERM).each { |signal| trap(signal) { socket.close } }
      end
    end
  end
end
