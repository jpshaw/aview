require 'smx/replication/protocol'
require 'smx/replication/parser'
require 'smx/replication/record'

module SMX
  module Replication
    class Worker
      include Kraken::Logger.console

      def self.run(*args)
        new(*args).run
      end

      def initialize(client)
        @client = client
        @tid = Thread.current.object_id
      end

      def run
        family, port, _, address = client.peeraddr
        logger.info "[#{tid}] Starting worker: #{address}:#{port} (#{family})"

        loop do
          request = read_request_from_client

          if SMX::Replication.subscribed_to_type?(request.data_type)
            record = record_from_request(request)

            if record.valid?
              publish_record_to_message_bus(record)
            end
          end

          write_response_to_client(request.header)
        end

      rescue Errno::ECONNRESET
        logger.error "[#{tid}] Connection reset"
      rescue EOFError, IOError, Errno::EBADF
        logger.error "[#{tid}] Closed"
      rescue => error
        logger.error "[#{tid}] Error:"
        logger.error error
      ensure
        client.close if client
      end

      private

      attr_reader :client, :tid

      def read_request_from_client
        request = Request.new
        request.read(client)
        print_request_info(request)
        request
      end

      def write_response_to_client(header)
        response = Response.new
        response.header = header
        response.header.msg_length = 0x007E
        response.header.total_length = response.num_bytes
        response.write(client)
      end

      def print_request_info(request)
        info = request.replication_info.reduce([]) do |array, (key, value)|
          array << "#{key}=#{value}"
        end.join(', ')

        logger.info "[#{tid}] Replication #{info}"
      end

      def record_from_request(request)
        Record.new(Parser.parse(request))
      end

      def publish_record_to_message_bus(record)
        ::MessageBus.publish(
          record.to_json,
          topic: SMX::Replication.topic_for_type(record.type)
        )

        info = "type=#{record.type}, correlator=#{record.correlator}"
        logger.info "[#{tid}] Published #{info} to message bus"
      end
    end
  end
end
