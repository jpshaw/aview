module SMX
  module Replication
    class Attribute
      attr_reader :name, :format, :size

      def initialize(data = {})
        @name   = data['name']
        @format = data['unpack_format']
        @size   = data['size']
      end

      def value_from_data(data)
        data
          .slice!(0, size)
          .unpack(format)
          .first
          .to_s
          .encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
      end

      def to_h
        { name: name, format: format, size: size }
      end

      def to_json(options = nil)
        to_h.to_json(options)
      end
    end
  end
end
