require 'smx/replication/attribute'

module SMX
  module Replication
    InvalidAttributesFile = Class.new(StandardError)

    module Attributes
      extend self

      def config_dir
        @config_dir ||= File.expand_path('../../../../config', __FILE__)
      end

      def store
        @store ||= {}
      end

      def for_type(type)
        name   = SMX::Replication.name_for_type(type)
        action = SMX::Replication.action_for_type(type)

        store[name] ||= {}
        store[name][action] ||= begin
          file = "#{config_dir}/#{name}_#{action}.json"

          raise InvalidAttributesFile.new(file) unless File.exist?(file)

          attributes = JSON.parse(File.read(file))
          attributes.map do |attribute|
            Attribute.new(attribute)
          end
        end
      end
    end
  end
end
