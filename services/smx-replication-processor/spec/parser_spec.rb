require 'spec_helper'

describe SMX::Replication::Parser do
  subject { described_class }

  describe '.parse' do
    context 'when given gateway data' do
      let(:data)    { file_fixture('gateway_data.txt') }
      let(:type)    { SMX::Replication::Types::GATEWAY.first }
      let(:request) { double('request') }
      let(:results) { subject.parse(request) }

      before do
        allow(request).to receive(:replication_data).and_return(data)
        allow(request).to receive(:data_type).and_return(type)
      end

      it 'sets the first and last attributes' do
        expect(results['deviceId']). to eq '00270423C2C4'
        expect(results['idsKafkaRestver']). to eq 'test'
      end
    end

    context 'when given internet route data' do
      let(:data)    { file_fixture('internet_route_data.txt') }
      let(:type)    { SMX::Replication::Types::INTERNET_ROUTE.first }
      let(:request) { double('request') }
      let(:results) { subject.parse(request) }

      before do
        allow(request).to receive(:replication_data).and_return(data)
        allow(request).to receive(:data_type).and_return(type)
      end

      it 'sets the first and last attributes' do
        expect(results['deviceId']). to eq '00270423C2C4'
        expect(results['ipAddressPrefix']). to eq '128'
      end
    end

    context 'when given cascaded network data' do
      let(:data)    { file_fixture('cascaded_network_data.txt') }
      let(:type)    { SMX::Replication::Types::CASCADED_NETWORK.first }
      let(:request) { double('request') }
      let(:results) { subject.parse(request) }

      before do
        allow(request).to receive(:replication_data).and_return(data)
        allow(request).to receive(:data_type).and_return(type)
      end

      it 'sets the first and last attributes' do
        expect(results['deviceId']). to eq '00270423C2C4'
        expect(results['ipv6RouterAddr']). to eq 'df25:e123:689d:8d36:6d26:fa15:5f5d:2058'
      end
    end
  end
end
