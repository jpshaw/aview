require 'spec_helper'

describe SMX::Replication::Attributes do
  subject { described_class }

  describe '.for_type' do
    let(:results) { subject.for_type(type).map(&:to_h) }

    context 'when given a gateway type' do
      let(:type) { SMX::Replication::Types::UPDATE_GATEWAY }

      it 'returns attributes for update gateway' do
        expect(results).to eq json_fixture('update_gateway')
      end
    end

    context 'when given an internet route type' do
      context 'for create' do
        let(:type) { SMX::Replication::Types::CREATE_INTERNET_ROUTE }

        it 'returns attributes for create internet route' do
          expect(results).to eq json_fixture('create_internet_route')
        end
      end

      context 'for update' do
        let(:type) { SMX::Replication::Types::UPDATE_INTERNET_ROUTE }

        it 'returns attributes for update internet route' do
          expect(results).to eq json_fixture('update_internet_route')
        end
      end

      context 'for delete' do
        let(:type) { SMX::Replication::Types::DELETE_INTERNET_ROUTE }

        it 'returns attributes for delete internet route' do
          expect(results).to eq json_fixture('delete_internet_route')
        end
      end
    end

    context 'when given a cascaded network type' do
      context 'for create' do
        let(:type) { SMX::Replication::Types::CREATE_CASCADED_NETWORK }

        it 'returns attributes for create cascaded network' do
          expect(results).to eq json_fixture('create_cascaded_network')
        end
      end

      context 'for update' do
        let(:type) { SMX::Replication::Types::UPDATE_CASCADED_NETWORK }

        it 'returns attributes for update cascaded network' do
          expect(results).to eq json_fixture('update_cascaded_network')
        end
      end

      context 'for delete' do
        let(:type) { SMX::Replication::Types::DELETE_CASCADED_NETWORK }

        it 'returns attributes for delete cascaded network' do
          expect(results).to eq json_fixture('delete_cascaded_network')
        end
      end
    end

    context 'when given an invalid type' do
      it 'throws an `InvalidDataTypeError` error' do
        expect {
          subject.for_type('invalid')
        }.to raise_error(SMX::Replication::InvalidDataTypeError)
      end
    end
  end
end
