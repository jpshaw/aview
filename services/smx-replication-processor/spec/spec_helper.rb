$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)

require 'smx_replication'
require 'json'

def spec_path
  @spec_path ||= File.join(File.dirname(__FILE__))
end

def fixtures_path
  @fixtures_path ||= "#{spec_path}/fixtures"
end

def fixture(name)
  File.new(fixtures_path + '/' + name, 'r')
end

def json_fixture(name)
  result = JSON.parse(fixture(name + '.json').read)

  if result.is_a? Hash
    result.symbolize_keys
  else
    result.map(&:symbolize_keys)
  end
end

def file_fixture(name)
  fixture(name).read
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
