require 'spec_helper'

describe 'Parser' do
  context 'when given client type data' do
    context 'when given total port' do
      let(:inbound_data) { json_fixture('client_total_metric.json') }
      let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
      let(:result) { subject.execute }
      let(:expected_results) { {
        "timestamp":"2017-08-25T14:06:24+00:00",
        "mac":"0027042C8F5B",
        "server":nil,
        "usage":303588,
        "client":"goober.accelecon.com",
        "type":"client"
      } }

      it 'transforms the data in the format needed for storage' do
        expect(result).to eq expected_results
      end
    end

    context 'when given Other port' do
      let(:inbound_data)  { json_fixture('client_other_metric.json') }
      let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
      let(:result) { subject.execute }

      it 'returns nil' do
        expect(result).to eq nil
      end
    end
  end

  context 'when given server type data' do
    context 'when given data with usage' do
      let(:inbound_data)  { json_fixture('server_metric.json') }
      let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
      let(:result) { subject.execute }
      let(:expected_results) { {
        "timestamp":"2017-08-25T14:06:24+00:00",
        "mac":"0027042C8F5B",
        "server":"Facebook",
        "usage":152794,
        "client":"laptop",
        "type":"server"
      } }

      it 'transforms the data in the format needed for storage' do
        expect(result).to eq expected_results
      end
    end

    context 'when given data without any usage' do
      let(:inbound_data)  { json_fixture('server_zero_usage_metric.json') }
      let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
      let(:result) { subject.execute }

      it 'returns nil' do
        expect(result).to eq nil
      end
    end

    context 'when the server name does not exist' do
      let(:inbound_data)  { json_fixture('server_empty_name_metric.json') }
      let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
      let(:result) { subject.execute }

      it 'the address is used for the client field' do
        expect(result[:client]).to eq '172.16.0.20'
      end
    end

    context 'when given two server records when the usage has reset' do
      let(:inbound_data) { json_fixture('server_metric.json') }
      let(:inbound_data_after_reset) { json_fixture('server_metric_after_reset.json') }

      before do
        IntelliflowProcessor::Parser.new(inbound_data).execute
      end

      it 'returns the reset usage' do
        result = IntelliflowProcessor::Parser.new(inbound_data_after_reset).execute
        expect(result[:usage]).to eq 35
      end
    end
  end

  context 'when given ethernet type data' do
    let(:inbound_data)  { json_fixture('ethernet_metric.json') }
    let(:subject) { IntelliflowProcessor::Parser.new(inbound_data) }
    let(:result) { subject.execute }

    it 'returns nil' do
      expect(result).to eq nil
    end
  end
end
