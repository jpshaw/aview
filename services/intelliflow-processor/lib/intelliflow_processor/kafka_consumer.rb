require 'intelliflow_logger'
require 'base64'

module IntelliflowProcessor
  class KafkaConsumer
    attr_reader :consumer, :producer

    def initialize
      @producer = KafkaProducer.new
      @brokers  = Configuration.kafka_brokers
      @group_id = Configuration.kafka_group_id
      @topic = Configuration.kafka_inbound_topic
      init_consumer
    end

    def start
      IntelliflowLogger.info 'Starting processor ...'

      consumer_loop
    end

    def stop
      IntelliflowLogger.info 'Stopping processor ...'

      @consumer.stop

      IntelliflowLogger.info 'Stopped processor'
    end

    private

    def init_consumer
      @consumer = Kafka.new(seed_brokers: @brokers).consumer(group_id: @group_id)
      @consumer.subscribe(@topic)
    end

    def consumer_loop
      IntelliflowLogger.info 'Started consumer loop ...'

      @consumer.each_message do |message|
        process_message(message)
      end

      IntelliflowLogger.info 'Stopped consumer loop'
    rescue => exception
      IntelliflowLogger.error exception
    end

    private

    def process_message(message)
      JSON.parse(unzipped_message(message.value)).each do |metric|
        processed_message = Parser.new(metric).execute
        @producer.publish(processed_message) unless processed_message.nil?
      end
    rescue => exception
      IntelliflowLogger.error "failed to process message: #{message.value}"
      IntelliflowLogger.error exception
    end

    def unzipped_message(zipped_message)
      string_io = StringIO.new(zipped_message)
      gzip_reader = Zlib::GzipReader.new(string_io)
      gzip_reader.read()
    end
  end
end
