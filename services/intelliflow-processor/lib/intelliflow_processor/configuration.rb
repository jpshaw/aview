module IntelliflowProcessor
  module Configuration
    class << self
      def kafka_brokers
        ENV['KAFKA_BROKERS'] || 'localhost:9092'
      end

      def kafka_group_id
        ENV['KAFKA_GROUP_ID'] || 'intelliflow-processor'
      end

      def kafka_client_id
        ENV['KAFKA_CLIENT_ID'] || 'intelliflow'
      end

      def kafka_inbound_topic
        ENV['KAFKA_INTELLIFLOW_INBOUND_TOPIC'] || 'accns.test.aview.devices.metric'
      end

      def kafka_outbound_topic
        ENV['KAFKA_INTELLIFLOW_OUTBOUND_TOPIC'] || 'accns.staging.aview.events.metrics.intelliflow'
      end

      def kafka_delivery_threshold
        threshold = ENV['KAFKA_DELIVERY_THRESHOLD'].to_i
        threshold > 0 ? threshold : 10 # Messages
      end

      def kafka_delivery_interval
        interval = ENV['KAFKA_DELIVERY_INTERVAL'].to_i
        interval > 0 ? interval : 5 # Seconds
      end
    end
  end
end
