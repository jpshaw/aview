module IntelliflowProcessor
  class KafkaProducer
    def initialize
      @client = Kafka.new(
        seed_brokers: Configuration.kafka_brokers,
        client_id: Configuration.kafka_client_id
      )
      @producer = @client.async_producer(
        delivery_threshold: Configuration.kafka_delivery_threshold,
        delivery_interval: Configuration.kafka_delivery_interval
      )
      @topic = Configuration.kafka_outbound_topic
    end

    def publish(message)
      @producer.produce(message.to_json, topic: @topic)
    rescue Kafka::BufferOverflow
      IntelliflowLogger.error "Buffer overflow, backing off for 50ms"
      sleep 0.05
      retry
    end

    def shutdown
      @producer.shutdown if @producer
    end
  end
end
