require 'intelliflow_logger'
require 'date'

module IntelliflowProcessor
  class Parser
    attr_reader :data

    def initialize(inbound_data)
      @data = inbound_data
    end

    def execute
      return unless allowed_metric?
      formatted_metric[:usage] > 0 ? formatted_metric : nil
    end

    private

    def allowed_metric?
      if metric_type == 'client'
        data['port'] == 'total' && metric_usage > 0
      else
        metric_type == 'server' && metric_usage > 0
      end
    end

    def formatted_metric
      @formatted_metric ||= {
        timestamp: metric_timestamp,
        mac: metric_mac,
        server: metric_server,
        usage: metric_usage,
        client: metric_client,
        type: metric_type
      }
    end

    def metric_type
      data['type']
    end

    def metric_timestamp
      @metric_timestamp ||= Time.at(data['time']).utc.to_datetime.to_s
    end

    def metric_mac
      @metric_mac ||= data['host'].gsub(':', '').upcase
    end

    def metric_server
      data['server']
    end

    def metric_client
      @metric_client ||= data['name'].to_s.empty? ? data['address'] : data['name']
    end

    def metric_usage
      @metric_usage ||= data['values'].inject(0){ |sum, x| sum + x }
    end
  end
end
