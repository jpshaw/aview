# Intelliflow Processor

This consumes Intelliflow data from Kafka that was put there by devices,
mutates that data, then puts it on another Kafka topic to be consumed
by the Druid database.

Because the device sends us total usage numbers and resets to zero on a
restart, we store the totals in a MySQL database. MySQL was chosen because
of its ease of use and availability in our environments. Saving the totals
allows us to compare new flows to the stored flows and acquire the delta
to give to Druid.

## Run the processor

The processor reads and writes to the same instance of Kafka.
Configuration is done via environment variables and defaults can be found
in the `Configuration` ruby file. Start the processor with the helper script:

```bash
$ ./bin/processor
```

Kill the processor by sending it a `SIGINT` (pressing CTRL-C).

## Test

Run the tests with rake our rspec:

```bash
$ rake
...
or
...
$ rspec
```
