$stdout.sync = true

# Add `services/lib` to load path
#
# Be careful not to name anything in `services/lib` that is already declared in
# the rails application. For example, adding and requiring
# `services/lib/geocoder.rb` will override the `geocoder` gem.
lib = File.expand_path('../lib', __FILE__)
$:.unshift(lib) unless $:.include?(lib)

# Make gems from the rails app available for services to require
require 'rubygems'
require 'bundler/setup'

# Load minimal service dependencies
require 'service'
