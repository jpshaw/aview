package com.accelerated.voltage_service;

import com.voltage.toolkit.ToolkitException;

public interface CryptoLibrary {
  String decryptElement(String encryptedText) throws Exception;

  String encryptElement(String plainText) throws Exception;

  byte[] Base64Decode(byte[] textBytes) throws ToolkitException;

  byte[] Base64Encode(byte[] textBytes) throws ToolkitException;

  byte[] Base64EncodeNoNewLine(byte[] textBytes) throws ToolkitException;
}
