package com.accelerated.voltage_service;

import java.io.UnsupportedEncodingException;

public class CryptoToolkit {
  private CryptoLibrary library;
  private static final String ENCODING = "UTF-8";

  public CryptoToolkit(CryptoLibrary library) {
    this.library = library;
  }

  public String decryptBusinessCenterToken(String encryptedText) throws Exception {
    String decodedText = decodeByBase64(encryptedText);
    String decryptedText = library.decryptElement(decodedText);
    String formattedText = decryptedText.replace(':', ' ').trim();
    byte[] decodedTextBytes = library.Base64Decode(formattedText.getBytes());
    return new String(decodedTextBytes);
  }

  public String encryptBusinessCenterToken(String plainText) throws Exception {
    byte[] encodedTextBytes = library.Base64Encode(plainText.getBytes());
    String formattedText = new String(encodedTextBytes).replace(' ', ':');
    String encryptedText = library.encryptElement(formattedText);
    return encodeByBase64(encryptedText);
  }

  private String decodeByBase64(String urlEncodedBase64String) throws Exception {
    String base64String = unHashSpecialChar(urlEncodedBase64String);

    String output;
    byte[] b_strd;
    byte[] b64_b_strd;
    try {
      b64_b_strd = base64String.getBytes();
      b_strd = library.Base64Decode(b64_b_strd);
      output = new String(b_strd);
    } catch (Exception e) {
      //failsafe - retry without url decoding
      b64_b_strd = urlEncodedBase64String.getBytes();
      b_strd = library.Base64Decode(b64_b_strd);
      output = new String(b_strd);
    }
    return output;
  }

  private String unHashSpecialChar(String hashedString) throws UnsupportedEncodingException {
    return java.net.URLDecoder.decode(hashedString, ENCODING);
  }

  private String encodeByBase64(String plainText) throws Exception {
    byte[] plainTextBytes = plainText.getBytes();
    byte[] encodedPlainTextBytes = library.Base64EncodeNoNewLine(plainTextBytes);
    String encodedPlainText = new String(encodedPlainTextBytes);
    return hashSpecialChar(encodedPlainText);
  }

  private String hashSpecialChar(String hashedString) throws UnsupportedEncodingException {
    return java.net.URLEncoder.encode(hashedString, ENCODING);
  }
}