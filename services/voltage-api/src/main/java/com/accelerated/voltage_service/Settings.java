package com.accelerated.voltage_service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Settings {
  final Logger logger = LoggerFactory.getLogger(Settings.class);

  private static Settings settings;
  private String settingsFileLocation;
  private JsonObject jsonObject;

  Settings() {
    settingsFileLocation = System.getProperty("voltage.settings.file");
    try {
      parseSettings();
    } catch (Exception e) {
      logger.error(e.toString());
    }
  }

  public static Settings getInstance() {
    if (settings != null) return settings;
    settings = new Settings();
    return settings;
  }

  public void reload() throws Exception {
    parseSettings();
  }

  public JsonObject getLibrary() {
    return jsonObject.getAsJsonObject("library");
  }

  public JsonObject getCrypto() {
    return jsonObject.getAsJsonObject("crypto");
  }

  public JsonObject getService() {
    return jsonObject.getAsJsonObject("service");
  }

  private void parseSettings() throws Exception {
    try {
      FileReader settingsFile = new FileReader(settingsFileLocation);
      jsonObject = new JsonParser().parse(settingsFile).getAsJsonObject();
    } catch (FileNotFoundException e) {
      throw new Exception("Settings file not found", e);
    } catch (NullPointerException e) {
      throw new Exception("System property not set: voltage.settings.file", e);
    } catch (JsonSyntaxException e) {
      throw new Exception("Settings file not in JSON format", e);
    }
  }
}
