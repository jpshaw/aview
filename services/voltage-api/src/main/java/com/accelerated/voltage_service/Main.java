package com.accelerated.voltage_service;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;

public class Main {
  final static Logger logger = LoggerFactory.getLogger(VoltageLibrary.class);

  public static void main(String[] args) {
    configureServer();

    CryptoLibrary library = new VoltageLibrary();
    CryptoToolkit toolkit = new CryptoToolkit(library);

    get("/decrypt", (request, response) -> {
      response.type("application/json");
      try {
        String decryptedText = toolkit.decryptBusinessCenterToken(request.queryParams("p"));
        return getSimpleJson("decryptedText", decryptedText);
      } catch (Exception e) {
        response.status(500);
        return getSimpleJson("exception", e.getMessage());
      }
    });

    get("/encrypt", (request, response) -> {
      response.type("application/json");
      try {
        String encryptedText = toolkit.encryptBusinessCenterToken(request.queryParams("p"));
        return getSimpleJson("encryptedText", encryptedText);
      } catch (Exception e) {
        response.status(500);
        return getSimpleJson("exception", e.getMessage());
      }
    });

    get("/reload", (request, response) -> {
      response.type("application/json");
      try {
        Settings.getInstance().reload();
        return getSimpleJson("success", "reloaded settings");
      } catch (Exception e) {
        response.status(500);
        return getSimpleJson("exception", e.getMessage());
      }
    });
  }

  private static void configureServer() {
    port(Settings.getInstance().getService().get("port").getAsInt());

    exception(Exception.class, (exception, request, response) -> {
      logger.error("internal server error - request: " + completeRequest(request));
      exception.printStackTrace();
      response.status(500);
      response.type("application/json");
      response.body(getSimpleJson("error", "Oops, there was an unexpected problem on the server."));
    });
  }

  private static String getSimpleJson(String key, String value) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty(key, value);
    return jsonObject.toString();
  }

  private static String completeRequest(spark.Request request) {
    StringBuilder builder = new StringBuilder(request.pathInfo() + "?");
    for (String param : request.queryParams()) {
      builder.append(param + "=" + request.queryParams(param) + "&");
    }
    String result = builder.toString();
    return result.substring(0, result.length() - 1);
  }
}