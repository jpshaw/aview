package com.accelerated.voltage_service;

import com.voltage.toolkit.LibraryContext;
import com.voltage.toolkit.ToolkitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VoltageLibrary implements CryptoLibrary {
  final Logger logger = LoggerFactory.getLogger(VoltageLibrary.class);
  private LibraryContext library;
  private Settings settings;

  public VoltageLibrary() {
    settings = Settings.getInstance();
    initializeLibrary();
  }

  @Override
  public String decryptElement(String encryptedText) throws Exception {
    return library.FPEAccess(settings.getCrypto().get("identity").getAsString(),
        LibraryContext.getFPE_FORMAT_CUSTOM(),
        settings.getCrypto().get("format").getAsString(),
        LibraryContext.getAUTH_METHOD_USER_PASSWORD(),
        settings.getCrypto().get("authInfo").getAsString(),
        encryptedText);
  }

  @Override
  public String encryptElement(String plainText) throws Exception {
    return library.FPEProtect(settings.getCrypto().get("identity").getAsString(),
        LibraryContext.getFPE_FORMAT_CUSTOM(),
        settings.getCrypto().get("format").getAsString(),
        LibraryContext.getAUTH_METHOD_USER_PASSWORD(),
        settings.getCrypto().get("authInfo").getAsString(),
        plainText);
  }

  @Override
  public byte[] Base64Decode(byte[] textBytes) throws ToolkitException {
    return library.Base64Decode(0, textBytes);
  }

  @Override
  public byte[] Base64Encode(byte[] textBytes) throws ToolkitException {
    return library.Base64Encode(0, textBytes);
  }

  @Override
  public byte[] Base64EncodeNoNewLine(byte[] textBytes) throws ToolkitException {
    return library.Base64Encode(LibraryContext.BASE64_NO_NEW_LINE, textBytes);
  }

  private void initializeLibrary() {
    System.load(settings.getService().get("voltageLibraryFile").getAsString());
    logger.info("successfully loaded library");

    try {
      library = new LibraryContext(settings.getLibrary().get("flags").getAsInt(),
          settings.getLibrary().get("characterSet").getAsInt(),
          settings.getLibrary().get("keyServerUrl").getAsString(),
          settings.getLibrary().get("cachePath").getAsString(),
          settings.getLibrary().get("storePassword").getAsString(),
          settings.getLibrary().get("trustStorePath").getAsString());
      logger.info("successfully initialized library context");
    } catch (ToolkitException e) {
      logger.error(e.getMessage(), e);
    }
  }
}