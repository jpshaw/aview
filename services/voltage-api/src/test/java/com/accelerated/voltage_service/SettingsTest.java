package com.accelerated.voltage_service;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;

import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Enclosed.class)
public class SettingsTest {
  static final String SETTINGS_FILE = "src/test/fixtures/settings.json";

  public static class GetInstance {
    Appender mockLogAppender;

    @Before
    public void setupTestLogger() {
      mockLogAppender = mock(Appender.class);
      Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
      when(mockLogAppender.getName()).thenReturn("MOCK");
      root.addAppender(mockLogAppender);
    }

    @After
    public void cleanupTestLogger() {
      Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
      root.detachAppender(mockLogAppender);
      mockLogAppender.stop();
      System.clearProperty("voltage.settings.file");
    }

    @Test
    public void itLoadsTheSettingsFileGivenAsAJavaPropertyWithoutThrowingAnException() throws Exception {
      System.setProperty("voltage.settings.file", SETTINGS_FILE);
      new Settings();
    }

    @Test
    public void itLogsAnErrorWhenNoFileIsGivenToSystemProperties() throws Exception {
      new Settings();
      String expectedLog = "System property not set: voltage.settings.file";
      verify(mockLogAppender).doAppend(argThat(event ->
          ((LoggingEvent) event).getFormattedMessage().contains(expectedLog)));
    }

    @Test
    public void itLogsAnErrorWhenTheGivenFileDoesNotExist() throws Exception {
      System.setProperty("voltage.settings.file", "does_not_exist.json");
      new Settings();
      String expectedLog = "Settings file not found";
      verify(mockLogAppender).doAppend(argThat(event ->
          ((LoggingEvent) event).getFormattedMessage().contains(expectedLog)));
    }

    @Test
    public void itLogsAnErrorWhenTheGivenFileIsNotInJsonFormat() throws Exception {
      System.setProperty("voltage.settings.file", "src/test/fixtures/settings.yml");
      new Settings();
      String expectedLog = "Settings file not in JSON format";
      verify(mockLogAppender).doAppend(argThat(event ->
          ((LoggingEvent) event).getFormattedMessage().contains(expectedLog)));
    }
  }

  public static class GetLibrary {
    Settings subject;

    @Before
    public void setUp() throws Exception {
      System.setProperty("voltage.settings.file", SETTINGS_FILE);
      subject = new Settings();
    }

    @Test
    public void itEnablesRetrievalOfTheKeyServerUrl() throws Exception {
      String expected = "https://voltage-pp-0000.dataprotection.voltage.com/policy/clientPolicy.xml";
      String result = subject.getLibrary().get("keyServerUrl").getAsString();
      assertEquals(expected, result);
    }
  }

  public static class GetCrypto {
    Settings subject;

    @Before
    public void setUp() throws Exception {
      System.setProperty("voltage.settings.file", SETTINGS_FILE);
      subject = new Settings();
    }

    @Test
    public void itEnablesRetrievalOfTheAuthInfo() throws Exception {
      String expected = "username:password";
      String result = subject.getCrypto().get("authInfo").getAsString();
      assertEquals(expected, result);
    }
  }

  public static class GetService {
    Settings subject;

    @Before
    public void setUp() throws Exception {
      System.setProperty("voltage.settings.file", SETTINGS_FILE);
      subject = new Settings();
    }

    @Test
    public void itEnablesRetrievalOfThePort() throws Exception {
      String expected = "8080";
      String result = subject.getService().get("port").getAsString();
      assertEquals(expected, result);
    }
  }

  public static class Reload {
    static final String NEW_SETTINGS_FILE = "src/test/fixtures/settings.json.new";
    Settings subject;

    @Before
    public void setup() throws Exception {
      System.setProperty("voltage.settings.file", SETTINGS_FILE);
      subject = new Settings();
      String originalValue = subject.getCrypto().get("authInfo").getAsString();
      assertEquals("username:password", originalValue);
      editSettingsFile();
    }

    @After
    public void cleanup() throws Exception {
      resetSettingsFile();
    }

    @Test
    public void itReloadsTheSettingsFromTheGivenSettingsFile() throws Exception {
      subject.reload();
      String newValue = subject.getCrypto().get("authInfo").getAsString();
      assertEquals("username2:password2", newValue);
    }

    private void editSettingsFile() throws Exception {
      File oldSettings = new File(SETTINGS_FILE);
      File oldSettingsBackup = new File(SETTINGS_FILE + ".bak");
      oldSettings.renameTo(oldSettingsBackup);
      File newSettings = new File(NEW_SETTINGS_FILE);
      newSettings.renameTo(oldSettings);
    }

    private void resetSettingsFile() {
      File oldSettings = new File(SETTINGS_FILE);
      File newSettings = new File(NEW_SETTINGS_FILE);
      oldSettings.renameTo(newSettings);
      File oldSettingsBackup = new File(SETTINGS_FILE + ".bak");
      oldSettingsBackup.renameTo(oldSettings);
    }
  }
}