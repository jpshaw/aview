package com.accelerated.voltage_service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (Enclosed.class)
public class CryptoToolkitTest {
  public static class encryptBusinessCenterToken {
    CryptoToolkit subject;
    CryptoLibrary mockLibrary;

    @Before
    public void setup() {
      mockLibrary = mock(CryptoLibrary.class);
      subject = new CryptoToolkit(mockLibrary);
    }

    @Test
    public void itEncryptsTheGivenPlainText() throws Exception {
      String plainText = "smAccountID=ATTDEMO&smUserID=ACCEL6&mac=00D0CF1AFE80&timestamp=2017-02-01T20:47:35Z";
      String encryptedText = "Z#x'M?BS>@5jUrk&8m`ZwiQOvOoI#yFhy[{\\2FZ?R&\"$yK%UNj0\\<K1}ltvBIFD{\n" +
          "sfGmF\\PCcFW_BQW@LoSTat{e@x_vY$(]zMn:rp5[n{#P|a`)\n";

      when(mockLibrary.Base64Encode(plainText.getBytes())).thenReturn(new byte[] {});
      when(mockLibrary.encryptElement(isA(String.class))).thenReturn(encryptedText);
      when(mockLibrary.Base64EncodeNoNewLine(encryptedText.getBytes())).thenReturn(new byte[] {87, 105, 78, 52, 74, 48, 48, 47, 81, 108, 77, 43, 81, 68, 86, 113, 86, 88, 74, 114, 74, 106, 104, 116, 89, 70, 112, 51, 97, 86, 70, 80, 100, 107, 57, 118, 83, 83, 78, 53, 82, 109, 104, 53, 87, 51, 116, 99, 77, 107, 90, 97, 80, 49, 73, 109, 73, 105, 82, 53, 83, 121, 86, 86, 84, 109, 111, 119, 88, 68, 120, 76, 77, 88, 49, 115, 100, 72, 90, 67, 83, 85, 90, 69, 101, 119, 48, 75, 99, 50, 90, 72, 98, 85, 90, 99, 85, 69, 78, 106, 82, 108, 100, 102, 81, 108, 70, 88, 81, 69, 120, 118, 85, 49, 82, 104, 100, 72, 116, 108, 81, 72, 104, 102, 100, 108, 107, 107, 75, 70, 49, 54, 84, 87, 52, 54, 99, 110, 65, 49, 87, 50, 53, 55, 73, 49, 66, 56, 89, 87, 65, 112, 68, 81, 111, 61});

      String result = subject.encryptBusinessCenterToken(plainText);
      String expected = "WiN4J00%2FQlM%2BQDVqVXJrJjhtYFp3aVFPdk9vSSN5Rmh5W3tcMkZaP1ImIiR5SyVVTmowXDxLMX1sdHZCSUZEew0Kc2ZHbUZcUENjRldfQlFXQExvU1RhdHtlQHhfdlkkKF16TW46cnA1W257I1B8YWApDQo%3D";
      assertEquals(expected, result);
    }
  }
}
