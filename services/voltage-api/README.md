# Voltage Service

This Voltage Service is our intermediary to the AT&T Voltage crypto service. Due to it's dependence
on the `libvibesmiplejava.so` linux library, it currently only runs in a Linux environment.

## Build

The build process is managed my Maven.

```
$ mvn package

[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building voltage_service 1.0
[INFO] ------------------------------------------------------------------------

...

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 6.576 s
[INFO] Finished at: 2017-01-18T14:28:34-05:00
[INFO] Final Memory: 26M/301M
[INFO] ------------------------------------------------------------------------
```

This will create a `target` directory that contains the voltage service jar with dependencies.

## Run

This service requires a few files and directories to be in place in order to run successfully.
The settings file is used to set the paths of these dependencies. When the dependencies are in
place, the jar can be ran but needs to be given the location of the settings file as a java option.

```
$ java -Dvoltage.settings.file=/source/voltage-service/config/settings.json -jar voltage_service-1.0-jar-with-dependencies.jar
```

The settings file should look similar to this:

```
{
  "service": {
    "port": 8080,
    "voltageLibraryFile": "/voltage/libvibesimplejava.so"
  },
  "library": {
    "flags": 0,
    "characterSet": 0,
    "keyServerUrl": "https://voltage-pp-0000.dataprotection.voltage.com/policy/clientPolicy.xml",
    "storePassword": "",
    "cachePath": "/voltage/cache",
    "trustStorePath": "/voltage/trustStore"
  },
  "crypto": {
    "identity": "accounts22@dataprotection.voltage.com",
    "format": "formatName=B64_GENERIC",
    "authMethod": "UserPassword",
    "authInfo": "username:password"
  }
}
```

## Routes

GET `decrypt` requires a single parameter: `p`

```
http://localhost:38080/decrypt?p=TTpaIzNrelBtM1YofTE0OnVrSzRuZy17WFBjaylES0JcdXBJXnt3LnNcRTpjQ1NnLjZtWlV0IU5WYHIwRSYnZ0Z4Ok9nJi1ZfTNnfH00bVInQT9ra15HREE%2FKW8xOWE2N1VbSlIpdlg4elRyV1N7Xw%3D%3D
```

Decrypts the given string and returns the unencrypted result in JSON format:

```
{"decryptedText":"smAccountID=XXXX&smUserID=XXXX&mac=002704243DCB&timestamp=2017-01-19-11:25:37"}
```

GET `encrypt` requires a single parameter: `p`

```
http://localhost:38080/encrypt?p=smAccountID%3DXXXX%26smUserID%3DXXXX%26mac%3D00D0CF1AFE80%26timestamp%3D2017-02-01T20%3A47%3A35Z
```

Encrypts the given string and returns the encrypted result in JSON format:

```
{"encryptedText":"ODxBP01FbEZZWXRBOVgrImojIi1IOCI%2BdSlLQy5JNnQ0eWBXUGUqcn1fQC1pWydUNUxld0xxYiUxKmpPeGh4Uw0KOWgxNSRrOFAmREFvJSspOWhpYFZUYCp9RjFaYjYmTDc2aVBofXF4ew0K"}
```

GET `reload`

```
http://localhost:38080/reload
```

Reloads the crypto authentication settings from the config file given at startup. It returns a success or failure message in JSON format:

```
{"message":"Successfully loaded settings"}
```