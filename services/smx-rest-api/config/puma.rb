app_path = File.expand_path(__dir__ + '/..')
threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }

directory   "#{app_path}"
rackup      "#{app_path}/config.ru"
environment ENV.fetch("RAILS_ENV") { "development" }
port        ENV.fetch("PORT") { 3000 }
threads     threads_count, threads_count
daemonize   false
