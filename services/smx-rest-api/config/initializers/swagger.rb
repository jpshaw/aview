GrapeSwaggerRails.options.url           = '/v1/swagger_doc'
GrapeSwaggerRails.options.app_url       = 'http://localhost:3000'
GrapeSwaggerRails.options.doc_expansion = 'full'
