# ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdInternetRoutes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Object** | The internet route data |
**principal_id** | **String** | The principal ID | [optional]
