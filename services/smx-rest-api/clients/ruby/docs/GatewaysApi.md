# ServiceManagerRest::GatewaysApi

All URIs are relative to *https://example.orgv1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_gateway_device**](GatewaysApi.md#get_gateway_device) | **GET** /accounts/{account_id}/gateways/{device_id} |
[**update_gateway_device**](GatewaysApi.md#update_gateway_device) | **PUT** /accounts/{account_id}/gateways/{device_id} |


# **get_gateway_device**
> Envelope get_gateway_device(account_id, device_id)



Get a single gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::GatewaysApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID


begin
  result = api_instance.get_gateway_device(account_id, device_id)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling GatewaysApi->get_gateway_device: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |

### Return type

[**Envelope**](Envelope.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_gateway_device**
> SuccessResult update_gateway_device(account_id, device_id, body)



Update an existing gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::GatewaysApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

body = ServiceManagerRest::Device.new # Device |


begin
  result = api_instance.update_gateway_device(account_id, device_id, body)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling GatewaysApi->update_gateway_device: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **body** | [**Device**](Device.md)|  |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json
