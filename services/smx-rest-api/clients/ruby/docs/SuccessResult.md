# ServiceManagerRest::SuccessResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | HTTP Reponse Message | [default to &quot;Success&quot;]
**code** | **Integer** | HTTP Reponse code | [default to 200]
