# ServiceManagerRest::CascadedNetworksApi

All URIs are relative to *https://example.orgv1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_cascaded_network**](CascadedNetworksApi.md#create_cascaded_network) | **POST** /accounts/{account_id}/gateways/{device_id}/cascaded_networks |
[**delete_cascaded_network**](CascadedNetworksApi.md#delete_cascaded_network) | **DELETE** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |
[**get_cascaded_network**](CascadedNetworksApi.md#get_cascaded_network) | **GET** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |
[**get_cascaded_networks**](CascadedNetworksApi.md#get_cascaded_networks) | **GET** /accounts/{account_id}/gateways/{device_id}/cascaded_networks |
[**update_cascaded_network**](CascadedNetworksApi.md#update_cascaded_network) | **PUT** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |


# **create_cascaded_network**
> SuccessResult create_cascaded_network(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks)



Create a cascaded network for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

accounts_account_id_gateways_device_id_cascaded_networks = ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks.new # PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks |


begin
  result = api_instance.create_cascaded_network(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->create_cascaded_network: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **accounts_account_id_gateways_device_id_cascaded_networks** | [**PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks**](PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks.md)|  |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_cascaded_network**
> SuccessResult delete_cascaded_network(account_id, device_id, ip_address)



Delete a cascaded network for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The cascaded network IP Address


begin
  result = api_instance.delete_cascaded_network(account_id, device_id, ip_address)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->delete_cascaded_network: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The cascaded network IP Address |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_cascaded_network**
> CascadedNetworkEnvelope get_cascaded_network(account_id, device_id, ip_address)



Get a cascaded network for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The cascaded network IP Address


begin
  result = api_instance.get_cascaded_network(account_id, device_id, ip_address)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->get_cascaded_network: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The cascaded network IP Address |

### Return type

[**CascadedNetworkEnvelope**](CascadedNetworkEnvelope.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_cascaded_networks**
> CascadedNetworksEnvelope get_cascaded_networks(account_id, device_id)



Get all of the cascaded networks for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID


begin
  result = api_instance.get_cascaded_networks(account_id, device_id)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->get_cascaded_networks: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |

### Return type

[**CascadedNetworksEnvelope**](CascadedNetworksEnvelope.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_cascaded_network**
> SuccessResult update_cascaded_network(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_cascaded_networks)



Update a cascaded network for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The cascaded network IP Address

accounts_account_id_gateways_device_id_cascaded_networks = ServiceManagerRest::DeviceDeviceIdCascadedNetworks.new # DeviceDeviceIdCascadedNetworks |


begin
  result = api_instance.update_cascaded_network(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_cascaded_networks)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->update_cascaded_network: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The cascaded network IP Address |
 **accounts_account_id_gateways_device_id_cascaded_networks** | [**DeviceDeviceIdCascadedNetworks**](DeviceDeviceIdCascadedNetworks.md)|  |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json
