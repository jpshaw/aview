# ServiceManagerRest::DeviceDeviceIdCascadedNetworks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Object** | The cascaded network data |
**principal_id** | **String** | The principal ID | [optional]
