# ServiceManagerRest::Device

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Object** | The device data |
**principal_id** | **String** | The principal ID | [optional]
