# ServiceManagerRest::InternetRoutesApi

All URIs are relative to *https://example.orgv1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_internet_route**](InternetRoutesApi.md#create_internet_route) | **POST** /accounts/{account_id}/gateways/{device_id}/internet_routes |
[**delete_internet_route**](InternetRoutesApi.md#delete_internet_route) | **DELETE** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |
[**get_internet_route**](InternetRoutesApi.md#get_internet_route) | **GET** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |
[**get_internet_routes**](InternetRoutesApi.md#get_internet_routes) | **GET** /accounts/{account_id}/gateways/{device_id}/internet_routes |
[**update_internet_route**](InternetRoutesApi.md#update_internet_route) | **PUT** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |


# **create_internet_route**
> SuccessResult create_internet_route(account_id, device_id, accounts_account_id_gateways_device_id_internet_routes)



Create an internet route for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::InternetRoutesApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

accounts_account_id_gateways_device_id_internet_routes = ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdInternetRoutes.new # PostAccountsAccountIdGatewaysDeviceIdInternetRoutes |


begin
  result = api_instance.create_internet_route(account_id, device_id, accounts_account_id_gateways_device_id_internet_routes)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling InternetRoutesApi->create_internet_route: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **accounts_account_id_gateways_device_id_internet_routes** | [**PostAccountsAccountIdGatewaysDeviceIdInternetRoutes**](PostAccountsAccountIdGatewaysDeviceIdInternetRoutes.md)|  |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **delete_internet_route**
> SuccessResult delete_internet_route(account_id, device_id, ip_address)



Delete an internet route for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::InternetRoutesApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The internet route IP Address


begin
  result = api_instance.delete_internet_route(account_id, device_id, ip_address)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling InternetRoutesApi->delete_internet_route: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The internet route IP Address |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_internet_route**
> InternetRouteEnvelope get_internet_route(account_id, device_id, ip_address)



Get an internet route for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::InternetRoutesApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The internet route IP Address


begin
  result = api_instance.get_internet_route(account_id, device_id, ip_address)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling InternetRoutesApi->get_internet_route: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The internet route IP Address |

### Return type

[**InternetRouteEnvelope**](InternetRouteEnvelope.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_internet_routes**
> InternetRoutesEnvelope get_internet_routes(account_id, device_id)



Get all of the internet routes for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::InternetRoutesApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID


begin
  result = api_instance.get_internet_routes(account_id, device_id)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling InternetRoutesApi->get_internet_routes: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |

### Return type

[**InternetRoutesEnvelope**](InternetRoutesEnvelope.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **update_internet_route**
> SuccessResult update_internet_route(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_internet_routes)



Update an internet route for a gateway device

### Example
```ruby
# load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::InternetRoutesApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

ip_address = "ip_address_example" # String | The internet route IP Address

accounts_account_id_gateways_device_id_internet_routes = ServiceManagerRest::DeviceDeviceIdInternetRoutes.new # DeviceDeviceIdInternetRoutes |


begin
  result = api_instance.update_internet_route(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_internet_routes)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling InternetRoutesApi->update_internet_route: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| The account ID |
 **device_id** | **String**| The device ID |
 **ip_address** | **String**| The internet route IP Address |
 **accounts_account_id_gateways_device_id_internet_routes** | [**DeviceDeviceIdInternetRoutes**](DeviceDeviceIdInternetRoutes.md)|  |

### Return type

[**SuccessResult**](SuccessResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json
