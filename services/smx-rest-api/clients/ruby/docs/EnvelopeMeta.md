# ServiceManagerRest::EnvelopeMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The ID of the envelope resource | [optional] [default to &quot;FFFF000000001&quot;]
**type** | **String** | The type of resource in the envelope | [optional] [default to &quot;device&quot;]
**model** | **String** | The resource model in the envelope | [optional] [default to &quot;gateway&quot;]
**source** | **String** | Where the envelope originated | [optional] [default to &quot;smx&quot;]
**schema** | [**EnvelopeSchema**](EnvelopeSchema.md) |  | [optional]
