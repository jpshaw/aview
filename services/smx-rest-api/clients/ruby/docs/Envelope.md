# ServiceManagerRest::Envelope

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**EnvelopeMeta**](EnvelopeMeta.md) |  |
**data** | **Object** |  |
