# ServiceManagerRest::EnvelopeSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The envelope data schema name | [optional] [default to &quot;smx&quot;]
**version** | **Integer** | The envelope data schema version | [optional] [default to 1]
