# smx-rest-client

ServiceManagerRest - the Ruby gem for the Service Manager REST API

HTTP REST API for AT&T Service Manager

This SDK is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 0.6.0
- Package version: 0.6.0
- Build package: io.swagger.codegen.languages.RubyClientCodegen

## Installation

### Build a gem

To build the Ruby code into a gem:

```shell
gem build smx-rest-client.gemspec
```

Then either install the gem locally:

```shell
gem install ./smx-rest-client-0.6.0.gem
```
(for development, run `gem install --dev ./smx-rest-client-0.6.0.gem` to install the development dependencies)

or publish the gem to a gem hosting service, e.g. [RubyGems](https://rubygems.org/).

Finally add this to the Gemfile:

    gem 'smx-rest-client', '~> 0.6.0'

### Install from Git

If the Ruby gem is hosted at a git repository: https://github.com/GIT_USER_ID/GIT_REPO_ID, then add the following in the Gemfile:

    gem 'smx-rest-client', :git => 'https://github.com/GIT_USER_ID/GIT_REPO_ID.git'

### Include the Ruby code directly

Include the Ruby code directly using `-I` as follows:

```shell
ruby -Ilib script.rb
```

## Getting Started

Please follow the [installation](#installation) procedure and then run the following code:
```ruby
# Load the gem
require 'smx-rest-client'

api_instance = ServiceManagerRest::CascadedNetworksApi.new

account_id = "account_id_example" # String | The account ID

device_id = "device_id_example" # String | The device ID

accounts_account_id_gateways_device_id_cascaded_networks = ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks.new # PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks |


begin
  result = api_instance.create_cascaded_network(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks)
  p result
rescue ServiceManagerRest::ApiError => e
  puts "Exception when calling CascadedNetworksApi->create_cascaded_network: #{e}"
end

```

## Documentation for API Endpoints

All URIs are relative to *https://example.orgv1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ServiceManagerRest::CascadedNetworksApi* | [**create_cascaded_network**](docs/CascadedNetworksApi.md#create_cascaded_network) | **POST** /accounts/{account_id}/gateways/{device_id}/cascaded_networks |
*ServiceManagerRest::CascadedNetworksApi* | [**delete_cascaded_network**](docs/CascadedNetworksApi.md#delete_cascaded_network) | **DELETE** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |
*ServiceManagerRest::CascadedNetworksApi* | [**get_cascaded_network**](docs/CascadedNetworksApi.md#get_cascaded_network) | **GET** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |
*ServiceManagerRest::CascadedNetworksApi* | [**get_cascaded_networks**](docs/CascadedNetworksApi.md#get_cascaded_networks) | **GET** /accounts/{account_id}/gateways/{device_id}/cascaded_networks |
*ServiceManagerRest::CascadedNetworksApi* | [**update_cascaded_network**](docs/CascadedNetworksApi.md#update_cascaded_network) | **PUT** /accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address} |
*ServiceManagerRest::GatewaysApi* | [**get_gateway_device**](docs/GatewaysApi.md#get_gateway_device) | **GET** /accounts/{account_id}/gateways/{device_id} |
*ServiceManagerRest::GatewaysApi* | [**update_gateway_device**](docs/GatewaysApi.md#update_gateway_device) | **PUT** /accounts/{account_id}/gateways/{device_id} |
*ServiceManagerRest::InternetRoutesApi* | [**create_internet_route**](docs/InternetRoutesApi.md#create_internet_route) | **POST** /accounts/{account_id}/gateways/{device_id}/internet_routes |
*ServiceManagerRest::InternetRoutesApi* | [**delete_internet_route**](docs/InternetRoutesApi.md#delete_internet_route) | **DELETE** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |
*ServiceManagerRest::InternetRoutesApi* | [**get_internet_route**](docs/InternetRoutesApi.md#get_internet_route) | **GET** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |
*ServiceManagerRest::InternetRoutesApi* | [**get_internet_routes**](docs/InternetRoutesApi.md#get_internet_routes) | **GET** /accounts/{account_id}/gateways/{device_id}/internet_routes |
*ServiceManagerRest::InternetRoutesApi* | [**update_internet_route**](docs/InternetRoutesApi.md#update_internet_route) | **PUT** /accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address} |


## Documentation for Models

 - [ServiceManagerRest::CascadedNetworkEnvelope](docs/CascadedNetworkEnvelope.md)
 - [ServiceManagerRest::CascadedNetworksEnvelope](docs/CascadedNetworksEnvelope.md)
 - [ServiceManagerRest::Device](docs/Device.md)
 - [ServiceManagerRest::DeviceDeviceIdCascadedNetworks](docs/DeviceDeviceIdCascadedNetworks.md)
 - [ServiceManagerRest::DeviceDeviceIdInternetRoutes](docs/DeviceDeviceIdInternetRoutes.md)
 - [ServiceManagerRest::Envelope](docs/Envelope.md)
 - [ServiceManagerRest::EnvelopeMeta](docs/EnvelopeMeta.md)
 - [ServiceManagerRest::EnvelopeSchema](docs/EnvelopeSchema.md)
 - [ServiceManagerRest::InternetRouteEnvelope](docs/InternetRouteEnvelope.md)
 - [ServiceManagerRest::InternetRoutesEnvelope](docs/InternetRoutesEnvelope.md)
 - [ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks](docs/PostAccountsAccountIdGatewaysDeviceIdCascadedNetworks.md)
 - [ServiceManagerRest::PostAccountsAccountIdGatewaysDeviceIdInternetRoutes](docs/PostAccountsAccountIdGatewaysDeviceIdInternetRoutes.md)
 - [ServiceManagerRest::SuccessResult](docs/SuccessResult.md)


## Documentation for Authorization

 All endpoints do not require authorization.
