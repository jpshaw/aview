require 'spec_helper'
require 'json'
require 'date'

# Unit tests for ServiceManagerRest::CascadedNetworksEnvelope
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'CascadedNetworksEnvelope' do
  before do
    # run before each test
    @instance = ServiceManagerRest::CascadedNetworksEnvelope.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of CascadedNetworksEnvelope' do
    it 'should create an instance of CascadedNetworksEnvelope' do
      expect(@instance).to be_instance_of(ServiceManagerRest::CascadedNetworksEnvelope)
    end
  end
  describe 'test attribute "data"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end

