require 'spec_helper'
require 'json'

# Unit tests for ServiceManagerRest::GatewaysApi
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'GatewaysApi' do
  before do
    # run before each test
    @instance = ServiceManagerRest::GatewaysApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of GatewaysApi' do
    it 'should create an instance of GatewaysApi' do
      expect(@instance).to be_instance_of(ServiceManagerRest::GatewaysApi)
    end
  end

  # unit tests for get_gateway_device
  # Get a single gateway device
  # Get a single gateway device
  # @param account_id The account ID
  # @param device_id The device ID
  # @param [Hash] opts the optional parameters
  # @return [Envelope]
  describe 'get_gateway_device test' do
    it "should work" do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for update_gateway_device
  # Update an existing gateway device
  # Update an existing gateway device
  # @param account_id The account ID
  # @param device_id The device ID
  # @param body 
  # @param [Hash] opts the optional parameters
  # @return [SuccessResult]
  describe 'update_gateway_device test' do
    it "should work" do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
