require "uri"

module ServiceManagerRest
  class GatewaysApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Get a single gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [Envelope]
    def get_gateway_device(account_id, device_id, opts = {})
      data, _status_code, _headers = get_gateway_device_with_http_info(account_id, device_id, opts)
      return data
    end

    #
    # Get a single gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(Envelope, Fixnum, Hash)>] Envelope data, response status code and response headers
    def get_gateway_device_with_http_info(account_id, device_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: GatewaysApi.get_gateway_device ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling GatewaysApi.get_gateway_device"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling GatewaysApi.get_gateway_device"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Envelope')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: GatewaysApi#get_gateway_device\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Update an existing gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param body
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def update_gateway_device(account_id, device_id, body, opts = {})
      data, _status_code, _headers = update_gateway_device_with_http_info(account_id, device_id, body, opts)
      return data
    end

    #
    # Update an existing gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param body
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def update_gateway_device_with_http_info(account_id, device_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: GatewaysApi.update_gateway_device ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling GatewaysApi.update_gateway_device"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling GatewaysApi.update_gateway_device"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling GatewaysApi.update_gateway_device"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: GatewaysApi#update_gateway_device\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
