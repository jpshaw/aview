require "uri"
require "smx-rest-client/addressable"

module ServiceManagerRest
  class InternetRoutesApi
    include ServiceManagerRest::Addressable

    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Create an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param accounts_account_id_gateways_device_id_internet_routes
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def create_internet_route(account_id, device_id, accounts_account_id_gateways_device_id_internet_routes, opts = {})
      data, _status_code, _headers = create_internet_route_with_http_info(account_id, device_id, accounts_account_id_gateways_device_id_internet_routes, opts)
      return data
    end

    #
    # Create an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param accounts_account_id_gateways_device_id_internet_routes
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def create_internet_route_with_http_info(account_id, device_id, accounts_account_id_gateways_device_id_internet_routes, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: InternetRoutesApi.create_internet_route ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling InternetRoutesApi.create_internet_route"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling InternetRoutesApi.create_internet_route"
      end
      # verify the required parameter 'accounts_account_id_gateways_device_id_internet_routes' is set
      if @api_client.config.client_side_validation && accounts_account_id_gateways_device_id_internet_routes.nil?
        fail ArgumentError, "Missing the required parameter 'accounts_account_id_gateways_device_id_internet_routes' when calling InternetRoutesApi.create_internet_route"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/internet_routes".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(accounts_account_id_gateways_device_id_internet_routes)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: InternetRoutesApi#create_internet_route\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Delete an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def delete_internet_route(account_id, device_id, ip_address, opts = {})
      data, _status_code, _headers = delete_internet_route_with_http_info(account_id, device_id, formatted_ip_address(ip_address), opts)
      return data
    end

    #
    # Delete an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def delete_internet_route_with_http_info(account_id, device_id, ip_address, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: InternetRoutesApi.delete_internet_route ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling InternetRoutesApi.delete_internet_route"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling InternetRoutesApi.delete_internet_route"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling InternetRoutesApi.delete_internet_route"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: InternetRoutesApi#delete_internet_route\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Get an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param [Hash] opts the optional parameters
    # @return [InternetRouteEnvelope]
    def get_internet_route(account_id, device_id, ip_address, opts = {})
      data, _status_code, _headers = get_internet_route_with_http_info(account_id, device_id, formatted_ip_address(ip_address), opts)
      return data
    end

    #
    # Get an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param [Hash] opts the optional parameters
    # @return [Array<(InternetRouteEnvelope, Fixnum, Hash)>] InternetRouteEnvelope data, response status code and response headers
    def get_internet_route_with_http_info(account_id, device_id, ip_address, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: InternetRoutesApi.get_internet_route ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling InternetRoutesApi.get_internet_route"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling InternetRoutesApi.get_internet_route"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling InternetRoutesApi.get_internet_route"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'InternetRouteEnvelope')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: InternetRoutesApi#get_internet_route\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Get all of the internet routes for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [InternetRoutesEnvelope]
    def get_internet_routes(account_id, device_id, opts = {})
      data, _status_code, _headers = get_internet_routes_with_http_info(account_id, device_id, opts)
      return data
    end

    #
    # Get all of the internet routes for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(InternetRoutesEnvelope, Fixnum, Hash)>] InternetRoutesEnvelope data, response status code and response headers
    def get_internet_routes_with_http_info(account_id, device_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: InternetRoutesApi.get_internet_routes ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling InternetRoutesApi.get_internet_routes"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling InternetRoutesApi.get_internet_routes"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/internet_routes".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'InternetRoutesEnvelope')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: InternetRoutesApi#get_internet_routes\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Update an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param accounts_account_id_gateways_device_id_internet_routes
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def update_internet_route(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_internet_routes, opts = {})
      data, _status_code, _headers = update_internet_route_with_http_info(account_id, device_id, formatted_ip_address(ip_address), accounts_account_id_gateways_device_id_internet_routes, opts)
      return data
    end

    #
    # Update an internet route for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The internet route IP Address
    # @param accounts_account_id_gateways_device_id_internet_routes
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def update_internet_route_with_http_info(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_internet_routes, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: InternetRoutesApi.update_internet_route ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling InternetRoutesApi.update_internet_route"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling InternetRoutesApi.update_internet_route"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling InternetRoutesApi.update_internet_route"
      end
      # verify the required parameter 'accounts_account_id_gateways_device_id_internet_routes' is set
      if @api_client.config.client_side_validation && accounts_account_id_gateways_device_id_internet_routes.nil?
        fail ArgumentError, "Missing the required parameter 'accounts_account_id_gateways_device_id_internet_routes' when calling InternetRoutesApi.update_internet_route"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/internet_routes/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(accounts_account_id_gateways_device_id_internet_routes)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: InternetRoutesApi#update_internet_route\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
