require "uri"
require "smx-rest-client/addressable"

module ServiceManagerRest
  class CascadedNetworksApi
    include ServiceManagerRest::Addressable

    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Create a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param accounts_account_id_gateways_device_id_cascaded_networks
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def create_cascaded_network(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks, opts = {})
      data, _status_code, _headers = create_cascaded_network_with_http_info(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks, opts)
      return data
    end

    #
    # Create a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param accounts_account_id_gateways_device_id_cascaded_networks
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def create_cascaded_network_with_http_info(account_id, device_id, accounts_account_id_gateways_device_id_cascaded_networks, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CascadedNetworksApi.create_cascaded_network ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling CascadedNetworksApi.create_cascaded_network"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling CascadedNetworksApi.create_cascaded_network"
      end
      # verify the required parameter 'accounts_account_id_gateways_device_id_cascaded_networks' is set
      if @api_client.config.client_side_validation && accounts_account_id_gateways_device_id_cascaded_networks.nil?
        fail ArgumentError, "Missing the required parameter 'accounts_account_id_gateways_device_id_cascaded_networks' when calling CascadedNetworksApi.create_cascaded_network"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/cascaded_networks".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(accounts_account_id_gateways_device_id_cascaded_networks)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CascadedNetworksApi#create_cascaded_network\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Delete a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def delete_cascaded_network(account_id, device_id, ip_address, opts = {})
      data, _status_code, _headers = delete_cascaded_network_with_http_info(account_id, device_id, formatted_ip_address(ip_address), opts)
      return data
    end

    #
    # Delete a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def delete_cascaded_network_with_http_info(account_id, device_id, ip_address, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CascadedNetworksApi.delete_cascaded_network ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling CascadedNetworksApi.delete_cascaded_network"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling CascadedNetworksApi.delete_cascaded_network"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling CascadedNetworksApi.delete_cascaded_network"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CascadedNetworksApi#delete_cascaded_network\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Get a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param [Hash] opts the optional parameters
    # @return [CascadedNetworkEnvelope]
    def get_cascaded_network(account_id, device_id, ip_address, opts = {})
      data, _status_code, _headers = get_cascaded_network_with_http_info(account_id, device_id, formatted_ip_address(ip_address), opts)
      return data
    end

    #
    # Get a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param [Hash] opts the optional parameters
    # @return [Array<(CascadedNetworkEnvelope, Fixnum, Hash)>] CascadedNetworkEnvelope data, response status code and response headers
    def get_cascaded_network_with_http_info(account_id, device_id, ip_address, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CascadedNetworksApi.get_cascaded_network ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling CascadedNetworksApi.get_cascaded_network"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling CascadedNetworksApi.get_cascaded_network"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling CascadedNetworksApi.get_cascaded_network"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'CascadedNetworkEnvelope')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CascadedNetworksApi#get_cascaded_network\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Get all of the cascaded networks for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [CascadedNetworksEnvelope]
    def get_cascaded_networks(account_id, device_id, opts = {})
      data, _status_code, _headers = get_cascaded_networks_with_http_info(account_id, device_id, opts)
      return data
    end

    #
    # Get all of the cascaded networks for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(CascadedNetworksEnvelope, Fixnum, Hash)>] CascadedNetworksEnvelope data, response status code and response headers
    def get_cascaded_networks_with_http_info(account_id, device_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CascadedNetworksApi.get_cascaded_networks ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling CascadedNetworksApi.get_cascaded_networks"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling CascadedNetworksApi.get_cascaded_networks"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/cascaded_networks".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'CascadedNetworksEnvelope')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CascadedNetworksApi#get_cascaded_networks\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Update a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param accounts_account_id_gateways_device_id_cascaded_networks
    # @param [Hash] opts the optional parameters
    # @return [SuccessResult]
    def update_cascaded_network(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_cascaded_networks, opts = {})
      data, _status_code, _headers = update_cascaded_network_with_http_info(account_id, device_id, formatted_ip_address(ip_address), accounts_account_id_gateways_device_id_cascaded_networks, opts)
      return data
    end

    #
    # Update a cascaded network for a gateway device
    # @param account_id The account ID
    # @param device_id The device ID
    # @param ip_address The cascaded network IP Address
    # @param accounts_account_id_gateways_device_id_cascaded_networks
    # @param [Hash] opts the optional parameters
    # @return [Array<(SuccessResult, Fixnum, Hash)>] SuccessResult data, response status code and response headers
    def update_cascaded_network_with_http_info(account_id, device_id, ip_address, accounts_account_id_gateways_device_id_cascaded_networks, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CascadedNetworksApi.update_cascaded_network ..."
      end
      # verify the required parameter 'account_id' is set
      if @api_client.config.client_side_validation && account_id.nil?
        fail ArgumentError, "Missing the required parameter 'account_id' when calling CascadedNetworksApi.update_cascaded_network"
      end
      # verify the required parameter 'device_id' is set
      if @api_client.config.client_side_validation && device_id.nil?
        fail ArgumentError, "Missing the required parameter 'device_id' when calling CascadedNetworksApi.update_cascaded_network"
      end
      # verify the required parameter 'ip_address' is set
      if @api_client.config.client_side_validation && ip_address.nil?
        fail ArgumentError, "Missing the required parameter 'ip_address' when calling CascadedNetworksApi.update_cascaded_network"
      end
      # verify the required parameter 'accounts_account_id_gateways_device_id_cascaded_networks' is set
      if @api_client.config.client_side_validation && accounts_account_id_gateways_device_id_cascaded_networks.nil?
        fail ArgumentError, "Missing the required parameter 'accounts_account_id_gateways_device_id_cascaded_networks' when calling CascadedNetworksApi.update_cascaded_network"
      end
      # resource path
      local_var_path = "/accounts/{account_id}/gateways/{device_id}/cascaded_networks/{ip_address}".sub('{' + 'account_id' + '}', account_id.to_s).sub('{' + 'device_id' + '}', device_id.to_s).sub('{' + 'ip_address' + '}', ip_address.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(accounts_account_id_gateways_device_id_cascaded_networks)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'SuccessResult')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CascadedNetworksApi#update_cascaded_network\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
