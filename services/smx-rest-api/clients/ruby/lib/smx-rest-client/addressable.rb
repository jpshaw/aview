module ServiceManagerRest
  module Addressable
    def formatted_ip_address(address)
      address.to_s.gsub('.','-')
    end
  end
end
