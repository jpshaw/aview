# Common files
require 'smx-rest-client/api_client'
require 'smx-rest-client/api_error'
require 'smx-rest-client/version'
require 'smx-rest-client/configuration'

# Models
require 'smx-rest-client/models/cascaded_network_envelope'
require 'smx-rest-client/models/cascaded_networks_envelope'
require 'smx-rest-client/models/device'
require 'smx-rest-client/models/device_device_id_cascaded_networks'
require 'smx-rest-client/models/device_device_id_internet_routes'
require 'smx-rest-client/models/envelope'
require 'smx-rest-client/models/envelope_meta'
require 'smx-rest-client/models/envelope_schema'
require 'smx-rest-client/models/internet_route_envelope'
require 'smx-rest-client/models/internet_routes_envelope'
require 'smx-rest-client/models/post_accounts_account_id_gateways_device_id_cascaded_networks'
require 'smx-rest-client/models/post_accounts_account_id_gateways_device_id_internet_routes'
require 'smx-rest-client/models/success_result'

# APIs
require 'smx-rest-client/api/cascaded_networks_api'
require 'smx-rest-client/api/gateways_api'
require 'smx-rest-client/api/internet_routes_api'

module ServiceManagerRest
  class << self
    # Customize default settings for the SDK using block.
    #   ServiceManagerRest.configure do |config|
    #     config.username = "xxx"
    #     config.password = "xxx"
    #   end
    # If no block given, return the default Configuration object.
    def configure
      if block_given?
        yield(Configuration.default)
      else
        Configuration.default
      end
    end
  end
end
