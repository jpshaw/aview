namespace :docs do
  namespace :generate do
    swagger_file = Rails.root.join('doc', 'swagger_doc.json').to_s

    desc 'Generate a swagger definition file for the current API'
    task swagger_doc: :environment do
      require 'rake/swagger_tasks'
      ServiceManagerRest::Rake::ApiTasks.new(api_class: ::API::Base,
                                             file_path: swagger_file)

      Rake::Task['swagger_definition:fetch'].invoke
    end

    desc 'Generate an HTML page for the latest swagger definition'
    task :html do
      puts "swagger-codegen-cli generate " \
         "--input-spec #{swagger_file} " \
         "--output doc/ " \
         "--lang html"
      sh "swagger-codegen-cli generate " \
         "--input-spec #{swagger_file} " \
         "--output doc/ " \
         "--lang html"
    end

    task all: %w(swagger_doc html)
  end

  task generate: %w(generate:all)
end

desc 'Generate all documentation'
task docs: 'docs:generate'
