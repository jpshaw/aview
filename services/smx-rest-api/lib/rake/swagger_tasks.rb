# frozen_string_literal: true

require 'rake'
require 'rake/tasklib'
require 'rack/test'
require 'grape-swagger/rake/oapi_tasks'

module ServiceManagerRest
  module Rake
    class ApiTasks < ::Rake::TaskLib
      include Rack::Test::Methods

      attr_reader :oapi
      attr_reader :api_class

      def initialize(api_class: ::API::Base, file_path: nil)
        super()

        @api_class = api_class
        @file_path = file_path

        define_tasks
      end

      private

      def define_tasks
        namespace :swagger_definition do
          fetch
          validate
        end
      end

      # tasks
      #
      # get swagger/OpenAPI documentation
      def fetch
        desc 'generates OpenApi documentation …
          params (usage: key=value):
          store    – save as JSON file, default: false            (optional)
          resource - if given only for that it would be generated (optional)'
        task fetch: :environment do
          # :nocov:
          make_request
          apply_patches

          save_to_file? ? write_to_file : $stdout.print(@oapi)
          # :nocov:
        end
      end

      # validates swagger/OpenAPI documentation
      def validate
        desc 'validates the generated OpenApi file …
          params (usage: key=value):
          resource - if given only for that it would be generated (optional)'
        task validate: :environment do
          # :nocov:
          ENV['store'] = 'true'
          ::Rake::Task['oapi:fetch'].invoke
          exit if error?

          output = system "swagger validate #{file}"

          $stdout.puts 'install swagger-cli with `npm install swagger-cli -g`' if output.nil?
          FileUtils.rm(file)
          # :nocov:
        end
      end

      # helper methods
      #
      def make_request
        get url_for

        @oapi = JSON.pretty_generate(
          JSON.parse(
            last_response.body, symolize_names: true
          )
        ) + "\n"
      end

      def apply_patches
        return if @oapi.nil?

        api_class.definition_patches.each do |old_name, new_name|
          @oapi.gsub!(old_name, new_name)
        end
      end

      def url_for
        oapi_route = api_class.routes[-2]
        path = oapi_route.path.sub(/\(\.\w+\)$/, '').sub(/\(\.:\w+\)$/, '')
        path.sub!(':version', oapi_route.version.to_s)

        [path, ENV['resource']].join('/').chomp('/')
      end

      def save_to_file?
        @file_path.present? && !error?
      end

      def error?
        JSON.parse(@oapi).keys.first == 'error'
      end

      def file
        @file_path
      end

      def write_to_file
        puts "Writing swagger definition to #{file}"
        File.write(file, @oapi)
      end

      def app
        api_class.new
      end
    end
  end
end
