# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.14.0] - 2018-07-10
### Added
- Support for Principal ID

## [0.13.0] - 2018-03-16
### Added
- service-manager gem

## [0.12.0] - 2018-02-03
### Changed
- Upgrade service-manager gem to 0.19
