# Service Manager REST API

## Usage

### Server

To run the server, first export the Service Manager environment variables:

```bash
  export SMX_ENV="development"
  export SMX_APPLICATION="ACCELERATED"
  export SMX_SECRET="SECRET"
```

Then start it:

```bash
  ./bin/server
```

Note: The server does not daemonize. See `config/puma.rb` for the server configuration.

### Gateway Query

```bash
account_id="ACCARMT"
device_id="FFFF000000001"
content_type='Content-Type: application/json'
curl -s -H $content_type http://localhost:3000/v1/accounts/$account_id/gateways/$device_id | jq
{
  "meta": {
    "name": "FFFF000000001",
    "type": "device",
    "model": "gateway",
    "source": "smx",
    "schema": {
      "name": "smx",
      "version": 1
    }
  },
  "data": {
    "deviceId": "FFFF000000001",
    "account": "ACCARMT",
    "modelInd": "N",
    "deviceStatus": "",
    "modelId": "VPN-DEVICE1-MDL",
    "customerName": "",
    "contactName": "TEST USER",
    "contactNumber": "555 555-5555",
    ...
  }
}
```

### Gateway Update

```bash
account_id="ACCARMT"
device_id="FFFF000000001"
device_data='{"data": {"contactName": "Accelerated User"}, "principal_id": "atap.accel1"}'
content_type='Content-Type: application/json'
curl -s -H $content_type -X PUT -d $device_data http://localhost:3000/v1/accounts/$account_id/gateways/$device_id | jq
{
  "message": "Success",
  "code": 200
}
```

## Documentation

Generate all API documentation with:

```bash
  rake docs:generate
```

A JSON [swagger definition](doc/swagger_doc.json) and
[single-page HTML document](doc/index.html) are created in the `doc` folder.

To generate the swagger definition:

```bash
  rake docs:generate:swagger_doc
```

To generate the html page:

```bash
  rake docs:generate:html
```

## Clients

### Generate client for a language

Use the `generate-client` tool to generate a client from the [swagger definition](doc/swagger_doc.json)
in any language:

```bash
  ./bin/generate-client LANGUAGE
```

The client code will be created in `clients/LANGUAGE`. You can pass language-specific
configurations to the `swagger-codegen-cli` with a file `clients/LANGUAGE-config.json`. To view the available configurations for a
language run:

```bash
  swagger-codegen-cli config-help -l LANGUAGE
```

### Ruby

The ruby client exists at [`clients/ruby`](clients/ruby) and is generated with:

```bash
  ./bin/generate-client ruby
```

See [`clients/ruby-config.json`](clients/ruby-config.json) for ruby-specific configurations.

To use the ruby client, make sure you set the configuration before calling:

```ruby
  ServiceManagerRest.configure do |config|
    config.scheme = 'http'
    config.host = 'localhost:3000'
  end

  client = ServiceManagerRest::GatewaysApi.new
  account_id = "ACCARMT"
  device_id = "FFFF000000001"

  begin
    #Get a single gateway device
    result = client.get_gateway_device(account_id, device_id)
    p result
  rescue ServiceManagerRest::ApiError => e
    puts "Exception when calling GatewaysApi->get_gateway_device: #{e}"
  end
```

## Docker

### Build

```bash
  docker build -t smx-outbound:latest .
```

### Run as a daemon

Use the `-d` flag to daemonize:

```bash
  docker run -d -p 3000:3000 \
    -e "SMX_APPLICATION=$SMX_APPLICATION" \
    -e "SMX_SECRET=$SMX_SECRET" \
    -e "SMX_ENV=$SMX_ENV" \
    -e "RAILS_ENV=$RAILS_ENV" \
    smx-outbound:latest
```

### Run interactively

Use the flags `-t` and `-i` to run interactively (ctrl-c works):

```bash
  docker run -t -i -p 3000:3000 \
    -e "SMX_APPLICATION=$SMX_APPLICATION" \
    -e "SMX_SECRET=$SMX_SECRET" \
    -e "SMX_ENV=$SMX_ENV" \
    -e "RAILS_ENV=$RAILS_ENV" \
    smx-outbound:latest
```
