require 'service_manager/protocol/gateway_fetch_request'
require 'service_manager/protocol/gateway_fetch_response'
require 'service_manager/protocol/gateway_update_request'
require 'service_manager/protocol/gateway_update_response'
require 'service_manager/protocol/gateway_query'

module ServiceManager
  module API
    module Gateway
      def gateway_device(**options)
        object_request ServiceManager::Protocol::GatewayFetchRequest.new(**options)
      end

      def update_gateway_device(**options)
        object_request ServiceManager::Protocol::GatewayUpdateRequest.new(**options)
      end
    end
  end
end
