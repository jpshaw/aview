require 'service_manager/configurable'
require 'service_manager/protocol'

module ServiceManager
  class Client
    include ServiceManager::Configurable

    def initialize(options = {})
      # Use options passed in, but fall back to module defaults
      ServiceManager::Configurable.keys.each do |key|
        value = options[key] || ServiceManager.instance_variable_get(:"@#{key}")
        instance_variable_set(:"@#{key}", value)
      end

      @session = ServiceManager::Protocol::Session.instance(options)
    end

    # TODO: Move these methods into modules

    ## Gateway / Network

    def gateway_device(**options)
      object_request Protocol::GatewayFetchRequest, options
    end

    def update_gateway_device(**options)
      object_request Protocol::GatewayUpdateRequest, options
    end

    ## Internet Routes

    def internet_routes(**options)
      list_request Protocol::InternetRoutesFetchRequest, options
    end

    def create_internet_route(**options)
      object_request Protocol::InternetRouteCreateRequest, options
    end

    def get_internet_route(**options)
      object_request Protocol::InternetRouteFetchRequest, options
    end

    def update_internet_route(**options)
      object_request Protocol::InternetRouteUpdateRequest, options
    end

    def delete_internet_route(**options)
      object_request Protocol::InternetRouteDeleteRequest, options
    end

    ## Cascaded Networks

    def cascaded_networks(**options)
      list_request Protocol::CascadedNetworksFetchRequest, options
    end

    def create_cascaded_network(**options)
      object_request Protocol::CascadedNetworkCreateRequest, options
    end

    def get_cascaded_network(**options)
      object_request Protocol::CascadedNetworkFetchRequest, options
    end

    def update_cascaded_network(**options)
      object_request Protocol::CascadedNetworkUpdateRequest, options
    end

    def delete_cascaded_network(**options)
      object_request Protocol::CascadedNetworkDeleteRequest, options
    end

    # Text representation of the client, masking secrets
    #
    # @return [String]
    def inspect
     inspected = super
     inspected = inspected.gsub! @secret, "*******" if @secret
     inspected
    end

    private

    def object_request(klass, options)
      principal_id = options.delete(:principal_id)
      @session.send_object_request(klass.new(**options), principal_id)
    end

    def list_request(klass, options)
      principal_id = options.delete(:principal_id)
      @session.send_list_request(klass.new(**options), principal_id)
    end
  end
end
