module ServiceManager

  # https://github.com/octokit/octokit.rb/blob/master/lib/octokit/configurable.rb
  #
  # Configuration options for {Client}, defaulting to values
  # in {Default}
  module Configurable
    attr_accessor :application, :domain, :log_level, :servers
    attr_writer :secret

    class << self
      # List of configurable keys for {ServiceManager::Client}
      # @return [Array] of option keys
      def keys
        @keys ||= [
          :application,
          :secret,
          :domain,
          :log_level,
          :report_interval,
          :session_restart_interval,
          :default_timeout,
          :servers
        ]
      end
    end

    # Set configuration options using a block
    def configure
      yield self
    end

    # Reset configuration options to default values
    def reset!
      ServiceManager::Configurable.keys.each do |key|
        instance_variable_set(:"@#{key}", ServiceManager::Default.options[key])
      end
      self
    end
    alias_method :setup, :reset!

    # Compares client options to a Hash of requested options
    #
    # @param opts [Hash] Options to compare with current client options
    # @return [Boolean]
    def same_options?(opts)
      opts.hash == options.hash
    end

    private

    def options
      Hash[ServiceManager::Configurable.keys.map { |key| [key, instance_variable_get(:"@#{key}")] }]
    end
  end
end
