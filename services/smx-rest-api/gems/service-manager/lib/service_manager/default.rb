require 'yaml'

module ServiceManager
  module Default
    class << self

      def options
        Hash[ServiceManager::Configurable.keys.map { |key| [key, send(key)]}]
      end

      def environment
        ENV['SMX_ENV'] || 'development'
      end

      def application
        ENV['SMX_APPLICATION'] || config[:application]
      end

      def secret
        ENV['SMX_SECRET'] || config[:secret]
      end

      def domain
        ENV['SMX_DOMAIN'] || config[:domain] || ServiceManager::Protocol::Domains.default
      end

      def log_level
        ENV['SMX_LOG_LEVEL'] || config[:log_level] || 'normal'
      end

      def report_interval
        ENV['SMX_REPORT_INTERVAL'] || config[:report_interval] || 120
      end

      def session_restart_interval
        ENV['SMX_SESSION_RESTART_INTERVAL'] || config[:session_restart_interval] || 20
      end

      def default_timeout
        ENV['SMX_DEFAULT_TIMEOUT'] || config[:default_timeout] || 100
      end

      # TODO: Add support for ENV['SMX_SERVERS']
      # SMX_SERVERS="tcp://129.37.0.176:5080,tcp://32.96.129.227:5080"
      def servers
        config[:servers]
      end

      private

      def config_dir
        @config_dir ||= ::File.expand_path('../../config', __dir__)
      end

      def config_file
        @config_file ||= begin
          if ENV['SMX_CONF_FILE']
            if File.exist?(ENV['SMX_CONF_FILE'])
              ENV['SMX_CONF_FILE']
            else
              raise ConfigurationFileNotFoundError.new(ENV['SMX_CONF_FILE'])
            end
          else
            "#{config_dir}/smx.yml"
          end
        end
      end

      def config
        @config ||= YAML.load_file(config_file)[environment].deep_symbolize_keys
      end

    end
  end
end
