require 'service_manager/field_mapper'

module ServiceManager
  module Commands
    class Generate < Thor
      include Thor::Actions

      no_commands do
        def self.data_dir
          File.expand_path("../../../data", __dir__)
        end
      end

      desc 'fields NAME', 'Generate a JSON field map for resource with NAME'
      method_option :data_dir, aliases: '-d', desc: 'The directory where the field map file will be generated', default: data_dir
      def fields(name)
        file_path = "#{options[:data_dir]}/#{name}.json"
        field_map = ServiceManager::FieldMapper.new(name).execute

        create_file file_path do
          JSON.pretty_generate(field_map)
        end
      end
    end
  end
end
