module ServiceManager
  # Classes and utilities provided by AT&T to communicate with Service Manager
  module Protocol
    IPAddressField = com.att.servicemanager.ram.IPAddressField
    SMStringField = com.att.servicemanager.ram.SMStringField
  end
end

require 'service_manager/protocol/client'
require 'service_manager/protocol/domains'
require 'service_manager/protocol/errors'
require 'service_manager/protocol/flag_field'
require 'service_manager/protocol/cascaded_network'
require 'service_manager/protocol/cascaded_networks_fetch_request'
require 'service_manager/protocol/cascaded_networks_fetch_response'
require 'service_manager/protocol/cascaded_network_fetch_request'
require 'service_manager/protocol/cascaded_network_fetch_response'
require 'service_manager/protocol/cascaded_network_create_request'
require 'service_manager/protocol/cascaded_network_update_request'
require 'service_manager/protocol/cascaded_network_delete_request'
require 'service_manager/protocol/cascaded_network_response'
require 'service_manager/protocol/cascaded_network_query'
require 'service_manager/protocol/gateway_device'
require 'service_manager/protocol/gateway_fetch_request'
require 'service_manager/protocol/gateway_fetch_response'
require 'service_manager/protocol/gateway_update_request'
require 'service_manager/protocol/gateway_update_response'
require 'service_manager/protocol/gateway_query'
require 'service_manager/protocol/internet_route'
require 'service_manager/protocol/internet_routes_fetch_request'
require 'service_manager/protocol/internet_routes_fetch_response'
require 'service_manager/protocol/internet_route_fetch_request'
require 'service_manager/protocol/internet_route_fetch_response'
require 'service_manager/protocol/internet_route_create_request'
require 'service_manager/protocol/internet_route_update_request'
require 'service_manager/protocol/internet_route_delete_request'
require 'service_manager/protocol/internet_route_response'
require 'service_manager/protocol/internet_route_query'
require 'service_manager/protocol/message'
require 'service_manager/protocol/properties_builder'
require 'service_manager/protocol/request_builder'
require 'service_manager/protocol/request'
require 'service_manager/protocol/response'
require 'service_manager/protocol/session'
