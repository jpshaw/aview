module ServiceManager
  class FieldMapper

    attr_reader :resource, :fields

    def initialize(name)
      @resource = find_resource(name)
      @fields = []
    end

    def execute
      resource.java_class.fields.each do |field|
        process_field_data(field)
      end

      fields
    end

    private

    def find_resource(name)
      class_name = name.split('_').collect(&:capitalize).join

      if ServiceManager::Protocol.const_defined?(class_name)
        ServiceManager::Protocol.const_get(class_name).new
      else
        fail "ServiceManager::Protocol::#{class_name} is not defined"
      end
    end

    def process_field_data(field)
      field_data = {
        name: field.name,
        size: field_size(field),
        unpack_format: field_format(field)
      }

      @fields << field_data
    end

    def field_size(field)
      resource.send(field.name).length
    end

    def field_format(field)
      case resource.send(field.name)
      when ServiceManager::Protocol::FlagField
        'A'
      else
        'A*'
      end
    end
  end
end
