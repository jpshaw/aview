module ServiceManager
  module Protocol
    # TODO: Look into adding an Enumerable mixin for batching responses, similar
    # to how the Twitter gem does it:
    # https://github.com/sferik/twitter/blob/master/lib/twitter/enumerable.rb
    class Response < com.att.servicemanager.ram.ProcessRAMResponse
      # Enumerate over each returned row with the type `row_class` and extract
      # data from it to a yielded hash.
      def each_row_for_class(row_class)
        list_rows_ret.times do |i|
          row = row_class.new
          row.init_to_hex_zeros
          row.value_fields(get_response_data_at(i))

          row_data = {}

          row_class.field_list.each do |field|
            if row.respond_to?(field)
              row_data[field] = row.send(field).send(:field_value).strip
            end
          end

          yield(row_data) if block_given?
        end
      end

      # Check how many rows are left in the response
      def finished?
        list_rows_ret >= list_rows_avail
      end
    end
  end
end
