module ServiceManager
  module Protocol
    class GatewayFetchRequest

      attr_reader :device_id, :account_id, :inherit, :domain, :data

      def initialize(device_id:, account_id:, inherit: false, domain: Protocol::Domains.default)
        @device_id  = device_id
        @account_id = account_id
        @inherit    = inherit
        @domain     = domain
        @data       = build_data
      end

      def name
        'QUERY_VD1'
      end

      def response_class
        Protocol::GatewayFetchResponse
      end

      private

      def build_data
        data = Protocol::GatewayQuery.new
        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)
        data
      end

    end
  end
end
