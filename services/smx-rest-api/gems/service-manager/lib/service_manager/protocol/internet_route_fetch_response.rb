require 'service_manager/internet_route'

module ServiceManager
  module Protocol
    class InternetRouteFetchResponse

      attr_reader :data

      def initialize(response)
        @data = parse_response(response)
      end

      private

      def parse_response(response)
        route = Protocol::InternetRoute.new
        route.init_to_hex_zeros
        route.value_fields(response.get_response_data_at(0))

        hash = {}

        Protocol::InternetRoute.field_list.each do |field|
          hash[field] = route.send(field).send(:field_value).strip
        end

        ServiceManager::InternetRoute.new(hash)
      end

    end
  end
end
