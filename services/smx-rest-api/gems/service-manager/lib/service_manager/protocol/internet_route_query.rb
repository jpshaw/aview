module ServiceManager
  module Protocol
    # public DeviceIdField    deviceId    = new DeviceIdField();
    # public AccountNameField account     = new AccountNameField();
    # public IPAddressField   ipAddress   = new IPAddressField();
    # public SMStringField    ipv6Address = new SMStringField(45);
    # public IPAddressField   lastIpAddress   = new IPAddressField();
    # public SMStringField    lastIpv6Address = new SMStringField(45);
    class InternetRouteQuery < com.att.servicemanager.ram.KeyStruct50
      # Internet Routes Monkey Patch
      #
      # Because it's much easier to do it here than the magical incantation it
      # takes to recompile the SMX JAR.
      attr_accessor :lastIpAddress, :lastIpv6Address

      def initialize
        super
        @lastIpAddress = Protocol::IPAddressField.new
        @lastIpv6Address = Protocol::SMStringField.new(45)
      end

      def getFields
        super + [
          lastIpAddress,
          lastIpv6Address
        ]
      end
      alias_method :fields, :getFields
    end
  end
end
