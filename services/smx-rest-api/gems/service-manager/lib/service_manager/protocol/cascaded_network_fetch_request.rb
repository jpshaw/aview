module ServiceManager
  module Protocol
    class CascadedNetworkFetchRequest
      attr_reader :device_id, :account_id, :ip_address, :inherit, :domain, :data

      def initialize(device_id:, account_id:, ip_address:, inherit: false, domain: Protocol::Domains.default)
        @device_id  = device_id
        @account_id = account_id
        @ip_address = ip_address
        @inherit    = inherit
        @domain     = domain
        @data       = build_data
      end

      def name
        'QUERY_VL1'
      end

      def response_class
        Protocol::CascadedNetworkFetchResponse
      end

      private

      def build_data
        data = Protocol::CascadedNetworkQuery.new
        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)

        if ip_address.include?(':')
          data.ipv6Address.set_field_value(ip_address)
        else
          data.ipAddress.set_field_value(ip_address)
        end

        data
      end

    end
  end
end
