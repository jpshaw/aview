module ServiceManager
  module Protocol
    # Interface between our code and the AT&T SM Java code.
    #
    # A psuedo-singleton is implemented since the Java client caches and reuses
    # connections to SM.
    class Session
      @instance = nil
      @mutex = Mutex.new

      def self.instance(options = {})
        return @instance if @instance

        @mutex.synchronize do
          return @instance if @instance
          @instance = new(options)
        end

        @instance
      end

      def start
        @client ||= Protocol::Client.new(@properties)
      end

      def started?
        !@client.nil? && @client.is_alive?
      end

      def stop
        @client.interrupt if @client
        @client = nil
      end

      # Request a single object and expect a single response.
      def send_object_request(request, principal_id)
        response = send_smx_request(request, principal_id)
        response_class = request.response_class
        response_class.new(response).data if response_class
      end

      # Request a list of objects and expect one or more responses (batching).
      def send_list_request(request, principal_id)
        response = send_smx_request(request, principal_id)
        response_class = request.response_class
        response_class.new(request, response, self).data
      end

      # Builds and sends a ProcessRAMRequest to SM and validates the
      # ProcessRAMResponse before returning it.
      def send_smx_request(request, principal_id)
        response = Protocol::Response.new

        with_exception_handler do
          @client.send_request(build_smx_request(request, principal_id), response)
        end

        validate_response!(response)

        response
      end

      private

      # TODO: Perhaps we should use Ruby's `Singleton` mixin instead of rolling our own?
      def initialize(options = {})
        @properties = Protocol::PropertiesBuilder.new(options).build
        @request_builder = Protocol::RequestBuilder.new(
          application: options[:application],
          secret: options[:secret]
        )

        start unless started?
      end
      private_class_method :new

      # Create a ProcessRAMRequest from a generic request object.
      def build_smx_request(request, principal_id)
        @request_builder.build_request(
          name: request.name,
          data: request.data,
          inherit: request.inherit,
          domain: request.domain,
          principal_id: principal_id
        )
      end

      # Catch and handle SM errors thrown during a transaction.
      def with_exception_handler(max_retries = 5)
        retry_count ||= 0
        yield
      rescue com.att.servicemanager.NoAvailableConnectionsException => exception
        # Gradually sleep and retry while waiting for session to reconnect
        sleep(retry_count) and retry unless (retry_count += 1) > max_retries

        # Raise error if connection retry failed
        error = ServiceManager::NoAvailableConnectionsError.new(exception.message)
        error.set_backtrace(exception.backtrace)
        raise error
      end

      # This follows the logic defined in the `checkRAMResponse` method in
      # com.att.servicemanager.ram.SMQueryEngine.
      def validate_response!(response)
        case response.status_code
        when Protocol::Message::SUCCESSFUL
          # OK
        else
          case response.return_code
          when Protocol::Message::RAM_PROFILE_NOT_FOUND
            raise ServiceManager::ProfileNotFoundError.new('Profile not found')
          when Protocol::Message::RAM_EDIT_CHECK_ERROR
            raise ServiceManager::BadRequestError.new(edit_errors(response))
          else
            raise ServiceManager::BadRequestError.new(error_message(response))
          end
        end
      end

      def edit_errors(response)
        Protocol::EditError.new(response.edit_codes_array).check_edit_array(response.request_name)
      rescue => exception
        exception.message
      end

      def error_message(response)
        Protocol::Message.map_return_codes(response.status_code, response.return_code, response.reason_code.to_s)
      rescue => exception
        "Status Code = #{response.status_code}, Return Code: #{response.return_code}, Reason Code: #{response.reason_code}"
      end
    end
  end
end
