module ServiceManager
  module Protocol
    class InternetRouteFetchRequest
      attr_reader :device_id, :account_id, :ip_address, :inherit, :domain, :data

      def initialize(device_id:, account_id:, ip_address:, inherit: false, domain: Protocol::Domains.default)
        @device_id  = device_id
        @account_id = account_id
        @ip_address = ip_address
        @inherit    = inherit
        @domain     = domain
        @data       = build_data
      end

      def name
        'QUERY_VIR'
      end

      def response_class
        Protocol::InternetRouteFetchResponse
      end

      private

      def build_data
        data = Protocol::InternetRouteQuery.new
        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)

        if ip_address.include?(':')
          data.ipv6Address.set_field_value(ip_address)
        else
          data.ipAddress.set_field_value(ip_address)
        end

        data
      end

    end
  end
end
