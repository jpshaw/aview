module ServiceManager
  module Protocol
    class CascadedNetwork < com.att.servicemanager.ram.VL1Row
      def self.field_list
        @field_list ||= java_class.fields.map { |field| field.name }
      end
    end
  end
end
