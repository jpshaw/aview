module ServiceManager
  module Protocol
    module Domains
      US = 'US' # United States
      CA = 'CA' # Canada
      JP = 'JP' # Japan
      EM = 'EM' # EMEA/Europe

      DEFAULT = US
      LIST = [US, CA, JP, EM].freeze

      def self.default
        DEFAULT
      end

      def self.include?(domain)
        LIST.include?(domain)
      end
    end
  end
end
