require 'service_manager/gateway_device'

module ServiceManager
  module Protocol
    class GatewayFetchResponse

      attr_reader :data

      def initialize(response)
        @data = parse_response(response)
      end

      private

      def parse_response(response)
        device = Protocol::GatewayDevice.new
        device.init_to_hex_zeros
        device.value_fields(response.get_response_data_at(0))

        hash = {}

        Protocol::GatewayDevice.field_list.each do |field|
          hash[field] = device.send(field).send(:field_value).strip
        end

        ServiceManager::GatewayDevice.new(hash)
      end

    end
  end
end
