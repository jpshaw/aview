require 'service_manager/internet_route'

module ServiceManager
  module Protocol
    class InternetRoutesFetchResponse
      def initialize(request, response, session)
        @request = request
        @response = response
        @session = session
        @results = []
      end

      def data
        parse_response

        until response.finished?
          fetch_next_response
          parse_response
        end

        results
      end

      private

      attr_reader :request, :response, :session, :results

      def parse_response
        response.each_row_for_class(Protocol::InternetRoute) do |row|
          results << ServiceManager::InternetRoute.new(row)
        end
      end

      def fetch_next_response
        return if response.finished?

        last_route = results.last

        request.last_ipv4 = last_route.ipAddress
        request.last_ipv6 = last_route.ipv6Address

        @response = session.send_smx_request(request)
      end
    end
  end
end
