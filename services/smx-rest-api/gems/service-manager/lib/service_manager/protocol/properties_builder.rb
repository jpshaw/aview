module ServiceManager
  module Protocol
    class PropertiesBuilder
      def initialize(options)
        @options = options.dup
      end

      def build
        properties = java.util.Properties.new

        properties_hash.each do |key, value|
          properties.set_property(key.to_s, value.to_s)
        end

        properties
      end

      private

      def properties_hash
        hash = Hash.new

        hash['default.trace.dest'] = :syserr
        hash['default.trace.level'] = @options[:log_level]
        hash['default.trace.level.svm'] = @options[:log_level]
        hash['servicemanager.reportinterval'] = @options[:report_interval]
        hash['servicemanager.sessionrestartinterval'] = @options[:session_restart_interval]
        hash['servicemanager.defaulttimeout'] = @options[:default_timeout]

        @options[:servers].each do |server|
          prefix = "svmconnection.#{server[:name]}"

          hash["#{prefix}.protocol"] = server[:protocol]
          hash["#{prefix}.ipaddress"] = server[:host]
          hash["#{prefix}.port"] = server[:port]
          hash["#{prefix}.domain"] = server[:domain]
          hash["#{prefix}.lateresponseintervallimit"] = server[:response_timeout]
          hash["#{prefix}.maxoutstandingrequests"] = server[:request_limit]
          hash["#{prefix}.receivetimeout"] = server[:timeout]
        end

        hash
      end
    end
  end
end
