module ServiceManager
  module Protocol
    class CascadedNetworkResponse
      attr_reader :data

      def initialize(response)
        @data = true
      end
    end
  end
end
