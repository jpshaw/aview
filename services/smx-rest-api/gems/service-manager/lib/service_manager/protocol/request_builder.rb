module ServiceManager
  module Protocol
    class RequestBuilder
      def initialize(application:, secret:)
        @application = application
        @encryption_key = build_encryption_key(secret)
      end

      def build_request(name:, data:, inherit: false, domain: Protocol::Domains.default, principal_id: nil)
        request = Protocol::Request.new

        if principal_id.present?
          request.set_set_req_prn(true)
          request.set_req_prn(principal_id.to_s.upcase)
        end

        request.set_request_data(data, name)
        request.set_inherit_ind(boolean(inherit))
        request.set_application_name(@application)
        request.set_domain(domain)
        request.set_trusted_interface(true)

        # Encryption
        request.set_request_encryption(true)
        request.set_encryption_keys(@encryption_key)
        request.set_encryption_key_num(1)

        request.validate

        request
      end

      private

      def build_encryption_key(secret)
        key    = java.lang.String[2].new
        key[0] = java.lang.String.new(secret).trim
        key
      end

      def boolean(input)
        input ? 'Y' : 'N'
      end
    end
  end
end
