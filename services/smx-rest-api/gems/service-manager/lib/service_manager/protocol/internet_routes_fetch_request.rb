module ServiceManager
  module Protocol
    class InternetRoutesFetchRequest
      attr_reader :device_id, :account_id, :inherit, :domain
      attr_accessor :last_ipv4, :last_ipv6

      def initialize(device_id:, account_id:, inherit: false, domain: Protocol::Domains.default,
                     last_ipv4: '', last_ipv6: '')
        @device_id  = device_id
        @account_id = account_id
        @inherit    = inherit
        @domain     = domain
        @last_ipv4  = last_ipv4
        @last_ipv6  = last_ipv6
      end

      def name
        'LIST_VIRS'
      end

      def response_class
        Protocol::InternetRoutesFetchResponse
      end

      def data
        build_data
      end

      private

      def build_data
        data = Protocol::InternetRouteQuery.new
        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)
        data.ipAddress.set_field_value('*')
        data.ipv6Address.set_field_value('*')
        data.lastIpAddress.set_field_value(last_ipv4)
        data.lastIpv6Address.set_field_value(last_ipv6)
        data
      end

    end
  end
end
