require 'service_manager/cascaded_network'

module ServiceManager
  module Protocol
    class CascadedNetworkFetchResponse

      attr_reader :data

      def initialize(response)
        @data = parse_response(response)
      end

      private

      def parse_response(response)
        route = Protocol::CascadedNetwork.new
        route.init_to_hex_zeros
        route.value_fields(response.get_response_data_at(0))

        hash = {}

        Protocol::CascadedNetwork.field_list.each do |field|
          hash[field] = route.send(field).send(:field_value).strip
        end

        ServiceManager::CascadedNetwork.new(hash)
      end
    end
  end
end
