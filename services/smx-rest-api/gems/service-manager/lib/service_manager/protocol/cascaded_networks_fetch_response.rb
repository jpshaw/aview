require 'service_manager/cascaded_network'

module ServiceManager
  module Protocol
    class CascadedNetworksFetchResponse
      def initialize(request, response, session)
        @request = request
        @response = response
        @session = session
        @results = []
      end

      def data
        parse_response

        until response.finished?
          fetch_next_response
          parse_response
        end

        results
      end

      private

      attr_reader :request, :response, :session, :results

      def parse_response
        response.each_row_for_class(Protocol::CascadedNetwork) do |row|
          results << ServiceManager::CascadedNetwork.new(row)
        end
      end

      def fetch_next_response
        return if response.finished?

        last_network = results.last

        request.last_ipv4 = last_network.ipAddress
        request.last_ipv6 = last_network.ipv6Address

        @response = session.send_smx_request(request)
      end
    end
  end
end
