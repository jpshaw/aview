module ServiceManager
  module Protocol
    class InternetRoute < com.att.servicemanager.ram.VIRRow
      def self.field_list
        @field_list ||= java_class.fields.map { |field| field.name }
      end
    end
  end
end
