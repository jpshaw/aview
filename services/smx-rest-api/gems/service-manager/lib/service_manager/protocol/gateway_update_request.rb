module ServiceManager
  module Protocol
    class GatewayUpdateRequest

      attr_reader :device_id, :account_id, :inherit, :domain, :data

      def initialize(device_id:, account_id:, inherit: false, domain: Protocol::Domains.default, data: {})
        @device_id   = device_id
        @account_id  = account_id
        @inherit     = inherit
        @domain      = domain
        @device_data = data.dup.with_indifferent_access
        @data        = build_data
      end

      def name
        'UPDATE_VD1'
      end

      def response_class
        Protocol::GatewayUpdateResponse
      end

      private

      def build_data
        data = Protocol::GatewayDevice.new

        # This is important! Otherwise, the device data is cleared out
        data.init_to_hex_zeros

        verify_state_and_province

        @device_data.each do |key, value|
          if data.respond_to?(key)
            data.send(key).set_field_value(value)
          end
        end

        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)

        data
      end

      # Service Manager only supports `state` for USA, and `province` for all
      # other countries.
      def verify_state_and_province
        return if state_and_province_blank?

        if country_is_usa?
          @device_data[:province] = ''
        else
          @device_data[:state] = ''
        end
      end

      def state_and_province_blank?
        @device_data[:state].blank? && @device_data[:province].blank?
      end

      USA = 'UNITED STATES'.freeze

      def country_is_usa?
        @device_data[:country].present? && @device_data[:country].upcase == USA
      end
    end
  end
end
