module ServiceManager
  module Protocol
    class GatewayDevice < com.att.servicemanager.ram.Device1Row
      def self.field_list
        @field_list ||= java_class.fields.map { |field| field.name }
      end
    end
  end
end
