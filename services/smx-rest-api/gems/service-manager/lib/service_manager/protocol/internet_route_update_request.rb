module ServiceManager
  module Protocol
    class InternetRouteUpdateRequest

      attr_reader :device_id, :account_id, :ip_address, :inherit, :domain, :data

      def initialize(device_id:, account_id:, ip_address:, inherit: false, domain: Protocol::Domains.default, data: {})
        @device_id   = device_id
        @account_id  = account_id
        @ip_address  = ip_address
        @inherit     = inherit
        @domain      = domain
        @route_data  = data.dup
        @data        = build_data
      end

      def name
        'UPDATE_VIR'
      end

      def response_class
        Protocol::InternetRouteResponse
      end

      private

      def build_data
        data = Protocol::InternetRoute.new
        data.init_to_hex_zeros

        @route_data.each do |key, value|
          if data.respond_to?(key)
            data.send(key).set_field_value(value)
          end
        end

        if ip_address.include?(':')
          data.ipv6Address.set_field_value(ip_address)
          data.ipAddress.set_field_value('')
        else
          data.ipAddress.set_field_value(ip_address)
        end

        data.account.set_field_value(account_id)
        data.deviceId.set_field_value(device_id)

        data
      end

    end
  end
end
