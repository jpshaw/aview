module ServiceManager
  ClientError = Class.new(StandardError)

  # Raised if SMX_CONF_FILE doesn't exist
  ConfigurationFileNotFoundError = Class.new(ClientError)

  # Raised if a profile doesn't exist
  ProfileNotFoundError = Class.new(ClientError)

  # Raised if a request has errors
  BadRequestError = Class.new(ClientError)

  # Raised if no connections are available
  NoAvailableConnectionsError = Class.new(ClientError)
end
