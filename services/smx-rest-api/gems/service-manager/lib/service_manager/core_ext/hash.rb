# https://github.com/rails/rails/blob/master/activesupport/lib/active_support/core_ext/hash/keys.rb
class Hash
  def as_json(opts = {})
    self
  end

  def deep_symbolize_keys
    deep_transform_keys(self) { |key| key.to_sym rescue key }
  end

  def deep_transform_keys(object, &block)
    case object
    when Hash
      object.each_with_object({}) do |(key, value), result|
        result[yield(key)] = deep_transform_keys(value, &block)
      end
    when Array
      object.map { |e| deep_transform_keys(e, &block) }
    else
      object
    end
  end
end
