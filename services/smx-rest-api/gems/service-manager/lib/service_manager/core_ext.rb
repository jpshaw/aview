Dir.glob(File.expand_path("core_ext/*.rb", __dir__)).each do |path|
  require path
end

require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/hash/indifferent_access'
