require 'thor'
require 'service_manager'
require 'service_manager/commands/generate'

Signal.trap("INT") { puts; exit(1) }

module ServiceManager
  class CLI < Thor
    desc 'generate COMMANDS', 'Generate Service Manager Files'
    subcommand 'generate', ServiceManager::Commands::Generate
  end
end

ServiceManager::CLI.start(ARGV)
