module ServiceManager
  class BaseObject < OpenStruct

    def to_json(options = {})
      as_json(options).to_json
    end

    def as_json(options = {})
      @table.as_json(options)
    end

    def keys
      to_h.keys
    end

  end
end
