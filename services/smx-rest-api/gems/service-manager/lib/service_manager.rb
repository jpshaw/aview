require 'java'
require 'smx.jar'
require 'json'
require 'thread'
require 'ostruct'

require 'service_manager/core_ext'
require 'service_manager/base_object'
require 'service_manager/client'
require 'service_manager/default'
require 'service_manager/errors'

module ServiceManager

  class << self
    include ServiceManager::Configurable

    # API client based on configured options {Configurable}
    #
    # @return [ServiceManager::Client] API wrapper
    def client
      return @client if defined?(@client) && @client.same_options?(options)
      @client = ServiceManager::Client.new(options)
    end

    private

    def respond_to_missing?(method_name, include_private=false)
      client.respond_to?(method_name, include_private)
    end

    def method_missing(method_name, *args, &block)
      if client.respond_to?(method_name)
        return client.send(method_name, *args, &block)
      end

      super
    end
  end

end

ServiceManager.setup
