# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'service_manager/version'

Gem::Specification.new do |spec|
  spec.name          = 'service-manager'
  spec.version       = ServiceManager::VERSION
  spec.authors       = ['Accelerated Concepts']
  spec.email         = ['aview@accelerated.com']
  spec.summary       = 'A JRuby client for AT&T Service Manager.'
  spec.description   = 'A JRuby client for AT&T Service Manager. Proprietary gem created by Accelerated Concepts, Inc.'
  spec.homepage      = 'https://bitbucket.org/accelecon/service-manager-jruby'
  spec.license       = 'Nonstandard'
  spec.platform      = 'java'

  # Prevent pushing this gem to RubyGems.org.
  spec.metadata['allowed_push_host'] = 'http://rubygems.accns.com/'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = %w(smx)
  spec.require_paths = %w(lib)
  spec.required_ruby_version = '>= 2.0'

  spec.add_dependency 'json', '>= 1.8.0'
  spec.add_dependency 'thor', '>= 0.18.1', '< 2.0'
  spec.add_dependency 'activesupport', '>= 4.2.0'

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
