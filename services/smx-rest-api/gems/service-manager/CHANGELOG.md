# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.21.0] - 2018-07-10
### Added
- Support for SMX Principal ID

## [0.20.0] - 2018-02-22
### Changed
- Don't explicitly override province for non-us countries

## [0.19.0] - 2018-02-02
### Added
- Batch requests for internet routes and cascaded networks

### Changed
- Only set state for USA, province for all other countries
