# JRuby Client for AT&T Service Manager

JRuby wrapper around the AT&T SM Java client, found here: http://gitlab.accns.com/att/smx-java-client.

## Installation

Add this line to your application's Gemfile:

```ruby
source 'http://rubygems.accns.com' do
  gem 'service-manager', '~> 0.18', platform: 'jruby'
end
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install service-manager

## Configuring

There are multiple ways to configure the gem. The easiest is to use the
`.configure` block:


```ruby
ServiceManager.configure do |config|
  config.application = '<APPLICATION>'
  config.secret = '<SECRET>'
end
```

Note: `<APPLICATION>` and `<SECRET>` are provided by AT&T. See [Michael Zaccari](mailto:michael.zaccari@accelerated.com)
for credentials.

You can use environment variables as well:

```bash
export SMX_APPLICATION="<APPLICATION>"
export SMX_SECRET="<SECRET>"
```

You can configure settings for each environment type (development, test,
production) and provide them in `yaml` file. The default configuration file is
located at `config/smx.yml`:

```yaml
default: &default
  application: ACCELERATED
  secret: TESTSECRET
  domain: US
  log_level: normal
  server_defaults: &server_defaults
    protocol: tcp
    port: 5080
    timeout: 60
    domain: US
    request_limit: 50
    response_timeout: 2

development:
  <<: *default
  log_level: debug
  servers:
    - <<: *server_defaults
      name: rigus1
      host: 129.37.0.176

test:
  <<: *default
  log_level: debug
  servers:
    - <<: *server_defaults
      name: rigus1
      host: 129.37.0.207

production:
  <<: *default
  servers:
    - <<: *server_defaults
      name: rigus1
      host: 32.96.129.227
    - <<: *server_defaults
      name: rigus2
      host: 32.96.129.234
    - <<: *server_defaults
      name: rigus3
      host: 32.96.129.228
    - <<: *server_defaults
      name: rigus4
      host: 32.96.129.242

```

A custom configuration file can be provided with `SMX_CONF_FILE`:

```bash
export SMX_CONF_FILE="/patch/to/smx.yaml"
```

By default the gem will use `development` environment settings. To set the
environment to something else:

```bash
export SMX_ENV="production"
```

## Usage

### Querying

```ruby
ServiceManager.configure do |config|
  config.application = '<APPLICATION>'
  config.secret = '<SECRET>'
end

device = ServiceManager.gateway_device(device_id: 'FFFF000000001', account_id: 'ACCARMT')
device.contactName
# => "NATE PLEASANT"

puts device.to_json
#=> {"deviceId":"FFFF000000001","account":"ACCARMT","modelInd":"N", ....}
```

You can also get an instance of the client and use it directly

```ruby
client = ServiceManager.client
device = client.gateway_device(device_id: 'FFFF000000001', account_id: 'ACCARMT')
device.contactName
# => "NATE PLEASANT"
```

Catch all client exceptions with `ServiceManager::ClientError`:

```ruby
begin
  device = ServiceManager.gateway_device(device_id: 'FFFF000000001', account_id: 'ACCARMT')
rescue ServiceManager::ClientError => exception
  puts exception.message
end
```

### Updating

```ruby
ServiceManager.update_gateway_device(
  device_id: 'FFFF000000001',
  account_id: 'ACCARMT',
  principal_id: 'atap.accel1',
  data: {
    contactName: 'Michael Zaccari',
    contactEmail: 'michael.zaccari@accelerated.com',
    contactNumber: '813 995 3312'
  }
)
# => true
```

## CLI

A basic command-line interface is available:

```bash
smx help

Commands:
  smx generate COMMANDS  # Generate Service Manager Files
  smx help [COMMAND]     # Describe available commands or one specific command
```

### Generate Field Maps

Field maps are useful for tools that can't load Java but need to parse data for
a resource from Service Manager.

```bash
smx generate help fields

Usage:
  smx fields NAME

Options:
  -d, [--data-dir=DATA_DIR]  # The directory where the field map file will be generated
                             # Default: /opt/gems/service-manager/data

Generate a JSON field map for resource with NAME
```

To create a field map for Gateway devices:

```bash
smx generate fields gateway_device
```

By default the map is stored at `data/gateway_device.json`.
