require 'spec_helper'

describe 'cascaded networks' do
  describe 'list' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D80' }

    context 'when given a device with many networks' do
      let(:result) { ServiceManager.cascaded_networks(device_id: device_id, account_id: account_id) }

      it 'returns all of the networks' do
        expect(result.size).to eq 211
      end
    end
  end

  describe 'create, get and delete' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D92' }

    context 'when given a correct ipv4 address' do
      let(:ip_address) { '10.4.1.1' }
      let(:data) { { ipAddress: ip_address,
                     subnetMask: '255.0.0.0',
                     routerIPAddress: '10.0.1.1',
                     description: 'auto generated',
                     lanAliasInd: 'N',
                     internetOnlyInd: 'N'
                 } }

      it 'creates, gets, and deletes a cascaded network' do
        ServiceManager.create_cascaded_network(device_id: device_id, account_id: account_id, data: data)
        network = ServiceManager.get_cascaded_network(device_id: device_id,
                                                     account_id: account_id,
                                                     ip_address: ip_address)
        expect(network.ipAddress).to eq ip_address

        ServiceManager.delete_cascaded_network(device_id: device_id,
                                              account_id: account_id,
                                              ip_address: ip_address)
        expect {
          network = ServiceManager.get_cascaded_network(device_id: device_id,
                                                       account_id: account_id,
                                                       ip_address: ip_address)
        }.to raise_error(ServiceManager::ProfileNotFoundError)
      end
    end
  end

  describe 'update' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D92' }

    context 'when given a new description' do
      let(:ip_address) { '10.4.1.1' }
      let(:new_description) { 'updated description' }
      let(:data) { { ipAddress: ip_address,
                     subnetMask: '255.0.0.0',
                     routerIPAddress: '10.0.1.1',
                     description: 'auto generated',
                     lanAliasInd: 'N',
                     internetOnlyInd: 'N'
                 } }

      before do
        ServiceManager.create_cascaded_network(device_id: device_id, account_id: account_id, data: data)
      end

      after do
        ServiceManager.delete_cascaded_network(device_id: device_id,
                                              account_id: account_id,
                                              ip_address: ip_address)
      end

      it 'updates the cascaded network' do
        ServiceManager.update_cascaded_network(device_id: device_id,
                                               account_id: account_id,
                                               ip_address: ip_address,
                                               data: { description: new_description })
        network = ServiceManager.get_cascaded_network(device_id: device_id,
                                                     account_id: account_id,
                                                     ip_address: ip_address)
        expect(network.description).to eq new_description.upcase
      end
    end
  end
end
