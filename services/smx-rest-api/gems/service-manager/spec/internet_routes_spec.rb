require 'spec_helper'

describe 'internet routes' do
  describe 'list' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D80' }

    context 'when given a device with many routes' do
      let(:result) { ServiceManager.internet_routes(device_id: device_id, account_id: account_id) }

      it 'returns all of the routes' do
        expect(result.size).to eq 109
      end
    end
  end

  describe 'create, get and delete' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D92' }

    context 'when given a correct ipv4 address' do
      let(:ip_address) { '10.4.1.1' }
      let(:data) { { ipAddress: ip_address,
                     subnetMask: '255.0.0.0',
                     description: 'auto generated'
                 } }

      it 'creates, gets, and deletes an internet route' do
        ServiceManager.create_internet_route(device_id: device_id, account_id: account_id, data: data)
        route = ServiceManager.get_internet_route(device_id: device_id,
                                                     account_id: account_id,
                                                     ip_address: ip_address)
        expect(route.ipAddress).to eq ip_address

        ServiceManager.delete_internet_route(device_id: device_id,
                                              account_id: account_id,
                                              ip_address: ip_address)
        expect {
          route = ServiceManager.get_internet_route(device_id: device_id,
                                                       account_id: account_id,
                                                       ip_address: ip_address)
        }.to raise_error(ServiceManager::ProfileNotFoundError)
      end
    end
  end

  describe 'update' do
    let(:account_id) { 'ACCELTES' }
    let(:device_id) { '0027042B6D92' }

    context 'when given a new description' do
      let(:ip_address) { '10.4.1.1' }
      let(:new_description) { 'updated description' }
      let(:data) { { ipAddress: ip_address,
                     subnetMask: '255.0.0.0',
                     description: 'auto generated'
                 } }

      before do
        ServiceManager.create_internet_route(device_id: device_id, account_id: account_id, data: data)
      end

      after do
        ServiceManager.delete_internet_route(device_id: device_id,
                                              account_id: account_id,
                                              ip_address: ip_address)
      end

      it 'updates the internet route' do
        ServiceManager.update_internet_route(device_id: device_id,
                                               account_id: account_id,
                                               ip_address: ip_address,
                                               data: { description: new_description })
        route = ServiceManager.get_internet_route(device_id: device_id,
                                                     account_id: account_id,
                                                     ip_address: ip_address)
        expect(route.description).to eq new_description.upcase
      end
    end
  end
end
