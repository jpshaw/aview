class GatewayUpdateService

  def execute(*params)
    debug(*params)
    ServiceManager.update_gateway_device(*params)
  end

  private

  def debug(*params)
    if Rails.env.development?
      Rails.logger.info "#{self.class.name}#execute: #{params}"
    end
  end

end
