module API
  class CascadedNetworks < Grape::API
    params do
      requires :account_id, type: String, desc: 'The account ID'
    end
    resource :accounts do
      desc 'Get all of the cascaded networks for a gateway device', {
        nickname: 'get_cascaded_networks',
        success: CascadedNetworksEnvelope,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['cascaded_networks']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
      end
      get ':account_id/gateways/:device_id/cascaded_networks' do
        result = CascadedNetworksFinder.new.execute(**declared_params)
        not_found!('Cascaded Networks') unless result
        present result, with: CascadedNetworksEnvelope
      end

      desc 'Create a cascaded network for a gateway device', {
        nickname: 'create_cascaded_network',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['cascaded_networks']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :data, type: Hash, desc: 'The cascaded network data', documentation: { type: Hash, param_type: 'body' }
        optional :principal_id, type: String, desc: 'The principal ID'
      end
      post ':account_id/gateways/:device_id/cascaded_networks' do
        result = ServiceManager.create_cascaded_network(**declared_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

      desc 'Get a cascaded network for a gateway device', {
        nickname: 'get_cascaded_network',
        success: CascadedNetworkEnvelope,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['cascaded_networks']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The cascaded network IP Address'
      end
      get ':account_id/gateways/:device_id/cascaded_networks/:ip_address' do
        result = ServiceManager.get_cascaded_network(**cascaded_network_params)
        not_found!('Cascaded Network') unless result
        present result, with: CascadedNetworkEnvelope
      end

      desc 'Update a cascaded network for a gateway device', {
        nickname: 'update_cascaded_network',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['cascaded_networks']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The cascaded network IP Address'
        requires :data, type: Hash, desc: 'The cascaded network data', documentation: { type: Hash, param_type: 'body' }
        optional :principal_id, type: String, desc: 'The principal ID'
      end
      put ':account_id/gateways/:device_id/cascaded_networks/:ip_address' do
        result = ServiceManager.update_cascaded_network(**cascaded_network_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

      desc 'Delete a cascaded network for a gateway device', {
        nickname: 'delete_cascaded_network',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['cascaded_networks']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The cascaded network IP Address'
      end
      delete ':account_id/gateways/:device_id/cascaded_networks/:ip_address' do
        result = ServiceManager.delete_cascaded_network(**cascaded_network_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

    end
  end
end
