module API
  class Base < Grape::API
    include API::DefinitionPatches

    version 'v1', using: :path

    rescue_from ServiceManager::ProfileNotFoundError do
      not_found!
    end

    rescue_from ServiceManager::BadRequestError do |exception|
      log_exception(exception)
      bad_request!(exception)
    end

    rescue_from :all do |exception|
      log_exception(exception)
      server_error!
    end

    format :json

    # Ensure the namespace is right, otherwise we might load Grape::API::Helpers
    helpers ::API::Helpers



    # Mount new stuff here
    mount ::API::Gateways
    mount ::API::InternetRoutes
    mount ::API::CascadedNetworks


    # Catch missing routes and render them as 404
    route :any, '*path' do
      not_found!
    end

    before do
      header['Access-Control-Allow-Origin'] = '*'
      header['Access-Control-Request-Method'] = '*'
    end

    add_swagger_documentation format: :json,
                              mount_path: '/swagger_doc',
                              base_path: 'v1',
                              add_version: false,
                              doc_version: '0.6.0',
                              info: {
                                title: "Service Manager REST API",
                                description: "HTTP REST API for AT&T Service Manager"
                              },
                              tags: [
                                { name: 'accounts', description: 'Account operations' },
                                { name: 'gateways', description: 'Gateway device operations' }
                              ]

  end
end
