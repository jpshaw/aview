module API
  class InternetRoutes < Grape::API
    params do
      requires :account_id, type: String, desc: 'The account ID'
    end
    resource :accounts do
      desc 'Get all of the internet routes for a gateway device', {
        nickname: 'get_internet_routes',
        success: InternetRoutesEnvelope,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['internet_routes']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
      end
      get ':account_id/gateways/:device_id/internet_routes' do
        internet_routes = InternetRoutesFinder.new.execute(**declared_params)
        not_found!('Internet Routes') unless internet_routes
        present internet_routes, with: InternetRoutesEnvelope
      end

      desc 'Create an internet route for a gateway device', {
        nickname: 'create_internet_route',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['internet_routes']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :data, type: Hash, desc: 'The internet route data', documentation: { type: Hash, param_type: 'body' }
        optional :principal_id, type: String, desc: 'The principal ID'
      end
      post ':account_id/gateways/:device_id/internet_routes' do
        result = ServiceManager.create_internet_route(**declared_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

      desc 'Get an internet route for a gateway device', {
        nickname: 'get_internet_route',
        success: InternetRouteEnvelope,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['internet_routes']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The internet route IP Address'
      end
      get ':account_id/gateways/:device_id/internet_routes/:ip_address' do
        result = ServiceManager.get_internet_route(**internet_route_params)
        not_found!('Internet Route') unless result
        present result, with: InternetRouteEnvelope
      end

      desc 'Update an internet route for a gateway device', {
        nickname: 'update_internet_route',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['internet_routes']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The internet route IP Address'
        requires :data, type: Hash, desc: 'The internet route data', documentation: { type: Hash, param_type: 'body' }
        optional :principal_id, type: String, desc: 'The principal ID'
      end
      put ':account_id/gateways/:device_id/internet_routes/:ip_address' do
        result = ServiceManager.update_internet_route(**internet_route_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

      desc 'Delete an internet route for a gateway device', {
        nickname: 'delete_internet_route',
        success: SuccessResult,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['internet_routes']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :ip_address, type: String, desc: 'The internet route IP Address'
      end
      delete ':account_id/gateways/:device_id/internet_routes/:ip_address' do
        result = ServiceManager.delete_internet_route(**internet_route_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end

    end
  end
end
