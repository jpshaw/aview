module API
  module DefinitionPatches
    extend ActiveSupport::Concern

    class_methods do
      # Manual changes to the swagger definition that I couldn't get working
      # with grape-swagger (yet)
      def definition_patches
        {
          'putAccountsAccountIdGateways' => 'Device'
        }
      end
    end
  end
end
