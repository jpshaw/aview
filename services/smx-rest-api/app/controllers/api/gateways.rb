module API
  class Gateways < Grape::API
    params do
      requires :account_id, type: String, desc: 'The account ID'
    end
    resource :accounts do
      desc 'Get a single gateway device', {
        nickname: 'get_gateway_device',
        success: GatewayEnvelope,
        failure: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['gateways']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
      end
      get ':account_id/gateways/:device_id' do
        device = GatewayFinder.new.execute(**declared_params)
        not_found!('Device') unless device

        present device, with: GatewayEnvelope
      end

      desc 'Update an existing gateway device', {
        nickname: 'update_gateway_device',
        body_name: 'body',
        success: SuccessResult,
        http_codes: [
           { code: 400, message: 'Bad Request' },
           { code: 404, message: 'Not Found' }
        ],
        tags: ['gateways']
      }
      params do
        requires :device_id, type: String, desc: 'The device ID'
        requires :data, type: Hash, desc: 'The device data', documentation: { type: Hash, param_type: 'body' }
        optional :principal_id, type: String, desc: 'The principal ID'
      end
      put ':account_id/gateways/:device_id' do
        result = GatewayUpdateService.new.execute(**declared_params)

        if result
          present result, with: SuccessResult
        else
          bad_request!
        end
      end
    end
  end
end
