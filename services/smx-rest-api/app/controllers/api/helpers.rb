module API
  # Copied from Gitlab:
  # https://github.com/gitlabhq/gitlabhq/blob/master/lib/api/helpers.rb
  module Helpers
    extend ActiveSupport::Concern

    def logger
      Rails.logger
    end

    # params helpers

    def declared_params(options = {})
      options = { include_parent_namespaces: true }.merge(options)
      declared(params, options).to_h.symbolize_keys
    end

    # TODO: Move to IR class
    def internet_route_params
      params = declared_params.dup
      # IPv4 addresses must have dots replaced as dashes
      params[:ip_address].gsub!('-','.')
      params
    end

    def cascaded_network_params
      params = declared_params.dup
      # IPv4 addresses must have dots replaced as dashes
      params[:ip_address].gsub!('-','.')
      params
    end

    # http status helpers

    def accepted!
      render_api_error!('Accepted', 202)
    end

    def bad_request!(error = nil)
      message = \
        case error
        when String
          error
        when StandardError
          error.message
        else
          'Bad Request'
        end

      render_api_error!(message, 400)
    end

    def not_found!(resource = nil)
      message = []
      message << resource if resource
      message << 'Not Found'

      render_api_error!(message.join(' '), 404)
    end

    def server_error!
      render_api_error!('Internal Server Error', 500)
    end

    def render_api_error!(message, status)
      error!({ message: message }, status, header)
    end

    def log_exception(exception)
      message = "\n#{exception.class} (#{exception.message}):\n"
      message << exception.annoted_source_code.to_s if exception.respond_to?(:annoted_source_code)
      message << "  " << exception.backtrace.join("\n  ")

      logger.error message
    end

    private

    # We could get a Grape or a standard Ruby exception. We should only report
    # anything that is clearly an error.
    def report_exception?(exception)
      return true unless exception.respond_to?(:status)

      exception.status == 500
    end

  end
end
