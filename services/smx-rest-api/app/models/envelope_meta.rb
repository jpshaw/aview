class EnvelopeMeta < Grape::Entity
  expose :name, documentation: { desc: 'The ID of the envelope resource', default: 'FFFF000000001' }
  expose :type, documentation: { desc: 'The type of resource in the envelope', default: 'device' }
  expose :model, documentation: { desc: 'The resource model in the envelope', default: 'gateway' }
  expose :source, documentation: { desc: 'Where the envelope originated', default: Envelope::ENVELOPE_ID }
  expose :schema, using: EnvelopeSchema

  def self.defaults
    {
      source: Envelope::ENVELOPE_ID,
      schema: {
        name: Envelope::ENVELOPE_ID,
        version: Envelope::ENVELOPE_VERSION
      }
    }
  end
end
