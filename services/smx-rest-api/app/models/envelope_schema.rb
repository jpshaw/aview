class EnvelopeSchema < Grape::Entity
  expose :name, documentation: { type: 'string', desc: 'The envelope data schema name', default: Envelope::ENVELOPE_ID }
  expose :version, documentation: { type: 'integer',  desc: 'The envelope data schema version', default: Envelope::ENVELOPE_VERSION }
end
