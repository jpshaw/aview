class CascadedNetworkEnvelope < Grape::Entity

  expose :data, documentation: { type: Hash, required: true } do |route|
    route
  end

  def self.entity_name
    'CascadedNetworkEnvelope'
  end

end
