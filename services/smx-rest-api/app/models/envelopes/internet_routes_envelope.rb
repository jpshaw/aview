class InternetRoutesEnvelope < Grape::Entity

  expose :data, documentation: { type: Hash, required: true, is_array: true }

  def self.entity_name
    'InternetRoutesEnvelope'
  end

end
