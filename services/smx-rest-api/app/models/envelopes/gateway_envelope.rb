class GatewayEnvelope < Grape::Entity

  expose :meta, using: EnvelopeMeta, documentation: { type: EnvelopeMeta, required: true }  do |device|
    EnvelopeMeta.defaults.merge(
      name: device.deviceId,
      type: 'device',
      model: 'gateway'
    )
  end

  expose :data, documentation: { type: Hash, required: true, default: { contactName: 'Test User' } } do |device|
    device.to_h
  end

  def self.entity_name
    'Envelope'
  end

end
