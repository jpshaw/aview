class CascadedNetworksEnvelope < Grape::Entity

  expose :data, documentation: { type: Hash, required: true, is_array: true }

  def self.entity_name
    'CascadedNetworksEnvelope'
  end

end
