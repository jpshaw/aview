class SuccessResult < Grape::Entity
  expose :message, documentation: { type: 'string', desc: 'HTTP Reponse Message', default: 'Success', required: true } do |result|
    'Success'
  end

  expose :code, documentation: { type: 'integer', desc: 'HTTP Reponse code', default: 200, required: true } do |result|
    200
  end
end
