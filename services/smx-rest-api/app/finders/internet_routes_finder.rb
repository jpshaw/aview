class InternetRoutesFinder

  def execute(*params)
    format(ServiceManager.internet_routes(*params))
  end

  private

  def format(internet_routes)
    {
      data: internet_routes.map { |internet_route| internet_route.to_h }
    }
  end

end
