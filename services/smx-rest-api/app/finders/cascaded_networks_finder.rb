class CascadedNetworksFinder

  def execute(*params)
    format(ServiceManager.cascaded_networks(*params))
  end

  private

  def format(cascaded_networks)
    {
      data: cascaded_networks.map { |network| network.to_h }
    }
  end

end
