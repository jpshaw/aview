require 'pathname'
require 'digest'
require 'json'
require 'memoist'
require 'kraken/logger'
require 'service/core_ext'
require 'service/configuration'

# Global constants
APP_ROOT    = Pathname.new File.expand_path('../../../', __FILE__)
SYSTEM_ROOT = ENV['AVIEW_DIR'] || ENV['HOME']

# Provides base utilities for running a service
module Service
  extend self

  @boot_callbacks = []
  @shutdown_callbacks = []

  def name
    @name ||= File.basename($0)
  end

  def name=(name)
    @name = name
  end

  def environment
    @environment ||= ENV['RAILS_ENV'] || 'development'
  end

  def environment=(environment)
    @environment = environment
  end

  def logger
    @logger ||= Kraken::Logger::Console[:Service]
  end

  def app_root
    APP_ROOT
  end

  def system_root
    SYSTEM_ROOT
  end

  def root
    app_root.join('services')
  end

  # Default service configurations
  def default_config_root
    "#{app_root}/services/config"
  end

  # System service configurations
  def system_config_root
    "#{system_root}/etc/services"
  end

  # Boots the service and runs the block (if given)
  #
  # Examples
  #
  #   # Long running service
  #   Service.start do
  #     loop do
  #       input = FetchInputData.execute
  #       output = TransformInputData.execute(input)
  #       MessageBus.publish(output, topic: 'foo.bar')
  #     end
  #   end
  #
  #   # Run a job
  #   Service.start do
  #     JobThatEventuallyFinishes.execute
  #   end
  #
  #   # Manual
  #   Service.start
  #   ... Do some stuff
  #   Service.stop
  #
  def start(&block)
    # Boot the service outside of the run scope to expose any errors that might
    # be otherwise suppressed in the runner.
    boot
    run(&block) if block_given?
  end

  def stop
    shutdown unless shutting_down?
  end

  def after_boot(&block)
    @boot_callbacks << block
  end

  def before_shutdown(&block)
    @shutdown_callbacks << block
  end

  # Register a filename the configuration loader should look for
  #
  # Search Paths:
  #   default - Local services/config directory
  #   system  - Deployment-specific etc/services directory
  #
  # Order: CLI > System > Default
  #
  # Similar to the `config` gem, having `.local.yml` files in the default path
  # will override the base default config. This is useful for development, where
  # creating a file in the system path can be annoying.
  def register_config_source(name, use_default: true, use_local: true, use_system: true)
    files = []
    files << File.join(default_config_root, "#{name}.yml").to_s       if use_default
    files << File.join(default_config_root, "#{name}.local.yml").to_s if use_local
    files << File.join(system_config_root, "#{name}.yml").to_s        if use_system

    Service::Configuration.register_files(*files) unless files.empty?
  end

  # Add service-specific command-line options
  #
  # Example
  #
  #    Service.register_cli_options do
  #      namespace :foo do
  #        option :bar, alias: :r, type: :string, desc: 'Foo bar', bannder: 'BAZ'
  #      end
  #    end
  #
  #  If a custom foo.yml file exists, this cli option would override foo.bar if
  #  it was passed in.
  def register_cli_options(&block)
    Service::Configuration::CLI.instance_eval(&block) if block_given?
  end

  private

  # Loads configuration, registers signal handlers, and runs boot callbacks
  def boot
    return if @booted

    load_configuration!

    logger.info "Booting #{name} in #{environment} ..."

    signals = %w(INT TERM)
    signals.each do |signal|
      trap signal do
        shutdown
      end
    end

    @boot_callbacks.each(&:call)
    @booted = true
  end

  # Any unhandled exceptions that reach the runner will trigger a shutdown and
  # stop the service.
  def run(&block)
    logger.info "Running in #{RUBY_DESCRIPTION}"
    logger.info "Starting service, hit Ctrl-C to stop"

    yield if block_given?

  # TODO: Should we sleep here in case supervisord restarts too fast?
  rescue ArgumentError => ex
    logger.error ex.message
  rescue => ex
    logger.error ex
  ensure
    stop
  end

  def load_configuration!
    register_config_source Service.name
    Service::Configuration.load!

    Kraken::Logger.verbose = !!Settings.verbose

    self.name        = Settings.name        if Settings.name
    self.environment = Settings.environment if Settings.environment
  end

  def shutdown
    @shutting_down = true
    logger.info 'Shutting down'
    @shutdown_callbacks.each(&:call)
    exit 0
  end

  def shutting_down?
    @shutting_down == true
  end
end

# Load the default configuration before all others
Service.register_config_source :default
