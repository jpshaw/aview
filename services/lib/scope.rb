require 'linked_list'

class Scope < LinkedList
  # Path for the scope.
  def path
    map(&:to_s).reverse.join(':')
  end

  # Scope lists always end with an EmptyScope object. See Null Object Pattern)
  class EmptyScope < EmptyLinkedList
    @parent = Scope

    def path
      ''
    end
  end

  # Singleton null object for an empty scope.
  EMPTY = EmptyScope.new
end
