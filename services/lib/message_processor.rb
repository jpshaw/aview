# Base class for processing messages from a message bus consumer
class MessageProcessor
  include Kraken::Logger.console

  class << self
    def execute(message)
      new(message).execute
    rescue => ex
      handle_exception(ex)
    end

    def handle_exception(ex)
      case ex
      when *debuggable_exceptions
        logger.debug ex.message
      else
        raise ex
      end
    end

    private

    def debuggable_exceptions
      @debuggable_exceptions ||= begin
        list = []
        if defined?(ActiveRecord)
          list << ActiveRecord::RecordNotFound
        end
        list
      end
    end
  end

  def initialize(message)
    @message = message
  end

  def execute
    raise NotImplementedError
  end

  private

  attr_reader :message

  def message_hash
    @message_hash ||= JSON.parse(message)
  end

  def message_data
    @message_data ||= message_fetch('data'.freeze)
  end

  def message_meta
    @message_meta ||= message_fetch('meta'.freeze)
  end

  def message_fetch(key)
    message_hash.fetch(key, {}).with_indifferent_access
  end
end
