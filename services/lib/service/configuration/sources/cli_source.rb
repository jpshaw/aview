require 'optparse'

module Service
  module Configuration
    class CLISource
      attr_reader :config

      def initialize(options)
        @options        = options
        @summary_width  = 32
        @summary_indent = ' ' * 4

        # Recursively build nested hashes to support scoped options
        @config = Hash.new { |h,k| h[k] = Hash.new(&h.default_proc) }
      end

      def load(argv = ARGV)
        parser = OptionParser.new do |o|
          o.separator "\nOptions:"

          o.on '-n', '--name NAME', "Service name (default: #{Service.name})" do |arg|
            config[:name] = arg
          end

          o.on '-e', '--environment ENV', "Service environment (default: #{Service.environment})" do |arg|
            config[:environment] = arg
          end

          # Load library and custom options
          options.each do |option|
            o.on(*option) do |arg|
              option_hash = fetch_option_hash(option.scope_keys)
              option_hash[option.name] = arg
            end
            update_summary_width_if_needed(option.width)
          end

          o.on '-v', '--verbose', 'Print more verbose output' do |arg|
            config[:verbose] = arg
          end
        end

        parser.banner = "Usage: \n\t #{Service.name} [OPTIONS]"
        parser.summary_width  = summary_width
        parser.summary_indent = summary_indent
        parser.on_tail '-h', '--help', 'Show help' do
          puts parser
          exit 1
        end
        parser.parse!(argv)

        config
      end

      private

      attr_accessor :options, :summary_width, :summary_indent

      def update_summary_width_if_needed(option_width)
        this_width = summary_indent_width + option_width

        if this_width > summary_width
          self.summary_width = this_width
        end
      end

      def fetch_option_hash(*keys)
        hash = config
        keys.flatten.each { |key| hash = hash[key] }
        hash
      end

      def summary_indent_width
        @summary_indent_width ||= 2 * summary_indent.length
      end
    end
  end
end
