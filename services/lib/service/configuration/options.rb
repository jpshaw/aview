require 'ostruct'
require 'deep_merge'
require 'service/configuration/sources/yaml_source'
require 'service/configuration/sources/cli_source'

module Service
  module Configuration
    class Options < OpenStruct
      include Enumerable

      def keys
        marshal_dump.keys
      end

      def empty?
        marshal_dump.empty?
      end

      def add_source!(source)
        @config_sources ||= []
        @config_sources << decorate_source(source)
      end

      def prepend_source!(source)
        @config_sources ||= []
        @config_sources.unshift(decorate_source(source))
      end

      def load!
        conf = {}

        @config_sources.each do |source|
          source_conf = source.load

          if conf.empty?
            conf = source_conf
          else
            DeepMerge.deep_merge!(source_conf, conf,
              preserve_unmergeables: false,
              knockout_prefix:       nil,
              overwrite_arrays:      true)
          end
        end

        # TODO: Add ENV override support
        conf['verbose'] = true if ENV['VERBOSE'].present?

        # Swap out the contents of the OStruct with a hash (need to recursively convert)
        marshal_load(__convert(conf).marshal_dump)

        self
      end
      alias_method :reload!, :load!

      def to_hash
        result = {}
        marshal_dump.each do |k, v|
          if v.instance_of? Service::Configuration::Options
            result[k] = v.to_hash
          elsif v.instance_of? Array
            result[k] = descend_array(v)
          else
            result[k] = v
          end
        end
        result
      end

      def each(*args, &block)
        marshal_dump.each(*args, &block)
      end

      def to_json(*args)
        to_hash.to_json(*args)
      end

      protected

      def decorate_source(source)
        source = YAMLSource.new(source) if source.is_a?(String)
        source = CLISource.new(source)  if source.is_a?(CLI::OptionList)
        source
      end

      def descend_array(array)
        array.map do |value|
          if value.instance_of? Service::Configuration::Options
            value.to_hash
          elsif value.instance_of? Array
            descend_array(value)
          else
            value
          end
        end
      end

      # Recursively converts Hashes to Options (including Hashes inside Arrays)
      def __convert(h) #:nodoc:
        s = self.class.new

        h.each do |k, v|
          k = k.to_s if !k.respond_to?(:to_sym) && k.respond_to?(:to_s)
          s.new_ostruct_member(k)

          if v.is_a?(Hash)
            v = v["type"] == "hash" ? v["contents"] : __convert(v)
          elsif v.is_a?(Array)
            v = v.collect { |e| e.instance_of?(Hash) ? __convert(e) : e }
          end

          s.send("#{k}=".to_sym, v)
        end
        s
      end
    end
  end
end
