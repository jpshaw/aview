module Service
  module Configuration
    module CLI
      class Option < OpenStruct
        def scope_keys
          self.scope ||= ''
          self.scope.split(':')
        end

        def short_switch
          "-#{self.alias}" if self.alias
        end

        def long_switch
          switch = "--#{self.name.to_s.gsub('_','-')}"
          switch << " #{self.banner}" if self.banner
          switch
        end

        def width
          @width ||= [short_switch, long_switch].compact.join(', ').length
        end

        def type_class
          self.type = self.type.downcase.to_sym if self.type.is_a? String

          case self.type
          when :array
            Array
          when :integer, :numeric
            Integer
          when :string
            String
          when :boolean
            nil
          else
            nil
          end
        end

        def to_a
          [short_switch, long_switch, type_class, desc].compact
        end
      end
    end
  end
end
