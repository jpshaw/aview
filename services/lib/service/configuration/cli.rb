require 'scope'
require 'service/configuration/cli/option_list'
require 'service/configuration/cli/option'

module Service
  module Configuration
    module CLI
      extend self

      @options = CLI::OptionList.new
      @scope = Scope.make

      def namespace(name)
        @scope = Scope.new(name, @scope)
        yield
      ensure
        @scope = @scope.tail
      end

      def option(name, args = {})
        option_data = {
          name: name,
          scope: @scope.path
        }.merge(args)

        @options << CLI::Option.new(option_data)
      end

      def options
        @options
      end
    end
  end
end
