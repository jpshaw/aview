require 'ipaddress_formatter'

module Service
  module Utils
    module IPAddress
      def format_ip(address)
        ::IPAddressFormatter.execute(address)
      end

      def format_ip_subnet(subnet)
        format_ip(subnet)
      end

      def format_ip_prefix(prefix)
        prefix.to_i.to_s
      end
    end
  end
end
