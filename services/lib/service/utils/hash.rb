require 'deep_merge'

module Service
  module Utils
    module Hash
      def merge_all(*sources)
        destination = ::HashWithIndifferentAccess.new
        sources.each { |source| destination.deep_merge!(source) }
        destination
      end
    end
  end
end
