require 'service/utils/hash'
require 'service/utils/ipaddress'

module Service
  module Utils
    include Service::Utils::Hash
    include Service::Utils::IPAddress
  end
end
