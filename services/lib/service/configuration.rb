require 'service/configuration/options'
require 'service/configuration/cli'

module Service
  # Copied from the config gem: https://github.com/railsconfig/config
  module Configuration
    extend self
    include Kraken::Logger.console

    mattr_accessor :const_name, :files
    @@const_name = 'Settings'
    @@files = []

    def register_files(*config_files)
      files.push(*config_files)
    end
    alias_method :register_file, :register_files

    def load!
      config = Options.new

      files.compact.uniq.each do |file|
        config.add_source!(file)
      end

      config.add_source!(CLI.options) if CLI.options.any?
      config.load!

      # Define the global config constant with the loaded data
      Kernel.send(:remove_const, const_name) if Kernel.const_defined?(const_name)
      Kernel.const_set(const_name, config)

      # Now you can use `Settings.path.to.config` anywhere in the service

      # Print the config to be sure we're running the right stuff
      logger.info config.to_json

      config
    end
  end
end
