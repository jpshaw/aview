require 'geocode/base_processor'

module Geocode
  class Processor < Geocode::BaseProcessor
    def execute
      geocoder_transaction do |location|
        if location.coordinates.present?
          logger.warn "Coordinates already exist for [#{location_id}]"
        else
          if result = geocoder_search(location.address.to_s)
            logger.debug result.to_json
            location.update_coordinates(
               latitude: result.latitude,
              longitude: result.longitude
            )
          else
            logger.debug "No geocoder result for [#{location_id}]"
          end
        end
      end
    end
  end
end
