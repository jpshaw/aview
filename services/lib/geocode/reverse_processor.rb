require 'geocode/base_processor'

module Geocode
  class ReverseProcessor < Geocode::BaseProcessor
    def execute
      geocoder_transaction do |location|
        if location.address.present?
          logger.warn "Address already exists for [#{location_id}]"
        else
          if result = geocoder_search(location.coordinates.to_s)
            logger.debug result.to_json

            if route = result.address_components_of_type(:route).first
              address_1 = route['short_name']
            end

            location.update_address(
                 address_1: address_1,
                      city: result.city,
                state_code: result.state_code,
                  province: result.state,
               postal_code: result.postal_code,
              country_code: result.country_code
            )
          else
            logger.debug "No reverse geocoder result for [#{location_id}]"
          end
        end
      end
    end
  end
end
