require 'database'
require 'geocoder'
require 'message_processor'
require 'kraken/replicator'

Service.register_config_source :geocoder

Service.after_boot do
  Geocoder.configure(Settings.geocoder.to_h)

  # Replicate location geocode changes to SMX if enabled
  if Settings.replicator.enabled
    Kraken::Replicator.on_replication do |payload|
      MessageBus.publish(payload, topic: Settings.replicator.topic)
    end

    Kraken::Replicator::SMX.load_location!
  end
end

module Geocode
  class BaseProcessor < ::MessageProcessor

    def geocoder_search(query)
      logger.debug query
      retry_count ||= 3
      best_geocoder_result(Geocoder.search(query))
    rescue Geocoder::InvalidRequest => ex
      logger.error "#{ex.message}: \"#{query}\""
      raise ex
    rescue Timeout::Error, SocketError => ex
      logger.error "Timeout/Socket error (attempt = #{retry_count})"
      logger.error ex
      retry unless (retry_count -= 1).zero?
      raise ex
    end

    def geocoder_transaction(&block)
      location = Location.find_by!(id: location_id)
      location.geocode_started!

      yield(location) if block_given?

      location.geocode_finished!
    rescue Geocoder::InvalidRequest
      location.geocode_failed! if location
    rescue => ex
      location.geocode_failed! if location
      raise ex
    end

    def location_id
      message_data[:id]
    end

    private

    # Google geocoder results precision
    # https://developers.google.com/maps/documentation/geocoding/intro#GeocodingResponses
    #
    # "ROOFTOP" indicates that the returned result is a precise geocode for which
    # we have location information accurate down to street address precision.
    #
    # "RANGE_INTERPOLATED" indicates that the returned result reflects an
    # approximation (usually on a road) interpolated between two precise points
    # (such as intersections). Interpolated results are generally returned when
    # rooftop geocodes are unavailable for a street address.
    #
    # "GEOMETRIC_CENTER" indicates that the returned result is the geometric
    # center of a result such as a polyline (for example, a street) or polygon
    # (region).
    #
    # "APPROXIMATE" indicates that the returned result is approximate.
    #
    ROOFTOP            = 'ROOFTOP'.freeze
    RANGE_INTERPOLATED = 'RANGE_INTERPOLATED'.freeze
    GEOMETRIC_CENTER   = 'GEOMETRIC_CENTER'.freeze
    APPROXIMATE        = 'APPROXIMATE'.freeze

    PRECISIONS = [
      ROOFTOP,
      RANGE_INTERPOLATED,
      GEOMETRIC_CENTER,
      APPROXIMATE
    ].freeze

    def best_geocoder_result(results)
      if results.blank?
        return nil
      elsif results.size == 1
        return results.first
      end

      # TODO: If result set is found, pick one that has the best data, instead
      # of just the first result.
      #
      # See ARMT issue with lat/long reverse lookup
      PRECISIONS.each do |precision|
        result = results.find { |r| r.precision == precision }
        return result if result
      end

      nil
    end
  end
end
