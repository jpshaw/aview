require 'active_support/notifications'
require 'kafka'
require 'kafka/datadog'

Service.register_config_source :message_bus

Service.register_cli_options do
  namespace :message_bus do
    option :brokers,  alias: :b, type: :array,   desc: 'Message bus brokers',        banner: 'B1,B2,B3'
    option :group_id, alias: :g, type: :string,  desc: 'Message bus consumer group', banner: 'GROUP'
    option :topic,    alias: :t, type: :string,  desc: 'Message bus consumer topic', banner: 'TOPIC'
    option :offset,   alias: :o, type: :integer, desc: 'Message bus topic offset',   banner: 'OFFSET'

    namespace :producer do
      option :threshold, desc: 'Number of messages to trigger delivery',
        alias: :T, type: :numeric, banner: 'INT'

      option :interval, desc: 'Number of seconds between automatic delivery',
        alias: :I, type: :numeric, banner: 'INT'
    end
  end
end

Service.before_shutdown do
  MessageBus.shutdown
end

module MessageBus
  extend self
  include Kraken::Logger.console

  @consumers = []
  @producers = []

  def each_message(group_id: config.group_id, topic: config.topic, &block)
    consumer(group_id: group_id, topic: topic).each_message do |message|
      begin
        logger.debug { message.value }
        yield(message.value) if block_given?
      rescue => ex
        logger.error message.value # Print offending message
        logger.error "Exception raised when processing " \
                     "#{message.topic}/#{message.partition} " \
                     "at offset #{message.offset}:"
        logger.error ex

        # Don't supress exception by default - let service decide to fail
        raise ex
      end
    end
  end

  # TODO: Add support for paritions?
  def consumer(group_id:, topic:, partition: 0)
    raise ArgumentError.new('Message bus group_id is blank') if group_id.blank?
    raise ArgumentError.new('Message bus topic is blank') if topic.blank?

    consumer = client.consumer(group_id: group_id)
    consumer.subscribe(topic)
    consumer.seek(topic, partition, config.offset) if config.offset
    @consumers << consumer
    consumer
  end

  def produce(data, topic:)
    producer.produce(serialize(data), topic: topic)
  rescue Kafka::BufferOverflow
    logger.warn 'Message bus buffer overflow'
    sleep 1
    retry
  end
  alias_method :publish, :produce

  def producer
    @producer ||= begin
      producer = client.async_producer(
        delivery_threshold: config.producer.threshold,
        delivery_interval: config.producer.interval,
      )
      @producers << producer
      producer
    end
  end

  def client(brokers = config.brokers)
    raise ArgumentError.new('Message bus brokers is empty') if brokers.blank?
    @client ||= Kafka.new(seed_brokers: brokers, logger: logger)
  end

  def shutdown
    @consumers.each(&:stop)
    @producers.each(&:shutdown)
  end

  private

  def config
    Settings.message_bus
  end

  def serialize(data)
    case data
    when String
      data
    when Hash, Array
      data.to_json
    else
      data.to_s
    end
  end
end
