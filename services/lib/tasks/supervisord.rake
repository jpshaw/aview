namespace :supervisord do
  
  def service_file?(path)
    !path.end_with?('console') && File.readlines(path).grep(/boot/).size > 0
  end

  def supervisord_config_path(name)
    Service.root.join('config/supervisord').join("#{name.dasherize}.conf")
  end

  def supervisord_config(name)
    dashed_name = name.dasherize
    <<-HEREDOC
[program:#{dashed_name}]
command=%(ENV_AVIEW_DIR)s/var/webapp/current/services/bin/launch #{name}
autostart=true
autorestart=false
startsecs=10
stdout_logfile=%(ENV_AVIEW_DIR)s/log/#{dashed_name}.log
stdout_logfile_maxbytes=50MB
stdout_logfile_backups=5
redirect_stderr=true
stopasgroup=true
environment=
  RAILS_ENV=production
    HEREDOC
  end

  desc 'Generate supervisord configurations from a template'
  task :generate do
    Dir.glob(Service.root.join('bin/*')).each do |file_path|
      if service_file?(file_path)
        name = file_path.split('/').last
        config_path = supervisord_config_path(name)

        unless File.exist?(config_path)
          File.open(config_path, 'w') do |file|
            file.write(supervisord_config(name))
          end
        end
      end
    end
  end
end
