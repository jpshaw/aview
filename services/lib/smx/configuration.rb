require 'smx'
require 'database'
require 'mac_address'
require 'service/utils'

module SMX
  InvalidConfigurationError = Class.new(StandardError)

  class Configuration
    include Kraken::Logger.console
    include Service::Utils
    extend Memoist

    DEFAULT_MODEL = 'DEFAULT-MODEL'.freeze
    SYSTEM_NAME   = 'SYSTEM-DEFAULTS'.freeze

    class << self
      extend Memoist

      def model_name(name)
        @model_name = name
      end

      def config_model
        @model_name.to_s.classify.constantize
      end
      memoize :config_model

      def fetch_record(name, resource = nil)
        config_model.where(name: name, resource: resource).first_or_create!
      end
    end

    delegate :fetch_record, to: :class

    def initialize(data:, meta:)
      @data = data || {}
      @meta = meta || {}
      validate!
    end

    def configuration
      fetch_record(name, resource)
    end
    memoize :configuration

    # Only models and devices have parents
    #
    # if resource == device, parent = model
    # if resource == model,  parent = system
    # if resource == system, parent = nil
    def parent
      case type
      when :device
        fetch_record(model_name, resource.organization)
      when :model
        fetch_record(SYSTEM_NAME)
      else
        nil
      end
    end
    memoize :parent

    def action
      meta[:action].to_s.downcase.to_sym
    end
    memoize :action

    # We *should* be using `strptime` to parse these, but since we don't
    # have timezone information the conversions can be incorrect.
    def history_data
      {
        created_by: data[:creator],
        created_on: format_timestamp(data[:createdTime]),
        updated_by: data[:updater],
        updated_on: format_timestamp(data[:updatedTime]),
      }
    end
    memoize :history_data

    private

    attr_reader :data, :meta

    def name
      data[:deviceId]
    end

    def account_name
      data[:account]
    end

    NULL_TIMESTAMP = '0001-01-01-00.00.00.000000'.freeze

    def format_timestamp(timestamp)
      if timestamp.present? && timestamp != NULL_TIMESTAMP
        year, month, day, time = timestamp.split('-')
        hour, minute, second, _ = time.split('.')
        "#{year}-#{month}-#{day} #{hour}:#{minute}:#{second}"
      else
        ''
      end
    end

    def model_name
      if data[:modelId].present?
        data[:modelId]
      elsif resource.respond_to?(:configuration_name) && resource.configuration_name.present?
        resource.configuration_name
      else
        DEFAULT_MODEL
      end
    end
    memoize :model_name

    # Configuration resource (device, organization)
    def resource
      case type
      when :device
        ::Gateway.find_by!(mac: name)
      when :model
        ::Organization.find_by!(name: account_name)
      else
        nil # System configs don't have resources (yet)
      end
    end
    memoize :resource

    def type
      if is_device_type?
        :device
      elsif is_model_type?
        :model
      elsif is_system_type?
        :system
      else
        nil
      end
    end
    memoize :type

    def is_device_type?
      name.valid_mac?
    end
    memoize :is_device_type?

    # We *could* check data[:modelInd], but there are replication records for
    # models where the field appears to be empty. More testing is needed.
    def is_model_type?
      !is_device_type? && !is_system_type?
    end
    memoize :is_model_type?

    def is_system_type?
      name == SYSTEM_NAME
    end
    memoize :is_system_type?

    def validate!
      if name.blank?
        raise SMX::InvalidConfigurationError.new('Configuration :name must be given')
      end

      if type.blank?
        raise SMX::InvalidConfigurationError.new('Configuration type must be one of (device, model, system)')
      end
    end
  end
end
