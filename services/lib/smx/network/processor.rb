require 'message_processor'
require 'smx/network'

class SMX::Network::Processor < ::MessageProcessor
  def execute
    network = SMX::Network.new(data: message_data, meta: message_meta)

    configuration = network.configuration
    parent        = network.parent
    attributes    = network.attributes

    configuration.parent_id = parent.id if parent
    configuration.update(attributes)
  end
end
