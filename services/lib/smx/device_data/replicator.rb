require 'smx'
require 'message_processor'
require 'smx/rest/client'
require 'smx/device_data'

class SMX::DeviceData::Replicator < ::MessageProcessor
  def self.client
    @client ||= SMX::REST::Client.new
  end
  delegate :client, to: :class

  def execute
    data = ::DeviceDataMapper.gateway_universal_to_smx(message_data)
    principal_id = message_hash['principal_id']
    client.update_device(data, principal_id)
  end
end
