require 'device-datamapper'
require 'database'
require 'ipaddress'
require 'message_processor'
require 'finders/contact_finder'
require 'smx/device_data'

Service.after_boot do
  Kraken::Location.on_geocode do |payload|
    MessageBus.produce(payload, topic: Settings.locations.geocoder.topic)
  end

  Kraken::Location.on_reverse_geocode do |payload|
    MessageBus.produce(payload, topic: Settings.locations.reverse_geocoder.topic)
  end
end

class SMX::DeviceData::Processor < ::MessageProcessor

  def execute
    @data   = ::DeviceDataMapper.gateway_smx_to_universal(message_data)
    @device = ::Gateway.find_by!(mac: data[:device_id])

    update_location
    update_contact
    update_details
    update_configuration
    update_management_host
    update_configuration_checked_at
    update_site_name
    update_serial
    update_hw_version

    device.save!
  end

  private

  attr_reader :data, :device

  # TODO: Move location processor to it's own service
  def update_location
    location = ::Kraken::Location.upsert(data[:location])

    if location.nil?
      device.location = nil
    elsif location.errors.any?
      logger.error "Location errors for device[#{device.mac}]: " \
                   "#{location.errors.full_messages.join("\n")}"
    else
      device.location = location
      replicate_location_if_needed(location)
    end
  end

  def update_contact
    contact = ::ContactFinder.new(data[:contact]).execute

    if contact
      device.contacts << contact unless device.contacts.exists?(contact.id)
      device.organization.contacts << contact unless device.organization.contacts.exists?(contact.id)

      # Clear extra contacts
      device.contacts.where.not(id: contact.id).each do |extra_contact|
        device.contacts.delete(extra_contact)
      end
    else
      device.contacts.delete_all
    end
  end

  def update_details
    return if data[:details].blank?

    details = device.details || device.build_details
    details.assign_attributes(data[:details])
    details.save
  end

  def update_configuration
    return if data[:ssl].blank?

    configuration = device.configuration || device.build_configuration
    portal        = configuration.portal || configuration.build_portal

    portal.ssl_password = data[:ssl][:password]
    portal.save

    configuration.portal = portal
    configuration.save

    device.configuration = configuration
  end

  def update_management_host
    return if data[:maintenance_tunnel_address].blank?

    management_host = data[:maintenance_tunnel_address]

    begin
      device.mgmt_host = ::IPAddress.parse(management_host).address
    rescue ArgumentError => ex
      logger.error ex.message # Invalid IP
    end
  end

  # Last Accessed At is the Date and Time the AT&T VPN Gateway last queried
  # Service Manager to retrieve the configuration.
  #
  # Examples:
  #   2017-10-18-10.53.00.370524
  #   2017-10-18 AT 10:32:55
  def update_configuration_checked_at
    return if data[:last_accessed_at].blank?

    last_accessed_at = data[:last_accessed_at]

    begin
      checked_at =
        if last_accessed_at.include?('AT')
          DateTime.strptime(last_accessed_at, '%Y-%m-%d AT %H:%M:%S')
        else
          DateTime.strptime(last_accessed_at, '%Y-%m-%d-%H.%M.%S')
        end

      device.configuration_checked_at = checked_at
    rescue ArgumentError => ex
      logger.error "#{ex.message}: #{last_accessed_at}" # Invalid Date
    end
  end

  def update_site_name
    site_name = data[:site_name].presence

    device.site =
      if site_name
        device.organization.sites.where(name: site_name).first_or_create
      else
        device.organization.sites.default
      end
  end

  def update_serial
    device.serial = data[:serial] if data[:serial].present?
  end

  def update_hw_version
    device.hw_version = data[:hw_version] if data[:hw_version].present?
  end

  ADDRESS_KEYS = [:address_1, :city, :state_code, :province, :postal_code, :country_code].freeze
  COORDINATES_KEYS = [:latitude, :longitude].freeze

  def replicate_location_if_needed(location)
    smx_coordinates = smx_location_slice(*COORDINATES_KEYS)
    smx_address     = smx_location_slice(*ADDRESS_KEYS)

    should_replicate_coordinates = smx_address.present? && smx_coordinates.blank? && location.coordinates.present?
    should_replicate_address     = smx_coordinates.present? && smx_address.blank? && location.address.present?

    if should_replicate_coordinates
      replicate_location_data(location.coordinates.to_h)
    elsif should_replicate_address
      replicate_location_data(location.address.to_h)
    end
  end

  def smx_location_slice(*keys)
    data
      .fetch(:location, {})
      .slice(*keys)
      .values
      .map(&:presence)
      .compact
  end

  def replicate_location_data(location_data)
    MessageBus.publish(
      {
        data: {
          device_id: device.mac,
          account_id: device.organization.name,
          location: location_data
        }
      },
      topic: Settings.replicator.topic
    )
  end
end
