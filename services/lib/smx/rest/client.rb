require 'typhoeus'

Service.register_config_source :smx_rest

module SMX
  module REST
    class Client
      include Kraken::Logger.console

      HEADERS = {
        'Accept'       => 'application/json',
        'Content-Type' => 'application/json'
      }.freeze

      def initialize
        @base_url = Settings.smx.rest.base_url
      end

      def update_device(data, principal_id)
        request = build_request(data, principal_id)

        request.on_complete do |response|
          if response.success?
            true
          elsif response.timed_out?
            logger.error "Time out"
          elsif response.code == 0
            logger.error "Could not get an http response, something's wrong."
            logger.error response.return_message
          else
            logger.error "HTTP request failed: " + response.code.to_s
          end
        end

        request.run
      end

      private

      attr_reader :base_url

      def build_request(data, principal_id)
        account_id = data[:account]
        device_id  = data[:deviceId]
        url        = device_url(account_id, device_id)
        body       = { data: data, principal_id: principal_id }.to_json
        options    = { method: :put, headers: HEADERS, body: body }

        Typhoeus::Request.new(url, options)
      end

      def device_url(account_id, device_id)
        "#{base_url}/accounts/#{account_id}/gateways/#{device_id}"
      end
    end
  end
end
