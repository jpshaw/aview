require 'smx/configuration'

module SMX
  class InternetRoute < SMX::Configuration

    model_name :internet_routes_configuration

    def attributes
      { data: merge_all(ip_data, history_data, description_data) }
    end
    memoize :attributes

    private

    def ip_data
      {
        ip_address: ip_address,
        subnet_mask: subnet_mask
      }
    end

    def description_data
      { description: data[:description] }
    end

    def ipv4?
      data[:ipAddress].present?
    end
    memoize :ipv4?

    def ip_address
      if ipv4?
        format_ip(data[:ipAddress])
      else
        format_ip(data[:ipv6Address])
      end
    end

    def subnet_mask
      if ipv4?
        format_ip_subnet(data[:subnetMask])
      else
        format_ip_prefix(data[:ipAddressPrefix])
      end
    end

  end
end
