require 'message_processor'
require 'smx/internet_route'
require 'smx/utils'

class SMX::InternetRoute::Processor < ::MessageProcessor
  extend Memoist
  include SMX::Utils

  def execute
    configuration.parent_id = parent.id if parent

    case action
    when :create then upsert
    when :update then upsert
    when :upsert then upsert
    when :delete then delete
    end
  end

  private

  delegate :configuration, :parent, :action, to: :internet_route

  def internet_route
    SMX::InternetRoute.new(data: message_data, meta: message_meta)
  end
  memoize :internet_route

  def data
    internet_route.attributes[:data]
  end

  def find
    configuration.routes.find { |route| data_matches?(route) }
  end

  def create
    configuration.update(routes: configuration.routes.push(data))
  end

  def update
    updated_routes = configuration.routes.map do |route|
      data_matches?(route) ? data_for_update(data, route) : route
    end
    configuration.update(routes: updated_routes)
  end

  def upsert
    find ? update : create
  end

  def delete
    configuration.routes.delete_if { |route| data_matches?(route) }
    configuration.save
  end

  def data_matches?(route)
    route[:ip_address] == data[:ip_address]
  end

end
