require 'message_processor'
require 'smx/cascaded_network'
require 'smx/utils'

class SMX::CascadedNetwork::Processor < ::MessageProcessor
  extend Memoist
  include SMX::Utils

  def execute
    configuration.parent_id = parent.id if parent

    case action
    when :create then upsert
    when :update then upsert
    when :upsert then upsert
    when :delete then delete
    end
  end

  private

  delegate :configuration, :parent, :action, to: :cascaded_network

  def cascaded_network
    SMX::CascadedNetwork.new(data: message_data, meta: message_meta)
  end
  memoize :cascaded_network

  def data
    cascaded_network.attributes[:data]
  end

  def find
    configuration.cascaded_networks.find do |network|
      data_matches?(network)
    end
  end

  def create
    configuration.update(
      cascaded_networks: configuration.cascaded_networks.push(data)
    )
  end

  def update
    updated_networks = configuration.cascaded_networks.map do |network|
      data_matches?(network) ? data_for_update(data, network) : network
    end
    configuration.update(cascaded_networks: updated_networks)
  end

  def upsert
    find ? update : create
  end

  def delete
    configuration.cascaded_networks.delete_if do |network|
      data_matches?(network)
    end
    configuration.save
  end

  def data_matches?(network)
    network[:ip_address] == data[:ip_address]
  end
end
