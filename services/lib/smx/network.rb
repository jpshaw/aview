require 'smx/configuration'

module SMX
  class Network < SMX::Configuration

    model_name :network_configuration

    def attributes
      { data: merge_all(network_data, history_data, access_data) }
    end
    memoize :attributes

    private

    def network_data
      mapped_data[:network] || {}
    end

    def access_data
      { last_access_on: mapped_data[:last_accessed_at] }
    end

    def mapped_data
      ::DeviceDataMapper.gateway_config_smx_to_ui(data)
    end
    memoize :mapped_data

  end
end
