require 'smx/configuration'

module SMX
  class CascadedNetwork < SMX::Configuration

    model_name :cascaded_networks_configuration

    def attributes
      { data: merge_all(network_data, history_data, description_data) }
    end
    memoize :attributes

    private

    def network_data
      {
        ip_address: ip_address,
        subnet_mask: subnet_mask,
        router_ip_address: router_ip_address,
        routing_metric: routing_metric,
        local_lan_alias: local_lan_alias,
        internet_only_route: internet_only_route,
        restrict_vpn_advertisement: restrict_vpn_advertisement
      }
    end

    def description_data
      {
        description: data[:description]
      }
    end

    def ipv4?
      data[:ipAddress].present?
    end
    memoize :ipv4?

    def ip_address
      if ipv4?
        format_ip(data[:ipAddress])
      else
        format_ip(data[:ipv6Address])
      end
    end

    def subnet_mask
      if ipv4?
        format_ip_subnet(data[:subnetMask])
      else
        format_ip_prefix(data[:ipAddressPrefix])
      end
    end

    def router_ip_address
      if ipv4?
        format_ip(data[:routerIPAddress])
      else
        format_ip(data[:ipv6RouterAddr])
      end
    end

    def routing_metric
      format_metric(data[:routingMetric])
    end

    def local_lan_alias
      booleanize(data[:lanAliasInd])
    end

    def internet_only_route
      booleanize(data[:internetOnlyInd])
    end

    def restrict_vpn_advertisement
      booleanize(data[:restrictVPNAdv])
    end

    def format_metric(metric)
      metric.present? ? metric.to_i.to_s : metric.to_s
    end

    def booleanize(field)
      field == 'Y' ? 'true' : 'false'
    end

  end
end
