module SMX
  module Utils

    # Replications for `update` have the `creator` data blanked out, so we
    # must merge it in from the old record.
    def data_for_update(data, record)
      data.merge(record.slice(:created_by, :created_on))
    end

  end
end
