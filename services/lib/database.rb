require 'erb'
require 'yaml'
require 'active_record'
require 'closure_tree'

module Database
  extend self
  include Kraken::Logger.console

  POOL_DEFAULT = 3

  def load
    database_file      = Service.app_root.join('config', 'database.yml')
    database_config    = YAML::load(ERB.new(IO.read(database_file)).result)
    environment_config = database_config[Service.environment] || {}
    database_pool      = ENV['RAILS_MAX_THREADS'] || config.pool || POOL_DEFAULT

    # Override application database connection pool
    environment_config.merge!(
      'pool' => database_pool.to_i
    )

    logger.debug environment_config.to_json

    ActiveRecord::Base.logger = logger
    ActiveRecord::Base.establish_connection(environment_config)

    Dir[File.expand_path('../models/*.rb', __FILE__)].each do |file|
      require file
    end
  end

  def config
    Settings.database
  end
end

Service.register_config_source :database

Service.register_cli_options do
  namespace :database do
    option :pool, alias: :p, type: :integer, desc: 'Database pool', banner: 'INT'
  end
end

Service.after_boot do
  Database.load
end
