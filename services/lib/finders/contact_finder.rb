class ContactFinder
  include Kraken::Logger.console

  def initialize(params)
    @params = params || {}
  end

  def execute
    return unless params.any? { |_,v| v.present? }

    Contact.find_by(token: contact_token) || Contact.create!(params)

  rescue ActiveRecord::RecordInvalid, ActiveRecord::StatementInvalid => ex
    logger.error "#{ex.message} - #{params}"
    nil
  end

  private

  attr_reader :params

  def contact_token
    Contact.tokenize(params)
  end

end
