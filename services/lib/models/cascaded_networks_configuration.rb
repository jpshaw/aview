require 'models/configuration' # TODO: Fix load order

class CascadedNetworksConfiguration < ::Configuration

  store :data, accessors: [ :cascaded_networks ], coder: JSON

  after_initialize :init

  private

  def init
    self.cascaded_networks ||= []
  end
end
