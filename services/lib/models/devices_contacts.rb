class DevicesContacts < ActiveRecord::Base

  belongs_to :device
  belongs_to :contact

end
