class Site < ActiveRecord::Base

  belongs_to :organization
  has_many :devices

  DEFAULT_NAME = 'Default'

  def self.default
    where(name: DEFAULT_NAME).first_or_create
  end

end
