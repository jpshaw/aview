require 'models/configuration'

class NetworkConfiguration < ::Configuration

  store :data, accessors: [
    :dhcp,
    :lan,
    :intelliflow_enabled,
    :ids_enabled
  ], coder: JSON

end
