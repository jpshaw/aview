class Contact < ActiveRecord::Base

  has_many :organizations_contacts, class_name: 'OrganizationsContacts'
  has_many :organizations, through: :organizations_contacts
  has_many :devices_contacts, class_name: 'DevicesContacts'
  has_many :devices, through: :devices_contacts

  before_save :set_token

  # Produce a somewhat-consistent digest for a given contact. This will become
  # obsolete once users can manage contacts.
  def self.tokenize(attributes)
    Digest::MD5.hexdigest(
      attributes
        .with_indifferent_access
        .slice(:name, :phone, :email)
        .values
        .compact
        .join(' ')
        .upcase
    )
  end

  private

  def set_token
    self.token ||= self.class.tokenize(attributes)
  end

end
