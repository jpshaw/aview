class Device < ActiveRecord::Base

  belongs_to :organization
  belongs_to :location
  belongs_to :site
  has_many   :devices_contacts, class_name: 'DevicesContacts'
  has_many   :contacts, through: :devices_contacts

  self.inheritance_column = :object_type

end
