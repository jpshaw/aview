class GatewayDetails < ActiveRecord::Base

  belongs_to :device, class_name: 'Gateway'

end
