require 'models/device'

class Gateway < Device

  has_one :details, class_name: 'GatewayDetails', foreign_key: :device_id, dependent: :destroy
  belongs_to :configuration, class_name: 'GatewayConfiguration'

  def configuration_name
    details && details.configuration_name
  end

end
