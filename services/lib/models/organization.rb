class Organization < ActiveRecord::Base

  has_many :devices
  has_many :locations
  has_many :sites
  has_many :organizations_contacts, class_name: 'OrganizationsContacts'
  has_many :contacts, through: :organizations_contacts

end
