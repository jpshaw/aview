class Configuration < ActiveRecord::Base

  self.inheritance_column = :type

  has_closure_tree

  store :data, coder: JSON

  belongs_to :resource, polymorphic: true

  validates :name, presence: true

end
