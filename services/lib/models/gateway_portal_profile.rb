class GatewayPortalProfile < ActiveRecord::Base

  before_save :set_auth_user

  private

  def set_auth_user
    self.auth_user ||= 'support'
  end

end
