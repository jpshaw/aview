class OrganizationsContacts < ActiveRecord::Base

  belongs_to :organization
  belongs_to :contact

end
