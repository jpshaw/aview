class GatewayConfiguration < ActiveRecord::Base

  belongs_to :portal, class_name: 'GatewayPortalProfile'
  belongs_to :organization

end
