require 'kraken/geolocation'

class Location < ActiveRecord::Base

  acts_as_geolocation

  has_many   :devices
  belongs_to :country
  belongs_to :organization

end
