require 'ipaddress'

module IPAddressFormatter
  extend self
  include Kraken::Logger.console

  EMPTY_IP = ''.freeze

  def execute(input)
    return EMPTY_IP if input.blank?

    ip = ::IPAddress.parse(input)

    if ip.ipv4?
      # We *could* use ip.to_s here, but SMX had to ruin the fun and send
      # addresses with left-padded zeroes, such as "108.004.034.076" instead
      # of "108.4.34.76".
      ip.octets.join('.')
    else
      ip.compressed.upcase
    end

  # Invalid IP
  rescue ArgumentError
    logger.error "Invaild IP: '#{input}'"
    EMPTY_IP

  # TODO: Figure out why some addresses from SMX throw weird exceptions
  rescue => ex
    logger.error "IPAddress.parse('#{input}') failed: #{ex.message}"
    EMPTY_IP
  end
end
