# SMX Identity Service

API built with Finatra for getting Service Manager Users and Authorization

* http://twitter.github.io/finatra/
* https://github.com/twitter/finatra

## Building

```bash
  sbt assembly
```

## Docker

```bash
  docker build -t smx-identity-service .
  docker run -d --name smx-identity-service -p 8888:8888 -p 9990:9990 smx-identity-service
```

## Configuration

There are several parameters needed to open a connection to Service Manager. Most
of these are defaulted in the client code but can be overridden with environment
variables:

```bash
  export SM_APP_NAME="ACCELERATED"
  export SM_DOMAIN="US"
  export SM_SECRET="<SECRET>"
```

The path to the `smx.properties` file is always required. See `config/smx.properties`
file for what parameters are available.

```bash
  export SM_CONFIG="/path/to/smx.properties"
```

# Usage

There are two parameters required by the service, `account-id` and `user-id`,
expected with the uri format:

```
  /accounts/account-id/users/user-id/authorization
```

Example:

```bash
  $ curl http://127.0.0.1:8888/accounts/accarmt/users/zaccari/authorization
  > {"data":{"level":3}}
```

# Admin Panel

Finatra provides an optional admin panel exposed on port `9990` that provides
a wealth of information.
