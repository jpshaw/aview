package com.accelerated.smx.identity.domain

import com.twitter.finatra.domain.WrappedValue

case class AccountId(
  account_id: String)
  extends WrappedValue[String]
