package com.accelerated.smx.identity.domain.http

import com.twitter.finatra.request.RouteParam
import com.accelerated.smx.identity.domain.{AccountId, UserId}

case class AuthorizationGetRequest(
  @RouteParam account_id: AccountId,
  @RouteParam user_id: UserId)
