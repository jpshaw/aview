package com.accelerated.smx.identity.services

import com.accelerated.smx.identity.domain.http.{AuthorizationResponse, AuthorizationData}
import com.accelerated.smx.identity.domain.{AccountId, UserId}
import com.accelerated.smx.{ServiceManagerClient => SmxClient}
import javax.inject.Inject
import com.twitter.util.Future

class SmxAuthorizationService @Inject() (
  client: SmxClient)
  {

  def getResponseAuthorization(accountId: AccountId, userId: UserId): AuthorizationData = {
    val user = client.getUser(accountId.toString, userId.toString)

    val authLevel = user match {
      case Some(user) => user.armtAccess
      case None => -1
    }

    AuthorizationResponse(authLevel).toDomain
  }

}
