package com.accelerated.smx.identity.modules

import com.google.inject.{Provides, Singleton}
import com.twitter.inject.TwitterModule
import com.accelerated.smx.ServiceManagerClient

object SmxClientModule
  extends TwitterModule {

  @Singleton
  @Provides
  def providesSmxClient(): ServiceManagerClient = {
    new ServiceManagerClient()
  }
}
