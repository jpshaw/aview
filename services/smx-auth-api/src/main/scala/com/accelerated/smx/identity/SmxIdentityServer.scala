package com.accelerated.smx.identity

import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.CommonFilters
import com.twitter.finatra.http.routing.HttpRouter

import com.accelerated.smx.identity.controllers.AuthsController
import com.accelerated.smx.identity.modules.SmxClientModule

object SmxIdentityServerMain extends SmxIdentityServer

class SmxIdentityServer extends HttpServer {

  // Admin server is enabled by default
  // override val disableAdminHttpServer = true

  override def modules = Seq(SmxClientModule)

  override def configureHttp(router: HttpRouter): Unit = {
    router
      .filter[CommonFilters]
      .add[AuthsController]
  }

  // TODO: Add warmup

}
