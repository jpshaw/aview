package com.accelerated.smx.identity.controllers

import javax.inject.{Inject, Singleton}

import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller

import com.accelerated.smx.identity.domain.http.AuthorizationGetRequest
import com.accelerated.smx.identity.services.SmxAuthorizationService

@Singleton
class AuthsController @Inject()(
  smxAuthorizationService: SmxAuthorizationService)
  extends Controller {

  get("/accounts/:account_id/users/:user_id/authorization") { authorizationGetRequest: AuthorizationGetRequest =>
    smxAuthorizationService.getResponseAuthorization(
      authorizationGetRequest.account_id,
      authorizationGetRequest.user_id
    )
  }
}
