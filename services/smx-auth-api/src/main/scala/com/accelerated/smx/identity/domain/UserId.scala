package com.accelerated.smx.identity.domain

import com.twitter.finatra.domain.WrappedValue

case class UserId(
  user_id: String)
  extends WrappedValue[String]
