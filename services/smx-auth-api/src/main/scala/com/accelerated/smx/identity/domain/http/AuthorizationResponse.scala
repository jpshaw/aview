package com.accelerated.smx.identity.domain.http

import com.accelerated.smx.identity.domain.{Authorization, AccountId, UserId}

case class AuthorizationData(
  data: Authorization
)

case class AuthorizationResponse(
  level: Int) {

  def toDomain = {
    AuthorizationData(
      Authorization(
        level
      )
    )
  }
}
