import Dependencies._

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

lazy val versions = new {
  val finatra = "2.7.0"
}

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.accelerated",
      scalaVersion := "2.11.8",
      version      := "0.2.0"
    )),
    name := "smx-identity-service",
    libraryDependencies ++= Seq(
      "com.twitter" %% "finatra-http" % versions.finatra
    ),
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      "Twitter Maven" at "https://maven.twttr.com"
    )
  )
