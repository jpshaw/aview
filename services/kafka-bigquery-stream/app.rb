#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'kafka'
require 'kafka/datadog'
require 'google/cloud/bigquery'
require 'logger'

$stdout.sync = true
$stderr.sync = true

logger = Logger.new(STDOUT)
logger.level = ENV['DEBUG'] ? Logger::DEBUG : Logger::INFO
logger.info "Running in #{RUBY_DESCRIPTION}"

kafka_brokers         = ENV['KAFKA_BROKERS'].to_s.split(',')
kafka_group           = ENV['KAFKA_GROUP'] || 'kraken-bigquery-streamer'
kafka_topic           = ENV['KAFKA_TOPIC']
kafka_topic_offset    = ENV['KAFKA_TOPIC_OFFSET'].to_i
kafka_topic_partition = ENV['KAFKA_TOPIC_PARTITION'].to_i
bigquery_dataset      = ENV['BIGQUERY_DATASET'] || 'application'
bigquery_table        = ENV['BIGQUERY_TABLE']

raise 'Please set KAFKA_BROKERS'  if kafka_brokers.empty?
raise 'Please set KAFKA_TOPIC'    unless kafka_topic
raise 'Please set BIGQUERY_TABLE' unless bigquery_table

kafka = Kafka.new(seed_brokers: kafka_brokers, logger: logger)
consumer = kafka.consumer(group_id: kafka_group)
consumer.subscribe(kafka_topic)
consumer.seek(kafka_topic, kafka_topic_partition, kafka_topic_offset) unless kafka_topic_offset.zero?

trap('TERM') { consumer.stop }

bigquery = Google::Cloud::Bigquery.new
dataset = bigquery.dataset(bigquery_dataset)
table = dataset.table(bigquery_table)

consumer.each_batch do |batch|
  logger.info "Received batch: #{batch.topic}/#{batch.partition}/#{batch.messages.count}"

  rows = []
  insert_ids = []

  batch.messages.each do |message|
    rows << JSON.parse(message.value)
    insert_ids << message.offset
  end

  # Use insert_ids for data consistency and help with deduplication
  # https://cloud.google.com/bigquery/streaming-data-into-bigquery#dataconsistency
  logger.info "Sending #{rows.count} rows"
  table.insert rows, insert_ids: insert_ids
end
