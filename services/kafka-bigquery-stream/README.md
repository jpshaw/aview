
## Start

```bash
bundle exec ruby app.rb
```

## Examples

```bash
GOOGLE_CLOUD_PROJECT="aview-staging" \
GOOGLE_CLOUD_KEYFILE="./aview-staging-keyfile.json" \
KAFKA_BROKERS="staging-kafka-001:9092,staging-kafka-002:9092,staging-kafka-003:9092,staging-kafka-004:9092" \
KAFKA_TOPIC="accns.staging.aview.events.metrics.cellular-utilization" \
BIGQUERY_TABLE="metrics_cellular_utilization" \
bundle exec ruby app.rb
```
