#!/usr/bin/env bash

# Output colors
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'


# Stop and remove any running containers
cleanup () {
  docker-compose -f docker-compose.yml -f docker-compose-test.yml -p ci stop
  docker-compose -f docker-compose.yml -f docker-compose-test.yml -p ci rm -f -v
}

# Catch unexpected failures, do cleanup and output an error message
trap 'cleanup ; printf "${RED}Tests failed for unexpected reasons${NC}\n"'\
  HUP INT QUIT PIPE TERM

# TODO: Build in separate script
docker-compose -f docker-compose.yml -f docker-compose-test.yml -p ci up -d

if [ $? -ne 0 ] ; then
  printf "${RED}Docker Compose Failed${NC}\n"
  cleanup
  exit -1
fi

# Wait for the test service to complete and grab the exit code
TEST_EXIT_CODE=`docker wait ci_integration-tests_1`

# Output test logs (for clarity)
docker logs ci_integration-tests_1

# Inspect the output of the test and display respective message
if [[ -z ${TEST_EXIT_CODE+x} ]] || [[ "$TEST_EXIT_CODE" -ne 0 ]] ; then
  printf "${RED}Tests Failed${NC} - Exit Code: $TEST_EXIT_CODE\n"
else
  printf "${GREEN}Tests Passed${NC}\n"
fi

# call the cleanup fuction
cleanup

# exit the script with the same code as the test service code
exit $TEST_EXIT_CODE
