module SmxData
  class Record
    TRUE  = 'Y'
    FALSE = 'N'

    def initialize(data, data_type)
      @data = data || {}
      @data_type = data_type
    end

    def armt_enabled?
      @data['armtEnabled'] == TRUE
    end

    def device_id
      @data['deviceId']
    end

    # Prevent looped replications
    #
    # Gateways are valid if not updated by armt (we don't support create yet).
    # Internet Routes, Cascaded Networks, etc. are valid if not created or
    # updated by armt.
    def valid?
      if is_gateway_type?
        !armt_updater?
      else
        !created_or_updated_by_armt?
      end
    end

    def to_h
      data_in_envelope
    end

    def to_json
      to_h.to_json
    end

    # Useful for debugging
    def pretty_print
      puts JSON.pretty_generate(@data)
    end

    private

    def data_in_envelope
      { meta: { action: action }, data: @data }
    end

    def action
      if is_create_type?
        'CREATE'
      elsif is_delete_type?
        'DELETE'
      else
        'UPDATE'
      end
    end

    def is_model?
      @data['modelInd'] == TRUE
    end

    def created_or_updated_by_armt?
      created_by_armt? || updated_by_armt?
    end

    def created_by_armt?
      is_create_type? && armt_creator?
    end

    def updated_by_armt?
      is_update_type? && armt_updater?
    end

    def is_gateway_type?
      SmxData.is_gateway_data_type?(@data_type)
    end

    def is_create_type?
      SmxData.is_create_data_type?(@data_type)
    end

    def is_update_type?
      SmxData.is_update_data_type?(@data_type)
    end

    def is_delete_type?
      SmxData.is_delete_data_type?(@data_type)
    end

    def armt_creator?
      @data['creator'].to_s.upcase == Configuration.smx_armt_user
    end

    def armt_updater?
      @data['updater'].to_s.upcase == Configuration.smx_armt_user
    end
  end
end
