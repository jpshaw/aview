module SmxData
  class KafkaMessageBus
    def initialize
      @client = Kafka.new(
        seed_brokers: ["#{Configuration.kafka_host}:#{Configuration.kafka_port}"],
        client_id: Configuration.kafka_client_id
      )
      @producer = @client.async_producer(
        delivery_threshold: Configuration.kafka_delivery_threshold,
        delivery_interval: Configuration.kafka_delivery_interval
      )
    end

    def publish(message, data_type)
      @producer.produce(message.to_json, topic: topic(data_type))
    rescue => ex
      SmxLogger.error "Failed to send message to kafka[#{topic(data_type)}]: #{message}"
      SmxLogger.error "#{ex.class}: #{ex.message}\n  #{ex.backtrace.join("\n  ")}\n"
    end

    def shutdown
      @producer.shutdown if @producer
    end

    private

    def topic(data_type)
      if SmxData::INTERNET_ROUTE_DATA_TYPES.include?(data_type)
        Configuration.kafka_internet_routes_topic
      elsif SmxData::CASCADED_NETWORK_DATA_TYPES.include?(data_type)
        Configuration.kafka_cascaded_networks_topic
      else
        Configuration.kafka_inbound_topic
      end
    end
  end
end
