module SmxData
  class Request < BinData::Record

    def data_type_is_valid?
      SmxData.data_types.include?(replication_data_type)
    end

    def gateway_data_type?
      SmxData::GATEWAY_DATA_TYPES.include?(replication_data_type)
    end

    def internet_route_data_type?
      SmxData::INTERNET_ROUTE_DATA_TYPES.include?(replication_data_type)
    end

    def cascaded_network_data_type?
      SmxData::CASCADED_NETWORK_DATA_TYPES.include?(replication_data_type)
    end

    def code
      replication_data_type
    end

    endian :big

    header :header
    uint16 :control_request_header_len
    uint16 :control_request_header_marker
    uint8 :confirm_processing_len
    uint8 :confirm_processing_marker
    uint8 :confirmation
    uint8 :app_name_len, value: lambda { app_name.length + 2}
    uint8 :app_name_marker
    string :app_name, read_length: lambda { app_name_len - 2 }
    uint8 :request_type_len
    uint8 :request_type_marker
    uint16 :request_type
    uint16 :request_data_header_len
    uint16 :request_data_header_marker
    uint16 :parm_llid_len
    uint16 :parm_llid_marker
    uint8 :entry_name_len, value: lambda { entry_name.length - 2}
    uint8 :entry_name_marker
    string :entry_name, read_length: lambda { entry_name_len - 2 }
    uint8 :entry_data_size_len
    uint8 :entry_data_size_marker
    uint32 :data_size
    uint16 :replication_data_llid_len
    uint16 :replication_data_llid_marker
    string :replication_data_type, read_length: 3
    uint8 :comma
    # subtract the 3 replication data type characters (e.g. 558, 720) and the comma
    string :replication_data, read_length: lambda { data_size - 4 }
    string :garbage
  end
end
