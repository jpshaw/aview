require 'smx_logger'

module SmxData
  class Parser
    class << self
      def gateway_fields
        @gateway_fields ||= parse_fields('gateway')
      end

      def internet_route_fields
        @internet_route_fields ||= parse_fields('internet_route')
      end

      def cascaded_network_fields
        @cascaded_network_fields ||= parse_fields('cascaded_network')
      end

      private

      def parse_fields(name)
        fields = JSON.parse(File.read("#{config_dir}/#{name}.json"))

        # Symbolize field keys
        fields.map do |field|
          field.inject({}) { |hash, (k,v)| hash[k.to_sym] = v; hash}
        end
      end

      def config_dir
        @config_dir ||= File.expand_path('../../../config', __FILE__)
      end
    end

    attr_reader :request, :data

    def initialize(request)
      @request = request
      @data = request.replication_data
    end

    def execute
      offset  = 0
      results = {}

      fields.each do |field|
        raw_value = data.slice(offset, field[:size])

        if raw_value
          key   = field[:name]
          value = unpack_raw_value(raw_value, field[:unpack_format])
          results[key] = value
        end

        offset += field[:size]
      end

      SmxData::Record.new(results, @request.replication_data_type)
    end

    private

    # Unpack the raw value and enforce utf-8 encoding
    def unpack_raw_value(raw_value, unpack_format)
      raw_value
        .unpack(unpack_format)
        .first
        .to_s
        .encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
    end

    def fields
      if request.gateway_data_type?
        self.class.gateway_fields
      elsif request.internet_route_data_type?
        self.class.internet_route_fields
      elsif request.cascaded_network_data_type?
        self.class.cascaded_network_fields
      else
        raise "Invalid request data type: #{request.replication_data_type}"
      end
    end
  end
end
