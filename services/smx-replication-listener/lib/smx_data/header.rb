module SmxData
  class Header < BinData::Record
    endian :big

    uint16 :total_length
    uint16 :msg_length
    uint16 :outer_env_header
    uint16 :common_header_len
    uint16 :common_header_marker
    uint8 :pu_version_lt_len
    uint8 :pu_version_lt_marker
    uint8 :version
    uint8 :eye_catcher_lt_len, value: lambda { eye_catcher.length + 2 }
    uint8 :eye_catcher_lt_marker
    string :eye_catcher, read_length: lambda { eye_catcher_lt_len - 2 }
    uint8 :correlator_lt_len
    uint8 :correlator_lt_marker
    uint32 :correlator
  end
end
