module SmxData
  module Configuration
    class << self
      def smx_socket_host
        ENV['SMX_SOCKET_HOST'] || '0.0.0.0'
      end

      def smx_socket_port
        ENV['SMX_SOCKET_PORT'] || 36000
      end

      def smx_armt_user
        ENV['SMX_ARMT_USER'] || 'ACCELRTD.ACCELRTD'
      end

      # TODO: Add kafka_brokers configuration to allow multiple endpoints

      def kafka_client_id
        ENV['KAFKA_CLIENT_ID'] || 'smx-inbound'
      end

      def kafka_host
        ENV['KAFKA_HOST'] || '127.0.0.1'
      end

      def kafka_port
        ENV['KAFKA_PORT'] || 9092
      end

      def kafka_inbound_topic
        ENV['KAFKA_INBOUND_TOPIC'] || 'aview.smx.inbound'
      end

      def kafka_internet_routes_topic
        ENV['KAFKA_INTERNET_ROUTES_TOPIC'] || 'aview.smx.internet-routes'
      end

      def kafka_cascaded_networks_topic
        ENV['KAFKA_CASCADED_NETWORKS_TOPIC'] || 'aview.smx.cascaded-networks'
      end

      def kafka_delivery_threshold
        ENV['KAFKA_DELIVERY_THRESHOLD'] || 10 # Messages
      end

      def kafka_delivery_interval
        ENV['KAFKA_DELIVERY_INTERVAL'] || 5 # Seconds
      end
    end
  end
end
