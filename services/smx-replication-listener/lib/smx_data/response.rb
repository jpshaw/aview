module SmxData
  class Response < BinData::Record
    endian :big

    header :header
    uint16 :control_response_header_len, value: 0x0020
    uint16 :control_response_header_marker, value: 0xCC01
    uint8 :response_rc_count_lt_len, value: 0x04
    uint8 :response_rc_count_lt_marker, value: 0x02
    uint16 :rc_count, value: 0x0002
    uint8 :rc_lt_len, value: 0x0A
    uint8 :rc_lt_marker, value: 0x03
    uint32 :status_code, value: 0x00000000
    uint32 :return_code, value: 0x00000000
    uint8 :response_type_lt_len, value: 0x04
    uint8 :response_type_lt_marker, value: 0x0E
    uint16 :replication_response, value: 0xAA91
  end
end
