require 'socket'
require 'smx_logger'

module SmxData
  class Server
    attr_reader :server, :message_bus

    def initialize
      @server = TCPServer.new(
        Configuration.smx_socket_host,
        Configuration.smx_socket_port
      )

      @message_bus = KafkaMessageBus.new
    end

    # TODO: Flush records to message bus before closing socket (on purpose or
    # when an exception is thrown)
    def run
      loop do
        Thread.start(server.accept) do |client|
          begin
            SmxLogger.info 'server accepted connection'
            socket_loop(client)
          rescue Errno::ECONNRESET
            SmxLogger.info 'connection reset'
          rescue EOFError
            SmxLogger.info 'socket closed'
          rescue Exception => e
            SmxLogger.error "#{e.message}\n  #{e.backtrace.join("\n  ")}\n"
          ensure
            client.close
          end
        end
      end

      # TODO: Catch SIGINT and shutdown message bus
      # message_bus.shutdown
    end

    private

    def socket_loop(client)
      loop do
        request = SmxData.read_request(client)
        #SmxLogger.info "[#{request.code}] sent"
        handle_request(request) if request.data_type_is_valid?
        SmxData.write_response(client, request.header)
      end
    end

    def handle_request(request)
      record = SmxData.parse_request(request)

      if record.valid?
        message_bus.publish(record, request.code)
        #SmxLogger.info "[#{request.code}] handled"
      end
    end

  end
end
