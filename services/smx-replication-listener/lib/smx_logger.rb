class SmxLogger
  class << self
    def info(message)
      puts "[INFO]  #{Time.now}: #{message}"
    end

    def error(message)
      puts "[ERROR] #{Time.now}: #{message}"
    end

    def warn(message)
      puts "[WARN]  #{Time.now}: #{message}"
    end
  end
end