require 'json'
require 'bindata'
require 'kafka'
require 'smx_data/configuration'
require 'smx_data/header'
require 'smx_data/request'
require 'smx_data/response'
require 'smx_data/parser'
require 'smx_data/record'
require 'smx_data/server'
require 'smx_data/kafka_message_bus'

module SmxData
  UPDATE_GATEWAY = '558'.freeze
  UPDATE_GATEWAY_VIA_QUERY = '677'.freeze
  UPDATE_GATEWAY_VIA_LAST_ACCESS = '562'.freeze
  CREATE_INTERNET_ROUTE = '347'.freeze
  UPDATE_INTERNET_ROUTE = '660'.freeze
  DELETE_INTERNET_ROUTE = '859'.freeze
  CREATE_CASCADED_NETWORK = '301'.freeze
  UPDATE_CASCADED_NETWORK = '593'.freeze
  DELETE_CASCADED_NETWORK = '812'.freeze

  GATEWAY_DATA_TYPES = [ UPDATE_GATEWAY ].freeze
  INTERNET_ROUTE_DATA_TYPES = [ CREATE_INTERNET_ROUTE, UPDATE_INTERNET_ROUTE, DELETE_INTERNET_ROUTE ].freeze
  CASCADED_NETWORK_DATA_TYPES = [ CREATE_CASCADED_NETWORK, UPDATE_CASCADED_NETWORK, DELETE_CASCADED_NETWORK ].freeze

  CREATE_DATA_TYPES = [ CREATE_INTERNET_ROUTE, CREATE_CASCADED_NETWORK ].freeze
  # TODO: Add gateway update types and test.
  UPDATE_DATA_TYPES = [ UPDATE_INTERNET_ROUTE, UPDATE_CASCADED_NETWORK ].freeze
  DELETE_DATA_TYPES = [ DELETE_INTERNET_ROUTE, DELETE_CASCADED_NETWORK ].freeze

  def self.data_types
    GATEWAY_DATA_TYPES +
    INTERNET_ROUTE_DATA_TYPES +
    CASCADED_NETWORK_DATA_TYPES
  end

  def self.read_request(client)
    request = SmxData::Request.new
    request.read(client)
  end

  def self.parse_request(request)
    SmxData::Parser.new(request).execute
  end

  def self.write_response(client, header)
    response = SmxData::Response.new
    response.header = header
    response.header.msg_length = 0x007E
    response.header.total_length = response.num_bytes
    response.write(client)
  end

  def self.is_gateway_data_type?(data_type)
    GATEWAY_DATA_TYPES.include?(data_type)
  end

  def self.is_create_data_type?(data_type)
    CREATE_DATA_TYPES.include?(data_type)
  end

  def self.is_update_data_type?(data_type)
    UPDATE_DATA_TYPES.include?(data_type)
  end

  def self.is_delete_data_type?(data_type)
    DELETE_DATA_TYPES.include?(data_type)
  end
end
