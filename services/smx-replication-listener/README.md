# Service Manager Inbound Microservice

Uses Ruby MRI 2.x

## Using

```bash
  $ bundle install
  $ ./bin/server
```

## Docker

### Building

```bash
  $ ./build.sh
```

### Testing

```bash
  $ ./run-tests.sh
```
