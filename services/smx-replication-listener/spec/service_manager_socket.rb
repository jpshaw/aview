require 'socket'

class ServiceManagerSocket
  def initialize
    @socket = TCPSocket.new(
      SmxData::Configuration.smx_socket_host,
      SmxData::Configuration.smx_socket_port
    )
  end

  def write(data)
    @socket.write(data)
    @socket.gets # wait for reply
  end

  def close
    @socket.close if @socket
    @socket = nil
  end
end
