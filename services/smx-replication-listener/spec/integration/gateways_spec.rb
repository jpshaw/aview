require 'spec_helper'

describe 'Gateways' do
  let(:gateways_input)  { fixture('gateways_input.bin') }
  let(:gateways_output) { json_fixture('gateways_output.json') }
  let(:output_count)    { gateways_output.count }

  before do
    @smx_socket = ServiceManagerSocket.new
    @kafka_consumer = KafkaConsumer.new
    @last_offset = @kafka_consumer.last_offset + 1
  end

  after do
    @smx_socket.close if @smx_socket
    @kafka_consumer.close if @kafka_consumer
  end

  before do
    gateways_input.each_line do |line|
      @smx_socket.write(line)
    end
  end

  it 'parses the binary data into json and sends to kafka' do
    results = @kafka_consumer.fetch_messages(count: output_count,
                                            offset: @last_offset)
    expect(results).to eq gateways_output
  end
end
