require 'kafka'

class KafkaConsumer
  def initialize
    @host = SmxData::Configuration.kafka_host
    @port = SmxData::Configuration.kafka_port
    @topic = SmxData::Configuration.kafka_inbound_topic

    @partition = 0

    @kafka = Kafka.new(seed_brokers: ["#{@host}:#{@port}"])
  end

  def last_offset
    @kafka.last_offset_for(@topic, @partition)
  end

  # Fetch `count` number of messages with `offset`.
  def fetch_messages(count: 1, offset: 0)
    results = []
    running_offset = offset

    # Give kafka some time to receive the messages
    5.times do
      messages = @kafka.fetch_messages(topic: @topic, partition: @partition, offset: running_offset)
      results += messages.map { |message| message.value }

      if results.count >= count
        break
      else
        running_offset += messages.count
        sleep 1
      end
    end

    results.map { |result| JSON.parse(result) }
  end

  def close
    @kafka.close if @kafka
    @kafka = nil
  end

  def self.wait_for_leader
    begin
      kafka = KafkaConsumer.new
      kafka.last_offset
    rescue Kafka::LeaderNotAvailable
      sleep 1
      retry
    ensure
      kafka.close if kafka
    end
  end
end
