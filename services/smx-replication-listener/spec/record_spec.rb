require 'spec_helper'

describe SmxData::Record do

  let(:data)      { {} }
  let(:data_type) { nil }
  let(:record)    { described_class.new(data, data_type) }

  describe '#valid?' do

    context 'when given a gateway record' do
      before { allow(record).to receive(:is_gateway_type?).and_return(true) }

      context 'when updated by armt' do
        before { allow(record).to receive(:armt_updater?).and_return(true) }

        it 'returns false' do
          expect(record.valid?).to be false
        end
      end

      context 'when not updated by armt' do
        before { allow(record).to receive(:armt_updater?).and_return(false) }

        context 'when armt is enabled' do
          before { allow(record).to receive(:armt_enabled?).and_return(true) }

          it 'returns true' do
            expect(record.valid?).to be true
          end
        end

        context 'when armt is not enabled' do
          before { allow(record).to receive(:armt_enabled?).and_return(false) }

          context 'when given a model' do
            before { allow(record).to receive(:is_model?).and_return(true) }

            it 'returns true' do
              expect(record.valid?).to be true
            end
          end

          context 'when not given a model' do
            before { allow(record).to receive(:is_model?).and_return(false) }

            it 'returns false' do
              expect(record.valid?).to be false
            end
          end
        end
      end
    end

    context 'when given a non-gateway record' do
      before { allow(record).to receive(:is_gateway_type?).and_return(false) }

      context 'when updated by armt' do
        before { allow(record).to receive(:created_or_updated_by_armt?).and_return(true) }

        it 'returns false' do
          expect(record.valid?).to be false
        end
      end

      context 'when not updated by armt' do
        before { allow(record).to receive(:created_or_updated_by_armt?).and_return(false) }

        it 'returns true' do
          expect(record.valid?).to be true
        end
      end
    end

  end

end
