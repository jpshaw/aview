class AddIdsAlert < ActiveRecord::Migration[5.1]
  def change
    create_table :ids_alerts do |t|
      t.integer :sig_gen
      t.integer :sig_id
      t.integer :sig_rev
      t.text :msg
      t.string :proto
      t.string :src
      t.integer :srcport
      t.string :dst
      t.integer :dstport
      t.text :classification
      t.integer :priority
      t.string :host
      t.string :interface
      t.text :reference, limit: 16.megabytes - 1 # mediumtext
      t.datetime :alerted_at
    end
  end
end
