require 'ids_alert_logger'

module IDSAlertProcessor
  class KafkaConsumer
    attr_reader :consumer, :producer

    def initialize
      @brokers  = Configuration.kafka_brokers
      @group_id = Configuration.kafka_group_id
      @topic = Configuration.kafka_inbound_topic
      init_consumer
    end

    def start
      IDSAlertLogger.info 'Starting processor ...'

      consumer_loop
    end

    def stop
      IDSAlertLogger.info 'Stopping processor ...'

      consumer.stop

      IDSAlertLogger.info 'Stopped processor'
    end

    private

    attr_reader :brokers, :group_id, :topic

    def init_consumer
      @consumer = Kafka.new(seed_brokers: brokers).consumer(group_id: group_id)
      consumer.subscribe(topic)
    end

    def consumer_loop
      IDSAlertLogger.info 'Started consumer loop ...'

      consumer.each_message { |message| process_message(message) }

      IDSAlertLogger.info 'Stopped consumer loop'
    rescue => exception
      IDSAlertLogger.error exception
    end

    def process_message(message)
      params = JSON.parse(message.value, symbolize_names: true)
      params = IDSAlertProcessor::Parser.new(params).to_h
      IDSAlertProcessor::IDSAlert.create(params).inspect
    rescue => exception
      IDSAlertLogger.error "failed to process message: #{message.value}"
      IDSAlertLogger.error exception
    end
  end
end
