module IDSAlertProcessor
  module Configuration
    class << self
      def kafka_brokers
        ENV['KAFKA_BROKERS'] || 'localhost:9092'
      end

      def kafka_group_id
        ENV['KAFKA_GROUP_ID'] || 'ids-alert-processor'
      end

      def kafka_inbound_topic
        ENV['KAFKA_IDS_ALERT_INBOUND_TOPIC'] || 'accns.test.aview.devices.ids'
      end

      def mysql_config
        ENV['MYSQL_CONFIG'] || 'db/config.yml'
      end
    end
  end
end
