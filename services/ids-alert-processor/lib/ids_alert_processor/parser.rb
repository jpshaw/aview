module IDSAlertProcessor
  class Parser
    def initialize(params)
      @data = parse_data(params)
    end

    def to_h
      data.to_h.keep_if { |k| IDSAlertProcessor::IDSAlert.attribute_names.include?(k.to_s) }
    end

    private

    attr_reader :data

    def parse_data(params)
      params[:alerted_at] = parse_timestamp(params)
      params[:host] = parse_host(params)
      params[:reference] = parse_reference(params)
      params
    end

    def parse_host(params)
      params[:host].delete(':').upcase
    end

    def parse_timestamp(params)
      timestamp = params.delete(:timestamp)
      timestamp.present? ? Time.at(timestamp) : nil
    end

    def parse_reference(params)
      params[:reference].delete(' ')
    end
  end
end
