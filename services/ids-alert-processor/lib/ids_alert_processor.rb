require 'active_record'
require 'json'
require 'kafka'
require 'ids_alert_processor/configuration'
require 'ids_alert_processor/ids_alert'
require 'ids_alert_processor/kafka_consumer'
require 'ids_alert_processor/parser'
require 'erb'

environment = ENV['RACK_ENV'] || 'development'
db_config_file = File.read(IDSAlertProcessor::Configuration.mysql_config)
dbconfig = YAML.load(ERB.new(db_config_file).result)
ActiveRecord::Base.establish_connection dbconfig[environment]

module IDSAlertProcessor
end
