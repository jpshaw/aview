ENV['RACK_ENV'] = 'test'
$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'ids_alert_processor'
require 'json'

def spec_path
  @spec_path ||= File.join(File.dirname(__FILE__))
end

def fixtures_path
  @fixtures_path ||= "#{spec_path}/fixtures"
end

def fixture(name)
  File.new(fixtures_path + '/' + name + '.json', 'r')
end

def json_fixture(name)
  JSON.parse(fixture(name).read, symbolize_names: true)
end

RSpec.configure do |config|
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
