require 'spec_helper'

describe IDSAlertProcessor::Parser do
  subject { IDSAlertProcessor::Parser.new(params) }
  let(:params) { json_fixture('ids_alert') }

  describe '.new' do
    context 'with valid data' do
      it 'creates an instance of Parser' do
        expect(subject).to be
      end
    end
  end

  describe '#to_h' do
    let(:result) { subject.to_h }

    it 'sets alerted_at to timestamp' do
      expect(result[:alerted_at]).to be_a Time
    end

    it 'strips colons and upcases host data' do
      expect(result[:host]).to eq '0027042C5919'
    end

    it 'strips whitespace between commas' do
      expect(result[:reference]).to eq "http://www.whitehats.com/info/IDS,123,http://cve.mitre.org/cgi-bin/cvename.cgi?name=,CAN3424"
    end
  end
end
