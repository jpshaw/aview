require 'spec_helper'

describe IDSAlertProcessor::IDSAlert do
  subject { IDSAlertProcessor::IDSAlert.create(params) }
  let(:params) { json_fixture('parsed_alert') }

  describe '.create' do
    context 'with no data' do
      let(:params) {}

      it 'creates an IDSAlert' do
        expect(subject).to be_persisted
      end
    end

    context 'with valid data' do
      it 'creates an IDSAlert' do
        expect(subject).to be_persisted
      end
    end
  end
end
