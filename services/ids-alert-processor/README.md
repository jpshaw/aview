# Intrusion Detection System (IDS) Alert Processor

This processor consumes data from a configurable Kafka topic and then creates
IDSAlerts from that data.

## Run the Processor
```bash
$ ./bin/processor
```

## Run the Tests
```bash
$ bundle exec rspec
```

## Migrations

This app uses the
[standalone_migrations](https://github.com/thuss/standalone-migrations) gem to
handle migrations. Usage is very similar to Rails.

Create a database.
```bash
bundle exec rake db:create
```

Migrate the database.
```bash
bundle exec rake db:migrate
```

Generate a new migration.
```bash
bundle exec rake db:new_migration name=some_name_here
```
