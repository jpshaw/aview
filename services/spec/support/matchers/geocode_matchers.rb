require 'rspec/expectations'

RSpec::Matchers.define :have_geocode_state do |expected|
  match do |location|
    location.send(:geocode).state.to_s == expected.to_s
  end
end
