RSpec.shared_context 'Service Features', service_feature: true do
  instance_eval do
    alias :background :before
    alias :given :let
    alias :given! :let!

    def service_name(name)
      puts ">>>service_name: #{name}"
      @service_name = name
    end
  end
end

RSpec.configure do |config|
  config.include_context 'Service Features', service_feature: true
  config.alias_example_group_to :feature, service_feature: true, type: :feature
  config.alias_example_to :scenario
end
