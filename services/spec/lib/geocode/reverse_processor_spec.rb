require 'geocode/reverse_processor'

describe Geocode::ReverseProcessor do

  let(:processor) { described_class.new('{}') }

  it_behaves_like 'a base geocode processor'

  describe '#execute' do
    let(:subject)   { processor.execute }

    context 'when location has an address' do
      let(:location) { create(:location_with_address) }

      before do
        allow(processor).to receive(:location_id).and_return(location.id)
      end

      it 'does not perform a geocoder search' do
        expect(processor).to_not receive(:geocoder_search)
        subject
      end

      it 'warns that an address already exists for the location' do
        expect(processor.logger).to receive(:warn).with(/address already exist/i)
        subject
      end
    end

    context 'when location does not have an address' do
      let(:location) { create(:location_without_address) }

      before do
        allow(processor).to receive(:location_id).and_return(location.id)
      end

      it 'performs a geocoder search' do
        expect(processor).to receive(:geocoder_search)
        subject
      end

      context 'with geocoder result' do
        let(:result) do
          double('geocoder result',
            street_address: '1120 East Kennedy Boulevard',
                      city: 'Tampa',
                state_code: 'FL',
                     state: 'Florida',
               postal_code: '33602',
              country_code: 'US'
          )
        end

        before do
          allow(processor).to receive(:geocoder_search).and_return(result)
        end

        it 'saves the address for the location' do
          expect { subject }.to change {
            Location.find(location.id).address.to_s
          }.from('').to('1120 East Kennedy Boulevard Tampa Florida 33602')
        end
      end

      context 'without geocoder result' do
        let(:result) { nil }

        before do
          allow(processor).to receive(:geocoder_search).and_return(result)
        end

        it 'logs debug info' do
          expect(processor.logger).to receive(:debug).with(/no reverse geocoder result/i)
          subject
        end
      end
    end
  end
end
