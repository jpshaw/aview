require 'geocode/processor'

describe Geocode::Processor do

  let(:processor) { described_class.new('{}') }

  it_behaves_like 'a base geocode processor'

  describe '#execute' do
    let(:subject)   { processor.execute }

    context 'when location has coordinates' do
      let(:location) { create(:location_with_coordinates) }

      before do
        allow(processor).to receive(:location_id).and_return(location.id)
      end

      it 'does not perform a geocoder search' do
        expect(processor).to_not receive(:geocoder_search)
        subject
      end

      it 'warns that coordinates already exist for the location' do
        expect(processor.logger).to receive(:warn).with(/coordinates already exist/i)
        subject
      end
    end

    context 'when location does not have coordinates' do
      let(:location) { create(:location_without_coordinates) }

      before do
        allow(processor).to receive(:location_id).and_return(location.id)
      end

      it 'performs a geocoder search' do
        expect(processor).to receive(:geocoder_search)
        subject
      end

      context 'with geocoder result' do
        let(:latitude)  { 27.951  }
        let(:longitude) { -82.447 }
        let(:result)    { double('geocoder result', latitude: latitude,
                                                   longitude: longitude) }

        before do
          allow(processor).to receive(:geocoder_search).and_return(result)
        end

        it 'saves the coordinates for the location' do
          expect { subject }.to change {
            Location.find(location.id).coordinates.to_a
          }.from([]).to([latitude, longitude])
        end
      end

      context 'without geocoder result' do
        let(:result) { nil }

        before do
          allow(processor).to receive(:geocoder_search).and_return(result)
        end

        it 'logs debug info' do
          expect(processor.logger).to receive(:debug).with(/no geocoder result/i)
          subject
        end
      end
    end
  end
end
