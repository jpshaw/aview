shared_examples_for 'a base geocode processor' do
  let!(:location) { create(:location_enqueued_for_geocoder) }

  before do
    allow(processor).to receive(:location_id).and_return(location.id)
  end

  describe '#geocoder_search' do
    it 'returns nil if given nil' do
      expect(processor.geocoder_search(nil)).to be nil
    end

    it 'returns a result for address input' do
      address = create(:location_with_address).address.to_s
      result = nil

      VCR.use_cassette('geocoder search address') do
        result = processor.geocoder_search(address)
      end

      expect(result).to be_present
      expect(result.latitude).to eq(27.95072)
      expect(result.longitude).to eq(-82.447779)
    end

    it 'returns a result for coordinates input' do
      coordinates = create(:location_with_coordinates).coordinates.to_s
      result = nil

      VCR.use_cassette('geocoder search coordinates') do
        result = processor.geocoder_search(coordinates)
      end

      expect(result).to be_present
      expect(result.street_address).to eq '1120 East Kennedy Boulevard'
      expect(result.city).to eq 'Tampa'
      expect(result.state_code).to eq 'FL'
      expect(result.postal_code).to eq '33602'
      expect(result.country_code).to eq 'US'
    end
  end

  describe '#geocoder_transaction' do
    it 'sets the geocode state to `started` before yielding' do
      processor.geocoder_transaction do
        expect(Location.find(location.id)).to have_geocode_state(:started)
      end
    end

    it 'sets the geocode state to `finished` after yielding' do
      processor.geocoder_transaction {}
      expect(Location.find(location.id)).to have_geocode_state(:finished)
    end

    it 'sets the geocode state to `failed` if an exception is thrown' do
      expect {
        processor.geocoder_transaction { raise 'error' }
      }.to raise_error(RuntimeError)
      expect(Location.find(location.id)).to have_geocode_state(:failed)
    end
  end
end
