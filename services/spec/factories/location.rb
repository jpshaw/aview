FactoryBot.define do
  factory :location do
    factory :location_with_address do
      address_1 { '1120 E. Kennedy Blvd.' }
      address_2 { 'Suite 227' }
      city { 'Tampa' }
      state_code { 'FL' }
      postal_code { '33602' }

      before(:create) do |location|
        location.country = Country.where(code: 'US').first_or_create
      end

      factory :location_enqueued_for_geocoder do
        after(:create) do |location|
          location.geocode_enqueued!
        end
      end
    end

    factory :location_without_address do
      latitude { 27.95072 }
      longitude { -82.447779 }
    end

    factory :location_with_coordinates do
      latitude { 27.95072 }
      longitude { -82.447779 }
    end

    factory :location_without_coordinates do
      latitude { nil }
      longitude { nil }
    end
  end
end
