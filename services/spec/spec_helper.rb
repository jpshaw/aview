ENV['RAILS_ENV'] = 'test'

# Base service
require File.expand_path('../../boot', __FILE__)
require 'database' # TODO: Make optional for services w/o database
Service.register_config_source :spec
Service.start

# Test tools
require 'rspec/core'

Dir[Service.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.order = :random
  Kernel.srand config.seed
end
