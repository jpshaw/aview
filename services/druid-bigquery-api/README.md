# Druid BigQuery API

## Console

```bash
  bin/console
```

## Running

```bash
  bundle exec ruby app.rb -p 8082
```

## Testing

```bash
  bundle exec rspec
```

## Debugging

```bash
  DEBUG=1 bin/console
  DEBUG=1 bundle exec ruby app.rb -p 8082
  DEBUG=1 bundle exec rspec
```
