ENV['RACK_ENV'] = 'test'

require_relative '../app'

require 'rspec'
require 'rack/test'
require 'vcr'

def specs_dir
  @specs_dir ||= File.expand_path(__dir__)
end

def fixtures_dir
  @fixtures_dir ||= "#{specs_dir}/fixtures"
end

def json_fixture(fixture_path)
  JSON.parse(File.read("#{fixtures_dir}/#{fixture_path}.json"))
end

def use_production_bigquery_project
  ENV['GOOGLE_CLOUD_PROJECT'] = 'aview-production'
  ENV['GOOGLE_CLOUD_KEYFILE'] = './aview-production-keyfile.json'
end

def reset_bigquery_project
  ENV['GOOGLE_CLOUD_PROJECT'] = 'aview-staging'
  ENV['GOOGLE_CLOUD_KEYFILE'] = './aview-staging-keyfile.json'
end

module RSpecMixin
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end

RSpec.configure do |conf|
  conf.include RSpecMixin
end

VCR.configure do |c|
  c.cassette_library_dir = "#{specs_dir}/cassettes"
  c.hook_into :webmock
end

Dir["#{specs_dir}/matchers/*.rb"].each { |f| require f }
