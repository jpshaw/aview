require 'rspec/expectations'

RSpec::Matchers.define :match_total_availability_in do |expected|
  match do |actual|
    actual_event = actual['event']

    expected.find do |expected_result|
      expected_event = expected_result['event']

      expected_event['mac']        == actual_event['mac']        &&
      expected_event['state'].to_i == actual_event['state'].to_i &&
      expected_event['count'].to_i == actual_event['count'].to_i
    end.present?
  end
end

RSpec::Matchers.define :match_trend_availability_in do |expected|
  match do |actual|
    actual_event = actual['event']

    expected.find do |expected_result|
      expected_event = expected_result['event']

      expected_result['timestamp'] == actual['timestamp']        &&
      expected_event['state'].to_i == actual_event['state'].to_i &&
      expected_event['count'].to_i == actual_event['count'].to_i
    end.present?
  end
end
