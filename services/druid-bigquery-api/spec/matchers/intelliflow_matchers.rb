require 'rspec/expectations'

def values_within_percent?(v1, v2, percent: 5.0)
  (((v1-v2).abs.to_f / (v1 + v2)) * 100) < percent
end

RSpec::Matchers.define :match_usage_per_client_in do |expected|
  match do |actual|
    actual_event = actual['event']

    expected.find do |expected_result|
      expected_event = expected_result['event']
      expected_event['client'] == actual_event['client'] &&
      values_within_percent?(expected_event['sum'], actual_event['sum'])
    end.present?
  end
end

RSpec::Matchers.define :match_usage_per_client_for_app_in do |expected|
  match do |actual|
    actual_event = actual['event']

    expected.find do |expected_result|
      expected_event = expected_result['event']
      expected_event['client'] == actual_event['client'] &&
      values_within_percent?(expected_event['sum'], actual_event['sum'])
    end.present?
  end
end

RSpec::Matchers.define :match_total_client_usage_in do |expected|
  match do |actual|
    actual_result = actual.first['result']
    expected_result = expected.first['result']

    values_within_percent?(expected_result['sum'], actual_result['sum'])
  end
end

RSpec::Matchers.define :match_usage_per_server_in do |expected|
  match do |actual|
    actual_event = actual['event']

    expected.find do |expected_result|
      expected_event = expected_result['event']
      expected_event['server'] == actual_event['server'] &&
      values_within_percent?(expected_event['sum'], actual_event['sum'], percent: 30.0) # 30 percent is due to shift in testing time. Real is <1%
    end.present?
  end
end
