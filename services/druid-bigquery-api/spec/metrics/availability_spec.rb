require 'spec_helper'

describe 'Availability Metrics' do

  describe '#total_by_device' do
    let(:druid_request)  { json_fixture('availability/total_by_device/request') }
    let(:druid_response) { json_fixture('availability/total_by_device/response') }

    it 'returns the correct state counts for each device' do
      VCR.use_cassette('availability total by device') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)

      response.each do |result|
        expect(result).to match_total_availability_in(druid_response)
      end
    end
  end

  describe '#trend_chart' do
    let(:druid_request)  { json_fixture('availability/trend_chart/request') }
    let(:druid_response) { json_fixture('availability/trend_chart/response') }

    it 'returns the correct state counts for each timestamp' do
      VCR.use_cassette('availability trend chart') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)

      response.each do |result|
        expect(result).to match_trend_availability_in(druid_response)
      end
    end
  end
end
