require 'spec_helper'

describe 'Intelliflow Metrics' do

  describe '#data_usage_per_client' do
    let(:druid_request)  { json_fixture('intelliflow/data_usage_per_client/request') }
    let(:druid_response) { json_fixture('intelliflow/data_usage_per_client/response') }

    it 'returns the data usage per client' do
      VCR.use_cassette('intelliflow data usage per client') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)

      response.each do |result|
        expect(result).to match_usage_per_client_in(druid_response)
      end
    end
  end

  describe '#data_usage_per_client_for_app' do
    let(:druid_request)  { json_fixture('intelliflow/data_usage_per_client_for_app/request') }
    let(:druid_response) { json_fixture('intelliflow/data_usage_per_client_for_app/response') }

    it 'returns the data usage per client for app' do
      VCR.use_cassette('intelliflow data usage per client for app') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)

      response.each do |result|
        expect(result).to match_usage_per_client_for_app_in(druid_response)
      end
    end
  end

  describe '#total_client_usage' do
    let(:druid_request)  { json_fixture('intelliflow/total_client_usage/request') }
    let(:druid_response) { json_fixture('intelliflow/total_client_usage/response') }

    it 'returns the total client usage' do
      VCR.use_cassette('intelliflow total client usage') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)
      expect(response).to match_total_client_usage_in druid_response
    end
  end

  describe '#total_client_usage_for_app' do
    let(:druid_request)  { json_fixture('intelliflow/total_client_usage_for_app/request') }
    let(:druid_response) { json_fixture('intelliflow/total_client_usage_for_app/response') }

    it 'returns the total client usage for app' do
      VCR.use_cassette('intelliflow total client usage for app') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)
      expect(response).to match_total_client_usage_in druid_response
    end
  end

  describe '#data_usage_per_app' do
    let(:druid_request)  { json_fixture('intelliflow/data_usage_per_app/request') }
    let(:druid_response) { json_fixture('intelliflow/data_usage_per_app/response') }

    it 'returns the data usage per app' do
      VCR.use_cassette('intelliflow data usage per app') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)

      response.each do |result|
        expect(result).to match_usage_per_server_in(druid_response)
      end
    end
  end

  describe '#data_usage_per_app_over_time' do
    let(:druid_request)  { json_fixture('intelliflow/data_usage_per_app_over_time/request') }
    let(:druid_response) { json_fixture('intelliflow/data_usage_per_app_over_time/response') }

    it 'returns the data usage per app over time' do
      VCR.use_cassette('intelliflow data usage per app over time') do
        post '/druid/v2', druid_request
      end

      expect(last_response.status).to eq 200
      expect(last_response.content_type).to eq 'application/json'

      response = JSON.parse(last_response.body)
      expect(response).to eq druid_response
    end
  end

end
