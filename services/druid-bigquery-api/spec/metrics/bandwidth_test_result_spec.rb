require 'spec_helper'

describe 'Bandwidth Test Result Metrics' do

  describe '#chart' do
    context 'by hour' do
      let(:druid_request)  { json_fixture('bandwidth_test_result/chart/by_hour/request') }
      let(:druid_response) { json_fixture('bandwidth_test_result/chart/by_hour/response') }

      it 'returns the bandwidth test results by hour' do
        VCR.use_cassette('bandwidth test results chart by hour') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

    context 'by day' do
      let(:druid_request)  { json_fixture('bandwidth_test_result/chart/by_day/request') }
      let(:druid_response) { json_fixture('bandwidth_test_result/chart/by_day/response') }

      it 'returns the bandwidth test results by day' do
        VCR.use_cassette('bandwidth test results chart by day') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

  end
end
