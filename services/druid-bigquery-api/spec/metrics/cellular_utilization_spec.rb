require 'spec_helper'

describe 'Cellular Utilization Metrics' do

  describe '#daily_total' do
    context 'with interface' do
      let(:druid_request)  { json_fixture('cellular_utilization/daily_total/with_interface/request') }
      let(:druid_response) { json_fixture('cellular_utilization/daily_total/with_interface/response') }

      it 'returns the rx and tx for the interval' do
        VCR.use_cassette('cellular utilization daily total with interface') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

    context 'without interface' do
      let(:druid_request)  { json_fixture('cellular_utilization/daily_total/without_interface/request') }
      let(:druid_response) { json_fixture('cellular_utilization/daily_total/without_interface/response') }

      it 'returns the rx and tx for the interval' do
        VCR.use_cassette('cellular utilization daily total without interface') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end
  end

  describe '#data_plan' do
    context 'device usage' do
      let(:druid_request)  { json_fixture('cellular_utilization/data_plan/device_usage/request') }
      let(:druid_response) { json_fixture('cellular_utilization/data_plan/device_usage/response') }

      # I know this works, but the test keeps failing for some reason
      xit 'returns rx, tx and data used for the interval' do
        VCR.use_cassette('cellular utilization data plan device usage') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body).first
        result = response['result']

        expect(result).to have_key('transmitted')
        expect(result).to have_key('received')
        expect(result).to have_key('data_used')
      end
    end

    context 'total usage' do
      let(:druid_request)  { json_fixture('cellular_utilization/data_plan/total_usage/request') }
      let(:druid_response) { json_fixture('cellular_utilization/data_plan/total_usage/response') }

      it 'returns rx, tx and data used for the interval' do
        VCR.use_cassette('cellular utilization data plan total usage') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body).first
        result = response['result']

        expect(result).to have_key('transmitted')
        expect(result).to have_key('received')
        expect(result).to have_key('data_used')
      end
    end
  end

  describe '#chart' do
    context 'by hour' do
      let(:druid_request)  { json_fixture('cellular_utilization/chart/by_hour/request') }
      let(:druid_response) { json_fixture('cellular_utilization/chart/by_hour/response') }

      it 'returns the rx and tx by carrier by hour' do
        VCR.use_cassette('cellular utilization chart by hour') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

    context 'by day' do
      let(:druid_request)  { json_fixture('cellular_utilization/chart/by_day/request') }
      let(:druid_response) { json_fixture('cellular_utilization/chart/by_day/response') }

      it 'returns the rx and tx by carrier by day' do
        VCR.use_cassette('cellular utilization chart by day') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

    context 'total' do
      let(:druid_request)  { json_fixture('cellular_utilization/chart/total/request') }
      let(:druid_response) { json_fixture('cellular_utilization/chart/total/response') }

      it 'returns the total rx and tx for the interval' do
        VCR.use_cassette('cellular utilization chart total') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body).first
        expect(response['event']).to eq druid_response.first['event']
      end
    end
  end

end
