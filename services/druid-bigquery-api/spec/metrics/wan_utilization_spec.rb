require 'spec_helper'

describe 'WAN Utilization Metrics' do

  before { use_production_bigquery_project }
  after  { reset_bigquery_project }

  describe '#chart' do
    context 'by hour' do
      let(:druid_request)  { json_fixture('wan_utilization/chart/by_hour/request') }
      let(:druid_response) { json_fixture('wan_utilization/chart/by_hour/response') }

      it 'returns the wan utilization by hour' do
        VCR.use_cassette('wan utilization chart by hour') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

    context 'by day' do
      let(:druid_request)  { json_fixture('wan_utilization/chart/by_day/request') }
      let(:druid_response) { json_fixture('wan_utilization/chart/by_day/response') }

      it 'returns the wan utilization by day' do
        VCR.use_cassette('wan utilization chart by day') do
          post '/druid/v2', druid_request
        end

        expect(last_response.status).to eq 200
        expect(last_response.content_type).to eq 'application/json'

        response = JSON.parse(last_response.body)
        expect(response).to eq druid_response
      end
    end

  end
end
