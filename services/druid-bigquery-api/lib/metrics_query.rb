require 'metrics_query/base'

module MetricsQuery
  extend self

  def execute(request)
    metrics_class =
      case request.metric_type
      when :device_availability
        AvailabilityQuery
      when :cellular_utilization
        CellularUtilizationQuery
      when :wan_utilization
        WanUtilizationQuery
      when :bandwidth_test_result
        BandwidthTestResultQuery
      when :intelliflow
        IntelliflowQuery
      else
        NullQuery
      end

    metrics_class.new(request).execute
  end

  def logger
    @logger ||= begin
      Logger.new(STDOUT).tap do |log|
        log.progname = 'druid-bigquery-api'
        log.level = ENV['DEBUG'] ? Logger::DEBUG : Logger::INFO # TODO: Add truthiness check
      end
    end
  end
end
