# Transforms results from BigQuery into Druid format
#
# You can find examples of the various types of response objects here:
#
#    ls -la spec/fixtures/**/response.json
#
class DruidResponse
  extend Memoist

  attr_reader :request, :results

  def initialize(request, results)
    @request = request
    @results = results
  end

  def to_a
    results.map { |result| decorate(result) }
  end
  memoize :to_a

  def to_json
    to_a.to_json
  end
  memoize :to_json

  private

  def decorate(result)
    timestamp = Time.at(result.delete(:timestamp)).utc.strftime("%FT%T.%3NZ")

    hash = {}
    hash[:version]   = :v1 if request.group_by?
    hash[:timestamp] = timestamp
    hash[result_key] = result
    hash
  end

  def result_key
    if request.group_by?
      :event
    else
      :result
    end
  end
  memoize :result_key
end
