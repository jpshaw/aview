# Wraps the Druid JSON Query
#
# You can find examples of the various types of query objects here:
#
#    ls -la spec/fixtures/**/request.json
#
# Example
#
# {
#   "queryType": "groupBy",
#   "dimensions": [
#     "state"
#   ],
#   "dataSource": "accns.staging.aview.events.metrics.device-availability",
#   "granularity": "hour",
#   "filter": {
#     "type": "regex",
#     "dimension": "mac",
#     "pattern": "00C0CA49D3A4|0027042965CA|00270427C395|0027042B6D80"
#   },
#   "intervals": "2018-06-14T05:49:37Z/2018-06-21T05:49:37Z",
#   "aggregations": [
#     {
#       "type": "longSum",
#       "name": "count",
#       "fieldName": "count"
#     }
#   ]
# }
class DruidRequest
  extend Memoist

  QUERY_TYPE  = 'queryType'.freeze
  TIMESERIES  = 'timeseries'.freeze
  GROUP_BY    = 'groupBy'.freeze
  GRANULARITY = 'granularity'.freeze
  FILTER      = 'filter'.freeze
  DATA_SOURCE = 'dataSource'.freeze
  INTERVALS   = 'intervals'.freeze
  SELECTOR    = 'selector'.freeze
  REGEX       = 'regex'.freeze
  AND         = 'and'.freeze
  OR          = 'or'.freeze
  MAC         = 'mac'.freeze
  INTERFACE   = 'interface'.freeze
  TYPE        = 'type'.freeze
  DIMENSION   = 'dimension'.freeze
  VALUE       = 'value'.freeze
  PATTERN     = 'pattern'.freeze
  FIELDS      = 'fields'.freeze
  DATA_PLAN   = 'data_plan'.freeze
  SERVER      = 'server'.freeze
  CLIENT      = 'client'.freeze

  attr_reader :request

  def initialize(request)
    @request = request
  end

  def query_type
    request[QUERY_TYPE]
  end

  def granularity
    request[GRANULARITY]
  end

  def filter
    request[FILTER]
  end

  # Underscore and symbolize the query metric name
  #
  # Example
  #
  #   { "dataSource": "accns.staging.aview.events.metrics.device-availability" }
  #   # => :device_availability
  #
  def metric_type
    request[DATA_SOURCE].to_s.split('.').last.underscore.to_sym
  end
  memoize :metric_type

  def intervals
    request[INTERVALS].to_s.split('/')
  end
  memoize :intervals

  def start_time
    iso8601_to_epoch(intervals.first)
  end
  memoize :start_time

  def end_time
    iso8601_to_epoch(intervals.last)
  end
  memoize :end_time

  def timeseries?
    query_type == TIMESERIES
  end

  def group_by?
    query_type == GROUP_BY
  end

  # Return the device id (mac)
  #
  # Examples
  #
  # "filter": {
  #   "type": "selector",
  #   "dimension": "mac",
  #   "value": "0027042B6D80"
  # }
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "selector",
  #       "dimension": "mac",
  #       "value": "0027042DC63A"
  #     },
  #     ...
  #   ]
  # }
  def device_id
    case filter[TYPE]
    when SELECTOR
      return filter[VALUE] if filter[DIMENSION] == MAC
    when AND
      filter[FIELDS].each do |filter_field|
        if filter_field[TYPE] == SELECTOR && filter_field[DIMENSION] == MAC
          return filter_field[VALUE]
        end
      end
      nil
    end
  end
  memoize :device_id

  # Return a list of device ids (macs)
  #
  # Example
  #
  # "filter": {
  #   "type": "regex",
  #   "dimension": "mac",
  #   "pattern": "00C0CA49D3A4|0027042965CA|00270427C395|0027042B6D80"
  # }
  def device_ids
    case filter[TYPE]
    when REGEX
      filter[PATTERN].to_s.split('|')
    end
  end
  memoize :device_ids

  # Return the interface to filter by
  #
  # Example
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "selector",
  #       "dimension": "interface",
  #       "value": "wwan0"
  #     },
  #     ...
  #   ]
  # }
  def device_interface
    case filter[TYPE]
    when AND
      filter[FIELDS].each do |filter_field|
        if filter_field[TYPE] == SELECTOR && filter_field[DIMENSION] == INTERFACE
          return filter_field[VALUE]
        end
      end
      nil
    end
  end
  memoize :device_interface

  # Return the data plan id to filter by
  #
  # Examples
  #
  # "filter": {
  #   "type": "selector",
  #   "dimension": "data_plan",
  #   "value": 1
  # }
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "selector",
  #       "dimension": "data_plan",
  #       "value": 1
  #     },
  #     ...
  #   ]
  # }
  def data_plan_id
    case filter[TYPE]
    when SELECTOR
      return filter[VALUE] if filter[DIMENSION] == DATA_PLAN
    when AND
      filter[FIELDS].each do |filter_field|
        if filter_field[TYPE] == SELECTOR && filter_field[DIMENSION] == DATA_PLAN
          return filter_field[VALUE]
        end
      end
      nil
    end
  end
  memoize :data_plan_id

  # Return the type of intelliflow query (client or server)
  #
  # Example
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "selector",
  #       "dimension": "type",
  #       "value": "server"
  #     },
  #     ...
  #   ]
  # }
  def intelliflow_type
    case filter[TYPE]
    when AND
      filter[FIELDS].each do |filter_field|
        if filter_field[TYPE] == SELECTOR && filter_field[DIMENSION] == TYPE
          return filter_field[VALUE]
        end
      end
      nil
    end
  end
  memoize :intelliflow_type

  def intelliflow_server?
    intelliflow_type == SERVER
  end

  def intelliflow_client?
    intelliflow_type == CLIENT
  end

  # Return the intelliflow server to filter by
  #
  # Example
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "selector",
  #       "dimension": "server",
  #       "value": "trustedsource.org"
  #     },
  #     ...
  #   ]
  # }
  def intelliflow_server
    case filter[TYPE]
    when AND
      filter[FIELDS].each do |filter_field|
        if filter_field[TYPE] == SELECTOR && filter_field[DIMENSION] == SERVER
          return filter_field[VALUE]
        end
      end
      nil
    end
  end
  memoize :intelliflow_server

  # Return the list of intelliflow servers to filter by
  #
  # Example
  #
  # "filter": {
  #   "type": "and",
  #   "fields": [
  #     {
  #       "type": "or",
  #       "fields": [
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "144.160.230.126"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "192.168.1.223"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "Google"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "10.13.168.52"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "144.160.242.204"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "10.222.226.207"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "10.102.37.208"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "mozaws.net"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "trustedsource.org"
  #         },
  #         {
  #           "type": "selector",
  #           "dimension": "server",
  #           "value": "10.102.37.194"
  #         }
  #       ]
  #     },
  #     ...
  #   ]
  # }
  def intelliflow_filter_servers
    or_fields = filter[FIELDS].find { |filter_field| filter_field[TYPE] == OR }[FIELDS]
    servers = or_fields.map { |filter_field| filter_field[VALUE] }
    servers
  end
  memoize :intelliflow_filter_servers

  private

  def iso8601_to_epoch(string)
    DateTime.parse(string).to_time.utc.to_i
  end
end
