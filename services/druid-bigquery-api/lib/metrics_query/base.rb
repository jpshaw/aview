module MetricsQuery
  class Base

    attr_reader :request

    delegate :start_time, :end_time, :granularity, :device_id, :device_ids,
      :device_interface, :data_plan_id, :timeseries?, :group_by?, :start_of_hour,
      :intelliflow_client?, :intelliflow_server?, :intelliflow_server,
      :intelliflow_filter_servers, to: :request

    delegate :debug, :info, :warn, :error, to: :logger

    ALL  = 'all'.freeze
    HOUR = 'hour'.freeze
    DAY  = 'day'.freeze

    def initialize(request)
      @request = request
    end

    def run_query(sql, name: '')
      debug_sql(sql, name)
      self.class.connection.query(sql).to_a
    end

    class << self
      def connection
        @connection ||= Google::Cloud::Bigquery.new
      end
    end

    def partition_time
      num_days = 3
      partition_time_null = end_time_db > Date.yesterday.to_time.to_i ? "OR _PARTITIONTIME IS NULL" : ""

      sql = <<~SQL
        AND (
               ( _PARTITIONTIME > TIMESTAMP_SUB(TIMESTAMP_SECONDS(#{start_time_db}), INTERVAL 1 DAY)
                 AND _PARTITIONTIME < TIMESTAMP_ADD(TIMESTAMP_SECONDS(#{end_time_db}), INTERVAL #{num_days} DAY)
               )
               #{partition_time_null}
             )
      SQL
      sql
    end

    private

    def device_ids_sql
      device_ids.map { |id| "'#{id}'" }.join(',')
    end

    def logger
      ::MetricsQuery.logger
    end

    def debug_sql(sql, name)
      length = [0, 75 - name.length].max
      message = "\n== #{name} #{'=' * length}\n#{sql.strip}\n#{'=' * 80}"
      logger.debug message if logger.debug?
    end

    HOUR_INTERVAL = 3600
    DAY_INTERVAL = 86400

    def interval_db
      @interval_db ||= granularity == HOUR ? HOUR_INTERVAL :
                       granularity == DAY  ? DAY_INTERVAL  :
                       (end_time - start_time) >  DAY_INTERVAL ? DAY_INTERVAL : HOUR_INTERVAL
    end

    def start_time_db
      @start_time_db ||= start_time - (start_time % interval_db)
    end

    def end_time_db
      @end_time_db ||= end_time - (end_time % interval_db)
    end

  end
end

Dir["#{File.expand_path(__dir__)}/queries/*.rb"].each { |f| require f }
