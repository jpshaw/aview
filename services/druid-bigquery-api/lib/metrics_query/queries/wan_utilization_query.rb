class WanUtilizationQuery < MetricsQuery::Base

  def execute
    chart
  end

  private

  def chart
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, #{granularity.upcase}, 'UTC')) AS timestamp,
        COUNT(*) AS count,
        interface AS interface,
        SUM(upload) AS total_upload,
        SUM(upload_hwm) AS total_upload_hwm,
        SUM(download) AS total_download,
        SUM(download_hwm) AS total_download_hwm,
        AVG(cast(NULLIF(upload, 0) AS INT64)) AS upload,
        AVG(upload_hwm) AS upload_hwm,
        AVG(cast(NULLIF(download, 0) AS INT64)) AS download,
        AVG(download_hwm) AS download_hwm
      FROM
        application.metrics_wan_utilization
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        #{partition_time}
      GROUP BY
        timestamp,
        interface
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: "wan_utilization/chart/#{granularity}"
  end

end
