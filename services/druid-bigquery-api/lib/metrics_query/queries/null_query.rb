class NullQuery < MetricsQuery::Base
  def execute
    warn "Metric type not found: `#{request.metric_type}`"
    []
  end
end
