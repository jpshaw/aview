class CellularUtilizationQuery < MetricsQuery::Base

  def execute
    case granularity
    when ALL
      if group_by?
        data_plan
      else
        chart_all
      end
    when HOUR
      chart_by_granularity
    when DAY
      if timeseries?
        daily_total
      else
        chart_by_granularity
      end
    end
  end

  private

  def daily_total
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, DAY, 'UTC')) AS timestamp,
        SUM(received) AS received,
        SUM(transmitted) AS transmitted
      FROM
        application.metrics_cellular_utilization
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        #{interface_filter_sql}
        #{partition_time}
      GROUP BY
        timestamp
    SQL

    run_query sql, name: "cellular_utilization/daily_total"
  end

  def data_plan
    end_time_data_plan = end_time_db + MetricsQuery::Base::DAY_INTERVAL

    sql = <<~SQL
      SELECT
        #{group_by_sql_select}
        IFNULL(UNIX_SECONDS(MAX(timestamp)), UNIX_SECONDS(CURRENT_TIMESTAMP())) AS timestamp,
        IFNULL(SUM(received), 0) AS received,
        IFNULL(SUM(transmitted), 0) AS transmitted,
        (IFNULL(SUM(received), 0) + IFNULL(SUM(transmitted), 0)) as data_used
      FROM
        application.metrics_cellular_utilization
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_data_plan} )
        #{data_plan_filter_sql}
        #{device_filter_sql}
        #{partition_time}
        #{group_by_sql_group}
    SQL

    run_query sql, name: "cellular_utilization/data_plan"
  end

  def chart_all
    sql = <<~SQL
      SELECT
        IFNULL(UNIX_SECONDS(MAX(timestamp)), UNIX_SECONDS(CURRENT_TIMESTAMP())) AS timestamp,
        IFNULL(SUM(received), 0) AS received,
        IFNULL(SUM(transmitted), 0) AS transmitted,
        carrier AS carrier
      FROM
        application.metrics_cellular_utilization
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        #{partition_time}
      GROUP BY
        carrier
    SQL

    run_query sql, name: "cellular_utilization/chart_all"
  end

  def chart_by_granularity
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, #{granularity.upcase}, 'UTC')) AS timestamp,
        IFNULL(SUM(received), 0) AS received,
        IFNULL(SUM(transmitted), 0) AS transmitted,
        carrier AS carrier
      FROM
        application.metrics_cellular_utilization
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        #{partition_time}
      GROUP BY
        timestamp,
        carrier
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: "cellular_utilization/chart_by_granularity"
  end

  def interface_filter_sql
    "AND interface = '#{device_interface}'" if device_interface.present?
  end

  def device_filter_sql
    "AND mac IN (#{device_ids_sql})" if device_ids.present?
  end

  def group_by_sql_select
    select = "mac AS mac," if group_by?
    select += "carrier AS carrier," if !data_plan_id
    select
  end

  def group_by_sql_group
    group = "GROUP BY mac" if group_by?
    group += ", carrier" if !data_plan_id
    group
  end

  def data_plan_filter_sql
    "AND data_plan = #{data_plan_id}" if data_plan_id
  end

end
