class AvailabilityQuery < MetricsQuery::Base

  def execute
    case granularity
    when ALL
      total_by_device
    when HOUR, DAY
      trend_chart
    end
  end

  private

  def total_by_device
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(MAX(timestamp)) AS timestamp,
        mac AS mac,
        SUM(count) AS count,
        state AS state
      FROM
        application.metrics_availability
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac IN (#{device_ids_sql})
        #{partition_time}
      GROUP BY
        mac,
        state
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: 'availability/total_by_device'
  end

  def trend_chart
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, #{granularity.upcase}, 'UTC')) AS timestamp,
        SUM(count) AS count,
        state AS state
      FROM
        application.metrics_availability
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac IN (#{device_ids_sql})
        #{partition_time}
      GROUP BY
        timestamp,
        state
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: "availability/trend_chart/#{granularity}"
  end

end
