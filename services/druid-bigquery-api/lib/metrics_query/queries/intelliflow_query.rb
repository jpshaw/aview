class IntelliflowQuery < MetricsQuery::Base

  def execute
    case granularity
    when ALL
      if timeseries?
        if intelliflow_client?
          total_client_usage
        elsif intelliflow_server?
          total_client_usage_for_app
        end
      else
        if intelliflow_client?
          data_usage_per_client
        elsif intelliflow_server?
          if intelliflow_server.present?
            data_usage_per_client_for_app
          else
            data_usage_per_app
          end
        end
      end
    when HOUR
      chart_by_granularity
    when DAY
      chart_by_granularity
    end
  end

  private

  def data_usage_per_client
    sql = <<~SQL
      SELECT
        #{timestamp_for_all_sql},
        client AS client,
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time})
        AND mac = '#{device_id}'
        AND type = 'client'
      GROUP BY
        client
      ORDER BY
        sum desc
      LIMIT
        10
    SQL

    run_query sql, name: "intelliflow/data_usage_per_client"
  end

  def data_usage_per_client_for_app
    sql = <<~SQL
      SELECT
        #{timestamp_for_all_sql},
        client AS client,
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time})
        AND mac = '#{device_id}'
        AND type = 'server'
        AND server = '#{intelliflow_server}'
      GROUP BY
        client
      ORDER BY
        sum desc
      LIMIT
        10
    SQL

    run_query sql, name: "intelliflow/data_usage_per_client_for_app"
  end

  def total_client_usage
    sql = <<~SQL
      SELECT
        #{timestamp_for_all_sql},
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time})
        AND mac = '#{device_id}'
        AND type = 'client'
    SQL

    run_query sql, name: "intelliflow/total_client_usage"
  end

  def total_client_usage_for_app
    sql = <<~SQL
      SELECT
        #{timestamp_for_all_sql},
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time})
        AND mac = '#{device_id}'
        AND type = 'server'
        AND server = '#{intelliflow_server}'
    SQL

    run_query sql, name: "intelliflow/total_client_usage_for_app"
  end

  def data_usage_per_app
    sql = <<~SQL
      SELECT
        #{timestamp_for_all_sql},
        server AS server,
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time})
        AND mac = '#{device_id}'
        AND type = 'server'
      GROUP BY
        server
      ORDER BY
        sum desc
      LIMIT
        10
    SQL

    run_query sql, name: "intelliflow/data_usage_per_app"
  end

  def chart_by_granularity
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, #{granularity.upcase}, 'UTC')) AS timestamp,
        server as server,
        SUM(usage) AS sum
      FROM
        application.metrics_intelliflow
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        AND type = 'server'
        AND server in (#{intelliflow_servers_sql})
        #{partition_time}
      GROUP BY
        timestamp,
        server
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: "intelliflow/chart_by_granularity/#{granularity}"
  end

  def timestamp_for_all_sql
    "IFNULL(UNIX_SECONDS(MAX(timestamp)), UNIX_SECONDS(CURRENT_TIMESTAMP())) AS timestamp"
  end

  def intelliflow_servers_sql
    intelliflow_filter_servers.map { |server| "'#{server}'" }.join(',')
  end

end
