class BandwidthTestResultQuery < MetricsQuery::Base

  def execute
    chart
  end

  private

  def chart
    sql = <<~SQL
      SELECT
        UNIX_SECONDS(TIMESTAMP_TRUNC(timestamp, #{granularity.upcase}, 'UTC')) AS timestamp,
        COUNT(*) AS count,
        interface AS interface,
        SUM(upload_result) AS total_upload,
        SUM(download_result) AS total_download,
        MAX(upload_result) AS upload_average,
        MAX(download_result) AS download_average,
        MAX(bandwidth_type) AS bandwidth_type,
        MAX(bandwidth_test_status) AS bandwidth_test_status
      FROM
        application.metrics_bandwidth_test
      WHERE
        timestamp >= TIMESTAMP_SECONDS(#{start_time_db})
        AND timestamp < TIMESTAMP_SECONDS(#{end_time_db})
        AND mac = '#{device_id}'
        #{partition_time}
      GROUP BY
        timestamp,
        interface
      ORDER BY
        timestamp ASC
    SQL

    run_query sql, name: "bandwidth_test_result/chart/#{granularity}"
  end

end
