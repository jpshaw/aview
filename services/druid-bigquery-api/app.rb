# Add lib folder to require path
lib = File.expand_path('../lib', __FILE__)
$:.unshift(lib) unless $:.include?(lib)

# Gems
require 'multi_json'
require 'sinatra'
require 'rack/contrib'
require 'google/cloud/bigquery'
require 'memoist'
require 'logger'
require 'active_support/core_ext/module/delegation'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/date_time'
require 'active_support/core_ext/numeric/time'

# lib/*
require 'druid_request'
require 'druid_response'
require 'metrics_query'

ENV['GOOGLE_CLOUD_PROJECT'] ||= 'aview-staging'
ENV['GOOGLE_CLOUD_KEYFILE'] ||= './aview-staging-keyfile.json'

use Rack::PostBodyContentTypeParser

before do
  content_type :json
end

post '/druid/v2' do
  request  = DruidRequest.new(env['rack.request.form_hash'])
  results  = MetricsQuery.execute(request)
  response = DruidResponse.new(request, results).to_json
  response
end

error do
  status 500
  { error: 500, message: env['sinatra.error'].message }.to_json
end
